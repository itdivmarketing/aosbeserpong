(function($) {

	// widget perspective plugin
	$.fn.binus_datepicker = function() {

		return $(this).each(function($index, $object) {
			if ($(this).data('has-init') == 'yes')
				return;
			__create_element(this);
		});

		function __create_element($element) {
			$($element).find('.datepicker').datepicker('destroy');
			$($element).find('.datepicker').datepicker({
				dateFormat  : 'd M yy',
				changeYear  : true,
				changeMonth : true,
				yearRange: 'c-50:c+5'
			});

			$($element).find('.icon-area').on('click', function() {
				$(this).parent().find('.datepicker').datepicker('show');
			});

			$($element).data('has-init', 'yes');
		}

	}

})(jQuery);
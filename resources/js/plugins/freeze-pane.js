(function($) {

	$.fn.binus_freeze_pane = function($options) {

		var $settings = $.extend({
			fixed_left     : 1,
			fixed_right    : 0,
			height         : 300,
			on_window_load : true
		}, $options);

		return $(this).each(function($index, $object) {
			if ($(this).data('has-init') == 'yes')
				return;
			__create_element(this);
		});

		function __create_element($element) {
			var $object = $($element);

			if ($object.parents('.popup').length > 0) {
				__init_element($object);
				return;
			}

			if ($settings.on_window_load) {
				$(window).load(function() {
					__init_element($object);
				});
			} else {
				__init_element($object);
			}
		}

		function __init_element($object) {
			var $fixed_columns_left = $settings.fixed_left;
			var $fixed_columns_right = $settings.fixed_right;
			var $freeze_height = $settings.height;

			var $table = $object.find('table').DataTable({
				paging: true,
				ordering: false,
				bFilter: true,
				info: true,
				scrollY: $freeze_height + 'px',
				scrollX: true
			});

			var $result = new $.fn.dataTable.FixedColumns($table, {
				leftColumns: $fixed_columns_left,
				rightColumns: $fixed_columns_right
			});

			$object.data('has-init', 'yes');
		}

	}

})(jQuery);
(function($) {

	$(document).ready(function() {

		/**
		 * Set container to the middle
		 * ------------------------------------------
		 */
		$window_height = $(window).height();

		$('.container').css('margin-top', (($window_height - $('.container').height()) / 2) + 'px');

	});

})(jQuery);
var loadMenu = function() {
	
}
function Show_Pagination(Opt)
{
	/*
		'Selector' : '#paging',
		'TotalPage' : ".$data['pagination']['last'].",
		'CurrentPage' : ".$data['pagination']['current'].",
		'PreviousPage' : ".$data['pagination']['previous'].",
		'NextPage' : ".$data['pagination']['next'].",
		'InfoPage' : ".$data['pagination']['info']."
	*/
	// Create Pagination , Ex : bo/cataloging/ddc_and_topic
	var Selector = Opt.Selector,
	TotalPage = Math.ceil(Opt.TotalPage),
	CurrentPage = Opt.CurrentPage || null;
	
	$(Selector).css("display","block");
	
	var PageNavi = $(Selector+" > .pagination");

	PageNavi.find(".page-display").html("Page "+CurrentPage+" of "+ TotalPage );
	PageNavi.find(".page-number").empty();
	
	// Show or Hidden .. in FirstPage
	if(CurrentPage > 5)
	{
		PageNavi.find(".page-number").append('<a class="item page-click" data-page="1">1</a>');
		PageNavi.find(".page-number").append('<a href="#" class="item">...</a>');
	}

	for(var i = (Math.ceil(CurrentPage/5)*5)-4; i <= Math.ceil(CurrentPage/5)*5  ; i++) 
	{	
		if(i == CurrentPage)
		{
			PageNavi.find(".page-number").append('<span class="item current">'+i+'</span>');			
		}
		else if(i <= TotalPage)
		{
			PageNavi.find(".page-number").append('<a class="item page-click" data-page="'+i+'">'+i+'</a>');
		}
	}
	
	// Show or Hidden .. LastPage
	if(Math.ceil(CurrentPage/5)*5 <= TotalPage)
	{
		PageNavi.find(".page-number").append('<a class="item next-dot">...</a>');
		PageNavi.find(".page-number").append('<a class="item page-click" data-page="'+TotalPage+'">'+TotalPage+'</a>');
	}

	var PageButtonNavi = $(Selector+" > .pagination > .page-button");
	// Disable enable Prev Next
	if(CurrentPage == 1)
	{
		PageButtonNavi.find(".prev").addClass("disable");
		$(".prev").css("visibility", "hidden");
	}
	else
	{	
		PageButtonNavi.find(".prev").removeClass("disable");
		$(".prev").css("visibility", "visible");
	}
	
	if(CurrentPage == TotalPage)
	{
		PageButtonNavi.find(".next").addClass("disable");
		$(".next").css("visibility", "hidden");
	}
	else
	{
		PageButtonNavi.find(".next").removeClass("disable");
		$(".next").css("visibility", "visible");		
	}
}
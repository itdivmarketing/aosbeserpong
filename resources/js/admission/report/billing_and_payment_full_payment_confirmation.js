function numeric(event) 
{
	// Backspace, tab, enter, end, home, left, right
	// We don't support the del key in Opera because del == . == 46.
	var controlKeys = [8, 9, 13, 35, 36, 37, 39];
	// IE doesn't support indexOf
	var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
	// Some browsers just don't raise events for control keys. Easy.
	// e.g. Safari backspace.
	if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
	  (48 <= event.which && event.which <= 57) || // Always 0 through 9
	  isControlKey) { // Opera assigns values for control keys.
	return;
	}else{
	event.preventDefault();
	}
}

var validate = function() {
	if($('[name="hfRegistrantID"]').val() == '')
		alert('RegistrantID required');
	else if($('[name="txtName"]').val() == '')
		alert('Name required');
	else if($('[name="txtCode"]').val() == '')
		alert('Letter Number required');
	else if($('[name="txtLetterNumber"]').val() == '')
		alert('Letter Number required');
	else if($('[name="cbSpecial"]').is(':checked') && $('[name="ddlPackage"]').val() == '')
		alert('Package required');
	else
	{
		$('[name="frmSearch"]').submit();
		//window.open( "view/template_report/laporan/print/rekap_pengembalian.html#/d="+start+"."+end);
	}
}

$(document).ready(function()
{
	$('[name="cbSpecial"]').change(function(){
		if($(this).is(':checked'))
		{
			$('[name="ddlPackage"]').attr('disabled',false);
			$('[name="cbDiscount"]').attr('disabled',false);
		}
		else
		{
			$('[name="ddlPackage"]').prop('selectedIndex', 0);
			$('[name="ddlPackage"]').attr('disabled','disabled');
			$('[name="cbDiscount"]').attr('checked',false);
			$('[name="cbDiscount"]').attr('disabled','disabled');
		}
	});
	
	$('.numeric').keypress(numeric);
	
	$('#btnPrint').on('click',validate);
});
var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/form_return_validate_form/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="all">ALL</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};

var loadYearLevel = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/form_return_validate_form/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlYearLevel"]').empty().append($('<option value="all">ALL</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="ddlYearLevel"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
			$('[name="ddlYearLevel"]').trigger("change");
        } 
	});
};

var validate = function() {
	if($('[name="ddlSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('[name="txtStartPeriod"]').val() == '')
		alert('Start Period required');
	else if($('[name="txtEndPeriod"]').val() == '')
		alert('End Period required');
	else if($('[name="txtFinancialController"]').val() == '')
		alert('financial controller required');
	else
	{
		var startDate = new Date($('[name="txtStartPeriod"]').val());
		var endDate = new Date($('[name="txtEndPeriod"]').val());
			
		if(startDate > endDate)
		{
			alert('Start Period must be later than End Period');
		}
		else
		{
			$('[name="frmSearch"]').submit();
		}
		//window.open( "view/template_report/laporan/print/rekap_pengembalian.html#/d="+start+"."+end);
	}
}

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
		loadYearLevel($(this).val());
	});
	
	$('#btnPrint').on('click',validate);
});
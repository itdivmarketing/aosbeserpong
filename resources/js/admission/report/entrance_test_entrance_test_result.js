var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/entrance_test_entrance_test_result/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};

var validate = function() {
	if($('[name="ddlSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('[name="ddlAdmissionTerm"]').val() == '')
		alert('Admission Term required');
	else
	{
		$('[name="frmSearch"]').submit();
	}
}

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
	});
	
	$('#btnPrint').on('click',validate);
});
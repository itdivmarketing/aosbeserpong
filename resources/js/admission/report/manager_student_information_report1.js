var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/manager_student_information_report/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="all">ALL</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};


var loadYearLevel = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/manager_student_information_report/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="YearLevel"]').empty().append($('<input type="checkbox" name="cbYearLevelAll" value="all"/> All </br>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="YearLevel"]').append($('<input type="checkbox" class="yearLevel" name="cbYearLevel[]" value="'+$.trim(data.YearLevel[i].YearLevelID)+'"/>'+data.YearLevel[i].YearLevelName + '<br/>'));
				}
			}
			$('[name="cbYearLevelAll"]').change(function() {
				if($(this).is(":checked")) {
					$('.yearLevel').attr('checked','checked');
				}
				else {
					$('.yearLevel').attr('checked',false);
				}
			});
        } 
	});
};

var validate = function() {
	
	if($('[name="ddlSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('.yearLevel:checked').length < 1)
		alert('Year Level required');
	else if($('.field:checked').length < 1)
		alert('Field required');
	else
	{
		$('[name="frmSearch"]').submit();
		//window.open( "view/template_report/laporan/print/rekap_pengembalian.html#/d="+start+"."+end);
	}
}

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
		loadYearLevel($(this).val());
	});
	$('[name="cbYearLevelAll"]').change(function() {
        if($(this).is(":checked")) {
            $('.yearLevel').attr('checked','checked');
        }
		else {
			$('.yearLevel').attr('checked',false);
		}
	});
	
	$('[name="cbFieldAll"]').change(function() {
        if($(this).is(":checked")) {
            $('.field').attr('checked','checked');
        }
		else {
			$('.field').attr('checked',false);
		}
	});
		
	$('#btnPrint').on('click',validate);
});
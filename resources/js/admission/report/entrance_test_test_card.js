function numeric(event) 
{
	// Backspace, tab, enter, end, home, left, right
	// We don't support the del key in Opera because del == . == 46.
	var controlKeys = [8, 9, 13, 35, 36, 37, 39];
	// IE doesn't support indexOf
	var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
	// Some browsers just don't raise events for control keys. Easy.
	// e.g. Safari backspace.
	if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
	  (48 <= event.which && event.which <= 57) || // Always 0 through 9
	  isControlKey) { // Opera assigns values for control keys.
	return;
	}else{
	event.preventDefault();
	}
}

var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/entrance_test_test_card/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};

var loadSchedule = function(SchoolLevel, PretestTerm){
	$.ajax({
		url: SiteURL+'report/entrance_test_test_card/get_schedule/'+SchoolLevel+'/'+PretestTerm,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlSchedule"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.Schedule)
			{
				for(var i in data.Schedule)
				{
					$('[name="ddlSchedule"]').append($('<option value="'+$.trim(data.Schedule[i].ScheduleID)+'">'+$.trim(data.Schedule[i].ScheduleID)+'</option>'));
				}
			}
			$('[name="ddlSchedule"]').trigger("change");
        } 
	});
};

var validate = function() {
	
	if($('[name="ddlSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('[name="ddlAdmissionTerm"]').val() == '')
		alert('Admission Term required');
	else if($('[name="ddlSchedule"]').val() == '')
		alert('Schedule required');
	else if($('#tblPrint :checkbox:checked').length <= 0)
		alert('Registrant required');
	else
	{
		$('[name="frmPrint"]').submit();
	}
}

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
	});
	
	$('[name="ddlAdmissionTerm"]').change(function(){
		loadSchedule($('[name="ddlSchoolLevel"]').val(),$(this).val());
	});
	
	$('.numeric').keypress(numeric);
	
	$('[name="cbCheckAll"]').change(function() {
		if($(this).is(":checked")) {
			$('[name="frmPrint"]').each(function(){
				$(this).find(':checkbox').prop('checked',true);
			})
        }
		else {
			$('[name="frmPrint"]').each(function(){
				$(this).find(':checkbox').prop('checked',false);
			})
		}
	});
	
	$('#btnPrint').on('click',validate);
	//$('#btnSearch').on('click',validate);
	$('#btnSearch').on('click',function(){ $('[name="frmSearch"]').submit(); });
});
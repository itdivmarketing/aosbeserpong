var validate = function() {
	if($('[name="rbReportType"]').val() == 'form_sold' && $('[name="ddlSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('[name="rbReportType"]').val() == 'metrics_statistic' && $('[name="ddlAdmissionTerm"]').val() == '')
		alert('Term required');
	else
	{
		$('[name="frmSearch"]').submit();
		//window.open( "view/template_report/laporan/print/rekap_pengembalian.html#/d="+start+"."+end);
	}
}

$(document).ready(function()
{
	$('#div_form_sold').hide();
	$('#div_marketing_metrics').hide();
	$('[name="ddlSchoolLevel"]').change(function(){
		$('[name="hfSchoolLevel"]').val($(this).find(":selected").text().trim());
	});
	
	
	$('[name="rbReportType"]').change(function(){
		$('#div_form_sold').hide();
		$('#div_marketing_metrics').hide();
		
		if ($(this).val() == 'form_sold')
		{
			$('#div_form_sold').show();
		}
		else if ($(this).val() == 'metrics_statistic')
		{
			$('#div_marketing_metrics').show();
		}
		
		
	});
	
	$('#btnPrint').on('click',validate);
});
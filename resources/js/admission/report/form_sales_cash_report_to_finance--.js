function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'report/form_sales_cash_report_to_finance/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="all">All</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
        } 
	});
};

var validate = function() {
	if($('[name="ddlSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('[name="ddlAdmissionTerm"]').val() == '')
		alert('Admission Term required');
	else if($('[name="txtStartPeriod"]').val() == '')
		alert('Start Period required');
	else if($('[name="txtEndPeriod"]').val() == '')
		alert('End Period required');
	else if($('[name="txtFinancialController"]').val() == '')
		alert('financial controller required');
	else
	{
		var startDate = new Date($('[name="txtStartPeriod"]').val());
		var endDate = new Date($('[name="txtEndPeriod"]').val());
			
		if(startDate > endDate)
		{
			alert('Start Period must be later than End Period');
		}
		else
		{
			$('[name="frmSearch"]').submit();
		}
		//window.open( "view/template_report/laporan/print/rekap_pengembalian.html#/d="+start+"."+end);
	}
}

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
	});
	
	$(".datepicker").datepicker( "setDate", new Date());
	
	$('#btnPrint').on('click',validate);
});
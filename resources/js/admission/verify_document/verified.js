function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}




var getVerifyDocumentView = function(FormNumber){
	var AjaxData;
	$.ajax({
		url: SiteURL+'verify_document/verified/get_view_verify_document/'+FormNumber,
		type: 'post',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			AjaxData=data;
		
				$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {

    		}
    		});
        }	,complete:function(){
				if(AjaxData.VerifyDocumentView)
				{
					
					for(var i in AjaxData.VerifyDocumentView)
					{ 
						$('[name="txtViewFormNumber"]').val( AjaxData.VerifyDocumentView[i].FormNumber);
						$('[name="txtViewStudentName"]').val( AjaxData.VerifyDocumentView[i].StudentName);
						$('[name="txtViewSchoolLevelName"]').val( AjaxData.VerifyDocumentView[i].SchoollevelName);
								
						$('[name="hfViewFormNumber"]').val( AjaxData.VerifyDocumentView[i].FormNumber);
						$('[name="hfViewSchoolLevel"]').val( AjaxData.VerifyDocumentView[i].SchoollevelName);
					}
				}
		
			
		} 
	});
};



var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	
	$.ajax({
		url: SiteURL+'verify_document/verified/get_submission_data/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
			
			if(AjaxData.SubmissionData)
			{
			
				var Form=$('#modal #frmVerify');
				var FileUrl=$('#file_url').val();
			
				$('#TableVerify').find('td').parent().remove();
				for(var i in AjaxData.SubmissionData)
				{		
					var checked=new Array();
					checked[1]="false";
					checked[2]="false";
					checked[3]="false";
					//alert(AjaxData.SubmissionData[i].Verification);
					switch(AjaxData.SubmissionData[i].Verification){
						case '1':checked[1]="checked";
							break;
						case '2':checked[2]="checked";
							break;
						case '3':checked[3]="checked";
							break;
					
					}
					var LinkString='<td><a href="'+(AjaxData.SubmissionData[i].FileName==null?'':FileUrl+AjaxData.SubmissionData[i].FileName)+'" target="_blank" ><i class="icon icon-view btnViewFile"  filename="'+AjaxData.SubmissionData[i].FileName+'"  ></i></a></td>';
					if(AjaxData.SubmissionData[i].FileName==null || AjaxData.SubmissionData[i].FileName==""){
						LinkString="<td>No File</td>";
					}
					if(AjaxData.SubmissionData[i].FileName=="dokumen_fisik.jpg"){
						LinkString="<td>File Fisik</td>";
					}
			
					var cell = [
						'<input type="hidden" name="hfDocumentID[]" value="'+AjaxData.SubmissionData[i].DocumentID+'" ></input>'
						,'<td>'+AjaxData.SubmissionData[i].DocumentName+'</td>'
						,LinkString
						,'<td><input type="radio" name="rbtStatus-'+i+'" id="rbtReceive" value="1"  '+(AjaxData.SubmissionData[i].Verification == 1 ? 'checked="checked"' : '' )+' "/></td>'
						,'<td><input type="radio" name="rbtStatus-'+i+'" id="rbtReject"  value="2"  '+(AjaxData.SubmissionData[i].Verification == 2 ? 'checked="checked"' : '' )+' /></td>'
						,'<td><input type="radio" name="rbtStatus-'+i+'"  id="rbtPending"  value="3" '+(AjaxData.SubmissionData[i].Verification == 3 ? 'checked="checked"' : '' )+'  /></td>'
						,'<td><input type="text" name="txtNote[]" maxlength=100 id="txtNote"  value="'+(AjaxData.SubmissionData[i].Note == null ? "" : AjaxData.SubmissionData[i].Note )+'"  /></td>'
						//,'<td><input type="checkbox" name="cbAllocation[]" class="comboboxAllocation" value="'+AjaxData.registrants[i].RegistrantID+'" '+(AjaxData.registrants[i].cek == 1 ? 'checked="checked"' : '' )+' /></td>'
					];
					var row = $('<tr data-id="'+AjaxData.SubmissionData[i].RegistrantID+'">'+cell.join()+'</tr>');
					$('#TableVerify').append(row);
				}
			} 
			else
			{
			
			}
		
        },
		complete:function(){
			getVerifyDocumentView(row.attr('data-id'));
		
			
			

			
		}
	});
	
};


function save(){
	//alert("tert");
	
	var FormNumber=$.trim($('[name="txtViewFormNumber"]').val());
	var Name=$.trim($('[name="txtViewStudentName"]').val());
	var SchoolLevel=$.trim($('[name="txtViewSchoolLevelName"]').val());
	
	if(FormNumber=="")
		alert("FormNumber Required");
	else if(Name=="")
		alert("Name Required");
	else if(SchoolLevel=="")
		alert("SchoolLevel Required");
	else
		$('#internal-popup [name="frmVerify"]').submit();		
}

var viewFile = function(){
		$("#modal-file_title").html($(this).attr("filename"));
		$("#popup-image").attr("src",$(this).attr("source"));
		$.fancybox.close();

		$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {

    		}
    	});
};

var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'verify_document/verified/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};


var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'verify_document/verified/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlAdmissionTerm");
		loadYearLevel($(this).val(),"ddlYearLevel");
	});
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val("");
		$('[name=frmSearch]').submit();
	});

	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
		$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
		$('[name=frmSearch]').submit();
	});
	$('.next').on('click',function(){
		$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
		$('[name=frmSearch]').submit();
	});
	
	$('#RateContent').on('click','.btnView',view);
	
});
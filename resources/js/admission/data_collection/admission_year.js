function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validate(){
	if($('.fancybox-opened [name="ddlSchoolLevel"]').val() == '' && $('.fancybox-opened [name="hfSchoolLevel"]').val() == '')
		alert('School Level required');
	else if($('.fancybox-opened  [name="txtSchoolDate"]').val() == '')
		alert('School Date required');
	else if($('.fancybox-opened [name="txtEffectiveDate"]').val() == '')
		alert('Effective Date required');
	// else if($('.fancybox-opened  [name="cbETGrading"]').val() == '')
	// 	alert('Entrance Test Grading required');
	else if(!(document.getElementById('yes').checked || document.getElementById('no').checked))
		alert('Entrance Test Grading required');
	else
	{
		var currDate = new Date();
		var schoolDate = new Date(Date.parse($('.fancybox-opened  [name="txtSchoolDate"]').val()));
		var effectiveDate = new Date(Date.parse($('.fancybox-opened  [name="txtEffectiveDate"]').val()));
		
		if(currDate > schoolDate)
			alert('school date cannot be in the past');
		else if(currDate > effectiveDate)
			alert('effective date cannot be in the past');
		else if(schoolDate > effectiveDate)	
			alert('effective date must be greater than school date');
		else
			$('.fancybox-opened  [name="frmAdd"]').submit();
	}
}

function Popup_reset(){
	$('.fancybox-opened [name="hfAdmissionID"]').val("");
	$('.fancybox-opened [name="txtAdmissionID"]').val("");
	$('.fancybox-opened [name="txtAdmissionYear"]').val("");
	$('.fancybox-opened [name="txtAdmissionSemester"]').val("");
	$('.fancybox-opened [name="ddlSchoolLevel"]').removeAttr('disabled');
	$('.fancybox-opened [name="ddlSchoolLevel"]').parent().removeClass('disabled');
	$('.fancybox-opened [name="ddlSchoolLevel"]').trigger("change");
	$('.fancybox-opened [name="txtSchoolDate"]').val("");
	$('.fancybox-opened [name="txtEffectiveDate"]').val("");
	$('input[name=cbETGrading]').prop('checked', false);

}

function Popup_add(){
	//Show FancyBox
		$.fancybox({
	        	href 		: '#internal-popup',
	       	    minWidth    : 640,
	            fitToView   : true,
	            closeClick  : false,
	        	afterShow: function () {
	        		Popup_reset();
	        		$('[name="hfStatus"]').val('insert');
					//Set to default
					//$('[name="ddlViewBuilding"]').val('');
					//var txtSelected = $("[name='ddlViewBuilding'] option[value='']").text();
					//$('[name="frmAdd"] .combobox-label').text(txtSelected);
					//$('.fancybox-opened #btnSave').click(validate);
	    		},
	    		beforeClose: function(){
	    			
	    		}
	    });
	
}

function Popup_edit(data){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		if(data.AdmissionYear){
				$('.fancybox-opened [name="hfAdmissionID"]').val(data.AdmissionYear[0].AdmissionID);
				$('.fancybox-opened [name="txtAdmissionID"]').val(data.AdmissionYear[0].AdmissionID);
				$('.fancybox-opened [name="txtAdmissionYear"]').val(data.AdmissionYear[0].AcademicYear);
				$('.fancybox-opened [name="txtAdmissionSemester"]').val(data.AdmissionYear[0].SmtId);
				$('.fancybox-opened [name="hfSchoolLevel"]').val(data.AdmissionYear[0].SchoolLevelID);
				
				$('.fancybox-opened [name="ddlSchoolLevel"]').attr('disabled','disabled');
				$('.fancybox-opened [name="ddlSchoolLevel"]').parent().addClass('disabled');
				$('.fancybox-opened [name="ddlSchoolLevel"]').val(data.AdmissionYear[0].SchoolLevelID);

				var selected = $('.fancybox-opened [name="ddlSchoolLevel"]').val();
				var txtSelected = $('.fancybox-opened [name="ddlSchoolLevel"] option[value="'+selected+'"]').text();
				$('.fancybox-opened #ddlSchoolLevel .combobox-label').text(txtSelected);


				$('.fancybox-opened [name="txtSchoolDate"]').val(data.AdmissionYear[0].CommencingDate);
				$('.fancybox-opened [name="txtEffectiveDate"]').val(data.AdmissionYear[0].EffectiveDate);
				$("input[name=cbETGrading][value=" + data.AdmissionYear[0].PreTestGrading + "]").prop('checked', true);
				}
				//$('.fancybox-opened #btnSave').click(validate);
    		},
    		beforeClose: function(){
    			//popup_rest();
    		}
   });
}



var edit = function(){
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('update');
	$('[name="hfAdmissionID"]').val(row.attr('data-id'));
	
	$.ajax({
		url: SiteURL+'data_collection/admission_year/get_admission_year_schedule/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_edit(AjaxData);
		}
	});
};

var deleted = function(e){
	e.preventDefault();
	var ask = confirm('Are you sure to delete this record ?');
	if(!ask)return;
	
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('delete');
	$('[name="hfAdmissionID"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
};




$(document).ready(function()
{
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});
		

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});
	
	$('#tblView').on('click','.btnEdit',edit);
	$('#tblView').on('click','.btnDelete',deleted);
});
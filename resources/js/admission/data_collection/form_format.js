function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewAdmissionID"]').val(row.attr('data-id'));
	//alert($('[name="hfViewAdmissionID"]').val());
	$('[name="hfViewFirstPart"]').val(row.attr('data-second-id'));
	$('[name="frmAdd"]').submit();
	
};





var getAdmissionID = function(SchoolLevelID){
	$.ajax({
		url: SiteURL+'data_collection/form_format/get_admissionID/'+SchoolLevelID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};


var validate = function(status) {
	
	if($.trim($('.fancybox-opened [name="txtViewAdmissionID"]').val()) == '')
			alert('AdmissionID required');
	else if($.trim($('.fancybox-opened [name="txtViewAdmissionYear"]').val() )== '')
		alert('Admission Year is required');	
	else if($.trim($('.fancybox-opened [name="txtViewAdmissionSemester"]').val() )== '')
		alert('Semester required');
	else if($.trim($('.fancybox-opened [name="ddlViewSchoolLevel"]').val() )== '')
		alert('School Level required');
	else if($.trim($('.fancybox-opened [name="txtViewFirstPart"]').val() )== '')
		alert('Format Form required');
	else if($.trim($('.fancybox-opened [name="txtViewSecondPartLength"]').val() )== '')
		alert('digit Count is required');	
	else if(validateNumeric($.trim($('.fancybox-opened [name="txtViewSecondPartLength"]').val() ))== false)
		alert('digit Count must be numeric');	
	else if($.trim($('.fancybox-opened [name="txtViewSecondPartStartsFrom"]').val() )== '')
		alert('Start from is required');	
	else if(validateNumeric($.trim($('.fancybox-opened [name="txtViewSecondPartStartsFrom"]').val() ))== false)
		alert('Start from must be numeric');	
	else{
		$('[name="hfStatus"]').val(status);
		//alert(status);
		$('[name="hfViewSchoolLevel"]').val($('[name="ddlViewSchoolLevel"]').val());
		$('.fancybox-opened [name="frmAdd"]').submit();
	}	

}

function Popup_reset(){
	$('[name="txtViewAdmissionID"]').val("");
	$('[name="ddlViewSchoolLevel"]').val("");
	$('[name="ddlViewSchoolLevel"]').trigger("change");
	$('[name="txtViewFirstPart"]').val("");
	$('[name="txtViewSecondPartLength"]').val("");
	$('[name="txtViewSecondPartStartsFrom"]').val("");

}
function restartPage(){
	$('[name=hfPage]').val(1);
}

function Popup_add(){
	//Show FancyBox
	$('.heading').text('ADD FORM FORMAT');
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		$('.fancybox-opened #btnSave').show();
        		Popup_reset();
    //     		$('[name="ddlViewSchoolLevel"]').prop("disabled",false);
    //     		$('.fancybox-opened [name="hfStatus"]').val('add');
				// $('.fancybox-opened [name="hfVenueID"]').val('');
				
				$('[name="ddlViewSchoolLevel"]').change(function(){
					getAdmissionID($(this).val());
				});
    		},
    		afterClose: function(){
    		}
    });
}



var add= function(){
	var row = $(this).parents('tr');

	Popup_add();
	$('[name="ddlViewSchoolLevel"]').prop("disabled",false);
	
	$('[name="ddlViewSchoolLevel"]').change(function(){
		getAdmissionID($(this).val());
	});

	$('.fancybox-opened #btnSave').click(function(){
		validate("save");		
	});	
				
};


function Popup_view(AjaxData){
	$('.heading').text('VIEW FORM FORMAT');
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
        	afterShow: function () {
        		if(AjaxData.FormFormatView){
					$('[name="ddlViewSchoolLevel"]').prop("disabled",true);
					for(var i in AjaxData.FormFormatView){
						$('[name="txtViewAdmissionID"]').val(AjaxData.FormFormatView[i].AdmissionID);
						$('[name="txtViewAdmissionYear"]').val(AjaxData.FormFormatView[i].AcademicYear);
						$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormFormatView[i].SmtID);
						$('[name="ddlViewSchoolLevel"]').val(AjaxData.FormFormatView[i].SchoolLevelID);
						var txtSelected = $(".fancybox-opened [name='ddlViewSchoolLevel'] option[value="+AjaxData.FormFormatView[i].SchoolLevelID+"]").text();
						$('.fancybox-opened [name="frmAdd"] .combobox-label').text(txtSelected);
						$('[name="txtViewFirstPart"]').val(AjaxData.FormFormatView[i].FirstPart);
						$('[name="txtViewSecondPartLength"]').val(AjaxData.FormFormatView[i].SecondPartLength);
						$('[name="txtViewSecondPartStartsFrom"]').val(AjaxData.FormFormatView[i].SecondPartStartsFrom);

						CanBeDeleted=AjaxData.FormFormatView[i].CanBeDeleted;
					}
				} 
				else{
					alert("failed");
				}
			//$('[name="txtViewAdmissionSemester"]').val($('[name="hfViewAdmissionSemester"]').val());
			if(CanBeDeleted==0)
				$('.fancybox-opened #btnSave').hide();
			else{
				$('.fancybox-opened #btnSave').show();
				$('.fancybox-opened #btnSave').click(function(){
					validate("update");		
				});	
			}
    		}
    });
}


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	var CanBeDeleted=1;
	$.ajax({
		url: SiteURL+'data_collection/form_format/get_form_format_view/'+row.attr('data-id')+'/'+row.attr('data-second-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_view(AjaxData);
			
			/*
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			*/
		}
	});
	
};

$(document).ready(function()
{
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});


	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});

	$('.prev').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
                              $('[name=frmSearch]').submit();
               }
	});

	$('.next').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
                              $('[name=frmSearch]').submit();
               }
	});

	
	
	
	$('#tblView').on('click','.btnDelete',dlt);
	$('#tblView').on('click','.btnView',view);
	
	$('#btnAdd').on('click',add);
});
function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}






var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet_amount/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};




var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet_amount/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};

var getAdmissionID = function(TermID){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet_amount/get_admissionID/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};

var getPacketIDView = function(PacketID){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet_amount/get_fee_packet_view/'+$.trim(PacketID),
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FeePacketView)
			{
				for(var i in data.FeePacketView)
				{
					$('[name="ddlViewSchoolLevel"]').val( data.FeePacketView[i].SchoolLevelID);
					var schoolLevel= $('[name="ddlSchoolLevel"] option[value="'+data.FeePacketView[i].SchoolLevelID+'"]').text();
					$('#span_ddlSchoolLevel .combobox-label').text(schoolLevel);
					
					loadPretestTerm(data.FeePacketView[i].SchoolLevelID,"ddlViewAdmissionTerm");
					loadYearLevel(data.FeePacketView[i].SchoolLevelID,"ddlViewYearLevel");
					
					$('[name="ddlViewAdmissionTerm"]').val(data.FeePacketView[i].TermID);
					var termID= $('[name="ddlViewAdmissionTerm"] option[value="'+data.FeePacketView[i].TermID+'"]').text();
					$('#span_ddlViewAdmissionTerm .combobox-label').text(termID);

					$('[name="ddlViewYearLevel"]').val(data.FeePacketView[i].YearLevelID);
					var yearLevelID= $('[name="ddlViewYearLevel"] option[value="'+data.FeePacketView[i].YearLevelID+'"]').text();
					$('#span_ddlViewYearLevel .combobox-label').text(yearLevelID);
					getAdmissionID(data.FeePacketView[i].TermID);
				}
			}
        } 
	});
};


function validate() {
	if($('[name="ddlViewSchoolLevel"]').val()==""){
		alert("School Level Required");
	}
	else if($('[name="ddlViewAdmissionTerm"]').val()==""){
		alert("Admission Term Required");
	}
	else if($('[name="ddlViewYearLevel"]').val()==""){
		alert("Year Level Required");
	}
	else if($('[name="ddlViewPacketID"]').val()==""){
		alert("PacketID Required");
	}
	else if($('[name="ddlViewFeeType"]').val()==""){
		alert("Fee TypeRequired");
	}
	else if($.trim($('[name="txtViewAmount"]').val())==""){
		alert("Amount Required");
	}
	else if(validateNumeric($.trim($('[name="txtViewAmount"]').val()))==false){
		alert("Amount must be Numeric");
	}
	else if($('[name="txtViewDueDate"]').val()==""){
		alert("Due Date Required");
	}
	else if($.trim($('[name="txtViewTypeOfPayment"]').val())==""){
		alert("Type of Payment Required");
	}
	else{
		var Form=$('#frmAdd');
		var $inputs = $(':input');
		
		$inputs.each(function() {
			$(this).prop('disabled', false);
		});
		
		//$('[name="hfStatus"]').val('');
		
		$('#frmAdd').submit();
	}
};

var dlt=function(){

	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewPacketID"]').val(row.attr('data-id'));
	$('[name="hfViewFeeTypeID"]').val(row.attr('second-data-id'));

	$('[name="frmAdd"]').submit();
};


function popup_reset(){
		$('[name="txtViewAdmissionID"]').val("");
		$('[name="ddlViewSchoolLevel"]').val("");
					
		loadPretestTerm("","ddlViewAdmissionTerm");
		loadYearLevel("","ddlViewYearLevel");
		$('[name="frmAdd"] .combobox-label').text("--Please Choose--");		
		$('[name="ddlViewAdmissionTerm"]').val("");
		$('[name="ddlViewYearLevel"]').val("");
		$('[name="ddlViewPacketID"]').val("");
		$('[name="ddlViewFeeType"]').val("");
		$('[name="txtViewAmount"]').val("");
		$('[name="txtViewDueDate"]').val("");
		$('[name="txtViewTypeOfPayment"]').val("");
}

function popup_add(){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	popup_reset();
    		}
    });
}

var add= function(){
	$('[name="hfStatus"]').val('add');
	var row = $(this).parents('tr');
	$('[name="ddlViewPacketID"]').prop("disabled", false); 
	$('[name="ddlViewFeeType"]').prop("disabled", false); 
	popup_add();

	loadPretestTerm($('[name="ddlViewSchoolLevel"]').val(),"ddlViewAdmissionTerm");
	loadYearLevel($('[name="ddlViewSchoolLevel"]').val(),"ddlViewYearLevel");

	
	
	$('[name="ddlViewSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
		loadYearLevel($(this).val(),"ddlViewYearLevel");
	});
	
	$('[name="ddlViewPacketID"]').change(function(){
		getPacketIDView($(this).val());
	});

	$('[name="ddlViewAdmissionTerm"]').change(function(){
		getAdmissionID($(this).val());
	});	
};

function popup_view(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            beforeShow: function () {
            popup_reset();

			if(AjaxData.FeePacketAmountView){
				$('[name="ddlViewPacketID"]').prop("disabled", true); 
				$('[name="ddlViewFeeType"]').prop("disabled", true); 
				
				for(var i in AjaxData.FeePacketAmountView){
					$('[name="txtViewAdmissionID"]').val(AjaxData.FeePacketAmountView[i].AdmissionID);
					$('[name="txtViewAdmissionYear"]').val(AjaxData.FeePacketAmountView[i].AcademicYear);
					$('[name="txtViewAdmissionSemester"]').val(AjaxData.FeePacketAmountView[i].SmtID);
					$('[name="ddlViewSchoolLevel"]').val(AjaxData.FeePacketAmountView[i].SchoolLevelID);
					var schoolLevel= $('[name="ddlSchoolLevel"] option[value="'+AjaxData.FeePacketAmountView[i].SchoolLevelID+'"]').text();
					$('#span_ddlSchoolLevel .combobox-label').text(schoolLevel);

					loadPretestTerm(AjaxData.FeePacketAmountView[i].SchoolLevelID,"ddlViewAdmissionTerm");
					loadYearLevel(AjaxData.FeePacketAmountView[i].SchoolLevelID,"ddlViewYearLevel");
				
					$('[name="ddlViewAdmissionTerm"]').val(AjaxData.FeePacketAmountView[i].TermID);
					var termID= $('[name="ddlViewAdmissionTerm"] option[value="'+AjaxData.FeePacketAmountView[i].TermID+'"]').text();
					$('#span_ddlViewAdmissionTerm .combobox-label').text(termID);
					
					$('[name="ddlViewYearLevel"]').val(AjaxData.FeePacketAmountView[i].YearLevelID);
					var yearLevelID= $('[name="ddlViewYearLevel"] option[value="'+AjaxData.FeePacketAmountView[i].YearLevelID+'"]').text();
					$('#span_ddlViewYearLevel .combobox-label').text(yearLevelID);

					$('[name="ddlViewPacketID"]').val(AjaxData.FeePacketAmountView[i].PaketID);
					var packetID= $('[name="ddlViewPacketID"] option[value="'+AjaxData.FeePacketAmountView[i].PaketID+'"]').text();
					$('#span_ddlViewPacketID .combobox-label').text(packetID);
					
					$('[name="ddlViewFeeType"]').val(AjaxData.FeePacketAmountView[i].FeeTypeID);
					var feeType= $('[name="ddlViewFeeType"] option[value="'+AjaxData.FeePacketAmountView[i].FeeTypeID+'"]').text();
					$('#span_ddlViewFeeType .combobox-label').text(feeType);
					
					$('[name="txtViewAmount"]').val(AjaxData.FeePacketAmountView[i].Amount);
					$('[name="txtViewDueDate"]').val(AjaxData.FeePacketAmountView[i].TanggalJatuhTempo);
					$('[name="txtViewTypeOfPayment"]').val(AjaxData.FeePacketAmountView[i].TypeOfPayment);
				}



			
			} else{

			}
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			
			$('[name="ddlViewPacketID"]').change(function(){
				getPacketIDView($(this).val());
			});
	   }
    });		
}

var view = function(){
	$('[name="hfStatus"]').val('edit');
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/fee_packet_amount/get_fee_packet_amount_view/'+row.attr('data-id')+'/'+row.attr('second-data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			popup_view(AjaxData);
		}
	});
	
	
};

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlAdmissionTerm");
		loadYearLevel($(this).val(),"ddlYearLevel");
	});
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	
	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
        if($(this).hasClass("disable") == false){
             $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
             $('[name=frmSearch]').submit();
        }
	});
	$('.next').on('click',function(){
       if($(this).hasClass("disable") == false){
             $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
             $('[name=frmSearch]').submit();
       }
	});

	
	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnDelete',dlt);
	$('#btnAdd').on('click',add);
});
function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

var deleted = function(e){
	e.preventDefault();
	var ask = confirm('Are you sure to delete this record ?');
	if(!ask)return;
	
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('delete');
	$('[name="hfAdmissionID"]').val(row.attr('data-id'));
	$('[name="hfProcessID"]').val(row.attr('data-process'));
	$('[name="hfDocumentID"]').val(row.attr('data-document'));
	
	$('[name="frmAdd"]').submit();
};

var loadAdmissionID = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'data_collection/list_document/get_admission_id/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
        	if(typeof data.AdmissionID[0] != 'undefined'){
				$('.fancybox-opened [name="txtAdmissionID"]').val(data.AdmissionID[0].AdmissionID);
				$('.fancybox-opened [name="hfAdmissionID"]').val(data.AdmissionID[0].AdmissionID);
			}else{
				alert("School Level has no AdmissionID, Please choose another SchoolLevel");
				$('.fancybox-opened [name="txtAdmissionID"]').val("");
				$('.fancybox-opened [name="hfAdmissionID"]').val("");
			}
        } 
	});
};

function validate() {
	if($('.fancybox-opened [name="hfAdmissionID"]').val() == '')
		alert('Admission ID required');
	else if($('.fancybox-opened [name="ddlProcess"]').val() == '')
		alert('Admission Process required');
	else if($('.fancybox-opened [name="ddlDocument"]').val() == '')
		alert('Document required');
	else
	{
		$('.fancybox-opened [name="frmAdd"]').submit();
	}
}

function restartPage(){
	$('[name=hfPage]').val(1);
}

function Popup_add(){
		//Show FancyBox
		$.fancybox({
	        	href 		: '#internal-popup',
	       	    minWidth    : 640,
	            fitToView   : true,
	            closeClick  : false,
	        	afterShow: function () {
	        		$('[name=ddlSchoolLevel]').change(function(){
						loadAdmissionID($(this).val());
					});
	    		},
	    		beforeClose: function(){

	    		}
	    });
}


$(document).ready(function()
{
//	$('#btnSearch').click(function(){
//		$('[name=hfPage]').val('1');
//		$('[name=frmSearch]').submit();
//	});
	$('[name=hfLastYear]').val(0); 
	$('#show-popup-internal').click(function(e){
		e.preventDefault();
		Popup_add();
		//create_modal( 'id', '#internal-popup' );		

	});
		
	$('#btnSearch').on('click',function(){
		$('[name=hfLastYear]').val(0); 
		/*
		 *	1 : last year; 
		 *	0 : current year;
			*/
		$('[name=frmSearch]').submit();
	});
	
	$('#btnSearchLastYear').on('click',function(){
		$('[name=hfLastYear]').val(1);
		if($('[name="ddlSchoolLevel"]').val() == '')
			alert('School Level required');
		else if($('[name="ddlProcess"]').val() == '')
			alert('Admission Process required');
		else
		{
			/*
			 *	1 : last year; 
			 *	0 : current year;
			 */
			$('[name=frmSearch]').submit();
		}
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});
	//$('#tblView').on('click','.btnEdit',edit);
	$('#tblView').on('click','.btnDelete',deleted);
});
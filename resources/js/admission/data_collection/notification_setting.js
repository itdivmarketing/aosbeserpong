function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}







function create_modal( source_type, source ){
			$('.datepicker').datepicker('destroy');
			$('.timepicker').timepicker('destroy');
			$('.datetimepicker').datetimepicker('destroy');
			
			
	        var window_width = $(window).width();
	        var window_height = $(window).height();
	        $('body').css({ 'overflow' : 'hidden' }).prepend('<div id="modal"><div id="modal-content"></div><a href="#" id="modal-close" class="icon icon-close">Close</a></div><div id="modal-background"></div>');         

	        if( source_type == 'url' ){
	        	$('#modal-content').load( source, function(){
					modal_custom_styling();			     
	        	});
	        } else {
	        	$('#modal-content').html( $(source).clone().html() );
	        	/*$('#modal-content').append('<p>\
                <label for="">Select Dropdown (Custom - Powered by FancyFields)</label>\
                <span class="custom-select">\
                  <select name="" id="">\
                    <option value="">Ornare Fringilla Risus Venenatis Tellus</option>\
                    <option value="">Amet Aenean Cras Sem Risus</option>\
                    <option value="">Dapibus Cras Lorem Fusce Mollis</option>\
                  </select>\
                </span>\
              </p>');*/
	        	if ( $('#modal .custom-select').length > 0) {
	        		$('#modal .custom-select').fancyfields();
	        	}
	        	modal_custom_styling();
	        }
	    }

function modal_custom_styling(){
	var modal_width_adj = 0 - $('#modal').outerWidth() / 2;

	// Calculate modal's height
	var m_height = $("#modal").outerHeight();
	var w_height = $(window).height();
	
	if( m_height > ( w_height * 0.9 ) ){
		modal_style = {
			'top' : 20,
			'bottom' : 20
		};
	} else {
		modal_style = {
			'top' : '50%',
			'margin-top' : 0 - ( m_height / 2 )
		}
	}

	$('#modal').css({
		'margin-left' : modal_width_adj
	}).animate( modal_style, 200);

	// Custom Select
	/*if( $('#modal .custom-select').length > 0 ){
		$('#modal .custom-select').fancyfields();        	
	}*/

	// Custom Checkbox
	if( $('#modal .custom-checkbox').length > 0 ){
		$('#modal .custom-checkbox').fancyfields();        	
	}

	// Custom Radiobutton
	if( $('#modal .custom-radiobutton').length > 0 ){
		$('#modal .custom-radiobutton').fancyfields();        	
	}		        		        

	// Custom Scrollbar
	if( $('#modal .custom-scrollbar').length > 0 ){
		$('#modal .custom-scrollbar').mCustomScrollbar({
			autoHideScrollbar: true
		});
	}

	// Combo Checkbox
	$('#modal .combo-checkbox input[type="checkbox"]').fancyfields( 'bind', 'onCheckboxChange', function( input, isChecked ){
		combo_checkbox_changed( input );
	});

	$('#modal .combo-checkbox input[type="checkbox"]').change(function(){
		combo_checkbox_changed( $(this) );
	});

	// Datepicker
	if( $('#modal .datepicker').length > 0 ){
		$('#modal .datepicker').datepicker({ dateFormat: 'dd MM yy' });
	}
	
	if( $('#modal .datetimepicker').length > 0 ){
		$('#modal .datetimepicker').datetimepicker({ dateFormat: 'dd M yy' });  
   
     // 2014-05-13 00:00:00
	}
	
	if( $('#modal .timepicker').length > 0 ){
		$('#modal .timepicker').timepicker();
	}

	// Expand Control
	$('#modal .expand-control-toggle').fancyfields( 'bind', 'onCheckboxChange', function( input, isChecked ){
		if( isChecked ){
			input.closest('.expand-control-wrap').find('.expand-control').slideDown();
		} else {
			input.closest('.expand-control-wrap').find('.expand-control').slideUp();			
		}
	});

	$('#modal .expand-control-toggle').change(function(){
		if( $(this).is(':checked') ){
			$(this).closest('.expand-control-wrap').find('.expand-control').slideDown();
		} else {
			$(this).closest('.expand-control-wrap').find('.expand-control').slideUp();			
		}		
	});	   

	// Expand by Radio
	$('#modal .expand-by-radio-toggle').fancyfields( 'bind', 'onRadioChange', function( input ){
		var name = input.attr('name');
		var targets = $('#modal .expand-by-radio[data-name="'+name+'"]');

		targets.each(function(){
			var target = $(this);
			var req_value = target.attr('data-value');

			if( input.val() == req_value ) {
				target.slideDown();
			} else {
				target.slideUp();
			}			
		});
	});	    	
}

// CLOSE MODAL
function close_modal(){
		var window_height = $(window).height();
		
		$('#modal').animate({
				'top' : (0 - (window_height * 2))
		}, 400, function(){
				$('#modal').remove();
				$('#modal-background').fadeOut(function(){
						$(this).remove();
						$('body').css('overflow', 'auto');
						$('.binus-gallery-item').removeClass('active');
				});
		});
		
}

$('body').on('click', '#modal-background, #modal-close', function(){
		close_modal();          
		return false;
});

$(document).keyup(function(e){
		if ( e.keyCode == 27 && $('body #modal').length > 0){
				close_modal();
		}
});

function validate(method) {
/*
		if(method=='view'&&$.trim($('#modal [name="txtViewRegionID"]').val()) == '')
			alert('RegionID required');
		else if($.trim($('#modal [name="txtViewRegionName"]').val() )== '')
			alert('Region Name required');
		else
		{	
			$('#modal [name="frmAdd"]').submit();
		}
		*/
$('#modal [name="frmAdd"]').submit();
}




var add= function(){
	var row = $(this).parents('tr');
	$('[name="hfStatus"]').val('add');
	$('[name="hfRegionID"]').val('');
	create_modal( 'id', '#internal-popup' );
	$('#modal').show();
	$('#cke_txtViewEmailBody').remove();
	$('#modal #btnSave').click(function(){
		validate('add')
		}
	);		

};


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfNotificationSettingID"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
	
};

var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/notification_setting/view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {

        },
		complete:function(){
		
			}
	});
	
};

/*
var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/notification_setting/get_notification_setting_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
			create_modal( 'id', '#internal-popup' );
			$('#cke_txtViewEmailBody').remove();
			$('#modal').hide();
        },
		complete:function(){
		
			if(AjaxData.NotificationSettingView)
			{

				for(var i in AjaxData.NotificationSettingView)
				{
					$('[name="txtViewNotificationSettingID"]').val(AjaxData.NotificationSettingView[i].NotificationSettingID);
					$('[name="ddlViewNotificationType"]').val(AjaxData.NotificationSettingView[i].NotificationTypeID);
					$('[name="ddlViewSchoolLevel"]').val(AjaxData.NotificationSettingView[i].SchoolLevelID);
					$('[name="txtViewEmailTo"]').val(AjaxData.NotificationSettingView[i].EmailTo);
					$('[name="txtViewEmailCC"]').val(AjaxData.NotificationSettingView[i].EmailCC);
					$('[name="txtViewEmailBCC"]').val(AjaxData.NotificationSettingView[i].EmailBCC);
					$('[name="txtViewEmailSubject"]').val(AjaxData.NotificationSettingView[i].EmailSubject);
					$('[name="txtViewEmailBody"]').val(AjaxData.NotificationSettingView[i].EmailBody);
					$('[name="hfStatus"]').val('view');
				}
				$('#modal').show();
			} 
			else
			{
			
			}
			$('#modal #btnSave').click(function(){
				validate('view')
		});
			//$('#modal #btnSave').click(validate);
		}
	});
	
};
*/
$(document).ready(function()
{
	
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	
	
	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
$('.prev-nav').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
                              $('[name=frmSearch]').submit();
               }
});
$('.next-nav').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
                              $('[name=frmSearch]').submit();
               }
});

	

	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnDelete',dlt);
	$('#btnAdd').on('click',add);
});
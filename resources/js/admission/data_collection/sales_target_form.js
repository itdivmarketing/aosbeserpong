function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewProductionID"]').val(row.attr('data-id'));
	//alert($('[name="hfViewAdmissionID"]').val());
	$('[name="frmAdd"]').submit();
	
};





var getAdmissionID = function(SchoolLevelID){
	$.ajax({
		url: SiteURL+'data_collection/sales_target_form/get_admissionID/'+SchoolLevelID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};



function create_modal( source_type, source ){
			$('.datepicker').datepicker('destroy');
			$('.timepicker').timepicker('destroy');
			$('.datetimepicker').datetimepicker('destroy');
			
			
	        var window_width = $(window).width();
	        var window_height = $(window).height();
	        $('body').css({ 'overflow' : 'hidden' }).prepend('<div id="modal"><div id="modal-content"></div><a href="#" id="modal-close" class="icon icon-close">Close</a></div><div id="modal-background"></div>');         

	        if( source_type == 'url' ){
	        	$('#modal-content').load( source, function(){
					modal_custom_styling();			     
	        	});
	        } else {
	        	$('#modal-content').html( $(source).clone().html() );
	        	/*$('#modal-content').append('<p>\
                <label for="">Select Dropdown (Custom - Powered by FancyFields)</label>\
                <span class="custom-select">\
                  <select name="" id="">\
                    <option value="">Ornare Fringilla Risus Venenatis Tellus</option>\
                    <option value="">Amet Aenean Cras Sem Risus</option>\
                    <option value="">Dapibus Cras Lorem Fusce Mollis</option>\
                  </select>\
                </span>\
              </p>');*/
	        	if ( $('#modal .custom-select').length > 0) {
	        		$('#modal .custom-select').fancyfields();
	        	}
	        	modal_custom_styling();
	        }
	    }

function modal_custom_styling(){
	var modal_width_adj = 0 - $('#modal').outerWidth() / 2;

	// Calculate modal's height
	var m_height = $("#modal").outerHeight();
	var w_height = $(window).height();
	
	if( m_height > ( w_height * 0.9 ) ){
		modal_style = {
			'top' : 20,
			'bottom' : 20
		};
	} else {
		modal_style = {
			'top' : '50%',
			'margin-top' : 0 - ( m_height / 2 )
		}
	}

	$('#modal').css({
		'margin-left' : modal_width_adj
	}).animate( modal_style, 200);

	// Custom Select
	/*if( $('#modal .custom-select').length > 0 ){
		$('#modal .custom-select').fancyfields();        	
	}*/

	// Custom Checkbox
	if( $('#modal .custom-checkbox').length > 0 ){
		$('#modal .custom-checkbox').fancyfields();        	
	}

	// Custom Radiobutton
	if( $('#modal .custom-radiobutton').length > 0 ){
		$('#modal .custom-radiobutton').fancyfields();        	
	}		        		        

	// Custom Scrollbar
	if( $('#modal .custom-scrollbar').length > 0 ){
		$('#modal .custom-scrollbar').mCustomScrollbar({
			autoHideScrollbar: true
		});
	}

	// Combo Checkbox
	$('#modal .combo-checkbox input[type="checkbox"]').fancyfields( 'bind', 'onCheckboxChange', function( input, isChecked ){
		combo_checkbox_changed( input );
	});

	$('#modal .combo-checkbox input[type="checkbox"]').change(function(){
		combo_checkbox_changed( $(this) );
	});

	// Datepicker
	if( $('#modal .datepicker').length > 0 ){
		$('#modal .datepicker').datepicker({ dateFormat: 'dd M yy' });
	}
	
	if( $('#modal .datetimepicker').length > 0 ){
		$('#modal .datetimepicker').datetimepicker({ dateFormat: 'dd M yy' });  
   
     // 2014-05-13 00:00:00
	}
	
	if( $('#modal .timepicker').length > 0 ){
		$('#modal .timepicker').timepicker();
	}

	// Expand Control
	$('#modal .expand-control-toggle').fancyfields( 'bind', 'onCheckboxChange', function( input, isChecked ){
		if( isChecked ){
			input.closest('.expand-control-wrap').find('.expand-control').slideDown();
		} else {
			input.closest('.expand-control-wrap').find('.expand-control').slideUp();			
		}
	});

	$('#modal .expand-control-toggle').change(function(){
		if( $(this).is(':checked') ){
			$(this).closest('.expand-control-wrap').find('.expand-control').slideDown();
		} else {
			$(this).closest('.expand-control-wrap').find('.expand-control').slideUp();			
		}		
	});	   

	// Expand by Radio
	$('#modal .expand-by-radio-toggle').fancyfields( 'bind', 'onRadioChange', function( input ){
		var name = input.attr('name');
		var targets = $('#modal .expand-by-radio[data-name="'+name+'"]');

		targets.each(function(){
			var target = $(this);
			var req_value = target.attr('data-value');

			if( input.val() == req_value ) {
				target.slideDown();
			} else {
				target.slideUp();
			}			
		});
	});	    	
}

// CLOSE MODAL
function close_modal(){
		var window_height = $(window).height();
		
		$('#modal').animate({
				'top' : (0 - (window_height * 2))
		}, 400, function(){
				$('#modal').remove();
				$('#modal-background').fadeOut(function(){
						$(this).remove();
						$('body').css('overflow', 'auto');
						$('.binus-gallery-item').removeClass('active');
				});
		});
		
}

$('body').on('click', '#modal-background, #modal-close', function(){
		close_modal();          
		return false;
});

$(document).keyup(function(e){
		if ( e.keyCode == 27 && $('body #modal').length > 0){
				close_modal();
		}
});

var validate = function() {
	
	if($.trim($('#modal [name="txtViewAdmissionID"]').val()) == '')
			alert('AdmissionID required');
	else if($.trim($('#modal [name="txtViewAdmissionYear"]').val() )== '')
		alert('Admission Year is required');	
	else if($.trim($('#modal [name="txtViewAdmissionSemester"]').val() )== '')
		alert('Semester required');
	else if($.trim($('#modal [name="ddlViewSchoolLevel"]').val() )== '')
		alert('School Level required');
	else if($.trim($('#modal [name="txtViewProductionDate"]').val() )== '')
		alert('Production Date is required');		
	else if($.trim($('#modal [name="txtViewTotalForms"]').val() )== '')
		alert('total form is required');	
	else if(validateNumeric($.trim($('#modal [name="txtViewTotalForms"]').val() ))== false)
		alert('total form must be numeric');	
	else{
		$('[name="hfStatus"]').val('');
		$('[name="hfViewSchoolLevel"]').val($('[name="ddlViewSchoolLevel"]').val());
		$('#modal [name="frmAdd"]').submit();
	}	

}



var add= function(){
	var row = $(this).parents('tr');

	create_modal( 'id', '#internal-popup' );
	$('#modal').hide();
	$('[name="ddlViewSchoolLevel"]').prop("disabled",false);
	
	$('#modal').show();

	
	$('[name="ddlViewSchoolLevel"]').change(function(){
		getAdmissionID($(this).val());
	});
	$('#modal #btnSave').show();				
	$('#modal #btnSave').click(validate);		

};


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/sales_target_form/get_sales_target_form_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
			create_modal( 'id', '#internal-popup' );
			$('#modal').hide();
        },
		complete:function(){
		
			if(AjaxData.FormGenerateView)
			{
			
				var Form=$('#modal #frmAdd');

				var $inputs = $('#modal :input');
				$('[name="ddlViewSchoolLevel"]').prop("disabled",true);
				for(var i in AjaxData.FormGenerateView)
				{
					$('[name="txtViewAdmissionID"]').val(AjaxData.FormGenerateView[i].AdmissionID);
					$('[name="txtViewAdmissionYear"]').val(AjaxData.FormGenerateView[i].AcademicYear);
					$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormGenerateView[i].SmtID);
					$('[name="ddlViewSchoolLevel"]').val(AjaxData.FormGenerateView[i].SchoolLevelID);
					$('[name="txtViewProductionDate"]').val(AjaxData.FormGenerateView[i].ProductionDate);
					$('[name="txtViewTotalForms"]').val(AjaxData.FormGenerateView[i].TotalForms);
					$('[name="cbxViewIsDummy"]').prop("checked",AjaxData.FormGenerateView[i].IsDummy==1?true:false);
					
					$('[name="cbxViewIsOnline"]').prop("checked",AjaxData.FormGenerateView[i].IsOnline==1?true:false);
				}
				$('#modal').show();
			} 
			else
			{
				alert("failed");
			}
			//$('[name="txtViewAdmissionSemester"]').val($('[name="hfViewAdmissionSemester"]').val());
			$('#modal #btnSave').hide();	
			/*
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			*/
		}
	});
	
};

$(document).ready(function()
{
/*
	 $('#tblView').find('tr').each(function (i, el) {
			var $tds = $(this).find('td'),
            FormSold = $tds.eq(2).find('input[type=text]').val(),
			Intake = $tds.eq(3).find('input[type=text]').val();
			
        // do something with productId, product, Quantity
    });*/
	

	$('.txtNumber').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	
		
	$('#btnReset').click(function(){
		$('#txtTotalFormSold').val(0);
		$('#txtTotalIntake').val(0);
		$('.txtNumber').each(
			function(){
				$(this).val(0);
				
			}
		
		);
		
	});
	
	
	
	$('.txtFormSold').bind('input', function(){
		var TotalFormSold=0;
		$('.txtFormSold').each(
			function(){
				var FormSold=$.trim($(this).val());
				TotalFormSold+=parseInt(FormSold);
			}
		);
		$('#txtTotalFormSold').val(TotalFormSold);

	});
	
	$('.txtIntake').bind('input', function(){
		var TotalIntake=0;
		$('.txtIntake').each(
			function(){
				var Intake=$.trim($(this).val());
				TotalIntake+=parseInt(Intake);
			}
		);
		$('#txtTotalIntake').val(TotalIntake);

	});
		
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});
	
	$('#btnSave').on('click',function(e){
		e.preventDefault();
		var ErrorFlag=0;
		var ErrorMessage="";
		$('.txtNumber').each(
			function(){
				var Value=$.trim($(this).val());
				if(Value==""){
					ErrorMessage="Please Fill all the text field";
					ErrorFlag++;
				}
				else if(validateNumeric(Value)==false){
					ErrorMessage="Value Must Be Numeric";
					ErrorFlag++;
				}
				if(ErrorFlag!=0){
					alert(ErrorMessage);
					return false;
				}
			}
		);
		if(ErrorFlag==0){
			$('#frmSave').submit();
		}
	});
	


	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});
	
	$('.prev-nav').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next-nav').on('click',function(){
	   if($(this).hasClass("disable") == false)
	   {
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
	   }
	});

	
	
	
	$('#tblView').on('click','.btnDelete',dlt);
	$('#tblView').on('click','.btnView',view);
	
	$('#btnAdd').on('click',add);
});
function numeric(event) 
{
	
	// Backspace, tab, enter, end, home, left, right
	// We don't support the del key in Opera because del == . == 46.
	var controlKeys = [8, 9, 13, 35, 36, 37, 39];
	// IE doesn't support indexOf
	var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
	// Some browsers just don't raise events for control keys. Easy.
	// e.g. Safari backspace.
	if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
	  (48 <= event.which && event.which <= 57) || // Always 0 through 9
	  isControlKey) { // Opera assigns values for control keys.
	return;
	}else{
	event.preventDefault();
	}
}

function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}


var edit = function(){
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('update');
	$('[name="hfTermID"]').val(row.attr('data-id'));
	
	$.ajax({
		url: SiteURL+'data_collection/admission_term/get_admission_term_schedule/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			Popup_edit(data);
			
        },
		complete:function(){
			//$('#modal #btnSave').click(validate);
		}
	});
};

var deleted = function(e){
	e.preventDefault();
	var ask = confirm('Are you sure to delete this record ?');
	if(!ask)return;
	
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('delete');
	$('[name="hfTermID"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
};

function Popup_reset(){
	$('.fancybox-opened [name="hfTermID"]').val("");
	$('.fancybox-opened [name="txtTermID"]').val("");
	$('.fancybox-opened [name="txtTermName"]').val("");
	$('.fancybox-opened [name="txtTermOrder"]').val("");
	$('.fancybox-opened [name="txtOpeningDate"]').val("");
	$('.fancybox-opened [name="txtClosingDate"]').val("");
	$('.fancybox-opened [name="txtEntranceTest"]').val("");
	$('.fancybox-opened [name="txtAnnouncementDate"]').val("");
}

function Popup_add(){
		$('[name="hfAdmissionID"]').val($('[name="txtAdmissionID"]').val());
		$('[name="hfStatus"]').val('insert');
		$.fancybox({
	        	href 		: '#internal-popup',
	       	    minWidth    : 640,
	            fitToView   : true,
	            closeClick  : false,
	        	afterShow: function () {
	        		//Popup_rest();
	        		Popup_reset();

	        		
	        		
					//Set to default
					//$('[name="ddlViewBuilding"]').val('');
					//var txtSelected = $("[name='ddlViewBuilding'] option[value='']").text();
					//$('[name="frmAdd"] .combobox-label').text(txtSelected);
					//$('.fancybox-opened #btnSave').click(validate);
	    		},
	    		beforeClose: function(){
	    	
	    		}
	    });
	
}


function Popup_edit(data){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		if(data.AdmissionTerm)
				{
				$('.fancybox-opened [name="hfTermID"]').val(data.AdmissionTerm[0].TermID);
				$('.fancybox-opened [name="hfAdmissionID"]').val($('[name="txtAdmissionID"]').val());
				$('.fancybox-opened [name="txtTermID"]').val(data.AdmissionTerm[0].TermID);
				$('.fancybox-opened [name="txtTermName"]').val(data.AdmissionTerm[0].TermName);
				$('.fancybox-opened [name="txtTermOrder"]').val(data.AdmissionTerm[0].TermNumber);
				$('.fancybox-opened [name="txtOpeningDate"]').val(data.AdmissionTerm[0].OpeningDate);
				$('.fancybox-opened [name="txtClosingDate"]').val(data.AdmissionTerm[0].ClosingDate);
				$('.fancybox-opened [name="txtEntranceTest"]').val(data.AdmissionTerm[0].PreTestDate);
				$('.fancybox-opened [name="txtAnnouncementDate"]').val(data.AdmissionTerm[0].AnnouncementDate);
				$('.numeric').keypress(numeric);
			}
    		},
    		beforeClose: function(){
    			//Popup_rest();
    		}
   });
}


function validate() {
	if($('.fancybox-opened [name="txtTermName"]').val() == '')
		alert('Term Name required');
	else if($('.fancybox-opened [name="txtOpeningDate"]').val() == '')
		alert('Opening Date required');
	else if($('.fancybox-opened [name="txtTermOrder"]').val() == '')
		alert('Term Number required');
		else if($('.fancybox-opened [name="txtClosingDate"]').val() == '')
		alert('Closing Date required');
	else if($('.fancybox-opened [name="txtEntranceTest"]').val() == '')
		alert('Entrance Test required');
	else if($('.fancybox-opened [name="txtAnnouncementDate"]').val() == '')
		alert('Announcement Date required');
	else
	{
		var currDate = new Date();
		var startDate = new Date(Date.parse($('.fancybox-opened [name="txtOpeningDate"]').val()));
		var endDate = new Date(Date.parse($('.fancybox-opened [name="txtClosingDate"]').val()));
		var etTestDate = new Date(Date.parse($('.fancybox-opened [name="txtEntranceTest"]').val()));
		var announcementDate = new Date(Date.parse($('.fancybox-opened [name="txtAnnouncementDate"]').val()));
		
		// if(currDate > startDate)
		// 	alert('opening date cannot be in the past');
		// else if(currDate > endDate)
		// 	alert('closing date cannot be in the past');
		// else if(currDate > etTestDate)
		// 	alert('entrance test date cannot be in the past');
		// else if(currDate > announcementDate)
		// 	alert('announcement date cannot be in the past');
		if(startDate > endDate)
			alert('closing date must be greater than opening date');
		else if(endDate > etTestDate)
			alert('entrance test date must be greater than closing date');
		else if(etTestDate > announcementDate)
			alert('announcement date must be greater than entrance test date');
		else
			$('.fancybox-opened [name="frmAdd"]').submit();
	}
}

$(document).ready(function()
{
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});

	$('.numeric').keypress(numeric);
	$('#tblView').on('click','.btnEdit',edit);
	$('#tblView').on('click','.btnDelete',deleted);
});
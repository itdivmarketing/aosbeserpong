function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}



function validate() {

	//if(method=='view'&&$.trim($('#modal [name="txtViewRegionID"]').val()) == '')
	//	alert('RegionID required');
	if($.trim($('[name="ddlViewNotificationType"]').val() )== '')
		alert('Notification Type required');
	else if($.trim($('[name="ddlViewSchoolLevel"]').val() )== '')
		alert('school Level required');
	else if($.trim($('[name="txtViewEmailTo"]').val() )== '')
		alert('Email To required');
	else if($.trim($('[name="txtViewEmailSubject"]').val() )== '')
		alert('Email Subject required');
	else if($('[name=txtViewEmailBody]')== '')
		alert('Email Body  required');
	else
	{	
		$('[name="frmAdd"]').submit();
	}

}






$(document).ready(function()
{
	$('#btnSave').on('click',function(){
		validate();
	});
});
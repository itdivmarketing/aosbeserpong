function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewProductionID"]').val(row.attr('data-id'));
	//alert($('[name="hfViewAdmissionID"]').val());
	$('[name="frmAdd"]').submit();
	
};





var getAdmissionID = function(SchoolLevelID){
	$.ajax({
		url: SiteURL+'data_collection/form_generate/get_admissionID/'+SchoolLevelID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};

function Popup_reset(){
	$('[name="txtViewAdmissionID"]').val("");
	$('[name="ddlViewSchoolLevel"]').val("");
	$('[name="frmAdd"] .combobox-label').text("--Please Choose--");

	$('[name="txtViewProductionDate"]').val("");
	$('[name="txtViewTotalForms"]').val("");
	$('[name="cbxViewIsDummy"]').removeAttr("checked");
	$('[name="cbxViewIsOnline"]').removeAttr("checked");
}


function Popup_view(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	Popup_reset();
      			if(AjaxData.FormGenerateView){
					$('[name="ddlViewSchoolLevel"]').prop("disabled",true);
					for(var i in AjaxData.FormGenerateView){
					$('[name="txtViewAdmissionID"]').val(AjaxData.FormGenerateView[i].AdmissionID);
					$('[name="txtViewAdmissionYear"]').val(AjaxData.FormGenerateView[i].AcademicYear);
					$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormGenerateView[i].SmtID);
					$('[name="ddlViewSchoolLevel"]').val(AjaxData.FormGenerateView[i].SchoolLevelID);
					var txtSelected = $("[name='ddlViewSchoolLevel'] option[value="+AjaxData.FormGenerateView[i].SchoolLevelID+"]").text();
					$('.fancybox-opened [name="frmAdd"] .combobox-label').text(txtSelected);
					$('[name="txtViewProductionDate"]').val(AjaxData.FormGenerateView[i].ProductionDate);
					$('[name="txtViewTotalForms"]').val(AjaxData.FormGenerateView[i].TotalForms);
					$('[name="cbxViewIsDummy"]').prop("checked",AjaxData.FormGenerateView[i].IsDummy==1?true:false);
					$('[name="cbxViewIsOnline"]').prop("checked",AjaxData.FormGenerateView[i].IsOnline==1?true:false);
					}
				} else{
					alert("failed");
				}
			//$('[name="txtViewAdmissionSemester"]').val($('[name="hfViewAdmissionSemester"]').val());
				$('.fancybox-opened #btnSave').hide();
    		}
    });
}


function Popup_add(){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            		Popup_reset();
      				$('[name="ddlViewSchoolLevel"]').prop("disabled",false);
					$('[name="ddlViewSchoolLevel"]').change(function(){
							getAdmissionID($(this).val());
					});
					$('.fancybox-opened #btnSave').show();	
    		}
    });
}

var validate = function() {
	
	if($.trim($('.fancybox-opened [name="txtViewAdmissionID"]').val()) == '')
			alert('AdmissionID required');
	else if($.trim($('.fancybox-opened [name="txtViewAdmissionYear"]').val() )== '')
		alert('Admission Year is required');	
	else if($.trim($('.fancybox-opened [name="txtViewAdmissionSemester"]').val() )== '')
		alert('Semester required');
	else if($.trim($('.fancybox-opened [name="ddlViewSchoolLevel"]').val() )== '')
		alert('School Level required');
	else if($.trim($('.fancybox-opened [name="txtViewProductionDate"]').val() )== '')
		alert('Production Date is required');		
	else if($.trim($('.fancybox-opened [name="txtViewTotalForms"]').val() )== '')
		alert('total form is required');	
	else if(validateNumeric($.trim($('.fancybox-opened [name="txtViewTotalForms"]').val() ))== false)
		alert('total form must be numeric');	
	else{
		$('[name="hfStatus"]').val('');
		$('[name="hfViewSchoolLevel"]').val($('[name="ddlViewSchoolLevel"]').val());
		$('.fancybox-opened [name="frmAdd"]').submit();
	}	
}



var add= function(){
	var row = $(this).parents('tr');
	Popup_add();
			
};


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/form_generate/get_form_generate_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_view(AjaxData);
			/*
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			*/
		}
	});
	
};

$(document).ready(function(){

	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});

	$('.prev-').on('click',function(){
        if($(this).hasClass("disable") == false){
           $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
           $('[name=frmSearch]').submit();
        }
	});

	$('.next').on('click',function(){
         if($(this).hasClass("disable") == false){
            $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
            $('[name=frmSearch]').submit();
         }
});

	
	
	
	$('#tblView').on('click','.btnDelete',dlt);
	$('#tblView').on('click','.btnView',view);
	$('#btnAdd').on('click',add);
});
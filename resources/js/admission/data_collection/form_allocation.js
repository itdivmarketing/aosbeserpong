function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewProductionID"]').val(row.attr('data-id'));
	//alert($('[name="hfViewAdmissionID"]').val());
	$('[name="frmAdd"]').submit();
	
};





var getAdmissionID = function(SchoolLevelID){
	$.ajax({
		url: SiteURL+'data_collection/form_allocation/get_admissionID/'+SchoolLevelID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			var AdmissionID="";
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
					var AdmissionID=data.AdmissionID[i].AdmissionID;
				}
			}
			
			getFormNo(AdmissionID);
        }
		
	});
};

var getFormNo = function(AdmissionID){
	var ObjectName="ddlFormNo";
	$.ajax({
		url: SiteURL+'data_collection/form_allocation/get_form_no/'+AdmissionID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
       	contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('.'+ObjectName+'').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.FormNo)
			{
				for(var i in data.FormNo)
				{
					$('.'+ObjectName+'').append($('<option value="'+$.trim(data.FormNo[i].FormNo)+'">'+data.FormNo[i].FormNo+'</option>'));
				}
			}
			$('.'+ObjectName+'').trigger("change");
        } 
	});
};

function Popup_reset(){
	$('.fancybox-opened [name="txtViewAdmissionID"]').val("");
	$('.fancybox-opened [name="ddlViewSchoolLevel"]').val("");
	$('.fancybox-opened [name="ddlViewFrom"]').val("");
	$('.fancybox-opened [name="ddlViewTo"]').val("");
	$('.fancybox-opened [name="ddlViewLocation"]').val("");
	$('[name="frmAdd"] .combobox-label').text("--Please Choose--");
}

function Popup_distribution(){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            		Popup_reset();

    		}
    });
}

function validate() {
	if($.trim($('.fancybox-opened [name="txtViewAdmissionID"]').val()) == '')
			alert('AdmissionID required');
	else if($.trim($('.fancybox-opened [name="txtViewAdmissionYear"]').val() )== '')
		alert('Admission Year is required');	
	else if($.trim($('.fancybox-opened [name="txtViewAdmissionSemester"]').val() )== '')
		alert('Semester required');
	else if($.trim($('.fancybox-opened [name="ddlViewSchoolLevel"]').val() )== '')
		alert('School Level required');
	else if($.trim($('.fancybox-opened [name="ddlViewFrom"]').val() )== '')
		alert('Form From  is required');		
	else if($.trim($('.fancybox-opened [name="ddlViewTo"]').val() )== '')
		alert('Form To is required');	
	else if($.trim($('.fancybox-opened [name="ddlViewFrom"]').val())>$.trim($('.fancybox-opened [name="ddlViewTo"]').val()) )
		alert('Form From Can not bigger than Form To');	
	else if($.trim($('.fancybox-opened [name="ddlViewLocation"]').val() )== '')
		alert('Location  is required');
	else{
		$('[name="hfStatus"]').val('');
		$('[name="hfViewSchoolLevel"]').val($('[name="ddlViewSchoolLevel"]').val());
		$('.fancybox-opened [name="frmAdd"]').submit();
	}	
	
}



var add= function(){
	var row = $(this).parents('tr');

	Popup_distribution();
	$('[name="ddlViewSchoolLevel"]').prop("disabled",false);
	
	$('[name="ddlViewSchoolLevel"]').change(function(){
		getAdmissionID($(this).val());
	});
	// $('#modal #btnSave').show();				
	// $('#modal #btnSave').click(validate);		

};


$(document).ready(function(){

	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});

	$('.prev').on('click',function(){
       if($(this).hasClass("disable") == false){
           $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
           $('[name=frmSearch]').submit();
       }
	});
	$('.next').on('click',function(){
        if($(this).hasClass("disable") == false){
           $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
           $('[name=frmSearch]').submit();
        }
	});

	
	
	
	// $('#tblView').on('click','.btnDelete',dlt);
	// $('#tblView').on('click','.btnView',view);
	
	$('#btnAdd').on('click',add);
});
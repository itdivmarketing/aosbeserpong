function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validate() {
		method = $('[name="hfStatus"]').val();
		if($.trim($('.fancybox-opened [name="ddlViewVenue"]').val()) == '')
			alert('VenueID required');
		else if($.trim($('.fancybox-opened [name="txtViewCapacity"]').val() )== '')
			alert('Capacity required');
		else if(validateNumeric( $.trim($('.fancybox-opened [name="txtViewCapacity"]').val() ))== false)
			alert('Capacity must be numeric');
		else
		{	
			if(method=="add")
				$('[name="hfVenueID"]').val($('.fancybox-opened [name="ddlViewVenue"]').val());
				
			$('.fancybox-opened [name="frmAdd"]').submit();
		}
}

function Popup_reset(){
	$('[name="ddlViewVenue"]').prop("disabled",false); 
	$('[name="ddlViewVenue"]').val('');
	$('.fancybox-opened #span_ddlViewVenue .combobox-label').text("--Please Choose--");
	$('[name="txtViewCapacity"]').val('');

}

function Popup_add(){
	//Show FancyBox
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		Popup_reset();
    		},
    		afterClose: function(){

    		}
    });
}

var add= function(){
	var row = $(this).parents('tr');
	$('[name="hfStatus"]').val('add');
	$('[name="hfVenueID"]').val('');
	Popup_add();
};

var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfVenueID"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
	
};

function Popup_edit(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		if(AjaxData.RoomTestView){
					for(var i in AjaxData.RoomTestView){
					$('[name="ddlViewVenue"]').val(AjaxData.RoomTestView[i].VenueID);
					var txtSelected = $(".fancybox-opened [name='ddlViewVenue'] option[value="+AjaxData.RoomTestView[i].VenueID+"]").text();
					$('[name="frmAdd"] .combobox-label').text(txtSelected);
					$('[name="txtViewCapacity"]').val(AjaxData.RoomTestView[i].Capacity);
					$('[name="hfStatus"]').val('view');
					$('[name="hfVenueID"]').val(AjaxData.RoomTestView[i].VenueID);
					}
				}
				$('[name="ddlViewVenue"]').prop("disabled",true); 
    		},
    		beforeClose: function(){
    			//popup_rest();
    		}
   });
}

var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/room_test_capacity/get_room_test_capacity_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_edit(AjaxData);
			//$('#modal #btnSave').click(validate);
		}
	});
	
};

$(document).ready(function()
{
	
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	
	
	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});

	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnDelete',dlt);
	$('#btnAdd').on('click',add);
});
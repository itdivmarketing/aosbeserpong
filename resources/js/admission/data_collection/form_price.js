function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewSchoolLevel"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
	
};





var getAdmissionID = function(SchoolLevelID){
	$.ajax({
		url: SiteURL+'data_collection/form_price/get_admissionID/'+SchoolLevelID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};



function validate() {
	
	if($.trim($('.fancybox-opened  [name="txtViewAdmissionID"]').val()) == '')
			alert('AdmissionID required');
	else if($.trim($('.fancybox-opened  [name="txtViewAdmissionYear"]').val() )== '')
		alert('Admission Year is required');	
	else if($.trim($('.fancybox-opened  [name="txtViewAdmissionSemester"]').val() )== '')
		alert('Semester required');
	else if($.trim($('.fancybox-opened  [name="ddlViewSchoolLevel"]').val() )== '')
		alert('School Level required');
	else if($.trim($('.fancybox-opened  [name="txtViewApplicationFee"]').val() )== '')
		alert('Application Fee is required');
	else if(validateNumeric($.trim($('.fancybox-opened [name="txtViewApplicationFee"]').val() ))==false)
		alert('Application Fee must be numeric');
	else{
		$('[name="hfStatus"]').val('');
		$('[name="hfViewSchoolLevel"]').val($('[name="ddlViewSchoolLevel"]').val());
		$('.fancybox-opened [name="frmAdd"]').submit();
	}	

}

function Popup_reset(){
	$('[name="txtViewAdmissionID"]').val("");
	$('[name="ddlViewSchoolLevel"]').val("");
	$('.fancybox-opened [name="frmAdd"] .combobox-label').text("--Please Choose--");
	$('[name="txtViewApplicationFee"]').val("");
	$('[name="hfViewApplicationFee"]').val("");
}


function Popup_add(){
	//Show FancyBox
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		Popup_reset();

        		$('[name="ddlViewSchoolLevel"]').prop("disabled",false);
        		$('.fancybox-opened [name="hfStatus"]').val('add');
				$('.fancybox-opened [name="hfVenueID"]').val('');
				
				$('[name="ddlViewSchoolLevel"]').change(function(){
					getAdmissionID($(this).val());
				});

    		},
    		afterClose: function(){
    		}
    });
}

var add= function(){
	Popup_add();
};



function Popup_view(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
        	afterShow: function () {
        		if(AjaxData.FormPriceView){
					$('[name="ddlViewSchoolLevel"]').prop("disabled",true);
					for(var i in AjaxData.FormPriceView){
						$('[name="txtViewAdmissionID"]').val(AjaxData.FormPriceView[i].AdmissionID);
						$('[name="txtViewAdmissionYear"]').val(AjaxData.FormPriceView[i].AcademicYear);
						$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormPriceView[i].SmtID);
						$('[name="ddlViewSchoolLevel"]').val(AjaxData.FormPriceView[i].SchoolLevelID);
						var txtSelected = $(".fancybox-opened [name='ddlViewSchoolLevel'] option[value="+AjaxData.FormPriceView[i].SchoolLevelID+"]").text();
						$('.fancybox-opened [name="frmAdd"] .combobox-label').text(txtSelected);
						$('[name="txtViewApplicationFee"]').val(AjaxData.FormPriceView[i].AmountValue);
						$('[name="hfViewApplicationFee"]').val(AjaxData.FormPriceView[i].AmountValue);
					}
				} 
    		},
    		afterClose: function(){

    		}
    });
}

var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/form_price/get_form_price_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_view(AjaxData);
		}
	});
	
};


$(document).ready(function()
{	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});


	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
        if($(this).hasClass("disable") == false){
            $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
            $('[name=frmSearch]').submit();
        }
	});
	$('.next').on('click',function(){
        if($(this).hasClass("disable") == false) {
            $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
            $('[name=frmSearch]').submit();
        }
});

	
	
	
	$('#tblView').on('click','.btnDelete',dlt);
	$('#tblView').on('click','.btnView',view);
	
	$('#btnAdd').on('click',add);
});
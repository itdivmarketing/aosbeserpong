function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}

function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validate() {
		var method = $('[name="hfStatus"]').val();
		if(method=='view'&& $.trim($('.fancybox-opened [name="txtViewVenueID"]').val()) == '')
			alert('VenueID required');
		else if($.trim($('.fancybox-opened [name="ddlViewBuilding"]').val() )== '')
			alert('Building required');	
		else if($.trim($('.fancybox-opened [name="txtViewVenueName"]').val() )== '')
			alert('Venue Name required');
		else
		{	
			$('.fancybox-opened [name="frmAdd"]').submit();
		}
}

// function OnchangeBuilding(){
// 	var selected = $('.fancybox-opened [name="ddlViewBuilding"]').val();;
// 	var txtSelected = $(".fancybox-opened [name='ddlViewBuilding'] option[value='"+selected+"']").text();
// 	$('.fancybox-opened [name="frmAdd"] .combobox-label').text(txtSelected);
// }

function Popup_reset(){
	$('.fancybox-opened [name="txtViewVenueID"]').val("");
	$('.fancybox-opened [name="txtViewVenueName"]').val("");
	$('.fancybox-opened [name="ddlViewSchoolLevel"]').val('');
	$('.fancybox-opened #span_ddlViewBuilding .combobox-label').text("--Please Choose--");
}

function Popup_add(){
	//Show FancyBox
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		Popup_reset();
        		$('.fancybox-opened [name="hfStatus"]').val('add');
				$('.fancybox-opened [name="hfVenueID"]').val('');

    		},
    		afterClose: function(){

    		}
    });
}

function Popup_view(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
        	afterShow: function () {
        		if(AjaxData.RoomTestView){
					for(var i in AjaxData.RoomTestView){
					$('.fancybox-opened [name="ddlViewBuilding"]').val(AjaxData.RoomTestView[i].BuildingID);

					//alert($('[name="ddlViewBuilding"] option:selected').text());
					//$('.combobox-label').text("xzczxc");
					var txtSelected = $(".fancybox-opened [name='ddlViewBuilding'] option[value="+AjaxData.RoomTestView[i].BuildingID+"]").text();
					$('.fancybox-opened [name="frmAdd"] .combobox-label').text(txtSelected);

					$('.fancybox-opened [name="txtViewVenueID"]').val(AjaxData.RoomTestView[i].VenueID);
					$('.fancybox-opened [name="txtViewVenueName"]').val(AjaxData.RoomTestView[i].VenueName);

					$('.fancybox-opened [name="hfStatus"]').val('view');
					$('.fancybox-opened [name="hfVenueID"]').val(AjaxData.RoomTestView[i].VenueID);
					}
				}
    		},
    		afterClose: function(){

    		}
    });
}




var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');  
	$('[name="hfVenueID"]').val(row.attr('data-id'));
	$('[name="frmAdd"]').submit();
};


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/room_test/get_room_test_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_view(AjaxData);
			//$('#modal #btnSave').click(validate);
		}
	});
	
};

$(document).ready(function()
{
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});

	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});

	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnDelete',dlt);
});
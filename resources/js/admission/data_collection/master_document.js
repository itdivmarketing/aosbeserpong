function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

var edit = function(){
	var row = $(this).parents('tr');
	
	
	
	$.ajax({
		url: SiteURL+'data_collection/master_document/get_master_document/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			Popup_edit(data);
			$('[name="hfStatus"]').val('update');
			$('[name="hfDocumentID"]').val(row.attr('data-id'));
        },
		complete:function(){
			
		}
	});
};

var deleted = function(e){
	e.preventDefault();
	var ask = confirm('Are you sure to delete this record ?');
	if(!ask)return;
	
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('delete');
	$('[name="hfDocumentID"]').val(row.attr('data-id'));
	$('[name="frmAdd"]').submit();
};

function validate () {
	if($('.fancybox-opened [name="txtDocumentName"]').val() == '')
		alert('Document Name required');
	else
	{
		$('.fancybox-opened [name="frmAdd"]').submit();
	}
}

function popup_reset(){
	$('.fancybox-opened [name="txtDocumentID"]').val("");
	$('.fancybox-opened [name="txtDocumentName"]').val("")
	$('[name="hfDocumentID"]').val('');
}

function Popup_edit(data){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		popup_reset();
        		if(data.Document){
					$('.fancybox-opened [name="hfDocumentID"]').val(data.Document[0].DocumentID);
					$('.fancybox-opened [name="txtDocumentID"]').val(data.Document[0].DocumentID);
					$('.fancybox-opened [name="txtDocumentName"]').val(data.Document[0].DocumentName);
				}
    		},
    		beforeClose: function(){
    			
    		}
   });
}

function Popup_add(){
	//Show FancyBox
		$.fancybox({
	        	href 		: '#internal-popup',
	       	    minWidth    : 640,
	            fitToView   : true,
	            closeClick  : false,
	        	afterShow: function () {
	        		popup_reset();
	        		//$('[name="hfStatus"]').val('insert');
					//Set to default
					//$('[name="ddlViewBuilding"]').val('');
					//var txtSelected = $("[name='ddlViewBuilding'] option[value='']").text();
					//$('[name="frmAdd"] .combobox-label').text(txtSelected);
					//$('.fancybox-opened #btnSave').click(validate);
	    		},
	    		beforeClose: function(){
	    
	    		}
	    });
}

$(document).ready(function()
{
	$('#btnSearch').click(function(){
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});
	
	$('#show-popup-internal').click(function(e){
	    	e.preventDefault();
	    	$('[name="hfStatus"]').val('insert');
			$('[name="hfDocumentID"]').val('');
			Popup_add();
	    });

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});
	
	$('#tblView').on('click','.btnEdit',edit);
	$('#tblView').on('click','.btnDelete',deleted);
});
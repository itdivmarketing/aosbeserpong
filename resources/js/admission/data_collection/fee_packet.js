function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}






var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};


var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};




var getAdmissionID = function(TermID){
	$.ajax({
		url: SiteURL+'data_collection/fee_packet/get_admissionID/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};




function validate() {
	if($('[name="ddlViewSchoolLevel"]').val()==""){
		alert("School Level Required");
	}
	else if($('[name="ddlViewAdmissionTerm"]').val()==""){
		alert("Admission Term Required");
	}
	else if($('[name="ddlViewYearLevel"]').val()==""){
		alert("Year Level Required");
	}
	else if($('[name="txtViewPacketName"]').val()==""){
		alert("Packet Name Required");
	}
	else{
		$('[name="hfStatus"]').val('');
		$('.fancybox-opened [name="frmAdd"]').submit();
	}
};

var dlt=function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');
	if(!conf)
		return false;	
	$('[name="hfStatus"]').val('delete');
	$('[name="hfViewPacketID"]').val(row.attr('data-id'));
	$('[name="frmAdd"]').submit();
};


function Popup_reset(){
	$('[name="txtViewAdmissionID"]').val("");
	$('[name="ddlViewSchoolLevel"]').val("");
	loadPretestTerm("","ddlViewAdmissionTerm");
	loadYearLevel("","ddlViewYearLevel");
	$('.fancybox-opened [name="frmAdd"] .combobox-label').text("--Please Choose--");
	$('[name="ddlViewAdmissionTerm"]').val("");
	$('[name="ddlViewYearLevel"]').val("");
	$('[name="txtViewPacketID"]').val("");
	$('[name="txtViewPacketName"]').val("");

}

function Popup_add(){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	Popup_reset();
            	loadPretestTerm($('[name="ddlViewSchoolLevel"]').val(),"ddlViewAdmissionTerm");
				loadYearLevel($('[name="ddlViewSchoolLevel"]').val(),"ddlViewYearLevel");
				$('[name="ddlViewSchoolLevel"]').change(function(){
						loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
						loadYearLevel($(this).val(),"ddlViewYearLevel");
				});
	
				$('[name="ddlViewAdmissionTerm"]').change(function(){
						getAdmissionID($(this).val());
				});	
    		}
    });
}


var add= function(){
	var row = $(this).parents('tr');
	Popup_add();
	// create_modal( 'id', '#internal-popup' );
	// $('#modal').hide();
};


function Popup_view(AjaxData){
	console.log(AjaxData);
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {			
				if(AjaxData.FeePacketView){
			
				// var Form=$('#modal #frmAdd');

				// var $inputs = $('#modal :input');
				
				// $inputs.each(function() {
				// 	$(this).prop('readonly', true);
				// });
			
				// $('[name="ddlViewSchoolLevel"]').prop("disabled", true); 
				// $('[name="ddlViewAdmissionTerm"]').prop("disabled", true); 
				// $('[name="ddlViewYearLevel"]').prop("disabled", true); 

					for(var i in AjaxData.FeePacketView){
						console.log(AjaxData.FeePacketView[i].AdmissionID);
						$('[name="txtViewAdmissionID"]').val(AjaxData.FeePacketView[i].AdmissionID);
						$('[name="txtViewAdmissionYear"]').val(AjaxData.FeePacketView[i].AcademicYear);
						$('[name="txtViewAdmissionSemester"]').val(AjaxData.FeePacketView[i].SmtID);
						$('[name="ddlViewSchoolLevel"]').val(AjaxData.FeePacketView[i].SchoolLevelID);
						var schoolLevelID = $("[name='ddlViewSchoolLevel'] option[value="+AjaxData.FeePacketView[i].SchoolLevelID+"]").text();
						$('[name="frmAdd"] #span_ddlViewSchoolLevel .combobox-label').text(schoolLevelID);

						loadPretestTerm(AjaxData.FeePacketView[i].SchoolLevelID,"ddlViewAdmissionTerm");
						loadYearLevel(AjaxData.FeePacketView[i].SchoolLevelID,"ddlViewYearLevel");

						$('[name="ddlViewAdmissionTerm"]').val(AjaxData.FeePacketView[i].TermID);
						var term = $("[name='ddlViewAdmissionTerm'] option[value="+AjaxData.FeePacketView[i].TermID+"]").text();
						$('[name="frmAdd"] #span_ddlViewAdmissionTerm .combobox-label').text(term);

						$('[name="ddlViewYearLevel"]').val(AjaxData.FeePacketView[i].YearLevelID);
						var year = $("[name='ddlViewYearLevel'] option[value="+AjaxData.FeePacketView[i].YearLevelID+"]").text();
						$('[name="frmAdd"] #span_ddlViewYearLevel .combobox-label').text(year);
						
						$('[name="txtViewPacketID"]').val(AjaxData.FeePacketView[i].PaketID);
						$('[name="txtViewPacketName"]').val(AjaxData.FeePacketView[i].NmPaket);
					}
				}else{

				}

				$('[name="ddlViewSchoolLevel"]').change(function(){
					loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				    loadYearLevel($(this).val(),"ddlViewYearLevel");
				});

    		}
    });
}

var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/fee_packet/get_fee_packet_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			Popup_view(AjaxData);
			// $('#modal #btnSave').click(validate);	
		}
	});
};

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlAdmissionTerm");
		loadYearLevel($(this).val(),"ddlYearLevel");
	});
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	
	$('.prev').on('click',function(){
        if($(this).hasClass("disable") == false){
            $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
            $('[name=frmSearch]').submit();
         }
	});

	$('.next').on('click',function(){
        if($(this).hasClass("disable") == false){
            $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
            $('[name=frmSearch]').submit();
        }
	});

	
	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnDelete',dlt);
	
	$('#btnAdd').on('click',add);
});
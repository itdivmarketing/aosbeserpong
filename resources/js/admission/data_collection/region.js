function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validate() {
		if($('.fancybox-opened [name="hfStatus"]').val() =='view'&&$.trim($('.fancybox-opened [name="txtViewRegionID"]').val()) == '')
			alert('RegionID required');
		else if($.trim($('.fancybox-opened [name="txtViewRegionName"]').val() )== '')
			alert('Region Name required');
		else
		{	
			checkRegionName();
			//$('.fancybox-opened [name="frmAdd"]').submit();
		}
}


function checkRegionName(){
	Region_name = $.trim($('.fancybox-opened [name="txtViewRegionName"]').val());
	$.ajax({
		url: SiteURL+'data_collection/region/check_RegionName/'+Region_name,
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			if(data!=null){
				alert("Region data with this name already exists [RegionName : "+ data.RegionName+"]\nPlease insert with other Region Name");
    		} else {
        		$('.fancybox-opened [name="frmAdd"]').submit();
			}
        },
		complete:function(){
			//location.reload();
		}
	});

}

function Popup_add(){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	Popup_reset();
    		}
    });
}


function Popup_reset(){
	$('[name="txtViewRegionID"]').val("");
	$('[name="txtViewRegionName"]').val("");
	$('[name="hfRegionID"]').val("");
}

function Popup_view(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	Popup_reset();
            	if(AjaxData.RegionView){
					for(var i in AjaxData.RegionView){
						$('[name="txtViewRegionID"]').val(AjaxData.RegionView[i].RegionID);
						$('[name="txtViewRegionName"]').val(AjaxData.RegionView[i].RegionName);
						$('[name="hfStatus"]').val('view');
						$('[name="hfRegionID"]').val(AjaxData.RegionView[i].RegionID);
					}
				}else{
			
				}
    		}
    });
}


var add= function(){
	var row = $(this).parents('tr');
	$('[name="hfStatus"]').val('add');
	$('[name="hfRegionID"]').val('');
	Popup_add();	
};


var dlt = function(){
	var conf=confirm("are you sure want to delete this data?");
	var row = $(this).parents('tr');

	if(!conf)
		return false;
		
	$('[name="hfStatus"]').val('delete');
	$('[name="hfRegionID"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
	
};


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'data_collection/region/get_region_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;;
        },
		complete:function(){
			Popup_view(AjaxData);
			//$('#modal #btnSave').click(validate);
		}
	});
	
};

$(document).ready(function()
{
	
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});

	
	
	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
$('.prev').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
                              $('[name=frmSearch]').submit();
               }
});
$('.next').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
                              $('[name=frmSearch]').submit();
               }
});

	

	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnDelete',dlt);
	$('#btnAdd').on('click',add);
});
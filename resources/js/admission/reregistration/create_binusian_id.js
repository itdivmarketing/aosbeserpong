var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'reregistration/create_binusian_id/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};

var validate = function(e){
	if($('[name="ddlAcademicYear"]').val() == '')
		alert('Academic Year is required');
	else if($('[name="ddlAcademicSemester"]').val() == '')
		alert('Academic Year is required');
	else if($('#tblRegistrant :checkbox:checked').length <= 0)
		alert('Registrant is required');
	else {
		// var internalpopup_default = $("#internal-popup").html();
		// $("#internal-popup").html(internalpopup_default);
		// e.preventDefault();
		// $.fancybox({
  //       	href 		: '#internal-popup',
  //      	    minWidth    : 640,
  //           fitToView   : true,
  //           closeClick  : false,
  //           live:false,
  //           afterShow: function () {
            	
  //           }
  //   	});

		// $("#btnCreate").click(function(e){
		// 	e.preventDefault();
		// 	if(document.getElementById('agree').checked){
				$('[name="frmSet"]').submit();
		// 	}
		// 	else{
		// 		alert("Please agree with school regulation");
		// 	}
		// })

	}
	/*else
		$('[name="frmSet"]').submit();*/
}
	


$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
	});
	$('.date').datepicker({ dateFormat: 'd-M-yy' }).datepicker('setDate',new Date());

	$('#btnSave').click(validate);
});
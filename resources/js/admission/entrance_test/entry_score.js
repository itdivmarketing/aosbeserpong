function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+(\.\d+)?$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	 }
}

var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
        } 
	});
};


var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
        } 
	});
};

function ValidateUD() {


		/*if($.trim($('[name="hfSchoolLevelID"]').val() ) == $('[name="ddlUDSchoolLevel"]').val() )
			alert('School Level can not be the same');
		else*/ if($('[name="hfTermID"]').val() == $('[name="ddlUDAdmissionTerm"]').val() )
			alert('Admission Term can not be the same');
		else if($.trim($('[name="ddlUDSchoolLevel"]').val() )== '')
			alert('School Level is required');
		else if($('[name="ddlUDAdmissionTerm"]').val() == '')
			alert('AdmissionTerm is required');
		else if($('[name="ddlUDYearLevel"]').val() == '')
			alert('Year Level is required');
		else if($('#formNoTxt').val() == ''){
			alert('Please input form no. dummy');
		}else{	
			  $('#form-UD').find('input:text').each(function(){
					$(this).val($.trim($(this).val()));
				});

			$('#form-UD').submit();
		}


}


function ValidateInterview() {

		if($.trim($('[name="txtP3AScore"]').val() )== '')
			alert('Score is required');
		else if(validateNumeric($.trim($('[name="txtP3AScore"]').val() ))==false)
				alert('Invalid Score Entry');
		else if($('[name="ddlP3AResultET"]').val() == '')
			alert('Result Entrance Test is required');
		else
		{	
			  $('#form-interview').find('input:text').each(function(){
					$(this).val($.trim($(this).val()));
				});

			$('#form-interview').submit();
		}

}

function ValidateSubject() {
		var isError=false;
		$('#TableSubject').find(':input').each(function(){
				if($(this).hasClass('txtSubjectScore')){
					if($.trim($(this).val())==""){
							alert('All Score is Required');
							isError=true;
					}
					else if(validateNumeric($.trim($(this).val() ))==false){
							alert('Invalid Score Entry');
							isError=true;
					}
				}
				
				if($(this).hasClass('ddlSubjectResultET')){
					if($(this).val()==""){
							alert('All Subject Result Entrance Test is Required');
							isError=true;
					}
				}
				
				if(isError==true){
					return false;
				}
		});
		
		
		$('#form-subject').find('input:text').each(function(){
			$(this).val($.trim($(this).val()));
		});
		if(isError==false)
		$('#form-subject').submit();
}


$(document).ready(function()
{
	$("#form-subject :input").each(function() {
		$(this).attr('disabled', true);
	});


	$("#TableSubject :input").each(function() {
		$(this).prop('disabled', true);
	});
	
	$("#form-interview :input").each(function() {
		$(this).prop('disabled', true);
	});
	$("#form-UD :input").each(function() {
		$(this).prop('disabled', true);
	});
		
		
	$('#cancel-edit-subject').click(function(){
		$("#form-subject :input").each(function() {
			$(this).prop('disabled', true);
		});
		$("#TableSubject :input").each(function() {
			$(this).prop('disabled', true);
		});
		
		$("#subject-edit-section").removeClass('hide');
		$("#subject-save-section").addClass('hide');
	});
	
	$('#edit-subject').click(function(){
	
		$("#form-subject :input").each(function() {
			$(this).prop('disabled', false);
		});
		$("#TableSubject :input").each(function() {
			$(this).prop('disabled', false);
		});
		$("#subject-edit-section").addClass('hide');
		$("#subject-save-section").removeClass('hide');
	});
	
	
	$('#cancel-edit-interview').click(function(){
		$("#form-interview :input").each(function() {
			$(this).prop('disabled', true);
		});
	
		$("#interview-edit-section").removeClass('hide');
		$("#interview-save-section").addClass('hide');
	});
	
	$('#edit-interview').click(function(){
	
		$("#form-interview :input").each(function() {
			$(this).prop('disabled', false);
		});
	
		$("#interview-edit-section").addClass('hide');
		$("#interview-save-section").removeClass('hide');
	});
	
	$('#cancel-edit-UD').click(function(){
		$("#form-UD :input").each(function() {
			$(this).prop('disabled', true);
		});
	
		$("#UD-edit-section").removeClass('hide');
		$("#UD-save-section").addClass('hide');
	});
	
	$('#edit-UD').click(function(){
	
		$("#form-UD :input").each(function() {
			$(this).prop('disabled', false);
		});
	
		$("#UD-edit-section").addClass('hide');
		$("#UD-save-section").removeClass('hide');
	});
	
	$('[name="ddlUDSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlUDAdmissionTerm");
		$('[name="ddlUDAdmissionTerm"]').trigger("change");
		loadYearLevel($(this).val(),"ddlUDYearLevel");
		$('[name="ddlUDYearLevel"]').trigger("change");
	});
	
	
	$('#btnSaveSubject').click(function(e){
		e.preventDefault();
		ValidateSubject();
	});
	
	$('#btnSaveInterview').click(function(e){

		e.preventDefault();
		ValidateInterview();
	});
				
	$('#btnSaveUD').click(function(e){
		e.preventDefault();
		ValidateUD();
		
		//$('#form-UD').submit();
	});
});
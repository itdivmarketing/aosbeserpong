function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'entrance_test/entrance_test_schedule/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('.fancybox-opened #span_ddlAdmissionTerm .combobox-label').text("--Please Choose--");
        } 
	});
};
var loadadmissionID = function(admissionTerm){
	$.ajax({
		url: SiteURL+'entrance_test/entrance_test_schedule/get_admission_id/'+admissionTerm,
		async:false,
		type: 'get',
		dataType: "json",
        success:function(data)
        {
            $('.fancybox-opened [name="txtAdmissionID"]').val(data.AdmissionID[0].AdmissionID);
        } 
	});
};


function reset_tblPreTestSubject(){
	$('.fancybox-opened #tblPreTestSubject').empty().append($(default_tblPreTestSubject));
}

function Popup_reset(){
		// $('[name="cbInterview"]').prop('checked', false);
		document.getElementById("expand-control").checked = false;
		$( "div.toggle" ).find('.body').stop().slideUp('medium', function() {
			$( "div.toggle" ).removeClass('current');
		});
		$('.fancybox-opened [name="hfStatus"]').val('');
		$('.fancybox-opened [name="hfEntranceTestID"]').val('');
		$('.fancybox-opened [name="txtAdmissionID"]').val(default_admissionID);
		$('.fancybox-opened [name="ddlSchoolLevel"]').val("");
		$('.fancybox-opened [name="hfSchoolLevel"]').val("");
		$('.fancybox-opened [name="ddlSchoolLevel"]').attr('disabled',false);
		$('.fancybox-opened #span_ddlSchoolLevel .combobox-label').text("--Please Choose--");
		$('.fancybox-opened [name="ddlAdmissionTerm"]').val("");
		$('.fancybox-opened #span_ddlAdmissionTerm .combobox-label').text("--Please Choose--");
		$('.fancybox-opened [name="hfAdmissionTerm"]').val("");
		$('.fancybox-opened [name="ddlAdmissionTerm"]').attr('disabled',false);
		$('.fancybox-opened [name="ddlVenue"]').val("");
		$('.fancybox-opened #span_ddlVenue .combobox-label').text("--Please Choose--");
		$('.fancybox-opened [name="txtStartDate"]').val("");
		$('.fancybox-opened [name="txtEndDate"]').val("");
		$('.fancybox-opened [name="txtStartTime"]').val("");
		$('.fancybox-opened [name="txtEndTime"]').val("");
		$('.fancybox-opened [name="ddlVenueInterview"]').val("");
		$('.fancybox-opened [name="txtDate"]').val("");
		$('.fancybox-opened [name="txtStartTimeInterview"]').val("");
		$('.fancybox-opened [name="txtEndTimeInterview"]').val("");
		$('#span_ddlVenueInterview .combobox-label').text("--Please Choose--");
		reset_tblPreTestSubject();
}

function Popup_add(){
	$('.datepicker').datepicker('destroy');
	$('.datepicker').datepicker({ 
				dateFormat  : 'd-M-yy',
				changeYear  : true,
				changeMonth : true}
	);
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	Popup_reset();
            	$('[name="hfStatus"]').val('insert');
    		}
    });
}


function Popup_edit(data, row){
	$('.datepicker').datepicker('destroy');
	$('.datepicker').datepicker({ 
		dateFormat  : 'd-M-yy',
		changeYear  : true,
		changeMonth : true}
	);
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            Popup_reset();
            $('[name="hfStatus"]').val('update');
            $('[name="hfEntranceTestID"]').val(row.attr('data-id'));
            if(data.EntranceTestSchedule)
			{
				$('.fancybox-opened [name="txtAdmissionID"]').val(data.EntranceTestSchedule[0].AdmissionID);
				$('.fancybox-opened [name="ddlSchoolLevel"]').val(data.EntranceTestSchedule[0].SchoolLevelID);
				$('.fancybox-opened [name="hfSchoolLevel"]').val(data.EntranceTestSchedule[0].SchoolLevelID);
				$('.fancybox-opened [name="hfSchoolLevel"]').val(data.EntranceTestSchedule[0].SchoolLevelID);
				$('.fancybox-opened [name="ddlSchoolLevel"]').attr('disabled',true);
				var schoolLevel= $('#span_ddlSchoolLevel [name="ddlSchoolLevel"] option[value="'+data.EntranceTestSchedule[0].SchoolLevelID+'"]').text();
				$('#span_ddlSchoolLevel .combobox-label').text(schoolLevel);

				loadPretestTerm(data.EntranceTestSchedule[0].SchoolLevelID);
				$('.fancybox-opened [name="ddlAdmissionTerm"]').val(data.EntranceTestSchedule[0].TermID);
				var admissionTerm= $('#span_ddlAdmissionTerm [name="ddlAdmissionTerm"] option[value="'+data.EntranceTestSchedule[0].TermID+'"]').text();
				$('#span_ddlAdmissionTerm .combobox-label').text(admissionTerm);
				$('.fancybox-opened [name="hfAdmissionTerm"]').val(data.EntranceTestSchedule[0].TermID);
				$('.fancybox-opened [name="ddlAdmissionTerm"]').attr('disabled',true);
				$('.fancybox-opened [name="ddlVenue"]').val(data.EntranceTestSchedule[0].Venue);
				var veneu = $('#span_ddlVenue [name="ddlVenue"] option[value="'+data.EntranceTestSchedule[0].Venue+'"]').text();
				$('#span_ddlVenue .combobox-label').text(veneu);
				 

				startDate_Edit = new Date(moment(data.EntranceTestSchedule[0].StartDate, 'DD-MM-YYYY', true).format());
				endDate_Edit = new Date(moment(data.EntranceTestSchedule[0].EndDate, 'DD-MM-YYYY', true).format());
				$('[name="txtStartDate"]').val(moment(startDate_Edit).format('D-MMM-YYYY'));
				$('[name="txtEndDate"]').val(moment(endDate_Edit).format('D-MMM-YYYY'));

				$('[name="txtStartTime"]').val(data.EntranceTestSchedule[0].StartTime);
				$('[name="txtEndTime"]').val(data.EntranceTestSchedule[0].EndTime);
				
				if(data.EntranceTestSchedule[0].InterviewVenue==0){
						$('.fancybox-opened [name="ddlVenueInterview"]').val("");
				}else{
						$('[name="cbInterview"]').attr('checked','checked');
						$('.fancybox-opened [name="ddlVenueInterview"]').val(data.EntranceTestSchedule[0].InterviewVenue);
						
						$( "div.toggle" ).find('.body').stop().slideDown('medium', function() {
							$( "div.toggle" ).addClass('current');
						});

				}
				var veneuforInterview = $('#span_ddlVenueInterview [name="ddlVenueInterview"] option[value="'+$('.fancybox-opened [name="ddlVenueInterview"]').val()+'"]').text();
				$('#span_ddlVenueInterview .combobox-label').text(veneuforInterview);
				if(data.EntranceTestSchedule[0].InterviewDate=="")
					$('.fancybox-opened [name="txtDate"]').val("");
				else{
					interviewDate_Edit = new Date(moment(data.EntranceTestSchedule[0].InterviewDate, 'DD-MM-YYYY', true).format());		
					$('.fancybox-opened [name="txtDate"]').val(moment(interviewDate_Edit).format('D-MMM-YYYY'));
					document.getElementById("expand-control").checked = true;

				}
				$('.fancybox-opened [name="txtStartTimeInterview"]').val(data.EntranceTestSchedule[0].InterviewStartTime);
				$('.fancybox-opened [name="txtEndTimeInterview"]').val(data.EntranceTestSchedule[0].InterviewEndTime);

			}
			if(data.PreTestSubject)
			{
				$('#tblPreTestSubject').find('input[type="checkbox"]').each(function(){
					for (var i in data.PreTestSubject)
					{
						if ($(this).val() == data.PreTestSubject[i].PreTestSubjectId)
						{
							$(this).attr('checked','checked');
						}
					}
				})
			}
    		}
    });
}

var edit = function(){
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('update');
	
	
	$.ajax({
		url: SiteURL+'entrance_test/entrance_test_schedule/get_entrance_test_schedule/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			Popup_edit(data, row);
        } 
	});
	
};

var deleted = function(e){
	e.preventDefault();
	var ask = confirm('Are you sure to delete this record ?');
	if(!ask)return;
	
	var row = $(this).parents('tr');
	
	$('[name="hfStatus"]').val('delete');
	$('[name="hfEntranceTestID"]').val(row.attr('data-id'));
	
	$('[name="frmAdd"]').submit();
};


var submitForm = function() {
		// var currDate = new Date();
		// currDate = currDate.getDate()+'-'+currDate.getMonth()+'-'+currDate.getYear();
		// currDate = new Date(Date.parse(currDate));
		var nowDate = new Date();
		nowDate.setHours(0,0,0,0);

		var startDate = new Date(Date.parse($('.fancybox-opened [name="txtStartDate"]').val()));
		var endDate = new Date(Date.parse($('.fancybox-opened [name="txtEndDate"]').val()));
		var startTime = new Date(Date.parse($('.fancybox-opened [name="txtStartTime"]').val()));
		var endTime = new Date(Date.parse($('.fancybox-opened [name="txtEndTime"]').val()));
		var interviewDate = new Date(Date.parse($('.fancybox-opened [name="txtDate"]').val()));
		interviewDate.setHours(0,0,0,0);
		var interviewStartTime = new Date(Date.parse($('.fancybox-opened [name="txtStartTimeInterview"]').val()));
		var interviewEndTime = new Date(Date.parse($('.fancybox-opened [name="txtEndTimeInterview"]').val()));

		var nowTime = new Date();
		console.log(startDate);
		console.log(endDate);

		if(nowDate > startDate)
			alert('Entrance test start date cannot be in the past');
		else if(nowDate > endDate)
			alert('Entrance test end date cannot be in the past');
		else if(nowDate > interviewDate && $('.fancybox-opened [name="cbInterview"]').attr('checked') == true)
			alert('Interview test date cannot be in the past');
		else if(startDate > endDate)
			alert('Entrance test end date must be greater than start date');
		else if(+startDate===+endDate && (startTime > endTime))
			alert("Invalid Entrance test time, Entrance test schedule start time can't greater than end time");
		else if(+startDate===+endDate && +startTime === +endTime)
			alert("Invalid Entrance test time, Entrance test schedule start time can't be same as end time");
		else if(+startDate===+nowDate && nowTime > startTime)
			alert("Invalid Entrance test time, Entrance test schedule start time already passed")
		else if(endDate > interviewDate && $('.fancybox-opened [name="cbInterview"]').is(':checked'))
			alert('Interview date must be greater than end date');
		else if($('.fancybox-opened [name="cbInterview"]').is(':checked') && +nowDate === +interviewDate && nowTime > interviewStartTime)
			alert("Invalid Interview time, Interview schedule start time already passed");
		else if($('.fancybox-opened [name="cbInterview"]').is(':checked') && interviewStartTime > interviewEndTime)
			alert("Invalid Interview time, Interview schedule start time can't greater than end time");
		else if($('.fancybox-opened [name="cbInterview"]').is(':checked') && +interviewStartTime === +interviewEndTime)
			alert("Invalid Interview time, Interview schedule start time can't be same as end time");
		else
			$('.fancybox-opened [name="frmAdd"]').submit();
}

function validate() {
	if($('.fancybox-opened [name="hfStatus"]').val() == 'update')
	{
		if($('.fancybox-opened [name="hfEntranceTestID"]').val() == '')
			alert('Entrance Test ID required');
		else if($('.fancybox-opened [name="hfSchoolLevel"]').val() == '')
			alert('School Level required');
		else if($('.fancybox-opened [name="hfAdmissionTerm"]').val() == '')
			alert('Admission Term required');
		else if($('.fancybox-opened [name="txtAdmissionID"]').val() == '')
			alert('Admission ID required');
		else if($('.fancybox-opened [name="ddlVenue"]').val() == '')
			alert('Venue required');
		else if($('.fancybox-opened [name="txtStartDate"]').val() == '')
			alert('Start date required');
		else if($('.fancybox-opened [name="txtEndDate"]').val() == '')
			alert('End date required');
		else if($('.fancybox-opened [name="txtStartTime"]').val() == '')
			alert('Start time required');
		else if($('.fancybox-opened [name="txtEndTime"]').val() == '')
			alert('End time required');
		else 
		{
			if ($('.fancybox-opened [name="cbInterview"]').is(':checked') && $('#tblPreTestSubject :checkbox:checked').length <= 0)
			{
				alert('you have to choose between Interview and PreTest');
			}
			else if ($('.fancybox-opened [name="cbInterview"]').is(':checked'))
			{
				if($('.fancybox-opened [name="ddlVenueInterview"]').val() == '')
					alert('Venue Interview ID required');
				else if($('.fancybox-opened [name="txtDate"]').val() == '')
					alert('Date required');
				else if($('.fancybox-opened [name="txtStartTimeInterview"]').val() == '')
					alert('Start Time required');
				else if($('.fancybox-opened [name="txtEndTimeInterview"]').val() == '')
					alert('End Time required');
				else
					submitForm();
			}
			else
			{
				if ($('#tblPreTestSubject :checkbox:checked').length <= 0 )
				{
					alert('PreTest Subject required');
				}
				else
				{
					submitForm();
				}				
			}
		}
	}
	else
	{
		if($('.fancybox-opened [name="ddlSchoolLevel"]').val() == '')
			alert('School Level required');
		else if($('.fancybox-opened [name="ddlAdmissionTerm"]').val() == '')
			alert('Admission Term required');
		else if($('.fancybox-opened [name="txtAdmissionID"]').val() == '')
			alert('Admission ID required');
		else if($('.fancybox-opened [name="ddlVenue"]').val() == '')
			alert('Venue required');
		else if($('.fancybox-opened[name="txtStartDate"]').val() == '')
			alert('Start date required');
		else if($('.fancybox-opened [name="txtEndDate"]').val() == '')
			alert('End date required');
		else if($('.fancybox-opened[name="txtStartTime"]').val() == '')
			alert('Start time required');
		else if($('.fancybox-opened [name="txtEndTime"]').val() == '')
			alert('End time required');
		else 
		{
			if ($('.fancybox-opened [name="cbInterview"]').is(':checked') && $('#tblPreTestSubject :checkbox:checked').length <= 0)
			{
				alert('you have to choose between Interview and PreTest');
			}
			else if ($('.fancybox-opened [name="cbInterview"]').is(':checked'))
			{
				if($('.fancybox-opened [name="ddlVenueInterview"]').val() == '')
					alert('Venue Interview ID required');
				else if($('.fancybox-opened [name="txtDate"]').val() == '')
					alert('Date required');
				else if($('.fancybox-opened [name="txtStartTimeInterview"]').val() == '')
					alert('Start Time required');
				else if($('.fancybox-opened [name="txtEndTimeInterview"]').val() == '')
					alert('End Time required');
				else
					submitForm();
			}
			else
				if ($('#tblPreTestSubject :checkbox:checked').length <= 0 )
				{
					alert('PreTest Subject required');
				}
				else
				{
					submitForm();
				}			
		}
	}
}

function drop_toggle(){
	var $element = document.getElementsByClassName(".toggle")[0];
	if ($( "div.toggle" ).hasClass('current') || $("#expand-control").ischecked) {
		$( "div.toggle" ).find('.body').stop().slideUp('medium', function() {
		$( "div.toggle" ).removeClass('current');
					});
	} else {
		$( "div.toggle" ).find('.body').stop().slideDown('medium', function() {
		$( "div.toggle" ).addClass('current');
		});
	}
	var veneuforInterview = $('#span_ddlVenueInterview [name="ddlVenueInterview"] option[value="'+$('.fancybox-opened [name="ddlVenueInterview"]').val()+'"]').text();
	$('#span_ddlVenueInterview .combobox-label').text(veneuforInterview);
}

function loadAdmissiID(){
 loadadmissionID($('.fancybox-opened [name="ddlAdmissionTerm"]').val());
}


$(document).ready(function()
{

	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());

	});


	
	$('#show-popup-internal').click(function(e){
	    	e.preventDefault();
	    	
			Popup_add();
			$('[name="ddlSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val());
			});

			$('[name="ddlAdmissionTerm"]').change(function(){
				loadadmissionID($(this).val());
			});
	    });

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});
	
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
	   if($(this).hasClass("disable") == false)
	   {
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
	   }
	});
	
	$('#tblView').on('click','.btnEdit',edit);
	$('#tblView').on('click','.btnDelete',deleted);

	if( $('#modal .expand-control-toggle').is(':checked') ){
		$('#modal .expand-control-toggle').closest('.expand-control-wrap').find('.expand-control').slideDown();
	}
});
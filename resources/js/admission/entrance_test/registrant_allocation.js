function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'entrance_test/entrance_test_schedule/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
				
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};

function validate() {
	if($('.fancybox-opened [name="txtScheduleID"]').val() == '')
		alert('Schedule ID required');
	else if($('.fancybox-opened  [name="hfAdmissionTerm"]').val() == '')
		alert('Admission Term required');
	else if($('.fancybox-opened  .comboboxAllocation:checked').length > $('.fancybox-opened [name="hfCapacity"]').val())
		alert('Registrant count('+$('.fancybox-opened .comboboxAllocation:checked').length+') exceed the capacity ('+$('.fancybox-opened [name="hfCapacity"]').val()+')');
	/*
	else if($('#modal .comboboxAllocation:checked').length ==  0)
		alert('choose a registrant to allocate');*/
	else
	{
		var slot = $('.fancybox-opened [name="hfCapacity"]').val() - $('.fancybox-opened .comboboxAllocation:checked').length;
		if(slot != 0)
		{
			var ask = confirm('This room still have '+ slot +' slot(s). Are you sure want to proceed?');
			if(!ask)return;
		}
		$('.fancybox-opened [name="frmAdd"]').submit();
	}
}

var edit = function(){
	var row = $(this).parents('tr');
	$.ajax({
		url: SiteURL+'entrance_test/registrant_allocation/get_all_registrant/'+row.attr('data-id')+'/'+row.attr('data-term')+'/'+row.attr('data-venue')+'/'+row.attr('data-capacity'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			$('[name="hfAdmissionTerm"]').val(data.termID);
			$('[name="hfScheduleID"]').val(data.scheduleID);
			$('[name="hfVenue"]').val(data.venue);
			$('[name="hfCapacity"]').val(data.capacity);
			
			$('[name="lblAdmissionTerm"]').text(data.termID);
			$('[name="lblScheduleID"]').text(data.scheduleID);
			$('[name="lblVenue"]').text(data.venue);
			$('[name="lblCapacity"]').text(data.capacity);
			if(data.registrants)
			{
				$('#tblRegistrant').find('td').parent().remove();
				
				for(var i in data.registrants)
				{
					var cell = [
						,'<td>'+data.registrants[i].RegistrantID+'</td>'
						,'<td>'+data.registrants[i].RegistrantName+'</td>'
						,'<td><input type="checkbox" name="cbAllocation[]" class="comboboxAllocation" value="'+data.registrants[i].RegistrantID+'" '+(data.registrants[i].cek == 1 ? 'checked="checked"' : '' )+' /></td>'
					];
					var row = $('<tr data-id="'+data.registrants[i].RegistrantID+'">'+cell.join()+'</tr>');
					$('#tblRegistrant').append(row);
				}
			}
			else
			{
				var row = $('<tr><td colspan="3">There\'s No Data</td></tr>');
				$('#tblRegistrant').append(row);
			}
        },
		complete:function(){
			$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
   			});
		}
	});
};

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val());
	});
	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val('1');
		$('[name=frmSearch]').submit();
	});
	
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	$('.next').on('click',function(){
	   if($(this).hasClass("disable") == false)
	   {
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
	   }
	});
	
	$('#tblView').on('click','.btnEdit',edit);
});
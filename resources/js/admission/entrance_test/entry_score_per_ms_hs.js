var loadPretestTerm = function(SchoolLevel){
	$.ajax({
		url: SiteURL+'entrance_test/entry_score_per_ms_hs/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlAdmissionTerm"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="ddlAdmissionTerm"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="ddlAdmissionTerm"]').trigger("change");
        } 
	});
};

var loadSchedule = function(SchoolLevel, PretestTerm){
	$.ajax({
		url: SiteURL+'entrance_test/entry_score_per_ms_hs/get_schedule/'+SchoolLevel+'/'+PretestTerm,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlSchedule"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.Schedule)
			{
				for(var i in data.Schedule)
				{
					$('[name="ddlSchedule"]').append($('<option value="'+$.trim(data.Schedule[i].ScheduleID)+'">'+$.trim(data.Schedule[i].ScheduleID)+'</option>'));
				}
			}
			$('[name="ddlSchedule"]').trigger("change");
        } 
	});
};

var to_json = function(workbook) {
		var result = {};
		workbook.SheetNames.forEach(function(sheetName) {
			var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
			if(roa.length > 0){
				result[sheetName] = roa;
			}
		});
		return result;
	}
	var tarea = $('#b64data');
	var b64it = function() {
		var wb = XLSX.read(tarea.value, {type: 'base64'});
		process_wb(wb);
	}
	var process_wb = function(wb) {
        $.ajax({
            url:SiteURL+'entrance_test/entry_score_per_ms_hs/set_score',
            dataType: 'json',
			async:false,
            type: 'POST',
            data: JSON.stringify(to_json(wb)),
            contentType: 'application/json;charset=utf-8',
            success:function(d){
				if(d.status)
				{
					alert('success save '+ d.total + ' data(s) into the database')
				}
				else
				{
					alert(d.message);
				}
			}
        });
	}
	
	var handleDrop = function (e) {
		e.stopPropagation();
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		if (files) {
			var file = files[0];
			var data = new FormData();
			data.append('file', file);
			$.ajax({
				url: SiteURL+'entrance_test/entry_score_per_ms_hs/upload',
				data: data,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				type: 'POST',
				async:false,
				success: function(data){
					if(!data.status)
					{
						alert(data.message);
						return;
					}else{
						for (i = 0, f = files[i]; i != files.length; ++i) {
							var reader = new FileReader();
							var name = f.name;
							reader.onload = function(e) {
							var data = e.target.result;
							//var wb = XLSX.read(data, {type: 'binary'});
							var arr = String.fromCharCode.apply(null, new Uint8Array(data));
							var wb = XLSX.read(btoa(arr), {type: 'base64'});
							process_wb(wb);
							};
							//reader.readAsBinaryString(f);
							reader.readAsArrayBuffer(f);
						}
					}
				}
			});
		}
	}
	
	var handleSelect=function(e) {
		var files = $('#file').prop('files');
		if (files) {
			var file = files[0];
			var data = new FormData();
			data.append('file', file);
			$.ajax({
				url: SiteURL+'entrance_test/entry_score_per_ms_hs/upload',
				data: data,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				async:false,
				type: 'POST',
				success: function(data){
					if(!data.status)
					{						
						alert(data.message);
						return;
					}else{
						for (i = 0, f = files[i]; i != files.length; ++i) {
						var reader = new FileReader();
						var name = f.name;
						reader.onload = function(e) {
						var data = e.target.result;
						//var wb = XLSX.read(data, {type: 'binary'});
						var arr = String.fromCharCode.apply(null, new Uint8Array(data));
						var wb = XLSX.read(btoa(arr), {type: 'base64'});
						process_wb(wb);
						};
						//reader.readAsBinaryString(f);
						reader.readAsArrayBuffer(f);
						}
					}
				}
			});
		}
		

		
	}

	var handleDragover = function (e) {
		e.stopPropagation();
		e.preventDefault();
		e.originalEvent.dataTransfer.dropEffect = 'copy';
	}
	
	$('document').ready(function(){
		$('[name="ddlSchoolLevel"]').change(function(){
			loadPretestTerm($(this).val());
		});
		
		$('[name="ddlAdmissionTerm"]').change(function(){
			loadSchedule($('[name="ddlSchoolLevel"]').val(),$(this).val());
		});
		
		$('#templateGrade').attr('href',BaseURL+'resources/template/entrance_test/TemplateScoreMSHS.xlsx');
		$('#file').on('change', handleSelect);
		$('#drop').bind('dragenter', handleDragover);
		$('#drop').bind('dragover', handleDragover);
		$('#drop').bind('drop', handleDrop);
	});

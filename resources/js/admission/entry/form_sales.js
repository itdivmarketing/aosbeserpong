function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}



function print(){
	/*var row = $(this).parents('tr');
	$.ajax({
		url: SiteURL+'entry/form_sales/get_printable_view/'+row.attr('data-id'),
		type: 'get',
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			var win = window.open();
            win.document.write(data);
        } 
	});*/
	var row = $(this).parents('tr');
	$("[name='hfFormNumber']").val(row.attr('data-id'));
	$("[name='frmReceipt']").submit();
};




var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};


var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};

var getAdmissionID = function(TermID){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_admissionID/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
			// else{
			// 	$('[name="txtViewAdmissionID"]').val("")
			// }
        } 
	});
};

var getFormAmount = function(FormNumber){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_form_amount/'+FormNumber,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FormSoldView)
			{
				
				for(var i in data.FormAmount)
				{ 
					$('[name="txtViewAmount"]').val( data.FormAmount[i].Amount);
				}
			}
        } 
	});
};

var loadPaymentMethod = function(){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_payment_method/',
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlViewPaymentMethod"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PaymentMethod)
			{
				for(var i in data.PaymentMethod)
				{
				
					$('[name="ddlViewPaymentMethod"]').append($('<option value="'+$.trim(data.PaymentMethod[i].PaymentMethodID)+'">'+$.trim(data.PaymentMethod[i].PaymentMethodName)+'</option>'));
				}
			}else{

			}
			$('[name="ddlViewPaymentMethod"]').trigger("change");
        } 
	});
};

var generatePassword = function(method,FormNumber,Email){
	$.ajax({
		url: SiteURL+'entry/form_sales/generate_password/'+method+'/'+FormNumber+'/'+Email,
		type: 'post',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
        	console.log(data.GeneratedPassword);
			if(data.GeneratedPassword)
			{
				$('[name="txtViewPassword"]').val(data.GeneratedPassword);
			}else{
				$('[name="txtViewPassword"]').val("");
			}
        } 
	});
};


var validate = function() {

	if($('.fancybox-opened [name="txtViewFormNumber"]').val() == '')
		alert('Form Number is required');
	else{ 
		checkForm($.trim($('.fancybox-opened [name="txtViewFormNumber"]').val()),$('.fancybox-opened [name="ddlViewAdmissionTerm"] option:selected').val(),myCallbackSave);
	}
}

function myCallbackSave(Valid) {
	//myCallbackCheck(Valid);
	if(Valid==0){
		if($.trim($('.fancybox-opened [name="txtViewAdmissionYear"]').val()) == '')
			alert('AdmissionYear required');
		else if($.trim($('.fancybox-opened [name="ddlViewYearLevel"]').val() )== '')
			alert('Year Level is required');	
		else if($.trim($('.fancybox-opened [name="ddlViewAdmissionTerm"]').val() )== '')
			alert('Admission Term required');
		else if($.trim($('.fancybox-opened [name="txtViewPaymentDate"]').val() )== '')
			alert('Payment Date Term required');
		else if($.trim($('.fancybox-opened [name="txtViewReceiptName"]').val() )== '')
			alert('Receipt Name is required');
		else if($.trim($('.fancybox-opened [name="txtViewPaidBy"]').val() )== '')
			alert('Paid By is required');
		else if($.trim($('.fancybox-opened [name="txtViewOfficerName"]').val()) == '')
			alert('Officer name is required');
		else if($.trim($('.fancybox-opened [name="txtViewStudentName"]').val() )== '')
			alert('Student name is required');
		else if($.trim($('.fancybox-opened [name="txtViewPhoneNumber"]').val()) == '')
			alert('Phone number required');
		else if(validateNumeric($.trim($('.fancybox-opened [name="txtViewPhoneNumber"]').val()))==false)
			alert('Phone Number must be numeric');
		else if($.trim($('.fancybox-opened [name="txtViewAmount"]').val() )== '')
			alert('Payment Amount is required');
		else if($.trim($('.fancybox-opened [name="txtViewAmountPaid"]').val() )== '')
			alert('Amount Paid is required');	
		else if(validateNumeric($.trim($('.fancybox-opened [name="txtViewAmountPaid"]').val()))==false)
			alert('Amount Paid must be numeric');
		else if($.trim($('.fancybox-opened [name="ddlViewPaymentMethod"]').val()) == '')
			alert('Payment method is required');
		else if($.trim($('.fancybox-opened [name="txtViewEmail"]').val()) == '')
			alert('Email is required');
		else if( validateEmail($('.fancybox-opened [name="txtViewEmail"]').val())==false )
			alert('Email field is not valid');
		else if($.trim($('.fancybox-opened [name="txtViewPassword"]').val()) == '')
				alert('Password is required');
		else
		{	
			$('.fancybox-opened [name="frmAdd"]').submit();
			$.fancybox.close();
		}
	}
}


var checkForm = function(FormNumber,TermID,callback) {
	var Valid=2;
	var Amount=0;
	var AmountValue=0;
	var AjaxData;
	if(FormNumber==""){
		alert("Please Input Form Number");
		return false;
	}
	/*
	if($('[name="ddlViewAdmissionTerm"]').val()==""){
		alert("Please Select School Level,AdmissionTerm and Year Level");
		return false;
	}*/
	
	$.ajax({
		url: SiteURL+'entry/form_sales/check_form_validation/'+FormNumber+'/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FormStatus){
				if(data.FormStatus.FormStatus=="0"){
						AjaxData=data;
						Amount=data.FormStatus.Amount;
						AmountValue=data.FormStatus.AmountValue;
						if(callback==""){
						alert("This FormNumber is Valid");
						}
					Valid=0;
					}
				else if(data.FormStatus.FormStatus=="1"){
					alert("This FormNumber has Been Purchased");
					Valid=1;
				}
				else{
					alert("Form Number doesn't Exist");
					Valid=2;
				}
			}
        },
		complete:function(){
				if(Valid==0){
						if(callback==""){
							$('.fancybox-opened [name="txtViewAmount"]').val(Amount);
							$('.fancybox-opened [name="hfViewAmount"]').val(AmountValue);
							$('.fancybox-opened [name="ddlViewSchoolLevel"]').val(AjaxData.FormStatus.SchoolLevelID);
							var schoolLevel= $('[name="ddlViewSchoolLevel"] option[value="'+AjaxData.FormStatus.SchoolLevelID+'"]').text();
							$('.fancybox-opened #ddlViewSchoolLevel .combobox-label').text(schoolLevel);
							loadPretestTerm(AjaxData.FormStatus.SchoolLevelID,"ddlViewAdmissionTerm");
							$('[name="ddlViewAdmissionTerm"]').val(AjaxData.FormStatus.TermID);
							var admmisionTerm = $('[name="ddlViewAdmissionTerm"] option[value="'+AjaxData.FormStatus.TermID+'"]').text();
							$('.fancybox-opened #ddlViewAdmissionTerm .combobox-label').text(admmisionTerm);

							loadYearLevel(AjaxData.FormStatus.SchoolLevelID,"ddlViewYearLevel");

							getAdmissionID(AjaxData.FormStatus.TermID);
							$('.fancybox-opened [name="txtViewAmountPaid"]').val(AmountValue);
						}
				}else{
					$('.fancybox-opened [name="txtViewAmount"]').val("");
					$('.fancybox-opened [name="hfViewAmount"]').val("");

					$('.fancybox-opened [name="ddlViewSchoolLevel"]').val("");
					$('.fancybox-opened [name="ddlViewSchoolLevel"]').trigger("change");
					
					loadPretestTerm("","ddlViewAdmissionTerm");
					$('[name="ddlViewAdmissionTerm"]').val("");
					$('[name="ddlViewAdmissionTerm"]').trigger("change");
					
					loadYearLevel("","ddlViewYearLevel");
					$('[name="ddlViewAdmissionTerm"]').val("");
					$('[name="ddlViewAdmissionTerm"]').trigger("change");

					getAdmissionID("");
					$('.fancybox-opened [name="txtViewAmountPaid"]').val("");

				}
				
				if(callback!=""){
					callback(Valid);
				}
		
		}
	});
}

var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'entry/form_sales/get_form_sold_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
			$('#popup-title').text('View Form Sales');
			Popup_edit(AjaxData);
			$('#btnSave').hide();
			$('#btnCheck').hide();
        },
		complete:function(){
			
			//$('[name="txtViewAdmissionSemester"]').val($('[name="hfViewAdmissionSemester"]').val());




			
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			//$('.fancybox-opened #btnSave').click(validate);
		}
	});
	
};

function generate(){
	if(validateEmail($('.fancybox-opened [name="txtViewEmail"]').val())!=false && $.trim($('.fancybox-opened [name="txtViewEmail"]').val())!=""){
		if($('[name="hfStatus"]').val()=="insert"){
			generatePassword("add","","");
		}else{
			generatePassword("view",$.trim($('.fancybox-opened [name="txtViewFormNumber"]').val()),$.trim($('.fancybox-opened [name="txtViewEmail"]').val()));
		}
	}else{
		alert("Email not Valid");
	}
}


function check(){
	var Valid = checkForm($.trim($('.fancybox-opened [name="txtViewFormNumber"]').val()),$('.fancybox-opened [name="ddlViewAdmissionTerm"] option:selected').val(),"");
}

//buat munculin pop up add
function Popup_add(){
	//Show FancyBox

		$('#popup-title').text('Add Form Sales');
		$.fancybox({
	        	href 		: '#internal-popup',
	       	    minWidth    : 640,
	            fitToView   : true,
	            closeClick  : false,
	        	afterShow: function () {

	        		$('[name="hfStatus"]').val('insert');
	        		$('.fancybox-opened #btnCheck').show(); //tambahin onclick dia ngapain
					$('.fancybox-opened #btnSave').show();
					//Set to default
					//$('[name="ddlViewBuilding"]').val('');
					//var txtSelected = $("[name='ddlViewBuilding'] option[value='']").text();
					//$('[name="frmAdd"] .combobox-label').text(txtSelected);
					//$('.fancybox-opened #btnSave').click(validate);

					loadPaymentMethod();
					loadPretestTerm($('[name="ddlViewSchoolLevel"]').val(),"ddlViewAdmissionTerm");
					loadYearLevel($('[name="ddlViewSchoolLevel"]').val(),"ddlViewYearLevel");

					//yang mana yang ke disable
					$('[name="txtViewAdmissionYear"]').attr("readonly", "readonly");
					$('[name="txtViewAdmissionID"]').attr("readonly", "readonly");
					$('[name="txtViewAdmissionSemester"]').attr("readonly", "readonly");
					$('[name="ddlViewSchoolLevel"]').attr("disabled", "readonly"); 
					$('[name="txtViewPassword"]').attr("readonly", "readonly");

					//yang mana yang ke enable
					$('[name="txtViewFormNumber"]').prop('readonly', false);
					$('[name="txtViewEmail"]').prop('readonly', false);
					$('[name="txtViewPaidBy"]').prop('readonly', false);
					// $('[name="txtViewPaymentDate"]').attr("disabled", "readonly");
					$('[name="txtViewReceiptName"]').prop('readonly', false);
					$('[name="txtViewOfficerName"]').prop('readonly', false);
					$('[name="txtViewStudentName"]').prop('readonly', false);
					$('[name="txtViewPhoneNumber"]').prop('readonly', false);
					$('[name="txtViewAmountPaid"]').prop('readonly', false);
					
					//button mana aja yang ke add


					//dropdown mana aja yang bisa kebuka
					$('[name="ddlViewAdmissionTerm"]').prop("disabled", false); 
					$('[name="ddlViewYearLevel"]').prop("disabled", false); 
					$('[name="ddlViewPaymentMethod"]').prop("disabled", false); 
					$('[name="txtViewPaymentDate"]').prop("readonly", false);


					$('[name="ddlViewSchoolLevel"]').change(function(){
						loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
						loadYearLevel($(this).val(),"ddlViewYearLevel");
					});
	
					$('[name="ddlViewAdmissionTerm"]').change(function(){
						getAdmissionID($(this).val());
					});

					
	    		},
	    		beforeClose: function(){
	    			Popup_reset();
	    		},

	    });
}

//buat ngapus semua data pop upnya
function Popup_reset(){
	$('.fancybox-opened [name="hfStatus"]').val('');
	$('.fancybox-opened [name="hfAdmissionID"]').val("");
	$('.fancybox-opened [name="hfScheduleID"]').val("");
	$('.fancybox-opened [name="hfVenue"]').val("");
	$('.fancybox-opened [name="hfViewAdmissionSemester"]').val("");
	$('.fancybox-opened [name="txtViewAdmissionID"]').val("");
	$('.fancybox-opened [name="ddlViewSchoolLevel"]').val("");
	//buat ngapus data dalam modal

	$('.fancybox-opened [name="frmAdd"] .combobox-label').text("--Please Choose--");
	$('.fancybox-opened [name="txtViewFormNumber"]').val("");
	$('.fancybox-opened [name="ddlViewAdmissionTerm"]').val("");
	$('.fancybox-opened [name="ddlViewYearLevel"]').val("");
	$('.fancybox-opened [name="txtViewPaymentDate"]').val("");
	$('.fancybox-opened [name="txtViewReceiptName"]').val("");
	$('.fancybox-opened [name="txtViewPaidBy"]').val("");
	$('.fancybox-opened [name="txtViewOfficerName"]').val("");
	$('.fancybox-opened [name="txtViewStudentName"]').val("");
	$('.fancybox-opened [name="txtViewPhoneNumber"]').val("");
	$('.fancybox-opened [name="txtViewAmount"]').val("");
	$('.fancybox-opened [name="hfViewAmount"]').val("");
	$('.fancybox-opened [name="txtViewAmountPaid"]').val("");
	$('.fancybox-opened [name="ddlViewPaymentMethod"]').val("");
	$('.fancybox-opened [name="txtViewEmail"]').val("");
	$('.fancybox-opened [name="txtViewFormNumber"]').val("");
	$('.fancybox-opened [name="txtViewPassword"]').val("");
}

function Popup_edit(AjaxData){
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {
        		/*$('[name="ddlViewSchoolLevel"]').val(data.FormSoldView.SchoolLevelID);
				var selected = $('.fancybox-opened [name="ddlSchoolLevel"]').val();
				var txtSelected = $('.fancybox-opened [name="ddlSchoolLevel"] option[value="'+selected+'"]').text(); */

				//$('.fancybox-opened #btnSave').click(validate);
			if(AjaxData.FormSoldView)
			{
			
				var Form=$('.fancybox-opened #frmAdd');

				var $inputs = $('.fancybox-opened :input');

				$inputs.each(function() {
					$(this).prop('readonly', true);
				});
				
				$('.fancybox-opened #btnCheck').hide();
				$('.fancybox-opened #btnSave').hide();

				$('[name="txtViewEmail"]').prop('readonly', false); 
				$('[name="ddlViewSchoolLevel"]').prop("disabled", true); 
				$('[name="ddlViewAdmissionTerm"]').prop("disabled", true); 
				$('[name="ddlViewYearLevel"]').prop("disabled", true); 
				$('[name="ddlViewPaymentMethod"]').prop("disabled", true); 
				$('[name="txtViewPaymentDate"]').prop("readonly", true);
				for(var i in AjaxData.FormSoldView)
				{
					$('[name="txtViewAdmissionID"]').val(AjaxData.FormSoldView[i].AdmissionID);
					$('[name="txtViewAdmissionYear"]').val(AjaxData.FormSoldView[i].AcademicYear);
					$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormSoldView[i].SmtId);
					$('[name="ddlViewSchoolLevel"]').val(AjaxData.FormSoldView[i].SchoolLevelID);

					var txtSelected = $('.fancybox-opened [name="ddlViewSchoolLevel"] option[value="'+AjaxData.FormSoldView[i].SchoolLevelID+'"]').text();
					$('.fancybox-opened #ddlViewSchoolLevel .combobox-label').text(txtSelected);

					loadPretestTerm(AjaxData.FormSoldView[i].SchoolLevelID,"ddlViewAdmissionTerm");
					loadYearLevel(AjaxData.FormSoldView[i].SchoolLevelID,"ddlViewYearLevel");
					loadPaymentMethod();

					$('[name="ddlViewAdmissionTerm"]').val(AjaxData.FormSoldView[i].TermID);
					txtSelected = $('.fancybox-opened [name="ddlViewAdmissionTerm"] option[value="'+AjaxData.FormSoldView[i].TermID+'"]').text();
					$('.fancybox-opened #ddlViewAdmissionTerm .combobox-label').text(txtSelected);

					$('[name="ddlViewYearLevel"]').val(AjaxData.FormSoldView[i].YearLevelID);
					txtSelected = $('.fancybox-opened [name="ddlViewYearLevel"] option[value="'+AjaxData.FormSoldView[i].YearLevelID+'"]').text();
					$('.fancybox-opened #ddlViewYearLevel .combobox-label').text(txtSelected);

					$('[name="txtViewFormNumber"]').val(AjaxData.FormSoldView[i].RegistrantId);
					//var date=new Date( AjaxData.FormSoldView[i].DateOfFormPurchased));
					$('[name="txtViewPaymentDate"]').val( AjaxData.FormSoldView[i].DateOfFormPurchased);
					//alert($('[name="txtViewPaymentDate"]').datetimepicker({ dateFormat: 'dd MM yy h:i' }).val());
			
					$('[name="txtViewReceiptName"]').val(AjaxData.FormSoldView[i].RecepientName);
					$('[name="txtViewPaidBy"]').val(AjaxData.FormSoldView[i].PaidBy);
					$('[name="txtViewOfficerName"]').val(AjaxData.FormSoldView[i].OfficerName);
					$('[name="txtViewStudentName"]').val(AjaxData.FormSoldView[i].Note);
					$('[name="txtViewPhoneNumber"]').val(AjaxData.FormSoldView[i].PhoneNumberSale);
					$('[name="txtViewAmount"]').val(AjaxData.FormSoldView[i].Amount);
					$('[name="txtViewAmountPaid"]').val(AjaxData.FormSoldView[i].AmountPaid);
					
					$('[name="ddlViewPaymentMethod"]').val(AjaxData.FormSoldView[i].PaymentMethodID);
					txtSelected = $('.fancybox-opened [name="ddlViewPaymentMethod"] option[value="'+AjaxData.FormSoldView[i].PaymentMethodID+'"]').text();
					$('.fancybox-opened #ddlViewPaymentMethod .combobox-label').text(txtSelected);
					
					$('[name="txtViewEmail"]').val(AjaxData.FormSoldView[i].Email);
					$('[name="txtViewPassword"]').val(AjaxData.FormSoldView[i].Password);

				}
				//panggil modalnya
				//$('.fancybox-opened').show();
				
			} 
    		},
    		beforeClose: function(){
    			Popup_reset();
    		}
   });
}

$(document).ready(function()
{
	$(".datetimepicker").datetimepicker({ dateFormat: 'dd M yy' });

	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlAdmissionTerm");
		loadYearLevel($(this).val(),"ddlYearLevel");
	});

	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val("1");
		$('[name=frmSearch]').submit();
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});

	$('.prev').on('click',function(){
        if($(this).hasClass("disable") == false)
            {
                $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
                $('[name=frmSearch]').submit();
            }
	});
	$('.next').on('click',function(){
        if($(this).hasClass("disable") == false)
            {
                $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
                $('[name=frmSearch]').submit();
            }
	});

	$('#tblView').on('click','.btnView',view);
	$('#tblView').on('click','.btnPrint',print);
});
$(document).ready(function(){
	var queries = {};
	
	$.each(document.location.search.substr(1).split('&'),function(c,q){
		var i = q.split('=');
		if(i!="") {
			queries[i[0].toString()] = i[1].toString();
		}
		
	});
	
	if(!queries.hasOwnProperty("rbFilterSibling")) {
		$("input[name=rbFilterSibling][value='0']").prop("checked",true);
	}
	
	
	$('.datepicker').datepicker('destroy');
	
	$('.datepicker').datepicker({
		showOn: 'focus',
		showButtonPanel: true,
		closeText: 'Clear', // Text to show for "close" button
		dateFormat: 'dd M yy', 
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd M yy',
		yearRange: '-22:+0',
		onClose: function () {
			var event = arguments.callee.caller.caller.arguments[0];
			// If "Clear" gets clicked, then really clear it
			if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
				$(this).val('');
			}
		}
	});

	$('#btnSearch').on('click',function(e){

		if($('#txtSiblingNameID').val() == '' && $('#txtDOBID').val() == '')
		{
			e.preventDefault();
		}else{
			e.preventDefault();
			$('[name=hfPage]').val("1");
			$('[name=frmSearch]').submit();
		}
	});

	$('#btnSave').on('click', function(e){
		var flag = 0;
		for(var i=1; i <= $('#counterFlag').val(); i++)
		{
			//alert($('#txtInputPotentialSibling'+i).val());
			//e.preventDefault();

			if($('#txtInputPotentialSibling'+i).val() != '') //check apakah kosong
			{
				if(!$.isNumeric($('#txtInputPotentialSibling'+i).val())){ //validasi number or not
					e.preventDefault();
				}
			} else { //menghitung textbox yang kosong
				flag++;
			}

			//kondisi kosong semua 
			if(flag==$('#counterFlag').val()){
				e.preventDefault();
			}

		}
	});

	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	
	$('.prev').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			$('[name=frmSearch]').submit();
		}
	});
	
	$('.next').on('click',function(){
		if($(this).hasClass("disable") == false)
		{
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
			$('[name=frmSearch]').submit();
		}
	});

	$('#form_entry').submit(function(e) {
		e.preventDefault();
		var data = $(this).serialize();
		$.ajax({
			url: SiteURL+'entry/entry_potential_sibling/save',    
            type:'post',
            async:false,
			data:data,
			beforeSend:function() {
			},
            error:function(d){
            },
            success:function(d){
				var obj = jQuery.parseJSON(d);
				if(obj.Msg == "Failed") {
					alert("Potential Sibling StudentID "+obj.flag+ " Not Valid");
				} else {
					alert(obj.Msg);
					location.reload();
				}
			}
        });	
	});

	$("#txtSiblingNameID").keypress(function(e) {
		//var keyGet = e.keyCode;
		var keyGet = (e.keyCode) ? e.keyCode : e.which;
		var browser = navigator.appName;
		
		/*if (browser == "Microsoft Internet Explorer" || browser == "Mozilla Firefox") {
			var keyGet = evtGet.keyCode;
		} else {
			var keyGet = evtGet.which; //(window.Event) ? evtGet.which : evtGet.keyCode;
		}
        
		if (!((keyGet > 64 && keyGet < 91) || (keyGet > 96 && keyGet < 123) || (keyGet == 9) || (keyGet == 32) || (keyGet == 8) || (keyGet == 0) || (keyGet == 13)||(keyGet==110)))
            return;
        else
            if (browser == "Microsoft Internet Explorer" || browser == "Mozilla Firefox")
            window.e.returnValue = null;
        else
            e.preventDefault();*/
		
		//console.log(keyGet);
		
		if (!((keyGet > 64 && keyGet < 91) || (keyGet > 96 && keyGet < 123) || (keyGet == 9) || (keyGet == 32) || (keyGet == 8) || (keyGet == 0) || (keyGet == 13)||(keyGet==110)))
			return false;
		
		/*if(browser == "Microsoft Internet Explorer" || browser == "Mozilla Firefox") {
			console.log
		} else {
			
		}*/
		
		/*if (!((keyGet > 64 && keyGet < 91) || (keyGet > 96 && keyGet < 123) || (keyGet == 9) || (keyGet == 32) || (keyGet == 8) || (keyGet == 0) || (keyGet == 13)||(keyGet==110)))
			return false;*/
	});
});
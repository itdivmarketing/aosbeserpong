function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}


	
function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}

var reloadPage = function(){
	$('#frmSearch').submit();
};
function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


	if( $('#modal .datepicker').length > 0 ){
		$('#modal .datepicker').datepicker({ dateFormat: 'dd M yy' });
	}
	

function Validate(ParentStatus){		
		//e.preventDefault();
		//if(StudentStatus!=0)
		//	return false;

		RegistrantIDCardData=[];
		var Nationality;
		var IdentityNo;
		
		var PassportNo;
		var PassportExpDate;
		
		var KITASNo;
		var KITASExpDate;
		
		//var VisaType=$('#select-visa_type option:selected');
		var VisaNo;
		var VisaExpDate;
		
		
		if(ParentStatus=='S'){
			 Nationality=$('#ddlStudentNationality option:selected');
			 IdentityNo=$('#txtStudentIDNo');
			
			 PassportNo=$('#txtStudentPassportNo');
			 PassportExpDate=$('#txtStudentPassportExpDate');
			
			 KITASNo=$('#txtStudentKITASNo');
			 KITASExpDate=$('#txtStudentKITASExpDate');
			
			// VisaType=$('#select-visa_type option:selected');
			 VisaNo=$('#txtStudentVISANo');
			 VisaExpDate=$('#txtStudentVISAExpDate');
		
		}
		else if(ParentStatus=='F'){
			 Nationality=$('#ddlFatherNationality option:selected');
			 IdentityNo=$('#txtFatherIDNo');
			
			 PassportNo=$('#txtFatherPassportNo');
			 PassportExpDate=$('#txtFatherPassportExpDate');
			
			 KITASNo=$('#txtFatherKITASNo');
			 KITASExpDate=$('#txtFatherKITASExpDate');
			
			// VisaType=$('#select-visa_type option:selected');
			// VisaNo=$('#txtFatherVISANo');
			// VisaExpDate=$('#txtFatherVISAExpDate');
		
		}
		else if(ParentStatus=='M'){
			 Nationality=$('#ddlMotherNationality option:selected');
			 IdentityNo=$('#txtMotherIDNo');
			
			 PassportNo=$('#txtMotherPassportNo');
			 PassportExpDate=$('#txtMotherPassportExpDate');
			
			 KITASNo=$('#txtMotherKITASNo');
			 KITASExpDate=$('#txtMotherKITASExpDate');
			
			// VisaType=$('#select-visa_type option:selected');
			 //VisaNo=$('#txtMotherVISANo');
			// VisaExpDate=$('#txtMotherVISAExpDate');
		}
		
		
		

		var IsError=false;
		//$('.field-error').hide();
		// /*
		// if($.trim(IdentityNo.val())!=""&&IsError==false){
		// 	if( validateNumeric($.trim(IdentityNo.val()))==false ){
		// 		alert("Identity No Field Must be Numeric");
		// 		IsError=true;
		// 	}
		// }*/
		
		// if($.trim(PassportNo.val())!=""&&IsError==false){
		// 	if( validateNumeric($.trim(PassportNo.val()))==false ){
		// 		alert("Passport No Field Must be Numeric");
		// 		IsError=true;
		// 	}
		// }
		
		// if($.trim(KITASNo.val())!=""&&IsError==false){
		// 	if( validateNumeric($.trim(KITASNo.val()))==false ){
		// 		alert("KITAS No Field Must be Numeric");
		// 		IsError=true;
		// 	}
		// }
		
		// if(ParentStatus=='S'){
		// 	if($.trim(VisaNo.val())!=""&&IsError==false){
		// 		if( validateNumeric($.trim(VisaNo.val()))==false ){
		// 			alert("Visa No Field Must be Numeric");
		// 			IsError=true;
		// 		}
		// 	}
		// }
		
		// if(IsError==true){
		// 	alert("Data Not Yet Complete \n Data belum lengkap \n\n Please Check Again \n Mohon Periksa Ulang");
		// 	return false;
		// }

		// if($.trim(IdentityNo.val())!=""&&IsError==false){
		// 	if( validateNumeric($.trim(IdentityNo.val()))==false ){
		// 		alert("Identity No Field Must be Numeric");
		// 		IsError=true;
		// 	}
		// }
		
		// if($.trim(PassportNo.val())!=""&&IsError==false){
		// 	if( validateNumeric($.trim(PassportNo.val()))==false ){
		// 		alert("Passport No Field Must be Numeric");
		// 		IsError=true;
		// 	}
		// }
		
		// if($.trim(KITASNo.val())!=""&&IsError==false){
		// 	if( validateNumeric($.trim(KITASNo.val()))==false ){
		// 		alert("KITAS No Field Must be Numeric");
		// 		IsError=true;
		// 	}
		// }
		
		// if(ParentStatus=='S'){
		// 	if($.trim(VisaNo.val())!=""&&IsError==false){
		// 		if( validateNumeric($.trim(VisaNo.val()))==false ){
		// 			alert("Visa No Field Must be Numeric");
		// 			IsError=true;
		// 		}
		// 	}
		// }

		if(IsError==false){
			if(ParentStatus=='S')
				$('#form-StudentIDCard').submit();
			else if(ParentStatus=='F')
				$('#form-FatherIDCard').submit();
			else if(ParentStatus=='M')
				$('#form-MotherIDCard').submit();
		}		
		
	};
	

	

$(document).ready(function()
{

	$('.IDCarddatepicker').datepicker({
			dateFormat: 'dd M yy', 
			changeMonth: true,
			changeYear: true
	});
		
		
	$( ".IDCarddatepicker" ).keyup(function(e) {
		if(e.keyCode == 8 || e.keyCode == 46) {
			$.datepicker._clearDate(this);
		}
		
	});
	
	$( ".IDCarddatepicker" ).keypress(function( event ) {
		event.preventDefault();
		return false;

	});
	
	var $StudentIDCardInputs = $('#form-StudentIDCard :input');
	
	
	$StudentIDCardInputs.each(function() {
		$(this).prop('disabled', true);
	});

	
	$('#edit-student-ID_card').click(function(){
		$StudentIDCardInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		$("#ID_card-student-edit-section").addClass('hide');
		$("#ID_card-student-save-section").removeClass('hide');
	});

	$('#cancel-edit-student-ID_card').click(function(){
		reloadPage();
		$StudentIDCardInputs.each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$("#ID_card-student-edit-section").removeClass('hide');
		$("#ID_card-student-save-section").addClass('hide');
	});
	$('#save-student-ID_card').click(function(){
	Validate('S');
	});	
	
	
	var $FatherIDCardInputs = $('#form-FatherIDCard :input');
	
	
	$FatherIDCardInputs.each(function() {
		$(this).prop('disabled', true);
	});

	
	$('#edit-father-ID_card').click(function(){
		$FatherIDCardInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		$("#ID_card-father-edit-section").addClass('hide');
		$("#ID_card-father-save-section").removeClass('hide');
	});

	$('#cancel-edit-father-ID_card').click(function(){
		reloadPage();
		$FatherIDCardInputs.each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$("#ID_card-father-edit-section").removeClass('hide');
		$("#ID_card-father-save-section").addClass('hide');
	});
	$('#save-father-ID_card').click(function(){
	Validate('F');
	});		
	
	var $MotherIDCardInputs = $('#form-MotherIDCard :input');
	
	
	$MotherIDCardInputs.each(function() {
		$(this).prop('disabled', true);
	});

	
	$('#edit-mother-ID_card').click(function(){
		$MotherIDCardInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		$("#ID_card-mother-edit-section").addClass('hide');
		$("#ID_card-mother-save-section").removeClass('hide');
	});

	$('#cancel-edit-mother-ID_card').click(function(){
		reloadPage();
		$MotherIDCardInputs.each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$("#ID_card-mother-edit-section").removeClass('hide');
		$("#ID_card-mother-save-section").addClass('hide');
	});
	$('#save-mother-ID_card').click(
	function(){
	Validate('M');
	});	
});
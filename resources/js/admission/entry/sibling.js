function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}
var reloadPage = function(){
	$('#frmSearch').submit();
};

function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}

var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/sibling/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
        } 
	});
};


	function Search(jenis=0){

		var txtSearch=$.trim($('#internal-popup-sibling #txtSearchStudentName_Sibling').val());
		if(txtSearch==""){
			txtSearch=null;
		}
		if(jenis==1){
			var hfPage = 1;
		}else{
			var hfPage=$.trim($('[name=hfPage]').val());
		}
		
		$.ajax({
			type: "get",
			url: SiteURL+'entry/sibling/get_sibling_by_name_paging/'+txtSearch+'/'+hfPage,
			async:false,
			dataType: "json",
			contentType: 'application/json;charset=utf-8',
			beforeSend: function(){
				var Table=$("#TableSearchSibling");
				//var ImgLoading=$("<tr><td colspan=3><img style='width:50px;height:50px' src='<?php echo base_url();?>images/loading.gif' title='Loading...' class='loading' /></td></tr>");
				//Table.append(ImgLoading);
			},
			
			success: function(data){
					if(data.status == true) {
							TempData=new Array();
							$("#internal-popup-sibling #TableSearchSibling > tbody:last").children().remove();
							if(!data.SiblingResult){
								alert("There is No data");
								$('#internal-popup-sibling #paging').hide();
							}
							else{
									//alert("DATA EXIST");
									$('#internal-popup-sibling #paging').show();
								}
								
								
								
								var Table=$("#internal-popup-sibling #TableSearchSibling");
									for(var i in data.SiblingResult){
										var Row=$("<tr>");
										
										if(!data.SiblingResult[i].StudentID)
											data.SiblingResult[i].StudentID="-";
										if(!data.SiblingResult[i].StudentName)
											data.SiblingResult[i].StudentName="-";	
										
										var td="<td><a href='' class='selector'>select</a></td>";
										Row.append(td);
										for(var j=0;j<2;j++){
											var Text="";
											switch(j){
												case 0:Text=data.SiblingResult[i].StudentID;	
													break;
												case 1:Text=data.SiblingResult[i].StudentName;
													break;
											}
											
											Row.append( $("<td>").text(Text) );
										}
										
											
										var Data={"StudentID":data.SiblingResult[i].StudentID,"StudentName":data.SiblingResult[i].StudentName}										
										TempData.push(Data);
										Table.append(Row);
									}	

																
					} 
					else {
						alert("There is No data");
					}
					if(data.pagination){
						Show_Pagination({
							'Selector' : '#paging',
							'TotalPage' : data.pagination.last,
							'CurrentPage' : data.pagination.current,
							'PreviousPage' : data.pagination.previous,
							'NextPage' : data.pagination.next,
							'InfoPage' : data.pagination.info
							});
							
					}
				//$('[name=hfPage]').val($(this).attr('data-page'));		
			},
			complete: function(){
					$('#internal-popup-sibling .selector').on('click',function(e){
							e.preventDefault();
							var myRow = $(this).parent().parent().index();
							$('#txtCheckStudentID').val(TempData[myRow].StudentID);
							$.fancybox.close();
					});

					$('#internal-popup-sibling .page-click').on('click',function(){
						$('[name=hfPage]').val($(this).attr('data-page'));
						Search();
						
						//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
					});	
			},
			error: function(xhr, textStatus, errorThrown){
				alert('failed');
			}
		})
	
			
	}
	
	
	$("#search-sibling").live('click', function(e) {
		e.preventDefault();
		var not_enable = $('#txtCheckStudentID').prop('disabled');
		if(not_enable==1){

		}else{
			
			$.fancybox({
        	href 		: '#internal-popup-sibling',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {

    		}});
		}
		// $("#internal-popup-sibling #btnSearch").click(function(){
		// 	Search();
			
		// 	$('.prev').on('click',function(e){
		
		// 		if($(this).hasClass('disable')){
		// 			e.preventDefault();
		// 			return;
		// 		}
		// 	$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
		// 		Search();
		// 	});
		// 	$('.next').on('click',function(e){
		// 		//alert($('[name=hfPage]').val());
		// 		if($(this).hasClass('disable')){
		// 			e.preventDefault();
		// 			return;
		// 		}
		// 		$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
				
		// 		Search();
		// 	});
			
		// });
		
	});
	

$(document).ready(function()
{

	
	$( ".siblingdatepicker" ).datepicker({ 
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd M yy',
		yearRange: '-50:+0'
	});
	
	
	var $inputs = $('#form-CESibling :input');
	var CESiblingData=new Array();
	var DeletedCESiblingData=new Array();
	GetCESiblingData();
	
	
	$inputs.each(function() {
		$(this).prop('disabled', true);
	});
	

	$('#edit-CESibling').click(function(){
		$("#form-CESibling :input").each(function() {
			$(this).prop('disabled', false);
		});

	});

	$('#cancel-edit-CESibling').click(function(){
		reloadPage();
		$("#form-CESibling :input").each(function() {
			$(this).prop('disabled', true);
		});

	});
	

	var StudentStatus="";
	var CESiblingData=new Array();
	var TempData=new Array();
	var RegistrantData;

	var ActiveRow=0;
	var Checked=false;

	var StudentID=$('#student_id');
	var Name=$('#sibling_name');
	var YearLevel=$('#year_level');
	var Pathway=$('#sibling_pathway');
	var SiblingID=$('#sibling_id');
	
	$("#btnAddCESibling").addClass('hide');
	$("#btnAddOthersSibling").addClass('hide');

	$('#edit-CESibling').click(function(){
		$("#form-CESibling :input").each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		$("#TableCESibling .icon").removeClass('hide');
		$("#edit-CESibling-section").addClass('hide');
		$("#cancel-edit-CESibling-section").removeClass('hide');
		$("#btnAddCESibling").removeClass('hide');
	});

	$('#cancel-edit-CESibling').click(function(){
		reloadPage();
		$("#form-CESibling :input").each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		
		$("#TableCESibling .icon").addClass('hide');
		$("#edit-CESibling-section").removeClass('hide');
		$("#cancel-edit-CESibling-section").addClass('hide');
		$("#btnAddCESibling").addClass('hide');
	});
	



	
	$("#btnCheck").click(function(e){
		e.preventDefault();
		
		if($.trim($('#txtCheckStudentID').val())==""){
			alert("Please Input StudentID");
			return false;
		}
		
		
		
		TempData=new Array();
		$("#TableInfoSibling > tbody:last").children().remove();
		$(this).prop('disabled',true);
	
		var RegistrantID=$('[name="hfFormNumber"]').val();
		var StudentID=$('#txtCheckStudentID').val();
		$.ajax({
			type: "post",
			url:  SiteURL+'entry/sibling/check_sibling_studentID/'+RegistrantID+'/'+StudentID,
			dataType: "json",
			success: function(data){
					if(data.data) {
							//TempData=new Array();
							//$("#RegistrantSiblingInfo").hide();
							//$("#TableInfoSibling > tbody:last").children().remove();
							if(data.data.length<=0){
								alert("There is No data");
							}
							else{							
								if(data.data[0].Registered==1){
									alert("DATA SIBLING GROUP EXIST");
								}
								else if(data.data[0].StudentStatusID==1 && data.data[0].Registered==0){
									alert("DATA MsStudent EXIST");
								}
								else{
							
									alert("DATA TRREGISTRANT EXIST");
									$("#RegistrantSiblingInfo").show();
								}
								var Table=$("#TableInfoSibling");
									for(var i in data.data){
										var Row=$("<tr>");
										
										if(!data.data[i].YearLevelName)
											data.data[i].YearLevelName="-";
										if(!data.data[i].Name)
											data.data[i].Name="-";
										if(!data.data[i].PathWay)
											data.data[i].PathWay="-";
										
										for(var j=0;j<4;j++){
											var Text="";
											switch(j){
												case 0:Text=data.data[i].StudentID;	
													break;
												case 1:Text=data.data[i].Name;
													break;
												case 2:Text=data.data[i].YearLevelName;	
													break;
												case 3:Text=data.data[i].PathWay;
													break;
											}
											
											Row.append( $("<td>").text(Text) );
											
							
										}
										
											
										var Data={"SiblingID":data.data[i].SiblingID,"StudentID":data.data[i].StudentID,"Name":data.data[i].Name,
										"YearLevel":data.data[i].YearLevelName,"Pathway":data.data[i].PathWay,"StudentStatusID":data.data[i].StudentStatusID,"Registered":data.data[i].Registered
										,"SelfID":data.data[i].SelfID,"SoloSiblingID":data.data[i].SoloSiblingID};
										
										TempData.push(Data);
										Table.append(Row);
									}
									
								Checked=true;
							}		
						
					} 
					else {
					
						alert("failed");
					}
					
					$("#btnCheck").prop('disabled',false);
			},
			error: function(xhr, textStatus, errorThrown){
				alert('failed');
				$("#btnCheck").prop('disabled',false);
			}
		})
	
			
	});
	
	  $("#txtCheckStudentID").on('change', function(e) {
			Checked=false;
	 });
	
	$("#btnAddCESibling").click(function(e){
		e.preventDefault();
		//alert("asd");
		
		
		
		if(Checked==false){
			alert("Data is not valid, Please Press Check Button first");
			return false;
		}
		
		
		
			
		//var Data={"SiblingID":SiblingID.val(),"StudentID":StudentID.val(),"Name":Name.text(),"YearLevel":YearLevel.text(),"Pathway":Pathway.text()};
		if(CheckSiblingIDSameness()==false)
				return false;
				
		for(var i=0;i<TempData.length;i++){
			if(CheckStudentIDUniqueness(TempData[i].StudentID)==false){
				alert("StudentID: "+TempData[i].StudentID+" is already Listed");
				return false;
			}
		}
		
		for(var i=0;i<TempData.length;i++){
			//alert(TempData[i].SelfID);
			if(TempData[i].SelfID==1){
			alert("Cannot Add Self ID");
			return false;
			}
		}
	
		//alert("tempdata studentid: "+TempData[0].StudentID);
		for(var i=0;i<TempData.length;i++){
			//alert(TempData[i].SelfID);
			if(TempData[i].SelfID!=1){
			CESiblingData.push(TempData[i]) ;
			}
		}
		
		//alert(CESiblingData[0].StudentID);
				

		for(var i=0;i<TempData.length;i++){
			var row = $("<tr>");
			var icon="<td class='IconColumn'><i class='icon icon-trash'></i></td>";	
			row.append(icon);
			for(var j=0;j<5;j++){
				var Text="";
				switch(j){
					case 0:Text=TempData[i].StudentID;
						break;
					case 1:Text=TempData[i].Name;
						break;
					case 2:Text=TempData[i].YearLevel;
						break;
					case 3:Text=TempData[i].Pathway;
						break;
					case 4:Text=TempData[i].StudentStatusID==1?"Yes":"No";
						break;
				}

				row.append( $("<td>").text(Text) );
					

			}
			if(TempData[i].SelfID!=1)
				$("#TableCESibling").append(row);
		}
	
		

	});
	
	function ValidateSaveCurrentSibling(){
		if(CESiblingData.length<=0){
			alert("No Data List to be Saved");
			return false;
			}
		if(CheckSiblingIDSameness()==false){
			alert("All SiblingID in the List are not Same");
			return false;
			}
			
		return true;
	}
		
	function CheckSiblingIDSameness(){
		var TempDataStudentStatus=0;
		var CEDataStudentStatus=0;
		for(var i=0;i<TempData.length;i++){
			if(TempData[i].StudentStatusID==1){
				TempDataStudentStatus=1;
			}
		}
		
		for(var i=0;i<CESiblingData.length;i++){
			if(CESiblingData[i].StudentStatusID==1){
				CEDataStudentStatus=1;
			}
		}
		
		if(TempDataStudentStatus==1&&CEDataStudentStatus==1){
			var val=confirm("This Sibling Group have different SiblingID with SiblingID That Exist in the list..Press Ok to Replace The old Student with the new list");
							if(val){
									//var SiblingID=CESiblingData[myRow].SiblingID;
									//alert(ArrayLength);
										for(var j=0;j<CESiblingData.length;j++){
											if(CESiblingData[j].StudentStatusID==1){
												CESiblingData.splice(j,1);
												$("#TableCESibling tr:eq("+(j+1)+")").remove();
												j=j-1;
											}
										}
								return true;
							}
							return false;
		
		
		}

		return true;
		/*
		for(var i=0;i<CESiblingData.length;i++){
			if(CESiblingData[i].StudentStatusID==1){
				//if(TempData[0].Registered==1){
					//if(TempData[0].SiblingID!=CESiblingData[i].SiblingID){
							var val=confirm("This Sibling Group have different SiblingID with SiblingID That Exist in the list..Press Ok to Replace The old Student with the new list");
							if(val){
									//var SiblingID=CESiblingData[myRow].SiblingID;
									//alert(ArrayLength);
										for(var j=0;j<CESiblingData.length;j++){
											if(CESiblingData[j].StudentStatusID==1){
												CESiblingData.splice(j,1);
												$("#TableCESibling tr:eq("+(j+1)+")").remove();
												j=j-1;
											}
										}
								return true;
							}
							return false;
					//}
				//}
					
			}
		}

		return true;
		*/
	}
	
	function CheckStudentIDSiblingSameness(StudentID){
		for(var i=0;i<CESiblingData.length;i++){
			if(CESiblingData[i].StudentID==StudentID)
				return false;
		}

		return true;
	}
	
	function CheckStudentIDUniqueness(StudentID){
		for(var i=0;i<CESiblingData.length;i++){
			if(CESiblingData[i].StudentID==StudentID&&CESiblingData[i].SelfID!=1)
				return false;
		}

		return true;
	}

		
	$("#btnSaveCESibling").click(function(e){
		
		e.preventDefault();
//if(StudentStatus!=0)
		//	return false;
		var SelfData;
		
		for(var i=0;i<CESiblingData.length;i++){
				if(CESiblingData[i].SelfID==1){
					SelfData=CESiblingData[i];
					break;					
				}
		}
		

		var HaveSiblingID=0;
		var SiblingID="";
		var SavedData=new Array();
		var HasStudent=false;
		var HasSiblingID=false;
		var SoloSiblingID=false;
		
		//tree 1- ada student
		for(var i=0;i<CESiblingData.length;i++){
			if(CESiblingData[i].StudentStatusID==1){
				HaveSiblingID=1;
				SiblingID=CESiblingData[i].SiblingID;
				HasStudent=true;
				break;
				}
		}
		
		if(HasStudent==false){
				if(SelfData.Registered==1){
					//Punya SiblingID
						var SameCounter=0;
						for(var i=0;i<CESiblingData.length;i++){
							if(SelfData.SiblingID==CESiblingData[i].SiblingID){
								SameCounter++;
								break;
							}
						}
						
						if(SameCounter==1){
							//Cuma si registrant 
							if(SelfData.SoloSiblingID ==1){
								HaveSiblingID=1;
								SiblingID=SelfData.SiblingID;
							}
							else{
								//alert("hhh");
								HaveSiblingID=0;
								SiblingID="";
							}
						}
						else{
							//ada kaka2 lain
							HaveSiblingID=0;
							SiblingID="";
						}
							
				}
				else{
					//Tidak Punya SiblingID
					HaveSiblingID=0;
					SiblingID="";
				}
		}
		
		
		for(var i=0;i<CESiblingData.length;i++){
			if(CESiblingData[i].StudentStatusID==0){
				SavedData.push(CESiblingData[i]);
				}
		}
		
		
		var NewDeletedCESiblingData=sortByKey(DeletedCESiblingData,"SiblingID");
		for(var i=0;i<NewDeletedCESiblingData.length;i++){
			$('#form-CESibling').append('<input type="hidden" name="hfDeletedSiblingID[]" value="'+NewDeletedCESiblingData[i].SiblingID+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfDeletedStudentID[]" value="'+NewDeletedCESiblingData[i].StudentID+'" />');
		}
	
		for(var i=0;i<SavedData.length;i++){

			$('#form-CESibling').append('<input type="hidden" name="hfSiblingID[]" value="'+SavedData[i].SiblingID+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfStudentID[]" value="'+SavedData[i].StudentID+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfName[]" value="'+SavedData[i].Name+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfYearLevel[]" value="'+SavedData[i].YearLevel+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfPathway[]" value="'+SavedData[i].Pathway+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfStudentStatusID[]" value="'+SavedData[i].StudentStatusID+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfRegistered[]" value="'+SavedData[i].Registered+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfSelfID[]" value="'+SavedData[i].SelfID+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfSoloSiblingID[]" value="'+SavedData[i].SoloSiblingID+'" />');
		}
			$('#form-CESibling').append('<input type="hidden" name="hfHaveSiblingID" value="'+HaveSiblingID+'" />');
			$('#form-CESibling').append('<input type="hidden" name="hfNewSiblingID" value="'+SiblingID+'" />');
			
			$('#form-CESibling').submit();
		//alert(HaveSiblingID);
		/*
		$.ajax({
			type: "post",
			url:  '<?php echo site_url('StudentInformation_sibling/save_current_enrolled_sibling'); ?>',
			data: {
				   asd:SavedData,
				   HaveSiblingID:HaveSiblingID,
				   SiblingID:SiblingID
				   },
			dataType: "json",
			success: function(data){
		
					if(data.status == 'success') {
						alert("success");
							window.location.replace("<?php echo site_url('StudentInformation_sibling/Others'); ?>");
					} else {
						alert("galau");
					}
			},
			error: function(xhr, textStatus, errorThrown){
				alert('failed');
			}
		})*/
	
		
		
	})
		
function sortByKey(array, key) {
	return array.sort(function(a, b) {
    var x = a[key]; var y = b[key];
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
});
}

function GetCESiblingData(){	

		var Param=$('[name="hfFormNumber"]').val();
		$.ajax({
			type: "post",
			url: SiteURL+'entry/sibling/get_CE_sibling_data/'+Param,
			dataType: "json",
			success: function(data){

					if(data.CESiblingData)
					{
						for(var i in data.CESiblingData)
						{
							var InitialData={
							"SiblingID":data.CESiblingData[i].SiblingID,
							"StudentID":data.CESiblingData[i].StudentID,
							"Name":data.CESiblingData[i].Name,
							"YearLevel":data.CESiblingData[i].YearLevel,
							"Pathway":data.CESiblingData[i].Pathway,
							"StudentStatusID":data.CESiblingData[i].StudentStatusID,
							"Registered":data.CESiblingData[i].Registered,
							"SelfID":data.CESiblingData[i].SelfID,
							"SoloSiblingID":data.CESiblingData[i].SoloSiblingID
							};

							CESiblingData.push(InitialData) ;
		
						}
					}
				
				}
		})
}

function GetOthersSiblingData(){	

		var Param=$('[name="hfFormNumber"]').val();
		$.ajax({
			type: "post",
			url: SiteURL+'entry/sibling/get_others_sibling_data/'+Param,
			dataType: "json",
			success: function(data){
					if(data.OthersSiblingData)
					{
						for(var i in data.OthersSiblingData)
						{
							var InitialData={
							"Name":data.OthersSiblingData[i].PotentialSiblingName,
							"BirthDate":data.OthersSiblingData[i].Birthday,
							"SchoolLevelID":data.OthersSiblingData[i].SchoolLevelID,
							"SchoolLevel":data.OthersSiblingData[i].SchoolLevelName,
							"YearLevelID":data.OthersSiblingData[i].YearLevelID,
							"YearLevel":data.OthersSiblingData[i].YearLevelName,
							"AdmissionPeriod":data.OthersSiblingData[i].AdmissionPeriod=="null"?"":data.OthersSiblingData[i].AdmissionPeriod
							};

							OthersSiblingData.push(InitialData) ;
		
						}
					}
				
				}
		})
}



$("#TableCESibling").on('click',".IconColumn", function(e) {
	
	//debugger;
	var myRow = $(this).parent().index();
	//alert(myRow);
	var target= $(e.target);
	if(target.is('.icon-trash')){
		//alert("zxc");
		//alert(CESiblingData.length);
		//alert(CESiblingData[myRow].StudentID);
		if(CESiblingData[myRow].Registered==1){
				if(CESiblingData[myRow].StudentStatusID==1){
					var Confirm=confirm("This Action Will Delete all Active Student in this SiblingGroup");
					if(Confirm){
						var ArrayLength=CESiblingData.length;
						var SiblingID=CESiblingData[myRow].SiblingID;
					//alert(ArrayLength);
						for(var i=0;i<CESiblingData.length;i++){
							if(CESiblingData[i].SiblingID==SiblingID&&CESiblingData[i].SelfID!=1&&CESiblingData[i].StudentStatusID==1){
								//DeletedCESiblingData.push(CESiblingData[i]);
								CESiblingData.splice(i,1);
								$("#TableCESibling tr:eq("+(i+1)+")").remove();
								i=i-1;
							}
						}
					}
				}
				else{
					DeletedCESiblingData.push(CESiblingData[myRow]);
					CESiblingData.splice(myRow,1) ;
					$(this).parent().remove();

				}				
				
				
				/*
			var Confirm=confirm("Are you Want");
			if(Confirm){
			
			var ArrayLength=CESiblingData.length;
			var SiblingID=CESiblingData[myRow].SiblingID;
			//alert(ArrayLength);
				for(var i=0;i<CESiblingData.length;i++){
					
					if(CESiblingData[i].SiblingID==SiblingID&&CESiblingData[i].SelfID!=1){
					//alert(i);
					
					CESiblingData.splice(i,1);
					$("#TableCESibling tr:eq("+(i+1)+")").remove();
					i=i-1;
					}
				}
			}*/
				
		}
		else{
		//alert("zxc");
		CESiblingData.splice(myRow,1) ;
		$(this).parent().remove();
		}
		//OnUpdate=false;
	}	
	if(target.is('.icon-edit')){
	//OnUpdate=true;

	ActiveRow=myRow;
	
	StudentID.val(CESiblingData[myRow].StudentID);
	Name.text(CESiblingData[myRow].Name);
	YearLevel.text(CESiblingData[myRow].YearLevel);
	Pathway.text(CESiblingData[myRow].Pathway);
	
	}

});




function validateNumeric($text) {
		  var Reg = /^\d+$/;
		  if( !Reg.test( $text ) ) {
			return false;
		  } else {
			return true;
		  }
	}
	
	
	$("#TableOthersSibling").on('click', '.IconColumn', function(e) {
	
	//debugger;
	var myRow = $(this).parent().index();
	var target= $(e.target);
	if(target.is('.icon-trash')){
	$(this).parent().remove();
	//alert(myRow);
	OthersSiblingData.splice(myRow,1) ;
	OnUpdate=false;
	$('#btnAddOthersSibling').text("Add New");
	}
	
	if(target.is('.icon-edit')){
	OnUpdate=true;
	$('#btnAddOthersSibling').text("Edit");
	ActiveRow=myRow;
	
	$('#txtNameOthersSibling').val(OthersSiblingData[myRow].Name);
	$('#txtDOBOthersSibling').val(OthersSiblingData[myRow].BirthDate);
	$('#txtAdmissionPeriodOthersSibling').val(OthersSiblingData[myRow].AdmissionPeriod);
	$('#ddlSchoolLevel').val(OthersSiblingData[myRow].SchoolLevelID);
	loadYearLevel($('#ddlSchoolLevel').val(),"ddlYearLevel");
	$('#ddlYearLevel').val(OthersSiblingData[myRow].YearLevelID);
	}
	
	});
	
	
	$("#btnAddOthersSibling").click(function(e){
		e.preventDefault();
			
		var IsError=false;
		$(".field-error").hide();
		
		var Name=$("#txtNameOthersSibling");
		var BirthDate=$("#txtDOBOthersSibling");
		var YearLevel=$("#ddlYearLevel");
		var SchoolLevel=$("#ddlSchoolLevel");
		var AdmissionPeriod=$("#txtAdmissionPeriodOthersSibling");

		if($.trim($("#txtNameOthersSibling" ).val())==""){
			alert("Name Field is Required");
				IsError=true;
		}
			
		else if($.trim($("#txtDOBOthersSibling" ).val())==""){
			alert("Date OF Birth Field is Required");
			IsError=true;
		}
		
		else if($.trim($("#ddlYearLevel" ).val())==""){
			alert("Year Level Field is Required");
			IsError=true;
		}
		
		else if($.trim($("#ddlSchoolLevel" ).val())==""){
			alert("School Level Field is Required");
			IsError=true;
		}
		else if($.trim($("#txtAdmissionPeriodOthersSibling" ).val())!=""&&validateNumeric($.trim($("#txtAdmissionPeriodOthersSibling" ).val()))==false){
			alert("Admission Period Must be Numeric");
			IsError=true;
		}
		else if($.trim($("#txtAdmissionPeriodOthersSibling" ).val()).length!=4){
			alert("Admission Period Must be 4 Chars long");
			IsError=true;
		}
		
		var Data={"Name":$.trim(Name.val()),"BirthDate":BirthDate.val(),"YearLevelID":YearLevel.val(),
				"YearLevel":YearLevel.text(),"SchoolLevelID":SchoolLevel.val(),"SchoolLevel":SchoolLevel.text(),
				"AdmissionPeriod":$.trim(AdmissionPeriod.val())
				};
		
		if(OnUpdate==false)	{	
			for(var i=0;i<OthersSiblingData.length;i++){
				if(OthersSiblingData[i].Name==Data.Name
					&& OthersSiblingData[i].BirthDate==Data.BirthDate
					&& OthersSiblingData[i].YearLevelID==Data.YearLevelID
					&& OthersSiblingData[i].SchoolLevelID==Data.SchoolLevelID
					&& OthersSiblingData[i].AdmissionPeriod==Data.AdmissionPeriod
				)
				{
					alert("Siblings Data Can not be same entirely");
					return false;
					break;
				}
			}
		}
		
		if(IsError==true){
			alert("Data Not Yet Complete \n Data belum lengkap \n\n Please Check Again \n Mohon Periksa Ulang");
			return false;
		}
		
			
			
		
		

		if(OnUpdate==false){
		OthersSiblingData.push(Data) ;
		}
		else
		OthersSiblingData[ActiveRow]=Data ;
		
				//alert(OthersSiblingData[0].StudentID);
				
		var row = $("<tr>");
		var icon="<td class='IconColumn'><i class='icon icon-trash'></i>&nbsp;<i class='icon icon-edit'></i></td>";	
		row.append(icon);
			
		for(var i=0;i<5;i++){
			var Text="";
			switch(i){
				case 0:Text=$.trim(Name.val());
					break;
				case 1:Text=BirthDate.val();
					break;
				case 2:Text=$('#ddlSchoolLevel option:selected').text();
					break;
				case 3:Text=$('#ddlYearLevel option:selected').text();
					break;
				case 4:Text=$.trim(AdmissionPeriod.val());
					break;
			}
			if(OnUpdate==false){
				
				row.append( $("<td>").text(Text) );
				
			}else{
				$('#TableOthersSibling tr').eq(ActiveRow+1).find('td').eq(i+1).html(Text);
			}
		}
		// alert(row);
		if(OnUpdate==false)
			$("#TableOthersSibling").append(row);

		OnUpdate=false;
		$('#btnAddOthersSibling').text("Add New");
	});
	
	function ValidateSaveOthersSibling(){
		if(OthersSiblingData.length<=0){
			alert("No data To Saved");
			return false;
			}
			
		return true;
	}
	
	$("#btnSaveOthersSibling").click(function(e){
	
		e.preventDefault();
			//if(StudentStatus!=0)
			//return false;

	
		/*
		if(ValidateSaveOthersSibling()==false){
			return false;
			}
		*/
		
		OnUpdate=false;
		$('#btnAddOthersSibling').text("Add New");
		
		for(var i=0;i<OthersSiblingData.length;i++){

		$('#form-OthersSibling').append('<input type="hidden" name="hfName[]" value="'+OthersSiblingData[i].Name+'" />');
		$('#form-OthersSibling').append('<input type="hidden" name="hfBirthDate[]" value="'+OthersSiblingData[i].BirthDate+'" />');
		$('#form-OthersSibling').append('<input type="hidden" name="hfYearLevelID[]" value="'+OthersSiblingData[i].YearLevelID+'" />');
		$('#form-OthersSibling').append('<input type="hidden" name="hfSchoolLevelID[]" value="'+OthersSiblingData[i].SchoolLevelID+'" />');
		$('#form-OthersSibling').append('<input type="hidden" name="hfAdmissionPeriod[]" value="'+OthersSiblingData[i].AdmissionPeriod+'" />');
		}

		
		$('#form-OthersSibling').submit();
/*
		$.ajax({
			type: "post",
			url:  '<?php echo site_url('StudentInformation_sibling/save_others_sibling'); ?>',
			data: {
				   Data:RegistrantSiblingData
				   },
			dataType: "json",
			success: function(data){
					if(data.status == 'success') {
						alert("success");
						window.location.replace("<?php echo site_url('StudentInformation_PersonalDevelopment/sickness'); ?>");
					} else {
						alert("failed");
					}
			},
			error: function(xhr, textStatus, errorThrown){
				alert('failed');
			}
	
		})
	
		*/
		
	})
	
	//var StudentStatus="<?php echo $StudentStatus; ?>";
	var OthersSiblingData=new Array();
	//var RegistrantData;
	var OnUpdate=false;
	var ActiveRow=0;
	var $OtherSiblingInputs = $('#form-OthersSibling :input');

	GetOthersSiblingData();
	
	$OtherSiblingInputs.each(function() {
		$(this).prop('disabled', true);
	});
	

	
	$('[name="ddlSchoolLevel"]').change(function(){
		loadYearLevel($(this).val(),"ddlYearLevel");
	});
	
	$('#edit-OtherSibling').click(function(){
		$("#form-OthersSibling :input").each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		
		$("#TableOthersSibling .icon").removeClass('hide');
		$("#OtherSibling-edit-section").addClass('hide');
		$("#OtherSibling-save-section").removeClass('hide');
		$("#btnAddOthersSibling").removeClass('hide');
	});

	$('#cancel-edit-OtherSibling').click(function(){
		reloadPage();
		$("#form-OthersSibling :input").each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		
		$("#TableOthersSibling .icon").addClass('hide');
		$("#OtherSibling-save-section").addClass('hide');
		$("#OtherSibling-edit-section").removeClass('hide');
		$("#btnAddOthersSibling").addClass('hide');
		
		OnUpdate=false;
		$('btnAddOthersSibling').text("Add New");
	});
	
	
});
		
	
						
	

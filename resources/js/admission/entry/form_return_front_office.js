function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}

var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change")
        } 
	});
};


var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
			$('[name="'+ObjectName+'"]').trigger("change");
        } 
	});
};

var getAdmissionID = function(TermID){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_admissionID/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};


var getFormAmount = function(FormNumber){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_form_amount/'+FormNumber,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FormSoldView)
			{
				
				for(var i in data.FormAmount)
				{ 
					$('[name="txtViewAmount"]').val( data.FormAmount[i].Amount);
				}
			}
        } 
	});
};

var loadPaymentMethod = function(){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_payment_method/',
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlViewPaymentMethod"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PaymentMethod)
			{
				for(var i in data.PaymentMethod)
				{
				
					$('[name="ddlViewPaymentMethod"]').append($('<option value="'+$.trim(data.PaymentMethod[i].PaymentMethodID)+'">'+$.trim(data.PaymentMethod[i].PaymentMethodName)+'</option>'));
				}
			}
        } 
	});
};

var generatePassword = function(method,FormNumber){
	
	$.ajax({
		url: SiteURL+'entry/form_sales/generate_password/'+method+'/'+FormNumber,
		type: 'post',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
		
			if(data.GeneratedPassword)
			{
				$('[name="txtViewPassword"]').val(data.GeneratedPassword);
			}
        } 
	});
};


var validate = function() {

	if($('.fancybox-opened [name="txtViewFormNumber"]').val() == '')
		alert('Form Number is required');
	else{
		checkForm($.trim($('.fancybox-opened [name="txtViewFormNumber"]').val()),myCallbackSave);
	}
		
		

}




function myCallbackSave(Valid) {
	//myCallbackCheck(Valid);
	if(Valid==1){
		
		if($.trim($('.fancybox-opened [name="txtViewAdmissionID"]').val()) == '')
			alert('AdmissionID is required');
		else if($.trim($('.fancybox-opened [name="txtViewAdmissionSemester"]').val()) == '')
			alert('Admission Semester is required');
		else if($.trim($('.fancybox-opened [name="txtViewSchoolLevel"]').val()) == '')
			alert('School Level is required');
		else if($.trim($('.fancybox-opened [name="txtViewAdmissionTerm"]').val())== '')
			alert('Admission Term is required');
		else if($.trim($('.fancybox-opened [name="txtViewFormNumber"]').val() )== '')
			alert('Form Number is required');
		else if($.trim($('.fancybox-opened [name="txtViewFirstName"]').val()) == '')
			alert('First name is required');
		else if($.trim($('.fancybox-opened [name="txtViewDateOfBirth"]').val()) == '')
			alert('Date Of Birth is required');
		else if($.trim($('.fancybox-opened [name="txtViewPlaceOfBirth"]').val()) == '')
			alert('Place Of Birth is required');	
		else if($.trim($('.fancybox-opened [name="ddlViewGender"]').val()) == '')
			alert('Gender is required');
		else if($.trim($('.fancybox-opened [name="ddlViewChildStatus"]').val()) == '')
			alert('Child Status is required');
		else if($.trim($('.fancybox-opened [name="ddlViewTypeOfRegistration"]').val()) == '')
			alert('Type Of Registration is required');
		else if($.trim($('.fancybox-opened [name="ddlViewToYearLevel"]').val()) == '')
				alert('Year Level is required');
		else if($.trim($('.fancybox-opened [name="ddlViewToSemester"]').val()) == '')
				alert('To Semester is required');
		else if($.trim($('.fancybox-opened [name="txtViewDateOfEntry"]').val() )== '')
				alert('Date entry is required');
		else if($.trim($('.fancybox-opened [name="txtViewDateOfFormReceipt"]').val()) == '')
				alert('Date Of Form Receipt is required');
	
		else
		{	
			$('.fancybox-opened [name="frmAdd"]').submit();
		}
	}
}

function myCallbackCheck(Valid) {
	switch(Valid){
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;

	}
}

var checkForm = function(FormNumber,callback) {

	if($.trim(FormNumber)==""){
		alert("Please Input Form Number");
		return false;
	}
	var Valid=2;
	var AjaxData;
	$.ajax({
		url: SiteURL+'entry/form_return_front_office/check_form_validation/'+FormNumber,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FormStatus){
			AjaxData=data;
			
				if(data.FormStatus.FormStatus=="1"){
						if(callback==""){
						alert("This FormNumber is Valid");
						}
					Valid=1;
					}
				else if(data.FormStatus.FormStatus=="0"){
					alert("This FormNumber has not Been Purchased");
					Valid=0;
					}
				else{
					alert("Form Number doesn't Exist");
					Valid=2;
					}
			}
        },
		complete:function(){
			switch(Valid){
				case 0:
					break;
				case 1:
					$('[name="txtViewSchoolLevel"]').val(AjaxData.FormStatus.SchoolLevelID);
					$('[name="txtViewAdmissionTerm"]').val(AjaxData.FormStatus.TermID);
					$('[name="txtViewAdmissionID"]').val(AjaxData.FormStatus.AdmissionID);
					break;
				case 2:
					break;

			}
				if(callback!=""){
					
					callback(Valid);
				}
		
		}
	});
}

var add= function(){
	var row = $(this).parents('tr');

	create_modal( 'id', '#internal-popup' );
	$('#modal').hide();
		$('[name="ddlViewSchoolLevel"]').prop("disabled", false); 
		$('[name="ddlViewAdmissionTerm"]').prop("disabled", false); 
		$('[name="ddlViewYearLevel"]').prop("disabled", false); 
		$('[name="ddlViewGender"]').prop("disabled", false); 
		$('[name="ddlViewChildStatus"]').prop("disabled", false); 
		$('[name="ddlViewTypeOfRegistration"]').prop("disabled", false); 
		$('[name="ddlViewToYearLevel"]').prop("disabled", false); 
		$('[name="ddlViewToSemester"]').prop("disabled", false); 


	$('#modal #btnCheck').on('click',
		function(){
			var Valid=checkForm($.trim($('#modal [name="txtViewFormNumber"]').val()),"");
		}
	);
	

	
	$('[name="ddlViewSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
		loadYearLevel($(this).val(),"ddlViewYearLevel");
	});
	
	$('[name="ddlViewAdmissionTerm"]').change(function(){
		getAdmissionID($(this).val());
	});
				
	$('#modal #btnSave').click(validate);		
	$('#modal').show();
};


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'entry/form_return_front_office/get_form_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
			
			$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
        	afterShow: function () {

    		},
    		afterClose: function(){

    		}
    });
        },
		complete:function(){
			
			if(AjaxData.FormData)
			{
				
				var Form=$('#modal #frmAdd');

				var $inputs = $('#modal :input');
				
				$('[name="txtViewFormNumber"]').prop('readonly',"readonly");
				/*
				$inputs.each(function() {
					$(this).prop('readonly', true);
				});
				
				$('[name="ddlViewSchoolLevel"]').prop("disabled", true); 
				$('[name="ddlViewAdmissionTerm"]').prop("disabled", true); 
				$('[name="ddlViewYearLevel"]').prop("disabled", true); 
				
				$('[name="ddlViewGender"]').prop("disabled", true); 
				$('[name="ddlViewChildStatus"]').prop("disabled", true); 
				$('[name="ddlViewTypeOfRegistration"]').prop("disabled", true); 
				$('[name="ddlViewToYearLevel"]').prop("disabled", true); 
				$('[name="ddlViewToSemester"]').prop("disabled", true); 
				*/
				
					var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
				   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
				   
				 
					var dt = new Date();
					var selectedMonthName =months[dt.getMonth()];
					var date= dt.getDate() + " " + selectedMonthName + " " + dt.getFullYear();
					var time = dt.getHours() + ":" + dt.getMinutes();
			
					var DateTime=date+" "+time;
		
				
				for(var i in AjaxData.FormData)
				{
					
				
					$('[name="txtViewAdmissionID"]').val(AjaxData.FormData[i].AdmissionID);
					$('[name="txtViewAdmissionYear"]').val(AjaxData.FormData[i].AcademicYear);
					$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormData[i].SmtId);
					$('[name="txtViewSchoolLevel"]').val(AjaxData.FormData[i].SchoolLevelID);
					$('[name="txtViewAdmissionTerm"]').val(AjaxData.FormData[i].TermID);
					
					//loadPretestTerm(AjaxData.FormData[i].SchoolLevelID,"ddlViewAdmissionTerm");
					//loadYearLevel(AjaxData.FormData[i].SchoolLevelID,"ddlViewYearLevel");
					//loadPaymentMethod();
					//$('[name="ddlViewAdmissionTerm"]').val(AjaxData.FormData[i].TermID);
					//$('[name="ddlViewYearLevel"]').val(AjaxData.FormData[i].YearLevelID);
					loadYearLevel($('[name="txtViewSchoolLevel"]').val(),"ddlViewToYearLevel");
					$('[name="txtViewFormNumber"]').val(AjaxData.FormData[i].RegistrantId);
					$('[name="txtViewFirstName"]').val(AjaxData.FormData[i].FirstName);
					$('[name="txtViewMiddleName"]').val(AjaxData.FormData[i].MiddleName);
					$('[name="txtViewLastName"]').val(AjaxData.FormData[i].LastName);
					
					$('[name="txtViewDateOfBirth"]').val(AjaxData.FormData[i].DOB);
					$('[name="txtViewPlaceOfBirth"]').val(AjaxData.FormData[i].POB);
					
					$('[name="ddlViewGender"]').val(AjaxData.FormData[i].GenderID).trigger("change");

					$('[name="ddlViewChildStatus"]').val(AjaxData.FormData[i].ChildStatusID).trigger("change");
					$('[name="ddlViewTypeOfRegistration"]').val(AjaxData.FormData[i].transfer).trigger("change");
					$('[name="ddlViewToYearLevel"]').val(AjaxData.FormData[i].YearLevelID).trigger("change");
					$('[name="ddlViewToSemester"]').val(AjaxData.FormData[i].ToSemester).trigger("change");
					
					$('[name="txtViewDateOfEntry"]').val(AjaxData.FormData[i].DateOfEntry);
					$('[name="txtViewDateOfFormReceipt"]').val(AjaxData.FormData[i].DateOfApplReceived);
					
					if(AjaxData.FormData[i].DateOfEntry==null){
					$('[name="txtViewDateOfEntry"]').val(DateTime);
					}
					
					if(AjaxData.FormData[i].DateOfApplReceived==null){
					$('[name="txtViewDateOfFormReceipt"]').val(DateTime);
					}
					var d = new Date();
					var y= d.getFullYear(); 
					var m = d.getMonth(); 
					if(AjaxData.FormData[i].JoinedYear==null||AjaxData.FormData[i].JoinedYear=="")
					$('[name="ddlViewJoinedYear"]').val(y).trigger("change");
						else
					$('[name="ddlViewJoinedYear"]').val(AjaxData.FormData[i].JoinedYear).trigger("change");
					
					if(AjaxData.FormData[i].JoinedMonth==null||AjaxData.FormData[i].JoinedMonth=="")
					$('[name="ddlViewJoinedMonth"]').val(m).trigger("change");
						else
					$('[name="ddlViewJoinedMonth"]').val(AjaxData.FormData[i].JoinedMonth).trigger("change");	
					
					
					
				}
			} 
			else
			{
			
			}
			//$('[name="txtViewAdmissionSemester"]').val($('[name="hfViewAdmissionSemester"]').val());
			//$('#modal #btnSave').hide();
			//$('#modal #btnCheck').hide();
			
			
			$('#btnCheck').hide();
			
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			
			// $('#modal #btnSave').click(validate);
			//$('#modal #btnSave').click(validate);
			
			
			//alert("werwer");
		}
	});
	
};

$(document).ready(function()
{

	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlAdmissionTerm");
		loadYearLevel($(this).val(),"ddlYearLevel");
	});
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val("");
		$('[name=frmSearch]').submit();
	});

	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
$('.prev').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
                              $('[name=frmSearch]').submit();
               }
});
$('.next').on('click',function(){
               if($(this).hasClass("disable") == false)
               {
                              $('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
                              $('[name=frmSearch]').submit();
               }
});

	
	$('#tblView').on('click','.btnView',view);
	
	$('#btnAdd').on('click',add);
});
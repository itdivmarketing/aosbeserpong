function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}




function ValidateFile(file){

	var ErrText="";
	
		if(!(file && file.length > 0)) {
			ErrText='Please Select File';
		}
		
		
		if(file && file.length > 0) {
			file = file[0];
			var ext = file.name.split(/\./).pop().toLowerCase();
			//alert(file.type);
			/*
			if(file.type != 'image/jpeg' && file.type != 'image/png' && file.type != 'application/pdf'
			&& file.type != 'application/x-rar-compressed' && file.type != 'application/zip') 
			{
				ErrText='Yor file is not in jpg, jpeg, png, rar, zip, 7z or pdf format';
			}
			*/
			
			if(ext != 'jpg'  && ext != 'png' && ext != 'jpeg' && ext != 'pdf' && ext != 'rar'  && ext != 'zip'   ) {
				ErrText='Yor file is not in jpg, jpeg, png, rar, zip or pdf format';
			}
			
			else if(file.size > 6 * 1024 * 1024) {
				ErrText='Your file size can not be bigger than 6MB';
			}
		} 
		
	
		return ErrText;
	}

function save(){
	var row = $(this).parents('tr');
	
	
	if($('#rbtUploadFile').prop('checked')){ 

		var file = $('.fancybox-opened [name="file"]').prop('files');
		
		
		var FileValid=ValidateFile(file);
		if(FileValid!=""){
			alert(FileValid);
			return;
		}
	}
	
	
	if($('#rbtPending').prop('checked')){ 

		var txtDate = $('.fancybox-opened #txtDateLine').val();
		
		if($.trim(txtDate)=="")
		{
			alert("Please Input Date");
			return;
		}
	}
	
	$('.fancybox-opened [name="frmUpload"]').submit();		

};


var view = function(){
		$("#modal-file_title").html($(this).attr("filename"));
		$("#popup-image").attr("src",$(this).attr("source"));
		$.fancybox({
        	href 		: '#view-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
        	afterShow: function () {
        		
    		},
    		afterClose: function(){

    		}});
};

var upload = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	var DocumentName=$.trim(row.find("td:nth-child(4)").text());
	var DocumentID=$.trim(row.find("td:nth-child(5)").text());
	var SubmissionID=$.trim(row.find("td:nth-child(6)").text());
	var FileName=$.trim(row.find("td:nth-child(9)").text());
	
	
	//alert($('#modal [name="hfAdmissionProcess"]').val());
	$('#modal-document_title').text(DocumentName);
	
	$('[name="hfDocumentID"]').val(DocumentID);
	$('[name="hfFileName"]').val(FileName);
	$('[name="hfSubmissionID"]').val(SubmissionID);

	
	$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
        	beforeShow: function () {
        		$("[name=frmUpload]").trigger('reset');
        		//Default
        		$("#rbtUploadFile").prop( "checked", true );
    		},
    		beforeClose: function(){

    }});
	
	$('#p-file').show();
	$('#p-DateLine').hide();
	$('#p-txtDate').show();
	$('#divKeterangan').show();
	
	$('#rbtUploadFile').change(function(){
		
		var checked=$(this).prop('checked');
		
		if(checked){
			$('#p-file').show();
			$('#p-DateLine').hide();
			$('#p-txtDate').show();
			$('#divKeterangan').show();
		}

	});
	
	$('#rbtSubmitPhysicalFile').change(function(){
		var checked=$(this).prop('checked');
		if(checked){
			$('#p-file').hide();
			$('#p-DateLine').hide();
			$('#p-txtDate').show();
			$('#divKeterangan').hide();
			
		}
	});
	
	$('#rbtPending').change(function(){
		var checked=$(this).prop('checked');
		
		if(checked){
			$('#p-DateLine').show();
			$('#p-file').hide();
			$('#p-txtDate').hide();
			$('#divKeterangan').hide();
		}
	});
	
};

$(document).ready(function()
{
	
	$('#tblView').on('click','.icon-view',view);
	
	$('#tblView').on('click','.icon-upload',upload);
	

});
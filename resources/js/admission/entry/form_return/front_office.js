function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}

var loadPretestTerm = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_pretest_term/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PretestTerm)
			{
				for(var i in data.PretestTerm)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.PretestTerm[i].TermID)+'">'+$.trim(data.PretestTerm[i].TermID)+ ' - ' +data.PretestTerm[i].TermName+'</option>'));
				}
			}
        } 
	});
};


var loadYearLevel = function(SchoolLevel,ObjectName){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_year_level/'+SchoolLevel,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="'+ObjectName+'"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.YearLevel)
			{
				for(var i in data.YearLevel)
				{
					$('[name="'+ObjectName+'"]').append($('<option value="'+$.trim(data.YearLevel[i].YearLevelID)+'">'+$.trim(data.YearLevel[i].YearLevelID)+ ' - ' +data.YearLevel[i].YearLevelName+'</option>'));
				}
			}
        } 
	});
};

var getAdmissionID = function(TermID){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_admissionID/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.AdmissionID)
			{
				for(var i in data.AdmissionID)
				{
					$('[name="txtViewAdmissionID"]').val( data.AdmissionID[i].AdmissionID);
				}
			}
        } 
	});
};


var getFormAmount = function(FormNumber){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_form_amount/'+FormNumber,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FormSoldView)
			{
				
				for(var i in data.FormAmount)
				{ 
					$('[name="txtViewAmount"]').val( data.FormAmount[i].Amount);
				}
			}
        } 
	});
};

var loadPaymentMethod = function(){
	$.ajax({
		url: SiteURL+'entry/form_sales/get_payment_method/',
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlViewPaymentMethod"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.PaymentMethod)
			{
				for(var i in data.PaymentMethod)
				{
				
					$('[name="ddlViewPaymentMethod"]').append($('<option value="'+$.trim(data.PaymentMethod[i].PaymentMethodID)+'">'+$.trim(data.PaymentMethod[i].PaymentMethodName)+'</option>'));
				}
			}
        } 
	});
};

var generatePassword = function(method,FormNumber){
	
	$.ajax({
		url: SiteURL+'entry/form_sales/generate_password/'+method+'/'+FormNumber,
		type: 'post',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
		
			if(data.GeneratedPassword)
			{
				$('[name="txtViewPassword"]').val(data.GeneratedPassword);
			}
        } 
	});
};

function create_modal( source_type, source ){
			$('.datepicker').datepicker('destroy');
			$('.timepicker').timepicker('destroy');
			$('.datetimepicker').datetimepicker('destroy');
			
			
	        var window_width = $(window).width();
	        var window_height = $(window).height();
	        $('body').css({ 'overflow' : 'hidden' }).prepend('<div id="modal"><div id="modal-content"></div><a href="#" id="modal-close" class="icon icon-close">Close</a></div><div id="modal-background"></div>');         

	        if( source_type == 'url' ){
	        	$('#modal-content').load( source, function(){
					modal_custom_styling();			     
	        	});
	        } else {
	        	$('#modal-content').html( $(source).clone().html() );
	        	/*$('#modal-content').append('<p>\
                <label for="">Select Dropdown (Custom - Powered by FancyFields)</label>\
                <span class="custom-select">\
                  <select name="" id="">\
                    <option value="">Ornare Fringilla Risus Venenatis Tellus</option>\
                    <option value="">Amet Aenean Cras Sem Risus</option>\
                    <option value="">Dapibus Cras Lorem Fusce Mollis</option>\
                  </select>\
                </span>\
              </p>');*/
	        	if ( $('#modal .custom-select').length > 0) {
	        		$('#modal .custom-select').fancyfields();
	        	}
	        	modal_custom_styling();
	        }
	    }

function modal_custom_styling(){
	var modal_width_adj = 0 - $('#modal').outerWidth() / 2;

	// Calculate modal's height
	var m_height = $("#modal").outerHeight();
	var w_height = $(window).height();
	
	if( m_height > ( w_height * 0.9 ) ){
		modal_style = {
			'top' : 20,
			'bottom' : 20
		};
	} else {
		modal_style = {
			'top' : '50%',
			'margin-top' : 0 - ( m_height / 2 )
		}
	}

	$('#modal').css({
		'margin-left' : modal_width_adj
	}).animate( modal_style, 200);

	// Custom Select
	/*if( $('#modal .custom-select').length > 0 ){
		$('#modal .custom-select').fancyfields();        	
	}*/

	// Custom Checkbox
	if( $('#modal .custom-checkbox').length > 0 ){
		$('#modal .custom-checkbox').fancyfields();        	
	}

	// Custom Radiobutton
	if( $('#modal .custom-radiobutton').length > 0 ){
		$('#modal .custom-radiobutton').fancyfields();        	
	}		        		        

	// Custom Scrollbar
	if( $('#modal .custom-scrollbar').length > 0 ){
		$('#modal .custom-scrollbar').mCustomScrollbar({
			autoHideScrollbar: true
		});
	}

	// Combo Checkbox
	$('#modal .combo-checkbox input[type="checkbox"]').fancyfields( 'bind', 'onCheckboxChange', function( input, isChecked ){
		combo_checkbox_changed( input );
	});

	$('#modal .combo-checkbox input[type="checkbox"]').change(function(){
		combo_checkbox_changed( $(this) );
	});

	// Datepicker
	if( $('#modal .datepicker').length > 0 ){
		$('#modal .datepicker').datepicker({ dateFormat: 'dd MM yy' });
	}
	
	if( $('#modal .datetimepicker').length > 0 ){
		$('#modal .datetimepicker').datetimepicker({ dateFormat: 'dd M yy' });  
   
     // 2014-05-13 00:00:00
	}
	
	if( $('#modal .timepicker').length > 0 ){
		$('#modal .timepicker').timepicker();
	}

	// Expand Control
	$('#modal .expand-control-toggle').fancyfields( 'bind', 'onCheckboxChange', function( input, isChecked ){
		if( isChecked ){
			input.closest('.expand-control-wrap').find('.expand-control').slideDown();
		} else {
			input.closest('.expand-control-wrap').find('.expand-control').slideUp();			
		}
	});

	$('#modal .expand-control-toggle').change(function(){
		if( $(this).is(':checked') ){
			$(this).closest('.expand-control-wrap').find('.expand-control').slideDown();
		} else {
			$(this).closest('.expand-control-wrap').find('.expand-control').slideUp();			
		}		
	});	   

	// Expand by Radio
	$('#modal .expand-by-radio-toggle').fancyfields( 'bind', 'onRadioChange', function( input ){
		var name = input.attr('name');
		var targets = $('#modal .expand-by-radio[data-name="'+name+'"]');

		targets.each(function(){
			var target = $(this);
			var req_value = target.attr('data-value');

			if( input.val() == req_value ) {
				target.slideDown();
			} else {
				target.slideUp();
			}			
		});
	});	    	
}

// CLOSE MODAL
function close_modal(){
		var window_height = $(window).height();
		
		$('#modal').animate({
				'top' : (0 - (window_height * 2))
		}, 400, function(){
				$('#modal').remove();
				$('#modal-background').fadeOut(function(){
						$(this).remove();
						$('body').css('overflow', 'auto');
						$('.binus-gallery-item').removeClass('active');
				});
		});
		
}

$('body').on('click', '#modal-background, #modal-close', function(){
		close_modal();          
		return false;
});

$(document).keyup(function(e){
		if ( e.keyCode == 27 && $('body #modal').length > 0){
				close_modal();
		}
});

var validate = function() {

	if($('#modal [name="txtViewFormNumber"]').val() == '')
		alert('Form Number is required');
	else{
		checkForm($.trim($('#modal [name="txtViewFormNumber"]').val()),$('#modal [name="ddlViewAdmissionTerm"] option:selected').val(),myCallbackSave);
	}
		
		

}


function myCallbackSave(Valid) {
	//myCallbackCheck(Valid);
	if(Valid==0){
		
		if($('#modal [name="txtViewAdmissionYear"]').val() == '')
			alert('AdmissionYear required');
		else if($('#modal [name="txtViewPaymentDate"]').val() == '')
			alert('Payment Date Term required');
		else if($('#modal [name="txtViewAmount"]').val() == '')
			alert('Payment Amount is required');
		else if($('#modal [name="ddlViewPaymentMethod"]').val() == '')
			alert('Payment method is required');
		else if($('#modal [name="txtViewStudentName"]').val() == '')
			alert('Student name is required');
		else if($('#modal [name="txtViewOfficerName"]').val() == '')
			alert('Officer name is required');
		else if($('#modal [name="txtViewPaidBy"]').val() == '')
			alert('Paid By is required');
		else if($('#modal [name="ddlViewYearLevel"]').val() == '')
			alert('Year Level is required');	
		else if($('#modal [name="txtViewPhoneNumber"]').val() == '')
			alert('Phone number required');
		else if($('#modal [name="ddlViewAdmissionTerm"]').val() == '')
			alert('Admission Term required');
		else if($('#modal [name="txtViewEmail"]').val() == '')
			alert('Email is required');
		else if( validateEmail($('#modal [name="txtViewEmail"]').val())==false )
			alert('Email field is not valid');
		else if($('#modal [name="txtViewPassword"]').val() == '')
				alert('Password is required');
		else
		{	
			$('#modal [name="frmAdd"]').submit();
		}
	}
}

function myCallbackCheck(Valid) {
	switch(Valid){
		case 0:
			$('[name="txtViewAmount"]').val( data.FormAmount[i].Amount);
			break;
		case 1:
			break;
		case 2:
			break;

	}
}

var checkForm = function(FormNumber,TermID,callback) {
	var Valid=2;
	var Amount=0;
	$.ajax({
		url: SiteURL+'entry/form_sales/check_form_validation/'+FormNumber+'/'+TermID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.FormStatus){
				if(data.FormStatus.FormStatus=="0"){
						Amount=data.FormStatus.Amount;
						if(callback==""){
						alert("This FormNumber is Valid");
						}
					Valid=0;
					}
				else if(data.FormStatus.FormStatus=="1"){
					alert("This FormNumber has Been Purchased");
					Valid=1;
					}
				else{
					alert("Form Number doesn't Exist");
					Valid=2;
					}
			}
        },
		complete:function(){
				switch(Valid){
					case 0:
						$('[name="txtViewAmount"]').val(Amount);
						$('[name="hfViewAmount"]').val(AmountValue);
						break;
					case 1:
						break;
					case 2:
						break;
				}
				
				if(callback!=""){
					callback(Valid);
				}
		
		}
	});
}

var add= function(){
	var row = $(this).parents('tr');

	create_modal( 'id', '#internal-popup' );
		$('[name="ddlViewSchoolLevel"]').prop("disabled", false); 
		$('[name="ddlViewAdmissionTerm"]').prop("disabled", false); 
		$('[name="ddlViewYearLevel"]').prop("disabled", false); 
		$('[name="ddlViewPaymentMethod"]').prop("disabled", false); 
	loadPaymentMethod();


	$('#modal #btnCheck').on('click',
		function(){

			var Valid=checkForm($.trim($('#modal [name="txtViewFormNumber"]').val()),$('#modal [name="ddlViewAdmissionTerm"] option:selected').val(),"");
		
		}
	);
	
	$('#modal #btnGenerate').on('click',
		function(){
		generatePassword("add","");
		}
	);
	
	$('[name="ddlViewSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
		loadYearLevel($(this).val(),"ddlViewYearLevel");
	});
	
	$('[name="ddlViewAdmissionTerm"]').change(function(){
		getAdmissionID($(this).val());
	});
				
	$('#modal #btnSave').click(validate);		

};


var view = function(){
	var row = $(this).parents('tr');
	var AjaxData;
	$.ajax({
		url: SiteURL+'entry/form_sales/get_form_sold_view/'+row.attr('data-id'),
		type: 'get',
		dataType: "json",
        success:function(data)
        {
			AjaxData=data;
        },
		complete:function(){
			create_modal( 'id', '#internal-popup' );
			if(AjaxData.FormSoldView)
			{
			
				var Form=$('#modal #frmAdd');

				var $inputs = $('#modal :input');

				$inputs.each(function() {
					$(this).prop('readonly', true);
				});
				
				$('[name="ddlViewSchoolLevel"]').prop("disabled", true); 
				$('[name="ddlViewAdmissionTerm"]').prop("disabled", true); 
				$('[name="ddlViewYearLevel"]').prop("disabled", true); 
				$('[name="ddlViewPaymentMethod"]').prop("disabled", true); 
				
				for(var i in AjaxData.FormSoldView)
				{
					$('[name="txtViewAdmissionID"]').val(AjaxData.FormSoldView[i].AdmissionID);
					$('[name="txtViewAdmissionYear"]').val(AjaxData.FormSoldView[i].AcademicYear);
					$('[name="txtViewAdmissionSemester"]').val(AjaxData.FormSoldView[i].SmtId);
					
					$('[name="ddlViewSchoolLevel"]').val(AjaxData.FormSoldView[i].SchoolLevelID);
					
					loadPretestTerm(AjaxData.FormSoldView[i].SchoolLevelID,"ddlViewAdmissionTerm");
					loadYearLevel(AjaxData.FormSoldView[i].SchoolLevelID,"ddlViewYearLevel");
					loadPaymentMethod();
					$('[name="ddlViewAdmissionTerm"]').val(AjaxData.FormSoldView[i].TermID);
					$('[name="ddlViewYearLevel"]').val(AjaxData.FormSoldView[i].YearLevelID);
					
					$('[name="txtViewFormNumber"]').val(AjaxData.FormSoldView[i].RegistrantId);

					//var date=new Date( AjaxData.FormSoldView[i].DateOfFormPurchased));
					$('[name="txtViewPaymentDate"]').val( AjaxData.FormSoldView[i].DateOfFormPurchased);
					//alert($('[name="txtViewPaymentDate"]').datetimepicker({ dateFormat: 'dd MM yy h:i' }).val());
					
					$('[name="txtViewReceiptName"]').val(AjaxData.FormSoldView[i].RecepientName);
					$('[name="txtViewPaidBy"]').val(AjaxData.FormSoldView[i].PaidBy);
					$('[name="txtViewOfficerName"]').val(AjaxData.FormSoldView[i].OfficerName);
					$('[name="txtViewStudentName"]').val(AjaxData.FormSoldView[i].Note);
					$('[name="txtViewPhoneNumber"]').val(AjaxData.FormSoldView[i].PhoneNumberSale);
					$('[name="txtViewAmount"]').val(AjaxData.FormSoldView[i].Amount);
					$('[name="txtViewAmountPaid"]').val(AjaxData.FormSoldView[i].AmountPaid);
					
					$('[name="ddlViewPaymentMethod"]').val(AjaxData.FormSoldView[i].PaymentMethodID);
					
					$('[name="txtViewEmail"]').val(AjaxData.FormSoldView[i].Email);
					$('[name="txtViewPassword"]').val(AjaxData.FormSoldView[i].Password);

				}
			} 
			else
			{
			
			}
			//$('[name="txtViewAdmissionSemester"]').val($('[name="hfViewAdmissionSemester"]').val());
			$('#modal #btnSave').hide();
			$('#modal #btnCheck').hide();
	
			
			$('#modal #btnGenerate').on('click',
				function(){
				generatePassword("view",$('#modal [name="txtViewFormNumber"]').val());
				}
			);
			
			$('[name="ddlViewSchoolLevel"]').change(function(){
				loadPretestTerm($(this).val(),"ddlViewAdmissionTerm");
				loadYearLevel($(this).val(),"ddlViewYearLevel");
			});
			//$('#modal #btnSave').click(validate);
		}
	});
	
};

$(document).ready(function()
{
	$('[name="ddlSchoolLevel"]').change(function(){
		loadPretestTerm($(this).val(),"ddlAdmissionTerm");
		loadYearLevel($(this).val(),"ddlYearLevel");
	});
	
	$('#btnSearch').on('click',function(e){
		e.preventDefault();
		$('[name=hfPage]').val("");
		$('[name=frmSearch]').submit();
	});

	
	$('.page-click').on('click',function(){
		$('[name=hfPage]').val($(this).attr('data-page'));
		$('[name=frmSearch]').submit();
		//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
	});
	$('.prev-nav').on('click',function(){
		$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
		$('[name=frmSearch]').submit();
	});
	$('.next-nav').on('click',function(){
		$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
		$('[name=frmSearch]').submit();
	});
	
	$('#tblView').on('click','.btnView',view);
	
	$('#btnAdd').on('click',add);
});
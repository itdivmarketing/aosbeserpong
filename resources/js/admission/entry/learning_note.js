function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}
var reloadPage = function(){
	$('#frmSearch').submit();
};

function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


$(document).ready(function() {

function GetLearningNoteData(){	

		var Param=$('[name="hfFormNumber"]').val();
		$.ajax({
			type: "post",
			url: SiteURL+'entry/learning_note/get_learning_note_data/'+Param,
			dataType: "json",
			success: function(data){

					if(data.LearningNoteData)
					{
						
						for(var i in data.LearningNoteData)
						{
							var InitialData={
							"AreaID":$.trim(data.LearningNoteData[i].ProblemID),
							"Description":$.trim(data.LearningNoteData[i].Description),
							"AttachReports":$.trim(data.LearningNoteData[i].AttachReports),
							"AttachReportName":$.trim(data.LearningNoteData[i].AttachReportName),
							"CanBeDeleted":$.trim(data.LearningNoteData[i].AttachReports)
							};

							LearningNoteData.push(InitialData) ;
							
						}
					}
				
				}
		})
}

	var $Inputs = $('#form-learning_note :input');
	$Inputs.each(function() {
		$(this).prop('disabled', true);
	});
	
	$('#btnAddLearningNote').addClass('hide');
	$('#edit-learning_note').click(function(){
		$("#form-learning_note :input").each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		$('#btnAddLearningNote').removeClass('hide');
		$('#TableLearningNote .icon-trash').removeClass('hide');
		$('#TableLearningNote .icon-edit').removeClass('hide');
		$('#learning_note-edit-section').addClass('hide');
		$('#learning_note-cancel-edit-section').removeClass('hide');
	});

	$('#cancel-edit-learning_note').click(function(){
		reloadPage();
		$("#form-learning_note :input").each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$('#btnAddLearningNote').addClass('hide');
		$('#TableLearningNote .icon-trash').addClass('hide');
		$('#TableLearningNote .icon-edit').addClass('hide');
		$('#learning_note-edit-section').removeClass('hide');
		$('#learning_note-cancel-edit-section').addClass('hide');
		
		OnUpdate=false;
		$('#btnAddLearningNote').text("Add New");
	});
	
	//var StudentStatus="<?php echo $StudentStatus; ?>";
	var LearningNoteData=new Array();
	var OnUpdate=false;
	var ActiveRow=0;
	var files=new Array();
	var DeleteSavedFile=new Array();
	var count=0;

	GetLearningNoteData();

	$("#TableLearningNote").on('click',".IconColumn", function(e){
	//debugger;
	var myRow = $(this).parent().index();
	var target= $(e.target);
	if(target.is('.icon-trash')){
	$(this).parent().remove();
	//alert("myrow"+myRow+" file"+files.length);
	var Limit=false;
	for(var i=0;i<files.length;i++){
	//	alert("fileRow"+files[i].Row);
		
		
		if(Limit==false){
			if(files[i].Row==myRow){
				Limit=true;
				files.splice(i,1);
				i=i-1;
				}
		}
		
		if(i>=0){
			if(files[i].Row>myRow)
				files[i].Row-=1;
		}
	}
	

	
	if(LearningNoteData[myRow].CanBeDeleted==1){
		DeleteSavedFile.push(LearningNoteData[myRow].AttachReportName);
	}
	LearningNoteData.splice(myRow,1) ;
	//alert("regis,len:"+LearningNoteData.length);
	OnUpdate=false;
	$('#btnAddLearningNote').text("Add New");
	}
	
	if(target.is('.icon-edit')){
	//alert("asd");
	OnUpdate=true;
	$('#btnAddLearningNote').text("Edit");
	ActiveRow=myRow;
	
	$('#ddlArea').val(LearningNoteData[myRow].AreaID);
	$('#txtNote').val(LearningNoteData[myRow].Description);

	}
	
	});
	
	
	$("#btnAddLearningNote").click(function(e){
		
		e.preventDefault();
		var ErrText="";
		var file =$('[name=file]', $('#form-learning_note')).prop('files');	
		
		ErrText=ValidateFile(file);
		if(ErrText!=""){
			alert(ErrText);
			return false;
		}
			
		if(xyz()==false)	
			return false;

		ErrText=HoldData();
	
		if(ErrText!=""){
		//alert(ErrText);
		return false;
		}

		if(OnUpdate==true){
	
			if(LearningNoteData[ActiveRow].CanBeDeleted==1){
	
				if(file && file.length > 0){
				DeleteSavedFile.push(LearningNoteData[ActiveRow].AttachReportName);
				LearningNoteData[ActiveRow].CanBeDeleted=0;
				}
			}
		}
		

		OnUpdate=false;
		$('#btnAddLearningNote').text("Add New");

		$('#ddlArea').val("");
		$('#txtNote').val("");
		$(".file-container #file").remove();
		$(".file-container").html("<input type='file' name='file' id='file' class='highlight'>");
	});
	
	function xyz(File){

		var IsError=false;
		var AttachReports=0;
		var CanBeDeleted=0;
		var AttachReportName="";
		if(OnUpdate==true){
			if(LearningNoteData[ActiveRow].CanBeDeleted==1){
				CanBeDeleted=1;
				AttachReportName=LearningNoteData[ActiveRow].AttachReportName;
			}			
		}	
			
		var file =$('[name=file]', $('#form-learning_note')).prop('files');
		if(file && file.length > 0) {
			AttachReports=1;
		}
		
		if(CanBeDeleted==1)
			AttachReports=1;
	
		var Area=$('#ddlArea option:selected');
		var Description=$('#txtNote');
		var Data={"AreaID":Area.val(),"AreaName":Area.text(),"Description":Description.val(),"AttachReports":AttachReports,"AttachReportPath":"","AttachReportName":AttachReportName,"CanBeDeleted":CanBeDeleted};
		var IsError=false;
		//$( ".field-error" ).hide();
		if(Area.val()==""){
			alert("Area Field is Required");
			IsError=true;
		}
		
		
		for(var i=0;i<LearningNoteData.length;i++){
			if(Data.AreaID==LearningNoteData[i].AreaID){
				if(!(LearningNoteData[ActiveRow].AreaID==Data.AreaID&&OnUpdate==true)){
					alert("There is Already "+$.trim(Data.AreaName)+" Area In The List");
					IsError=true;
				}
			}
		
		}
		
		if(IsError==true){
			return false;
		}
		
		
		
		if(OnUpdate==false){
		LearningNoteData.push(Data) ;
		}
		else
		LearningNoteData[ActiveRow]=Data ;
		
		//alert(LearningNoteData.length);
		//alert(Data.AreaID);
		
		var row = $("<tr>");
		var icon="<td class='IconColumn'><i class='icon icon-trash'></i>&nbsp;<i class='icon icon-edit'></i></td>";	
		//var column=$("<td>").append(icon));
		row.append(icon);
		
		for(var i=1;i<=2;i++){
			var Text=""
			switch(i){
				case 1:Text=Area.text();
					break;
				case 2:Text=Description.val();
					break;

			}
			
			if(OnUpdate==false){
				row.append( $("<td>").text(Text) );
			}else{
				$('#TableLearningNote tr').eq(ActiveRow+1).find('td').eq(i).html(Text);
				//RegistrantBGData.push(Data) ;
			}
		}
		//alert("popo");
		//var CheckBox="<td class='CheckBoxColumn'><span class='custom-checkbox'><div id='ffCheckboxWrapper on' class='ffCheckboxWrapper'><div class='ffCheckbox'></div><div id='ffCheckboxWrapper' class='ffCheckboxWrapper'><div class='ffCheckbox'  style='z-index:999999'></div><input type='checkbox' class='checkbox-2'  style='display: none;'/></div></div></span></td>";
		//var IconDownload="<td><a href='<?php echo base_url('useruploads/admission/temp'); ?>/"+Data.AttachReportName+"'  download='"+Data.AttachReportName+"' target='_blank' ><i class='icon icon-download-item'></i></a></td>";
		if(OnUpdate==false){
			if(AttachReports==1){
				row.append( $("<td>").text("Save To Preview") );
			}
			else{
				row.append( $("<td>").text("No Attachment") );
			}
		}
		else{
			
			if(file && file.length > 0){
				$('#TableLearningNote tr').eq(ActiveRow+1).find('td').eq(3).html("Save To Preview");
			}
			else{
				if(CanBeDeleted!=1){
				$('#TableLearningNote tr').eq(ActiveRow+1).find('td').eq(3).html("No Attachment");
				}
			}
		}
		//row.append(IconDownload);
		
		
		
		if(OnUpdate==false){
			$("#TableLearningNote").append(row);
		}else{
			//$('#TableBGLanguage tr').eq(ActiveRow+1).find('td').eq(1).html(Text);
		}

	

		return true;
	
		
	
	}
	
	
	
	
	
	
	
	function HoldData(){
	//alert("file petama "+files[0].name);
	
	 var file =$('[name=file]', $('#form-learning_note')).prop('files');
	 
	 var ErrText=ValidateFile(file);
	 
	 if(ErrText==""){
		if(file && file.length > 0) {
		if(OnUpdate==false){
			//alert("updatefalse");
			//alert(LearningNoteData.length);
			files.push({File:file[0],Row:LearningNoteData.length-1});
			}
		else{
			var Same=-1;
			for(var i=0;i<files.length;i++){
				if(files[i].Row==ActiveRow)
					Same=i;
			}
			if(Same==-1){
				//alert("same=-1");
				files.push({File:file[0],Row:ActiveRow});
				}
			else{
				//alert("same!=1");
				files[Same].File=file[0];
				files[Same].Row=ActiveRow;
				}
			}
		}
		//alert(files[0].File.name+""+files[0].Row);
	}

	
	 return ErrText;

	}
	
	function ValidateFile(file){
		var ErrText="";
		if(file && file.length > 0) {
	
			file = file[0];
			var ext = file.name.split(/\./).pop().toLowerCase();
		
			/*
			if(file.type != 'image/jpeg' && file.type != 'image/png' && file.type != 'application/pdf'
			&& file.type != 'application/x-rar-compressed' && file.type != 'application/zip') 
			{
				ErrText='File Anda tidak dalam format jpg, png, atau pdf';
			}
			*/
			if(ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'pdf' && ext != 'rar'  && ext != 'zip' ) {
				ErrText='Yor file is not in jpg, jpeg, png, rar, zip or pdf format';
			}
			
			if(file.size > 6 * 1024 * 1024) {
				ErrText='Your file size can not be bigger than 6MB';
			}
		} else {
			ErrText="";
		}
		
	
	
		return ErrText;
	}
	
	
	function SaveFile(callback){
		//alert("zxczczxc");
		var Temp=new Array();
		//alert("zxczczxc");
		var formData = new FormData();
		//alert(files.length);
		if(files.length>0){
		
		var FileTemp=new Array();
		
		for(var i=0;i<files.length;i++){
		
			FileTemp.push(files[i].File);
			//alert("file1"+files[i].File.name);
			//alert("file2"+FileTemp[i].name);
		}
		
		
			$.each(FileTemp, function(key, value)
					{
					//alert("val"+value);
					formData.append(key, value);
					
			});
			
				var Param=$('[name="hfFormNumber"]').val();
				//var formData = new FormData($('#form_learning_note')[0]);
				$.ajax({ 
					url: SiteURL+'entry/learning_note/save_learning_note_file/'+Param,  //Server script to process data
					type: 'POST',
					// Form data
					data:formData,
					dataType:'json',
					//Options to tell jQuery not to process data or worry about content-type.
					cache: false,
					contentType: false,
					processData: false,
					success:  function(data){
							//alert("Count"+data.Count);
							if(data.Count>1){
								for(var i in data.data.FileName){
								var TempData={"Path":data.data.FileName[i],"FileName":data.data.FileName[i]};	
								//alert("File Name"+TempData.FileName);
								Temp.push(TempData);
								}
							}
							else{
								var TempData={"Path":data.data.FileName,"FileName":data.data.FileName};
								//alert("File Name"+TempData.FileName);
								Temp.push(TempData);
							}
							callback(Temp);
						}
					});
				
			
		}
		else{
			//alert("zxczczxc");
			callback(Temp);
		}
	
		
	}
	
	
	function SubmitData(File){
		//alert("zxczczxc");	
		for(var i=0;i<File.length;i++){
			var Row=files[i].Row;
			//alert("Row"+Row);
			LearningNoteData[Row].AttachReportPath=File[i].Path;
			LearningNoteData[Row].AttachReportName=File[i].FileName;
			
		}

		for(var i=0;i<LearningNoteData.length;i++){

			$('#form-learning_note').append('<input type="hidden" name="hfAreaID[]" value="'+LearningNoteData[i].AreaID+'" />');
			$('#form-learning_note').append('<input type="hidden" name="hfDescription[]" value="'+LearningNoteData[i].Description+'" />');
			$('#form-learning_note').append('<input type="hidden" name="hfAttachReports[]" value="'+LearningNoteData[i].AttachReports+'" />');
			$('#form-learning_note').append('<input type="hidden" name="hfAttachReportName[]" value="'+LearningNoteData[i].AttachReportName+'" />');
			$('#form-learning_note').append('<input type="hidden" name="hfCanBeDeleted[]" value="'+LearningNoteData[i].CanBeDeleted+'" />');
		}
		
		for(var i=0;i<DeleteSavedFile.length;i++){
			$('#form-learning_note').append('<input type="hidden" name="hfDeleteSavedFile[]" value="'+DeleteSavedFile[i]+'" />');
		}
		$('#form-learning_note').submit();
	
		/*
			$.ajax({
				type: "post",
				url:  '<?php echo site_url('StudentInformation_LearningNote/save_learning_note'); ?>',
				data: {
					   Data:LearningNoteData,
					   DeleteSavedFileData:DeleteSavedFile
					   },
				dataType: "json",
				success: function(){
						alert("success");
						for(var i=0;i<File.length;i++){
							var Row=files[i].Row;
							LearningNoteData[Row].CanBeDeleted=1;
						}
						files=[];
						AppendDownloadIcon();
				},
				error: function(xhr, textStatus, errorThrown){
					alert('failed');
				}
			})
		*/
	
	}
	
	
	/*
	function AppendDownloadIcon(Data){
		//alert("removed");
		for(var i=0;i<LearningNoteData.length;i++){
			//var row = $("<tr>");
			
			$('#TableLearningNote tr').eq(i+1).find("td").eq(3).remove();
			//alert("kolom"+Count);
			if(LearningNoteData[i].AttachReports!=0){
			var IconDownload="<td><a href='<?php echo base_url('useruploads/admission/StudentInformation/LearningNote'); ?>/"+LearningNoteData[i].AttachReportName+"'  download='"+LearningNoteData[i].AttachReportName+"' target='_blank' ><i class='icon icon-download-item'></i></a></td>";
			$("#TableLearningNote tr").eq(i+1).append(IconDownload);
			}
			else{
			$("#TableLearningNote tr").eq(i+1).append( $("<td>").text("No Attachment") );
			}
		}
	
	}
	*/
	
	$("#btnSaveLearningNote").click(function(e){
	
		e.preventDefault();
		//if(StudentStatus!=0)
		//	return false;
		OnUpdate=false;
		$('#btnAddLearningNote').text("Add New");
		//var formData = new FormData();
		
		SaveFile(function (File){
			SubmitData(File);
			
		});
		
		
	});
		
						
	
});
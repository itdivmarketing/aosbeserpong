function numeric(event) 
{
	// Backspace, tab, enter, end, home, left, right
	// We don't support the del key in Opera because del == . == 46.
	var controlKeys = [8, 9, 13, 35, 36, 37, 39];
	// IE doesn't support indexOf
	var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
	// Some browsers just don't raise events for control keys. Easy.
	// e.g. Safari backspace.
	if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
	  (48 <= event.which && event.which <= 57) || // Always 0 through 9
	  isControlKey) { // Opera assigns values for control keys.
	return;
	}else{
	event.preventDefault();
	}
}

function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}
var reloadPage = function(){
	$('#frmSearch').submit();
};

	
function validateEmail($email) {
	  var emailReg = /^(-|([\w-\.]+@([\w-]+\.)+[\w-]{2,4}))?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}

var loadFatherCity = function(country){
	$.ajax({
		url: SiteURL+'entry/student_profile/get_city/'+country,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlFatherCity"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.City)
			{
				for(var i in data.City)
				{
					$('[name="ddlFatherCity"]').append($('<option value="'+$.trim(data.City[i].CityID)+'">'+data.City[i].CityName+'</option>'));
				}
			}


        } 
	});
};

var loadMotherCity = function(country){
	$.ajax({
		url: SiteURL+'entry/student_profile/get_city/'+country,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlMotherCity"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.City)
			{
				for(var i in data.City)
				{
					$('[name="ddlMotherCity"]').append($('<option value="'+$.trim(data.City[i].CityID)+'">'+data.City[i].CityName+'</option>'));
				}
			}
        } 
	});
};
	

var ProfileValidate = function() {

			  
	if($.trim($('#form-ParentProfile [name="txtFormNumber"]').val() )== '')
		alert('Form Number is required');
	else{
		if($('#ddlFatherLifeStatus').val()!=2 && $.trim($('#form-ParentProfile [name="txtFatherEmail"]').val()) == '')
			alert('Father Email is required');
		else if($('#ddlFatherLifeStatus').val()!=2 && $.trim($('#form-ParentProfile [name="txtFatherEmail"]').val()) != ''&& validateEmail($('#form-ParentProfile [name="txtFatherEmail"]').val()) == false )
			alert('Father Email is not valid');
		/*else if($.trim($('#form-ParentProfile [name="txtFatherCellPhone"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtFatherCellPhone"]').val()) == false)
			alert('Father Cellphone is not valid');	
		else if($.trim($('#form-ParentProfile [name="txtFatherPhone"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtFatherPhone"]').val()) == false)
			alert('FatherPhone is not valid');	
		else if($.trim($('#form-ParentProfile [name="txtFatherFax"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtFatherFax"]').val()) == false)
			alert('Father Fax is not valid');	*/
		else if($.trim($('#form-ParentProfile [name="txtFatherPostalCode"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtFatherPostalCode"]').val()) == false)
			alert('Father Postal Code is not valid');
		else if($.trim($('#form-ParentProfile [name="txtFatherName"]').val() )== '')
			alert('Father Name is required');	
		else if($.trim($('#form-ParentProfile [name="txtFatherAddress"]').val() )== '')
			alert('Father Address is required');
		else if($.trim($('#form-ParentProfile [name="ddlFatherCountry"]').val() )== '')
			alert('Father Country is required');
		else if($.trim($('#form-ParentProfile [name="ddlFatherCity"]').val() )== '')
			alert('Father City is required');
		/*else if($.trim($('#form-ParentProfile [name="txtFatherCellPhone"]').val() )== '')
			alert('Father Cell Phone Number is required');
		else if($.trim($('#form-ParentProfile [name="txtFatherEmail"]').val() )== '')
			alert('Father Email is required');*/
		else if($.trim($('#form-ParentProfile [name="ddlFatherReligion"]').val() )== '')
			alert('Father Religion is required');
			
		else if($.trim($('#form-ParentProfile [name="txtFatherRIPYear"]').val())!='' && validateNumeric($.trim($('#form-ParentProfile [name="txtFatherRIPYear"]').val()))==false )
				alert("Father Rip Year is not valid");
		else if($('#form-ParentProfile [name="ddlFatherLifeStatus"]').val()=="2" && $.trim($('#form-ParentProfile [name="txtFatherRIPYear"]').val())=="")
				alert("Father Rip Year is Required");
				
		else if($.trim($('#form-ParentProfile [name="txtMotherName"]').val() )== '')
			alert('Mother Name is required');	
		else if($.trim($('#form-ParentProfile [name="txtMotherAddress"]').val() )== '')
			alert('Mother Address is required');
		else if($.trim($('#form-ParentProfile [name="ddlMotherCountry"]').val() )== '')
			alert('Mother Country is required');
		else if($.trim($('#form-ParentProfile [name="ddlMotherCity"]').val() )== '')
			alert('Mother City is required');
		/*else if($.trim($('#form-ParentProfile [name="txtMotherCellPhone"]').val() )== '')
			alert('Mother Cell Phone Number is required');
		else if($.trim($('#form-ParentProfile [name="txtMotherEmail"]').val() )== '')
			alert('Mother Email is required');*/
		else if($.trim($('#form-ParentProfile [name="ddlMotherReligion"]').val() )== '')
			alert('Mother Religion is required');
		else if($('#ddlMotherLifeStatus').val()!=2  && $.trim($('#form-ParentProfile [name="txtMotherEmail"]').val()) == '')
			alert('Mother Email is required');
		else if($('#ddlMotherLifeStatus').val()!=2  && $.trim($('#form-ParentProfile [name="txtMotherEmail"]').val()) != ''&& validateEmail($('#form-ParentProfile [name="txtMotherEmail"]').val()) == false )
			alert('Mother Email is not valid');
		/*
		else if($.trim($('#form-ParentProfile [name="txtMotherCellPhone"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtMotherCellPhone"]').val()) == false)
			alert('Mother Cellphone is not valid');	
		else if($.trim($('#form-ParentProfile [name="txtMotherPhone"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtMotherPhone"]').val()) == false)
			alert('Mother Phone is not valid');	
		else if($.trim($('#form-ParentProfile [name="txtMotherFax"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtMotherFax"]').val()) == false)
			alert('Mother Fax is not valid');	*/
		else if($.trim($('#form-ParentProfile [name="txtMotherPostalCode"]').val() )!= '' && validateNumeric($('#form-ParentProfile [name="txtMotherPostalCode"]').val()) == false)
			alert('Mother Postal Code is not valid');
		else if($.trim($('#form-ParentProfile [name="txtMotherRIPYear"]').val())!='' && validateNumeric($.trim($('#form-ParentProfile [name="txtMotherRIPYear"]').val()))==false )
				alert("Mother Rip Year is not valid");
		else if($('#form-ParentProfile [name="ddlMotherLifeStatus"]').val()=="2" && $.trim($('#form-ParentProfile [name="txtMotherRIPYear"]').val())=="")
				alert("Mother Rip Year is Required");
		else
		{	
			var $ParentProfileInputs = $('#form-ParentProfile :input');
	
	
			$ParentProfileInputs.each(function() {
				$(this).prop('disabled', false);
			});

			$('#form-ParentProfile').find('input').each(function(){
				$(this).val($.trim($(this).val()));
			});

			$('#form-ParentProfile').submit();
		}
	}
		
		

}

var OfficeValidate = function() {

			 	
	if($.trim($('#form-ParentEmployment [name="txtFormNumber"]').val() ) == '')
		alert('Form Number is required');
	else{
		if($.trim($('#form-ParentEmployment [name="txtFatherOfficePhone"]').val() )!= '' && validateNumeric($('#form-ParentEmployment [name="txtFatherOfficePhone"]').val()) == false)
			alert('Father Offfice Phone is not valid');	
		else if($.trim($('#form-ParentEmployment [name="txtFatherOfficeFax"]').val() )!= '' && validateNumeric($('#form-ParentEmployment [name="txtFatherOfficeFax"]').val()) == false)
			alert('Father Office Fax is not valid');	
		else if($.trim($('#form-ParentEmployment [name="txtFatherOfficePostalCode"]').val() )!= '' && validateNumeric($('#form-ParentEmployment [name="txtFatherOfficePostalCode"]').val()) == false)
			alert('Father Postal Code is not valid');
		else if($.trim($('#form-ParentEmployment [name="txtMotherOfficePhone"]').val() )!= '' && validateNumeric($('#form-ParentEmployment [name="txtMotherOfficePhone"]').val()) == false)
			alert('Mother Offfice Phone is not valid');	
		else if($.trim($('#form-ParentEmployment [name="txtMotherOfficeFax"]').val() )!= '' && validateNumeric($('#form-ParentEmployment [name="txtMotherOfficeFax"]').val()) == false)
			alert('Mother Office Fax is not valid');			
		else if($.trim($('#form-ParentEmployment [name="txtMotherOfficePostalCode"]').val() )!= '' && validateNumeric($('#form-ParentEmployment [name="txtMotherOfficePostalCode"]').val()) == false)
			alert('Mother Postal Code is not valid');
		else
		{	
		
			var $ParentEmploymentInputs = $('#form-ParentEmployment :input');
	
			$ParentEmploymentInputs.each(function() {
				$(this).prop('disabled', false);
			});
			  $('#form-ParentEmployment').find('input').each(function(){
					$(this).val($.trim($(this).val()));
				});
			
			//alert($('#form-ParentEmployment[name="txtFatherOfficePhone"]').val());
			$('#form-ParentEmployment').submit();
		}
	}
		
		

}

var hapusFatherPhone = function(){
	var row = $(this).parents('tr');
	row.remove();
	
	/*var rowcount = $('#tblFatherPhone').find('tr:gt(0)').size();
	if(rowcount == 0 ){
		$('#tbFatherPhone').append($('<tr><td colspan="3">There\'s No Data</td></tr>'));
	}*/
}

var hapusMotherPhone = function(){
	var row = $(this).parents('tr');
	row.remove();
	
	/*var rowcount = $('#tblMotherPhone').find('tr:gt(0)').size();
	if(rowcount == 0 ){
		$('#tblMotherPhone').append($('<tr><td colspan="3">There\'s No Data</td></tr>'));
	}*/
}

var PhoneValidate = function(){
	if($.trim($('#form-ParentPhone [name="txtFormNumber"]').val() ) == '')
		alert('Form Number is required');
	else{
		var rowcountFather = $('#tblFatherPhone').find('tr:gt(0)').size();
		var rowcountMother = $('#tblMotherPhone').find('tr:gt(0)').size();
		
		if(rowcountFather <= 0)
			alert('Father Phone cannot be empty');
		else if(rowcountMother <= 0)
			alert('Mother Phone cannot be empty');
		else
		{	
		
			var $ParentPhoneInputs = $('#form-ParentInput :input');
	
			$ParentPhoneInputs.each(function() {
				$(this).prop('disabled', false);
				$(this).removeClass('highlight');
			
				$('#tblFatherPhone').each(function(){
					$(this).find('.btnDelete').hide();
				});
				
				$('#tblMotherPhone').each(function(){
					$(this).find('.btnDelete').hide();
				});
			});
			
			//alert($('#form-ParentEmployment[name="txtFatherOfficePhone"]').val());
			$('#form-ParentPhone').submit();
		}
	}
}


function checkRequirementbyLifeStatus(){
	if($('#ddlMotherLifeStatus').val()!=2){
		$('#email-mother-required').text("*");
	}else{
		$('#email-mother-required').text("");
	}

	if($('#ddlFatherLifeStatus').val()!=2){
		$('#email-father-required').text("*");
	}else{
		$('#email-father-required').text("");
	}


	if($('#ddlMotherLifeStatus').val()==2){
		$('#ripyear-mother-required').text("*");
		$('#txtMotherRIPYear').prop('readonly', false);
	}else{
		$('#ripyear-mother-required').text("");
		$('#txtMotherRIPYear').val("");
		$('#txtMotherRIPYear').prop('readonly', true);
	}

	if($('#ddlFatherLifeStatus').val()==2){
		$('#ripyear-father-required').text("*");
		$('#txtFatherRIPYear').prop('readonly', false);
	}else{
		$('#ripyear-father-required').text("");
		$('#txtFatherRIPYear').val("");
		$('#txtFatherRIPYear').prop('readonly', true);
	}
}


$(document).ready(function()
{

		checkRequirementbyLifeStatus();


		$('.parentdatepicker').datepicker({
			dateFormat: 'dd M yy', 
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd M yy',
			yearRange: '-80:-20'
		});
	
	
	$("#cbxLiveTogether").on('change', function(e) {
		   e.preventDefault();
		   var checkbox = $(this);   // keep reference of checkbox
           var checked = checkbox.is(":checked");      // check checkbox status
	
			if(checked==true){
				LiveTogetherGenerator();
			}
			else
				NotLiveTogetherGenerator();
    });
	
	$('#txtFatherAddress').bind('input', function(){
		if($("#cbxLiveTogether").prop("checked")==true)
		 $("#txtMotherAddress").val($('#txtFatherAddress').val());
	 });
	 

	 $('#txtFatherPostalCode').bind('input', function(){
		if($("#cbxLiveTogether").prop("checked")==true)
		 $("#txtMotherPostalCode").val($('#txtFatherPostalCode').val());
	 });

	 
	function LiveTogetherGenerator(){
	
		var FatherAddress=$('#txtFatherAddress');
		var FatherCountry=$('#ddlFatherCountry option:selected');
		var FatherCity=$('#ddlFatherCity option:selected');
		var FatherPostalCode=$('#txtFatherPostalCode');
		
		var MotherAddress=$('#txtMotherAddress');
		var MotherCountry=$('#ddlMotherCountry');
		var MotherCity=$('#ddlMotherCity');
		var MotherPostalCode=$('#txtMotherPostalCode');
			
		MotherAddress.val(FatherAddress.val());
		MotherCountry.val(FatherCountry.val());
		loadMotherCity(FatherCountry.val());
		MotherCity.val(FatherCity.val());
		MotherPostalCode.val(FatherPostalCode.val());
		
		MotherAddress.prop('disabled', true);
		MotherCountry.prop('disabled', true);
		MotherCity.prop('disabled', true);
		MotherPostalCode.prop('disabled', true);
		

	}
	
	function NotLiveTogetherGenerator(){
	
		var MotherAddress=$('#txtMotherAddress');
		var MotherCountry=$('#ddlMotherCountry');
		var MotherCity=$('#ddlMotherCity');
		var MotherPostalCode=$('#txtMotherPostalCode');
		
		MotherAddress.prop('disabled', false);
		MotherCountry.prop('disabled', false);
		MotherCity.prop('disabled', false);
		MotherPostalCode.prop('disabled', false);
	}

	
			
	$('#ddlFatherCity').on('change', function (e) {
		
		if($("#cbxLiveTogether").prop("checked")==true){
		var FatherCity=$('#ddlFatherCity option:selected');
		var MotherCity=$('#ddlMotherCity');
		MotherCity.val(FatherCity.val());
		}
		

	});
	
	
	
	
	var $ParentProfileInputs = $('#form-ParentProfile :input');
	
	
	$ParentProfileInputs.each(function() {
		$(this).prop('disabled', true);
	});

	
	$('#edit-parent-profile').click(function(){
		$ParentProfileInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});

		if($("#cbxLiveTogether").is(":checked"))
		{
			LiveTogetherGenerator();				
		}
		$("#parent-profile-edit-section").addClass('hide');
		$("#parent-profile-save-section").removeClass('hide');
	});

	$('#cancel-edit-parent-profile').click(function(){
		reloadPage();
		$ParentProfileInputs.each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$("#parent-profile-edit-section").removeClass('hide');
		$("#parent-profile-save-section").addClass('hide');
	});
	
	$('#btnProfileSave').click(ProfileValidate);	
	
	//phone
	var $ParentPhoneInputs = $('#form-ParentPhone :input');
	
	$('#tblFatherPhone').each(function(){
		$(this).find('.btnDelete').hide();
	});
	
	$('#tblMotherPhone').each(function(){
		$(this).find('.btnDelete').hide();
	});
	
	$ParentPhoneInputs.each(function() {
		$(this).prop('disabled', true);
	});
	
	$('#edit-parent-phone').click(function(){
		$ParentPhoneInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
			
			$('#tblFatherPhone').each(function(){
				$(this).find('.btnDelete').show();
			});
			
			$('#tblMotherPhone').each(function(){
				$(this).find('.btnDelete').show();
			});
		});
		$("#parent-phone-edit-section").addClass('hide');
		$("#parent-phone-save-section").removeClass('hide');
	});

	$('#cancel-edit-parent-phone').click(function(){
		reloadPage();
		$ParentPhoneInputs.each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
			
			$('#tblFatherPhone').each(function(){
				$(this).find('.btnDelete').hide();
			});
			
			$('#tblMotherPhone').each(function(){
				$(this).find('.btnDelete').hide();
			});
			
		});
		$("#parent-phone-edit-section").removeClass('hide');
		$("#parent-phone-save-section").addClass('hide');
	});
	
	$('#btnPhoneSave').click(PhoneValidate);
	
	$('[name=btnAddFatherPhone]').click(function(){
		if($('#txtFatherPhone').val() == '')
		{
			alert('Father Phone required');
		}
		else if($('#ddlFatherPhoneType').val() == '')
		{
			alert('Father Phone Type required');
		}
		else
		{
			var phone = $('#txtFatherPhone').val();
			var type = $('#ddlFatherPhoneType').val();
			var flag = 0;
			$('#tblFatherPhone').find('tr:gt(0)').each(function(){
				if( phone == $(this).attr('data-phone') && type == $(this).attr('data-type') ) {
					alert('Phone Type and Phone Number already exists in the table below.');
					flag = 1;
					return;
				}
			});
			if(flag == 0) {
				$('#tblFatherPhone').append($('<tr data-type="'+$('#ddlFatherPhoneType').val()+'" data-phone="'+$('#txtFatherPhone').val()+'"><td><a class="icon icon-trash btnDelete"></a></td>'+
					'<td><input type="hidden" name="hfFatherPhoneType[]" value="'+$('#ddlFatherPhoneType').val()+'" /> '+
					($('#ddlFatherPhoneType').val() == 'M' ? 'Mobile Phone' : ($('#ddlFatherPhoneType').val() == 'F' ? 'Fax' : 'Residence Phone'))+
					'</td><td><input type="hidden" name="hfFatherPhone[]" value="'+$('#txtFatherPhone').val()+'" /> '+$('#txtFatherPhone').val()+'</td>'));
				$('#txtFatherPhone').val('');
				$('#ddlFatherPhoneType').val('');
			}
		}
	});
	
	$('[name=btnAddMotherPhone]').click(function(){
		if($('#txtMotherPhone').val() == '')
		{
			alert('Mother Phone required');
		}
		else if($('#ddlMotherPhoneType').val() == '')
		{
			alert('Mother Phone Type required');
		}
		else
		{
			var phone = $('#txtMotherPhone').val();
			var type = $('#ddlMotherPhoneType').val();
			var flag = 0;
			$('#tblMotherPhone').find('tr:gt(0)').each(function(){
				if( phone == $(this).attr('data-phone') && type == $(this).attr('data-type')) {
					alert('Phone Type and Phone Number already exists in the table below.');
					flag = 1;
					return;
				}
			});
			if(flag == 0) {
				$('#tblMotherPhone').append($('<tr data-type="'+$('#ddlMotherPhoneType').val()+'" data-phone="'+$('#txtMotherPhone').val()+'"><td><a class="icon icon-trash btnDelete"></a></td>'+
					'<td><input type="hidden" name="hfMotherPhoneType[]" value="'+$('#ddlMotherPhoneType').val()+'" /> '+
					($('#ddlMotherPhoneType').val() == 'M' ? 'Mobile Phone' : ($('#ddlMotherPhoneType').val() == 'F' ? 'Fax' : 'Residence Phone'))+
					'</td><td><input type="hidden" name="hfMotherPhone[]" value="'+$('#txtMotherPhone').val()+'" /> '+$('#txtMotherPhone').val()+'</td>'));
					
				$('#txtMotherPhone').val('');
				$('#ddlMotherPhoneType').val('');
			}
		}
	});
	
	$('#tblFatherPhone').on('click','.btnDelete',hapusFatherPhone);
	$('#tblMotherPhone').on('click','.btnDelete',hapusMotherPhone);
	
	//office
	var loadFatherOfficeCity = function(country){
	$.ajax({
		url: SiteURL+'entry/student_profile/get_city/'+country,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlFatherOfficeCity"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.City)
			{
				for(var i in data.City)
				{
					$('[name="ddlFatherOfficeCity"]').append($('<option value="'+$.trim(data.City[i].CityID)+'">'+data.City[i].CityName+'</option>'));
				}
			}
        } 
	});
};

var loadMotherOfficeCity = function(country){
	$.ajax({
		url: SiteURL+'entry/student_profile/get_city/'+country,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlMotherOfficeCity"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.City)
			{
				for(var i in data.City)
				{
					$('[name="ddlMotherOfficeCity"]').append($('<option value="'+$.trim(data.City[i].CityID)+'">'+data.City[i].CityName+'</option>'));
				}
			}
        } 
	});
};
	
	function WorkTogetherGenerator(){
		var FatherOfficeAddress=$('#txtFatherOfficeAddress');
		var FatherOfficeCountry=$('#ddlFatherOfficeCountry option:selected');
		var FatherOfficeCity=$('#ddlFatherOfficeCity option:selected');
		var FatherOfficePostalCode=$('#txtFatherOfficePostalCode');
		var FatherCompany=$('#txtFatherCompany');
		
		var MotherCompany=$('#txtMotherCompany');
		var MotherOfficeAddress=$('#txtMotherOfficeAddress');
		var MotherOfficeCity=$('#ddlMotherOfficeCity');
		var MotherOfficePostalCode=$('#txtMotherOfficePostalCode');
		var MotherOfficeCountry=$('#ddlMotherOfficeCountry');
		
		MotherCompany.val(FatherCompany.val());
		MotherOfficeAddress.val(FatherOfficeAddress.val());
		MotherOfficeCountry.val(FatherOfficeCountry.val());
		loadMotherOfficeCity(FatherOfficeCountry.val());
		MotherOfficeCity.val(FatherOfficeCity.val());
		MotherOfficePostalCode.val(FatherOfficePostalCode.val());
		

		MotherCompany.prop('disabled', true);
		MotherOfficeAddress.prop('disabled', true);
		MotherOfficeCity.prop('disabled', true);
		MotherOfficeCountry.prop('disabled', true);
		MotherOfficePostalCode.prop('disabled', true);
	}
	
	
	function NotWorkTogetherGenerator(){
		
		var MotherCompany=$('#txtMotherCompany');
		var MotherOfficeAddress=$('#txtMotherOfficeAddress');
		var MotherOfficeCity=$('#ddlMotherOfficeCity');
		var MotherOfficeCountry=$('#ddlMotherOfficeCountry');
		var MotherOfficePostalCode=$('#txtMotherOfficePostalCode');
		
		MotherCompany.prop('disabled', false);
		MotherOfficeAddress.prop('disabled', false);
		MotherOfficeCity.prop('disabled', false);
		MotherOfficeCountry.prop('disabled', false);
		MotherOfficePostalCode.prop('disabled', false);
	}
	
	$("#cbxWorkTogether").on('change', function(e) {
		   e.preventDefault();

		   var checkbox = $(this);   // keep reference of checkbox
           var checked = checkbox.is(":checked");      // check checkbox status
	
			if(checked==true){
				WorkTogetherGenerator();
			}
			else
				NotWorkTogetherGenerator();
    });
		
	$('#ddlFatherOfficeCity').on('change', function (e) {
		
		if($("#cbxWorkTogether").prop("checked")==true){
		//alert("asd");
		var FatherOfficeCity=$('#ddlFatherOfficeCity option:selected');
		var MotherOfficeCity=$('#ddlMotherOfficeCity');
		MotherOfficeCity.val(FatherOfficeCity.val());
		//$('select-mother_office_city').val($('#select-father_office_city option:selected').val());
		}
		

	});
 
 
	 
	  $('#txtFatherCompany').bind('input', function(){
		if($("#cbxWorkTogether").prop("checked")==true)
		 $("#txtMotherCompany").val($('#txtFatherCompany').val());
	 });
	 
	 $('#txtFatherOfficeAddress').bind('input', function(){
		if($("#cbxWorkTogether").prop("checked")==true)
		 $("#txtMotherOfficeAddress").val($('#txtFatherOfficeAddress').val());
	 });
	 

	 $('#txtFatherOfficePostalCode').bind('input', function(){
		if($("#cbxWorkTogether").prop("checked")==true)
		 $("#txtMotherOfficePostalCode").val($('#txtFatherOfficePostalCode').val());
	 });
	 
	
	
	
	var $ParentOfficeInputs = $('#form-ParentEmployment :input');
	
	$ParentOfficeInputs.each(function() {
		$(this).prop('disabled', true);
		
	});
	
	$('#edit-parent-employment').click(function(){
		$ParentOfficeInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		if($("#cbxWorkTogether").is(":checked"))
		{
			WorkTogetherGenerator();				
		}
		$("#parent-employment-edit-section").addClass('hide');
		$("#parent-employment-save-section").removeClass('hide');
	});

	$('#cancel-edit-parent-employment').click(function(){
		reloadPage();
		$ParentOfficeInputs.each(function() {
			$(this).prop('disabled', true);
			
			$(this).removeClass('highlight');
		});
		$("#parent-employment-edit-section").removeClass('hide');
		$("#parent-employment-save-section").addClass('hide');
	});
	
	
	//$('#form-submit').click(validate);
	
	$('#btnOfficeSave').click(OfficeValidate);	
	
	$('[name="ddlMotherCountry"]').on('change',function(){
		loadMotherCity($(this).val());
	})
	
	$('[name="ddlFatherCountry"]').on('change',function(){
		loadFatherCity($(this).val());
		if($("#cbxLiveTogether").is(":checked"))
		{
			LiveTogetherGenerator();				
		}
	})
	
	
	$('[name="ddlFatherOfficeCountry"]').on('change',function(){
		loadFatherOfficeCity($(this).val());
		if($("#cbxWorkTogether").is(":checked"))
		{
			WorkTogetherGenerator();				
		}
	})
	$('[name="ddlMotherOfficeCountry"]').on('change',function(){
		loadMotherOfficeCity($(this).val());
	})
	
	$('.numeric').keypress(numeric);
});
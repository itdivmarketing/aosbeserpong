
function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}

var validate = function() {

			  
	if($.trim($('.front-editing [name="txtFormNumber"]').val() )== '')
		alert('Form Number is required');
	else{
		if($.trim($('.front-editing [name="ddlReligion"]').val() )== '')
			alert('Religion is required');
		else if(validateEmail($('.front-editing [name="txtEmail"]').val()) == false)
			alert('Email is not valid');
		else if($.trim($('.front-editing [name="txtEmail"]').val()) == '')
			alert('Email is required');
		else if($.trim($('.front-editing [name="txtAddress"]').val()) == '')
			alert('Address is required');
		else if($.trim($('.front-editing [name="ddlCity"]').val() )== '')
			alert('City is required');
		else if($.trim($('.front-editing [name="ddlStayingWith"]').val() )== '')
			alert('Staying With is required');
		else if($.trim($('.front-editing [name="ddlEmergencyContact"]').val() )== '')
			alert('Emergency Contact is required');
		else if($.trim($('.front-editing [name="txtCellPhone"]').val() )== '')
			alert('Cellphone is required');	
		else if(validateNumeric($('.front-editing [name="txtCellPhone"]').val()) == false)
			alert('Cellphone is not valid');	
		else if($.trim($('.front-editing [name="txtPhone"]').val() )!= '' && validateNumeric($('.front-editing [name="txtPhone"]').val()) == false)
			alert('Phone is not valid');
		else
		{	
			  $('.front-editing').find('input:text').each(function(){
					$(this).val($.trim($(this).val()));
				});

			$('.front-editing').submit();
		}
	}
}

	function popup_SearchSchool(){

		var not_enable = $('#txtSchoolName').prop('disabled');
		if(not_enable==1){

		}else{
      	$.fancybox({
        	href 		: '#internal-popup-school',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            		
            }
    	});
		$('[name=hfPage]').val("1");
		$("#internal-popup-school #btnSearch").click(function(){
			$('[name=hfPage]').val("1");
			$('.prev').on('click',function(e){
		
				if($(this).hasClass('disable')){
					e.preventDefault();
					return;
			}
				$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
				Search();
			});

		$('.next').on('click',function(e){
				//alert($('[name=hfPage]').val());
			if($(this).hasClass('disable')){
					e.preventDefault();
					return;
				}
			$('[name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
				
			Search();
		});	
		});
		}
	}
	
	function Search(){
		var txtSearch=$.trim($('.fancybox-inner #txtSearchSchoolName').val());
	
		$.ajax({
			type: "post",
			url:  SiteURL+'entry/registrant_background/search_school',
			data: {
			TypeOfSchoolID:$('#ddlTypeOfSchool').val(),
			txtSearch:$.trim($('.fancybox-inner #txtSearchSchoolName').val()),
			hfPage:$.trim($('[name=hfPage]').val()),
			},
			dataType: "json",
			beforeSend: function(){
				var Table=$("#TableSearchSchool");
				//var ImgLoading=$("<tr><td colspan=3><img style='width:50px;height:50px' src='<?php echo base_url();?>images/loading.gif' title='Loading...' class='loading' /></td></tr>");
				//Table.append(ImgLoading);
			},
			success: function(data){
					if(data.status == 'success') {
							TempData=new Array();
							$("#internal-popup-school #TableSearchSchool > tbody:last").children().remove();
							if(!data.data.data.result){
								alert("There is No data");
								$('#internal-popup-school #paging').hide();
							}
							else{
									//alert("DATA EXIST");
									$('#internal-popup-school #paging').show();
								}
								
								
								
								var Table=$("#internal-popup-school #TableSearchSchool");
									for(var i in data.data.data.result){
										var Row=$("<tr>");
										
										if(!data.data.data.result[i].SchoolID)
											data.data.data.result[i].SchoolID="-";
										if(!data.data.data.result[i].SchoolName)
											data.data.data.result[i].SchoolName="-";	
										
										var td="<td><a href='' class='selector'>select</a></td>";
										Row.append(td);
										for(var j=0;j<2;j++){
											var Text="";
											switch(j){
												case 0:Text=data.data.data.result[i].SchoolID;	
													break;
												case 1:Text=data.data.data.result[i].SchoolName;
													break;
											}
											
											Row.append( $("<td>").text(Text) );
										}
										
											
										var Data={"SchoolID":data.data.data.result[i].SchoolID,"SchoolName":data.data.data.result[i].SchoolName,"TypeOfSchoolId": data.data.data.result[i].TypeOfSchoolId}										
										TempData.push(Data);
										Table.append(Row);
									}	

																
					} 
					else {
						alert(data.error);
					}
					if(data.data.data.result){
							Show_Pagination({
								'Selector' : '#paging',
								'TotalPage' : data.data.data.pagination.last,
								'CurrentPage' : data.data.data.pagination.current,
								'PreviousPage' : data.data.data.pagination.previous,
								'NextPage' : data.data.data.pagination.next,
								'InfoPage' : data.data.data.pagination.info
								});
					}
				//$('[name=hfPage]').val($(this).attr('data-page'));		
			},
			complete: function(){
					$('.selector').on('click',function(e){
							e.preventDefault();
							var myRow = $(this).parent().parent().index();
							$('#txtSchoolName').val(TempData[myRow].SchoolName);
							$('#hfSearchSchool').val(TempData[myRow].SchoolID);
							$.fancybox.close();

							if($('#txtSchoolName').val(TempData[myRow].SchoolName)){						
								$('#ddlTypeOfSchool').val(TempData[myRow].TypeOfSchoolId);
							}
					});

					$('.page-click').on('click',function(){
						$('[name=hfPage]').val($(this).attr('data-page'));
						Search();
						
						//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
					});	
			},
			error: function(xhr, textStatus, errorThrown){
				alert('failed');
			}
		})
	
			
	}

	

var reloadPage = function(){
	$('#frmSearch').submit();
};
$(document).ready(function()
{
function LoadSchoolName(){	
		var Obj = $('[name="ddlSchoolName"]');
		var SchoolListData=new Array();	
		var Value=$('[name="ddlTypeOfSchool"]').val()==""?"":$('[name="ddlTypeOfSchool"]').val();
		
		if(Value==""){
			return false;
		}
		
		$.ajax({
			type: "post",
			url: SiteURL+'entry/registrant_background/load_school_name/'+Value,
			dataType: "json",
			async :false,
			success: function(data){
			
					Obj.empty().append($('<option value="">--Please Choose--</option>'));
					if(data.SchoolName)
					{
						for(var i in data.SchoolName)
						{
							Obj.append($('<option value="'+$.trim(data.SchoolName[i].SchoolID)+'">'+data.SchoolName[i].SchoolName+'</option>'));
						}
					}
				
				}
		})
}

function GetBGSchoolData(){	

		var Param=$('[name="hfFormNumber"]').val();
		$.ajax({
			type: "post",
			url: SiteURL+'entry/registrant_background/get_bg_school_data/'+Param,
			dataType: "json",
			success: function(data){

					if(data.BGSchoolData)
					{
						for(var i in data.BGSchoolData)
						{
							var InitialData={"PrevSchoolType":"",
							"SchoolName":data.BGSchoolData[i].SchoolName,
							"SchoolID":data.BGSchoolData[i].SchoolID,
							"LastYearLevel":data.BGSchoolData[i].LastYearLevel,
							"StartAttendedYear":data.BGSchoolData[i].YearAttended,
							"EndAttendedYear":data.BGSchoolData[i].YearWithDrawn,
							"BeginSchoolYear":data.BGSchoolData[i].BeginningOfSchoolYear,
							"EndSchoolYear":data.BGSchoolData[i].EndOfSchoolYear,
							"TypeOfSchoolID":data.BGSchoolData[i].TypeOfSchoolID,
							};

							RegistrantBGSchoolData.push(InitialData) ;
		
						}
					}
				
				}
		})
}



function GetBGLanguageData(){	

		var Param=$('[name="hfFormNumber"]').val();
		$.ajax({
			type: "post",
			url: SiteURL+'entry/registrant_background/get_bg_language_data/'+Param,
			dataType: "json",
			success: function(data){

					if(data.BGLanguageData)
					{
						
						for(var i in data.BGLanguageData)
						{
							var InitialData={
							"LanguageID":$.trim(data.BGLanguageData[i].LanguageID),
							"LanguageName":$.trim(data.BGLanguageData[i].LanguageName),
							"FirstLanguage":$.trim(data.BGLanguageData[i].FirstLanguage)
							};

							RegistrantBGLanguageData.push(InitialData) ;
							
						}
					}
				
				}
		})
}

	
$("#TableBGSchool").on('click',".IconColumn", function(e){
	
	//debugger;
	var myRow = $(this).parent().index();
	var target= $(e.target);
	if(target.is('.icon-trash')){
	//alert(RegistrantBGSchoolData[0].SchoolName);
	$(this).parent().remove();
	//alert(myRow);
	RegistrantBGSchoolData.splice(myRow,1) ;
	OnBGSchoolUpdate=false;
	$("#btnAddBGSchool").text("Add New");
	}
	
	if(target.is('.icon-edit')){
			OnBGSchoolUpdate=true;
			$("#btnAddBGSchool").text("Edit");
			BGSChoolBGLanguageActiveRow=myRow;
		
			$('#ddlTypeOfSchool').val("");
			$('#ddlTypeOfSchool').val(RegistrantBGSchoolData[myRow].TypeOfSchoolID);
			
			//LoadSchoolName();
			
			$('#txtSchoolName').val(RegistrantBGSchoolData[myRow].SchoolName);
			$('#hfSearchSchool').val(RegistrantBGSchoolData[myRow].SchoolID);
			$('#ddlYearLevel').val(RegistrantBGSchoolData[myRow].LastYearLevel);
			
			
			$('#txtStartAttendedYear').val(RegistrantBGSchoolData[myRow].StartAttendedYear);
			$('#txtEndAttendedYear').val(RegistrantBGSchoolData[myRow].EndAttendedYear);
			$('#ddlBeginSchoolYear').val(RegistrantBGSchoolData[myRow].BeginSchoolYear);
			$('#ddlEndSchoolYear').val(RegistrantBGSchoolData[myRow].EndSchoolYear);
			
	}

});	




function validateNumeric($text) {
		  var Reg = /^\d+$/;
		  if( !Reg.test( $text ) ) {
			return false;
		  } else {
			return true;
		  }
	}
	
	function ValidateAttendedYear(text1,text2,option){
		var CurrYear=parseInt((new Date).getFullYear());
		if(option=="Start"){
			if( validateNumeric(text1)==false||text1.length!=4)
				return option+" Attended Year Must be 4 chars Numeric";
		}
		else{
			if( validateNumeric(text2)==false||text2.length!=4)
				return option+" Attended Year Must be 4 chars Numeric";

		}
		
		if(parseInt(text1)<1995)
			return "Start Attended Year Must be bigger or equal than 1995";
		if(parseInt(text1)>CurrYear)
			return "Start Attended Year Can't be bigger than current Year";
		if(parseInt(text1)>parseInt(text2))
			return "Start Attended Year Must be Less Or Equal Than End Atended Year";
			/*
		if(parseInt(text2)-parseInt(text1)>1)
			return "Start and End Attended Year Only Can Have 1 Year of Difference";
	*/
		return "";
	}
	
	
	function ValidateSchoolYear(text1,text2){
		/*
		if(parseInt(text1)>parseInt(text2))
			return "Begin School Year Must be Less Or Equal Than End School Year";
		return "";
		*/
		return "";
	}


var AddBGSChool=function(){
	//alert("asd");
	//e.preventDefault();
	
	var PrevSchoolType=$('#ddlTypeOfSchool option:selected ');
	var SchoolName=$('#txtSchoolName');
	var SchoolID=$('#hfSearchSchool').val();
	var LastYearLevel=$('#ddlYearLevel option:selected ');

	var StartAttendedYear=$('#txtStartAttendedYear');
	var EndAttendedYear=$('#txtEndAttendedYear');
	var BeginSchoolYear=$('#ddlBeginSchoolYear option:selected ');
	var EndSchoolYear=$('#ddlEndSchoolYear option:selected ');
	
	
	var IsError=false;
		$( ".field-error" ).hide();
		
	//if(PrevSchoolType.val()==""){
	//	alert("Please Select Type Of School");
	//	return false;
	//}	
		
	//alert(SchoolName);
	if(SchoolName.val()=="N/A") {
			if($.trim(StartAttendedYear.val())!=""||$.trim(EndAttendedYear.val())!=""){
				var ErrorText=ValidateAttendedYear($.trim(StartAttendedYear.val()),$.trim(EndAttendedYear.val()),"Start");
				
				if(ErrorText!=""){
					alert(ErrorText);
					IsError=true;
				
				}
				
				else{
					
					ErrorText=ValidateAttendedYear($.trim(StartAttendedYear.val()),$.trim(EndAttendedYear.val()),"End");
					if(ErrorText!=""){
						alert(ErrorText);
						IsError=true;
					}
				}
			}
			if($.trim(BeginSchoolYear.val())!=""||$.trim(EndSchoolYear.val())!=""){
				
				if($.trim(BeginSchoolYear.val())!=""){
				
					if($.trim(EndSchoolYear.val())==""){
							alert("End School Year Field is Required");
							IsError=true;
					}
				}
			
				
				if($.trim(EndSchoolYear.val())!=""){
					if($.trim(BeginSchoolYear.val())==""){
						alert("Begin School Year Field is Required");
						IsError=true;
					}
				}
				
				var ErrorText=ValidateSchoolYear($.trim(BeginSchoolYear.val()),$.trim(EndSchoolYear.val()));
					
				if(ErrorText!=""){
					alert(ErrorText);
					IsError=true;
			
				}
		
			}
			
	}
	else{
		
		if(SchoolName.val()==""){
			alert("School Name field is Required");
			IsError=true;
		}
		
		if(LastYearLevel.val()==""){
			alert("Last Year Level Field is Required");
			IsError=true;
		}
		
	
		
		if($.trim(StartAttendedYear.val())==""){
				alert("Start Attended Year Field is Required");
				IsError=true;		
		}
	
		else if($.trim(EndAttendedYear.val())==""){
			
				alert("End Attended Year Field is Required");
				IsError=true;
			
		}
	
		
		else{
			
			var ErrorText=ValidateAttendedYear($.trim(StartAttendedYear.val()),$.trim(EndAttendedYear.val()),"Start");
			
			if(ErrorText!=""){
				alert(ErrorText);
				IsError=true;
			
			}
			
			else{
				ErrorText=ValidateAttendedYear($.trim(StartAttendedYear.val()),$.trim(EndAttendedYear.val()),"End");
				if(ErrorText!=""){
					alert(ErrorText);
					IsError=true;
				}
			}

		
		}
		

		
		if(BeginSchoolYear.val()==""){
		
				alert("Begin School Year Field is Required");
				IsError=true;
			
		}
		
		else if(EndSchoolYear.val()==""){
		
				alert("End School Year Field is Required");
				IsError=true;
			
		}
		
		else{
			var ErrorText=ValidateSchoolYear($.trim(BeginSchoolYear.val()),$.trim(EndSchoolYear.val()));
			
			if(ErrorText!=""){
				alert(ErrorText);
				IsError=true;
			
			}
		
		}
		
	}
				

		for(var i=0;i<RegistrantBGSchoolData.length;i++){
					
				if($.trim(EndAttendedYear.val())==$.trim(RegistrantBGSchoolData[i].EndAttendedYear)){
					//alert("h1");
					if($.trim(StartAttendedYear.val())==$.trim(RegistrantBGSchoolData[i].StartAttendedYear)){
						//alert("h2");
					if(!($.trim(EndAttendedYear.val())==$.trim(RegistrantBGSchoolData[i].EndAttendedYear)&&$.trim(StartAttendedYear.val())==$.trim(RegistrantBGSchoolData[i].StartAttendedYear)&&OnBGSchoolUpdate==true)){
					//alert("h3");
					alert("There Is Already List Data With the Same Start/End Attended Year ");
					IsError=true;
					break;
					}
					
					}
				}
		}
				
				
		if(IsError==true){
			alert("Data Not Yet Complete \n Data belum lengkap \n\n Please Check Again \n Mohon Periksa Ulang");
			return false;
		}
		
		
	
	
	
	var Data={"PrevSchoolType":PrevSchoolType.val(),"SchoolName":SchoolID==""?'-':SchoolName.val(),"SchoolID":SchoolID,"LastYearLevel":LastYearLevel.val(),"StartAttendedYear":StartAttendedYear.val(),
	"EndAttendedYear":EndAttendedYear.val(),"BeginSchoolYear":BeginSchoolYear.val(),"EndSchoolYear":EndSchoolYear.val(),"TypeOfSchoolID":PrevSchoolType.val()};
	//alert("sc"+Data.SchoolID);
	if(OnBGSchoolUpdate==false){
	RegistrantBGSchoolData.push(Data) ;
	}
	else
	RegistrantBGSchoolData[BGSChoolBGLanguageActiveRow]=Data;
	
	var row = $("<tr>");
	var icon="<td class='IconColumn'><i class='icon icon-trash'></i>&nbsp;<i class='icon icon-edit'></i></td>";	
	//var column=$("<td>").append(icon));
	row.append(icon);
	for(var i=0;i<6;i++){
		var Text="";
		switch(i){
			case 0:Text=SchoolID==""?'-':SchoolName.val();
				break;
			case 1:Text=LastYearLevel.val()==""?'-':LastYearLevel.text();
				break;
			case 2:Text=StartAttendedYear.val()==""?'-':StartAttendedYear.val();
				break;
			case 3:Text=EndAttendedYear.val()==""?'-':EndAttendedYear.val();
				break;
			case 4:Text=BeginSchoolYear.val()==""?'-':BeginSchoolYear.val();
				break;
			case 5:Text=EndSchoolYear.val()==""?'-':EndSchoolYear.val();
				break;
		}
		if(OnBGSchoolUpdate==false){
			
			row.append( $("<td>").text(Text) );	
			

		}else{
			$('#TableBGSchool tr').eq(BGSChoolBGLanguageActiveRow+1).find('td').eq(i+1).html(Text);
		}
	}
	
	if(OnBGSchoolUpdate==false){
			$("#TableBGSchool").append(row);

	}

	OnBGSchoolUpdate=false;
	$("#btnAddBGSchool").text("Add New");

}




	var OnBGSchoolUpdate=false;
	var BGSChoolBGLanguageActiveRow=0;
	var $inputs = $('.front-editing :input');
	var RegistrantBGSchoolData=new Array();
	
	GetBGSchoolData();
	
	
	$inputs.each(function() {
		$(this).prop('disabled', true);
	});
	
		$('#TableBGSchool .icon').addClass('hide');
		$('#btnAddBGSchool').addClass('hide');
		$('#btnAddBGLanguage').addClass('hide');

		
	//$('[name="ddlTypeOfSchool"]').on('change', function(){LoadSchoolName()});
	
	$("#btnAddBGSchool").click(AddBGSChool);
	
	$('#edit-personal').click(function(){
		$("#save-section-bg-school").removeClass('hide');
		$("#edit-section-bg-school").addClass('hide');
		$("#form-BGSchool :input").each(function() {
			$(this).prop('disabled', false);
		});
		$('#TableBGSchool .icon').removeClass('hide');
		$('#btnAddBGSchool').removeClass('hide');
	});

	$('#cancel-edit-personal').click(function(){
		$("#edit-section-bg-school").removeClass('hide');
		$("#save-section-bg-school").addClass('hide');
		reloadPage();
		$("#form-BGSchool :input").each(function() {
			$(this).prop('disabled', true);
		});
		
		$('#TableBGSchool .icon').addClass('hide');
		$('#btnAddBGSchool').addClass('hide');
		OnBGSchoolUpdate=false;
		$("#btnAddBGSchool").text("Add New");
	});
	
	$('#edit-language').click(function(){
		$('#ddlLanguage').prop('disabled', false);
		$("#TableBGLanguage .icon").removeClass('hide');
		$("#language-edit-section").addClass('hide');
		$("#language-save-section").removeClass('hide');
		$('#btnAddBGLanguage').removeClass('hide');

	});

	$('#cancel-edit-language').click(function(){
			reloadPage();
		$('#ddlLanguage').prop('disabled', true);
	
		$("#TableBGLanguage .icon").addClass('hide');
		$("#language-save-section").addClass('hide');
		$("#language-edit-section").removeClass('hide');
		$('#btnAddBGLanguage').addClass('hide');
		OnBGLanguageUpdate=false;
		$("#btnAddBGLanguage").text("Add New");
	});
	
	
	
	
	$('#form-submit').click(function(e){
		e.preventDefault();
		
		OnBGSchoolUpdate=false;
		$("#btnAddBGSchool").text("Add New");
		for(var i=0;i<RegistrantBGSchoolData.length;i++){

			$('#form-BGSchool').append('<input type="hidden" name="hfSchoolID[]" value="'+RegistrantBGSchoolData[i].SchoolID+'" />');
			$('#form-BGSchool').append('<input type="hidden" name="hfSchoolName[]" value="'+RegistrantBGSchoolData[i].SchoolName+'" />');
			$('#form-BGSchool').append('<input type="hidden" name="hfYearLevelID[]" value="'+RegistrantBGSchoolData[i].LastYearLevel+'" />');
			$('#form-BGSchool').append('<input type="hidden" name="hfStartAttendedYear[]" value="'+RegistrantBGSchoolData[i].StartAttendedYear+'" />');
			$('#form-BGSchool').append('<input type="hidden" name="hfEndAttendedYear[]" value="'+RegistrantBGSchoolData[i].EndAttendedYear+'" />');
			$('#form-BGSchool').append('<input type="hidden" name="hfBeginSchoolYear[]" value="'+RegistrantBGSchoolData[i].BeginSchoolYear+'" />');
			$('#form-BGSchool').append('<input type="hidden" name="hfEndSchoolYear[]" value="'+RegistrantBGSchoolData[i].EndSchoolYear+'" />');
			
		}
		
		$('#form-BGSchool').submit();
	});
	
	//language
	
	var RegistrantBGLanguageData=new Array();
	var OnBGLanguageUpdate=false;
	var BGLanguageActiveRow=0;
	
	
	GetBGLanguageData();
	
	$("#TableBGLanguage").on('click', ".IconColumn", function(e) {
	
	//debugger;
	var myRow = $(this).parent().index();
	var target= $(e.target);
	if(target.is('.icon-trash')){
	$(this).parent().remove();
	//alert(myRow);
	RegistrantBGLanguageData.splice(myRow,1) ;
	//alert();
	OnBGLanguageUpdate=false;
	$("#btnAddBGLanguage").text("Add New");
	}
	
	if(target.is('.icon-edit')){
	OnBGLanguageUpdate=true;
	$("#btnAddBGLanguage").text("Edit");
	BGLanguageActiveRow=myRow;
	
	$('#ddlLanguage').val(RegistrantBGLanguageData[myRow].LanguageID);

	
	}
	
	});
	
	
	
	
	
	$("#btnAddBGLanguage").click(function(e){
	
		e.preventDefault();
			
		var Language=$('#ddlLanguage option:selected');

		var IsError=false;
		
		$( ".field-error" ).hide();
		
		if(Language.val()==""){
			alert("language Field is Required");

			IsError=true;
		}
		
			

		var FirstLanguage=0;
		
		if(OnBGLanguageUpdate==true){
			if(RegistrantBGLanguageData[BGLanguageActiveRow].FirstLanguage==1)
				FirstLanguage=1;
		}
		
		var Data={"LanguageID":Language.val(),"LanguageName":$.trim(Language.text()),"FirstLanguage":FirstLanguage};
		
		for(var i=0;i<RegistrantBGLanguageData.length;i++){
			if(Data.LanguageID==RegistrantBGLanguageData[i].LanguageID){
				if(!(RegistrantBGLanguageData[BGLanguageActiveRow].LanguageID==Data.LanguageID&&OnBGLanguageUpdate==true)){
					alert("There is Already "+Data.LanguageName+" Language In The List");
					IsError=true;
				}
			}
		
		}
		
	
		
		if(IsError==true){
			alert("Data Not Yet Complete \n Data belum lengkap \n\n Please Check Again \n Mohon Periksa Ulang");
			return false;
		}
		
		
		if(OnBGLanguageUpdate==false){
		RegistrantBGLanguageData.push(Data) ;
		}
		else
		RegistrantBGLanguageData[BGLanguageActiveRow]=Data ;
		
		
	
		var row = $("<tr>");
		var icon="<td class='IconColumn'><i class='icon icon-trash'></i>&nbsp;<i class='icon icon-edit'></i></td>";	
		//var column=$("<td>").append(icon));
		row.append(icon);
		
	
			var Text=Data.LanguageName;

			if(OnBGLanguageUpdate==false){
				row.append( $("<td>").text(Text) );
			}else{
				$('#TableBGLanguage tr').eq(BGLanguageActiveRow+1).find('td').eq(1).html(Text);
			}
		
		var CheckBox="<td class='CheckBoxColumn'><input type='checkbox' id='cbxSame' class='cbxSame' /></td>";
		row.append(CheckBox);
		if(OnBGLanguageUpdate==false){
			$("#TableBGLanguage").append(row);
		}else{
			//$('#TableBGLanguage tr').eq(BGLanguageActiveRow+1).find('td').eq(1).html(Text);
		}
		OnBGLanguageUpdate=false;
		$("#btnAddBGLanguage").text("Add New");
		});

	$(".cbxSame").on('click', function(e) {
		
			//e.stopPropagation();
	
			//this.parent('<td>').click();
			var checked = $(this).is(":checked");
			var myRow = $(this).closest('tr').index();
			//alert(checked+"Row"+myRow);
			if(checked==true)
				RegistrantBGLanguageData[myRow].FirstLanguage=1;
			else
				RegistrantBGLanguageData[myRow].FirstLanguage=0;	

			
    });
	
	

		
		
	$("#btnSaveBGLanguage").click(function(e){
		
		e.preventDefault();
		/*
		if(StudentStatus!=0)
		return false;
		*/

		OnBGLanguageUpdate=false;
		$("#btnAddBGLanguage").text("Add New");
		
		if(RegistrantBGLanguageData.length<=0){
			alert("Language can not be empty");
			return;
		}
		for(var i=0;i<RegistrantBGLanguageData.length;i++){

			$('#form-BGLanguage').append('<input type="hidden" name="hfLanguageID[]" value="'+RegistrantBGLanguageData[i].LanguageID+'" />');
			$('#form-BGLanguage').append('<input type="hidden" name="hfLanguageName[]" value="'+RegistrantBGLanguageData[i].LanguageName+'" />');
			$('#form-BGLanguage').append('<input type="hidden" name="hfFirstLanguage[]" value="'+RegistrantBGLanguageData[i].FirstLanguage+'" />');
		}
		
		$('#form-BGLanguage').submit();
		
	});
	
						
	
});
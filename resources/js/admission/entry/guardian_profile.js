function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}


	
function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}
var reloadPage = function(){
	$('#frmSearch').submit();
};

function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}


	if( $('#modal .datepicker').length > 0 ){
		$('#modal .datepicker').datepicker({ dateFormat: 'dd M yy' });
	}
	

function ProfileValidate(EmergencyContact, StayingWithID) {	  
	if($.trim($('#form-GuardianProfile [name="txtFormNumber"]').val() )== '')
		alert('Form Number is required');
	else{
		//var valid=1;
		
			//valid=0;
			if($.trim($('#form-GuardianProfile [name="txtGuardianName"]').val())==""){
				alert("Name Field  is Required");
			}
			else if($.trim($('#form-GuardianProfile [name="txtGuardianAddress"]').val())==""){
				alert("Address field  is Required");
			}
			else if($('#ddlGuardianLifeStatus').val()!=2 && $.trim($('#form-GuardianProfile [name="txtGuardianEmail"]').val())==""){
				alert("Email field  is Required");
			}
			else if($.trim($('#form-GuardianProfile [name="txtGuardianCellPhone"]').val())==""){
				alert("CellPhone Field is Required");
			}
			else if($.trim($('#form-GuardianProfile [name="txtGuardianEmail"]').val()) != ''&& validateEmail($('#form-GuardianProfile [name="txtGuardianEmail"]').val()) == false )
				alert('Guardian Email is not valid');
			else if($.trim($('#form-GuardianProfile [name="txtGuardianCellPhone"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianCellPhone"]').val()) == false)
				alert('Guardian Cellphone is not valid');	
			else if($.trim($('#form-GuardianProfile [name="txtGuardianPhone"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianPhone"]').val()) == false)
				alert('GuardianPhone is not valid');	
			else if($.trim($('#form-GuardianProfile [name="txtGuardianFax"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianFax"]').val()) == false)
				alert('Guardian Fax is not valid');	
			else if($.trim($('#form-GuardianProfile [name="txtGuardianPostalCode"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianPostalCode"]').val()) == false)
				alert('Guardian Postal Code is not valid');
			else if($.trim($('#form-GuardianProfile [name="txtGuardianEmail"]').val()) != ''&& validateEmail($('#form-GuardianProfile [name="txtGuardianEmail"]').val()) == false )
				alert('Email is not valid');
			else if($.trim($('#form-GuardianProfile [name="txtGuardianCellPhone"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianCellPhone"]').val()) == false)
				alert('Mother Cellphone is not valid');	
			else if($.trim($('#form-GuardianProfile [name="txtGuardianPhone"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianPhone"]').val()) == false)
				alert('Mother Phone is not valid');	
			else if($.trim($('#form-GuardianProfile [name="txtGuardianFax"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianFax"]').val()) == false)
				alert('Mother Fax is not valid');	
			else if($.trim($('#form-GuardianProfile [name="txtGuardianPostalCode"]').val() )!= '' && validateNumeric($('#form-GuardianProfile [name="txtGuardianPostalCode"]').val()) == false)
				alert('Mother Postal Code is not valid');
			else if($.trim($('#form-GuardianProfile [name="txtGuardianRIPYear"]').val())!='' && validateNumeric($.trim($('#form-GuardianProfile [name="txtGuardianRIPYear"]').val()))==false ){
					alert("Rip Year is not valid");
			}
			else if($('#form-GuardianProfile [name="ddlGuardianLifeStatus"]').val()=="2" && $.trim($('#form-GuardianProfile [name="txtGuardianRIPYear"]').val())==""){
					alert("Rip Year is Required");
			}
			else
			{	
				var $GuardianProfileInputs = $('#form-GuardianProfile :input');
		
		
				$GuardianProfileInputs.each(function() {
					$(this).prop('disabled', false);
				});

				$('#form-GuardianProfile').find('input').each(function(){
					$(this).val($.trim($(this).val()));
				});

				$('#form-GuardianProfile').submit();
			}
		
	}
		
		

}

var loadCity = function(country){
	$.ajax({
		url: SiteURL+'entry/guardian_profile/get_city/'+country,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
		success:function(data)
		{
			$('[name="ddlGuardianCity"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.City)
			{
				for(var i in data.City)
				{
					$('[name="ddlGuardianCity"]').append($('<option value="'+$.trim(data.City[i].CityID)+'">'+data.City[i].CityName+'</option>'));
				}
			}
		} 
	});
};

function checkRequirementbyLifeStatus(){
	if($('#ddlGuardianLifeStatus').val()!=2){
		$('#email-guardian-required').text("*");
	}else{
		$('#email-guardian-required').text("");
	}


	if($('#ddlGuardianLifeStatus').val()==2){
		$('#ripyear-guardian-required').text("*");
		$('#txtGuardianRIPYear').prop('readonly', false);
	}else{
		$('#ripyear-guardian-required').text("");
		$('#txtGuardianRIPYear').val("");
		$('#txtGuardianRIPYear').prop('readonly', true);
	}



}

$(document).ready(function()
{

	$('.guardiandatepicker').datepicker({
		dateFormat: 'dd M yy', 
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd M yy',
		yearRange: '-80:-20'
	});

	
	var $GuardianProfileInputs = $('#form-GuardianProfile :input');

	var EmergencyContact=$.trim($('#form-GuardianProfile [name="txtEmergencyContact"]').val() );
	var StayingWithID=$.trim($('#form-GuardianProfile [name="txtStayingWithID"]').val() );
	
	$GuardianProfileInputs.each(function() {
		$(this).prop('disabled', true);
	});

	checkRequirementbyLifeStatus();

	$('#edit-guardian-profile').click(function(){
		$GuardianProfileInputs.each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		$("#guardian-profile-edit-section").addClass('hide');
		$("#guardian-profile-save-section").removeClass('hide');
	});

	$('#cancel-edit-guardian-profile').click(function(){
		reloadPage();
		$GuardianProfileInputs.each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$("#guardian-profile-edit-section").removeClass('hide');
		$("#guardian-profile-save-section").addClass('hide');
	});
	
	$('#btnSave').click(function(){
	
	ProfileValidate(EmergencyContact,StayingWithID);
	
	});

	$('[name="ddlGuardianCountry"]').on('change',function(){
		loadCity($(this).val());
	})	
	
	
});
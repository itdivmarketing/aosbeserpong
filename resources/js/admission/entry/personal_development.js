function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}
var reloadPage = function(){
	$('#frmSearch').submit();
};

$(document).ready(function() {

function GetSicknessData(){	

		var Param=$('[name="hfFormNumber"]').val();
		$.ajax({
			type: "post",
			url: SiteURL+'entry/personal_development/get_sickness_data/'+Param,
			dataType: "json",
			success: function(data){

					if(data.SicknessData)
					{
						
						for(var i in data.SicknessData)
						{
							var InitialData={
							"SicknessID":$.trim(data.SicknessData[i].SicknessID),
							"SicknessName":$.trim(data.SicknessData[i].SicknessName),
							"Note":$.trim(data.SicknessData[i].Memo)
							};

							SicknessData.push(InitialData) ;
							
						}
					}
				
				}
		})
}

	var $SicknessInputs = $('#form-sickness :input');
	$SicknessInputs.each(function() {
		$(this).prop('disabled', true);
	});
	
	$("#btnAddSickness").addClass('hide');
	
	$('#edit-sickness').click(function(){
		$("#form-sickness :input").each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
			$('#TableSickness .icon').removeClass('hide');
		$('#sickness-edit-section').addClass('hide');
		$('#sickness-cancel-edit-section').removeClass('hide');
		$("#btnAddSickness").removeClass('hide');
	});

	$('#cancel-edit-sickness').click(function(){
		reloadPage();
		$("#form-sickness :input").each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});
		$('#TableSickness .icon').addClass('hide');
		$('#sickness-edit-section').removeClass('hide');
		$("#btnAddSickness").addClass('hide');
		$('#sickness-cancel-edit-section').addClass('hide');
		OnSicknessUpdate=false;
		$('#btnAddSickness').text("Add New");
	});
	
	
	//var StudentStatus="<?php echo $StudentStatus; ?>";
	var SicknessData=new Array();
	var OnSicknessUpdate=false;
	var SicknessActiveRow=0;
	
	GetSicknessData();
	
	
	$("#TableSickness").on('click', '.IconColumn',function(e) {
		var myRow = $(this).parent().index();
		var target= $(e.target);
		if(target.is('.icon-trash')){
			$(this).parent().remove();
			//alert(myRow);
			SicknessData.splice(myRow,1) ;
			OnSicknessUpdate=false;
			$('#btnAddSickness').text("Add New");
		}
		
		if(target.is('.icon-edit')){
			OnSicknessUpdate=true;
			$('#btnAddSickness').text("Edit");
			SicknessActiveRow=myRow;
			$('#ddlSickness').val(SicknessData[myRow].SicknessID);
			$('#txtSicknessNote').val(SicknessData[myRow].Note);
		}
	
	});
	
	$("#btnAddSickness").click(function(e){
		//alert("asd");
		e.preventDefault();

		var Sickness=$('#ddlSickness option:selected');
		var Note=$('#txtSicknessNote');
		var Data={"SicknessID":Sickness.val(),"SicknessName":$.trim(Sickness.text()),"Note":$.trim(Note.val())};
		
		var IsError=false;
		
		if($('#ddlSickness').val()==""){
				alert("Please Select Sickness Field");
				IsError=true;
		}
		
		for(var i=0;i<SicknessData.length;i++){
			if(Data.SicknessID==SicknessData[i].SicknessID){
				if(!(SicknessData[SicknessActiveRow].SicknessID==Data.SicknessID&&OnSicknessUpdate==true)){
					alert("There is Already "+$.trim(Data.SicknessName)+" Sickness In The List");
					IsError=true;
				}
			}
		}
		
		
		if(IsError==true){
			return false;
		}
		
		if(OnSicknessUpdate==false){
			SicknessData.push(Data) ;
		}
		else
			SicknessData[SicknessActiveRow]=Data ;
		
		var row = $("<tr>");
		var icon="<td class='IconColumn'><i class='icon icon-trash'></i>&nbsp;<i class='icon icon-edit'></i></td>";	
		row.append(icon);
		
	
		for(var i=0;i<2;i++){
			var Text="";
			switch(i){
				case 0:Text=Data.SicknessName;
					break;
				case 1:Text=Data.Note;
					break;
			}
			if(OnSicknessUpdate==false){
				row.append($("<td>").text(Text));
				
			}else{
				$('#TableSickness tr').eq(SicknessActiveRow+1).find('td').eq(i+1).html(Text);
			}
		}
		

		if(OnSicknessUpdate==false){
			$("#TableSickness").append(row);
		}else{
			//$('#TableSickness tr').eq(SicknessActiveRow+1).find('td').eq(1).html(Text);
		}
		OnSicknessUpdate=false;
		$('#btnAddSickness').text("Add New");
		});
		

		
		
	$("#btnSaveSickness").click(function(e){
	
		e.preventDefault();
	//	if(StudentStatus!=0)
		//	return false;
		OnSicknessUpdate=false;
		$('#btnAddSickness').text("Add New");

		for(var i=0;i<SicknessData.length;i++){

			$('#form-sickness').append('<input type="hidden" name="hfSicknessID[]" value="'+SicknessData[i].SicknessID+'" />');
			$('#form-sickness').append('<input type="hidden" name="hfSicknessName[]" value="'+SicknessData[i].SicknessName+'" />');
			$('#form-sickness').append('<input type="hidden" name="hfSicknessNote[]" value="'+SicknessData[i].Note+'" />');
		}
		
		$('#form-sickness').submit();
	});
	
	
	//health
	
	
	var $HealthInputs = $('#form-health :input');
	$HealthInputs.each(function() {
		$(this).prop('disabled', true);
	});
	$('#downloadlink').prop('disabled',true);
	
	 $('form-health #downloadlink').click(function (e) {
		if($('#health-cancel-edit-section').hasClass("hide")){
			e.preventDefault();
		}
	});
		

	$('#edit-health').click(function(){
		$("#form-health :input").each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		
		$('#health-edit-section').addClass('hide');
		$('#health-cancel-edit-section').removeClass('hide');
		
	});

	$('#cancel-edit-health').click(function(){
		reloadPage();
		$("#form-health :input").each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});

		$('#health-edit-section').removeClass('hide');
		$('#health-cancel-edit-section').addClass('hide');
	});
	
	

		//var StudentStatus="<?php echo $StudentStatus; ?>";

		

		$('#btnSaveHealth').click(function(e){
		var HealthNote=$('#form-health  #txtHealthNote');
		var HealthFile=$('#form-health #HealthFile');
		var DownloadLink=('#form-health #downloadlink');
		e.preventDefault();
		
		//if(StudentStatus!=0)
		//return false;
		

		var SavedFile=$('#form-health  #SavedFile').val();
		
		var file = $('#HealthFile').prop('files');
		
		var ErrText="";
		
			ErrText=ValidateFile(file,"health");
			if(!ErrText=="") {
				e.preventDefault();
				alert(ErrText);
				return false;
			}
		
			$('#form-health').submit();
			
	
	});
	
	
	//SchoolWithdrawal
	
	
	var $SchoolWithdrawalInputs = $('#form-SchoolWithdrawal :input');
	$SchoolWithdrawalInputs.each(function() {
		$(this).prop('disabled', true);
	});

	
	 $('#form-SchoolWithdrawal downloadlink').click(function (e) {
		if($('#SchoolWithdrawal-cancel-edit-section').hasClass("hide")){
			e.preventDefault();
		}
	});
		

	$('#edit-SchoolWithdrawal').click(function(){
		$("#form-SchoolWithdrawal :input").each(function() {
			$(this).prop('disabled', false);
			$(this).addClass('highlight');
		});
		
		$('#SchoolWithdrawal-edit-section').addClass('hide');
		$('#SchoolWithdrawal-cancel-edit-section').removeClass('hide');
		
	});

	$('#cancel-edit-SchoolWithdrawal').click(function(){
		reloadPage();
		$("#form-SchoolWithdrawal :input").each(function() {
			$(this).prop('disabled', true);
			$(this).removeClass('highlight');
		});

		$('#SchoolWithdrawal-edit-section').removeClass('hide');
		$('#SchoolWithdrawal-cancel-edit-section').addClass('hide');
	});
	
	

		//var StudentStatus="<?php echo $StudentStatus; ?>";

		

		$('#btnSaveSchoolWithdrawal').click(function(e){
		var SchoolWithdrawalote=$('#form-SchoolWithdrawal  #txtHealthNote');
		var SchoolWithdrawalFile=$('#form-SchoolWithdrawal #HealthFile');
		var DownloadLink=('#form-SchoolWithdrawal #downloadlink');
		e.preventDefault();
		
		//if(StudentStatus!=0)
		//return false;
		

		var SavedFile=$('#form-SchoolWithdrawal  #SavedFile').val();
		
		var file = $('#SchoolWithdrawalFile').prop('files');
		
		var ErrText="";
		
			ErrText=ValidateFile(file,"SchoolWithdrawal");
			if(!ErrText=="") {
				e.preventDefault();
				alert(ErrText);
				return false;
			}
		
			$('#form-SchoolWithdrawal').submit();
			
	
	});
	
	
	
	
	function ValidateFile(file,type){

	var ErrText="";
	if(type=="health")
		$('#form-health #HasFile').val("0");
	else if(type=="SchoolWithdrawal")
		$('#form-SchoolWithdrawal #HasFile').val("0");
		if(file && file.length > 0) {
			file = file[0];
			if(type=="health")
				$('#form-health #HasFile').val("1");
			else if(type=="SchoolWithdrawal")
				$('#form-SchoolWithdrawal #HasFile').val("1");

			var ext = file.name.split(/\./).pop().toLowerCase();
			//alert(file.type);
			/*
			if(file.type != 'image/jpeg' && file.type != 'image/png' && file.type != 'application/pdf'
			&& file.type != 'application/x-rar-compressed' && file.type != 'application/zip') 
			{
				ErrText='Yor file is not in jpg, jpeg, png, rar, zip, 7z or pdf format';
			}
			*/
			
			if(ext != 'jpg'  && ext != 'png' && ext != 'jpeg' && ext != 'pdf' && ext != 'rar'  && ext != 'zip'   ) {
				ErrText='Yor file is not in jpg, jpeg, png, rar, zip or pdf format';
			}
			
			else if(file.size > 6 * 1024 * 1024) {
				ErrText='Your file size can not be bigger than 6MB';
			}
		} 
		
	
		return ErrText;
	}
	


						
	
});
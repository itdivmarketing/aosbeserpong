function updateQueryStringParameter(key, value) {
	var uri = window.location.href;
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}

function validateEmail($email) {
	  var emailReg = /^(-|([\w-\.]+@([\w-]+\.)+[\w-]{2,4}))?$/;
	  if( !emailReg.test( $email ) ) {
		return false;
	  } else {
		return true;
	  }
}


function validateNumeric($text) {
	  var Reg = /^\d+$/;
	  if( !Reg.test( $text ) ) {
		return false;
	  } else {
		return true;
	  }
}

var validate = function() {

			  
	if($.trim($('.front-editing [name="txtFormNumber"]').val() )== '')
		alert('Form Number is required');
	else{
		if($.trim($('.front-editing [name="ddlReligion"]').val() )== '')
			alert('Religion is required');
	    else if($.trim($('.front-editing [name="txtEmail"]').val()) == '')
			alert('Email is required');
		else if(validateEmail($('.front-editing [name="txtEmail"]').val()) == false)
			alert('Email is not valid');
		else if($.trim($('.front-editing [name="txtCellPhone"]').val() )== '')
			alert('Cellphone is required');	
		else if(validateNumeric($('.front-editing [name="txtCellPhone"]').val()) == false)
			alert('Cellphone is not valid');	
		else if($.trim($('.front-editing [name="txtAddress"]').val()) == '')
			alert('Address is required');
		else if($.trim($('.front-editing [name="ddlCountry"]').val() )== '')
			alert('Country is required');
		else if($.trim($('.front-editing [name="ddlCity"]').val() )== '')
			alert('City is required');
		else if($.trim($('.front-editing [name="txtPostalCode"]').val() )!= '' && validateNumeric($.trim($('.front-editing [name="txtPostalCode"]').val() ))==false)
			alert('PostalCode must be numeric');
		else if($.trim($('.front-editing [name="txtPhone"]').val() )!= '' && validateNumeric($('.front-editing [name="txtPhone"]').val()) == false)
			alert('Phone is not valid');			
		else if($.trim($('.front-editing [name="ddlStayingWith"]').val() )== '')
			alert('Staying With is required');
		else if($.trim($('.front-editing [name="ddlEmergencyContact"]').val() )== '')
			alert('Emergency Contact is required');
		

		else
		{	
			  $('.front-editing').find('input:text').each(function(){
					$(this).val($.trim($(this).val()));
				});

			$('.front-editing').submit();
		}
	}
		
		

}

var loadCity = function(country){
	$.ajax({
		url: SiteURL+'entry/student_profile/get_city/'+country,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlCity"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.City)
			{
				for(var i in data.City)
				{
					$('[name="ddlCity"]').append($('<option value="'+$.trim(data.City[i].CityID)+'">'+data.City[i].CityName+'</option>'));
				}
			}
        } 
	});
};

var reloadPage = function(){
	$('#frmSearch').submit();
};

$(document).ready(function()
{
	var $inputs = $('.front-editing :input');
	
	$inputs.each(function() {
		$(this).prop('disabled', true);
	});
	
	$('#edit-personal').click(function(){
	
		$inputs.each(function() {
			$(this).prop('disabled', false);
		});
		$(".save-section").removeClass('hide');
		$(".edit-section").addClass('hide');

	});

	$('#cancel-edit-personal').click(function(){
		reloadPage();
		$inputs.each(function() {
			$(this).prop('disabled', true);
		});
		$(".edit-section").removeClass('hide');
		$(".save-section").addClass('hide');

	});
	
	$('#form-submit').click(validate);
	
	$('[name="ddlCountry"]').on('change',function(){
		loadCity($(this).val());
	})
						
	
});
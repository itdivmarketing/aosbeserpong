var edit = function(){
	var row = $(this).parents('tr');
	$("[name=hfRoleID]").val(row.attr('data-id'));
	$("[name=txtRoleName]").val(row.find('td:eq(1)').html());
	$("[name=hfDeleteStatus]").val(0);
}

var hapus = function(){
	var row = $(this).parents('tr');
	$("[name=hfRoleID]").val(row.attr('data-id'));
	$("[name=txtRoleName]").val('');
	$("[name=hfDeleteStatus]").val(1);
	
	var ask = confirm('Are you sure to Delete this record?');
	if(!ask)return;
	
	$('[name=frmLogin]').submit();
}

$(document).ready(function()
{
	$('#tblRole').on('click','.btnEdit',edit);
	$('#tblRole').on('click','.btnDelete',hapus);
});
var edit = function(){
	var row = $(this).parents('tr');
	$("[name=hfStaffGroupMapping]").val(row.attr('data-id'));
	$("[name=txtStaffEmail]").val(row.find('td:eq(1)').html());
	$("[name=hfStaffEmail]").val(row.find('td:eq(1)').html());
	$("[name=txtStaffName]").val(row.find('td:eq(2)').html());
	$("[name=ddlRole]").val(row.find('td:eq(2)').html());
	$("[name=hfDeleteStatus]").val(0);
	$("[name=frmCheckUserMapping]").submit();
}

var hapus = function(){
	var row = $(this).parents('tr');
	$("[name=hfStaffGroupMapping]").val(row.attr('data-id'));
	$("[name=txtStaffEmail]").val('');
	$("[name=txtStaffName]").val('');
	$("[name=ddlRole]").val('');
	$("[name=hfDeleteStatus]").val(1);
	
	var ask = confirm('Are you sure to Delete this record?');
	if(!ask)return;
	
	$('[name=frmSetUserMapping]').submit();
}

var validate = function(){
	if($.trim($('[name="txtStaffEmail"]').val() ) == '')
		alert('Email is required');
	else{
		$('[name=frmCheckUserMapping]').submit();
	}
}

$(document).ready(function()
{
	$('#tblUserMapping').on('click','.btnEdit',edit);
	$('#tblUserMapping').on('click','.btnDelete',hapus);
	
	$('#btnCheck').click(validate);
});
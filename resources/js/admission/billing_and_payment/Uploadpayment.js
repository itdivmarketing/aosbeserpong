jQuery(document).ready(function($) {

	$('#btnSave').click(function(e){
		e.preventDefault();
		
		var data = [];
		var checking =$('.chkAll:checked').closest('tr');
		
		for(var i=0; i<checking.length; i++){
			//if(typeof($('.chkAll:checked')[i])!='undefined'){
				var d = {};
				var that = checking[i];

				d['RegistrantId'] = $(that).attr('data-id').trim();
				d['FeeTypeId'] = $('.feetype', that).attr('feetype-id');
				d['AmountBayar'] = $('.amountPaid', that).text();
				d['TanggalBayar'] = $('.paymentDate', that).text();
				d['Notes'] = $('.notes', that).val();
			//}
			data.push(d);
		}
		
		$.ajax({
			url: SiteURL+'billing_and_payment/Uploadpayment/set_payment',
			data: {data},
			type:'post',
			dataType: 'json',
			success:function(d){
				if(d.status == 'success'){
					alert('Save was success');
					location.reload(true);
				}
			}
		})

		/*if($('.chkAll:checked').length>0){
			$('[name="frmAdd"]').submit();
		}else{
			alert('Please check it first');
		}*/
	});

	$('#chkParent').change(function(){
		if(this.checked){
			$('.chkAll').attr('checked','checked');
		}else{
			$('.chkAll').attr('checked',false);
		}
	});

	$('#UploadFilePayment').change(function(e){
		var file = e.target.files;
		if($(this).val().split('.').pop()!='txt' && $(this).val().split('.').pop()!='TXT'){
			alert('Please insert .txt file');
			$(this).val('');
		}else{
			formdata = new FormData();
			formdata.append('file',file[0]);
			$.ajax({
				url : SiteURL+'billing_and_payment/Uploadpayment/upload',
				type : 'POST',
				data : formdata,
				cache: false,
				dataType: 'json',
				processData: false,
				contentType: false,
				success: function(d){
					console.log(d);
					if(d.status == 'failed'){
						alert('The file was wrong');
					}else{
						$('#tblView tbody').empty();
						var flag = false;
						for(var i in d.data){
							for(var a in d.result[i]){
								var total = d.result[i][0].NETAmount - d.result[i][0].AmountPaid;
								var amountPaidTxt = d.data[i][4].split('.').join('').replace(',00', '');
								

								if(d.result[i][a].FeeType.trim() == 'Development Fee'){
									if(total >= amountPaidTxt){
										flag = true;
										var cell = [
										'<td><input type="checkbox" class="chkAll"></td>'
										,'<td class="regisID">'+d.data[i][2]+'</td>'
										,'<td class="feetype" feetype-id="'+d.result[i][a].FeetypeID+'">'+d.result[i][a].FeeType+'</td>'
										,'<td class="netAmount">'+d.result[i][a].NETAmount+'</td>'
										,'<td class="totalAmountPaid">'+d.result[i][a].AmountPaid+'</td>'
										,'<td class="amountPaid">'+d.data[i][4].split('.').join('').replace(',00', '')+'</td>'
										,'<td class="paymentDate">'+d.data[i][5]+'</td>'
										,'<td ><input class="notes" type="text" maxlength="500"></td>'
										];
									}else{
										flag = false;
										
										var cell = [
										'<td><input type="checkbox" class="chkAll"></td>'
										,'<td class="regisID">'+d.data[i][2]+'</td>'
										,'<td class="feetype" feetype-id="'+d.result[i][a].FeetypeID+'">'+d.result[i][a].FeeType+'</td>'
										,'<td class="netAmount">'+d.result[i][a].NETAmount+'</td>'
										,'<td class="totalAmountPaid">'+d.result[i][a].AmountPaid+'</td>'
										,'<td class="amountPaid">'+total+'</td>'
										,'<td class="paymentDate">'+d.data[i][5]+'</td>'
										,'<td ><input class="notes" type="text" maxlength="500"></td>'
										];

											if(total == 0){
												var cell = [

												];
											}
									}
								}else{
									if(flag){
										var cell = [
										
										];
									}else{
										var totalFinal = amountPaidTxt - total;
										console.log(totalFinal);
										var cell = [
										'<td><input type="checkbox" class="chkAll"></td>'
										,'<td class="regisID">'+d.data[i][2]+'</td>'
										,'<td class="feetype" feetype-id="'+d.result[i][a].FeetypeID+'">'+d.result[i][a].FeeType+'</td>'
										,'<td class="netAmount">'+d.result[i][a].NETAmount+'</td>'
										,'<td class="totalAmountPaid">'+d.result[i][a].AmountPaid+'</td>'
										,'<td class="amountPaid">'+totalFinal+'</td>'
										,'<td class="paymentDate">'+d.data[i][5]+'</td>'
										,'<td ><input class="notes" type="text" maxlength="500"></td>'
										];
									}
								}
								

								var row = $('<tr data-id="'+d.data[i][2]+'">'+cell.join()+'</tr>');


								$('#tblView tbody').append(row);
							}
						}
						if(d.data.length>0){
							$('#btnSave').show();
						}


					}
				}
			});
		}

	})
})
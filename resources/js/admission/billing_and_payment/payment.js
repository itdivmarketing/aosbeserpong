function numeric(event) 
{	
	// Backspace, tab, enter, end, home, left, right
	// We don't support the del key in Opera because del == . == 46.
	var controlKeys = [8, 9, 13, 39];
	// IE doesn't support indexOf
	var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
	// Some browsers just don't raise events for control keys. Easy.
	// e.g. Safari backspace.
	if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
	  (48 <= event.which && event.which <= 57) || // Always 0 through 9
	  isControlKey) { // Opera assigns values for control keys.
	return;
	}else{
	event.preventDefault();
	}
}

var enableTextbox = function(){
	var row = $(this).parents('tr');
	if (row.find('.check').attr('checked') == 'checked')
	{
		row.find('.txt').removeAttr('readonly');
		row.find('.txtAmountBeforeDiscount').val(row.find('td').eq(1).html());
	}
	else
	{
		row.find('.txt').attr('readonly','readonly');
		row.find('.txt').val('');
	}
};

var countNetAmount = function(){
	var row = $(this).parents('tr');
	
	if(row.find('.txtPercentageDiscount').val() < 0 || row.find('.txtPercentageDiscount').val() > 100)
	{
		alert('Discount out pf range.. valid range : 0 - 100');
		row.find('.txtNetAmount').val('');
	}
	else
	{
		var net = parseInt(row.find('td').eq(1).html()) - ( parseInt(row.find('td').eq(1).html()) * parseInt(row.find('.txtPercentageDiscount').val()) / 100 );
		row.find('.txtNetAmount').val(net);
	}
}

function validate(){
	paymentDate = new Date(Date.parse($('.fancybox-opened [name="txtPaymentDate"]').val()));
	var nowDate = new Date();
	if($('[name="hfRegistrantID"]').val() == '')
		alert('RegistrantID is required');
	else if($('[name="hfFeeType"]').val() == '')
		alert('Fee Type is required');
	else if($('[name="hfPackage"]').val() == '')
		alert('Package is required');
	else if($('[name="txtAmountPaid"]').val() == '')
		alert('Amount Paid is required');
	else if($('[name="txtPaymentDate"]').val() == '')
		alert('Payment Date is required');
	else if($('[name="ddlPaymentMethod"] option:selected').val() == '')
	{
		alert('Payment Method is required');
	}
	/*else if($('[name="txtAmountPaid"]').val() >  $('[name="hfRemainingPayment"]').val())
		alert('Amount Paid must be less than ' + $('[name="hfRemainingPayment"]').val());*/
	else if($('[name="txtAmountPaid"]').val() <= 0)
		alert('Amount Paid must be more than 0');
	else if(paymentDate > nowDate)
		alert("payment date can't be greater than today");
	else{
		/*var startDate = new Date($('#txtPaymentDate').val());
		var endDate = new Date($('#txtDueDate').val());
		
		if(startDate > endDate)
			alert('payment overdue');
		else*/
		//if ($('[name="txtPaymentID"]').val()=="" && parseInt($('[name="txtAmountPaid"]').val()) >  parseInt($('[name="hfRemainingPayment"]').val()))
		if ($('[name="txtPaymentID"]').val()=="" && parseInt($('[name="txtAmountPaid"]').val()) >  parseInt($('[name="hfRemainingPayment"]').val()))
		{
			var ask = confirm('Amount Paid is bigger than Remaining Payment. Do you want to save the money to deposit?' );
			if(!ask)return;
			
			//alert($('[name="txtPaymentID"]').val());
		}
		$('[name="frmAdd"]').submit();

	}
}



var getPaymentID = function(RegistrantID,feeTypeID, packageID){
	$.ajax({
		url: SiteURL+'billing_and_payment/payment/get_paymentID/'+RegistrantID+'/'+feeTypeID+'/'+packageID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			if(data.PaymentID)
			{
				$('[name="txtPaymentID"]').val(data.PaymentID[0].PaymentID);
				$('[name="txtAmount"]').val(data.PaymentID[0].Amount);
				$('[name="txtAmountPaid"]').val(data.PaymentID[0].Amount);
				$('[name="txtDueDate"]').val(data.PaymentID[0].DueDate);
			}
			else
			{
				$('[name="txtPaymentID"]').val('');
				$('[name="txtAmount"]').val('');
				$('[name="txtAmountPaid"]').val('');
				$('[name="txtDueDate"]').val('');
			}
        } 
	});
};

var getpackage = function(RegistrantID,feeTypeID){
	$.ajax({
		url: SiteURL+'billing_and_payment/payment/get_package/'+RegistrantID+'/'+feeTypeID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlPackage"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.packages)
			{
				for(var i in data.packages)
				{
					$('[name="ddlPackage"]').append($('<option value="'+$.trim(data.packages[i].PackageID)+'">'+data.packages[i].PackageName+'</option>'));
				}
			}
        }
	});
};


var edit = function(){
	$('.datepicker').datepicker('destroy');
	$('.datepicker').datepicker({ 
				dateFormat  : 'd-M-yy',
				changeYear  : true,
				changeMonth : true}
	);
	var row = $(this).parents('tr');
	$('[name="hfStatus"]').val('edit');
	$('[name="hfRegistrantID"]').val(row.attr('data-id'));
	$.ajax({
		url: SiteURL+'billing_and_payment/payment/get_payment/'+row.attr('data-id')+'/'+row.attr('data-package')+'/'+row.attr('data-feetype')+'/'+row.attr('data-paymentid'),
		type: 'post',
		async:false,
		dataType: "json",
        success:function(data)
        {
			$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false});

			if(data.PaymentList)
			{
				$('[name="hfRegistrantID"]').val(data.PaymentList[0].RegistrantID);
				$('[name="txtPaymentID"]').val(data.PaymentList[0].PaymentID);
				$('[name="txtFeeType"]').val(data.PaymentList[0].FeeTypeName);
				$('[name="hfFeeType"]').val(data.PaymentList[0].FeeTypeID);
				$('[name="hfPackage"]').val(data.PaymentList[0].PackageID);
				$('[name="txtPackage"]').val(data.PaymentList[0].PackageName);
				$('[name="txtAmount"]').val(data.PaymentList[0].Amount);
				$('[name="txtDueDate"]').val(data.PaymentList[0].DueDate);
				$('[name="txtAmountPaid"]').val(data.PaymentList[0].AmountPaid);
				$('[name="txtPaymentDate"]').val(data.PaymentList[0].PaymentDate);
				$('[name="txtNote"]').val(data.PaymentList[0].Notes);
				$('[name="ddlPaymentMethod"]').val(data.PaymentList[0].PaymentMethodID);
				$('[name="ddlPaymentMethod"]').trigger("change");			
				$('[name="hfPaid"]').val(data.PaymentList[0].TotalPaid);
				$('[name="hfRemainingPayment"]').val(data.PaymentList[0].Remainings);
				

				$('[name="cbNotPaid"]').on("change",function(){
					$('[name="txtAmountPaid"]').attr("readonly","readonly");
					$('[name="txtAmountPaid"]').val($('[name="txtAmount"]').val());
				});
				if(data.PaymentList[0].Status == 1)
				{
					$('[name="cbNotPaid"]').attr('checked','checked');
				}
			}
			$('.numeric').keypress(numeric);
        } 
	});
};

$(document).ready(function()
{


	$('.numeric').keypress(numeric);
	$('[name="txtAmountPaid"]').on('focus', function(){
		$('[name="txtAmountPaid"]').val('');
	});
	$('#tblView').on('click','.btnEdit',edit);
});
var statusbox = $('#status-box');
function numeric(event) 
{
	// Backspace, tab, enter, end, home, left, right
	// We don't support the del key in Opera because del == . == 46.
	var controlKeys = [8, 9, 13, 35, 36, 37, 39];
	// IE doesn't support indexOf
	var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
	// Some browsers just don't raise events for control keys. Easy.
	// e.g. Safari backspace.
	if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
	  (48 <= event.which && event.which <= 57) || // Always 0 through 9
	  isControlKey) { // Opera assigns values for control keys.
	return;
	}else{
	event.preventDefault();
	}
}

var enableTextbox = function(){
	var row = $(this).parents('tr');
	var check = row.find('.check').checked;
	if (row.find('.check').attr('checked') == 'checked')
	{
		row.find('.check').removeAttr('checked');
		row.find('.txt').attr('readonly','readonly');
		row.find('.txtNetAmount').val(row.find('td').eq(1).html());
		row.find('.txt').val('');
		
	}
	else
	{
		row.find('.check').attr('checked','checked');
		row.find('.txt').removeAttr('readonly');

	}
};

var countNetAmountFromPercentage = function(){
	var row = $(this).parents('tr');
	
	if(row.find('.txtPercentageDiscount').val() < 0 || row.find('.txtPercentageDiscount').val() > 100)
	{
		//alert('Discount out of range.. valid range : 0 - 100');
		row.find('.txtAmountDiscount').val('0');
		row.find('.txtPercentageDiscount').val('0');
		row.find('.txtNetAmount').val(row.find('td').eq(1).html());
	}
	else
	{
		var net = parseInt(row.find('td').eq(1).html()) - ( parseInt(row.find('td').eq(1).html()) * parseInt(row.find('.txtPercentageDiscount').val()) / 100 );
		row.find('.txtNetAmount').val(net);
	}
}

var countNetAmountFromAmount = function(){
	var row = $(this).parents('tr');
	
	if(row.find('.txtAmountDiscount').val() < 0 || row.find('.txtAmountDiscount').val() > parseInt(row.find('td').eq(1).html()))
	{
		//alert('Discount out of range.. valid range : 0 - '+ parseInt(row.find('td').eq(1).html()));
		row.find('.txtAmountDiscount').val('0');
		row.find('.txtPercentageDiscount').val('0');
		row.find('.txtNetAmount').val(row.find('td').eq(1).html());
	}
	else
	{
		var net = parseInt(row.find('td').eq(1).html()) - parseInt(row.find('.txtAmountDiscount').val());
		row.find('.txtNetAmount').val(net);
	}
}

var countDiscountAmount = function(){
	var row = $(this).parents('tr');
	
	if(row.find('.txtPercentageDiscount').val() < 0 || row.find('.txtPercentageDiscount').val() > 100)
	{
		alert('Discount out of range.. valid range : 0 - 100');
		row.find('.txtAmountDiscount').val('0');
		row.find('.txtPercentageDiscount').val('0');
		row.find('.txtNetAmount').val(row.find('td').eq(1).html());
	}
	else
	{
		var net = parseInt(row.find('td').eq(1).html()) * parseInt(row.find('.txtPercentageDiscount').val()) / 100;
		row.find('.txtAmountDiscount').val(net);
		
	}
}

var countDiscountPercentage = function(){
	var row = $(this).parents('tr');
	
	if(row.find('.txtAmountDiscount').val() < 0 || row.find('.txtAmountDiscount').val() > parseInt(row.find('td').eq(1).html()))
	{
		alert('Discount out of range.. valid range : 0 - '+ parseInt(row.find('td').eq(1).html()));
		row.find('.txtAmountDiscount').val('0');
		row.find('.txtPercentageDiscount').val('0');
		row.find('.txtNetAmount').val(row.find('td').eq(1).html());
	}
	else
	{
		var net = parseInt(row.find('.txtAmountDiscount').val()) /  parseInt(row.find('td').eq(1).html()) * 100;
		row.find('.txtPercentageDiscount').val(Math.round(net).toFixed(2));
	}
}

var validate = function(){
	var rows = $("#tblView tr:gt(0)");
	var valid = true;
	rows.each(function(){
		var check = $('td:nth-child(4) input', this).attr("checked");
		
		if(check == "checked")
		{
			var temp1 = $(this).find('.txtPercentageDiscount').val();
			var temp2 = $(this).find('.txtAmountBeforeDiscount').val();
			var temp3 = $(this).find('.txtNetAmount').val();
			
			if(temp1 == "" || temp2 == "" || temp3 == "")
			{
				alert('if you want to give discount, every field must be filled');
				valid=false;
				return false;
			}
		}
	});
	if(valid){
		var answer = confirm('WARNING : if registrant have ever been recorded, the system will not overwrite the existing data with the new one. All payment with "PAID" status will not be saved. Do you wish to continue?');
    	if(!answer) return;
		$('[name="frmCreateInvoice"]').submit();

	}

}

var getPackage = function(RegistrantID){
	$.ajax({
		url: SiteURL+'billing_and_payment/create_invoice/get_packages/'+RegistrantID,
		type: 'get',
		async:false,
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
        success:function(data)
        {
			$('[name="ddlPackage"]').empty().append($('<option value="">--Please Choose--</option>'));
			if(data.Packages)
			{
				for(var i in data.Packages)
				{
					$('[name="ddlPackage"]').append($('<option value="'+$.trim(data.Packages[i].PackageID)+'">'+data.Packages[i].PackageName+'</option>'));
				}
			}
			$('[name="ddlPackage"]').trigger("change");
        } 
	});
};

var autoZero = function(){
	var row = $(this).parents('tr');
	row.find('.txtNetAmount').val(row.find('td').eq(1).html());

	if (row.find('.txtPercentageDiscount').val() == '')
	{
		row.find('.txtPercentageDiscount').val('0');
	}
	if (row.find('.txtAmountDiscount').val() == '')
	{
		row.find('.txtAmountDiscount').val('0');
	}

	var PercentageDiscount = row.find('.txtPercentageDiscount').val();
	row.find('.txtPercentageDiscount').val(parseInt(PercentageDiscount));
	var AmountDiscount = row.find('.txtAmountDiscount').val();
	row.find('.txtAmountDiscount').val(parseInt(AmountDiscount));
};




$(document).ready(function()
{

	$('.numeric').keypress(numeric);
	$('[name=txtFormNumber]').change(function(){
			getPackage($(this).val());
	});
	
	$('#tblView').on('change','.check',enableTextbox);
	$('#tblView').on('keyup','.txtPercentageDiscount',autoZero);
	$('#tblView').on('keyup','.txtPercentageDiscount',countDiscountAmount);
	$('#tblView').on('keyup','.txtPercentageDiscount',countNetAmountFromPercentage);
	
	$('#tblView').on('keyup','.txtAmountDiscount',autoZero);
	$('#tblView').on('keyup','.txtAmountDiscount',countDiscountPercentage);
	$('#tblView').on('keyup','.txtAmountDiscount',countNetAmountFromAmount);
	
	$('#btnSave').click(validate);
});
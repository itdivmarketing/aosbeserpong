(function($) {	

	$(document).ready(function() {

		// init navigation
	
	

		window.binus_navigation();

		$.ajax({
			url: SiteURL+'user_session/init_menu'
		})
		.done(function(data){
			data = $.parseJSON(data);
					if(data.status == 'failed')
		{
		//	location.href = SiteURL;
		}
		else
		{
			//Fill Credential
			//fill_credential(data.credential)
			//Submenu
			var $app = $('#menu-list');
			$app.empty().append('');
			var $ul = $('<ul></ul>');
			
			function recurseNav(subData, dom, lvl) {
				lvl = lvl || 1;
				
				for(var i=0;i<subData.length;i++){
					var $li = $('<li></li>');
					var h3; // to prevent ambiguity of scope
					
					if(subData[i].StaffModuleUrl != null) 
					{
						h3 = $('<a></a>')
							.attr('href', SiteURL+subData[i].StaffModuleUrl)
							.attr('data-ref','primaryspace')
							.text(''+subData[i].StaffModuleName)
							.prepend('<i class="indicator"></i>');
						if(subData[i].StaffModuleUrl.indexOf('http://') == 0)
						{
							h3.attr('target', '_blank');
						}
						if(subData[i].StaffModuleUrl == null)
						{
							h3.removeAttr('href');
						}
					} 
					else 
					{
						//Agar cursor style sama dengan anchor
						h3 = $('<a style="cursor:pointer;"></a>')
							.append('<span class="label">'+subData[i].StaffModuleName+'</span>')
							.prepend('<i class="indicator"></i>');
						$li.addClass('has-menu');
					}
					$li.append(h3);
					if(typeof subData[i].sub != 'undefined')
												{
													/*var $ul2 = $('<ul class="subapp" style = "overflow: hidden; display: none; "></ul>').addClass('nav-lvl' + lvl);*/
													var $ul2 = $('<ul class="sub-sub-menu" style = "overflow: hidden; display: none; "></ul>');
													recurseNav(subData[i].sub, $ul2, lvl+1);
													$li.append($ul2);
					}
					dom.append($li);
				}
			}
			recurseNav(data.menu.json, $app, 1);
		}
		});
		// init pre content
		$('.login-role').binus_combobox();

		// init bootstrap
		$('body').binus_bootstrap();

		// init footer
		$('.footer').binus_footer();

		// init fullcalendar
		$event_as_an_array = [
			{
				id: 1,
				title: 'Event Title',
				start: '2015-06-20',
				color: '#0098D7',
				body : [
					{
						title   : 'Place',
						content : 'BINUS INTERNATIONAL SCHOOL SERPONG Jl. Lengkong Karya - Jelupang No. 58 Lengkong Karya Serpong, Tangerang, Indonesia'
					},
					{
						title   : 'Link',
						content : '<a href="http://google.com">http://google.com</a>'
					}
				]
			},
			{
				id: 2,
				title: 'Long Event',
				start: '2015-06-20',
				end  : '2015-06-25',
				color: '#f07273',
				body : [
					{
						title   : 'Description',
						content : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
					}
				]
			},
			{
				id: 3,
				title: 'Long Event',
				start: '2015-06-1',
				end  : '2015-06-9',
				color: '#f07273',
				body : [
					{
						title   : 'Description',
						content : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
					}
				]
			},
			{
				id: 4,
				title: 'Long Event',
				start: '2015-07-20',
				end  : '2015-07-25',
				color: '#f07273',
				body : [
					{
						title   : 'Description',
						content : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
					}
				]
			}
		];
		$event_as_json_feed = 'http://localhost/frontend/binus-bootstrap/json/json.events.php';

		$('.fullcalendar').binus_fullcalendar({
			limit  : 4,
			source : $event_as_an_array
		});

		// fancybox trigger
		$('body').find('.fancybox-trigger').fancybox({
			wrapCSS: 'popup',
			beforeShow: function() {
				$('.popup').binus_bootstrap();
			},
			afterShow: function() {
				// Update 27 May 2015
				var $popup = $('.popup');
				var $wndow = $(window);

				if ($popup.find('.fancybox-inner').height() < ($wndow.height() - 40)) {
					var $content_height = $popup.find('.fancybox-inner').height();
					var $container = ($wndow.height() - 40);
					var $margin = (($container - $content_height) / 2);

					$popup.css('margin-top', $margin + 'px');
				}

				$('.popup').css('visibility', 'visible');
			}
		});

	});

})(jQuery);
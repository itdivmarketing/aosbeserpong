<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BN_Model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	
	function sp($sp_name, $array = array())
	{
		$param_name = array();
		$param_list = array();
		foreach($array as $key => $val)
		{
			$param_name[] = '@' . $key . '=?';
			$param_list[] = $val;
		}
		
		$sp_name .= ' '. implode(', ', $param_name);
	
		return $this->db->query($sp_name, $param_list);
	}
}

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class User_session extends CI_Controller {
	
		function __construct()
		{
		
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model('user_session_model');
			$this->load->library('Soap_library');
		}
		
		public function index()
        {
         	echo 'index user_session.php';
        }	
        
        public function login()
        {
        	$UserName = $this->input->post('txtUserName');
        	$InputPassword = $this->input->post('txtPassword');
        	
			$user = array();
			
			$isAuth = array();
			$isAuth = $this->soap_library->isAuth($UserName,$InputPassword);
			
			if($isAuth->Name != null)
			{
				$Param = array();
				$Param['Email'] = $isAuth->Email;
				$UserData = $this->user_session_model->get_user_login_data($Param);

                if($UserData[0]->StaffID =="" || $UserData[0]->StaffID == null){
                    $data['errorMessage'] = "User is not authorized to access this web";
                    $this->load->view('login',$data);
                }else{
                    $P['userId'] = $UserData[0]->StaffID;
                    $GeneralSetting = $this->general_setting_model->get_general_setting($P);
                    $Period = trim($GeneralSetting[0]->Period);

                    $this->session->set_userdata(
                    array(
                        'UserEmail' => $UserData[0]->email1,
                        'Name' => $UserData[0]->nama,
                        'BinusianLevel' => $UserData[0]->level_binusian,
                        'SupervisorID' => $UserData[0]->atasan_langsung,
                        'SupervisorName' => $UserData[0]->nm_atasan_langsung,
                        'RoleID' => $UserData[0]->RoleID,
                        'BinusianID' => $UserData[0]->Binusian_id,
                        'UserId' => $UserData[0]->StaffID,
                        'Period' => $Period,
                        'Initial' =>$UserData[0]->Initial,
                    ));

                               
                    //$this->template->display('staff_desk',$data);
                    redirect(site_url().'/user_session/home');  
                }

				
			}
			else
			{
				$data['errorMessage'] = "Invalid Username and Password";
				$this->load->view('login',$data);
			}
        }
		
		public function home(){
			$user = $this->session->userdata('UserId');
            if($user =="" || $user == null){
                 alert("You Must Login !!!");
                 redirect2(base_url().'index.php');
            }		
			$this->template->display('staff_desk');
		}
        
        public function logout()
        {
        	$this->session->sess_destroy();
        	
        	redirect(site_url());
        }
        
        public function init_menu()
        {
        	$Param['staffid'] = $this->session->userdata('UserId');
        	$this->load->helper('modules');
        	$GetMenu = $this->user_session_model->get_menu($Param); 
        	$menu = build_menu($GetMenu);
			
        	//$UserRole = $this->user_session_model->get_user_role($Param);
        	 
        	/*if($UserRole)
        	{
        		foreach($UserRole as $roles)
        		{
        			if(strtolower(trim($roles->GroupName)) == 'Administrator Library')
        			{
        				$setting_menu =  array(
        						'StaffModuleID'=>'settings',
        						'StaffModuleName'=>'Pengaturan',
        						'StaffModuleIdentifier'=>'settings',
        						'ParentStaffModuleID'=>0,
        						'StaffModuleUrl'=>null,
        						'sub'=>array()
        				);
        				array_push($setting_menu['sub'],array(
        				'StaffModuleID'=>'register',
        				'StaffModuleName'=>'Staff',
        				'StaffModuleIdentifier'=>'staff',
        				'ParentStaffModuleID'=>'settings',
        				'StaffModuleUrl'=>'admisi/settings/register'
        						));
        						array_push($setting_menu['sub'],array(
        						'StaffModuleID'=>'settings_users',
        						'StaffModuleName'=>'Staff Roles',
        						'StaffModuleIdentifier'=>'users',
        						'ParentStaffModuleID'=>'settings',
        						'StaffModuleUrl'=>'admisi/settings/user_management'
        								));
        								array_push($setting_menu['sub'],array(
        								'StaffModuleID'=>'settings_roles',
        								'StaffModuleName'=>'Group Roles',
        								'StaffModuleIdentifier'=>'groups',
        								'ParentStaffModuleID'=>'settings',
        								'StaffModuleUrl'=>'admisi/settings/roles'
        										));
        										if(strtolower(trim($roles->GroupName)) == 'super admin')
        										{
        											if(!isset($setting_menu))$setting_menu = array(
        													'StaffModuleID'=>'settings',
        													'StaffModuleName'=>'Pengaturan',
        													'StaffModuleIdentifier'=>'settings',
        													'ParentStaffModuleID'=>0,
        													'StaffModuleUrl'=>null,
        													'child'=>array()
        											);
        											array_push($setting_menu['sub'],array(
        											'StaffModuleID'=>'settings_privilege',
        											'StaffModuleName'=>'Hak Akses',
        											'StaffModuleIdentifier'=>'privileges',
        											'ParentStaffModuleID'=>'settings',
        											'StaffModuleUrl'=>'admisi/settings/privileges'
        													));
        										}
        										array_push($menu,$setting_menu);
        			}
        		}
        	}*/
        	$data['menu'] = array('json' => $menu);
			
			echo json_encode($data);
			return;
        }
	}
/*	End	of	file	user_session.php	*/
/*	Location:		./controllers/user_session.php */

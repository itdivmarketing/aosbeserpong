<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class User_mapping extends CI_Controller {
		
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("user_management/user_mapping_model");
			$this->load->model("user_management/role_management_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			is_logged_in();
		}
		
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['UserMapping'] = $this->user_mapping_model->get_user_mapping();
			$data['Roles'] = $this->role_management_model->get_role();
			$data['Email'] = '';
			$data['StaffName'] = '';
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('user_management/user_mapping',$data);
        }
		
		public function check_name()
        {
			$Param['email'] = $this->input->post('txtStaffEmail');
			$BinusianData = $this->user_mapping_model->check_binusian_name($Param);
			
			$data['Roles'] = $this->role_management_model->get_role();
			if($BinusianData)
			{
				$data['StaffName'] = $BinusianData[0]->StaffName;
				$data['Email'] = $BinusianData[0]->Email;
				$data['IsNew'] = $BinusianData[0]->IsNew;
			}
			else
			{
				$data['StaffName'] = 'N/A';
				$data['Email'] = '';
			}
			$data['UserMapping'] = $this->user_mapping_model->get_user_mapping();
         	$this->template->display('user_management/user_mapping',$data);
        }
		
		public function set_user_mapping()
        {
			if($this->input->post('hfDeleteStatus') != '1')
			{
				$this->form_validation->set_rules('ddlRole', 'Role ID', 'required');
				$this->form_validation->set_rules('hfStaffEmail', 'Email', 'required');
			}
			else
			{
				$this->form_validation->set_rules('hfStaffGroupMapping', 'Staff Group Mapping ID', 'required');
			}
			if($this->form_validation->run())
			{
				if($this->input->post('hfDeleteStatus') != '1')
				{
					$Param = array(
						'delete' => 0,
						'userchange' => $this->session->userData('UserId'),
						'StaffGroupMappingID' => null_setter($this->input->post('hfStaffGroupMapping')),
						'roleID' => $this->input->post('ddlRole'),
						'email' => $this->input->post('hfStaffEmail')
					);
				}
				else
				{
					$Param = array(
						'delete' => 1,
						'userchange' => $this->session->userData('UserId'),
						'StaffGroupMappingID' => $this->input->post('hfStaffGroupMapping'),
						'roleID' => 'delete',
						'email' => 'delete'
					);
				}
				$this->user_mapping_model->set_user_mapping($Param);
				
				$data['status'] = 'success';
				$data['message'] = 'success save to database';
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>')) ;
			}
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/user_management/user_mapping');
			// echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			// $this->index();
        }
        
	}
/*	End	of	file	user_mapping.php	*/
/*	Location:		./controllers/user_management/user_mapping.php */

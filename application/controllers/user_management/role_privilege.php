<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Role_privilege extends CI_Controller {
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("user_management/role_privilege_model");
			$this->load->model("user_management/role_management_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			is_logged_in();
			
		}
		
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['Roles'] = $this->role_management_model->get_role();
			$data['current'] = $this->session->userData('RoleID');
			$Param['roleid'] = $data['current'];
			$data['Menu'] = $this->role_privilege_model->get_role_privilege($Param);
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('user_management/role_privilege',$data);
        }
		
		public function get_privilege()
        {
			$data['Roles'] = $this->role_management_model->get_role();
			$data['current'] = ($this->input->get('ddlRole') != "" ? $this->input->get('ddlRole') : $this->session->userData('RoleID'));
			$Param['roleid'] = $data['current'];
			$data['Menu'] = $this->role_privilege_model->get_role_privilege($Param);
         	$this->template->display('user_management/role_privilege',$data);
        }
        
        public function set_privilege()
        {
			$Param['roleid'] = $this->input->post('hfRoleID');
			$flag = $this->input->post('cbPrivilege');
			$Param['staffmoduleIDS'] = '';
			if ($flag)
			{
				foreach($flag as $key=>$value)
				{
					$Param['staffmoduleIDS'] .= $value . ',';
				}
			}
			else
			{
				$Param['staffmoduleIDS'] = '';
			}
			
			$Param['userchange'] = $this->session->userData('UserId');
				
			$this->role_privilege_model->set_role_privilege($Param);
			
			$data['message'] = 'success save to database';
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/user_management/role_privilege');
         	// $this->index();
		}
	}
/*	End	of	file	role_privilege.php	*/
/*	Location:		./controllers/user_management/role_privilege.php */

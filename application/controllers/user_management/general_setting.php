<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class General_setting extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
		}
		
		public function index()
        {
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			if ($GeneralSetting)
			{
				$temp = explode("-", $GeneralSetting[0]->Period);
				$data['year'] = $temp[0];
				$data['semester'] = $temp[1];
			}
			else
			{
				$data['year'] = '';
				$data['semester'] = '';
			}
         	$this->template->display('user_management/general_setting',$data);
        }
        
        public function set_general_setting()
        {
			
			$this->form_validation->set_rules('txtYear', 'Year', 'required');
			$this->form_validation->set_rules('txtSemester', 'Semester', 'required');
			
			
			if($this->form_validation->run())
			{
				$Param = array(
					'userId' => $this->session->userData('UserId'),
					'period' => trim(trim($this->input->post('txtYear')).'-'.trim($this->input->post('txtSemester')))
				);
				
				
				$this->general_setting_model->set_general_setting($Param);
				
				$this->session->set_userdata(
        			array(
						'Period' => trim(trim($this->input->post('txtYear')).'-'.trim($this->input->post('txtSemester'))),
					));
				
				
				$data['status'] = 'success';
				$data['message'] = 'success save to database';
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>')) ;
				
				
			}
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->index();
		}
	}
/*	End	of	file	role_management.php	*/
/*	Location:		./controllers/user_management/role_management.php */

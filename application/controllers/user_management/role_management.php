<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Role_management extends CI_Controller {
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("user_management/role_management_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			is_logged_in();
		}
		
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['Roles'] = $this->role_management_model->get_role();
			
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('user_management/role_management',$data);
        }	
        
        public function set_role()
        {
			if($this->input->post('hfDeleteStatus') != '1')
			{
				$this->form_validation->set_rules('txtRoleName', 'Role Name', 'required');
			}
			else
			{
				$this->form_validation->set_rules('hfRoleID', 'Role ID', 'required');
			}
			
			if($this->form_validation->run())
			{
				if($this->input->post('hfDeleteStatus') != '1')
				{
					$par = array(
						'roleName' => $this->input->post('txtRoleName')
					);
					$check = $this->role_management_model->check_roles($par);
					if ($check[0]->Message == 'No')
					{
						$Param = array(
							'delete' => 0,
							'AuditUserName' => $this->session->userData('UserId'),
							'roleID' => null_setter($this->input->post('hfRoleID')),
							'roleName' => $this->input->post('txtRoleName')
						);
					}
					else
					{
						$data['status'] = 'failed';
						$data['message'] = 'Role Name already exists in database';
						goto a;
					}
				}
				else
				{
					$Param = array(
						'delete' => 1,
						'AuditUserName' => $this->session->userData('UserId'),
						'roleID' => $this->input->post('hfRoleID'),
						'roleName' => 'delete'
					);
				}
				
				$this->role_management_model->set_role($Param);
				
				$data['status'] = 'success';
				$data['message'] = 'success save to database';
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = 'Role Name field is required';
				
			}
			a :
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			//$this->index();
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/user_management/role_management');
		}
	}
/*	End	of	file	role_management.php	*/
/*	Location:		./controllers/user_management/role_management.php */

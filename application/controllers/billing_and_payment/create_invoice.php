<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Create_invoice extends CI_Controller {
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("billing_and_payment/create_invoice_model");
			$this->load->model("user_management/general_setting_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}

		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
         	$this->template->display('billing_and_payment/create_invoice',$data);
        }

        public function search($txtFormNumber = null, $ddlPackage = null)
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$input = $this->input->get();
			if($input['txtFormNumber'] == '' && $txtFormNumber == '')
			{
				$data['message'] = 'Form Number Required';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			else if ($input['ddlPackage'] == '' && $ddlPackage == '')
			{
				$data['message'] = 'Package Required';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			else
			{
				$Param['RegistrantID'] = (isset($txtFormNumber) ? $txtFormNumber : $input['txtFormNumber']);
				$data['Sibling']= $this->create_invoice_model->get_sibling($Param);
				$Param['Period'] = $this->period;
				$Param['PackageID'] = (isset($ddlPackage) ? $ddlPackage : $input['ddlPackage']);
				$data['Packages'] = $this->get_packages((isset($txtFormNumber) ? $txtFormNumber : $input['txtFormNumber']), true);
				$data['PaymentList']= $this->create_invoice_model->get_payment_registrant($Param);
			}
			$data['Packages'] = $this->get_packages((isset($txtFormNumber) ? $txtFormNumber : $input['txtFormNumber']), true);
			$this->template->display('billing_and_payment/create_invoice',$data);
		}
		
		public function get_packages($registrantID, $postback = false)
        {
			$Param['RegistrantID'] = $registrantID;
			$data['Packages'] = $this->create_invoice_model->get_packages($Param);
			
			if ($postback) return $data['Packages'];
			else echo json_encode($data);
		}

		public function set_payment()
        {
			$input = $this->input->post();
			$d = array();
			for($i = 0 ; $i < sizeof($input['hfFeeType']) ; $i++)
			{
				$d['RegistrantId'] = $input['hfRegistrantID'][$i];
				$d['PaketId'] = $input['hfPackageID'][$i];
				$d['FeeTypeId'] = $input['hfFeeType'][$i];
				$d['Amount'] = $input['hfAmount'][$i];
				$d['TanggalJatuhTempo'] = sql_srv_date($input['hfDueDate'][$i]);
				if(isset($input['cbDiscount']))
				{
					if(in_array($input['hfDiscount'][$i],$input['cbDiscount']))
					{
						$d['FlagDiskon'] = '1';
						$d['PersenDiskon'] = (float)$input['txtPercentageDiscount'][$input['hfDiscount'][$i]];
						//print_r((float)'4.75' );die;
						$d['DiskonDariHarga'] = $input['txtAmountBeforeDiscount'][$input['hfDiscount'][$i]];
						$d['NetAmount'] = $input['txtNetAmount'][$input['hfDiscount'][$i]];
					}
					else
					{
						$d['FlagDiskon'] = '0';
						$d['PersenDiskon'] = null;
						$d['DiskonDariHarga'] = null;
						$d['NetAmount'] = null;
					}
				}
				else
				{
					$d['FlagDiskon'] = '0';
					$d['PersenDiskon'] = null;
					$d['DiskonDariHarga'] = null;
					$d['NetAmount'] = null;
				}
			$d['AuditUserName'] = $this->session->userdata('UserId');
			$this->create_invoice_model->set_payment($d);
			}
			
			$data['status'] = 'success';
			$data['message'] = "Data updated successfully" ;
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->search($input['hfRegistrantID'][0],$input['hfPackageID'][0]);
		}
	}

/*	End	of	file	create_invoice.php	*/
/*	Location:		./controllers/billing_and_payment/create_invoice.php */

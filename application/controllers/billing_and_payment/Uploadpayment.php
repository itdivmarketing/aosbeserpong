<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

        class Uploadpayment extends CI_Controller {
		
        var $period;
	function __construct()
	{
		parent::__construct();

                $this->load->model("user_management/general_setting_model");
                $this->load->model("billing_and_payment/upload_payment_model");	
                $this->load->library('form_validation');
                $this->load->helper('admission_helper');
                $this->load->helper('site_helper');

                $Param['userId'] = $this->session->userData('UserId');
                $GeneralSetting = $this->general_setting_model->get_general_setting($Param);
                $this->period = trim($GeneralSetting[0]->Period);
	}


	public function index()
    {
		$this->template->display('billing_and_payment/Uploadpayment');	
    }

    public function upload()
    {
            // print_r($_FILES); die;
    	$this->config->load('app_config');
    	$uploaddir = $this->config->item('upload_payment_path');
    	
    	$uploadfile = $uploaddir.basename($_FILES['file']['name']);
    	// print_r($uploadfile);
    	if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
		   
    		$file = file_get_contents($uploadfile);
    		// print_r ($file); die();
    		$search = 'LOKASI';
    		$line = strpos($file, $search);
    		if($line === false){
    			$payload['json'] = array(
				    'status' => 'failed',
				    'error' => 'Failed on uploading'
			     );
		
			     return $this->load->view('json_view', $payload);
    		}else{
    			$next_line = substr($file, $line+strlen($search));
    			$next_line = substr($next_line, strpos($next_line, ' '));
    			
    			$search_line = strpos($next_line, '-');
    			$last_line = substr($next_line, $search_line+strlen(' '));
    			$last_line = substr($last_line, strpos($last_line, ' '));
    			
    			$first_arr = explode('  ', trim($last_line));
    			$is_empty = array_filter($first_arr);
    			
    			$arr = array();
    			$parent = 0;
    			$child = 0;
    			foreach($is_empty as $value){
    				$child++;
    				if($child % 7 == 0){
    					$arr[$parent][$child] = $value;
    					$parent++;
    					$child = 0;
    				}
    				else{
    					$arr[$parent][$child] = $value;
    				}
    			}
    			$flagParent;
    			for($i = 0; $i<$parent; $i++){
    			   if(substr($arr[$i][7],-1)=='0'){
    			         $flagParent = $i;
    				     break;
    			   }
    			}
    			for($j = $flagParent+1; $j<=$parent; $j++){
    				unset($arr[$j]);
    			}
                foreach($arr as $key=>$value){
                    $Param['formNo'] = trim($arr[$key][2]);   
                    $result[] = $this->upload_payment_model->get_payment($Param);
                }
    			
    			$payload['json'] = array(
				    'status' => 'success',
				    'data' => $arr,
                    'result' => $result,
				    'error' => 'Success on uploading'
			    );
			    return $this->load->view('json_view', $payload);
    		}
	    } 

    }

    public function set_payment()
    {
        $input = $this->input->post();
        $d = array();
        foreach($input['data'] as $value){
            $d['RegistrantId'] = $value['RegistrantId'];
            $d['FeeTypeId'] = $value['FeeTypeId'];
            $d['AmountBayar'] = $value['AmountBayar'];
            $d['TanggalBayar'] = date('Y-m-d', strtotime($value['TanggalBayar']));
            $d['Notes'] = $value['Notes'];
            $d['AuditUserName'] = $this->session->userdata('UserId');
            
            $this->upload_payment_model->set_payment($d);
  
        }
        
        $payload['json'] = array(
                'status' => 'success',
                'error' => 'Success in saving'
            );
        return $this->load->view('json_view', $payload);
    }
}

/*	End	of	file	Uploadpayment.php	*/
/*	Location:		./controllers/billing_and_payment/Uploadpayment.php */

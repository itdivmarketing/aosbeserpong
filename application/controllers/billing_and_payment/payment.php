<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Payment extends CI_Controller {
		
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("billing_and_payment/payment_model");
			$this->load->model("user_management/general_setting_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}

		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['FeeType']= $this->payment_model->get_fee_type();
			$this->template->display('billing_and_payment/payment',$data);	
        }

        public function search($RegistrantID = null)
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['FeeType']= $this->payment_model->get_fee_type();
			$data['Payment'] = $this->payment_model->get_payment_method();
			$Param['Period'] = $this->period;
			$input = $this->input->get();
			if((!isset($input['txtFormNumber']) || $input['txtFormNumber'] == '') && !isset($RegistrantID))
			{
				$data['message'] = 'Form Number Required';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			else
			{
				$Param['RegistrantID'] = (isset($input['txtFormNumber']) ? $input['txtFormNumber'] : $RegistrantID);
				
				$data['PaymentList'] = $this->get_payment($Param['RegistrantID'], null, null,null, true);
			}
			
			$this->template->display('billing_and_payment/payment',$data);
		}
		
		public function get_payment($registrantID = null, $PackageID = null, $FeeTypeID = null, $PaymentID = null, $postback = false)
		{
			$Param['Period'] = $this->period;
			$Param['RegistrantID'] = $registrantID;
			$Param['PackageID'] = $PackageID;
			$Param['FeeTypeID'] = $FeeTypeID;
			$Param['PaymentID'] = $PaymentID;
			$data['PaymentList'] = $this->payment_model->get_payment($Param);
			
			if($postback) return $data['PaymentList'];
			echo json_encode($data);
		}
		
		public function set_payment()
        {
			/*@TrPembayaranID, @RegistrantId VARCHAR(13), @FeeTypeId, @AmountBayar,
		 @TanggalBayar, @Notes, @StatusLunas, @PaketId, @AuditUserName*/
			
        	$input = $this->input->post();
			/*$this->load->library('form_validation');
			$this->form_validation->set_rules("hfFeeType","Fee Type","required");
			$this->form_validation->set_rules("hfRegistrantID","RegistrantID","required");
			$this->form_validation->set_rules("hfPackage","Package","required");
			$this->form_validation->set_rules("txtAmountPaid","Amount Paid","required");
			$this->form_validation->set_rules("txtPaymentDate","Payment Date","required");
			
			if($this->form_validation->run() != false)
			{	*/
				$d = array();
				$d['TrPembayaranID'] = null_setter($input['txtPaymentID']);
				$d['RegistrantId'] = $input['hfRegistrantID'];
				$d['FeeTypeId'] = $input['hfFeeType'];
				$d['AmountBayar'] = $input['txtAmountPaid'];
				$d['TanggalBayar'] = $input['txtPaymentDate'];
				$d['StatusLunas'] = (isset($input['cbNotPaid']) ? '0' : '1');
				$d['PaketId'] = $input['hfPackage'];
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Notes'] = $input['txtNote'];
				$d['PaymentMethod'] = $input['ddlPaymentMethod'];
				// print_r($d); die;
				$this->payment_model->set_payment($d);
				$data['status'] = 'success';
				$data['message'] = "Data updated successfully" ;
			/*}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("",''));
				//$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}*/
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->search($input['hfRegistrantID']);
		}
	}

/*	End	of	file	payment.php	*/
/*	Location:		./controllers/billing_and_payment/payment.php */

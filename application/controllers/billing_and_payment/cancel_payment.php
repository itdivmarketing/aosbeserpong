<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Cancel_payment extends CI_Controller {
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("billing_and_payment/cancel_payment_model");
			$this->load->model("user_management/general_setting_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}

		public function index($formNo="")
        {
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			if ($GeneralSetting)
			{
				$temp = explode("-", $GeneralSetting[0]->Period);
				$data['year'] = $temp[0];
				$data['semester'] = $temp[1];
			}
			else
			{
				$data['year'] = '';
				$data['semester'] = '';
			}
			$data['formNo']=$formNo;
         	$this->template->display('billing_and_payment/cancel_payment',$data);
         	//if($data['formNo']!="")
         	//	$this->search_payment_list();
         		//echo "<script type=\"text/javascript\">$('#btnSearch').trigger('submit');</script>";
        }

        public function search_payment_list($formNumber = null)
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			$input = $this->input->post();

			if(isset($input['txtFormNo']) && $input['txtFormNo'] == '' &&  $formNumber == '')
			{
				$data['status'] = 'failed';
				$data['message'] = "Form Number Required";
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				$this->template->display('billing_and_payment/cancel_payment',$data);
			}
			else
			{
				$form = (isset($input['txtFormNo']) ? $input['txtFormNo'] : $formNumber);
				$Param = array(
					'year' => $temp[0],
					'semester' => $temp[1],
					'formNo' => $form
				);
				$data['PaymentList']=$this->cancel_payment_model->get_payment_list($Param);

				$data['year'] = (isset($input['txtYear']) ? $input['txtYear'] : $temp[0]);
				$data['semester'] = (isset($input['txtSemester']) ? $input['txtSemester'] : $temp[1]);
				$data['formNo'] = (isset($input['txtFormNo']) ? $input['txtFormNo'] : $formNumber);
				$this->template->display('billing_and_payment/cancel_payment',$data);
			}
			//$this->index();
		}

		 public function set_payment()
        {
			$input = $this->input->post();
			$arr['paymentID'] = array();
			if(isset($input['cbCancel']) && sizeof($input['cbCancel']) > 0)
			{
				$data['year'] = $input['hfYear'];
				$data['semester'] = $input['hfSemester'];
				$data['formNo'] = $input['hfFormNo'];
				$flag = $input['cbCancel'];
				foreach($flag as $key=>$value)
				{
					array_push($arr['paymentID'], $value);
				}
			}
			
			if(sizeof($arr['paymentID'])>0){
				foreach ($arr['paymentID'] as $paymentID) {
					$Param['userchange'] = $this->session->userData('UserId');
					$Param['paymentID'] = $paymentID;
					$this->cancel_payment_model->cancel_payment($Param);
				}
				$data['message'] = 'success save to database';
			}
			else
			{
				$data['message'] = 'no data to be saved';
			}
			
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			//$this->index($input['hfFormNo']);
			$this->search_payment_list($input['hfFormNo']);
			
		}
	}

/*	End	of	file	cancel_payment.php	*/
/*	Location:		./controllers/billing_and_payment/cancel_payment.php */

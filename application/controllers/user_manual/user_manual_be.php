<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class User_manual_be extends CI_Controller {
		function __construct() {
			parent::__construct();
			$this->load->helper('admission_helper');
			$this->load->helper('download');			
		}
		
		public function index() {
			$path = file_get_contents(base_url() . "resources/template/user_manual/AOSSerpongBE.pdf");
			
			force_download("AOS Serpong BE.pdf", $path);
		}
	}
?>

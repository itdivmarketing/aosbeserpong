<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class parent_profile extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->library('form_validation');
			$this->load->config('app_config');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }
		
		public function get_city($country, $postback = false)
		{
			$Param['CountryID'] = $country;
			$data['City'] = $this->parent_profile_model->get_city($Param);

			if($postback) return $data['City'];
			else echo json_encode($data);
		}


		public function save_parent_profile_data()
		{
			
			$input = $this->input->post();
			$this->load->library('form_validation');
	
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				//$username = $this->session->userdata('username');
				//$RegistrantID =  $this->session->userdata('RegistrantID');
				$RegistrantID =  trim($input['txtFormNumber']);
				$this->db->trans_begin();
				
				
				
				$CanInputFather=1;
				$CanInputMother=1;
				/*
				if( trim($input['txtFatherName'])==""&&trim($input['txtFatherDOB'])==""&& trim($input['txtFatherPOB'])=="" &&trim($input['ddlFatherReligion'])=="" &&
					 trim($input['txtFatherAddress'])==""&&	trim($input['ddlFatherCity'])=="" && trim($input['txtFatherPostalCode']) =="" && trim($input['txtFatherEmail'])==""
					&& trim($input['txtFatherEducation'])=="" &&  trim($input['txtFatherCellPhone'])==""&&  trim($input['txtFatherPhone'])==""&&  trim($input['txtFatherFax'])==""){
					$CanInputFather=0;
				}
				
				if( trim($input['txtMotherName'])==""&&trim($input['txtMotherDOB'])==""&& trim($input['txtMotherPOB'])=="" &&trim($input['ddlMotherReligion'])=="" &&
					 trim($input['txtMotherAddress'])==""&&	trim($input['ddlMotherCity'])=="" && trim($input['txtMotherPostalCode']) =="" && trim($input['txtMotherEmail'])==""
					&& trim($input['txtMotherEducation'])=="" &&  trim($input['txtMotherCellPhone'])==""&&  trim($input['txtMotherPhone'])==""&&  trim($input['txtMotherFax'])==""){
					$CanInputMother=0;
				}
				*/
				$Row=array();
				
				if($CanInputFather==1){
					$Row['Father']=array(
							'StudentID' => $RegistrantID,  
							'ParentStatus' => 'F',
							'ParentName' =>  trim($input['txtFatherName']),
							'DOB' =>  trim($input['txtFatherDOB'])==""?NULL: trim($input['txtFatherDOB']),
							'POB' =>  trim($input['txtFatherPOB']),
							'ReligionID' => (int)trim($input['ddlFatherReligion']),  
							'Address' => trim($input['txtFatherAddress']),
							'CityID' => trim($input['ddlFatherCity'])==""?NULL:(int)trim($input['ddlFatherCity']),
							'PostalCode' =>trim($input['txtFatherPostalCode']),
							'Email' => null_setter(trim($input['txtFatherEmail'])),  
							'Education' => trim($input['txtFatherEducation']),
							//'CellPhoneNumber'=> trim($input['txtFatherCellPhone']),
							//'PhoneNumber'=>trim($input['txtFatherPhone']),
							//'FaxNumber'=> trim($input['txtFatherFax']),
							'LifeStatusID'=> trim($input['ddlFatherLifeStatus']),
							'RIPYear'=> trim($input['txtFatherRIPYear']),
							'AuditUserName' => $this->session->userdata('UserId')
					);
					
					
					
				}
				
				if($CanInputMother==1){
					$Row['Mother']=array(
							'StudentID' => $RegistrantID,  
							'ParentStatus' => 'M',
							'ParentName' =>  trim($input['txtMotherName']),
							'DOB' =>  trim($input['txtMotherDOB'])==""?NULL: trim($input['txtMotherDOB']),
							'POB' =>  trim($input['txtMotherPOB']),
							'ReligionID' => (int)trim($input['ddlMotherReligion']),  
							'Address' => trim($input['txtMotherAddress']),
							'CityID' => trim($input['ddlMotherCity'])==""?NULL:(int)trim($input['ddlMotherCity']),
							'PostalCode' =>trim($input['txtMotherPostalCode']),
							'Email' => trim($input['txtMotherEmail']),
							'Education' => trim($input['txtMotherEducation']),
							//'CellPhoneNumber'=> trim($input['txtMotherCellPhone']),
							//'PhoneNumber'=> trim($input['txtMotherPhone']),
							//'FaxNumber'=> trim($input['txtMotherFax']),
							'LifeStatusID'=> trim($input['ddlMotherLifeStatus']),
							'RIPYear'=> trim($input['txtMotherRIPYear']),
							'AuditUserName' => $this->session->userdata('UserId')
					);
				}
				
				if($Row){
					foreach($Row as $ChildRow){
				
						$Array = array(
							'StudentID' => $ChildRow['StudentID'],  
							'ParentStatus' => $ChildRow['ParentStatus'],
							'ParentName' => $ChildRow['ParentName'],
							'DOB' => $ChildRow['DOB'],
							'POB' => $ChildRow['POB'],
							'ReligionID' => $ChildRow['ReligionID'],  
							'Address' => $ChildRow['Address'],
							'CityID' => $ChildRow['CityID'],
							'PostalCode' =>$ChildRow['PostalCode'],
							'Email' => $ChildRow['Email'],  
							'Education' =>$ChildRow['Education'],
							'LifeStatusID'  =>$ChildRow['LifeStatusID'],
							'RIPYear'  =>$ChildRow['RIPYear'],
							'AuditUserName' => $ChildRow['AuditUserName']
						);
						
						$this->parent_profile_model->insert_full_parent_data($Array);
						$p=array();
						$p['StudentID']=$ChildRow['StudentID'];
						$p['ParentStatus']=$ChildRow['ParentStatus'];
						$p['AuditUserName']=$ChildRow['AuditUserName'];
						
						/*$this->phone_model->delete_phone_data($p);

						$p['PhoneType']='M';
						$p['PhoneNumber']= $ChildRow['CellPhoneNumber'];
						
						if($p['PhoneNumber']!=""){
							$this->phone_model->insert_phone_data( $p);
						}

						$p['PhoneType']='R';
						$p['PhoneNumber']= $ChildRow['PhoneNumber'];
						
						if($p['PhoneNumber']!=""){
							$this->phone_model->insert_phone_data( $p);
						}

						$p['PhoneType']='F';
						$p['PhoneNumber']= $ChildRow['FaxNumber'];
						
						if($p['PhoneNumber']!=""){
							$this->phone_model->insert_phone_data( $p);
						}
						*/
					
					}
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['txtFormNumber']));
		}
		
		public function save_parent_employment_data()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
	
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				//$username = $this->session->userdata('username');
				//$RegistrantID =  $this->session->userdata('RegistrantID');
				$RegistrantID =  trim($input['txtFormNumber']);
				$this->db->trans_begin();
				//$CanInputFather=1;
				//$CanInputMother=1;
				/*
				if( trim($input['txtFatherCompany'])==""&&trim($input['txtFatherJobTitle'])==""&& trim($input['txtFatherOfficeAddress'])=="" &&trim($input['ddlFatherOfficeCity'])=="" &&
					 trim($input['txtFatherOfficePostalCode'])==""&&	trim($input['txtFatherOfficePhone'])=="" && trim($input['txtFatherOfficeFax']) =="" && trim($input['txtFatherContactPerson'])==""
					&& trim($input['txtFatherTitle'])=="" &&  !isset($input['rbtFatherPaySchoolFee']) ){
					$CanInputFather=0;
				}
				
				if( trim($input['txtMotherCompany'])==""&&trim($input['txtMotherJobTitle'])==""&& trim($input['txtMotherOfficeAddress'])=="" &&trim($input['ddlMotherOfficeCity'])=="" &&
					 trim($input['txtMotherOfficePostalCode'])==""&&	trim($input['txtMotherOfficePhone'])=="" && trim($input['txtMotherOfficeFax']) =="" && trim($input['txtMotherContactPerson'])==""
					&& trim($input['txtMotherTitle'])=="" &&  !isset($input['rbtMotherPaySchoolFee'])){
					$CanInputMother=0;
				}
				*/
					$Row=array();

			
					$Row['Father']= array(
						'StudentID' => $RegistrantID,  
						'ParentStatus' => 'F',
						'Company' =>  trim($input['txtFatherCompany']),
						"JobTitle" =>  trim($input['txtFatherJobTitle']),
						"OfficeAddress" =>  trim($input['txtFatherOfficeAddress']),
						"OfficeCityID" =>  trim($input['ddlFatherOfficeCity'])==""?NULL:(int) trim($input['ddlFatherOfficeCity']),
						"OfficePostalCode" => trim($input['txtFatherOfficePostalCode']),
						"OfficePhone" =>  trim($input['txtFatherOfficePhone']),
						"OfficeFax" =>  trim($input['txtFatherOfficeFax']),
						"ContactPersonName" => trim($input['txtFatherContactPerson']),
						"ContactPersonTitle" =>  trim($input['txtFatherTitle']),
						"PayTheSchoolFee" =>  isset($input['rbtFatherPaySchoolFee'])?trim($input['rbtFatherPaySchoolFee']):NULL,
						'AuditUserName' => $this->session->userdata('UserId')
					);
				
				
					$Row['Mother']=array(
						'StudentID' => $RegistrantID,  
						'ParentStatus' => 'M',
						'Company' =>  trim($input['txtMotherCompany']),
						"JobTitle" =>  trim($input['txtMotherJobTitle']),
						"OfficeAddress" =>  trim($input['txtMotherOfficeAddress']),
						"OfficeCityID" =>  trim($input['ddlMotherOfficeCity'])==""?NULL:(int) trim($input['ddlMotherOfficeCity']),
						"OfficePostalCode" => trim($input['txtMotherOfficePostalCode']),
						"OfficePhone" =>  trim($input['txtMotherOfficePhone']),
						"OfficeFax" =>  trim($input['txtMotherOfficeFax']),
						"ContactPersonName" => trim($input['txtMotherContactPerson']),
						"ContactPersonTitle" =>  trim($input['txtMotherTitle']),
						"PayTheSchoolFee" =>  isset($input['rbtMotherPaySchoolFee'])?trim($input['rbtMotherPaySchoolFee']):NULL,
						'AuditUserName' => $this->session->userdata('UserId')
					);
				
				
				if($Row){
					foreach($Row as $ChildRow){
				
						$Array = array(
							'StudentID' => $ChildRow['StudentID'],  
							'ParentStatus' => $ChildRow['ParentStatus'],
							'Company' => $ChildRow['Company'],
							'JobTitle' => $ChildRow['JobTitle'],
							'OfficeAddress' => $ChildRow['OfficeAddress'],
							'OfficeCityID' => $ChildRow['OfficeCityID'],  
							'OfficePostalCode' => $ChildRow['OfficePostalCode'],
							'OfficePhone' => $ChildRow['OfficePhone'],
							'OfficeFax' =>$ChildRow['OfficeFax'],
							'ContactPersonName' => $ChildRow['ContactPersonName'],  
							'ContactPersonTitle' =>$ChildRow['ContactPersonTitle'],
							'PayTheSchoolFee' =>$ChildRow['PayTheSchoolFee'],
							'AuditUserName' => $ChildRow['AuditUserName']
						);
						
						$this->parent_profile_model->insert_parent_employment_data($Array);
					}
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			
		
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['txtFormNumber']));
		}
		
		public function save_parent_phone_data()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
	
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				$RegistrantID =  trim($input['txtFormNumber']);
				$FatherPhonesType =  $input['hfFatherPhoneType'];
				$FatherPhones =  $input['hfFatherPhone'];
				$MotherPhonesType =  $input['hfMotherPhoneType'];
				$MotherPhones =  $input['hfMotherPhone'];
				
				
				$this->db->trans_begin();
				$param['StudentID'] = $RegistrantID;
				$param['ParentStatus'] = 'F';
				$param['AuditUserName'] = $this->session->userdata('UserId');
				$this->phone_model->delete_phone_data($param);
				
				$param['StudentID'] = $RegistrantID;
				$param['ParentStatus'] = 'M';
				$param['AuditUserName'] = $this->session->userdata('UserId');
				$this->phone_model->delete_phone_data($param);
				
				for ($i = 0 ; $i < sizeof($FatherPhonesType) ; $i++)
				{
					$param['StudentID'] = $RegistrantID;
					$param['ParentStatus'] = 'F';
					$param['PhoneType'] = $FatherPhonesType[$i];
					$param['PhoneNumber'] =  $FatherPhones[$i];
					$param['AuditUserName'] = $this->session->userdata('UserId');
					$this->phone_model->insert_phone_data($param);
				}
				
				for ($i = 0 ; $i < sizeof($MotherPhonesType) ; $i++)
				{
					$param['StudentID'] = $RegistrantID;
					$param['ParentStatus'] = 'M';
					$param['PhoneType'] = $MotherPhonesType[$i];
					$param['PhoneNumber'] =  $MotherPhones[$i];
					$param['AuditUserName'] = $this->session->userdata('UserId');
					$this->phone_model->insert_phone_data($param);
				}
				
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
					$data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
				} else {    
					$this->db->trans_commit();    
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;

				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['txtFormNumber']));
		}
		
		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			$data['Religion'] = $this->student_profile_model->get_religion();
			
			$data['Country'] = $this->student_profile_model->get_country();
			
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='F';
			$data['FatherData']=$this->parent_profile_model->get_parent_profile_data($Param);
			
			$data['FatherPhone']=$this->parent_profile_model->get_parent_phone_data($Param);
			
			if(isset($data['FatherData']->CountryID)){
			$param['CountryID'] = $data['FatherData']->CountryID;
			
			
			
			$data['FatherCity'] = $this->parent_profile_model->get_city($param);
			}
			if(isset($data['FatherData']->OfficeCountryID)){
			$param['CountryID'] = $data['FatherData']->OfficeCountryID;

			$data['FatherOfficeCity'] = $this->parent_profile_model->get_city($param);
			}
			
			//die;
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='M';
			
			
			$data['MotherData']=$this->parent_profile_model->get_parent_profile_data($Param);
			$data['MotherPhone']=$this->parent_profile_model->get_parent_phone_data($Param);
			if(isset($data['MotherData']->CountryID)){
			$param['CountryID'] = $data['MotherData']->CountryID;
			$data['MotherCity'] = $this->parent_profile_model->get_city($param);
			}
			
			if(isset($data['MotherData']->OfficeCountryID)){
				$param['CountryID'] = $data['MotherData']->OfficeCountryID;
				$data['MotherOfficeCity'] = $this->parent_profile_model->get_city($param);
			}
			
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='F';
			$RegistrantFatherPhoneDataDB= $this->phone_model->get_phone_data($Param);
			
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='M';
			$RegistrantMotherPhoneDataDB= $this->phone_model->get_phone_data($Param);
			$data['LifeStatus'] = $this->parent_profile_model->get_life_status();
			foreach($RegistrantFatherPhoneDataDB as $row){
			
			if($row->PhoneType=='M'){
				$data['FatherCellPhoneData']=$row->PhoneNumber;
				}
			else if($row->PhoneType=='R'){
				$data['FatherPhoneData']=$row->PhoneNumber;	
				}
			else if($row->PhoneType=='F'){
				$data['FatherFaxData']=$row->PhoneNumber;	
				}
			}
			
			foreach($RegistrantMotherPhoneDataDB as $row){
			
			if($row->PhoneType=='M'){
				$data['MotherCellPhoneData']=$row->PhoneNumber;
				}
			else if($row->PhoneType=='R'){
				$data['MotherPhoneData']=$row->PhoneNumber;	
				}
			else if($row->PhoneType=='F'){
				$data['MotherFaxData']=$row->PhoneNumber;	
				}
			}
			
			$par['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($par);
			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}

			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
			
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
			$data['TabName'] = "parent_profile";
			//print_r($data['StudentData']->StayingWithID);die;
         	$this->template->display('entry/parent_profile',$data);
		}
	}
/*	End	of	file	parent_profile.php	*/
/*	Location:		./controllers/entry/form_return/parent_profile.php */

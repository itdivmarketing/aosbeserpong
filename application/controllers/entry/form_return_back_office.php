<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_return_back_office extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$BaseUrl=base_url("entry/student_profile");
			header("location:".$BaseUrl);
        }
		public function search_student($FormNumber = null,$Name= null,$postback = false){
		
			if(isset($FormNumber)||isset($Name))
			{
				$Param['FormNumber'] = $FormNumber;
				$Param['StudentName'] = $Name;
				$data['SearchResult'] = $this->form_return_back_office_model->search_student($Param);
			}
			else
			{
				$data['SearchResult'] ='';
			}
			if($postback) return $data['SearchResult'];
			else echo json_encode($data);
		
		
		
		
		}
		
		public function get_student_paging($StudentName= null,$FormNumber = null,$hfPage=null)
        {
		
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($hfPage) and !empty($hfPage)) ? $hfPage : 1;
			
			$table['f'] = array(
				'StudentName'  => isset($StudentName)&&$StudentName!="null" ? null_setter($StudentName)  : null,
				'FormNumber'  => isset($FormNumber)&&$FormNumber!="null" ? null_setter($FormNumber)  : null
			);
			
			$query = $this->form_return_back_office_model->search_student(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->form_return_back_office_model->search_student(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				//$data['data'] = array();
				$data['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['SearchResult']     = $result;
			}
			else
			{
				$data['status'] = false;
				//$data['data'] = array();
				$data['pagination'] = null;
				$data['totaldata'] = $table['f']  ;
				$data['SearchResult']  = null;
			}
			
			echo json_encode($data); 
		}
		
		
		public function save_profile_picture($method = '')
		{
			
			$this->config->load('app_config');
			$this->stepstatus = 3;
			$TempPath="";
			$final_filename = "";
			
			$allowed_ext = array('jpg', 'jpeg', 'png');
			$post = $this->input->post();
			$RegistrantID = $post['RegistrantID'];
			//$file=$_FILES['file'];
			$i=0;
				foreach($_FILES as $file)
				{
					$md5name = md5($file['name']);
					$orig_name = preg_replace('/[^a-zA-Z0-9\.-_]/si', '', $file['name']);
					$final_path = $this->config->item('form_return_BO_upload_path').'ProfilePicture/';
					$temp_path = $final_path . $md5name;
					@move_uploaded_file($file['tmp_name'], $temp_path);
					$extension = explode('.', $orig_name);
					
					$extension = strtolower($extension[count($extension) - 1]);

					if(in_array($extension, $allowed_ext))
					{
						$random = substr(md5(uniqid(microtime(), true)), 0, 6);
						$final_filename = 'profpic_'.'RegistrantID'. '_' . $random . '_' . $orig_name;

						//$file_size = filesize($temp_path);
						rename($temp_path, $final_path . $final_filename);
						
						$TempPath=$final_path . $final_filename;
						$TempData=array('FileName'=>$final_filename,'FileSize'=>0);
						

						$i++;
					}
					else
					{
						// remove the data if it's not needed anymore
						@unlink( $final_path . $md5name );
					}
		
						//$Data=array('TempPath'=>$TempPath,'FileName'=>$final_filename);

				}
			

			
			
			return $this->load->view('entry/json_view', array('json' => array(
				'status' => 'success',
				'Photo' => $final_filename,
				'RegistrantID' => $RegistrantID
			)));
		
		
		}
		
		public function save_profile_picture_db($method = '')
		{
				$request = $this->input->post();

				$this->config->load('app_config');

				$RegistrantID = $request['RegistrantID'];

				$Data="";
				
				$Directory = $this->config->item('form_return_BO_upload_path').'ProfilePicture/';
				if(!empty($request['Data'])){
					$Data=$request['Data'];
					$OldPath=$Data;
					$NewPath=str_replace("RegistrantID",$RegistrantID,$Data);
					rename($Directory.$OldPath, $Directory.$NewPath);
				}
		
				$this->db->trans_begin();
		
				$OldPhoto=array();
				
				
				if(isset($Data)){
					$Array =array( 
							'RegistrantID' => $RegistrantID, 
							'Photo' => $NewPath,
							'AuditUserName' => $this->session->userData('UserId')


					);
					
					$OldPhoto=$this->form_return_back_office_model->insert_registrant_photo($Array);
					
				}
				
				$this->db->trans_status() === FALSE?$this->db->trans_rollback():$this->db->trans_commit();

			
				if(isset($OldPhoto->OldPhoto))
					@unlink( $this->config->item('form_return_BO_upload_path').'ProfilePicture/' . $OldPhoto->OldPhoto );
					
				return $this->load->view('entry/json_view', array('json' => array(
				'status' => 'success',
				'Photo' => $OldPhoto->OldPhoto
			)));
		
		}
	}
	
/*	End	of	file	form_return_back_office.php	*/
/*	Location:		./controllers/entry/form_return/form_return_back_office.php */

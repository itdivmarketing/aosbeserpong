<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_return_front_office extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$d = $this->get_form_data_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_return_front_office_model->get_school_level();
			$data['YearLevel'] = $this->form_return_front_office_model->get_year_level();
			$data['Gender'] = $this->form_return_front_office_model->get_gender();
			$data['ChildStatus'] = $this->form_return_front_office_model->get_child_status();
			$data['TypeOfRegistration'] = $this->form_return_front_office_model->get_type_of_registration();
			$data['Semester'] = $this->form_return_front_office_model->get_semester();
			$data['JoinedYear'] = $this->form_return_front_office_model->get_academic_year();
         	$this->template->display('entry/form_return_front_office',$data);
        }
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_return_front_office_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_return_front_office_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		public function get_admissionID($TermID = null,$postback = false)
		{
			if($TermID)
			{
				$Param['TermID'] = $TermID;
				$data['AdmissionID']=$this->form_sales_model->get_admissionID($Param);
			}
			else
			{
				$data['AdmissionID'] = '';
			}
			if($postback) return $data['AdmissionID'];
			else echo json_encode($data);
		}
		
		public function get_payment_method($postback = false)
		{
			
			$data['PaymentMethod'] = $this->form_sales_model->get_payment_method();
		
			if($postback) return $data['PaymentMethod'];
			else echo json_encode($data);
		}
		
		public function check_form_validation($FormNumber="",$TermID="",$postback = false)
		{
			if($FormNumber){
				$Param['FormNo']=$FormNumber;
				$Param['LocationID']='8';
				$data['FormStatus'] = $this->form_return_front_office_model->check_return_form_FO($Param);
			}
			if($postback) return $data['FormStatus'];
			else echo json_encode($data);
		}
		
		public function get_form_amount($FormNumber,$postback = false)
		{
			if($FormNumber){
				$Param['FormNo']=$FormNumber;
				$data['FormAmount'] = $this->form_sales_model->get_form_amount($Param);
			}
			if($postback) return $data['FormAmount'];
			else echo json_encode($data);
		}
		
	

		public function get_form_view($RegistrantID, $postback = false)
		{
			$Param['RegistrantID'] = $RegistrantID;
			
			$data['FormData'] = $this->form_return_front_office_model->get_form_view($Param);
			//echo $data['FormSoldView'][0];
			if($postback) return $data['FormData'];
			else echo json_encode($data);
		}
		
		public function save_form_return_FO()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			$this->form_validation->set_rules("txtViewAdmissionID","Admission ID","required");
			$this->form_validation->set_rules("txtViewAdmissionYear","Admission Year","required");
			$this->form_validation->set_rules("txtViewAdmissionSemester","Semester","required");
			$this->form_validation->set_rules("txtViewSchoolLevel","School Level","required");	
			$this->form_validation->set_rules("txtViewAdmissionTerm","Admission Term","required");
			$this->form_validation->set_rules("txtViewFormNumber","Form Number","required");	
			
			$this->form_validation->set_rules("txtViewFirstName","First Name","required");
			$this->form_validation->set_rules("txtViewDateOfBirth","Date Of Birth","required");
			$this->form_validation->set_rules("txtViewPlaceOfBirth","Place Of Birth","required");
			$this->form_validation->set_rules("ddlViewGender","Gender","required");
			$this->form_validation->set_rules("ddlViewChildStatus","Child Status","required");
			$this->form_validation->set_rules("ddlViewTypeOfRegistration","Type of Registration","required");
			$this->form_validation->set_rules("ddlViewToYearLevel","to Year Level","required");
			
			$this->form_validation->set_rules("ddlViewToSemester","To Semester","required");
			$this->form_validation->set_rules("txtViewDateOfEntry","Date of Entry","required");
			$this->form_validation->set_rules("txtViewDateOfFormReceipt","Date of Form Receipt","required");
		
			if($this->form_validation->run() != false)
			{	
				$d = array();
				$this->db->trans_begin();
				
				$d['RegistrantID'] = trim($input['txtViewFormNumber']);
				$d['FirstName'] = trim($input['txtViewFirstName']);
				$d['MiddleName'] = trim($input['txtViewMiddleName']);
				$d['LastName'] = trim($input['txtViewLastName']);
				$Date=date_parse_from_format("d M Y",$input['txtViewDateOfBirth']);
				$d['DOB'] = $Date['year'].'-'.$Date['month'].'-'.$Date['day'];
				$d['POB'] = trim($input['txtViewPlaceOfBirth']);
				$d['GenderID'] = trim($input['ddlViewGender']);
				$d['ChildStatusID'] = (int)trim($input['ddlViewChildStatus']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Transfer'] = (int)trim($input['ddlViewTypeOfRegistration']);
				$d['YearLevelId'] = (int)trim($input['ddlViewToYearLevel']);
				$Date=date_parse_from_format("d M Y H:i",$input['txtViewDateOfEntry']);
				//$d['DateOfEntry'] = $Date['year'].'-'.$Date['month'].'-'.$Date['day'].' '.$Date['hour'].':'.$Date['minute'];
				$d['DateOfEntry'] = trim($input['txtViewDateOfEntry']);
				$Date=date_parse_from_format("d M Y H:i",$input['txtViewDateOfFormReceipt']);
				//$d['DateOfApplReceived'] = $Date['year'].'-'.$Date['month'].'-'.$Date['day'].' '.$Date['hour'].':'.$Date['minute'];
				$d['DateOfApplReceived'] = trim($input['txtViewDateOfFormReceipt']);
				$d['smtId'] = trim($input['ddlViewToSemester']);
				
				$d['JoinedYear'] = trim($input['ddlViewJoinedYear'])==""?NULL:trim($input['ddlViewJoinedYear']);
				$d['JoinedMonth'] = trim($input['ddlViewJoinedMonth'])==""?NULL:trim($input['ddlViewJoinedMonth']);
				//print_r($d['smtId']);die;
				$this->form_return_front_office_model->save_return_form($d);
				
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
				     $data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
				} else {    
					$this->db->trans_commit();    
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;

				}
				
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}
			
			$this->index();
		}
		
		public function get_form_data_paging($input = null)
        {
		
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'Period'		=> $this->period,
				'RegistrantID'  => isset($input['txtFormNumber']) ? null_setter($input['txtFormNumber']) : null,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'YearLevel'	=> isset($input['ddlYearLevel']) ? null_setter($input['ddlYearLevel']) : null,
				'AdmissionTerm'	=> isset($input['ddlAdmissionTerm']) ? null_setter($input['ddlAdmissionTerm']) : null
			);
			$query = $this->form_return_front_office_model->get_form_data_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->form_return_front_office_model->get_form_data_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
			
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_form_data_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['SchoolLevel'] = $this->form_return_front_office_model->get_school_level();
			$data['YearLevel'] = $this->get_year_level($input['ddlSchoolLevel'],true);
			$data['ViewYearLevel'] = $this->form_return_front_office_model->get_year_level();
			$data['Gender'] = $this->form_return_front_office_model->get_gender();
			$data['ChildStatus'] = $this->form_return_front_office_model->get_child_status();
			$data['TypeOfRegistration'] = $this->form_return_front_office_model->get_type_of_registration();
			$data['Semester'] = $this->form_return_front_office_model->get_semester();
			$data['JoinedYear'] = $this->form_return_front_office_model->get_academic_year();
			
         	$this->template->display('entry/form_return_front_office',$data);
		}
	}
/*	End	of	file	form_return_front_office.php	*/
/*	Location:		./controllers/entry/form_return/form_return_front_office.php */

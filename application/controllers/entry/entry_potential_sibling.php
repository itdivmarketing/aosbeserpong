<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Entry_potential_sibling extends CI_Controller {

		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/entry_potential_sibling_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
         	$this->template->display
         		('entry/entry_potential_sibling');
        }
		
        /*public function get_entry_potential_sibling_paging($input = null)
        {

        	$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'Name'		=> (isset($input['txtSiblingName']) and !empty($input['txtSiblingName'])) ? $input['txtSiblingName'] : '',
				'DOB'	=> (isset($input['txtDOB']) and !empty($input['txtDOB'])) ? date('Y-m-d',strtotime($input['txtDOB'])) : '' ,
				'Flag' => (isset($input['rbFilterSibling'])) ? $input['rbFilterSibling'] : '',
			);
			
			$query = $this->entry_potential_sibling_model->search_potential_sibling_studentID
			(array_merge(array('count'=>1),$table['f']));
			
			$table['total_data']    = $query[0]->TOTAL_DATA;
			//print_r(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f'])); die();
			$result = $this->entry_potential_sibling_model->search_potential_sibling_studentID
			(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));

			//echo json_encode($result); die();
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
        }*/

		public function search()
		{
			$input = (array)$this->input->get();
			//print_r ($input); die();

			//$d = $this->get_entry_potential_sibling_paging($input);
			
			//$data['data']['result'] = $d['data']['result'];
			//$data['data']['pagination'] = $d['data']['pagination'];
			

			$param['Name']=(isset($input['txtSiblingName']) and !empty($input['txtSiblingName'])) ? $input['txtSiblingName'] : '' ;
			$param['DOB']=(isset($input['txtDOB']) and !empty($input['txtDOB'])) ? date('Y-m-d',strtotime($input['txtDOB'])) : '' ;
			//echo $param['DOB']; die();
			$param['Flag']=(isset($input['rbFilterSibling'])) ? $input['rbFilterSibling'] : '' ;
			//echo json_encode($param); die();
			$data['result'] = $this->entry_potential_sibling_model->search_potential_sibling_studentID($param);
			//print_r($data['result']); die();
         	$this->template->display('entry/entry_potential_sibling',$data);
		}

		public function save()
		{	
			$input = $_POST;
			
			$result = $this->entry_potential_sibling_model->save_potential_sibling_student($input);			
			
			/* Change by iben rustandi 20150319 | ALL Proses move to model, insert using transaction */
			
			/*$count=0;
			
			foreach($input['PotentialSiblingStudentID'] as $row){
				$count++;
			}
			
			for($i=1;$i<=$count;$i++){
				if(isset($input['PotentialSiblingStudentID'][$i]) and !empty($input['PotentialSiblingStudentID'][$i])){					
					$data['RegistrantID']= $input['RegistrantID'][$i];
					$data['PotentialSiblingID']= $input['PotentialSiblingID'][$i];
					$data['SiblingStudentID'] = $input['PotentialSiblingStudentID'][$i];
					$data['AuditUserName'] = $this->session->userdata("UserId");
					
								
				} else {
					$data=null;
				}
			}*/
			
			//$retval = j
			echo json_encode($result);
			//$this->template->display('entry/entry_potential_sibling',$data);
		}
		
	}
/*	End	of	file	entrance_potential_sibling.php	*/
/*	Location:		./controllers/entry/entrance_potential_sibling.php */

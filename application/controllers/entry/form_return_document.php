<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_return_document extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_sales_model");
			$this->load->model("entrance_test/registrant_allocation_model");
			$this->load->model("entry/form_return_document_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->config('app_config');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			$data["AdmissionProcess"]=$this->form_return_document_model->get_admission_process_list();
         	$this->template->display('entry/form_return_document',$data);
        }
	
		

		public function save_document()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
			$this->form_validation->set_rules("hfAdmissionProcess","Admission Process","required");
			$this->form_validation->set_rules("hfDocumentID","Document ID","required");
			$this->form_validation->set_rules("rbtAction","Action","required");
			
			if($this->form_validation->run() != false)
			{	
				
				$RegistrantID = trim($input['hfFormNumber']);
				$SubmissionID = trim($input['hfSubmissionID']);
				$DocumentID = trim($input['hfFormNumber']);
				$Date = trim($input['txtDate']);
				
				$this->db->trans_begin();
				
				
				$Param['RegistrantID']=trim($RegistrantID);
				$Param['AdmissionProcessID']=(int)$input['hfAdmissionProcess'];
				$RegistrantSubmissionData =	$this->form_return_document_model->get_registrant_document_submission_data($Param);
				
				$DocumentList=$this->form_return_document_model->get_document_list($Param);
				
				if(empty($RegistrantSubmissionData))
				{
					if(!empty($DocumentList)){
						
						foreach($DocumentList as $row){
							$Array =array( 
								'RegistrantID' => $RegistrantID, 
								'DocumentID' => $row->DocumentID, 
								'AdmissionProcessID' =>  (int)$row->AdmissionProcessID,
								'Verification' => '3',
								'AuditUserName' =>$this->session->userdata('UserId'),
								'Date' => $Date
							);
							
							$this->form_return_document_model->insert_registrant_document_submission_data($Array);
							//print_r("asdasd");
						}
					}
				}
				//die;
				

				if($input['rbtAction']=="UploadFile"){
				
					$final_filename = "";
					$file=$_FILES['file'];
					$allowed_ext = array('jpg', 'jpeg', 'png', 'pdf','rar','zip','7z');
					
				
					if(isset($file) && $file['error'] == 0 && is_uploaded_file($file['tmp_name']) )
					{
						$md5name = md5($file['name']);
						$orig_name = preg_replace('/[^a-zA-Z0-9\.-_]/si', '', $file['name']);
						$final_path = $this->config->item('form_return_BO_upload_path').'DocumentUpload/';
						$temp_path = $final_path . $md5name;
						@move_uploaded_file($file['tmp_name'], $temp_path);
						
						$extension = explode('.', $orig_name);
						$extension = strtolower($extension[count($extension) - 1]);
						if(in_array($extension, $allowed_ext))
						{
							$random = substr(md5(uniqid(microtime(), true)), 0, 6);
							$final_filename = 'document_'.$RegistrantID . '_' . $random . '_' . $orig_name;

							$file_size = filesize($temp_path);
							rename($temp_path, $final_path . $final_filename);
							
							$Array =array( 
								'RegistrantID' => $RegistrantID, 
								'DocumentID' => $input['hfDocumentID'], 
								'FileName' => $final_filename==""?NULL:$final_filename,
								'FileSize' =>(float)$file_size,
								'Verification' => '3',
								'Note' => trim($input['txtNote']),
								'ReceivedBy' => $this->session->userdata('UserId'),
								'DueDate' => NULL,
								'AdmissionProcessID' =>  (int)$input['hfAdmissionProcess'],
								'AuditUserName' =>$this->session->userdata('UserId'),
								'Date' => $Date
							);	
						

							$this->form_return_document_model->insert_registrant_document_submission_data($Array);
						
							if( $input['hfFileName'] !="" &&  $input['hfFileName']!==NULL )
								@unlink( $this->config->item('form_return_BO_upload_path').'DocumentUpload/' . $input['hfFileName'] );
						}
						else
						{
							// remove the data if it's not needed anymore
							@unlink( $final_path . $md5name );
						}
					
					}
				}
				else if($input['rbtAction']=="SubmitPhysicalFile"){
						$Array =array( 
						'RegistrantID' => $RegistrantID, 
						'DocumentID' => $input['hfDocumentID'], 
						'FileName' => "dokumen_fisik.jpg",
						'FileSize' => 0,
						'Verification' => '3',
						'Note' => trim($input['txtNote']),
						'ReceivedBy' => $this->session->userdata('UserId'),
						'DueDate' => NULL,
						'AdmissionProcessID' =>  (int)$input['hfAdmissionProcess'],
						'AuditUserName' =>$this->session->userdata('UserId'),
						'Date' => $Date
					);
				

					$this->form_return_document_model->insert_registrant_document_submission_data($Array);
				
					if( $input['hfFileName'] !="" &&  $input['hfFileName']!==NULL )
						@unlink( $this->config->item('form_return_BO_upload_path').'DocumentUpload/' . $input['hfFileName'] );

				}
				else if($input['rbtAction']=="Pending"){
					$Array =array( 
						'RegistrantID' => $RegistrantID, 
						'DocumentID' => $input['hfDocumentID'], 
						'FileName' => NULL,
						'FileSize' => NULL,
						'Verification' => '3',
						'Note' => trim($input['txtNote']),
						'ReceivedBy' => $this->session->userdata('UserId'),
						'DueDate' =>  $input['txtDateLine'],
						'AdmissionProcessID' =>  (int)$input['hfAdmissionProcess'],
						'AuditUserName' =>$this->session->userdata('UserId'),
						'Date' => $Date
					);
				

					$this->form_return_document_model->insert_registrant_document_submission_data($Array);
				}
				
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
				     $data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
				} else {    
					$this->db->trans_commit();    
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;

				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}
			
			$this->search(trim($input['hfFormNumber']),$input['hfAdmissionProcess']);
		}
		
		
		
		public function search($FormParam='blank',$AdmissionProcessParam='blank')
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
		
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			$FormNumber="";
			$AdmissionProcessID=0;
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			if($AdmissionProcessParam!="blank")
				$AdmissionProcessID=$AdmissionProcessParam;
			else
				$AdmissionProcessID=$input['ddlAdmissionProcess'];
			
			$Param['RegistrantID']=trim($FormNumber);
			$Param['AdmissionProcessID']=(int)$AdmissionProcessID;
			$data["RegistrantSubmissionData"]=	$this->form_return_document_model->get_registrant_document_submission_data($Param);
			
			$data["DocumentList"]=$this->form_return_document_model->get_document_list($Param);
			$data["AdmissionProcess"]=$this->form_return_document_model->get_admission_process_list();
			
			$data["FormNumber"]=trim($FormNumber);
			$data["AdmissionProcessID"]=$AdmissionProcessID;
         	$this->template->display('entry/form_return_document',$data);
		}
	}
/*	End	of	file	form_return_document.php	*/
/*	Location:		./controllers/entry/form_return_document.php */

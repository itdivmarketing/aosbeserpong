<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class registrant_background extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->model("entry/student_profile_model");
			$this->load->library('form_validation');
			$this->load->config('app_config');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }
		
		public function get_bg_school_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['RegistrantID'] = $RegistrantID;
				$data['BGSchoolData'] = $this->registrant_background_model->get_registrant_previous_school_data($Param);
			}
			else
			{
				$data['BGSchoolData'] = '';
			}
			if($postback) return $data['BGSchoolData'];
			else echo json_encode($data);
		}
		
		public function get_bg_language_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['RegistrantID'] = $RegistrantID;
				$data['BGLanguageData'] = $this->registrant_background_model->get_registrant_background_language_data($Param);
			}
			else
			{
				$data['BGLanguageData'] = '';
			}
			if($postback) return $data['BGLanguageData'];
			else echo json_encode($data);
		}
		
		
		public function load_school_name($TypeOfSchoolID = null,$postback = false)
		{
			if($TypeOfSchoolID)
			{
				$Param['TypeOfSchoolID'] =$TypeOfSchoolID;
				if($TypeOfSchoolID=="all")
					$data['SchoolName'] = $this->registrant_background_model->get_school_list();
				else
					$data['SchoolName'] = $this->registrant_background_model->get_school_list($Param);
			}
			else
			{
				$data['SchoolName'] = '';
			}
			if($postback) return $data['SchoolName'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_sales_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
	public function get_school_list_paging($input = null)
	{
		// TABEL PARAMETER SEARCH, SORT
		$table = array();
		$table['per_page'] =  10;
		//print_r($input['hfPage']);
		
		$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? (int)$input['hfPage'] : 1;
		
		$table['f'] = array(
			'TypeOfSchoolID'  => (isset($input['TypeOfSchoolID']) ? ($input['TypeOfSchoolID']==""?null:$input['TypeOfSchoolID']) : null),
			'txtSearch'	=> isset($input['txtSearch']) ? $input['txtSearch'] : ""
		);
		
		$query = $this->registrant_background_model->get_school_list_paging(array_merge(array('count'=>1),$table['f']));
		$table['total_data']    = $query[0]->TOTAL_DATA;
		$result = $this->registrant_background_model->get_school_list_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
		
		if($result)
		{
			$data['status'] = true;
			$data['data'] = array();
			$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
			$data['data']['result']     = $result;
		}
		else
		{
			$data['status'] = false;
			$data['data'] = array();
			$data['data']['pagination'] = null;
			$data['data']['result']     = null;
		}
		
		
		
		return $data;
		
		
	}
	
	public function search_school()
	{	
		$input = (array)$this->input->post();
		
		$d = $this->get_school_list_paging($input);
		
		$data['data']['result'] = $d['data']['result'];
		$data['data']['pagination'] = $d['data']['pagination'];

		return $this->load->view('entry/json_view', array('json' => array(
			'status' => 'success',
			'data' => $data
		)));
	}
		
		

		public function save_BG_school_data(){
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
			$this->form_validation->set_rules("hfSchoolID","SchoolID","required");
			$this->form_validation->set_rules("hfSchoolName","School Name","required");
			$this->form_validation->set_rules("hfYearLevelID","Year Level","required");
			$this->form_validation->set_rules("hfStartAttendedYear","Start Attended Year","required");
			$this->form_validation->set_rules("hfEndAttendedYear","End Attended Year","required");
			$this->form_validation->set_rules("hfBeginSchoolYear","Begin School Year","required");
			$this->form_validation->set_rules("hfEndSchoolYear","End School Year","required");

			if($this->form_validation->run() != false)
			{	
				$RegistrantID =  $input["hfFormNumber"];
				if($input){
					$this->db->trans_begin();
					$Param['RegistrantID']=$RegistrantID;
					$Param['AuditUserName']=$this->session->userdata('UserId');
					$this->registrant_background_model->delete_registrant_previous_school_data($Param);
					
					$i=1;
					if(isset($input["hfSchoolID"])){
						foreach($input["hfSchoolID"] as $id=>$fieldvalue){
								$SchoolID = $input["hfSchoolID"][$id];
								$SchoolName = $input["hfSchoolName"][$id];
								$YearLevelID = $input["hfYearLevelID"][$id];
								$StartAttendedYear = $input["hfStartAttendedYear"][$id];
								$EndAttendedYear = $input["hfEndAttendedYear"][$id];
								$BeginSchoolYear = $input["hfBeginSchoolYear"][$id];
								$EndSchoolYear = $input["hfEndSchoolYear"][$id];
								$query =array(
									'RegistrantID' => $RegistrantID,  
									'SchoolNo' => $i,
									'SchoolID' => (int)$SchoolID,
									'SchoolName' => $SchoolName,
									'LastYearLevel' =>  (int)$YearLevelID,
									'YearAttended' => $StartAttendedYear,
									'YearWithDrawn' => $EndAttendedYear,
									'BeginningOfSchoolYear' => (int)$BeginSchoolYear,
									'EndOfSchoolYear' => (int)$EndSchoolYear,
									'AuditUserName' => $this->session->userdata('UserId')

								);

								$i++;
								
								$this->registrant_background_model->insert_registrant_previous_school_data($query);
						}
					}

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
					
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}

		
		public function save_BG_language_data(){
		
		
			$input = $this->input->post();

			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
			$this->form_validation->set_rules("hfLanguageID","LanguageID","required");
			$this->form_validation->set_rules("hfFirstLanguage","First Language","required");

		
			if($this->form_validation->run() != false)
			{	
				$RegistrantID =  $input["hfFormNumber"];
				//$RegistrantID =  "1416206";
				if($input){
					$this->db->trans_begin();
					$Param['RegistrantID']=$RegistrantID;
					$Param['AuditUserName']=$this->session->userdata('UserId');
					$this->registrant_background_model->delete_registrant_background_language_data($Param);

					if(isset($input["hfLanguageID"])){
						foreach($input["hfLanguageID"] as $id=>$fieldvalue){
								$LanguageID = $input["hfLanguageID"][$id];
								$FirstLanguage = $input["hfFirstLanguage"][$id];

								$query =array(
									'RegistrantID' => $RegistrantID,  
									'LanguageID' => $LanguageID,
									'FirstLanguage' => (int)$FirstLanguage,
									'AuditUserName' => $this->session->userdata('UserId')
								);

								$this->registrant_background_model->insert_registrant_background_language_data($query);
						}
					}

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			

			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
	
		}
		
		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			$data['TypeOfSchool'] = $this->registrant_background_model->get_type_of_school_list();
			$data['YearLevel'] = $this->registrant_background_model->get_year_level($FormNumber);
			$data['SchoolName'] = $this->registrant_background_model->get_school_list();
			//print_r($data['SchoolName']);die;
			$Param['RegistrantID']=$FormNumber;
			$data['BGSchoolData'] = $this->registrant_background_model->get_registrant_previous_school_data($Param);
			
			$par['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($par);
			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
			
			$data['BGLanguageData'] = $this->registrant_background_model->get_registrant_background_language_data($Param);
			$data['Language'] = $this->registrant_background_model->get_language_list();
			
			
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
						
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
			//print_r($data['BGLanguageData'] );die;
			$data['TabName'] = "registrant_background";
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
         	$this->template->display('entry/registrant_background',$data);
		}
	}
	
	
/*	End	of	file	registrant_background.php	*/
/*	Location:		./controllers/entry/form_return/registrant_background.php */

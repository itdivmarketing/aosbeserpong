<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class personal_development extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/personal_development_model");
					$this->load->model("entry/form_return_back_office_model");
					$this->load->config('app_config');
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->config->load('app_config');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }
		
		public function get_sickness_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['RegistrantID'] = $RegistrantID;
				$data['SicknessData'] = $this->personal_development_model->get_registrant_sickness_data($Param);
			}
			else
			{
				$data['SicknessData'] = '';
			}
			if($postback) return $data['SicknessData'];
			else echo json_encode($data);
		}
		
		public function get_bg_language_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['RegistrantID'] = $RegistrantID;
				$data['BGLanguageData'] = $this->registrant_background_model->get_registrant_background_language_data($Param);
			}
			else
			{
				$data['BGLanguageData'] = '';
			}
			if($postback) return $data['BGLanguageData'];
			else echo json_encode($data);
		}
		
		

		public function save_sickness_data(){
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
	

			if($this->form_validation->run() != false)
			{	
				$RegistrantID = $input["hfFormNumber"];
				if($input){

					$this->db->trans_begin();
					$Param['RegistrantID']=$RegistrantID;
					$Param['AuditUserName']=$this->session->userdata('UserId');
					$this->personal_development_model->delete_registrant_sickness_data($Param);

					if(isset($input["hfSicknessID"])){
						foreach($input["hfSicknessID"] as $id=>$fieldvalue){
								$RegistrantID = $RegistrantID;
								$SicknessID = (int)$input["hfSicknessID"][$id];
								$Memo = $input["hfSicknessNote"][$id];
								$AuditUserName = $this->session->userdata('UserId');
								
								$Array =array(
									'RegistrantID' => $RegistrantID,  
									'SicknessID' => $SicknessID,
									'Memo' => $Memo,
									'AuditUserName' => $AuditUserName
								);

								$this->personal_development_model->insert_registrant_sickness_data($Array);
						}
					}

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}
		
		public function save_health_and_difficulties(){
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");


			if($this->form_validation->run() != false)
			{	
					$RegistrantID = $input["hfFormNumber"];	
					if($input){
					
					$allowed_ext = array('jpg', 'jpeg', 'png', 'pdf','rar','zip');
					$ProblemID=$input['problem_id'];
					

					if(isset($_FILES['file']))
						$file=$_FILES['file'];
						
					$HasFile=$input['HasFile'];
					
					$SavedFile=$input['SavedFile'];
					$Note=$input['txtProblemsNote'];
					$Path=$ProblemID=='3'?"Health":"SchoolWithdrawal";
					$final_filename="";
					
					if($SavedFile!="")
					{
						$final_filename=$SavedFile;
					}
					
					if(isset($file) && $file['error'] == 0 && is_uploaded_file($file['tmp_name']) )
					{
						if($SavedFile!="")
						{
							@unlink( $this->config->item('form_return_BO_upload_path')."StudentInformation/".$Path."/" . $SavedFile );
						}
						$md5name = md5($file['name']);
						$orig_name = preg_replace('/[^a-zA-Z0-9\.-_]/si', '', $file['name']);

						//if($post['type'] == '1')
						//{
							$final_path = $this->config->item('form_return_BO_upload_path')."StudentInformation/".$Path."/";
							//print_r($this->config->site_url()); die;
							$temp_path = $final_path . $md5name;
						//}

						@move_uploaded_file($file['tmp_name'], $temp_path);

						$extension = explode('.', $orig_name);
						$extension = strtolower($extension[count($extension) - 1]);

						if(in_array($extension, $allowed_ext))
						{
							$random = substr(md5(uniqid(microtime(), true)), 0, 6);
							$final_filename = $RegistrantID . '_' . $random . '_' . $orig_name;

							//$file_size = filesize($temp_path);
							rename($temp_path, $final_path . $final_filename);

							//@move_uploaded_file($file['tmp_name'],$final_path . $final_filename);
					
						}
						else
						{
							// remove the data if it's not needed anymore
							@unlink( $final_path . $md5name );
						}
					}
					
					
					$AttachReports=0;
				
					if($SavedFile!="")
						$AttachReports=1;
					else if($HasFile=='1')
						$AttachReports=1;
						
					//print_r($SavedFile);die;
						
					$array = array(
						'RegistrantID' => $RegistrantID,
						'ProblemID' => (int)$ProblemID,
						'Description' => $Note,
						'AttachReports' =>  $AttachReports,
						'AttachReportName' => $final_filename,
						'AuditUserName' => $RegistrantID
					);

					$this->db->trans_start();


					$this->personal_development_model->insert_registrant_problem_difficulties_data($array);


					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
				
			
		}
		echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
		$this->search(trim($input['hfFormNumber']));
	}

		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			$Param['RegistrantID']=$FormNumber;
			
			$data['SicknessData']=  $this->personal_development_model->get_registrant_sickness_data($Param);
			$data['Sickness']=$this->personal_development_model->get_sickness_list(); 
			
			$Param=array();
			$Param['RegistrantID']=$FormNumber;
			$Param['ProblemID']='3';
			$data['HealthData']=$this->personal_development_model->get_registrant_problem_difficulties_data($Param);
			$data['HealthFile']=isset($data['HealthData']->AttachReportName)?$data['HealthData']->AttachReportName:""; 
			
			$par['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($par);
			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
			
			$Param=array();
			$Param['RegistrantID']=$FormNumber;
			$Param['ProblemID']='4';
			$data['SchoolWithdrawalData']=$this->personal_development_model->get_registrant_problem_difficulties_data($Param);
			$data['SchoolWithdrawalFile']=isset($data['SchoolWithdrawalData']->AttachReportName)?$data['SchoolWithdrawalData']->AttachReportName:""; 
			//print_r($data['SicknessData'] );die;
			$data['TabName'] = "personal_development";
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
			
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
         	$this->template->display('entry/personal_development',$data);
		}
	}
	
	
/*	End	of	file	personal_development.php	*/
/*	Location:		./controllers/entry/form_return/personal_development.php */

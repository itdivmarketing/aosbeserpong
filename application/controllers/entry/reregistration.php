<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class reregistration extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/reregistration_model");
			$this->load->library('form_validation');
			$this->load->config('app_config');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}

		public function index(){		
			$this->search("");
			$this->template->display('entry/reregistration');
		}

		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else{
				$FormNumber=trim($input['txtFormNumber']);
			}


			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];

			$data['FormNumber']=$FormNumber;
			$par['StudentID']=$FormNumber;

			$data['StudentData']=$this->student_profile_model->get_student_data($par);
			$par['StudentID']=$FormNumber;
			 

			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				$data['isAgree'] = $data['StudentData']->isAgree;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;

				$YearLevelID= $data['StudentData']->YearLevelID; 
				$StatusLulus = $data['StudentData']->StatusLulus;
				
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
				$data['isAgree'] = '0';
			}
							
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
						
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
		
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
			//print_r($data['BGLanguageData'] );die;
			$data['TabName'] = "reregistration";
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);

			$data['SchoolRegulation'] = $this->reregistration_model->get_school_regulation();
			$data['message'] = $this->session->flashdata('message');
			if($data['message'] != null && $data['message']!= "")
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
         	$this->template->display('entry/reregistration',$data);


		}

		public function check_agree(){
			$input = $this->input->post();
			$FormNumber = $input['hfFormNumber'];
			$checked = $this->input->post('agree');

			$par['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($par);

			$Param['RegistrantID'] = $par['StudentID'];
			$Param['AuditUserName']=$this->session->userdata('UserId');
			$Param['YearLevelID'] = $data['StudentData']->YearLevelID; 
			$Param['Pass'] = $data['StudentData']->StatusLulus;
			$Param['isAgree'] = $checked;
			
			$this->reregistration_model->insert_check_agree($Param);
			$data['message'] = "Success save to database";
			$this->session->set_flashdata('message', $data['message']);
			//$this->search(trim($input['hfFormNumber']));		
			redirect(site_url().'/entry/reregistration/search?txtFormNumber='.$FormNumber);
		}


}
<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class student_profile extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->library('form_validation');
			$this->load->config('app_config');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }

		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_sales_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		public function get_city($country, $postback = false)
		{
			$Param['CountryID'] = $country;
			$data['City'] = $this->student_profile_model->get_city($Param);

			if($postback) return $data['City'];
			else echo json_encode($data);
		}
		
		public function save_student_data()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
	
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			$this->form_validation->set_rules("ddlReligion","Religion","required");
			$this->form_validation->set_rules("txtEmail","Email","required");
			$this->form_validation->set_rules("txtAddress","Address","required");
			$this->form_validation->set_rules("ddlCity","City","required");	
			$this->form_validation->set_rules("ddlStayingWith","Staying With","required");
			$this->form_validation->set_rules("ddlEmergencyContact","Emergency Contact","required");	
			
			$this->form_validation->set_rules("txtCellPhone","CellPhone","required");
		
		
				
			
			if($this->form_validation->run() != false)
			{	
				$d = array();
				$p= array();
				$StayingWithID="";
				$ParentStatus="";
				$RegistrantID =  $this->session->userdata('RegistrantID');
		
				switch($input['ddlStayingWith']){
					case 'F':$StayingWithID=1;$ParentStatus='F';
						break;
					case 'M':$StayingWithID=2;$ParentStatus='M';
						break;
					case 'BP':$StayingWithID=3;$ParentStatus='BP';
						break;
					case 'G':$StayingWithID=4;$ParentStatus='G';
						break;
					case 'O':$StayingWithID=5;
						break;
				
				}
				
				$this->db->trans_begin();
				
				$d['StudentID'] = trim($input['txtFormNumber']);
				$d['ReligionID'] = (int)trim($input['ddlReligion']);
				$d['SubjectReligionID'] = (int)trim($input['ddlSubjectReligion']);
				$d['Email'] = trim($input['txtEmail']);
				$d['DomincileID'] = trim($input['ddlDomicile']);
				$d['Address'] = trim($input['txtAddress']);
				$d['CityID'] = (int)trim($input['ddlCity']);
				$d['RegionID'] = trim($input['ddlRegion']);
				$d['PostalCode'] = trim($input['txtPostalCode']);
				$d['StayingWithID'] = (int)trim($StayingWithID);
				$d['EmergencyStatus'] = trim($input['ddlEmergencyContact']);
				$d['AuditUserName'] =$this->session->userdata('UserId');
			
				//print_r($d['CityID']);die;
				$this->student_profile_model->insert_student_data($d);
				
				if($ParentStatus!=""&&$ParentStatus!="BP"){
		
					$query = array(
					'StudentID' => trim($input['txtFormNumber']),  
					'ParentStatus' => $ParentStatus,
					'Address' => trim($input['txtAddress']),
					'CityID' => (int) $input['ddlCity'],
					'PostalCode' => $input['txtPostalCode'],
					'StayingFlag' => 1,
					'AuditUserName' => $this->session->userdata('UserId')
					);
					
					$this->parent_profile_model->insert_parent_data($query);

				}
				
				if($ParentStatus=="BP"){
				
					$query = array(
					'StudentID' =>trim($input['txtFormNumber']) ,  
					'ParentStatus' => 'F',
					'Address' => trim($input['txtAddress']),
					'CityID' => (int) $input['ddlCity'],
					'PostalCode' => $input['txtPostalCode'],
					'StayingFlag' => 1,
					'AuditUserName' => $this->session->userdata('UserId')
					);
					
					$this->parent_profile_model->insert_parent_data($query);
					
					
					
					$query = array(
					'StudentID' =>trim($input['txtFormNumber']) ,  
					'ParentStatus' => 'M',
					'Address' => trim($input['txtAddress']),
					'CityID' =>( int) $input['ddlCity'],
					'PostalCode' => $input['txtPostalCode'],
					'StayingFlag' => 1,
					'AuditUserName' => $this->session->userdata('UserId')
					);
					
					$this->parent_profile_model->insert_parent_data($query);
				

				}
				
				$p['StudentID'] = trim($input['txtFormNumber']);
				$p['ParentStatus'] = 'S';
				$p['AuditUserName'] =$this->session->userdata('UserId');
				
				$this->phone_model->delete_phone_data($p);
				
				$p['StudentID'] = trim($input['txtFormNumber']);
				$p['ParentStatus'] = 'S';
				$p['PhoneType'] = 'M';
				$p['PhoneNumber'] = trim($input['txtCellPhone']);
				$p['AuditUserName'] =$this->session->userdata('UserId');
				
				if( trim($input['txtCellPhone'])!=""){
					$this->phone_model->insert_phone_data($p);
				}
				$p['StudentID'] = trim($input['txtFormNumber']);
				$p['ParentStatus'] = 'S';
				$p['PhoneType'] = 'R';
				$p['PhoneNumber'] = trim($input['txtPhone']);
				$p['AuditUserName'] =$this->session->userdata('UserId');
				
				if( trim($input['txtPhone'])!=""){
					$this->phone_model->insert_phone_data($p);
				}
				
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
					$data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
				} else {    
					$this->db->trans_commit();    
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;

				}

			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['txtFormNumber']));
		}
		
		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=trim($input['txtFormNumber']);
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			$data['Religion'] = $this->student_profile_model->get_religion();
			$data['DomicileType'] = $this->student_profile_model->get_domicile_type();
			$data['TypeOfSchool'] = $this->student_profile_model->get_type_of_school_list();
			$data['Country'] = $this->student_profile_model->get_country();
			$data['Region'] = $this->student_profile_model->get_region();
			$data['StayingWith'] = $this->student_profile_model->get_parent();
				$options=array();
			foreach($data['StayingWith'] as $row){
			if($row->ParentID =='F' || $row->ParentID =='M' || $row->ParentID =='G')
				$options=$options+array($row->ParentID => $row->ParentName);
			}
			$options=$options+array('BP' => 'Both Parent');
			$options=$options+array('O' => 'Others');
				
			
			$data['StayingWith'] = $options;
			
			$data['EmergencyContact'] = $this->student_profile_model->get_parent();
			$Param['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($Param);
			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
			
			$param['CountryID'] = (isset($data['StudentData']->CountryID) ? $data['StudentData']->CountryID : 0);
			$data['City'] = $this->student_profile_model->get_city($param);
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='S';
			$RegistrantPhoneDataDB= $this->phone_model->get_phone_data($Param);
			
			foreach($RegistrantPhoneDataDB as $row){
			
			if($row->PhoneType=='M'){
				$data['StudentCellPhoneData']=$row->PhoneNumber;
				}
			else if($row->PhoneType=='R'){
				$data['StudentPhoneData']=$row->PhoneNumber;	
				}
			}
			$data['TabName'] = "student_profile";
			
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
		
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			
			$FORegisteredParam['RegistrantID']=$FormNumber;
			$data['FORegistered']=$this->student_profile_model->check_fo_registered($FORegisteredParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
		
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
		
			//print_r($data['StudentData']->StayingWithID);die;
         	$this->template->display('entry/student_profile',$data);
		}
	}
/*	End	of	file	student_profile.php	*/
/*	Location:		./controllers/entry/form_return/student_profile.php */

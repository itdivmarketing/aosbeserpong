<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Form_sales extends CI_Controller {
	
		var $period;
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_sales_model");
			$this->load->model("entrance_test/registrant_allocation_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			//$this->output->nocache();
			
		}
		public function index()
        {
			$d = $this->get_form_sales_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_sales_model->get_school_level();
			$data['YearLevel'] = $this->form_sales_model->get_year_level();
         	$this->template->display('entry/form_sales',$data);
        }
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_sales_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_sales_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		public function get_admissionID($TermID = null,$postback = false)
		{
			if($TermID)
			{
				$Param['TermID'] = $TermID;
				$data['AdmissionID']=$this->form_sales_model->get_admissionID($Param);
			}
			else
			{
				$data['AdmissionID'] = '';
			}
			if($postback) return $data['AdmissionID'];
			else echo json_encode($data);
		}
		
		public function get_payment_method($postback = false)
		{
			
			$data['PaymentMethod'] = $this->form_sales_model->get_payment_method();
		
			if($postback) return $data['PaymentMethod'];
			else echo json_encode($data);
		}
		
		public function check_form_validation($FormNumber="",$TermID="",$postback = false)
		{
			$TermID=($TermID==NULL||empty($TermID))?"":$TermID;
			if(!empty($FormNumber)){
				$Param['RegistrantID']=$FormNumber;
				$Param['Location']='8';
				$Param['TermID']=$TermID;
				$data['FormStatus'] = $this->form_sales_model->check_form_validation($Param);
				
				if($postback) return $data['FormStatus'];
					else echo json_encode($data);
			}
		
		}
		
		public function get_form_amount($FormNumber="",$postback = false)
		{
			if($FormNumber){
				$Param['FormNo']=$FormNumber;
				$data['FormAmount'] = $this->form_sales_model->get_form_amount($Param);
			}
			if($postback) return $data['FormAmount'];
			else echo json_encode($data);
		}
		
		public function send_email($Email="",$RegistrantID=""){
			// send email to the user
			if($Email==""||empty($Email)||$Email==NULL)
				return;
			if($RegistrantID==""||empty($RegistrantID)||$RegistrantID==NULL)
				return;	
			// send email to the user
			$EmailDetail=$this->form_sales_model->get_email_detail($RegistrantID,'002',$this->session->userData('GeneratedPassword'));
			//echo $EmailDetail;
			$this->load->library('email');

			$this->config->load('app_config');
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = $this->config->item('mail_host');
			$config['smtp_user'] = $this->config->item('mail_username');
			$config['smtp_pass'] = $this->config->item('mail_password');
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from('no-reply-admission@binus.ac.id', 'Admisi Binus School');
			$this->email->to($EmailDetail->EmailTo);
			$cc=explode(";",$EmailDetail->EmailCC);
			$bcc=explode(";",$EmailDetail->EmailBCC);
		
			$this->email->cc($cc);
			$this->email->bcc($bcc);
		
			$this->email->subject($EmailDetail->EmailSubject);

			$mail_body = $EmailDetail->EmailBody;

			$this->email->message($mail_body);
			$this->email->send();
		
		
		}
		
		
		public function generate_password($method,$RegistrantID="",$Email="",$postback = false)
		{
			
			//$Param['FormNo']=$FormNumber;
			$rand= substr(md5(microtime()),rand(0,26),6);
			$data['GeneratedPassword'] = md5(md5($rand));
			$this->session->set_userdata(
				array(
					'GeneratedPassword' => $rand
				)
			);
			
			if($method=="view"){
				if($Email!=""&&$Email!=NULL){
				
				$Param['RegistrantID']=$RegistrantID;
				$Param['Password']=$data['GeneratedPassword'];
				$Param['Email']=$Email;
				$Param['AuditUserName']=$this->session->userdata('UserId');
				
				$this->db->trans_begin();
				
				$this->form_sales_model->update_registrant_password($Param);
				
				
				if ( $this->db->trans_status() === FALSE  ) {
				   $this->db->trans_rollback();
				} else {    
				   $this->db->trans_commit();    
				   $this->send_email($Email,$RegistrantID);
				}
				
			
				//$data['GeneratedPassword']=$Email;
				}
			}
			
			
			
		
			//$this->session->userData('GeneratedPassword')=$rand;
			if($postback) return $data['GeneratedPassword'];
			else echo json_encode($data);
		}
		

		public function get_form_sold_view($RegistrantID, $postback = false)
		{
			$Param['RegistrantID'] = $RegistrantID;
			
			$data['FormSoldView'] = $this->form_sales_model->get_form_sold_view($Param);
			//echo $data['FormSoldView'][0];
			if($postback) return $data['FormSoldView'];
			else echo json_encode($data);
		}
		
		public function get_printable_view($formnumber = null)
		{
			$this->load->model("report/billing_and_payment_receipt_model");
			$temp = explode("-", $this->period);
			$P['RegistrantID'] = (isset($_GET['hfFormNumber']) ? $_GET['hfFormNumber'] : $formnumber);
			$data['report'] =  $this->billing_and_payment_receipt_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->billing_and_payment_receipt_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['Semester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->billing_and_payment_receipt_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->billing_and_payment_receipt_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			$this->load->view('report/print_receipt',$data);
		}
		
		public function save_form_sold()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			$this->form_validation->set_rules("txtViewAdmissionID","Admission ID","required");
			$this->form_validation->set_rules("txtViewFormNumber","Form Number","required");
			$this->form_validation->set_rules("txtViewAdmissionYear","Admission Year","required");
			$this->form_validation->set_rules("txtViewAdmissionSemester","Semester","required");
			$this->form_validation->set_rules("txtViewPaymentDate","Payment Date","required");
			$this->form_validation->set_rules("txtViewAmount","View Amount","required");
			$this->form_validation->set_rules("hfViewAmount","HF View Amount","required");
			//$this->form_validation->set_rules("ddlViewSchoolLevel","School Level","required");
			$this->form_validation->set_rules("ddlViewPaymentMethod","Payment Method","required");
			$this->form_validation->set_rules("txtViewAmountPaid","Amount Paid","required");
			$this->form_validation->set_rules("txtViewReceiptName","Receipt Name","required");
			$this->form_validation->set_rules("txtViewStudentName","Student Name","required");
			$this->form_validation->set_rules("txtViewOfficerName","Officer Name","required");
			$this->form_validation->set_rules("txtViewPaidBy","Paid By","required");
			$this->form_validation->set_rules("ddlViewYearLevel","Year Level","required");
			$this->form_validation->set_rules("txtViewPhoneNumber","Phone Number","required");
			$this->form_validation->set_rules("ddlViewAdmissionTerm","Admission Term","required");
			$this->form_validation->set_rules("txtViewEmail","Email","required");
			$this->form_validation->set_rules("txtViewPassword","Password","required");
				
			
			if($this->form_validation->run() != false)
			{	
				$d = array();
				$this->db->trans_begin();
				
				$d['RegistrantID'] = trim($input['txtViewFormNumber']);
				$d['AcademicYear'] = $input['txtViewAdmissionYear'];
				$PaymentDate = date_parse_from_format("d M Y H:i",$input['txtViewPaymentDate']);
				//print_r($PaymentDate);
				//die;
				//$d['DueDate'] =$PaymentDate['year'].'-'.$PaymentDate['month'].'-'.$PaymentDate['day'].' '.$PaymentDate['hour'].':'.$PaymentDate['minute'];
				$d['DueDate'] = $input['txtViewPaymentDate'];
				$d['Amount'] = (float)$input['hfViewAmount'];
				$d['PaymentMethodID'] = (int)$input['ddlViewPaymentMethod'];
				$d['AmountPaid'] =(float) $input['txtViewAmountPaid'];
				$d['Desc'] = "";
				$d['Name'] = $input['txtViewReceiptName'];
				$d['Note'] = $input['txtViewStudentName'];
				$d['Officer'] = $input['txtViewOfficerName'];
				$d['PaidBy'] = $input['txtViewPaidBy'];
				$d['YearLevelId'] = (int)$input['ddlViewYearLevel'];
				$d['PhoneNumberSale'] = $input['txtViewPhoneNumber'];
				$d['TermId'] = $input['ddlViewAdmissionTerm'];
				$d['Email'] = $input['txtViewEmail'];
				$d['Password'] = $input['txtViewPassword'];
				$d['AuditUserName'] = $this->session->userdata('UserId');
				

				$this->form_sales_model->save_form_sold($d);
				
		
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
				     $data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
				} else {    
					$this->db->trans_commit();    
					 $this->send_email($d['Email'],$d['RegistrantID']);
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
					$this->get_printable_view(trim($input['txtViewFormNumber']));
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				$this->index();
			}
		}
		
		public function get_form_sales_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'Period'		=> $this->period,
				'RegistrantID'  => isset($input['txtFormNumber']) ? null_setter($input['txtFormNumber']) : null,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'YearLevel'	=> isset($input['ddlYearLevel']) ? null_setter($input['ddlYearLevel']) : null,
				'AdmissionTerm'	=> isset($input['ddlAdmissionTerm']) ? null_setter($input['ddlAdmissionTerm']) : null
			);
			$query = $this->form_sales_model->get_form_sales(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->form_sales_model->get_form_sales(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_form_sales_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['SchoolLevel'] = $this->registrant_allocation_model->get_school_level();
			$data['YearLevel'] = $this->get_year_level($input['ddlSchoolLevel'],true);
         	$this->template->display('entry/form_sales',$data);
		}
	}
/*	End	of	file	form_sales.php	*/
/*	Location:		./controllers/entry/form_sales.php */

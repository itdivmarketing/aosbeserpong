<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class ID_card extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/ID_card_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->config('app_config');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }
		
		public function save_student_ID_card()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				
					$RegistrantID=trim($input['txtFormNumber']);
					$Param['RegistrantID']=$RegistrantID;
					$Param['ParentID']='S';
					$Param['AuditUserName']=$this->session->userdata('UserId');
					$this->db->trans_begin();
					$this->ID_card_model->delete_registrant_id_card_data($Param);
					
					$CanInputID['Identity']=0;
					$CanInputID['Passport']=0;
					$CanInputID['KITAS']=0;
					$CanInputID['VISA']=0;
					
					if(trim($input['txtStudentIDNo'])!="")
						$CanInputID['Identity']=1;
					if(trim($input['txtStudentPassportNo'])!="")
						$CanInputID['Passport']=1;
					if(trim($input['txtStudentKITASNo'])!="")
						$CanInputID['KITAS']=1;
					if(trim($input['txtStudentVISANo'])!="")
						$CanInputID['VISA']=1;
					
					if($CanInputID['Identity']==1){
						$Row['Identity']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'S',
							'IDNo' => trim($input['txtStudentIDNo']),
							'IDType' =>  1,
							'ExpiredDate' => NULL,
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['Passport']==1){
						$Row['Passport']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'S',
							'IDNo' => trim($input['txtStudentPassportNo']),
							'IDType' =>  2,
							'ExpiredDate' =>  trim($input['txtStudentPassportExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['KITAS']==1){
						$Row['KITAS']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'S',
							'IDNo' => trim($input['txtStudentKITASNo']),
							'IDType' =>  3,
							'ExpiredDate' =>  trim($input['txtStudentKITASExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['VISA']==1){
						$Row['VISA']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'S',
							'IDNo' => trim($input['txtStudentVISANo']),
							'IDType' =>  4,
							'ExpiredDate' =>  trim($input['txtStudentVISAExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
					if(isset($Row)){
						foreach($Row as $ChildRow){
							$Array =array(
								'RegistrantID' => $ChildRow['RegistrantID'],  
								'ParentID' => $ChildRow['ParentID'],
								'IDNo' => $ChildRow['IDNo'],
								'IDType' => $ChildRow['IDType'],
								'ExpiredDate' => $ChildRow['ExpiredDate']==""?NULL:$ChildRow['ExpiredDate'],
								'AuditUserName' => $ChildRow['AuditUserName']
							);
							
							$this->ID_card_model->insert_registrant_id_card_data($Array);
						}
					}
					
					$Array = array( 
						'StudentID' => $RegistrantID,  
						'NationalityID' => (int)$input['ddlStudentNationality'],
						'AuditUserName' =>  $this->session->userdata('UserId')
					);
				
					$this->ID_card_model->insert_registrant_nationality_data($Array);
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->search(trim($input['txtFormNumber']));

			}
			
			
		public function save_father_ID_card()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				
					$RegistrantID=trim($input['txtFormNumber']);
					$Param['RegistrantID']=$RegistrantID;
					$Param['ParentID']='F';
					$Param['AuditUserName']=$this->session->userdata('UserId');
					$this->db->trans_begin();
					$this->ID_card_model->delete_registrant_id_card_data($Param);
					
					$CanInputID['Identity']=1;
					$CanInputID['Passport']=1;
					$CanInputID['KITAS']=1;
					//$CanInputID['VISA']=1;
					
					if(trim($input['txtFatherIDNo'])=="")
						$CanInputID['Identity']=1;
					if(trim($input['txtFatherPassportNo'])=="")
						$CanInputID['Passport']=1;
					if(trim($input['txtFatherKITASNo'])=="")
						$CanInputID['KITAS']=1;
				
					if($CanInputID['Identity']==1){
						$Row['Identity']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'F',
							'IDNo' => trim($input['txtFatherIDNo']),
							'IDType' =>  1,
							'ExpiredDate' => NULL,
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['Passport']==1){
						$Row['Passport']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'F',
							'IDNo' => trim($input['txtFatherPassportNo']),
							'IDType' =>  2,
							'ExpiredDate' =>  trim($input['txtFatherPassportExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['KITAS']==1){
						$Row['KITAS']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'F',
							'IDNo' => trim($input['txtFatherKITASNo']),
							'IDType' =>  3,
							'ExpiredDate' =>  trim($input['txtFatherKITASExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
					
					
					if(isset($Row)){
						foreach($Row as $ChildRow){
							$Array =array(
								'RegistrantID' => $ChildRow['RegistrantID'],  
								'ParentID' => $ChildRow['ParentID'],
								'IDNo' => $ChildRow['IDNo'],
								'IDType' => $ChildRow['IDType'],
								'ExpiredDate' => $ChildRow['ExpiredDate']==''?NULL:$ChildRow['ExpiredDate'],
								'AuditUserName' => $ChildRow['AuditUserName']
							);
							
							$this->ID_card_model->insert_registrant_id_card_data($Array);
						}
					}
					
					$Array = array( 
						'StudentID' => $RegistrantID, 
						'ParentStatus' => 'F', 
						'NationalityID' => (int)$input['ddlFatherNationality'],
						'AuditUserName' =>  $this->session->userdata('UserId')
					);
				
					$this->ID_card_model->insert_registrant_parent_nationality_data($Array);
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->search(trim($input['txtFormNumber']));

			}
		
		
		public function save_mother_ID_card()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				
					$RegistrantID=trim($input['txtFormNumber']);
					$Param['RegistrantID']=$RegistrantID;
					$Param['ParentID']='M';
					$Param['AuditUserName']=$this->session->userdata('UserId');
					$this->db->trans_begin();
					$this->ID_card_model->delete_registrant_id_card_data($Param);
					
					$CanInputID['Identity']=1;
					$CanInputID['Passport']=1;
					$CanInputID['KITAS']=1;
					//$CanInputID['VISA']=1;
					
					if(trim($input['txtMotherIDNo'])=="")
						$CanInputID['Identity']=1;
					if(trim($input['txtMotherPassportNo'])=="")
						$CanInputID['Passport']=1;
					if(trim($input['txtMotherKITASNo'])=="")
						$CanInputID['KITAS']=1;
				
					if($CanInputID['Identity']==1){
						$Row['Identity']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'M',
							'IDNo' => trim($input['txtMotherIDNo']),
							'IDType' =>  1,
							'ExpiredDate' => NULL,
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['Passport']==1){
						$Row['Passport']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'M',
							'IDNo' => trim($input['txtMotherPassportNo']),
							'IDType' =>  2,
							'ExpiredDate' =>  trim($input['txtMotherPassportExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
										
					if($CanInputID['KITAS']==1){
						$Row['KITAS']=array(
							'RegistrantID' => $RegistrantID,  
							'ParentID' => 'M',
							'IDNo' => trim($input['txtMotherKITASNo']),
							'IDType' =>  3,
							'ExpiredDate' =>  trim($input['txtMotherKITASExpDate']),
							'AuditUserName' => $this->session->userdata('UserId')
						);
					}
					
					
					
					if(isset($Row)){
						foreach($Row as $ChildRow){
							$Array =array(
								'RegistrantID' => $ChildRow['RegistrantID'],  
								'ParentID' => $ChildRow['ParentID'],
								'IDNo' => $ChildRow['IDNo'],
								'IDType' => $ChildRow['IDType'],
								'ExpiredDate' => $ChildRow['ExpiredDate']==''?NULL:$ChildRow['ExpiredDate'],
								'AuditUserName' => $ChildRow['AuditUserName']
							);
							
							$this->ID_card_model->insert_registrant_id_card_data($Array);
						}
					}
					
					$Array = array( 
						'StudentID' => $RegistrantID, 
						'ParentStatus' => 'M', 
						'NationalityID' => (int)$input['ddlMotherNationality'],
						'AuditUserName' =>  $this->session->userdata('UserId')
					);
				
					$this->ID_card_model->insert_registrant_parent_nationality_data($Array);
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->search(trim($input['txtFormNumber']));

		}
			

		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			$data['Country']=$this->ID_card_model->get_country_list();
			$Param['StudentID']=$FormNumber;
			$data['StudentNationality']=$this->ID_card_model->get_registrant_nationality_data($Param);
			//$data['StudentData']=$tihs->ID_card_model->;
			$Param=array();
			$Param['RegistrantID']=$FormNumber;
			$Param['ParentID']='S';
			$StudentIDCardDataDB=$this->ID_card_model->get_registrant_id_card_data($Param);
			
			$StudentIDCardData=array();
			
			
			foreach($StudentIDCardDataDB as $row){
				if($row->IDType=='1'){
					$StudentIDCardData['Identity']=$row;
				
				}
				else if($row->IDType=='2'){
					$StudentIDCardData['passport']=$row;
				
				}
				else if($row->IDType=='3'){
					$StudentIDCardData['KITAS']=$row;
				
				}
				else if($row->IDType=='4'){
					$StudentIDCardData['visa']=$row;
				
				}	
			}
			
			$data['StudentData']=$StudentIDCardData;
			
			//father
			$Param=array();
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='F';
			$data['FatherNationality']=$this->ID_card_model->get_registrant_parent_nationality_data($Param);
			$Param=array();
			$Param['RegistrantID']=$FormNumber;
			$Param['ParentID']='F';
			$FatherIDCardDataDB=$this->ID_card_model->get_registrant_id_card_data($Param);
			
			$FatherIDCardData=array();
			
			
			foreach($FatherIDCardDataDB as $row){
				if($row->IDType=='1'){
					$FatherIDCardData['Identity']=$row;
				
				}
				else if($row->IDType=='2'){
					$FatherIDCardData['passport']=$row;
				
				}
				else if($row->IDType=='3'){
					$FatherIDCardData['KITAS']=$row;
				
				}
				else if($row->IDType=='4'){
					$FatherIDCardData['visa']=$row;
				
				}	
			}
			
			$data['FatherData']=$FatherIDCardData;
			
			//mother
			
				$Param=array();
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='M';
			$data['MotherNationality']=$this->ID_card_model->get_registrant_parent_nationality_data($Param);
			$Param=array();
			$Param['RegistrantID']=$FormNumber;
			$Param['ParentID']='M';
			$MotherIDCardDataDB=$this->ID_card_model->get_registrant_id_card_data($Param);
			
			$MotherIDCardData=array();
			
			
			foreach($MotherIDCardDataDB as $row){
				if($row->IDType=='1'){
					$MotherIDCardData['Identity']=$row;
				
				}
				else if($row->IDType=='2'){
					$MotherIDCardData['passport']=$row;
				
				}
				else if($row->IDType=='3'){
					$MotherIDCardData['KITAS']=$row;
				
				}
				else if($row->IDType=='4'){
					$MotherIDCardData['visa']=$row;
				
				}	
			}
			
			$data['MotherData']=$MotherIDCardData;
			$data['TabName'] = "ID_card";
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
			
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
			//print_r($data['StudentNationality']);die;
			
			$par['StudentID']=$FormNumber;
			$data['StudentDatas']=$this->student_profile_model->get_student_data($par);
			
			if(sizeof($data['StudentDatas']) > 0)
			{
				$data['txtName'] = $data['StudentDatas']->FirstName.' '.$data['StudentDatas']->MiddleName.' '.$data['StudentDatas']->LastName;
				$data['txtGrade'] = $data['StudentDatas']->YearLevelName;
				$data['txtTerm'] = $data['StudentDatas']->TermID.' - '.$data['StudentDatas']->TermName;
				switch($data['StudentDatas']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
         	$this->template->display('entry/ID_card',$data);
		}
	}
/*	End	of	file	ID_card.php	*/
/*	Location:		./controllers/entry/form_return/ID_card.php */

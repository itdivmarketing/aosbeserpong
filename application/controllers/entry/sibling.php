<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class sibling extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/sibling_model");
			$this->load->library('form_validation');
			$this->load->model("entry/form_return_back_office_model");
			$this->load->helper('admission_helper');
			$this->load->config('app_config');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }
		
		public function get_CE_sibling_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['StudentID'] = $RegistrantID;
				$data['CESiblingData'] = $this->sibling_model->get_current_enrolled_sibling_data($Param);
			}
			else
			{
				$data['CESiblingData'] ='';
			}
			if($postback) return $data['CESiblingData'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->sibling_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		public function search_sibling_by_name($StudentID = null,$postback = false)
		{
			if(isset($StudentID))
			{
				$Param['StudentID'] = $StudentID;
				$data['SiblingResult'] = $this->sibling_model->search_sibling_by_name($Param);
			}
			else
			{
				$data['SiblingResult'] ='';
			}
			if($postback) return $data['SiblingResult'];
			else echo json_encode($data);
		}
		
		public function get_others_sibling_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['RegistrantID'] = $RegistrantID;
				$data['OthersSiblingData'] = $this->sibling_model->get_others_sibling_data($Param);
			}
			else
			{
				$data['OthersSiblingData'] ='';
			}
			if($postback) return $data['OthersSiblingData'];
			else echo json_encode($data);
		}		
		
		public function check_sibling_studentID($RegistrantID = null,$StudentID = null,$postback = false)
		{
			if($RegistrantID && $StudentID ){
				$Param['StudentID']=$StudentID;
				$Param['SelfID']=$RegistrantID;
				$data['data'] = $this->sibling_model->check_sibling_studentID($Param);
			}
			else{
				$data['data'] = '';
			}
			if($postback) return $data['data'];
			else echo json_encode($data);
		}
	
	
		public function save_current_enrolled_sibling($method = '')
		{
			$input = $this->input->post();
			$RegistrantID =  $input["hfFormNumber"];
			
			if($input){
				$this->db->trans_begin();
				$Param['RegistrantID']=$RegistrantID;
				$Param['AuditUserName']=$this->session->userdata('UserId');
				$HaveSiblingID=$input['hfHaveSiblingID'];
				$SiblingID=$input['hfNewSiblingID'];
				
				$LastDeletedSiblingID="";
				$NewDeletedSiblingID="";
				if(isset($input["hfDeletedSiblingID"])){
				foreach($input["hfDeletedSiblingID"] as $id=>$fieldvalue){
						//print_r($input["hfDeletedSiblingID"][$id]);die;
						$DeleteParam['StudentID']=$input["hfDeletedStudentID"][$id];
						$DeleteParam['AuditUserName']=$this->session->userdata('UserId');
						$this->sibling_model->delete_current_enrolled_sibling_data($DeleteParam);
						
						if($input["hfDeletedSiblingID"][$id]!=$LastDeletedSiblingID){
							$NewDeletedSiblingID=$this->sibling_model->get_current_enrolled_sibling_id()->SiblingID;
							
							}

							$query =array(
								'SiblingID' => $NewDeletedSiblingID,  
								'StudentID' => $input["hfDeletedStudentID"][$id],
								'AuditUserName' => $this->session->userdata('UserId'),
								'HaveSiblingID' => 1
							);
							
							$this->sibling_model->insert_current_enrolled_sibling_data($query);
						
						$LastDeletedSiblingID=$input["hfDeletedStudentID"][$id];
					}
				}
				
				if(isset($input["hfSiblingID"])){	
					foreach($input["hfSiblingID"] as $id=>$fieldvalue){
						$DeleteParam['StudentID']=$input["hfStudentID"][$id];
						$DeleteParam['AuditUserName']=$this->session->userdata('UserId');
						$this->sibling_model->delete_current_enrolled_sibling_data($DeleteParam);
					}
					
					if($HaveSiblingID!=1){
						$LastSiblingID= $this->sibling_model->get_current_enrolled_sibling_id();
						$SiblingID=$LastSiblingID->SiblingID;
					}
					
					foreach($input["hfSiblingID"] as $id=>$fieldvalue){
							$SiblingID = $SiblingID;
							$StudentID = $input["hfStudentID"][$id];
							$AuditUserName = $this->session->userdata('UserId');
							$HaveSiblingID = $HaveSiblingID;

							$query =array(
								'SiblingID' => $SiblingID,  
								'StudentID' => $StudentID,
								'AuditUserName' => $AuditUserName,
								'HaveSiblingID' => (int)$HaveSiblingID	
							);
							
							$this->sibling_model->insert_current_enrolled_sibling_data($query);
					}
				}

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
			}

			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}
		


			
		public function save_others_sibling($method = '')
		{
			$input = $this->input->post();
			$RegistrantID =  $input["hfFormNumber"];
			
			if($input){
				$this->db->trans_begin();
				$Param['RegistrantID']=$RegistrantID;
				$Param['AuditUserName']=$this->session->userdata('UserId');

				
				$this->sibling_model->delete_others_sibling_data($Param);
				
				if(isset($input["hfName"])){
					
					
					$i=1;
					foreach($input["hfName"] as $id=>$fieldvalue){
							$PotentialSiblingName = $input["hfName"][$id];
							$SchoolLevelID =  $input["hfSchoolLevelID"][$id];
							$YearLevelID = $input["hfYearLevelID"][$id];
							$Birthday =  $input['hfBirthDate'][$id]==""?NULL:$input['hfBirthDate'][$id];
							$AdmissionPeriod= trim($input["hfAdmissionPeriod"][$id]);
							$AuditUserName = $this->session->userdata('UserId');
							
							$query =array(
								'RegistrantID' => $RegistrantID,  
								'PotentialSiblingName' => $PotentialSiblingName,
								'PotentialSiblingID' => $i,
								'SchoolLevelID' => $SchoolLevelID,
								'YearLevelID' => $YearLevelID,
								'Birthday' => $Birthday,	
								'AdmissionPeriod' => $AdmissionPeriod,
								'AuditUserName' => $AuditUserName		
							);
							$this->sibling_model->insert_others_sibling_data($query);
							$i++;
					}
				}

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
			}

			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}
		
	

		public function get_sibling_by_name_paging($StudentName=null,$hfPage=null)
        {
		
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($hfPage) and !empty($hfPage)) ? $hfPage : 1;
			
			$table['f'] = array(
				'StudentName'  => isset($StudentName)&&$StudentName!="null" ? null_setter($StudentName)  : null

			);
			$query = $this->sibling_model->search_sibling_by_name(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->sibling_model->search_sibling_by_name(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				//$data['data'] = array();
				$data['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['SiblingResult']     = $result;
			}
			else
			{
				$data['status'] = false;
				//$data['data'] = array();
				$data['pagination'] = null;
				$data['totaldata'] = $table['f']  ;
				$data['SiblingResult']  = null;
			}
			
			echo json_encode($data); 
		}
		
		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];

			$data['FormNumber']=$FormNumber;

			$Param['StudentID']=$FormNumber;
			$data['CESiblingData'] = $this->sibling_model->get_current_enrolled_sibling_data($Param);
			
			$data['StudentData']=$this->student_profile_model->get_student_data($Param);
if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
		
			
			$Param=array();
			$Param['RegistrantID']=$FormNumber;
			$data['OthersSiblingData']=$this->sibling_model->get_others_sibling_data($Param);
			$data['SchoolLevel']=$this->sibling_model->get_school_level_list();
			$data['YearLevel']=$this->sibling_model->get_year_level();
			//print_r($data['CESiblingData'] );die;
			$data['TabName'] = "sibling";
		$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
			
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
				$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
         	$this->template->display('entry/sibling',$data);
		}
	}
	
	
/*	End	of	file	sibling.php	*/
/*	Location:		./controllers/entry/form_return/sibling.php */

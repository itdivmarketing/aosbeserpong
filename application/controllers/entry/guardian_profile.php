<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class guardian_profile extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->library('form_validation');
			$this->load->config('app_config');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {

			$this->search("");
        }

		public function get_city($country, $postback = false)
		{
			$Param['CountryID'] = $country;
			$data['City'] = $this->student_profile_model->get_city($Param);

			if($postback) return $data['City'];
			else echo json_encode($data);
		}

		public function save_guardian_profile_data()
		{
			
			$input = $this->input->post();
			$this->load->library('form_validation');
	
			$this->form_validation->set_rules("txtFormNumber","Form Number","required");
			
			if($this->form_validation->run() != false)
			{	
				//$username = $this->session->userdata('username');
				//$RegistrantID =  $this->session->userdata('RegistrantID');
				$RegistrantID =  trim($input['txtFormNumber']);
				$this->db->trans_begin();
				
				
			
				$CanInputGuardian=1;
					/*
				$CanInputMother=1;
				
				if( trim($input['txtGuardianName'])==""&&trim($input['txtGuardianDOB'])==""&& trim($input['txtGuardianPOB'])=="" &&trim($input['ddlGuardianReligion'])=="" &&
					 trim($input['txtGuardianAddress'])==""&&	trim($input['ddlGuardianCity'])=="" && trim($input['txtGuardianPostalCode']) =="" && trim($input['txtGuardianEmail'])==""
					&& trim($input['txtGuardianEducation'])=="" &&  trim($input['txtGuardianCellPhone'])=="" &&  trim($input['txtGuardianPhone'])=="" &&  trim($input['txtGuardianFax'])==""){
					$CanInputGuardian=0;
				}
				
				if( trim($input['txtMotherName'])==""&&trim($input['txtMotherDOB'])==""&& trim($input['txtMotherPOB'])=="" &&trim($input['ddlMotherReligion'])=="" &&
					 trim($input['txtMotherAddress'])==""&&	trim($input['ddlMotherCity'])=="" && trim($input['txtMotherPostalCode']) =="" && trim($input['txtMotherEmail'])==""
					&& trim($input['txtMotherEducation'])=="" &&  trim($input['txtMotherEducation'])==""){
					$CanInputMother=0;
				}*/
				
				$Row=array();
				
				if($CanInputGuardian==1){
					$Row['Guardian']=array(
							'StudentID' => $RegistrantID,  
							'ParentStatus' => 'G',
							'ParentName' =>  trim($input['txtGuardianName']),
							'DOB' =>  trim($input['txtGuardianDOB'])==""?NULL: trim($input['txtGuardianDOB']),
							'POB' =>  trim($input['txtGuardianPOB']),
							'ReligionID' => (int)trim($input['ddlGuardianReligion']),  
							'Address' => trim($input['txtGuardianAddress']),
							'CityID' => trim($input['ddlGuardianCity'])==""?NULL:(int)trim($input['ddlGuardianCity']),
							'PostalCode' =>trim($input['txtGuardianPostalCode']),
							'Email' => trim($input['txtGuardianEmail']),  
							'LifeStatusID' => (int)trim($input['ddlGuardianLifeStatus']),  
							'RIPYear' => trim($input['txtGuardianRIPYear']),  
							'Education' => trim($input['txtGuardianEducation']),
							'CellPhoneNumber'=> trim($input['txtGuardianCellPhone']),
							'PhoneNumber'=>trim($input['txtGuardianPhone']),
							'FaxNumber'=> trim($input['txtGuardianFax']),
							'AuditUserName' => $this->session->userdata('UserId')
					);
				}
				
				
				if($Row){
					foreach($Row as $ChildRow){
				
						$Array = array(
							'StudentID' => $ChildRow['StudentID'],  
							'ParentStatus' => $ChildRow['ParentStatus'],
							'ParentName' => $ChildRow['ParentName'],
							'DOB' => $ChildRow['DOB'],
							'POB' => $ChildRow['POB'],
							'ReligionID' => $ChildRow['ReligionID'],  
							'Address' => $ChildRow['Address'],
							'CityID' => $ChildRow['CityID'],
							'PostalCode' =>$ChildRow['PostalCode'],
							'Email' => $ChildRow['Email'],  
							'LifeStatusID' => $ChildRow['LifeStatusID'],  
							'RIPYear' => $ChildRow['RIPYear'],  
							'Education' =>$ChildRow['Education'],
							'AuditUserName' => $ChildRow['AuditUserName']
						);
						
						$this->parent_profile_model->insert_full_parent_data($Array);
						$p=array();
						$p['StudentID']=$ChildRow['StudentID'];
						$p['ParentStatus']=$ChildRow['ParentStatus'];
						$p['AuditUserName']=$ChildRow['AuditUserName'];
						
						$this->phone_model->delete_phone_data($p);

						$p['PhoneType']='M';
						$p['PhoneNumber']= $ChildRow['CellPhoneNumber'];
						
						if($p['PhoneNumber']!=""){
							$this->phone_model->insert_phone_data( $p);
						}

						$p['PhoneType']='R';
						$p['PhoneNumber']= $ChildRow['PhoneNumber'];
						
						if($p['PhoneNumber']!=""){
							$this->phone_model->insert_phone_data( $p);
						}

						$p['PhoneType']='F';
						$p['PhoneNumber']= $ChildRow['FaxNumber'];
						
						if($p['PhoneNumber']!=""){
							$this->phone_model->insert_phone_data( $p);
						}
					
					
					}
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['txtFormNumber']));
		}
		
		
		
		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			$data['Religion'] = $this->student_profile_model->get_religion();
			
			$data['Country'] = $this->student_profile_model->get_country();
			$data['LifeStatus'] = $this->parent_profile_model->get_life_status();
			$Param['StudentID']=$FormNumber;
			
			$Param['ParentStatus']='G';
			$data['GuardianData']=$this->parent_profile_model->get_parent_profile_data($Param);
			$param['CountryID'] = (isset($data['GuardianData']->CountryID) ? $data['GuardianData']->CountryID : 0);
			$data['City'] = $this->student_profile_model->get_city($param);
			$Param['StudentID']=$FormNumber;

			
			$Param['StudentID']=$FormNumber;
			$Param['ParentStatus']='G';
			$RegistrantGuardianPhoneDataDB= $this->phone_model->get_phone_data($Param);
			
			$StudentParam['StudentID']=$FormNumber;
			$MsStudentData=$this->student_profile_model->get_student_data($StudentParam);
			
			$EmergencyContact="";
			$StayingWithID="";
			
			if(!empty($MsStudentData)){
			$EmergencyContact=$MsStudentData->EmergencyStatus;
			$StayingWithID=$MsStudentData->StayingWithID;
			}
			//print_r($StayingWithID);die;
			$data['EmergencyContact']=!empty($EmergencyContact)?$EmergencyContact:"";
			$data['StayingWithID']=!empty($StayingWithID)?$StayingWithID:"";
				
			
			foreach($RegistrantGuardianPhoneDataDB as $row){
			
			if($row->PhoneType=='M'){
				$data['GuardianCellPhoneData']=$row->PhoneNumber;
				}
			else if($row->PhoneType=='R'){
				$data['GuardianPhoneData']=$row->PhoneNumber;	
				}
			else if($row->PhoneType=='F'){
				$data['GuardianFaxData']=$row->PhoneNumber;	
				}
			}
			
			$par['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($par);
			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
		
			$data['TabName'] = "guardian_profile";
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
			
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
			//print_r($data['StudentData']->StayingWithID);die;
         	$this->template->display('entry/guardian_profile',$data);
		}
	}
/*	End	of	file	guardian_profile.php	*/
/*	Location:		./controllers/entry/form_return/guardian_profile.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class learning_note extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/student_profile_model");
			$this->load->model("entry/personal_development_model");
			$this->load->model("entry/learning_note_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->config->load('app_config');
			$this->load->config('app_config');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        
        }
		
		
		
		public function get_learning_note_data($RegistrantID = null,$postback = false)
		{
			if($RegistrantID)
			{
				$Param['RegistrantID'] = $RegistrantID;
				$Param['ProblemID'] = 1;
				$data['LearningNoteData'] = $this->learning_note_model->get_registrant_learning_note_data($Param);
			}
			else
			{
				$data['LearningNoteData'] = '';
			}
			if($postback) return $data['LearningNoteData'];
			else echo json_encode($data);
		}
		
		public function save_learning_note_file($Param=null)
		{	
			if($Param){
				$RegistrantID =  $Param;			
				$this->config->load('app_config');
				$TempPath="";
				$final_filename = "";
				
				$allowed_ext = array('jpg', 'jpeg', 'png', 'pdf','rar','zip');
				$post = $this->input->post();
				$Data=array();
				//$file=$_FILES['file'];
				$i=0;
					foreach($_FILES as $file)
					{
						$md5name = md5($file['name']);
						$orig_name = preg_replace('/[^a-zA-Z0-9\.-_]/si', '', $file['name']);
						$final_path = $this->config->item('form_return_BO_upload_path').'StudentInformation/LearningNote/';
						$temp_path = $final_path . $md5name;
						@move_uploaded_file($file['tmp_name'], $temp_path);
						$extension = explode('.', $orig_name);
						$extension = strtolower($extension[count($extension) - 1]);
						
						if(in_array($extension, $allowed_ext))
						{
							$random = substr(md5(uniqid(microtime(), true)), 0, 6);
							$final_filename = $RegistrantID . '_' . $random . '_' . $orig_name;

							$file_size = filesize($temp_path);
							rename($temp_path, $final_path . $final_filename);
							
							$TempPath=$final_path . $final_filename;
							
							//$Data=array('TempPath'=>$TempPath,'FileName'=>$final_filename);
							$TempData=array('TempPath'=>$TempPath,'FileName'=>$final_filename);
							$Data=array_merge_recursive($Data,$TempData);
							$i++;
						}
						else
						{
							// remove the data if it's not needed anymore
							@unlink( $final_path . $md5name );
						}
							
					}
			}
			
			return $this->load->view('entry/json_view', array('json' => array(
				'status' => 'success',
				'data' => $Data,
				'Count' => $i
			)));
		
		}
		
		
		
		public function save_learning_note_data($method = '')
		{	
			$this->config->load('app_config');

			$input = $this->input->post();
				$this->load->library('form_validation');
				
				$this->form_validation->set_rules("hfFormNumber","Form Number","required");

				if($this->form_validation->run() != false)
				{	
					$RegistrantID = $input["hfFormNumber"];
					if($input){

						$this->db->trans_begin();
						$Param['RegistrantID']=$RegistrantID;
						$Param['ProblemTypeID']=1;
						$Param['AuditUserName']=$this->session->userdata('UserId');
						$this->learning_note_model->delete_registrant_problem_difficulties_data($Param);

						if(isset($input["hfAreaID"])){
							foreach($input["hfAreaID"] as $id=>$fieldvalue){
									$RegistrantID = $RegistrantID;
									$ProblemID = (int)$input["hfAreaID"][$id];
									$Description = $input["hfDescription"][$id];
									$AttachReports =(int) $input["hfAttachReports"][$id];
									$AttachReportName = $input["hfAttachReportName"][$id];
									$AuditUserName = $this->session->userdata('UserId');
									
									$Array =array(
										'RegistrantID' => $RegistrantID, 
										'ProblemID' => $ProblemID,
										'Description' => $Description, 
										'AttachReports' => $AttachReports,
										'AttachReportName' =>$AttachReportName,
										'AuditUserName' => $AuditUserName
									);
									
									$this->learning_note_model->insert_registrant_problem_difficulties_data($Array);
							}
						}

						if ( $this->db->trans_status() === FALSE  ) {
							$this->db->trans_rollback();
							$data['status'] = 'failed';
							$data['message'] = "failed save to database" ;
						} else {    
							$this->db->trans_commit();    
							$data['status'] = 'success';
							$data['message'] = "success save to database" ;

						}
						
						if(isset($input['hfDeleteSavedFile'])){
							foreach($input["hfDeleteSavedFile"] as $id=>$fieldvalue){
								
								@unlink( $this->config->item('form_return_BO_upload_path').'StudentInformation/LearningNote/' . $input["hfDeleteSavedFile"][$id] );
							}
						}
					
					}
				}
				else
				{
					$data['status'] = 'failed';
					$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
					$data['message'] = str_replace('.','<br/><br/>',$data['message']);
					
				}

				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				
					
				$this->search(trim($input['hfFormNumber']));
		}
	
	

		
		
		

		public function search($FormParam="blank")
        {	
			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank")
				$FormNumber=$FormParam;
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			
			$Param['RegistrantID']=$FormNumber;
			$Param['ProblemID']=1;
			$data['LearningNoteData']=$this->learning_note_model->get_registrant_learning_note_data($Param);
			$data['Area']=$this->learning_note_model->get_registrant_problem_difficulties_list();
			
			$par['StudentID']=$FormNumber;
			$data['StudentData']=$this->student_profile_model->get_student_data($par);
			if(sizeof($data['StudentData']) > 0)
			{
				$data['txtName'] = $data['StudentData']->FirstName.' '.$data['StudentData']->MiddleName.' '.$data['StudentData']->LastName;
				$data['txtGrade'] = $data['StudentData']->YearLevelName;
				$data['txtTerm'] = $data['StudentData']->TermID.' - '.$data['StudentData']->TermName;
				switch($data['StudentData']->StatusLulus){
					case 1:  $data['txtResultET']="Pass";
					break;
					case 2:  $data['txtResultET']="Pass With Condition";
					break;
					case 3:  $data['txtResultET']="Failed";
					break;
					default: $data['txtResultET']="No Result Yet";
					break;
				}
				//$data['txtResultET'] = $data['StudentData']->StatusLulus==null?'No Result Yet':;
			}
			else
			{
				$data['txtName'] = '';
				$data['txtGrade'] = '';
				$data['txtTerm'] = '';
				$data['txtResultET'] = '';
			}
			
			$StudentStatusParam['RegistrantID']=$FormNumber;
			$StudentStatus=$this->form_return_back_office_model->check_student_status($StudentStatusParam);
			
			$StudentValidityParam['RegistrantID']=$FormNumber;
			$data['StudentValidity']=$this->form_return_back_office_model->check_student_validity($StudentValidityParam);
			
			if(isset($StudentStatus->StudentStatusID))
			$data['StudentStatus']=$this->form_return_back_office_model->check_student_status($StudentStatusParam)->StudentStatusID;
			//print_r($data['LearningNoteData'] );die;
			$data['TabName'] = "learning_note";
			$data['Photo']=$this->form_return_back_office_model->get_registrant_photo($StudentStatusParam);
         	$this->template->display('entry/learning_note',$data);
		}
	}
	
	
/*	End	of	file	learning_note.php	*/
/*	Location:		./controllers/entry/form_return/learning_note.php */

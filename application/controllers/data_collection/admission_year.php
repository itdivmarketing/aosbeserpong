<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Admission_year extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/admission_year_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			$d = $this->get_admission_year_schedule_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->admission_year_model->get_school_level();
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('data_collection/admission_year',$data);
        }
		
		public function get_admission_year_schedule($AdmissionID = null,$postback = false)
		{
			$Param['AdmissionID'] = $AdmissionID;
			$data['AdmissionYear'] = $this->admission_year_model->get_admission_year_schedule($Param);
			if($postback) return $data;
			else echo json_encode($data);
		}
		
		public function set_admission_year_schedule()
		{
			$input = $this->input->post();
			$d = array();
			if($input['hfStatus'] != 'delete')
			{
				if($input['hfStatus'] != 'update')
				{
					$param['SchoolLevelID'] = (isset($input['ddlSchoolLevel']) ? $input['ddlSchoolLevel'] : $input['hfSchoolLevel']);
					$temp = explode("-", $this->period);
					$param['AcademicYear'] = $temp[0];
					$param['SmtId'] = $temp[1];
					$checked = $this->admission_year_model->check_exist_schedule($param);
					if($checked[0]->Message == 'Yes')
					{
						$data['message'] = 'This school level already have admission schedule.';
						echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
						goto a;
					}
				}
				$temp = explode("-", $this->period);
				$d['AcademicYear'] = $temp[0];
				$d['SmtId'] = $temp[1];
				$d['CommencingDate'] = $input['txtSchoolDate'];
				$d['EffectiveDate'] = $input['txtEffectiveDate'];
				$d['SchoolLevelID'] = (isset($input['ddlSchoolLevel']) ? $input['ddlSchoolLevel'] : $input['hfSchoolLevel']);
				$d['CurriculumLevelId'] = '';
				$d['PreTestGrading'] = isset($input['cbETGrading'])?$input['cbETGrading']:NULL;
				$d['AdmissionID'] = null_setter($input['hfAdmissionID']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '0';
			}
			else
			{
				$param['AdmissionID'] = $input['hfAdmissionID'];
				$checked = $this->admission_year_model->check_exist_year($param);
				if($checked[0]->Message == 'Yes')
				{
					$data['message'] = 'You cannot delete this schedule. One or more registrants already allocated to this schedule';
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
					goto a;
				}
				//BEGIN -- NOT USE IN DELETE PART
				$temp = explode("-", $this->period);
				$d['AcademicYear'] = $temp[0];
				$d['SmtId'] = $temp[1];
				$d['CommencingDate'] = '2014-08-25';
				$d['EffectiveDate'] = '2014-08-25';
				$d['SchoolLevelID'] = '';
				$d['CurriculumLevelId'] = '';
				$d['PreTestGrading'] = '';
				//END -- NOT USE IN DELETE PART
				
				$d['AdmissionID'] = null_setter($input['hfAdmissionID']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '1';
			}				
			
			$query = $this->admission_year_model->set_admission_year_schedule($d);
			
			$data['status'] = 'success';
			$data['message'] = "Data updated successfully" ;
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			
			a :
			$d = $this->get_admission_year_schedule_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->admission_year_model->get_school_level();
			$schoollevel = (isset($input['ddlSchoolLevel']) ? $input['ddlSchoolLevel'] : '');

			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/admission_year');

			//$this->template->display('data_collection/admission_year',$data);
		}
		
		public function get_admission_year_schedule_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			$table['f'] = array(
				'Period'		=> $this->period,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
			);
			$query = $this->admission_year_model->get_admission_year_schedule_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->admission_year_model->get_admission_year_schedule_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_admission_year_schedule_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->admission_year_model->get_school_level();
         	$this->template->display('data_collection/admission_year',$data);
		}
	}
/*	End	of	file	entrance_test_schedule.php	*/
/*	Location:		./controllers/entrance_test/entrance_test_schedule.php */

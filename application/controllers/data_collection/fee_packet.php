<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class fee_packet extends CI_Controller {
	
		var $period;
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/fee_packet_model");
			$this->load->model("entrance_test/registrant_allocation_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			//$this->output->nocache();
			
		}
		public function index()
        {
			$d = $this->get_fee_packet_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->fee_packet_model->get_school_level();
			$data['YearLevel'] = $this->fee_packet_model->get_year_level();
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('data_collection/fee_packet',$data);
        }
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->fee_packet_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->fee_packet_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		public function get_admissionID($TermID = null,$postback = false)
		{
			if($TermID)
			{
				$Param['TermID'] = $TermID;
				$data['AdmissionID']=$this->fee_packet_model->get_admissionID($Param);
			}
			else
			{
				$data['AdmissionID'] = '';
			}
			if($postback) return $data['AdmissionID'];
			else echo json_encode($data);
		}
		
		public function get_fee_packet_view($FeePacketID=null, $postback = false)
		{
			$Param['FeePacketID'] = $FeePacketID;
			
			$data['FeePacketView'] = $this->fee_packet_model->get_fee_packet_view($Param);
		
			if($postback) return $data['FeePacketView'];
			else echo json_encode($data);
		}
		
	
		
		public function save_fee_packet()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			if($input['hfStatus']!='delete'){
					$this->form_validation->set_rules("txtViewAdmissionID","Admission ID","required");
					$this->form_validation->set_rules("txtViewAdmissionYear","Admission Year","required");
					$this->form_validation->set_rules("txtViewAdmissionSemester","Semester","required");
					$this->form_validation->set_rules("ddlViewSchoolLevel","School Level","required");
					$this->form_validation->set_rules("ddlViewAdmissionTerm","Term","required");
					$this->form_validation->set_rules("ddlViewYearLevel","Year Level","required");
					
					if($this->form_validation->run() != false)
					{	
						$d = array();
						$this->db->trans_begin();
					
						$d['FeePacketID'] = trim($input['txtViewPacketID'])==""?null:trim($input['txtViewPacketID']);
						$d['PacketName'] = trim($input['txtViewPacketName']);
						$d['TermID'] = $input['ddlViewAdmissionTerm'];
						$d['YearLevelID'] = (int)$input['ddlViewYearLevel'];
						$d['AuditUserName'] = $this->session->userdata('UserId');
						

						$this->fee_packet_model->save_fee_packet($d);			
				
						if ( $this->db->trans_status() === FALSE  ) {
							$this->db->trans_rollback();
							 $data['status'] = 'failed';
							$data['message'] = "failed save to database" ;
						} else {    
							$this->db->trans_commit();    
							$data['status'] = 'success';
							$data['message'] = "success save to database" ;
						}
					}
					else
					{
						$data['status'] = 'failed';
						$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
						$data['message'] = str_replace('.','<br/><br/>',$data['message']);
					}
			}
			else{
				$this->form_validation->set_rules("hfViewPacketID","Packet ID","required");
		
				if($this->form_validation->run() != false)
				{
					$this->db->trans_begin();
					$Param['FeePacketID']=trim($input['hfViewPacketID']);
					$Param['AuditUserName']= $this->session->userdata('UserId');
					
					$this->fee_packet_model->delete_fee_packet($Param);
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						 $data['status'] = 'failed';
						$data['message'] = "failed delete the data" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success delete the data" ;
					
					}
				}
				else{
					$data['status'] = 'failed';
					$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
					$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				}
			}
			//echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			//$this->index();
			
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/fee_packet');
		}
		
		public function get_fee_packet_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'Period'		=> $this->period,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'YearLevel'	=> isset($input['ddlYearLevel']) ? null_setter($input['ddlYearLevel']) : null,
				'AdmissionTerm'	=> isset($input['ddlAdmissionTerm']) ? null_setter($input['ddlAdmissionTerm']) : null
			);
			$query = $this->fee_packet_model->get_fee_packet_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->fee_packet_model->get_fee_packet_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_fee_packet_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['SchoolLevel'] = $this->registrant_allocation_model->get_school_level();
			$data['YearLevel'] = $this->get_year_level($input['ddlSchoolLevel'],true);
         	$this->template->display('data_collection/fee_packet',$data);
		}
	}
/*	End	of	file	fee_packet.php	*/
/*	Location:		./controllers/data_collection/fee_packet.php */

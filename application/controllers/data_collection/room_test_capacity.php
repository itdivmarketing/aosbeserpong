<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class room_test_capacity extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/room_test_model");
			$this->load->model("data_collection/room_test_capacity_model");
			$this->load->model("entrance_test/registrant_allocation_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}	
		public function index()
        {
			$d = $this->get_venue_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['VenueList']=$this->room_test_capacity_model->get_venue_list();
			$data['AddVenueList']=$this->room_test_capacity_model->get_add_venue_list();
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('data_collection/room_test_capacity',$data);
        }
		
		public function get_room_test_capacity_view($VenueID = null,$postback = false)
		{
			if(isset($VenueID))
			{
				$Param['VenueID']=$VenueID;
				$data['RoomTestView'] = $this->room_test_capacity_model->get_view_venue_capacity($Param);
			}
			else
			{
				$data['RoomTestView'] = '';
			}
			if($postback) return $data['RoomTestView'];
			else echo json_encode($data);
		}
		
		
		
		
		public function save_room_test_capacity()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			if($input['hfStatus'] != 'delete')
			{
				//$this->form_validation->set_rules("txtViewVenueID","Venue ID","required");
				$this->form_validation->set_rules("hfVenueID","Venue ID","required");
				$this->form_validation->set_rules("txtViewCapacity","Capacity","required");
			
				if($this->form_validation->run() != false)
				{	
					$d = array();
					$this->db->trans_begin();
					
					$d['VenueID'] = (int)trim($input['hfVenueID']);
					$d['Capacity'] =  (int)trim($input['txtViewCapacity']);
					$d['AuditUserName'] = $this->session->userdata('UserId');
					

					$this->room_test_capacity_model->save_venue_capacity($d);
					
			
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						 $data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;
					
					}
				}
				else
				{
					$data['status'] = 'failed';
					$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
					$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				}
			}
			else{
				$this->form_validation->set_rules("hfVenueID","Venue ID","required");
				if($this->form_validation->run() != false)
				{
					$this->db->trans_begin();

					$Param['VenueID']=(int)trim($input['hfVenueID']);
					$Param['AuditUserName']= $this->session->userdata('UserId');
					$this->room_test_capacity_model->delete_venue_capacity($Param);
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						 $data['status'] = 'failed';
						$data['message'] = "failed delete the data" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success delete the data" ;
					
					}
				}
				else{
					$data['status'] = 'failed';
					$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
					$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				}
			}
			
			//echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/room_test_capacity');
			//$this->index();
		}
		
		public function get_venue_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'VenueID'		=> isset($input['ddlVenue']) ? null_setter($input['ddlVenue']) : null
			);
			$query = $this->room_test_capacity_model->get_venue_by_ID_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->room_test_capacity_model->get_venue_by_ID_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_venue_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			//$data['YearLevel'] = $this->get_year_level($input['ddlSchoolLevel'],true);
			$data['VenueList']=$this->room_test_capacity_model->get_venue_list();
			$data['AddVenueList']=$this->room_test_capacity_model->get_add_venue_list();
         	$this->template->display('data_collection/room_test_capacity',$data);
		}
	}
/*	End	of	file	room_test_capacity.php	*/
/*	Location:		./controllers/data_collection/room_test_capacity.php */

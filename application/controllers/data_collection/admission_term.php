<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Admission_term extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/admission_term_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			/*$d = $this->get_admission_term_schedule_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];*/
			$temp = explode("-", $this->period);
			$data['term'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->admission_term_model->get_school_level();
			$data['message'] = $this->session->flashdata('message');

         	$this->template->display('data_collection/admission_term',$data);
        }
		
		public function get_admission_term_schedule($TermID = null,$postback = false)
		{
			$Param['TermID'] = $TermID;
			$data['AdmissionTerm'] = $this->admission_term_model->get_admission_term_schedule($Param);
			if($postback) return $data;
			else echo json_encode($data);
		}
		
		public function set_admission_term_schedule()
		{
			$input = $this->input->post();
			$d = array();
			if($input['hfStatus'] != 'delete')
			{
				$d['TermID'] = null_setter($input['hfTermID']);
				$d['AdmissionID'] = null_setter($input['hfAdmissionID']);
				$temp = explode("-", $this->period);
				$d['AcademicYear'] = $temp[0];
				$d['TermName'] = $input['txtTermName'];
				$d['OpeningDate'] = $input['txtOpeningDate'];
				$d['ClosingDate'] = $input['txtClosingDate'];
				$d['PreTestDate'] = $input['txtEntranceTest'];
				$d['TermOrder'] = $input['txtTermOrder'];
				$d['AnnouncementDate'] = $input['txtAnnouncementDate'];
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '0';
			}
			else
			{
				$param['TermID'] = $input['hfTermID'];
				$checked = $this->admission_term_model->check_exist_term($param);
				if($checked[0]->Message == 'Yes')
				{
					$data['message'] = 'You cannot delete this term. One or more registrants already allocated to this term';
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
					goto a;
				}
				//BEGIN -- NOT USE IN DELETE PART
				$d['AdmissionID'] = '';
				$d['AcademicYear'] = '';
				$d['TermName'] = '';
				$d['OpeningDate'] = '';
				$d['ClosingDate'] = '';
				$d['PreTestDate'] = '';
				$d['AnnouncementDate'] = '';
				//END -- NOT USE IN DELETE PART
				
				$d['TermID'] = null_setter($input['hfTermID']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '1';
			}
			$query = $this->admission_term_model->set_admission_term_schedule($d);
			$data['status'] = 'success';
			$data['message'] = "Data updated successfully" ;
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			
			a :
			/*$d = $this->get_admission_term_schedule_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];*/
			$temp = explode("-", $this->period);
			// $data['term'] = $temp[0];
			// $data['semester'] = $temp[1];
			// $data['SchoolLevel'] = $this->admission_term_model->get_school_level();
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/admission_term');
			//$schoollevel = (isset($input['ddlSchoolLevel']) ? $input['ddlSchoolLevel'] : '');
			//$this->template->display('data_collection/admission_term',$data);

		}
		
		public function get_admission_term_schedule_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			$table['f'] = array(
				'Period'		=> $this->period,
				'AdmissionID'		=> $input['AdmissionID'],
				'SchoolLevel'	=> $input['SchoolLevel'],
			);
			$query = $this->admission_term_model->get_admission_term_schedule_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->admission_term_model->get_admission_term_schedule_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$p['Period'] = $this->period;
			$p['SchoolLevel'] = $input['ddlSchoolLevel'];
			$general = $this->admission_term_model->get_admission_id($p);
			if(sizeof($general) > 0)
			{
				$data['admissionID'] = $general[0]->AdmissionID;
				$data['schoolDate'] = $general[0]->CommencingDate;
				$data['effectiveDate'] = $general[0]->EffectiveDate;
				
				$param['Period'] = $this->period;
				$param['AdmissionID'] = $data['admissionID'];
				$param['SchoolLevel'] = $input['ddlSchoolLevel'];
				$param['hfPage'] = $input['hfPage'];
				
				$d = $this->get_admission_term_schedule_paging($param);
				$data['data']['result'] = $d['data']['result'];
				$data['data']['pagination'] = $d['data']['pagination'];
			}
			else
			{
				$data['message'] = 'There\'s no Data';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			
			$temp = explode("-", $this->period);
			$data['term'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->admission_term_model->get_school_level();
         	$this->template->display('data_collection/admission_term',$data);
		}
	}
/*	End	of	file	entrance_test_schedule.php	*/
/*	Location:		./controllers/entrance_test/entrance_test_schedule.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class List_document extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/list_document_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			$d = $this->get_list_document_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->list_document_model->get_school_level();
			$data['Process'] = $this->list_document_model->get_process();
			$data['Document'] = $this->list_document_model->get_document();
			//Session
			$data['schoollevel'] =  $this->session->flashdata('schoollevel');
			$data['process'] = $this->session->flashdata('process');
			$data['message'] = $this->session->flashdata('message');
         	
         	$this->template->display('data_collection/list_document',$data);
        }
		
		/*public function get_list_document($AdmissionID = null,$postback = false)
		{
			$Param['AdmissionID'] = $AdmissionID;
			$data['Document'] = $this->list_document_model->get_list_document($Param);
			if($postback) return $data;
			else echo json_encode($data);
		}*/
		
		public function set_list_document()
		{
			$input = $this->input->post();
			$d = array();
			if($input['hfStatus'] != 'delete')
			{
				$d['AdmissionId'] = $input['hfAdmissionID'];
				$d['AdmissionProcessID'] = $input['ddlProcess'];
				$d['DocumentID'] = $input['ddlDocument'];
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '0';
			}
			else
			{
				/*$param['AdmissionID'] = $input['hfAdmissionID'];
				$checked = $this->admission_year_model->check_exist_term($param);
				if($checked[0]->Message == 'Yes')
				{
					$data['message'] = 'You cannot delete this schedule. One or more registrants already allocated to this schedule';
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
					goto a;
				}*/
				//BEGIN -- NOT USE IN DELETE PART
				//END -- NOT USE IN DELETE PART
				
				$d['AdmissionId'] = null_setter($input['hfAdmissionID']);
				$d['AdmissionProcessID'] = null_setter($input['hfProcessID']);
				$d['DocumentID'] = null_setter($input['hfDocumentID']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '1';
			}				
			
			$query = $this->list_document_model->set_list_document($d);
			
			$data['status'] = 'success';
			$data['message'] = "Data updated successfully" ;
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			
			//a :
			$d = $this->get_list_document_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->list_document_model->get_school_level();
			$data['Process'] = $this->list_document_model->get_process();
			$data['Document'] = $this->list_document_model->get_document();
			$data['schoollevel'] = (isset($input['ddlSchoolLevel']) ? $input['ddlSchoolLevel'] : '');
			$data['process'] = (isset($input['ddlProcess']) ? $input['ddlProcess'] : '');

			$this->session->set_flashdata('message', $data['message']);
			$this->session->set_flashdata('schoollevel', $data['schoollevel']);
			$this->session->set_flashdata('process', $data['process']);

			redirect(site_url().'/data_collection/list_document');
			//$this->template->display('data_collection/list_document',$data);
		}
		
		public function get_admission_id($SchoolLevel = null,$postback = false)
		{
			$Param['SchoolLevelID'] = $SchoolLevel;
			$Param['Period'] = $this->period;
			$data['AdmissionID'] = $this->list_document_model->get_admission_id($Param);
			if($postback) return $data;
			else echo json_encode($data);
		}
		
		public function get_list_document_paging($input = null)
        {
			$periods = $this->period;
			/*
			 *	1 : last year; 
			 *	0 : current year;
			 */
			 
			/*if($input['hfLastYear'] == '1') 
			{
				$temp = explode("-", $this->period);
				$temp1 = $temp[0]-1;
				$periods = $temp1.'-'.$temp[1];
			}	*/
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			$table['f'] = array(
				'Period'		=>  $periods,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'Process'	=> isset($input['ddlProcess']) ? null_setter($input['ddlProcess']) : null,
			);
			$query = $this->list_document_model->get_list_document_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->list_document_model->get_list_document_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			if($input['hfLastYear'] == 1)
			{
				$this->save_from_last_year($input);
			}
			$d = $this->get_list_document_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->list_document_model->get_school_level();
			$data['Process'] = $this->list_document_model->get_process();
			$data['Document'] = $this->list_document_model->get_document();
         	$this->template->display('data_collection/list_document',$data);
		}
		
		public function save_from_last_year($input)
		{
			$d['SchoolLevel'] = $input['ddlSchoolLevel'];
			$d['Period'] = $this->period;
			$d['AdmissionProcessID'] = $input['ddlProcess'];
			$d['AuditUserName'] = $this->session->userdata('UserId');
			
			$query = $this->list_document_model->save_from_last_year($d);
			
			if(isset($query[0]->errorMessage))
			{
				$data['status'] = 'failed';
				$data['message'] = $query[0]->errorMessage ;
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			else
			{
				$data['status'] = 'success';
				$data['message'] = "Data updated successfully" ;
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
		}
	}
/*	End	of	file	entrance_test_schedule.php	*/
/*	Location:		./controllers/entrance_test/entrance_test_schedule.php */

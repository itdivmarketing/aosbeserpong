<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class school_regulation extends CI_Controller {

		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("data_collection/school_regulation_model"); //reg
			$this->load->library('form_validation');
			$this->load->config('app_config');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}

		public function index(){
			$data['SchoolRegulation'] = $this->school_regulation_model->get_school_regulation();
			$this->template->display('data_collection/school_regulation',$data);
			
		}

		public function save_school_regulation(){
			$Param['AuditUsername'] = $this->session->userData('UserId');
			$Param['SchoolRegulationText'] = $this->input->post('editor');
			$this->school_regulation_model->insert_school_regulation($Param);
			$this->index();
			alert("Succes save to database");

		}

		public function cancel_school_regulation(){
			$this->index();

		}


	}

?>
<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class sales_target_form extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/sales_target_form_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$d = $this->get_sales_target_form();
			$data['data']['result'] = $d['data']['result'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('data_collection/sales_target_form',$data);
        }
		
		
		

	

	
	
		public function save_sales_target_form()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
		

			$this->form_validation->set_rules("hfSchoolLevelID","School Level ID","required");
			$this->form_validation->set_rules("hfYearLevelID","Year Level","required");
			$this->form_validation->set_rules("txtFormSold","Form Sold","required");
			$this->form_validation->set_rules("txtIntake","Intake","required");

			if($this->form_validation->run() != false)
			{	
				$d = array();
				$this->db->trans_begin();
				$temp = explode("-", $this->period);
				$year = $temp[0];
				
				
				$AdmissionYear = $year;
				$SchoolLevelID = '-';
				$YearLevelID = 0;
				$TargetFormSold = trim($input["txtTotalFormSold"]);
				$TargetIntake = trim($input["txtTotalIntake"]);
				
				$query =array(
					'AdmissionYear' => $AdmissionYear,  
					'SchoolLevelID' => $SchoolLevelID,
					'YearLevelID' => $YearLevelID,
					'TargetFormSold' => (int)$TargetFormSold,
					'TargetIntake' =>  (int)$TargetIntake,
					'IsTotal' => '1',
					'AuditUserName' => $this->session->userdata('UserId')

				);

				
				$this->sales_target_form_model->save_sales_target_form($query);
				
				
				
				
				foreach($input["hfSchoolLevelID"] as $id=>$fieldvalue){
						$AdmissionYear = $year;
						$SchoolLevelID = trim($input["hfSchoolLevelID"][$id]);
						$YearLevelID = trim($input["hfYearLevelID"][$id]);
						$TargetFormSold = trim($input["txtFormSold"][$id]);
						$TargetIntake = trim($input["txtIntake"][$id]);

						$query =array(
							'AdmissionYear' => $AdmissionYear,  
							'SchoolLevelID' => $SchoolLevelID,
							'YearLevelID' => $YearLevelID,
							'TargetFormSold' => (int)$TargetFormSold,
							'TargetIntake' =>  (int)$TargetIntake,
							'IsTotal' => '0',
							'AuditUserName' => $this->session->userdata('UserId')

						);

						
						$this->sales_target_form_model->save_sales_target_form($query);
				}

				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
					 $data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
					
				} else {    
					$this->db->trans_commit();    
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}
		

			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/sales_target_form');
		}
		
		public function get_sales_target_form($input = null)
        {
			$Param['Period'] = $this->period;
			$data['data']['result']=$this->sales_target_form_model->get_sales_target_form($Param);
			return $data;
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_sales_target_form($input);
			$data['data']['result'] = $d['data']['result'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
         	$this->template->display('data_collection/sales_target_form',$data);
		}
	}
/*	End	of	file	sales_target_form.php	*/
/*	Location:		./controllers/data_collection/sales_target_form.php */

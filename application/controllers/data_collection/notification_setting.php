<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class notification_setting extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/notification_setting_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'resources/ckeditor/';
			$this->ckeditor->config['toolbar'] = 'Full';
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '100%';
			$this->ckeditor->config['height'] = '300px'; 
			
		}
		public function index()
        {
			$d = $this->get_notification_setting_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$data['SchoolLevel'] = $this->notification_setting_model->get_school_level();
			$data['NotificationType'] = $this->notification_setting_model->get_notification_type_list();
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
         	$this->template->display('data_collection/notification_setting',$data);
        }
		
		public function get_notification_setting_view($NotificationSettingID = null,$postback = false)
		{
			if(isset($NotificationSettingID))
			{
				$Param['NotificationSettingID']=$NotificationSettingID;
				$data['NotificationSettingView'] = $this->notification_setting_model->get_notification_setting_view($Param);
			}
			else
			{
				$data['NotificationSettingView'] = '';
			}
			if($postback) return $data['NotificationSettingView'];
			else echo json_encode($data);
		}
		
		
		public function view($NotificationSettingID = null,$postback = false)
		{
			$data['NotificationSettingID'] = $NotificationSettingID ;
			$data['SchoolLevel'] = $this->notification_setting_model->get_school_level();
			$data['NotificationType'] = $this->notification_setting_model->get_notification_type_list();
			if(isset($NotificationSettingID))
			{
				$Param['NotificationSettingID']=$NotificationSettingID;
				$data['NotifData'] = $this->notification_setting_model->get_notification_setting_view($Param);
			}
			else
			{
				$data['NotifData'] = '';
			}
			$this->template->display('data_collection/notification_setting_view',$data);
		}
		
		
		public function save_notification_setting()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			//print_r($input['ckeditor']);
			//die;
			if($input['hfStatus'] != 'delete')
			{
				$this->form_validation->set_rules("ddlViewSchoolLevel","School Level","required");
				$this->form_validation->set_rules("ddlViewNotificationType","Notification Type","required");
				
				//$this->form_validation->set_rules("txtViewnotification_settingID","notification_setting ID","required");
				//$this->form_validation->set_rules("txtViewnotification_settingName","notification_setting Name","required");
			
				if($this->form_validation->run() != false)
				{
					$d = array();
					$this->db->trans_begin();
					
					$d['NotificationSettingID'] = trim($input['txtViewNotificationSettingID'])==""?null:trim($input['txtViewNotificationSettingID']);
					$d['NotificationTypeID'] = trim($input['ddlViewNotificationType']);
					$d['SchoolLevelID'] = trim($input['ddlViewSchoolLevel']);
					$d['EmailTo'] = trim($input['txtViewEmailTo']);
					$d['EmailCC'] = trim($input['txtViewEmailCC']);
					$d['EmailBCC'] = trim($input['txtViewEmailBCC']);
					$d['EmailSubject'] = trim($input['txtViewEmailSubject']);
					$d['EmailBody'] = trim($input['txtViewEmailBody']);
					$d['AuditUserName'] = $this->session->userdata('UserId');
	
					

					$this->notification_setting_model->save_notification_setting($d);
					
			
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						 $data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;
					
					}
				}
				else
				{
					$data['status'] = 'failed';
					$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
					$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				}
			}
			else{
				$this->form_validation->set_rules("hfNotificationSettingID","notification_setting ID","required");
				if($this->form_validation->run() != false)
				{
					$this->db->trans_begin();

					$Param['NotificationSettingID']=trim($input['hfNotificationSettingID']);
					$Param['AuditUserName']= $this->session->userdata('UserId');
					
					
					$this->notification_setting_model->delete_notification_setting($Param);
					
					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						 $data['status'] = 'failed';
						$data['message'] = "failed delete the data" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success delete the data" ;
					
					}
				}
				else{
					$data['status'] = 'failed';
					$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
					$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				}
			}
			
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				
			$this->index();
		}
		
		public function get_notification_setting_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'NotificationTypeID'  => isset($input['ddlNotificationType']) ? null_setter($input['ddlNotificationType']) : null,
				'SchoolLevelID'  => isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'EmailTo'  => isset($input['txtEmailTo']) ? null_setter($input['txtEmailTo']) : null
			);
			$query = $this->notification_setting_model->get_notification_setting_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->notification_setting_model->get_notification_setting_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_notification_setting_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$data['SchoolLevel'] = $this->notification_setting_model->get_school_level();
			$data['NotificationType'] = $this->notification_setting_model->get_notification_type_list();
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			//$data['YearLevel'] = $this->get_year_level($input['ddlSchoolLevel'],true);
         	$this->template->display('data_collection/notification_setting',$data);
		}
	}
/*	End	of	file	notification_setting.php	*/
/*	Location:		./controllers/data_collection/notification_setting.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_allocation extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/form_allocation_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$d = $this->get_form_allocation_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_allocation_model->get_school_level();
			$data['Location'] = $this->form_allocation_model->get_location();
			$data['TabName'] = "form_allocation";
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('data_collection/form_allocation',$data);
        }
		
		
		
		
		public function get_admissionID($SchoolLevelID=null,$postback = false)
		{
			if($SchoolLevelID!==null)
			{
				$Param['Period'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevelID;
				$data['AdmissionID']=$this->form_allocation_model->get_admissionID_by_school_level($Param);
			}
			else
			{
				$data['AdmissionID'] = '';
			}
			if($postback) return $data['AdmissionID'];
			else echo json_encode($data);
		}
		
		public function get_form_no($AdmissionID=null,$postback = false)
		{
			if($AdmissionID!==null)
			{
				$Param['AdmissionID'] = $AdmissionID;
				$data['FormNo']=$this->form_allocation_model->get_form_no($Param);
			}
			else
			{
				$data['FormNo'] = '';
			}
			if($postback) return $data['FormNo'];
			else echo json_encode($data);
		}
	
		public function get_form_allocation_view($ProductionID=null,$postback = false)
		{
			$Param['ProductionID'] = $ProductionID;	
	
			
			$data['FormGenerateView'] = $this->form_allocation_model->get_form_allocation_view($Param);
			//echo $data['FormSoldView'][0];
			if($postback) return $data['FormGenerateView'];
			else echo json_encode($data);
		}
	

	
	
		public function save_form_allocation()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
		

			$this->form_validation->set_rules("txtViewAdmissionID","Admission ID","required");
			$this->form_validation->set_rules("txtViewAdmissionYear","Admission Year","required");
			$this->form_validation->set_rules("txtViewAdmissionSemester","Semester","required");
			$this->form_validation->set_rules("ddlViewSchoolLevel","School Level","required");
			$this->form_validation->set_rules("ddlViewFrom","From Range","required");
			$this->form_validation->set_rules("ddlViewTo","To Range","required");
			$this->form_validation->set_rules("ddlViewLocation","Location","required");
			if($this->form_validation->run() != false)
			{	
				$d = array();
				$this->db->trans_begin();
				
				$d['AdmissionID'] = $input['txtViewAdmissionID'];
				$d['FormFrom'] = $input['ddlViewFrom'];
				$d['FormTo'] = $input['ddlViewTo'];
				$d['LocationID'] = trim($input['ddlViewLocation']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				
				
				$this->form_allocation_model->save_form_allocation($d);

				
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
					 $data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
					
				} else {    
					$this->db->trans_commit();    
					$data['status'] = 'success';
					$data['message'] = "success save to database" ;
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}
		
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/form_allocation');
			
			// echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			// $this->index();
		}
		
		public function get_form_allocation_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'Period'		=> $this->period,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null
			);
			$query = $this->form_allocation_model->get_form_allocation_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->form_allocation_model->get_form_allocation_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_form_allocation_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];

			$data['SchoolLevel'] = $this->form_allocation_model->get_school_level();
			$data['Location'] = $this->form_allocation_model->get_location();
			$data['TabName'] = "form_allocation";
         	$this->template->display('data_collection/form_allocation',$data);
		}
	}
/*	End	of	file	form_allocation.php	*/
/*	Location:		./controllers/data_collection/form_allocation.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Master_document extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("data_collection/master_document_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			$d = $this->get_master_document_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('data_collection/master_document',$data);
        }
		
		public function get_master_document($MasterDocumentID = null,$postback = false)
		{
			$Param['DocumentID'] = $MasterDocumentID;
			$data['Document'] = $this->master_document_model->get_master_document($Param);
			if($postback) return $data;
			else echo json_encode($data);
		}
		
		public function set_master_document()
		{
			$input = $this->input->post();
			$d = array();
			if($input['hfStatus'] != 'delete')
			{
				$d['DocumentID'] = null_setter($input['hfDocumentID']);
				$d['DocumentName'] = $input['txtDocumentName'];
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '0';
			}
			else
			{
				$param['DocumentId'] = $input['hfDocumentID'];
				$checked = $this->master_document_model->check_exist_document($param);
				if($checked[0]->Message == 'Yes')
				{
					$data['message'] = 'You cannot delete this document. One or more school level already use to this document';
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
					goto a;
				}
				//BEGIN -- NOT USE IN DELETE PART
				$d['DocumentName'] = '';
				//END -- NOT USE IN DELETE PART
				
				$d['DocumentID'] = null_setter($input['hfDocumentID']);
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$d['Delete'] = '1';
			}				
			
			$query = $this->master_document_model->set_master_document($d);
			
			//tambain pesan kalau sudah ada doc name yg sama
			
			
				$data['status'] = 'success';
				$data['message'] = "Data updated successfully" ;
		
			
			//echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			
			a :
			// $d = $this->get_master_document_paging();
			// $data['data']['result'] = $d['data']['result'];
			// $data['data']['pagination'] = $d['data']['pagination'];
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/data_collection/master_document');
			//$this->template->display('data_collection/master_document',$data);
		}
		
		public function get_master_document_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			$table['f'] = array(
				'DocumentName'	=> isset($input['txtDocumentName']) ? null_setter($input['txtDocumentName']) : null,
			);
			$query = $this->master_document_model->get_master_document_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->master_document_model->get_master_document_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			$d = $this->get_master_document_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
         	$this->template->display('data_collection/master_document',$data);
		}
	}
/*	End	of	file	entrance_test_schedule.php	*/
/*	Location:		./controllers/entrance_test/entrance_test_schedule.php */

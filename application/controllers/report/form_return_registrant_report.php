<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_return_registrant_report extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/form_return_registrant_report_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_return_registrant_report_model->get_school_level();
         	$this->template->display('report/form_return_registrant_report',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_return_registrant_report_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_return_registrant_report_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period); 
			$P['AdmissionID'] =  '';
			$P['AcademicYear'] = $temp[0];
			$P['SchoolLevelID'] = ($input['ddlSchoolLevel'] == 'all' ? '' : $input['ddlSchoolLevel']);
			$P['TermId'] = ($input['ddlAdmissionTerm'] == 'all' ? '' : $input['ddlAdmissionTerm']);
			$P['YearLevelId'] = ($input['ddlYearLevel'] == 'all' ? 'all' : $input['ddlYearLevel']);
			if($input['rbReportType'] == 'region')
				$P['tipe'] = 'REGION';
			else if ($input['rbReportType'] == 'school')
				$P['tipe'] = 'PREVIOUS SCHOOL';
			else if ($input['rbReportType'] == 'parent')
				$P['tipe'] = 'PARENT OCCUPATION '.$input['ddlParent'];
			$data['report'] =  $this->form_return_registrant_report_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->form_return_registrant_report_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['AcademicSemester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->form_return_registrant_report_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->form_return_registrant_report_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			if($input['rbReportType'] == 'region')
				$this->load->view('report/print_registrant_report_region',$data);
			else if ($input['rbReportType'] == 'school')
				$this->load->view('report/print_registrant_report_school',$data);
			else
				$this->load->view('report/print_registrant_report_parent',$data);
			
		}
	}
/*	End	of	file	form_return_registrant_report.php	*/
/*	Location:		./controllers/report/form_return_registrant_report.php */

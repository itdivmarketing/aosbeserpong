<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class manager_metrics_report extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/manager_metrics_report_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->manager_metrics_report_model->get_school_level();
			$Param['AdmissionYear']=$temp[0];
			$data['AdmissionTerm'] = $this->manager_metrics_report_model->get_admission_term($Param);
         	$this->template->display('report/manager_metrics_report',$data);
        }
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$d['Style'] = '106';
			$data['currentDate'] =  $this->manager_metrics_report_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['Semester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->manager_metrics_report_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->manager_metrics_report_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			switch ($input['rbReportType'])
			{
				case 'report_achievement' :
					$Param['AcademicYear'] = $temp[0];
					$Param['SchoolLevelID'] = '';
					$data['report'] =  $this->manager_metrics_report_model->generate_report_achievement($Param);
					if(sizeof($data['report']) <= 0)
					{
						$data['report'] = null;
					}
					$this->load->view('report/print_metrics_report_achievement',$data);
					break;
				case 'form_sold' :
					$Param['AcademicYear'] = $temp[0];
					$Param['SchoolLevelID'] = ($input['ddlSchoolLevel'] == 'all' ? '' : $input['ddlSchoolLevel']);
					$data['report'] =  $this->manager_metrics_report_model->generate_report_form_sold($Param);
					if(sizeof($data['report']) <= 0)
					{
						$data['report'] = null;
					}
					$this->load->view('report/print_metrics_report_form_sold',$data);
					break;
					
				case 'metrics_statistic' :
					$Param['AcademicYear'] = $temp[0];
					$Param['Termnumber']=($input['ddlAdmissionTerm'] == 'all' ? 0 : (int)$input['ddlAdmissionTerm']);
					$data['report'] =  $this->manager_metrics_report_model->generate_report_metrics($Param);
					if(sizeof($data['report']) <= 0)
					{
						$data['report'] = null;
					}
					$this->load->view('report/print_metrics_report_metrics',$data);
					break;
				default :
					break;
			}
		}
	}
/*	End	of	file	manager_metrics_report.php	*/
/*	Location:		./controllers/report/manager_metrics_report.php */

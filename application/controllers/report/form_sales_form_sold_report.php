<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_sales_form_sold_report extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/form_sales_form_sold_report_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_sales_form_sold_report_model->get_school_level();
         	$this->template->display('report/form_sales_form_sold_report',$data);
        }
		function convert_to_rupiah($angka)
		{
			return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
		}
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_sales_form_sold_report_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AdmissionID'] = '';
			$P['AcademicYear'] =  $temp[0];
			$P['SmtId'] = $temp[1];
			$P['SchoolLevelID'] = ($input['ddlSchoolLevel'] == 'all' ? '' : $input['ddlSchoolLevel']);
			$P['TermId'] = ($input['ddlAdmissionTerm'] == 'all' ? '' : $input['ddlAdmissionTerm']);
			$P['TglMl'] = $input['txtStartPeriod'];
			$P['TglSl'] = $input['txtEndPeriod'];
			
			
			$data['report'] =  $this->form_sales_form_sold_report_model->generate_report($P);
			
			if (sizeof($data['report']) <= 0)
				$data['report'] =null;
			$data['AcademicYear'] =  $temp[0];
			$d['Style'] = '106';
			$data['currentDate'] =  $this->form_sales_form_sold_report_model->get_current_date($d);
			$d['Style'] = '106';
			$data['printedDate'] =  $this->form_sales_form_sold_report_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->form_sales_form_sold_report_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			if(sizeof($data['report']) <= 0)
			{
				$data['message'] = 'Data not found';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			
			
			$this->load->view('report/print_form_sales_form_sold_report',$data);
		}
		
		function print_report_pdf()
		{
			$Content = $this->input->post('hfContent');
			$this->load->library('Pdf');

			/** Starting this part, some part of the code and comment is based on TCPDF example **/
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Binus International School');
			$pdf->SetTitle('Form Sold Report');
			$pdf->SetSubject('FormSoldReport');
			$pdf->SetKeywords('FormSoldReport');

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//set margins
			$pdf->SetHeaderMargin(0);
			$pdf->SetFooterMargin(0);

			// remove default footer
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);

			//set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			//set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			$pdf->setCellPaddings(1, 1, 1, 1);
			$pdf->setCellMargins(0, 0, 0, 0);
			
			// disable auto-page-break
			//$pdf->SetAutoPageBreak(false, 0);
			
			// add a page
			$pdf->SetMargins(10, 20, 20);
			$pdf->AddPage('P', 'A4');

			// set font
			$pdf->SetFont('helvetica', '', 9);
			$pdf->WriteHTML($Content, true, false, true, false, '');
			// Close and output PDF document
			$pdf->Output('form_sales_form_sold_report.pdf','I');
			return true;
		}
	}
/*	End	of	file	form_sales_form_sold_report.php	*/
/*	Location:		./controllers/report/form_sales_form_sold_report.php */

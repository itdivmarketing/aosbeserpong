<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class others_report_sms_blast extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			//$this->load->model("report/manager_student_information_report_model");
			$this->load->model("report/others_report_sms_blast_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
        	//echo json_encode($this->session->userData('UserId'));die;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->others_report_sms_blast_model->get_school_level();
			$param['reportType'] = '1';
		 	$data['Field'] = $this->others_report_sms_blast_model->generate_report($param);
		 	//echo json_encode($data['Field']);die;
  			// $this->template->display('report/manager_student_information_report',$data);
  			$this->template->display('report/others_report_sms_blast',$data);


        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			// if($SchoolLevel)
			// {
			// 	$Param['PeriodSemesterSetting'] = $this->period;
			// 	$Param['SchoolLevelID'] = $SchoolLevel;
			// 	$data['PretestTerm'] = $this->manager_student_information_report_model->get_pretest_term($Param);
			// }
			// else
			// {
			// 	$data['PretestTerm'] = '';
			// }
			// if($postback) return $data['PretestTerm'];
			// else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->others_report_sms_blast_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AdmissionYear'] = $temp[0];

			$flag = $input['cbYearLevel'];
			$P['YearLevelID'] = '';

			$flagcontact = $input['cbContact'];
			$P['contact'] = '';
			foreach($flag as $key=>$value)
			{
				$P['YearLevelID'] .= $value . ',';
			}

			foreach($flagcontact as $key=>$value)
			{
				$P['contact'] .= $value . ',';
			}
			$P['reportType'] = '2';
			$data['report'] =  $this->others_report_sms_blast_model->generate_report($P);
			$report = $data['report'];
			$Param['reportType'] = '1';
			$data['field'] =  $this->others_report_sms_blast_model->generate_report($Param);
			
			//looping for field name
			$fieldText = array();
			$cbFieldText = array();
			$cbField = $input['cbField'];
			$field = $data['field'];

			foreach ($field as $key => $value) 
			{
				foreach ($cbField as $row) 
				{
					if(trim($row) === trim(str_replace(" ", "", $value->Column_Name)))
					{
						$fieldText[$value->Column_Name] = "1";
					}		
				}
			}
			
			$data['field_filter'] = $fieldText;
			$this->load->view('report/print_others_report_sms_blast',$data);

			// foreach ($field as $key => $value) 
			// {
			// 	foreach ($cbField as $row) 
			// 	{
			// 		if(trim($row) === trim(str_replace(" ", "", $value->Column_Name)))
			// 		{
			// 			$fieldText[$value->Column_Name] = "1";
			// 		}		
			// 	}
			// }
			
			// //looping for content
			// $result = array();
			// foreach ($report as $key => $value)
			// {
			// 	$result2 = array();
			// 	foreach ($value as $key2 => $value2) 
			// 	{
			// 		foreach ($cbField as $row) 
			// 		{
			// 			if(trim($row) === str_replace(" ", "", $key2))
			// 			{
			// 				$result2[] = $value2;
			// 			}
			// 		}
			// 	}
			// 	$result[$key] = $result2;
			// }
			// $report=$result;

			// echo json_encode($report);die;
 
			//export to csv
			// header("Content-type: text/csv; charset=utf-8");
			// header("Content-Disposition: attachment; filename=report_for_sms_blast.csv");
			// header("Pragma: no-cache");
			// header("Expires: 0");

			// $outstream = fopen("php://output", "w");    
		 //    fputcsv($outstream, array_keys($fieldText));

		 //    foreach($report as $result)
		 //    {
		 //        fputcsv($outstream, $result);
		 //    }

		 //    fclose($outstream);
		 //    exit();

			
		}
	}
/*	End	of	file	others_report_sms_blast.php	*/
/*	Location:		./controllers/report/others_report_sms_blast.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class entrance_test_absent_list extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/entrance_test_absent_list_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->entrance_test_absent_list_model->get_school_level();
         	$this->template->display('report/entrance_test_absent_list',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->entrance_test_absent_list_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_schedule($SchoolLevel = null, $PretestTerm = null,$postback = false)
		{
			if($SchoolLevel)
			{
				if($PretestTerm)
				{
					$Param['Period'] = $this->period;
					$Param['SchoolLevel'] = $SchoolLevel;
					$Param['AdmissionTerm'] = $PretestTerm;
					$data['Schedule'] = $this->entrance_test_absent_list_model->get_schedule($Param);
				}
				else
				{
					$data['Schedule'] = '';
				}
			}
			else
			{
				$data['Schedule'] = '';
			}
			if($postback) return $data['Schedule'];
			else echo json_encode($data);
		}
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			
			$P['TermId'] = $input['ddlAdmissionTerm'];
			$P['KdMsJadwalET'] =  $input['ddlSchedule'];
			
			if($input['rbType'] == 'pretest')
				$P['Type'] =  '1'; //pretest
			else
				$P['Type'] =  '2'; //interview
			
			$data['report'] =  $this->entrance_test_absent_list_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
				$data['report'] = null;

			$temp = explode("-", $this->period);
			$data['AcademicYear'] =  $temp[0];
			$d['Style'] = '106';
			$data['currentDate'] =  $this->entrance_test_absent_list_model->get_current_date($d);
			$d['Style'] = '106';
			$data['printedDate'] =  $this->entrance_test_absent_list_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->entrance_test_absent_list_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			if(sizeof($data['report']) <= 0)
			{
				$data['message'] = 'Data not found';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			
			
			if($input['rbType'] == 'pretest')
				$this->load->view('report/print_absent_list',$data);
			else
				$this->load->view('report/print_absent_list_interview',$data);
				
		}
	}
/*	End	of	file	entrance_test_absent_list.php	*/
/*	Location:		./controllers/report/entrance_test_absent_list.php */

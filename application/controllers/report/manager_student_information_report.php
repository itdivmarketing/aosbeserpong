<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class manager_student_information_report extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/manager_student_information_report_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->manager_student_information_report_model->get_school_level();
			$param['reportType'] = '1';
			$data['Field'] = $this->manager_student_information_report_model->generate_report($param);
         	$this->template->display('report/manager_student_information_report',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->manager_student_information_report_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->manager_student_information_report_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AdmissionYear'] = $temp[0];
			$P['TermID'] = ($input['ddlAdmissionTerm'] == 'all' ? '' : $input['ddlAdmissionTerm']);
			$flag = $input['cbYearLevel'];
			$P['YearLevelID'] = '';
			
			foreach($flag as $key=>$value)
			{
				$P['YearLevelID'] .= $value . ',';
			}
			$P['reportType'] = '2';  
			$data['report'] =  $this->manager_student_information_report_model->generate_report($P);
			$Param['reportType'] = '1';  
			$data['field'] =  $this->manager_student_information_report_model->generate_report($Param);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->manager_student_information_report_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['Semester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->manager_student_information_report_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->manager_student_information_report_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			$this->load->view('report/print_student_information_report',$data);
		}
	}
/*	End	of	file	manager_student_information_report_model.php	*/
/*	Location:		./controllers/report/manager_student_information_report_model.php */

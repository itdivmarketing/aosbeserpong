<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_return_validate_form extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/form_return_validate_form_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_return_validate_form_model->get_school_level();
         	$this->template->display('report/form_return_validate_form',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_return_validate_form_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_return_validate_form_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AdmissionID'] =  '';
			$P['AcademicYear'] = $temp[0];
			$P['SchoolLevelID'] = $input['ddlSchoolLevel'];
			$P['TermId'] = ($input['ddlAdmissionTerm'] == 'all' ? '' : $input['ddlAdmissionTerm']);
			$P['YearLevelId'] = ($input['ddlYearLevel'] == 'all' ? '0' : $input['ddlYearLevel']);
			$data['report'] =  $this->form_return_validate_form_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->form_return_validate_form_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->form_return_validate_form_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->form_return_validate_form_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			$this->load->view('report/print_validate_form',$data);
		}
		
		function print_report_pdf()
		{
			$Content = $this->input->post('hfContent');
			$this->load->library('Pdf');

			/** Starting this part, some part of the code and comment is based on TCPDF example **/
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Binus International School');
			$pdf->SetTitle('Surat Konfirmasi');
			$pdf->SetSubject('Konfirmasi');
			$pdf->SetKeywords('Konfirmasi');

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//set margins
			$pdf->SetHeaderMargin(0);
			$pdf->SetFooterMargin(0);

			// remove default footer
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);

			//set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			//set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			$pdf->setCellPaddings(0, 0, 0, 0);
			$pdf->setCellMargins(0, 0, 0, 0);
			
			// disable auto-page-break
			//$pdf->SetAutoPageBreak(false, 0);
			
			// add a page
			$pdf->SetMargins(10, 0, 10);
			$pdf->AddPage('P', 'A4');

			// set font
			$pdf->SetFont('helvetica', '', 8);
			
			$pdf->WriteHTML($Content, true, false, true, false, '');
			// Close and output PDF document
			$pdf->Output('Notification_Letter.pdf','D');
			return true;
		}
	}
/*	End	of	file	entrance_test_test_card.php	*/
/*	Location:		./controllers/report/entrance_test_test_card.php */

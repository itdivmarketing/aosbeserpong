<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class billing_and_payment_detail_payment extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/billing_and_payment_detail_payment_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->billing_and_payment_detail_payment_model->get_school_level();
			$data['Payment'] = $this->billing_and_payment_detail_payment_model->get_payment_method();
    
         	$this->template->display('report/billing_and_payment_detail_payment',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->billing_and_payment_detail_payment_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->billing_and_payment_detail_payment_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AdmissionID'] =  '';
			$P['AcademicYear'] = $temp[0];
			$P['SchoolLevelID'] = ($input['ddlSchoolLevel'] == 'all' ? '' : $input['ddlSchoolLevel']);
			$P['TermId'] = ($input['ddlAdmissionTerm'] == 'all' ? '' : $input['ddlAdmissionTerm']);
			$P['YearLevelId'] = ($input['ddlYearLevel'] == 'all' ? '' : $input['ddlYearLevel']);
			$P['PaymentMethod'] = $input['ddlPaymentMethod'];
			
			//$P['tipe'] = $input['rbReportType'];
			$tipe="";
			if(isset($input['cbxAll'])){
				$tipe="1,2,3";
			}
			if(isset($input['cbxFull'])){
				$tipe=$tipe."1,";
			}
			if(isset($input['cbxNotFull'])){
				$tipe=$tipe."2,";
			}
			if(isset($input['cbxNotPaid'])){
				$tipe=$tipe."3,";
			}
			if(substr($tipe, -1)==","){
				$tipe=substr($tipe,0, strlen($tipe)-1);
			}
			$P['tipe'] = $tipe;
			//print_r($tipe);die;
			$data['report'] =  $this->billing_and_payment_detail_payment_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->billing_and_payment_detail_payment_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['AcademicSemester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->billing_and_payment_detail_payment_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->billing_and_payment_detail_payment_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			$this->load->view('report/print_detail_payment',$data);
		}
	}
/*	End	of	file	billing_and_payment_detail_payment.php	*/
/*	Location:		./controllers/report/billing_and_payment_detail_payment.php */

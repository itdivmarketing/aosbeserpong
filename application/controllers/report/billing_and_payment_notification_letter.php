<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class billing_and_payment_notification_letter extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/billing_and_payment_notification_letter_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period); 
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$par['RegistrantId'] = '';
			
			$data['last_letter'] = $this->billing_and_payment_notification_letter_model->get_notification_letter($par);  
			$param['academicYear'] = $temp[0];
			$data['package'] = $this->billing_and_payment_notification_letter_model->get_paket_fee($param);
			$p['RegistrantID'] = '';
			//SERPONG
			$p['lokasi'] = '15';
			//SIMRPUG
			//$p['lokasi'] = '16';
			$data['new_letter'] = $this->billing_and_payment_notification_letter_model->generate_new_letter_number($p);
         	$this->template->display('report/billing_and_payment_notification_letter',$data);
        }
		
		public function search()
        {
			$input = (array)$this->input->get();
			
			if($input['txtRegistrantID'] == '')
			{
				$data['message'] = 'RegistrantID must be filled';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				
			}
			else
			{
				
				$P['RegistrantID'] = $input['txtRegistrantID'];
				$data['report'] =  $this->billing_and_payment_notification_letter_model->search($P);
				$p['RegistrantId'] = $input['txtRegistrantID'];
				$data['letter'] = $this->billing_and_payment_notification_letter_model->get_notification_letter($p);  
			}

			
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$param['academicYear'] = $temp[0];
			$data['package'] = $this->billing_and_payment_notification_letter_model->get_paket_fee($param);
			$par['RegistrantId'] = '';
			$data['last_letter'] = $this->billing_and_payment_notification_letter_model->get_notification_letter($par); 
		
			$pa['RegistrantID'] = $input['txtRegistrantID'];
			//SERPONG
			$pa['lokasi'] = '15';
			//SIMRPUG
			//$pa['lokasi'] = '16';
			$data['new_letter'] = $this->billing_and_payment_notification_letter_model->generate_new_letter_number($pa);
			$this->template->display('report/billing_and_payment_notification_letter',$data);
        }
		
		public function generate_report()
		{
			$lg = $this->input->get('lg');

			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AcademicYear'] = $temp[0];
			$P['SmtId'] = $temp[1];
			
			$p['RegistrantId'] = $input['hfRegistrantID'];
			$p['NoSurat'] = $input['txtCode'].$input['txtLetterNumber'];
			$p['AuditUserName'] = $this->session->userdata('UserId');
			$this->billing_and_payment_notification_letter_model->save_nomor_surat_notifikasi($p);

			$s['StudentID'] = $input['hfRegistrantID'];
			$status = $this->billing_and_payment_notification_letter_model->get_student_data($s);
            if(sizeof($status) <= 0){
                echo "<script>alert('No data')</script>";
                return;
            }

			$par['RegistrantId'] = $input['hfRegistrantID'];
			$data['report'] = $this->billing_and_payment_notification_letter_model->print_notification_letter($par);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			
			$param['PaketId'] = (isset($input['ddlPacakge']) ? $input['ddlPacakge'] : '');
			$data['package_tbc'] =  $this->billing_and_payment_notification_letter_model->get_biaya_paket_fee_tbc($param);
			if(sizeof($data['package_tbc']) <= 0)
			{
				$data['package_tbc'] = null;
			}
			if(isset($input['cbSpecial']))
			{
				if(isset($input['cbDiscount']))
				{
					$Par['PaketId'] = $input['ddlPackage'];
					$data['package'] =  $this->billing_and_payment_notification_letter_model->get_biaya_paket_fee_diskon($Par);
					if(sizeof($data['package']) <= 0)
					{
						$data['package'] = null;
					}
				}
				else
				{
					$Par['PaketId'] = $input['ddlPackage'];
					$data['package'] =  $this->billing_and_payment_notification_letter_model->get_biaya_paket_fee($Par);
					if(sizeof($data['package']) <= 0)
					{
						$data['package'] = null;
					}
				}
			}
			
			$d['Style'] = '106';
			$data['currentDate'] =  $this->billing_and_payment_notification_letter_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['Semester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->billing_and_payment_notification_letter_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->billing_and_payment_notification_letter_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			

			//$this->load->view('report/print_notification_letter',$data);
			
			$this->load->library('Pdf');

			/** Starting this part, some part of the code and comment is based on TCPDF example **/
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Binus International School');
			$pdf->SetTitle('Notification Letter');
			$pdf->SetSubject('NotificationLetter');
			$pdf->SetKeywords('NotificationLetter');

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			//set margins
			$pdf->SetHeaderMargin(0);
			$pdf->SetFooterMargin(0);

			// remove default footer
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);

			//set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			//set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			$pdf->setCellPaddings(0, 0, 0, 0);
			$pdf->setCellMargins(0, 0, 0, 0);
			
			// disable auto-page-break
			//$pdf->SetAutoPageBreak(false, 0);
			
			// add a page
			//$pdf->SetMargins(30, 55, 30);
			//$pdf->AddPage('P', 'A4');

			// set font
			$pdf->SetFont('helvetica', '', 9);
			
			//set text
			$this->load->helper('admission_helper');
			
			$content = '';
			$content2 = '';

			$txtDueDate = $input['txtDueDate'];	
			$txtDueDate = date("F d, Y", strtotime($txtDueDate));
			$txtDueDate_indo = indonesian_date(strtotime($txtDueDate),'j F Y','');


			$CurrDate = $data['printedDate'][0]->CurrDate;
			$CurrDate = date("d F, Y", strtotime($CurrDate));
			$CurrDate_indo = indonesian_date(strtotime($CurrDate),'j F Y','');

			if($status->StatusLulus=='1'){
			//english

				if (isset($lg[1]))
				{
						if($lg[0]=="id"&& $lg[1]=="en"){
						$checked=' ';
						
						for($i=0; $i<sizeof($lg);$i++){
								$temp = $lg[$i].",";
								$checked = $checked.$temp;
								
						}
						if($lg[0]){
							$pdf->SetMargins(35, 30, 20);
							$pdf->AddPage('P', 'A4');
							$content .=
								'<div width="30%" style="text-align:right;">'.
										'Tangerang, '.$CurrDate_indo.'<br/>'.
										'No : '.$input['txtCode'].$input['txtLetterNumber'].
								'</div>'.
								'<div width="100%" style="text-align:center;">'.
									'<b><u>Subjek : Surat Notifikasi Hasil Ujian Tes Masuk </u></b>'.
								'</div>';

							if(isset($data['report']))
							{
								$content .=
									'<div width="80%" style="text-align:left;">';
								if($data['report'][0]->FatherName<>'')
								{
									$content .=
											'<div width="80%">'.
											'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.',
											</div>';
								}
								if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
								{
									$content .=
											'<div width="80%">'.
											'Yth. Bpk/Ibu '.$data['report'][0]->MotherName.',
											</div>';
								}
								$content .=	
										'<div width="80%">'.						
										'Mohon membaca notifikasi berikut dengan seksama <br/>'.
										'Berdasarkan hasil ujian tes masuk terakhir, kami ingin memberitahu bahwa anak Bpk/Ibu : </div>'.
										
										'<div width="80%" style="text-align:left;">'.
										'<table>'.
											'<tr>'.
												'<td width="150px">&nbsp;</td>'.
												'<td width="100px">Nama</td>'.
												'<td width="10px"> :</td>'.
												'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
											'</tr>'.
											'<tr>'.
												'<td width="150px">&nbsp;</td>'.
												'<td width="100px">Level Akademis</td>'.
												'<td width="10px"> :</td>'.
												'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
											'</tr>'.
											'<tr>'.
												'<td width="150px">&nbsp;</td>'.
												'<td width="100px">Hasil</td>'.
												'<td width="10px"> :</td>'.
												'<td width="300px"><b>Direkomendasikan</b></td>'.
											'</tr>'.
										'</table>'.								
										'</div>'.
										'<span style="text-align: justify;">'.
											'Berdasarkan surat perjanjian yang biaya uang sekolah yang sudah ditandatangani, kami menyambut dengan baik apabila Bpk/Ibu berkenan untuk melanjutkan ke proses selanjutnya. Mohon mengikuti instruksi pembayaran dibawah ini :'.
										'</span><br/>'.
										'<table>'.
											'<thead>'.
												'<tr>'.
													'<th style="border: 1px solid black" align="center"><b>Batas Pembayaran</b></th>'.
													'<th style="border: 1px solid black" align="center"><b>Jumlah</b></th>'.
													'<th style="border: 1px solid black" align="center"><b>Tipe Pembayaran</b></th>'.
												'</tr>'.
											'</thead>';
								$countLength = sizeof($data['report']);
								//print_r($countLength);die;
								$count=1;
								$total = 0;
								//if($data['report'][0]->FeeTypeIdTBC != '') $countLength+=1;
								foreach ($data['report'] as $Row) :
									if($count == 1)
									{
										$content .= 
											'<tr>'.
												'<td style="vertical-align: bottom; border: 1px solid black;" align="center"  rowspan="'.($countLength+=1).'" > <b>'.str_repeat('&nbsp;<br/>',1).$txtDueDate_indo.'</b> </td>'.
												'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).' </td>'.
												'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
											'</tr>';
										$total+=$Row->NetAmount;
									}
									else
									{
										$content .= 
										'<tr>'.
											'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).'</td>'.
											'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
										'</tr>';
										$total+=$Row->NetAmount;
									}
									if ($count == $countLength) 
									{
										$count = 1;
									}
									else $count++;
								endforeach;
								$content .= 
									'<tr>'.
										'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
								$flag = false;
								if($Row->FlagDiskon)
								{
									$flag=true;
								}
								if($flag) $content .= ' (termasuk 10% Diskon Saudara) *';
								/*
								$content .= 
										'</td>'.
									'</tr>'.
									'<tr>'.
										'<td style="border: 1px solid black" align="center"> <b>Date to be confirmed later</b></td>'.
										'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
										'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
									'</tr>';*/
								$content .= '</td>'.
									'</tr>';

								if(isset($data['package_tbc'][0]->AmountTBC))
								{
									$content .= '<tr>'.
										'<td style="border: 1px solid black" align="center"> <b>Tanggal yang akan dikonfirmasi</b></td>'.
										'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
										'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
									'</tr>';
								}
								if(isset($_GET['cbSpecial']))
								{
									$content .= 
										'<tr>'.
											'<td style="border: 1px solid black" align="center" colspan="3"><b>atau</b></td>'.
										'</tr>';
									$countLength = sizeof($data['package'])+1;
									$count=1;
									$total = 0;
									foreach ($data['package'] as $Row) :
									if($count == 1)
									{
										$TanggalJatuhTempo = $Row->TanggalJatuhTempo;

										$TanggalJatuhTempo = indonesian_date(strtotime($TanggalJatuhTempo),'j F Y','');
										$content .= 
											'<tr>'.
												'<td style="vertical-align:bottom; border: 1px solid black;" align="center" rowspan="'.($countLength+=1).'"><b>'.str_repeat('&nbsp;<br/>',1).$TanggalJatuhTempo.'</b></td>'.
												'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
												'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
											'</tr>';
										$total+=$Row->Amount;
									}
									else
									{
										$content .= 
											'<tr>'.
												'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
												'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
											'</tr>';
										$total+=$Row->Amount;
									}
									if ($count == $countLength) 
									{
										$count = 1;
									}
									else $count++;
									endforeach;
									$content .= 
										'<tr>'.
											'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
											//(isset($data['package'][0]->NoteDiskon) ? $data['package'][0]->NoteDiskon : '').
									$flag_special = false;
									if(isset($input['cbDiscount']))
									{
										$flag_special=true;
									}		
									if($flag_special) $content .= ' (termasuk 10% Diskon Saudara) *';
									$content .= 
											'</td>'.
										'</tr>';
								}
								$content .= 
									'</table>';
										if($flag  || (isset($flag_special) && $flag_special)) 
										{ 
											$content .='<i style="font-size:8px;">'.
											'*) Diskon Saudara hanya berlaku untuk pembayaran pendaftaran dan jika murid mempunyai saudara yang sedang belajar atau sedang mendaftar di BINUS SCHOOL Serpong pada tahun yang sama.<br/>'.
											( $data['report'][0]->SiblingName != "" ? 'Nama Saudara : '.$data['report'][0]->SiblingName : '').
										'</i>';
										}
										else
										{
											$content .= '<br/>';
										}
								$content .=
									'<div style="text-align: justify;">'.
										'Semua pembayaran harus dilakukan dengan transfer melalui ATM BCA, Mohon mengikuti langkah pembayaran dibawah ini :'.
									'</div>'.
									'<table>'.
										'<tr>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">1.</td>'.
											'<td style="border: 1px solid black" width="40%"> Insert your ATM Card<br/><i> Masukkan kartu ATM Anda</i></td>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">7.</td>'.
											'<td style="border: 1px solid black" width="40%"> Enter Company\'s Code : 710023<br/><i> Masukkan Kode Perusahaan : 710023</i></td>'.
										'</tr>'.
										'<tr>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">2.</td>'.
											'<td style="border: 1px solid black" width="40%"> Enter your PIN<br/><i> Masukkan PIN Anda</i></td>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">8.</td>'.
											'<td style="border: 1px solid black" width="40%"> Enter Registration Number : '.$data['report'][0]->RegistrantID.' <br/><i> Masukkan Nomor Registrasi : '.$data['report'][0]->RegistrantID.' </i></td>'.
										'</tr>'.
										'<tr>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">3.</td>'.
											'<td style="border: 1px solid black" width="40%"> Press "Other Transaction"<br/><i> Pilih menu "Transaksi Lainnya"</i></td>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">9.</td>'.
											'<td style="border: 1px solid black" width="40%"> Enter Payment Amount<br/><i> Masukkan Jumlah Pembayaran</i></td>'.
										'</tr>'.
										'<tr>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">4.</td>'.
											'<td style="border: 1px solid black" width="40%"> Press "Payment"<br/><i> Pilih menu "Pembayaran"</i></td>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">10.</td>'.
											'<td style="border: 1px solid black" width="40%"> Payment Confirmation, press "YES" <br/><i> Konfirmasi Pembayaran, Pilih "YA"</i></td>'.
										'</tr>'.
										'<tr>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">5.</td>'.
											'<td style="border: 1px solid black" width="40%"> Press "Next Screen"<br/><i> Pilih menu "Layar Berikut"</i></td>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">11.</td>'.
											'<td style="border: 1px solid black" width="40%"> Another Transaction, press "NO"<br/><i> Pilihan untuk transaksi lain, pilih "TIDAK"</i></td>'.
										'</tr>'.
										'<tr>'.
											'<td style="vertical-align:middle; border: 1px solid black;" align="center" width="10%">6.</td>'.
											'<td style="border: 1px solid black" width="40%"> Press "Others"<br/><i> Pilih menu "Lain-lain"</i></td>'.
											'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">12.</td>'.
											'<td style="border: 1px solid black" width="40%"> Get ATM Payment Receipt<br/><i> Terima Bukti Pembayaran ATM</i></td>'.
										'</tr>'.
									'</table>'.
									'<br/>'.
									'<div style="text-align:justify">'.
										'Jika Bpk/Ibu belum memenuhi prosedur pembayaran atau konfirmasi pembayaran belum diterima oleh Departemen Marketing dan belum di registrasi ulang hingga <b>'.$txtDueDate_indo.'</b>, maka kami menganggap bahwa anak Bpk/Ibu telah mengundurkan diri dari BINUS SCHOOL Serpong.'.
									'</div>'.
									'<div style="text-align:justify">'.
										'Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.'.
									'</div>'.
									'<br/>'.
									'Hormat kami,<br/><br/>';
								if(isset($_GET['cbSignature']))
								{
									$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
								}
								else
								{
									$content .='<br/><br/><br/>';
								}
								$content .=	
								'<b><u>Pamela Zenita </u></b><br/>'.						
									'Marketing Manager<br/>'.
									'<i>PZ/'.$this->session->userData('Initial').'</i>'.
								'</div>';
							}
							$pdf->WriteHTML($content, true, false, true, false, '');
						}
						if($lg[1])
						{
							$pdf->SetMargins(35, 30, 20);
							$pdf->AddPage('P', 'A4');
							$content2 .=
					'<div width="30%" style="text-align:right;">'.	
							
							'Tangerang, '.$CurrDate.'<br/>'.
							'No : '.$input['txtCode'].$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subject : Notification Letter of Entrance Test Result </u></b>'.
					'</div>';
				if(isset($data['report']))
				{//23
					$content2 .=
						'<div width="80%" style="text-align:left;">';
					if($data['report'][0]->FatherName<>'')
					{//24
						$content2 .=
								'<div width="80%">'.
								'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.',
								</div>';
					}
					if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
					{
						$content2 .=
								'<div width="80%">'.
								'Dear Mr. & Mrs. '.$data['report'][0]->MotherName.',
								</div>';
					}
					$content2 .=	
							'<div width="80%">'.						
							'Please read the following notification carefully <br/>'.
							'Based on the Final Entrance Test Result, we would like to inform you, that your child :</div>'.
							
							'<div width="80%" style="text-align:left;">'.
							'<table>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Name</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Academic Level</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Result</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>Recommended</b></td>'.
								'</tr>'.
							'</table>'.
							'</div>'.
								'Based on Agreement Letter for School Fee\'s payment that had been signed, we would be most grateful if you would proceed to the next step. Please follow the instructions below concerning payment procedure :<br/>'.
							'<table>'.
								'<thead>'.
									'<tr>'.
										'<th style="border: 1px solid black" align="center"><b>Due Date</b></th>'.
										'<th style="border: 1px solid black" align="center"><b>Amount</b></th>'.
										'<th style="border: 1px solid black" align="center"><b>Type of Payment</b></th>'.
									'</tr>'.
								'</thead>';

					$countLength = sizeof($data['report']);
					//print_r($countLength);die;
					$count=1;
					$total = 0;
					//if($data['report'][0]->FeeTypeIdTBC != '') $countLength+=1;
					foreach ($data['report'] as $Row) :
						if($count == 1)
						{
							$content2 .= 
								'<tr>'.
									'<td style="vertical-align: bottom; border: 1px solid black;" align="center"  rowspan="'.($countLength+=1).'" > <b>'.str_repeat('&nbsp;<br/>',1).$txtDueDate.'</b> </td>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->NetAmount;
						}
						else
						{
							$content2 .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).'</td>'.
								'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
							'</tr>';
							$total+=$Row->NetAmount;
						}
						if ($count == $countLength) 
						{
							$count = 1;
						}
						else $count++;
					endforeach;
					$content2 .= 
						'<tr>'.
							'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
					$flag = false;
					if($Row->FlagDiskon)
					{
						$flag=true;
					}
					if($flag) $content2 .= ' (includes 10% of Sibling Discount) *';
					/*
					$content .= 
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td style="border: 1px solid black" align="center"> <b>Date to be confirmed later</b></td>'.
							'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
							'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
						'</tr>';*/
					$content2 .= '</td>'.
						'</tr>';

					if(isset($data['package_tbc'][0]->AmountTBC))
					{
						$content2 .= '<tr>'.
							'<td style="border: 1px solid black" align="center"> <b>Date to be confirmed later</b></td>'.
							'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
							'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
						'</tr>';
					}
					if(isset($_GET['cbSpecial']))
					{
						$content2 .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center" colspan="3"><b>OR</b></td>'.
							'</tr>';
						$countLength = sizeof($data['package'])+1;
						$count=1;
						$total = 0;
						foreach ($data['package'] as $Row) :
						if($count == 1)
						{
							$TanggalJatuhTempo = $Row->TanggalJatuhTempo;
							$TanggalJatuhTempo = date("F d, Y", strtotime($TanggalJatuhTempo));
							$content2 .= 
								'<tr>'.
									'<td style="vertical-align:bottom; border: 1px solid black;" align="center" rowspan="'.($countLength+=1).'"><b>'.str_repeat('&nbsp;<br/>',1).$TanggalJatuhTempo.'</b></td>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->Amount;
						}
						else
						{
							$content2 .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->Amount;
						}
						if ($count == $countLength) 
						{
							$count = 1;
						}
						else $count++;
						endforeach;
						$content2 .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
								//(isset($data['package'][0]->NoteDiskon) ? $data['package'][0]->NoteDiskon : '').
						$flag_special = false;
						if(isset($input['cbDiscount']))
						{
							$flag_special=true;
						}		
						if($flag_special) $content2 .= ' (includes 10% of Sibling Discount) *';
						$content2 .= 
								'</td>'.
							'</tr>';
					}
					$content2 .= 
						'</table>';
							if($flag || (isset($flag_special) && $flag_special)) 
							{ 
								$content2 .='<i style="font-size:8px;">'.
								'*) Sibling Discount only applies to Enrollment Fee and if student has a sibling studying or also enrolling at BINUS SCHOOL Serpong in the same academic year.<br/>'.
								( $data['report'][0]->SiblingName != "" ? 'Sibling\'s Name :'.$data['report'][0]->SiblingName : '').
							'</i>';
							}
							else
							{
								$content2 .= '<br/>';
							}
					$content2 .=
						'<br/>'.
							'All the payment above must be fulfilled by transfer to BCA ATM, please follow the payment steps as below :<br/>'.
						'<table>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">1.</td>'.
								'<td style="border: 1px solid black" width="40%"> Insert your ATM Card<br/><i> Masukkan kartu ATM Anda</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">7.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Company\'s Code : 710023<br/><i> Masukkan Kode Perusahaan : 710023</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">2.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter your PIN<br/><i> Masukkan PIN Anda</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">8.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Registration Number : '.$data['report'][0]->RegistrantID.' <br/><i> Masukkan Nomor Registrasi : '.$data['report'][0]->RegistrantID.' </i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">3.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Other Transaction"<br/><i> Pilih menu "Transaksi Lainnya"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">9.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Payment Amount<br/><i> Masukkan Jumlah Pembayaran</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">4.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Payment"<br/><i> Pilih menu "Pembayaran"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">10.</td>'.
								'<td style="border: 1px solid black" width="40%"> Payment Confirmation, press "YES" <br/><i> Konfirmasi Pembayaran, Pilih "YA"</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">5.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Next Screen"<br/><i> Pilih menu "Layar Berikut"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">11.</td>'.
								'<td style="border: 1px solid black" width="40%"> Another Transaction, press "NO"<br/><i> Pilihan untuk transaksi lain, pilih "TIDAK"</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black;" align="center" width="10%">6.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Others"<br/><i> Pilih menu "Lain-lain"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">12.</td>'.
								'<td style="border: 1px solid black" width="40%"> Get ATM Payment Receipt<br/><i> Terima Bukti Pembayaran ATM</i></td>'.
							'</tr>'.
						'</table>'.
						'<br/>'.
						'<div style="text-align: justify;">'.
							'If you have not fulfilled the payment procedure or payment confirmation have not received yet by Marketing Dept. and have not-done Re-Registration by <b>'.$txtDueDate.'</b>, then we will consider that you have resigned your child\'s enrollment in BINUS SCHOOL Serpong.'.
						'<div/>'.
						'<br/>'.
							'You\'re most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.'.
						'<br/>'.
						'<br/>'.
							'Yours Sincerely,<br/><br/>';
					if(isset($_GET['cbSignature']))
					{
						$content2 .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
					}
					else
					{
						$content2 .='<br/><br/><br/>';
					}
					$content2 .=
					'<b><u>Pamela Zenita </u></b><br/>'.
						'Marketing Manager<br/>'.
						'<i>PZ/'.$this->session->userData('Initial').'</i>'.
					'</div>';

						}
					}
					$pdf->WriteHTML($content2, true, false, true, false, '');
					} 
				}
		
			else if($lg[0]=="en"){
				$pdf->SetMargins(35, 30, 20);
				$pdf->AddPage('P', 'A4');
				$content .=
					'<div width="30%" style="text-align:right;">'.
							'Tangerang, '.$CurrDate.'<br/>'.
							'No : '.$input['txtCode'].$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subject : Notification Letter of Entrance Test Result </u></b>'.
					'</div>';
				if(isset($data['report']))
				{
					$content .=
						'<div width="80%" style="text-align:left;">';
					if($data['report'][0]->FatherName<>'')
					{
						$content .=
								'<div width="80%">'.
								'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.',
								</div>';
					}
					if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
					{
						$content .=
								'<div width="80%">'.
								'Dear Mr. & Mrs. '.$data['report'][0]->MotherName.',
								</div>';
					}
					$content .=	
							'<div width="80%">'.						
							'Please read the following notification carefully <br/>'.
							'Based on the Final Entrance Test Result, we would like to inform you, that your child:</div>'.
							
							'<div width="80%" style="text-align:left;">'.
							'<table>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Name</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Academic Level</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Result</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>Recommended</b></td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'</div>'.
								'Based on Agreement Letter for School Fee\'s payment that had been signed, we would be most grateful if you would proceed to the next step. Please follow the instructions below concerning payment procedure :<br/>'.
							'<table>'.
								'<thead>'.
									'<tr>'.
										'<th style="border: 1px solid black" align="center"><b>Due Date</b></th>'.
										'<th style="border: 1px solid black" align="center"><b>Amount</b></th>'.
										'<th style="border: 1px solid black" align="center"><b>Type of Payment</b></th>'.
									'</tr>'.
								'</thead>';
					$countLength = sizeof($data['report']);
					//print_r($countLength);die;
					$count=1;
					$total = 0;
					//if($data['report'][0]->FeeTypeIdTBC != '') $countLength+=1;
					foreach ($data['report'] as $Row) :
						if($count == 1)
						{
							$content .= 
								'<tr>'.
									'<td style="vertical-align: bottom; border: 1px solid black;" align="center"  rowspan="'.($countLength+=1).'" > <b>'.str_repeat('&nbsp;<br/>',1).$txtDueDate.'</b> </td>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->NetAmount;
						}
						else
						{
							$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).'</td>'.
								'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
							'</tr>';
							$total+=$Row->NetAmount;
						}
						if ($count == $countLength) 
						{
							$count = 1;
						}
						else $count++;
					endforeach;
					$content .= 
						'<tr>'.
							'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
					$flag = false;
					if($Row->FlagDiskon)
					{
						$flag=true;
					}
					if($flag) $content .= ' (includes 10% of Sibling Discount) *';
					/*
					$content .= 
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td style="border: 1px solid black" align="center"> <b>Date to be confirmed later</b></td>'.
							'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
							'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
						'</tr>';*/
					$content .= '</td>'.
						'</tr>';

					if(isset($data['package_tbc'][0]->AmountTBC))
					{
						$content .= '<tr>'.
							'<td style="border: 1px solid black" align="center"> <b>Date to be confirmed later</b></td>'.
							'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
							'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
						'</tr>';
					}
					if(isset($_GET['cbSpecial']))
					{
						$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center" colspan="3"><b>OR</b></td>'.
							'</tr>';
						$countLength = sizeof($data['package'])+1;
						$count=1;
						$total = 0;
						foreach ($data['package'] as $Row) :
						if($count == 1)
						{
							$TanggalJatuhTempo = $Row->TanggalJatuhTempo;
							$TanggalJatuhTempo = date("F d, Y", strtotime($TanggalJatuhTempo));
							$content .= 
								'<tr>'.
									'<td style="vertical-align:bottom; border: 1px solid black;" align="center" rowspan="'.($countLength+=1).'"><b>'.str_repeat('&nbsp;<br/>',1).$TanggalJatuhTempo.'</b></td>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->Amount;
						}
						else
						{
							$content .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->Amount;
						}
						if ($count == $countLength) 
						{
							$count = 1;
						}
						else $count++;
						endforeach;
						$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
								//(isset($data['package'][0]->NoteDiskon) ? $data['package'][0]->NoteDiskon : '').
						$flag_special = false;
						if(isset($input['cbDiscount']))
						{
							$flag_special=true;
						}				
						if($flag_special) $content .= ' (includes 10% of Sibling Discount) *';
						$content .= 
								'</td>'.
							'</tr>';
					}
					$content .= 
						'</table>';
							if($flag  || (isset($flag_special) && $flag_special)) 
							{ 
								$content .='<i style="font-size:8px;">'.
								'*) Sibling Discount only applies to Enrollment Fee and if student has a sibling studying or also enrolling at BINUS SCHOOL Serpong in the same academic year.<br/>'.
								( $data['report'][0]->SiblingName != "" ? 'Sibling\'s Name :'.$data['report'][0]->SiblingName : '').
							'</i>';
							}
							else
							{
								$content .= '<br/>';
							}
					$content .=
						'<br/>'.
							'All the payment above must be fulfilled by transfer to BCA ATM, please follow the payment steps as below:<br/>'.
						'<table>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">1.</td>'.
								'<td style="border: 1px solid black" width="40%"> Insert your ATM Card<br/><i> Masukkan kartu ATM Anda</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">7.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Company\'s Code : 710023<br/><i> Masukkan Kode Perusahaan : 710023</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">2.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter your PIN<br/><i> Masukkan PIN Anda</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">8.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Registration Number : '.$data['report'][0]->RegistrantID.' <br/><i> Masukkan Nomor Registrasi : '.$data['report'][0]->RegistrantID.' </i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">3.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Other Transaction"<br/><i> Pilih menu "Transaksi Lainnya"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">9.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Payment Amount<br/><i> Masukkan Jumlah Pembayaran</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">4.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Payment"<br/><i> Pilih menu "Pembayaran"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">10.</td>'.
								'<td style="border: 1px solid black" width="40%"> Payment Confirmation, press "YES" <br/><i> Konfirmasi Pembayaran, Pilih "YA"</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">5.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Next Screen"<br/><i> Pilih menu "Layar Berikut"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">11.</td>'.
								'<td style="border: 1px solid black" width="40%"> Another Transaction, press "NO"<br/><i> Pilihan untuk transaksi lain, pilih "TIDAK"</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black;" align="center" width="10%">6.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Others"<br/><i> Pilih menu "Lain-lain"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">12.</td>'.
								'<td style="border: 1px solid black" width="40%"> Get ATM Payment Receipt<br/><i> Terima Bukti Pembayaran ATM</i></td>'.
							'</tr>'.
						'</table>'.'<br/>'.
						'<div style="text-align: justify;">'.
							'If you have not fulfilled the payment procedure or payment confirmation have not received yet by Marketing Dept. and have not-done Re-Registration by <b>'.$txtDueDate.'</b>, then we will consider that you have resigned your child\'s enrollment in BINUS SCHOOL Serpong.'.
						'</div>'.
						'<br/>'.
							'You\'re most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.'.
						'<br/>'.
						'<br/>'.
							'Yours Sincerely,<br/><br/>';
					if(isset($_GET['cbSignature']))
					{
						$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
					}
					else
					{
						$content .='<br/><br/><br/>';
					}
					$content .=
					'<b><u>Pamela Zenita </u></b><br/>'.
						'Marketing Manager<br/>'.
						'<i>PZ/'.$this->session->userData('Initial').'</i>'.
					'</div>';
				}
				$pdf->WriteHTML($content, true, false, true, false, '');
			}

			//indo
			else if($lg[0]=="id"){ 

				$pdf->SetMargins(35, 30, 20);
				$pdf->AddPage('P', 'A4');
				$content .=
					'<div width="30%" style="text-align:right;">'.
							'Tangerang, '.$CurrDate_indo.'<br/>'.
							'No : '.$input['txtCode'].$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subjek : Surat Notifikasi Hasil Ujian Tes Masuk</u></b>'.
					'</div>';
				if(isset($data['report']))
				{
					$content .=
						'<div width="80%" style="text-align:left;">';
					if($data['report'][0]->FatherName<>'')
					{
						$content .=
								'<div width="80%">'.
								'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.',
								</div>';
					}
					if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
					{
						$content .=
								'<div width="80%">'.
								'Yth. Bpk/Ibu '.$data['report'][0]->MotherName.',
								</div>';
					}
					$content .=	
							'<div width="80%">'.						
							'Mohon membaca notifikasi berikut dengan seksama <br/>'.
							'Berdasarkan hasil ujian tes masuk terakhir, kami ingin memberitahu bahwa anak Bpk/Ibu :</div>'.
							
							'<div width="80%" style="text-align:left;">'.
							'<table>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Nama</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Level Akademis</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Hasil</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>Direkomendasikan</b></td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'</div>'.
							'<span style="text-align: justify;">'.
								'Berdasarkan surat perjanjian yang biaya uang sekolah yang sudah ditandatangani, kami menyambut dengan baik apabila Bpk/Ibu berkenan untuk melanjutkan ke proses selanjutnya. Mohon mengikuti instruksi pembayaran dibawah ini :'.
							'</span><br/>'.
							'<table>'.
								'<thead>'.
									'<tr>'.
										'<th style="border: 1px solid black" align="center"><b>Batas Pembayaran</b></th>'.
										'<th style="border: 1px solid black" align="center"><b>Jumlah</b></th>'.
										'<th style="border: 1px solid black" align="center"><b>Tipe Pembayaran</b></th>'.
									'</tr>'.
								'</thead>';
					$countLength = sizeof($data['report']);
					//print_r($countLength);die;
					$count=1;
					$total = 0;
					//if($data['report'][0]->FeeTypeIdTBC != '') $countLength+=1;
					foreach ($data['report'] as $Row) :
						if($count == 1)
						{
							$content .= 
								'<tr>'.
									'<td style="vertical-align: bottom; border: 1px solid black;" align="center"  rowspan="'.($countLength+=1).'" > <b>'.str_repeat('&nbsp;<br/>',1).$txtDueDate_indo.'</b> </td>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->NetAmount;
						}
						else
						{
							$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->Amount).'</td>'.
								'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
							'</tr>';
							$total+=$Row->NetAmount;
						}
						if ($count == $countLength) 
						{
							$count = 1;
						}
						else $count++;
					endforeach;
					$content .= 
						'<tr>'.
							'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
					$flag = false;
					if($Row->FlagDiskon)
					{
						$flag=true;
					}
					if($flag) $content .= ' (termasuk 10% Diskon Saudara) *';
					/*
					$content .= 
							'</td>'.
						'</tr>'.
						'<tr>'.
							'<td style="border: 1px solid black" align="center"> <b>Date to be confirmed later</b></td>'.
							'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
							'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
						'</tr>';*/
					$content .= '</td>'.
						'</tr>';

					if(isset($data['package_tbc'][0]->AmountTBC))
					{
						$content .= '<tr>'.
							'<td style="border: 1px solid black" align="center"> <b>Tanggal yang akan dikonfirmasi</b></td>'.
							'<td style="border: 1px solid black" align="center"> <b>Rp '.moneyFormat($data['package_tbc'][0]->AmountTBC).'</b> </td>'.
							'<td style="border: 1px solid black" align="center"> <b>'.$data['package_tbc'][0]->TypeOfPaymentTBC.'</b> </td>'.
						'</tr>';
					}
					if(isset($_GET['cbSpecial']))
					{
						$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center" colspan="3"><b>atau</b></td>'.
							'</tr>';
						$countLength = sizeof($data['package'])+1;
						$count=1;
						$total = 0;
						foreach ($data['package'] as $Row) :
						if($count == 1)
						{
							$TanggalJatuhTempo = $Row->TanggalJatuhTempo;
							$TanggalJatuhTempo = indonesian_date(strtotime($TanggalJatuhTempo),'j F Y','');
							$content .= 
								'<tr>'.
									'<td style="vertical-align:bottom; border: 1px solid black;" align="center" rowspan="'.($countLength+=1).'"><b>'.str_repeat('&nbsp;<br/>',1).$TanggalJatuhTempo.'</b></td>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->Amount;
						}
						else
						{
							$content .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center"> Rp '.moneyFormat($Row->FirstAmount).' </td>'.
									'<td style="border: 1px solid black" align="center"> '.$Row->TypeOfPayment.' </td>'.
								'</tr>';
							$total+=$Row->Amount;
						}
						if ($count == $countLength) 
						{
							$count = 1;
						}
						else $count++;
						endforeach;
						$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center" colspan="2"><b>Total : Rp '.moneyFormat($total).' </b>';
								//(isset($data['package'][0]->NoteDiskon) ? $data['package'][0]->NoteDiskon : '').
						$flag_special = false;
						if(isset($input['cbDiscount']))
						{
							$flag_special=true;
						}			
						if($flag_special) $content .= ' (termasuk 10% Diskon Saudara) *';
						$content .= 
								'</td>'.
							'</tr>';
					}
					$content .= 
						'</table>';
							if($flag  || (isset($flag_special) && $flag_special)) 
							{ 
								$content .='<i style="font-size:8px;">'.
								'*) Diskon Saudara hanya berlaku untuk pembayaran pendaftaran dan jika murid mempunyai saudara yang sedang belajar atau sedang mendaftar di BINUS SCHOOL Serpong pada tahun yang sama.<br/>'.
								( $data['report'][0]->SiblingName != "" ? 'Nama Saudara : '.$data['report'][0]->SiblingName : '').
							'</i>';
							}
							else
							{
								$content .= '<br/>';
							}
					$content .=
						'<div style="text-align: justify;">'.
							'Semua pembayaran harus dilakukan dengan transfer melalui ATM BCA, Mohon mengikuti langkah pembayaran dibawah ini :</div>'.
						'<table>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">1.</td>'.
								'<td style="border: 1px solid black" width="40%"> Insert your ATM Card<br/><i> Masukkan kartu ATM Anda</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">7.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Company\'s Code : 710023<br/><i> Masukkan Kode Perusahaan : 710023</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">2.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter your PIN<br/><i> Masukkan PIN Anda</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">8.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Registration Number : '.$data['report'][0]->RegistrantID.' <br/><i> Masukkan Nomor Registrasi : '.$data['report'][0]->RegistrantID.' </i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">3.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Other Transaction"<br/><i> Pilih menu "Transaksi Lainnya"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">9.</td>'.
								'<td style="border: 1px solid black" width="40%"> Enter Payment Amount<br/><i> Masukkan Jumlah Pembayaran</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">4.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Payment"<br/><i> Pilih menu "Pembayaran"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">10.</td>'.
								'<td style="border: 1px solid black" width="40%"> Payment Confirmation, press "YES" <br/><i> Konfirmasi Pembayaran, Pilih "YA"</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">5.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Next Screen"<br/><i> Pilih menu "Layar Berikut"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">11.</td>'.
								'<td style="border: 1px solid black" width="40%"> Another Transaction, press "NO"<br/><i> Pilihan untuk transaksi lain, pilih "TIDAK"</i></td>'.
							'</tr>'.
							'<tr>'.
								'<td style="vertical-align:middle; border: 1px solid black;" align="center" width="10%">6.</td>'.
								'<td style="border: 1px solid black" width="40%"> Press "Others"<br/><i> Pilih menu "Lain-lain"</i></td>'.
								'<td style="vertical-align:middle; border: 1px solid black" align="center" valign="middle" width="10%">12.</td>'.
								'<td style="border: 1px solid black" width="40%"> Get ATM Payment Receipt<br/><i> Terima Bukti Pembayaran ATM</i></td>'.
							'</tr>'.
						'</table>'.
						'<br/><div style="text-align: justify;">'.
							'Jika Bpk/Ibu belum memenuhi prosedur pembayaran atau konfirmasi pembayaran belum diterima oleh Departemen Marketing dan belum di registrasi ulang hingga <b>'.$txtDueDate_indo.'</b>, maka kami menganggap bahwa anak Bpk/Ibu telah mengundurkan diri dari BINUS SCHOOL Serpong.'.
						'</div>'.
						'<div style="text-align: justify;">'.
							'Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.'.
						'</div>'.
						'<br/>'.
							'Hormat kami,<br/><br/>';
					if(isset($_GET['cbSignature']))
					{
						$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
					}
					else
					{
						$content .='<br/><br/><br/>';
					}
					$content .=
					'<b><u>Pamela Zenita </u></b><br/>'.
						'Marketing Manager<br/>'.
						'<i>PZ/'.$this->session->userData('Initial').'</i>'.
					'</div>';
					$pdf->WriteHTML($content, true, false, true, false, '');
				}
			
		}
	}
		else if($status->StatusLulus=='2'){//passed with condition 

			if (isset($lg[1]))
				{

					if($lg[0]=="id"&& $lg[1]=="en"){
					$checked=' ';
					
					for($i=0; $i<sizeof($lg);$i++){
							$temp = $lg[$i].",";
							$checked = $checked.$temp;
							
					}
					if($lg[1]){
						$pdf->SetMargins(35, 30, 20);
						$pdf->AddPage('P', 'A4');

						$content .=
								'<div width="30%" style="text-align:right;">'.
										'Tangerang, '.$CurrDate.'<br/>'.
										'No : '.$input['txtCode'].$input['txtLetterNumber'].
								'</div>'.
								'<div width="100%" style="text-align:center;">'.
									'<b><u>Subject : Notification Letter of Entrance Test Result</u></b>'.
								'</div><br/>';
						if($data['report'][0]->FatherName<>'')
						{
							$content .=
									'<div width="80%">'.
									'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.',
									</div>';
						}
						if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
						{
							$content .=
									'<div width="80%">'.
									'Dear Mr. & Mrs. '.$data['report'][0]->MotherName.',
									</div>';
						}

						$content .=	
							'<div width="80%">'.						
							'Please read the following notification carefully <br/>'.
							'Based on the Final Entrance Test Result, we would like to inform you, that your child :</div>'.
							
							'<div width="80%" style="text-align:left;">'.
							'<table>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Name</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
								'</tr>'.

								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Academic Level</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Result</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>Passed with condition</b></td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'</div>';
							
						$content.=	'You\'re passed with condition '.$status->Notes.
									'<br/>'.						
									'<br/>'.
									'<span style="text-align:justify;">'.
										'You\'re most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.</span>'.
									'<br/>'.
									'<br/>'.
									'Yours Sincerely,<br/><br/>';
							if(isset($_GET['cbSignature']))
							{
								$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
							}
							else
							{
								$content .='<br/><br/><br/>';
							}
							$content .=
								'<b><u>Pamela Zenita </u></b><br/>'.
								'Marketing Manager<br/>'.
								'<i>PZ/'.$this->session->userData('Initial').'</i>'.
							'</div>';

						$pdf->WriteHTML($content, true, false, true, false, '');
					}
					if($lg[0])
					{
						$pdf->SetMargins(35, 30, 20);
						$pdf->AddPage('P', 'A4');

						$content2 .=
					'<div width="30%" style="text-align:right;">'.
					
							'Tangerang, '.$CurrDate_indo.'<br/>'.
							'No : '.$input['txtCode'].$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subjek : Surat Notifikasi Hasil Ujian Tes Masuk </u></b>'.
					'</div><br/>';
					if($data['report'][0]->FatherName<>'')
					{
						$content2 .=
								'<div width="80%">'.
								'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.',
								</div>';
					}
					if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
					{
						$content2 .=
								'<div width="80%">'.
								'Yth. Bpk/Ibu. '.$data['report'][0]->MotherName.',
								</div>';
					}

					$content2 .=	
						'<div width="80%">'.						
						'Mohon membaca notifikasi berikut dengan seksama <br/>'.
						'Berdasarkan hasil ujian tes masuk terakhir, kami ingin memberitahu bahwa anak Bpk/Ibu :</div>'.
						
						'<div width="80%" style="text-align:left;">'.
						'<table>'.
							'<tr>'.
								'<td width="150px">&nbsp;</td>'.
								'<td width="100px">Nama</td>'.
								'<td width="10px"> :</td>'.
								'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
							'</tr>'.

							'<tr>'.
								'<td width="150px">&nbsp;</td>'.
								'<td width="100px">Level Akademis</td>'.
								'<td width="10px"> :</td>'.
								'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td width="150px">&nbsp;</td>'.
								'<td width="100px">Hasil</td>'.
								'<td width="10px"> :</td>'.
								'<td width="300px"><b>Lulus dengan syarat</b></td>'.
							'</tr>'.
						'</table>'.
						'<br/>'.
						'</div>';
						
					$content2.=	'Lulus dengan syarat '.$status->Notes.
								'<br/>'.
								'<br/>'.
								'Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.'.
								'<br/>'.
								'<br/>'.
								'Hormat kami,<br/><br/>';
						if(isset($_GET['cbSignature']))
						{
							$content2 .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
						}
						else
						{
							$content2 .='<br/><br/><br/>';
						}
						$content2 .=
						'<b><u>Pamela Zenita </u></b><br/>'.
							'Marketing Manager<br/>'.
							'<i>PZ/'.$this->session->userData('Initial').'</i>'.
						'</div>';
						$pdf->WriteHTML($content2, true, false, true, false, '');
					}
					
			} 

		}

		else if($lg[0]=="en"){ //english
			$pdf->SetMargins(35, 30, 20);
			$pdf->AddPage('P', 'A4');
			
			$content .=			'<div width="30%" style="text-align:right;">'.
										'Tangerang, '.$CurrDate.'<br/>'.
										'No : '.$input['txtCode'].$input['txtLetterNumber'].
								'</div>'.
								'<div width="100%" style="text-align:center;">'.
									'<b><u>Subject : Notification Letter of Entrance Test Result</u></b>'.
								'</div>';
			if($data['report'][0]->FatherName<>'')
			{
				$content .=
						'<br/><div width="80%">'.
						'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.',
						</div>';
			}
			if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
			{
				$content .=
						'<div width="80%">'.
						'Dear Mr. & Mrs. '.$data['report'][0]->MotherName.',
						</div>';
			}

			$content .=	
				'<div width="80%">'.						
				'Please read the following notification carefully <br/>'.
				'Based on the Final Entrance Test Result, we would like to inform you, that your child:</div>'.
				
				'<div width="80%" style="text-align:left;">'.
				'<table>'.
					'<tr>'.
						'<td width="150px">&nbsp;</td>'.
						'<td width="100px">Name</td>'.
						'<td width="10px"> :</td>'.
						'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
					'</tr>'.

					'<tr>'.
						'<td width="150px">&nbsp;</td>'.
						'<td width="100px">Academic Level</td>'.
						'<td width="10px"> :</td>'.
						'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td width="150px">&nbsp;</td>'.
						'<td width="100px">Result</td>'.
						'<td width="10px"> :</td>'.
						'<td width="300px"><b>Passed with condition</b></td>'.
					'</tr>'.
				'</table>'.
				'<br/>'.
				'</div>';
				
			$content.=	'You\'re passed with condition '.$status->Notes.
						'<br/>'.						
						'<br/>'.
							'<span style="text-align:justify">You\'re most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.</span>'.
						'<br/>'.
						'<br/>'.
						'Yours Sincerely,<br/><br/>';
				if(isset($_GET['cbSignature']))
				{
					$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
				}
				else
				{
					$content .='<br/><br/><br/>';
				}
				$content .=
					'<b><u>Pamela Zenita </u></b><br/>'.
					'Marketing Manager<br/>'.
					'<i>PZ/'.$this->session->userData('Initial').'</i>'.
				'</div>';
				$pdf->WriteHTML($content, true, false, true, false, '');
			}

			else if($lg[0]=="id"){//indo
				$pdf->SetMargins(35, 30, 20);
				$pdf->AddPage('P', 'A4');
				$content .=
					'<div width="30%" style="text-align:right;">'.

							'Tangerang, '.$CurrDate_indo.'<br/>'.
							'No : '.$input['txtCode'].$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subjek : Surat Notifikasi Hasil Ujian Tes Masuk </u></b>'.
					'</div>';
				if($data['report'][0]->FatherName<>'')
				{
					$content .=
							'<div width="80%">'.
							'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.',
							</div>';
				}
				if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
				{
					$content .=
							'<div width="80%">'.
							'Yth. Bpk/Ibu. '.$data['report'][0]->MotherName.',
							</div>';
				}

				$content .=	
					'<div width="80%">'.						
					'Mohon membaca notifikasi berikut dengan seksama <br/>'.
					'Berdasarkan hasil ujian tes masuk terakhir, kami ingin memberitahu bahwa anak Bpk/Ibu:</div>'.
					
					'<div width="80%" style="text-align:left;">'.
					'<table>'.
						'<tr>'.
							'<td width="150px">&nbsp;</td>'.
							'<td width="100px">Nama</td>'.
							'<td width="10px"> :</td>'.
							'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
						'</tr>'.

						'<tr>'.
							'<td width="150px">&nbsp;</td>'.
							'<td width="100px">Level Akademis</td>'.
							'<td width="10px"> :</td>'.
							'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
						'</tr>'.
						'<tr>'.
							'<td width="150px">&nbsp;</td>'.
							'<td width="100px">Hasil</td>'.
							'<td width="10px"> :</td>'.
							'<td width="300px"><b>Lulus dengan syarat</b></td>'.
						'</tr>'.
					'</table>'.
					'<br/>'.
					'</div>';
					
					$content.=	'Lulus dengan syarat '.$status->Notes.
								'<br/>'.
								'<br/>'.
								'<span style="text-align:justify">'.
								'Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.'.
								'<br/>'.
								'<br/>'.
								'Hormat kami,<br/><br/>';

					if(isset($_GET['cbSignature']))
					{
						$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
					}
					else
					{
						$content .='<br/><br/><br/>';
					}
					$content .=
					'<b><u>Pamela Zenita </u></b><br/>'.
						'Marketing Manager<br/>'.
						'<i>PZ/'.$this->session->userData('Initial').'</i>'.
					'</div>';

					$pdf->WriteHTML($content, true, false, true, false, '');
				}

		}
	

		else if($status->StatusLulus=='3'){//failed
		
		if (isset($lg[1]))
				{
					if($lg[0]=="id"&& $lg[1]=="en"){ 
					$checked=' ';
					
					for($i=0; $i<sizeof($lg);$i++){
							$temp = $lg[$i].",";
							$checked = $checked.$temp;
							
					}
					if($lg[1]){
						$pdf->SetMargins(35, 30, 20);
						$pdf->AddPage('P', 'A4');

						$content .=
								'<div width="30%" style="text-align:right;">'.
										'Tangerang, '.$CurrDate.'<br/>'.
										'No : '.$input['txtCode'].$input['txtLetterNumber'].
								'</div>'.
								'<div width="100%" style="text-align:center;">'.
									'<b><u>Subject : Notification Letter of Entrance Test Result</u></b>'.
								'</div><br/>';
						if($data['report'][0]->FatherName<>'')
						{
							$content .=
									'<div width="80%">'.
									'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.',
									</div>';
						}
						if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
						{
							$content .=
									'<div width="80%">'.
									'Dear Mr. & Mrs. '.$data['report'][0]->MotherName.',
									</div>';
						}

						$content .=	
							'<div width="80%">'.						
							'Please read the following notification carefully <br/>'.
							'Based on the Final Entrance Test Result, we would like to inform you, that your child :</div>'.
							
							'<div width="80%" style="text-align:left;">'.
							'<table>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Name</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
								'</tr>'.

								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Academic Level</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Result</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>Not Recommended</b></td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'</div>';
							
						$content.=	'You\'re most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.'.
									'<br/>'.
									'<br/>'.
									'Yours Sincerely,<br/><br/>';
							if(isset($_GET['cbSignature']))
							{
								$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
							}
							else
							{
								$content .='<br/><br/><br/>';
							}
							$content .=
								'<b><u>Pamela Zenita </u></b><br/>'.
								'Marketing Manager<br/>'.
								'<i>PZ/'.$this->session->userData('Initial').'</i>'.
							'</div>';
							$pdf->WriteHTML($content, true, false, true, false, '');
					}

					if($lg[0])
							{
								$pdf->SetMargins(35, 30, 20);
								$pdf->AddPage('P', 'A4');
								$content2 .=
							'<div width="30%" style="text-align:right;">'.
									
									'Tangerang, '.$CurrDate_indo.'<br/>'.
									'No : '.$input['txtCode'].$input['txtLetterNumber'].
							'</div>'.
							'<div width="100%" style="text-align:center;">'.
								'<b><u>Subjek : Surat Notifikasi Hasil Ujian Tes Masuk </u></b>'.
							'</div>';
						if($data['report'][0]->FatherName<>'')
						{
							$content2 .=
									'<div width="80%">'.
									'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.',
									</div>';
						}
						if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
						{
							$content2 .=
									'<div width="80%">'.
									'Yth. Bpk/Ibu. '.$data['report'][0]->MotherName.',
									</div>';
						}

						$content2 .=	
							'<div width="80%">'.						
							'Mohon membaca notifikasi berikut dengan seksama <br/>'.
							'Berdasarkan hasil ujian tes masuk terakhir, kami ingin memberitahu bahwa anak Bpk/Ibu:</div>'.
							
							'<div width="80%" style="text-align:left;">'.
							'<table>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Nama</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
								'</tr>'.

								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Level Akademis</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
								'</tr>'.
								'<tr>'.
									'<td width="150px">&nbsp;</td>'.
									'<td width="100px">Hasil</td>'.
									'<td width="10px"> :</td>'.
									'<td width="300px"><b>Tidak Direkomendasikan</b></td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'</div>';
							
						$content2.=	'Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.'.
									'<br/><br/>'.
									'Hormat kami,<br/><br/>';
							if(isset($_GET['cbSignature']))
							{
								$content2 .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
							}
							else
							{
								$content2 .='<br/><br/><br/>';
							}
							$content2 .=
							'<b><u>Pamela Zenita </u></b><br/>'.
								'Marketing Manager<br/>'.
								'<i>PZ/'.$this->session->userData('Initial').'</i>'.
							'</div>';
							$pdf->WriteHTML($content2, true, false, true, false, '');
						}
					
					} 
				
				}
		 else if($lg[0]=="en"){ //english
			$pdf->SetMargins(35, 30, 20);
			$pdf->AddPage('P', 'A4');
			$content .=
					'<div width="30%" style="text-align:right;">'.
							'Tangerang, '.$CurrDate.'<br/>'.
							'No : '.$input['txtCode'].$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subject : Notification Letter of Entrance Test Result</u></b>'.
					'</div><br/>';
			if($data['report'][0]->FatherName<>'')
			{
				$content .=
						'<div width="80%">'.
						'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.',
						</div>';
			}
			if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
			{
				$content .=
						'<div width="80%">'.
						'Dear Mr. & Mrs. '.$data['report'][0]->MotherName.',
						</div>';
			}

			$content .=	
				'<div width="80%">'.						
				'Please read the following notification carefully <br/>'.
				'Based on the Final Entrance Test Result, we would like to inform you, that your child :</div>'.
				
				'<div width="80%" style="text-align:left;">'.
				'<table>'.
					'<tr>'.
						'<td width="150px">&nbsp;</td>'.
						'<td width="100px">Name</td>'.
						'<td width="10px"> :</td>'.
						'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
					'</tr>'.

					'<tr>'.
						'<td width="150px">&nbsp;</td>'.
						'<td width="100px">Academic Level</td>'.
						'<td width="10px"> :</td>'.
						'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td width="150px">&nbsp;</td>'.
						'<td width="100px">Result</td>'.
						'<td width="10px"> :</td>'.
						'<td width="300px"><b>Not Recommended</b></td>'.
					'</tr>'.
				'</table>'.
				'<br/>'.
				'</div>';
				
			$content.=	'You\'re most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.'.
						'<br/>'.
						'<br/>'.
						'Yours Sincerely,<br/><br/>';
				if(isset($_GET['cbSignature']))
				{
					$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
				}
				else
				{
					$content .='<br/><br/><br/>';
				}
				$content .=
					'<b><u>Pamela Zenita </u></b><br/>'.
					'Marketing Manager<br/>'.
					'<i>PZ/'.$this->session->userData('Initial').'</i>'.
				'</div>';
				$pdf->WriteHTML($content, true, false, true, false, '');
			}

			else if($lg[0]=="id"){//indo
				$pdf->SetMargins(35, 30, 20);
				$pdf->AddPage('P', 'A4');
				$content .=
					'<div width="30%" style="text-align:right;">'.
							'Tangerang, '.$CurrDate_indo.'<br/>'.
							'No : '.$input['txtCode'].'/'.$input['txtLetterNumber'].
					'</div>'.
					'<div width="100%" style="text-align:center;">'.
						'<b><u>Subjek : Surat Notifikasi Hasil Ujian Tes Masuk </u></b>'.
					'</div>';
				if($data['report'][0]->FatherName<>'')
				{
					$content .=
							'<div width="80%">'.
							'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.',
							</div>';
				}
				if($data['report'][0]->FatherName=='' && $data['report'][0]->MotherName=='')
				{
					$content .=
							'<div width="80%">'.
							'Yth. Bpk/Ibu. '.$data['report'][0]->MotherName.',
							</div>';
				}
				$content .=	
					'<div width="80%">'.						
					'Mohon membaca notifikasi berikut dengan seksama <br/>'.
					'Berdasarkan hasil ujian tes masuk terakhir, kami ingin memberitahu bahwa anak Bpk/Ibu :</div>'.
					
					'<div width="80%" style="text-align:left;">'.
					'<table>'.
						'<tr>'.
							'<td width="150px">&nbsp;</td>'.
							'<td width="100px">Nama</td>'.
							'<td width="10px"> :</td>'.
							'<td width="300px"><b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')'.'</b></td>'.
						'</tr>'.

						'<tr>'.
							'<td width="150px">&nbsp;</td>'.
							'<td width="100px">Level Akademis</td>'.
							'<td width="10px"> :</td>'.
							'<td width="300px"><b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b></td>'.
						'</tr>'.
						'<tr>'.
							'<td width="150px">&nbsp;</td>'.
							'<td width="100px">Hasil</td>'.
							'<td width="10px"> :</td>'.
							'<td width="300px"><b>Tidak Direkomendasikan</b></td>'.
						'</tr>'.
					'</table>'.
					'<br/>'.
					'</div>';
					
				$content.=	'<span style="text-align:justify">Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.</span>'.
							'<br/>'.
							'<br/>'.
							'Hormat kami,<br/><br/>';
							
					if(isset($_GET['cbSignature']))
					{
						$content .='<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>';
					}
					else
					{
						$content .='<br/><br/><br/>';
					}
					$content .=
					'<b><u>Pamela Zenita </u></b><br/>'.
						'Marketing Manager<br/>'.
						'<i>PZ/'.$this->session->userData('Initial').'</i>'.
					'</div>';

					$pdf->WriteHTML($content, true, false, true, false, '');
				}
				
		}
	

			//var_dump($content);
			//$pdf->WriteHTML($content, true, false, true, false, '');
			// Close and output PDF document
			$pdf->Output('Notification_Letter.pdf','I');
			return true;
		

	}
}
/*	End	of	file	billing_and_payment_notification_letter.php	*/
/*	Location:		./controllers/report/billing_and_payment_notification_letter.php */

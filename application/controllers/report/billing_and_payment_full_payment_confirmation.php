<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class billing_and_payment_full_payment_confirmation extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/billing_and_payment_full_payment_confirmation_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1]; 
			$param['academicYear'] = $temp[0];
			$data['package'] = $this->billing_and_payment_full_payment_confirmation_model->get_paket_fee($param);
         	
			//SERPONG
			$p['RegistrantID'] = '';
			$p['lokasi'] = '15';
			//SIMRPUG
			//$p['lokasi'] = '16'
			$data['new_letter'] = $this->billing_and_payment_full_payment_confirmation_model->generate_new_letter_number($p);
         	
			$this->template->display('report/billing_and_payment_full_payment_confirmation',$data);
        }
		
		public function search()
        {
			$input = (array)$this->input->get();
			
			if($input['txtRegistrantID'] == '')
			{
				$data['message'] = 'RegistrantID must be filled';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			else
			{
				$P['RegistrantID'] = $input['txtRegistrantID'];
				$data['report'] =  $this->billing_and_payment_full_payment_confirmation_model->search($P);
			}
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1]; 
			$param['academicYear'] = $temp[0];
			$data['package'] = $this->billing_and_payment_full_payment_confirmation_model->get_paket_fee($param);
			
			//SERPONG
			$p['RegistrantID'] = $input['txtRegistrantID'];
			$p['lokasi'] = '15';
			//SIMRPUG
			//$p['lokasi'] = '16'
			$data['new_letter'] = $this->billing_and_payment_full_payment_confirmation_model->generate_new_letter_number($p);
         	
			
			$this->template->display('report/billing_and_payment_full_payment_confirmation',$data);
        }
		
		public function generate_report()
		{
			$lg = $this->input->get('lg');
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AcademicYear'] = $temp[0];
			$P['SmtId'] = $temp[1];
			
			//SERPONG
			$param['RegistrantID'] = $input['hfRegistrantID'];
			$param['lokasi'] = '15';
			//SIMRPUG
			//$param['lokasi'] = '16'
			$data['new_letter'] = $this->billing_and_payment_full_payment_confirmation_model->generate_new_letter_number($param);
         	$p['NoSurat'] = $input['txtCode'].$input['txtLetterNumber'];
			
			if($data['new_letter'][0]->NewLetterNumber == $p['NoSurat'])
			{
				//SERPONG
				$p['lokasi'] = '15';
				//SIMPRUG
				//$p['lokasi'] = '16';
			}
			$p['RegistrantId'] = $input['hfRegistrantID'];
			
			$p['AuditUserName'] = $this->session->userdata('UserId');
			$this->billing_and_payment_full_payment_confirmation_model->save_nomor_surat_notifikasi($p);
			
			$par['RegistrantId'] = $input['hfRegistrantID'];
			$data['report'] = $this->billing_and_payment_full_payment_confirmation_model->generate_report($par);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			
			if(isset($input['cbSpecial']))
			{
				if(isset($input['cbDiscount']))
				{
					$Par['PaketId'] = $input['ddlPackage'];
					$data['package'] =  $this->billing_and_payment_full_payment_confirmation_model->get_biaya_paket_fee_diskon($Par);
					if(sizeof($data['package']) <= 0)
					{
						$data['package'] = null;
					}
				}
				else
				{
					$Par['PaketId'] = $input['ddlPackage'];
					$data['package'] =  $this->billing_and_payment_full_payment_confirmation_model->get_biaya_paket_fee($Par);
					if(sizeof($data['package']) <= 0)
					{
						$data['package'] = null;
					}
				}
			}
			
			$d['Style'] = '106';
			$data['currentDate'] =  $this->billing_and_payment_full_payment_confirmation_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['Semester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->billing_and_payment_full_payment_confirmation_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->billing_and_payment_full_payment_confirmation_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			if (isset($data['report']))
			{
				//$this->load->view('report/print_full_payment_confirmation',$data);
				$this->load->library('Pdf');

				/** Starting this part, some part of the code and comment is based on TCPDF example **/
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('Binus International School');
				$pdf->SetTitle('Full Payment Confirmation');
				$pdf->SetSubject('FullPaymentConfirmation');
				$pdf->SetKeywords('FullPaymentConfirmation');

				// set header and footer fonts
				$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

				// set default monospaced font
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

				//set margins
				$pdf->SetHeaderMargin(0);
				$pdf->SetFooterMargin(0);

				// remove default footer
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);

				//set auto page breaks
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				
				//set image scale factor
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

				$pdf->setCellPaddings(0, 0, 0, 0);
				$pdf->setCellMargins(0, 0, 0, 0);
				
				// disable auto-page-break
				//$pdf->SetAutoPageBreak(true, 0);
				
				// add a page
				//$pdf->SetMargins(30, 55, 20);
				//$pdf->AddPage('P', 'A4');

				// set font
				$pdf->SetFont('helvetica', '', 10);
				
				//set text
				$this->load->helper('admission_helper');
				$content = '';
				$content2 = '';
				if(isset($data['report']))
				{
					//klik 22nya
					if (isset($lg[1]))
					{
						if($lg[0]=="id"&& $lg[1]=="en"){
						$checked=' ';
						
						for($i=0; $i<sizeof($lg);$i++){
								$temp1 = $lg[$i].",";
								$checked = $checked.$temp1;
								
						}
						if($lg[0]){
							$pdf->SetMargins(35, 30, 20);
							$pdf->AddPage('P', 'A4');

							$CurrDate = $data['printedDate'][0]->CurrDate;
							$CurrDate = date("d F, Y", strtotime($CurrDate));
							$CurrDate_indo = indonesian_date(strtotime($CurrDate),'j F Y','');
							$content .=
								'<div width="30%" style="text-align:right;">'.
										'Serpong, '.$CurrDate_indo.'<br/>'.
								'</div><br/><br/><br/>';
							$content .= 
								'<div>'.
								'No : '.$_GET['txtCode'].$_GET['txtLetterNumber'].'<br/>'.
								'Subjek : Notifikasi Konfirmasi Pembayaran ('.$data['report'][0]->SchoolLevelName.')'.
								'<br/><br/>';
							
							$txtDueDate = $data['report'][0]->TanggalJatuhTempo;
							if($txtDueDate!=null){
								$txtDueDate = indonesian_date(strtotime($txtDueDate),'j F Y','');
							}		
							$content .=
								'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.' '.$data['report'][0]->MotherName.', <br/>'.
								$data['report'][0]->Address.'<br/>'.
								$data['report'][0]->City.' - '.$data['report'][0]->Country.'<br/>'.
							'<br/>'.
							'<div style="text-align:justify;">'.
								'Terima kasih telah melengkapi prosedur sebagai bagian dari proses admisi ini.<br/>'.
								'Kami ingin memberitahukan bahwa anak Bpk/Ibu, <b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')</b>'.
								', telah diterima sebagai mahasiswa <b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b>'.
								' di BINUS SCHOOL Serpong. Kami telah menerima konfirmasi pembayaran sebagai berikut:'.
							'</div>'.
							'<br/>'.
							'<table cellpadding="3">'.
								'<tr>'.
									'<th style="border: 1px solid black" width="20%" align="center"><b>Tanggal</b></th>'.
									'<th style="border: 1px solid black" width="20%" align="center"><b>Jumlah</b></th>'.
									'<th style="border: 1px solid black" width="60%" align="center"><b>Keterangan</b></th>'.
								'</tr>';
							
							$total = 0;
							foreach ($data['report'] as $Row) :
								$content .= 
									'<tr>'.
										'<td style="border: 1px solid black" align="center">'.$txtDueDate.'</td>'.
										'<td style="border: 1px solid black"> Rp '.moneyFormat($Row->Amount).' &nbsp;</td>'.
										'<td style="border: 1px solid black">Payment for '.$Row->TypeOfPayment.'<br/>'.
										' '.$Row->SchoolLevelName.' - '.$Row->YearLevelName.
										' (AY '.$temp[0].' - '.($temp[0]+1).') </td>'.
									'</tr>';
								$total+=$Row->NetAmount;
							endforeach;
							$content .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center"> <b>Total Pembayaran </b> </td>'.
									'<td style="border: 1px solid black"> <b>Rp '.moneyFormat($total).' &nbsp;</b> </td>'.
									'<td style="border: 1px solid black" align="center"> &nbsp; </td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'<br/>'.
							'Kami siap membantu apabila ada pertanyaan yang ingin ditanyakan.<br/>'.
							'Terima kasih atas perhatian dan kerja samanya.'.
							'<br/>'.
							'<br/>'.
							'Hormat kami,<br/><br/>'.
							'<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>'.
							'<b><u>Pamela Zenitha</u></b><br/>'.
							'Marketing Manager'.
							'</div>';
							$pdf->WriteHTML($content, true, false, true, false, '');
						}//end indo
						if($lg[1])
						{
							$pdf->SetMargins(35, 30, 20);
							$pdf->AddPage('P', 'A4');

							$CurrDate = $data['printedDate'][0]->CurrDate;
							$CurrDate = date("d F, Y", strtotime($CurrDate));
							$content2 .=
								'<div width="30%" style="text-align:right;">'.
										'Serpong, '.$CurrDate.'<br/>'.
								'</div><br/><br/><br/>';
							$content2 .= 					
								'<div>'.
								'No : '.$_GET['txtCode'].$_GET['txtLetterNumber'].'<br/>'.
								'Subject : Notification of Payment Confirmation ('.$data['report'][0]->SchoolLevelName.')'.
								'<br/><br/>';
							
							$txtDueDate = $data['report'][0]->TanggalJatuhTempo;
							if($txtDueDate!=null){
								$txtDueDate = date("F d, Y", strtotime($txtDueDate));
							}							
							$content2 .=
								'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.' '.$data['report'][0]->MotherName.', <br/>'.
								$data['report'][0]->Address.'<br/>'.
								$data['report'][0]->City.' - '.$data['report'][0]->Country.'<br/>'.
							'<br/>'.
							'<div style="text-align:justify;">'.
								'Thank you very much for completing procedure as part of the admission process.<br/>'.
								'It is official to announce that your child, <b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')</b>'.
								', has been accepted as a student in <b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b>'.
								' at BINUS SCHOOL Serpong. We have received your payment confirmation as follow:'.
							'</div>'.
							'<br/>'.
							'<table cellpadding="3">'.
								'<tr>'.
									'<th style="border: 1px solid black" width="20%" align="center"><b>Date</b></th>'.
									'<th style="border: 1px solid black" width="20%" align="center"><b>Amount</b></th>'.
									'<th style="border: 1px solid black" width="60%" align="center"><b>Remarks</b></th>'.
								'</tr>';
							
							$total = 0;
							foreach ($data['report'] as $Row) :
								$content2 .= 
									'<tr>'.
										'<td style="border: 1px solid black" align="center">'.$txtDueDate.'</td>'.
										'<td style="border: 1px solid black"> Rp '.moneyFormat($Row->Amount).' &nbsp;</td>'.
										'<td style="border: 1px solid black">Payment for '.$Row->TypeOfPayment.'<br/>'.
										' '.$Row->SchoolLevelName.' - '.$Row->YearLevelName.
										' (AY '.$temp[0].' - '.($temp[0]+1).') </td>'.
									'</tr>';
								$total+=$Row->NetAmount;
							endforeach;
							$content2 .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center"> <b>Total of Payment </b> </td>'.
									'<td style="border: 1px solid black"> <b>Rp '.moneyFormat($total).' &nbsp;</b> </td>'.
									'<td style="border: 1px solid black" align="center"> &nbsp; </td>'.
								'</tr>'.
							'</table>'.
							'<br/>'.
							'<br/>'.
							'<span style="text-align:justify;">You\'re most welcome to contact us at any time if you have any points to clarify or questions or inquire.</span><br/>'.
							'Thank you for your kind attention and co-operation.'.
							'<br/>'.
							'<br/>'.
							'Yours Sincerely,<br/><br/>'.
							'<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>'.
							'<b><u>Pamela Zenitha</u></b><br/>'.
							'Marketing Manager'.
							'</div>';
							
							$pdf->WriteHTML($content2, true, false, true, false, '');
							}//end eng
						} 
					}


					else if($lg[0]=='en'){//english
						$pdf->SetMargins(35, 30, 20);
						$pdf->AddPage('P', 'A4');
						$CurrDate = $data['printedDate'][0]->CurrDate;
						$CurrDate = date("d F, Y", strtotime($CurrDate));
						$content .=
								'<div width="30%" style="text-align:right;">'.
										'Serpong, '.$CurrDate.'<br/>'.
								'</div><br/><br/><br/>';
						$content .= 					
								'<div>'.
								'No : '.$_GET['txtCode'].$_GET['txtLetterNumber'].'<br/>'.
								'Subject : Notification of Payment Confirmation ('.$data['report'][0]->SchoolLevelName.')'.
								'<br/><br/>';
						$txtDueDate = $data['report'][0]->TanggalJatuhTempo;
						if($txtDueDate!=null){
							$txtDueDate = date("F d, Y", strtotime($txtDueDate));
						}
						$content .=
							'Dear Mr. & Mrs. '.$data['report'][0]->FatherName.' '.$data['report'][0]->MotherName.', <br/>'.
							$data['report'][0]->Address.'<br/>'.
							$data['report'][0]->City.' - '.$data['report'][0]->Country.'<br/>'.
						'<br/>'.
						'<div style="text-align:justify">'.
							'Thank you very much for completing procedure as part of the admission process.<br/>'.
							'It is official to announce that your child, <b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')</b>'.
							', has been accepted as a student in <b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b>'.
							' at BINUS SCHOOL Serpong. We have received your payment confirmation as follow :'.
						'</div>'.
						'<br/>'.
						'<table cellpadding="3">'.
							'<tr>'.
								'<th style="border: 1px solid black" width="20%" align="center"><b>Date</b></th>'.
								'<th style="border: 1px solid black" width="20%" align="center"><b>Amount</b></th>'.
								'<th style="border: 1px solid black" width="60%" align="center"><b>Remarks</b></th>'.
							'</tr>';
						
						$total = 0;
						foreach ($data['report'] as $Row) :
							$content .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center">'.$txtDueDate.'</td>'.
									'<td style="border: 1px solid black"> Rp '.moneyFormat($Row->Amount).' &nbsp;</td>'.
									'<td style="border: 1px solid black;">Payment for '.$Row->TypeOfPayment.'<br/>'.
									' '.$Row->SchoolLevelName.' - '.$Row->YearLevelName.
									' (AY '.$temp[0].' - '.($temp[0]+1).') </td>'.
								'</tr>';
							$total+=$Row->NetAmount;
						endforeach;
						$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center"> <b>Total of Payment </b> </td>'.
								'<td style="border: 1px solid black"> <b>Rp '.moneyFormat($total).' &nbsp;</b> </td>'.
								'<td style="border: 1px solid black" align="center"> &nbsp; </td>'.
							'</tr>'.
						'</table>'.
						'<br/>'.
						'<br/>'.
						'<span style="text-align:jusitfy">You\'re most welcome to contact us at any time if you have any points to clarify or questions or inquire.</span><br/>'.
						'Thank you for your kind attention and co-operation.'.
						'<br/>'.
						'<br/>'.
						'Yours Sincerely,<br/><br/>'.
						'<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>'.
						'<b><u>Pamela Zenitha</u></b><br/>'.
						'Marketing Manager'.
						'</div>';
						$pdf->WriteHTML($content, true, false, true, false, '');
					}//end english

					else if($lg[0]=="id"){//indo
						$pdf->SetMargins(35, 30, 20);
						$pdf->AddPage('P', 'A4');
						$CurrDate = $data['printedDate'][0]->CurrDate;
						$CurrDate = date("d F, Y", strtotime($CurrDate));
						$CurrDate_indo = indonesian_date(strtotime($CurrDate),'j F Y','');
						$content .=
								'<div width="30%" style="text-align:right;">'.
										'Serpong, '.$CurrDate_indo.'<br/>'.
								'</div><br/><br/><br/>';
						$content .= 
							'<div>'.
							'No : '.$_GET['txtCode'].$_GET['txtLetterNumber'].'<br/>'.
							'Subjek : Notifikasi Konfirmasi Pembayaran ('.$data['report'][0]->SchoolLevelName.')'.
							'<br/><br/>';
						
						$txtDueDate = $data['report'][0]->TanggalJatuhTempo;
						if($txtDueDate!=null){
							$txtDueDate = indonesian_date(strtotime($txtDueDate),'j F Y','');
						}
						$content .=
							'Yth. Bpk/Ibu '.$data['report'][0]->FatherName.' '.$data['report'][0]->MotherName.', <br/>'.
							$data['report'][0]->Address.'<br/>'.
							$data['report'][0]->City.' - '.$data['report'][0]->Country.'<br/>'.
						'<br/>'.
						'<div style="text-align:justify">'.
							'Terima kasih telah melengkapi prosedur sebagai bagian dari proses admisi ini.<br/>'.
							'Kami ingin memberitahukan bahwa anak Bpk/Ibu, <b>'.$data['report'][0]->RegistrantName.' ('.$data['report'][0]->RegistrantID.')</b>'.
							', telah diterima sebagai mahasiswa <b>'.$data['report'][0]->SchoolLevelName.' - '.$data['report'][0]->YearLevelName.'</b>'.
							' di BINUS SCHOOL Serpong. Kami telah menerima konfirmasi pembayaran sebagai berikut:'.
						'</div>'.
						'<br/>'.
						'<table cellpadding="3">'.
							'<tr>'.
								'<th style="border: 1px solid black" width="20%" align="center"><b>Tanggal</b></th>'.
								'<th style="border: 1px solid black" width="20%" align="center"><b>Jumlah</b></th>'.
								'<th style="border: 1px solid black" width="60%" align="center"><b>Keterangan</b></th>'.
							'</tr>';
						
						$total = 0;
						foreach ($data['report'] as $Row) :
							$content .= 
								'<tr>'.
									'<td style="border: 1px solid black" align="center">'.$txtDueDate.'</td>'.
									'<td style="border: 1px solid black"> Rp '.moneyFormat($Row->Amount).' &nbsp;</td>'.
									'<td style="border: 1px solid black">Payment for '.$Row->TypeOfPayment.'<br/>'.
									' '.$Row->SchoolLevelName.' - '.$Row->YearLevelName.
									' (AY '.$temp[0].' - '.($temp[0]+1).') </td>'.
								'</tr>';
							$total+=$Row->NetAmount;
						endforeach;
						$content .= 
							'<tr>'.
								'<td style="border: 1px solid black" align="center"> <b>Total Pembayaran </b> </td>'.
								'<td style="border: 1px solid black"> <b>Rp '.moneyFormat($total).' &nbsp;</b> </td>'.
								'<td style="border: 1px solid black" align="center"> &nbsp; </td>'.
							'</tr>'.
						'</table>'.
						'<br/>'.
						'<br/>'.
						'Kami siap membantu apabila ada pertanyaan yang ingin ditanyakan.<br/>'.
						'Terima kasih atas perhatian dan kerja samanya.'.
						'<br/>'.
						'<br/>'.
						'Hormat,<br/><br/>'.
						'<img src="'.base_url().'resources/images/digital_signature.jpg" width="100px;" /><br/>'.
						'<b><u>Pamela Zenitha</u></b><br/>'.
						'Marketing Manager'.
						'</div>';
						$pdf->WriteHTML($content, true, false, true, false, '');
					}

				}
				//echo $content;
				
				//$pdf->WriteHTML($content, true, false, true, false, '');
				// Close and output PDF document
				$pdf->Output('Full_Payment_Confirmation.pdf','I');
				return true;
			}
			else
			{
				$data['message'] = 'Data not found';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
		}
	}
/*	End	of	file	billing_and_payment_notification_letter.php	*/
/*	Location:		./controllers/report/billing_and_payment_notification_letter.php */

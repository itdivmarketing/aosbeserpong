<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class entrance_test_test_card extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/entrance_test_test_card_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->entrance_test_test_card_model->get_school_level();
         	$this->template->display('report/entrance_test_test_card',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->entrance_test_test_card_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_schedule($SchoolLevel = null, $PretestTerm = null,$postback = false)
		{
			if($SchoolLevel)
			{
				if($PretestTerm)
				{
					$Param['Period'] = $this->period;
					$Param['SchoolLevel'] = $SchoolLevel;
					$Param['AdmissionTerm'] = $PretestTerm;
					$data['Schedule'] = $this->entrance_test_test_card_model->get_schedule($Param);
				}
				else
				{
					$data['Schedule'] = '';
				}
			}
			else
			{
				$data['Schedule'] = '';
			}
			if($postback) return $data['Schedule'];
			else echo json_encode($data);
		}
		
		public function search()
		{
			$input = (array)$this->input->get();
			$P['TermId'] = $input['ddlAdmissionTerm'];
			$p['KdMsJadwalET'] = $input['ddlSchedule'];
			$query = $this->entrance_test_test_card_model->generate_report($p);
			if(sizeof($query) > 0)
			{
				$data['data']['result'] = $query;
			}
			else
			{
				$data['message'] = 'There\'s no Data';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->entrance_test_test_card_model->get_school_level();
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'], true);
			$data['Schedule'] = $this->get_schedule($input['ddlSchoolLevel'],$input['ddlAdmissionTerm'], true);
         	$this->template->display('report/entrance_test_test_card',$data);
		}
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			
			//$P['TermId'] = $input['ddlAdmissionTerm'];
			//$P['KdMsJadwalET'] =  $input['ddlSchedule'];
			$P['RegistrantID'] = '';//$input['txtFormNumber'];
			$flag = $input['cbCheck'];
			foreach($flag as $key=>$value)
			{
				$P['RegistrantID'] .= $value . ',';
			}
			
			$data['report'] =  $this->entrance_test_test_card_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->entrance_test_test_card_model->get_current_date($d);
			$d['Style'] = '106';
			$data['printedDate'] =  $this->entrance_test_test_card_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->entrance_test_test_card_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
				
			if(sizeof($data['report']) <= 0)
			{
				$data['message'] = 'Data not found';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			$this->load->view('report/print_test_card',$data);
		}
	}
/*	End	of	file	entrance_test_test_card.php	*/
/*	Location:		./controllers/report/entrance_test_test_card.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class form_sales_cash_report_to_finance extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/form_sales_cash_report_to_finance_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_sales_cash_report_to_finance_model->get_school_level();
         	$this->template->display('report/form_sales_cash_report_to_finance',$data);
        }
		function convert_to_rupiah($angka)
		{
			return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
		}
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_sales_cash_report_to_finance_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function generate_report()
		{
			$input = (array)$this->input->get();
			$temp = explode("-", $this->period);
			$P['AdmissiONID'] = '';
			$P['AcademicYear'] =  $temp[0];
			$P['SmtId'] = $temp[1];
			$P['SchoolLevelID'] = ($input['ddlSchoolLevel'] == 'all' ? '' : $input['ddlSchoolLevel']);
			$P['TermId'] = ($input['ddlAdmissionTerm'] == 'all' ? '' : $input['ddlAdmissionTerm']);
			$P['TglMl'] = $input['txtStartPeriod'];
			$P['TglSl'] = $input['txtEndPeriod'];
			if($input['rbType'] != 'detail')
			{
				$P['Status'] = '2';
			}
			
			$data['report'] =  $this->form_sales_cash_report_to_finance_model->generate_report($P);
			
			if (sizeof($data['report']) <= 0)
				$data['report'] =null;
			$data['AcademicYear'] =  $temp[0];
			$d['Style'] = '106';
			$data['currentDate'] =  $this->form_sales_cash_report_to_finance_model->get_current_date($d);
			$d['Style'] = '106';
			$data['printedDate'] =  $this->form_sales_cash_report_to_finance_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->form_sales_cash_report_to_finance_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			if(sizeof($data['report']) <= 0)
			{
				$data['message'] = 'Data not found';
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			
			
			if($input['rbType'] == 'detail')
				$this->load->view('report/print_cash_report_to_finance_detail',$data);
			else
				$this->load->view('report/print_cash_report_to_finance_summary',$data);
		}
	}
/*	End	of	file	cash_report_to_finance.php	*/
/*	Location:		./controllers/report/form_sales_cash_report_to_finance.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class billing_and_payment_receipt extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("report/billing_and_payment_receipt_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
         	$this->template->display('report/billing_and_payment_receipt',$data);
        }
		
		public function generate_report()
		{
			$temp = explode("-", $this->period);
			$input = (array)$this->input->get();
			$P['RegistrantID'] = $input['txtRegistrantID'];
			$data['report'] =  $this->billing_and_payment_receipt_model->generate_report($P);
			if(sizeof($data['report']) <= 0)
			{
				$data['report'] = null;
			}
			$d['Style'] = '106';
			$data['currentDate'] =  $this->billing_and_payment_receipt_model->get_current_date($d);
			$data['AcademicYear'] = $temp[0];
			$data['Semester'] = $temp[1];
			$d['Style'] = '106';
			$data['printedDate'] =  $this->billing_and_payment_receipt_model->get_current_date($d);
			$d['Style'] = '108';
			$data['printedTime'] =  $this->billing_and_payment_receipt_model->get_current_date($d);
			$data['UserID'] =  $this->session->userdata('UserId').' - '.$this->session->userdata('Name') ;
			
			$this->load->view('report/print_receipt',$data);
		}
	}
/*	End	of	file	billing_and_payment_receipt.php	*/
/*	Location:		./controllers/report/billing_and_payment_receipt.php */

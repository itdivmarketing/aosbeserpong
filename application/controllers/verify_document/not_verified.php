<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class not_verified extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entrance_test/registrant_allocation_model");
			$this->load->model("entry/form_sales_model");
			$this->load->model("verify_document/verify_document_model");
			$this->load->model("entry/form_return_document_model");
			$this->load->config('app_config');
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$d = $this->get_verify_document();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->form_sales_model->get_school_level();
			$data['YearLevel'] = $this->form_sales_model->get_year_level();
         	$this->template->display('verify_document/not_verified',$data);
        }
		
	
		
		public function get_submission_data($FormNumber,$postback = false)
		{
			if($FormNumber){
				$Param['RegistrantID']=$FormNumber;
				$Param['AdmissionProcessID']=1;
				$data['SubmissionData'] = $this->form_return_document_model->get_registrant_document_submission_data($Param);
			}
			if($postback) return $data['SubmissionData'];
			else echo json_encode($data);
		}
	
		public function get_view_verify_document($RegistrantID, $postback = false)
		{
			$Param['RegistrantID'] = $RegistrantID;
			$Param['Status'] = 0;
			$data['VerifyDocumentView'] = $this->verify_document_model->get_view_verify_document($Param);
			if($postback) return $data['VerifyDocumentView'];
			else echo json_encode($data);
		}

		public function save_verify_document()
		{
			
			$input = $this->input->post();
			$this->load->library('form_validation');
			$this->form_validation->set_rules("hfViewFormNumber","Form Number","required");
			$this->form_validation->set_rules("hfDocumentID","Document ID","required");

			
			if($this->form_validation->run() != false)
			{	
				
				$RegistrantID = trim($input['hfViewFormNumber']);
				//$DocumentID = trim($input['hfFormNumber']);
				
				$this->db->trans_begin();
				
				
				$Param['RegistrantID']=trim($RegistrantID);
				//$Param['AdmissionProcessID']=(int)$input['hfAdmissionProcess'];
				/*
				$i=0;
				foreach($input["hfDocumentID"] as $id=>$fieldvalue){
					
					$rbtStatus='rbtStatus-'.$i;
					if(!isset($input[$rbtStatus])){
						//$this->index();
						print_r($rbtStatus);
						die;
					}
					$i++;
				}
				*/
				$i=0;
				foreach($input["hfDocumentID"] as $id=>$fieldvalue){
					$rbtStatus='rbtStatus-'.$i;
					
					
					
					$Array =array( 
						'RegistrantID' => $RegistrantID, 
						'DocumentID' => $input['hfDocumentID'][$id], 
						'FileName' => NULL,
						'FileSize' => NULL,
						'Verification' => isset($input[$rbtStatus])?$input[$rbtStatus]:null,
						'Note' => trim($input['txtNote'][$id]),
						'ReceivedBy' => $this->session->userdata('UserId'),
						'DueDate' => NULL,
						'AdmissionProcessID' =>  1,
						'AuditUserName' =>$this->session->userdata('UserId')
					);
					
						$this->form_return_document_model->insert_registrant_document_submission_data($Array);
					$i++;
				}
				

				

				
				if ( $this->db->trans_status() === FALSE  ) {
				   $this->db->trans_rollback();
				   $data['status'] = 'failed';
					$data['message'] = "failed save to database" ;
				} else {    
				   $this->db->trans_commit();    
				   $data['status'] = 'success';
					$data['message'] = "success save to database" ;
					if(isset($input['cbxViewIsEmail']))
					 $this->send_email($RegistrantID);
					//die;
				}
				
				
				
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			
			$this->index();
		}
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->form_sales_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->form_sales_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		public function send_email($RegistrantID=""){
			// send email to the user

			if($RegistrantID==""||empty($RegistrantID)||$RegistrantID==NULL)
				return;	
			// send email to the user
			$ParamEmail['NotificationTypeID']='003';
			$ParamEmail['RegistrantID']=$RegistrantID;
			$ParamEmail['AdmissionProcessID']=1;

			$EmailDetail=$this->verify_document_model->get_document_upload_email_content($ParamEmail);

			$this->load->library('email');

			$this->config->load('app_config');
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = $this->config->item('mail_host');
			$config['smtp_user'] = $this->config->item('mail_username');
			$config['smtp_pass'] = $this->config->item('mail_password');
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from('no-reply-admission@binus.ac.id', 'Admisi Binus School');
			$this->email->to($EmailDetail->EmailTo);
			$cc=explode(";",$EmailDetail->EmailCC);
			$bcc=explode(";",$EmailDetail->EmailBCC);
		
			$this->email->cc($cc);
			$this->email->bcc($bcc);
		
			$this->email->subject($EmailDetail->EmailSubject);

			$mail_body = $EmailDetail->EmailBody;

			$this->email->message($mail_body);
			$this->email->send();
		
		
		}
		
		public function get_verify_document($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			//print_r($input['hfPage']);die;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			
			$table['f'] = array(
				'Period'		=> $this->period,
				'RegistrantID'  => isset($input['txtFormNumber']) ? null_setter($input['txtFormNumber']) : null,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'YearLevel'	=> isset($input['ddlYearLevel']) ? null_setter($input['ddlYearLevel']) : null,
				'AdmissionTerm'	=> isset($input['ddlAdmissionTerm']) ? null_setter($input['ddlAdmissionTerm']) : null,
				'Name'	=> isset($input['txtStudentName']) ? null_setter($input['txtStudentName']) : null,
				'Status' => 0
			);
			$query = $this->verify_document_model->get_verify_document(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->verify_document_model->get_verify_document(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_verify_document($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['SchoolLevel'] = $this->registrant_allocation_model->get_school_level();
			$data['YearLevel'] = $this->get_year_level($input['ddlSchoolLevel'],true);
			

			
         	$this->template->display('verify_document/not_verified',$data);
		}
	}
/*	End	of	file	not_verified.php	*/
/*	Location:		./controllers/entry/not_verified.php */

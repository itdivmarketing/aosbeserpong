<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Main extends CI_Controller {

		function __construct() {
			parent::__construct();
			$this->output->nocache();
		}

	
		public function index()
        {
         	//Login Page
			$data['errorMessage'] = '';
         	$this->load->view('login', $data);
        }	
	}
/*	End	of	file	main.php	*/
/*	Location:		./controllers/main.php */

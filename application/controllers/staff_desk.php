<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Staff_desk extends CI_Controller {
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/user_mapping_model");
			$this->load->model("user_management/role_management_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
		}
		
		public function index()
        {
         	$this->template->display('staff_desk');
        }	
	}
/*	End	of	file	staff_desk.php	*/
/*	Location:		./controllers/staff_desk.php */

<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class entry_score extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entry/form_return_front_office_model");
			$this->load->model("entry/registrant_background_model");
			$this->load->model("entrance_test/entry_score_model");
			$this->load->model("entry/parent_profile_model");
			$this->load->model("entry/phone_model");
			$this->load->model("entry/form_return_back_office_model");
			$this->load->model("entry/student_profile_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			
		}
		public function index()
        {
			$this->search("");
        }
		
		

		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->entry_score_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_year_level($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['YearLevel'] = $this->entry_score_model->get_year_level($Param);
			}
			else
			{
				$data['YearLevel'] = '';
			}
			if($postback) return $data['YearLevel'];
			else echo json_encode($data);
		}
		
		

		public function save_subject_data(){
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
			

			if($this->form_validation->run() != false)
			{	
				$RegistrantID =  $input["hfFormNumber"];
				if($input){
					$this->db->trans_begin();
		
					
					$i=1;
					if(isset($input["hfSubjectID"])){
						foreach($input["hfSubjectID"] as $id=>$fieldvalue){
							
								$PreTestSubjectID = trim($input["hfSubjectID"][$id]);
								$Nilai = trim($input["txtSubjectScore"][$id]);
								$NilaiBeforeReTest = trim($input["txtBeforeReTestSubjectScore"][$id]);
								$Notes = trim($input["txtSubjectNote"][$id]);
								$ResultET = trim($input["ddlSubjectResultET"][$id]);
								$StatusLulus = isset($input["cbxSubjectStatusPass"][$id])?'1':'0';
								
									
								$query =array(
									'RegistrantID' => trim($RegistrantID),  
									'PreTestSubjectID' => (int)$PreTestSubjectID,
									'Nilai' => $Nilai,
									'NilaiBeforeReTest' => $NilaiBeforeReTest,
									'Notes' =>  $Notes,
									'ResultET' => (int)$ResultET,
									'StatusLulus' => $StatusLulus,
									'AuditUserName' => $this->session->userdata('UserId')

								);
								
								$i++;
									//print_r($query);die;
								$this->entry_score_model->save_entrance_test_score($query);
						}
					}
					//die;
					if(isset($input["ddlSubjectFinalScore"])){
									
						$Param['RegistrantID']=trim($RegistrantID);
						$Param['AuditUserName']=$this->session->userdata('UserId');
						$this->entry_score_model->delete_final_score($Param);
						
						if($input["ddlSubjectFinalScore"]!=""){
						
							$query=array(
							'RegistrantID' => trim($RegistrantID),  
							'StatusLulus' => $input["ddlSubjectFinalScore"],
							'AuditUserName' => $this->session->userdata('UserId')
							);
							
							$this->entry_score_model->save_final_score($query);
						}
					}

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
					
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}

		
		public function save_interview_data(){
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
			

			if($this->form_validation->run() != false)
			{	
				$RegistrantID =  $input["hfFormNumber"];
				if($input){
					$this->db->trans_begin();
				
						$NilaiP3A = trim($input["txtP3AScore"]);
						$NotesP3A = trim($input["txtP3ANote"]);
						$ResultETP3A = trim($input["ddlP3AResultET"]);
						$StatusLulusP3A = isset($input["cbxP3AStatusPass"])?'1':'0';
						$query =array(
							'RegistrantID' => trim($RegistrantID),  
							'NilaiP3A' => (int)$NilaiP3A,
							'NotesP3A' =>  $NotesP3A,
							'ResultETP3A' =>  (int)$ResultETP3A,
							'StatusLulusP3A' => $StatusLulusP3A,
							'AuditUserName' => $this->session->userdata('UserId')

						);
					
						$this->entry_score_model->save_interview_score($query);
						

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
					
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}
		
		public function save_UD_data(){
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("hfFormNumber","Form Number","required");
			$this->form_validation->set_rules("ddlUDSchoolLevel","School Level","required");
			$this->form_validation->set_rules("ddlUDAdmissionTerm","Admission Term","required");
			$this->form_validation->set_rules("ddlUDYearLevel","Year Level","required");
			
			if($this->form_validation->run() != false)
			{	
				$RegistrantID =  $input["hfFormNumber"];
				$RegistrantIDDU = $input["formNoTxt"];
				if($input){
					$this->db->trans_begin();
						
						$SchoolLevelID = trim($input["ddlUDSchoolLevel"]);
						$YearLevelID = trim($input["ddlUDYearLevel"]);
						$TermID = trim($input["ddlUDAdmissionTerm"]);
						$Notes = $input["txtUDNote"];
						$query =array(
							'RegistrantID' => trim($RegistrantID),  
							'SchoolLevelID' => $SchoolLevelID,
							'YearLevelID' =>  (int)$YearLevelID,
							'TermID' => $TermID,
							'RegistrantIDDU' => $RegistrantIDDU,
							'Notes' => trim($Notes),
							'AuditUserName' => $this->session->userdata('UserId')

						);
						
						$this->entry_score_model->save_UD($query);
						

					if ( $this->db->trans_status() === FALSE  ) {
						$this->db->trans_rollback();
						$data['status'] = 'failed';
						$data['message'] = "failed save to database" ;
					} else {    
						$this->db->trans_commit();    
						$data['status'] = 'success';
						$data['message'] = "success save to database" ;

					}
				}
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				
			}
					
			echo "<script type='text/javascript'>alert('".$data['message']."');</script>";	
			$this->search(trim($input['hfFormNumber']));
		}
		
		public function search($FormParam="blank")
        {	

			$input = (array)$this->input->get();
			$FormNumber="";
			
			if($FormParam!="blank"){
				$FormNumber=$FormParam;
			}
				
			else
				$FormNumber=$input['txtFormNumber'];
			
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			
			
			$data['FormNumber']=$FormNumber;
			
			$data['SchoolLevel'] = $this->entry_score_model->get_school_level();
			
			
			$Param['RegistrantID']=$FormNumber;

			$data['EntryScoreData'] = $this->entry_score_model->get_entry_score_data($Param);

			if($FormNumber!="" && $data['EntryScoreData']!=null){
				// $data['RegistrantIdDU'] = $data['EntryScoreData'][0]->RegistrantIdDU;
			 //print_r($data['EntryScoreData']); die;
			if($data['EntryScoreData'][0]->RegistrantIdDU == ''){
				// echo "gak ada"; die;
				$d['admissionid'] = trim($data['EntryScoreData'][0]->AdmissionID);
				
				$data['formNo'] = $this->entry_score_model->get_form_no($d);
				// print_r($data['formNo']); die;
			}else{
				// echo "ada"; die;
				$d['admissionid'] = $data['EntryScoreData'][0]->RegistrantIdDU;
				
				$data['formNo']->formno = $d['admissionid'];

			}


			$EntryScoreData=$data['EntryScoreData'];


			
			$data['YearLevel'] = $this->get_year_level(isset($EntryScoreData[0]->SchoolLevelIdDU)?$EntryScoreData[0]->SchoolLevelIdDU:"",true);
			$data['PretestTerm'] = $this->get_pretest_term(isset($EntryScoreData[0]->SchoolLevelIdDU)?$EntryScoreData[0]->SchoolLevelIdDU:"",true);
			
			
		
			$data['FinalScoreStatusList'] = $this->entry_score_model->get_final_score_status_list();
			$data['ResultETList'] = $this->entry_score_model->get_result_ET_list();
	

			$data['StudentValidity']=0;
			if(isset($data['EntryScoreData'] )&&Count($data['EntryScoreData'] )>0)
				$data['StudentValidity']=1;
			}
			

			// echo json_encode(trim($data['formNo'][0]->formno)); die;
         	$this->template->display('entrance_test/entry_score',$data);
		}
	}
	
	
/*	End	of	file	entry_score.php	*/
/*	Location:		./controllers/entrance_test/entry_score.php */

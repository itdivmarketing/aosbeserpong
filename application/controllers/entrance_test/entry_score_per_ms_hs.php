<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Entry_score_per_ms_hs extends CI_Controller {
		
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entrance_test/entry_score_per_ms_hs_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->entry_score_per_ms_hs_model->get_school_level();
         	$this->template->display('entrance_test/entry_score_per_ms_hs',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->entry_score_per_ms_hs_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_schedule($SchoolLevel = null, $PretestTerm = null,$postback = false)
		{
			if($SchoolLevel)
			{
				if($PretestTerm)
				{
					$Param['Period'] = $this->period;
					$Param['SchoolLevel'] = $SchoolLevel;
					$Param['AdmissionTerm'] = $PretestTerm;
					$data['Schedule'] = $this->entry_score_per_ms_hs_model->get_schedule($Param);
				}
				else
				{
					$data['Schedule'] = '';
				}
			}
			else
			{
				$data['Schedule'] = '';
			}
			if($postback) return $data['Schedule'];
			else echo json_encode($data);
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['Schedule'] = $this->get_schedule($input['ddlSchoolLevel'],$input['ddlAdmissionTerm'],true);
			$data['SchoolLevel'] = $this->entry_score_per_ms_hs_model->get_school_level();
			$Param['Period'] = $this->period;
			$Param['SchoolLevel'] = $input['ddlSchoolLevel'];
			$Param['AdmissionTerm'] = $input['ddlAdmissionTerm'];
			$Param['ScheduleID'] = $input['ddlSchedule'];
			$ScheduleID = $this->entry_score_per_ms_hs_model->get_schedule($Param);
			if($ScheduleID)
			{
				$data['hfSchedule'] = $ScheduleID[0]->ScheduleID;
				$data['txtVenue'] = $ScheduleID[0]->Venue;
				$data['txtDate'] = $ScheduleID[0]->StartDate.' - '.$ScheduleID[0]->EndDate;
				$data['txtTime'] = $ScheduleID[0]->StartTime.' - '.$ScheduleID[0]->EndDate;
			}
         	$this->template->display('entrance_test/entry_score_per_ms_hs',$data);
		}
		
		public function upload()
		{
			$data = array();
			$allowed_ext = array('xlsx');
			$file = $_FILES['file'];
			$this->config->load('app_config');
			
			if($file['error'] == 0 && is_uploaded_file($file['tmp_name']))
			{
				$orig_name = preg_replace('/[^a-zA-Z0-9\.-_]/si', '', $file['name']);

				$final_path = $this->config->item('score_ms_hs_path');
				
				$extension = explode('.', $orig_name);
				$extension = strtolower($extension[count($extension) - 1]); 
			
				if(in_array($extension, $allowed_ext))
				{
					if($file['name'])
					{
						//getDB date
						$d['Style'] = 120;
						$dt = $this->entry_score_per_ms_hs_model->get_current_date($d);
						list($datehour, $min, $sec) = explode(":", $dt[0]->CurrDate);
						list($date, $hour) = explode(" ", $datehour);
						list($year,$month,$date) = explode ("-",$date);
						
						$md5name = md5($file['name']);
						$temp_path = $final_path . $md5name;
						@move_uploaded_file($file['tmp_name'], $temp_path );
						$final_filename = 'Score_'.$this->session->userdata('UserId').'_'.$year.$month.$date.$hour.$min.$sec.'.'.$extension;
						$data['status'] = true;
						$file_size = filesize($temp_path);
						rename($temp_path, $final_path . $final_filename);
						
				
					}
				}
				else
				{
					$data['message'] = 'file should be in .xlsx extension';
					$data['status'] = false;
				}
			}
			else
			{
				$data['message'] = 'file should be in .xlsx extension';
				$data['status'] = false;
			}
			echo json_encode($data);
		}
		
		public function set_score()
		{
			$input = json_decode(file_get_contents("php://input"),true);

			if(!array_key_exists('TemplateScore', $input))
			{
				$data['status'] = false;
				$data['message'] = 'No Data';
				echo json_encode($data);
				return;
			}
			
			if($input['TemplateScore'] == null){
				$data['status'] = false;
				$data['message'] = 'No Data';
				echo json_encode($data);
				return;
			};

			$data['total'] = 0;
			$this->db->trans_begin();
			if ($input['TemplateScore'])
			{
				$flag = false;
				foreach($input['TemplateScore'] as $dt)
				{
					$d = array();
					$da = array();
					$dat = array();
					$d['AuditUserName'] = $this->session->userdata('UserId');
					
					if(array_key_exists('PretestSubject', $dt) && is_int($dt['PretestSubject']) && trim($dt['PretestSubject'])!='')
					{
						if((int)$dt['PretestSubject'] < 0 || (int)$dt['PretestSubject'] > 9)
						{
							$this->db->trans_rollback();
							$data['status'] = false;
							$data['message'] = 'error on line : '.($data['total']+1).' invalid PretestSubject';
							echo json_encode($data);
							return;
						}
					}
					else
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '.($data['total']+1).' invalid PretestSubject';
						echo json_encode($data);
						return;
					}
					
					$tempScore;
					
					if(array_key_exists('Score', $dt) && trim($dt['Score'])!='')
					{
						$tempScore = $dt['Score'];
						if (strpos($tempScore,',') !== false) {
							$Score = explode(',',$tempScore);
							$tempScore = $Score[0].'.'.$Score[1];
						}
						
						if(!is_numeric($tempScore) || ((float)$tempScore < 0 || (float)$tempScore > 100))
						{
							$this->db->trans_rollback();
							$data['status'] = false;
							$data['message'] = 'error on line : '.($data['total']+1).' invalid Score';
							echo json_encode($data);
							return;
						}
					}
					else
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '.($data['total']+1).' invalid Score';
						echo json_encode($data);
						return;
					}

					if(array_key_exists('ResultEntranceTest', $dt) && is_int($dt['ResultEntranceTest']) && trim($dt['ResultEntranceTest'])!='')
					{
						if((int)$dt['ResultEntranceTest'] < 1 || (int)$dt['ResultEntranceTest'] > 6)
						{
							$this->db->trans_rollback();
							$data['status'] = false;
							$data['message'] = 'error on line : '.($data['total']+1).' invalid ResultEntranceTest';
							echo json_encode($data);
							return;
						}
					}
					else
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '.($data['total']+1).' invalid ResultEntranceTest';
						echo json_encode($data);
						return;
					}

					if(array_key_exists('Status', $dt) && is_int($dt['Status']) && trim($dt['Status'])!='')
					{
						if((int)$dt['Status'] < 1 || (int)$dt['Status'] > 3)
						{
							$this->db->trans_rollback();
							$data['status'] = false;
							$data['message'] = 'error on line : '.($data['total']+1).' invalid Status';
							echo json_encode($data);
							return;
						}
					}
					else
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '.($data['total']+1).' invalid Status';
						echo json_encode($data);
						return;
					}

					if ($dt['PretestSubject'] == '1') //interview
					{

						$da['AuditUserName'] = $this->session->userdata('UserId');
						if(isset($dt['RegistrantID'])) $da['RegistrantID'] = $dt['RegistrantID'];
						if(isset($tempScore)) $da['NilaiP3A'] = $tempScore; else $da['NilaiP3A'] = '0';
						if(isset($dt['ResultEntranceTest'])) $da['ResultETP3A'] = $dt['ResultEntranceTest'];
						if(isset($dt['Notes'])) $da['NotesP3A'] = $dt['Notes'];
						$this->entry_score_per_ms_hs_model->set_score_interview($da);
					}
					else 
					{
						$d['AuditUserName'] = $this->session->userdata('UserId');
						if(isset($dt['RegistrantID'])) $d['RegistrantID'] = $dt['RegistrantID'];
						$d['PreTestSubjectId'] = $dt['PretestSubject'];
						if(isset($tempScore)) $d['Nilai'] = $tempScore; else $da['Nilai'] = '0';
						$d['NilaiBeforeReTest'] = '0';
						$d['Notes'] = '';
						if(isset($dt['Notes'])){
							$d['Notes'] = $dt['Notes'];
						}
					
						if(isset($dt['ResultEntranceTest'])) $d['ResultET'] = $dt['ResultEntranceTest'];
						$this->entry_score_per_ms_hs_model->set_score($d);
					}
					
					/*if ( $this->db->trans_status() === FALSE  ) 
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '+$data['total']+1;
					}*/
					
					if(!isset($dt['RegistrantID']) || is_int($dt['RegistrantID']) == false)
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '.($data['total']+1).' invalid RegistrantID';
						echo json_encode($data);
						return;
					}
					
					
					$dat['AuditUserName'] = $this->session->userdata('UserId');
					if(isset($dt['RegistrantID'])) $dat['RegistrantID'] = $dt['RegistrantID'];
					if(isset($dt['Status'])) $dat['StatusLulus'] = $dt['Status'];
					$this->entry_score_per_ms_hs_model->set_status($dat);
					/*if ( $this->db->trans_status() === FALSE  ) 
					{
						$this->db->trans_rollback();
						$data['status'] = false;
						$data['message'] = 'error on line : '+$data['total']+1;
					}*/
					$data['total'] = $data['total'] + 1;
				}
				
				if ( $this->db->trans_status() === FALSE  ) {
					$this->db->trans_rollback();
					$data['status'] = false;
					$data['message'] = 'error on line : '+$data['total']+1;
				} else {    
					$this->db->trans_commit();
					$data['status'] = true;
				}
			}
			else
			{
				$this->db->trans_rollback();
				$data['status'] = false;
				$data['message'] = 'file format is not appropriate. make sure you use a file that can be downloaded from the link above.';
			}
			
			echo json_encode($data);
		}
	}
/*	End	of	file	entry_score_per_ms_hs.php	*/
/*	Location:		./controllers/entrance_test/entry_score_per_ms_hs.php */

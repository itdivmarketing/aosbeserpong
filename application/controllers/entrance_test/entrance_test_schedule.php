<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Entrance_test_schedule extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entrance_test/entrance_test_schedule_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			$d = $this->get_entrance_test_schedule_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestSubject'] = $this->entrance_test_schedule_model->get_pre_test_subject();
			$data['Venue'] = $this->entrance_test_schedule_model->get_venue();
			$data['SchoolLevel'] = $this->entrance_test_schedule_model->get_school_level();
			$data['message'] = $this->session->flashdata('message');
         	$this->template->display('entrance_test/entrance_test_schedule',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->entrance_test_schedule_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		
		public function get_admission_id($PretestTerm = null,$postback = false)
		{
			if($PretestTerm)
			{
				$Param['TermID'] = $PretestTerm;
				$data['AdmissionID'] = $this->entrance_test_schedule_model->get_admission_id($Param);
			}
			else
			{
				$data['AdmissionID'] = '';
			}
			
			if($postback) return $data['AdmissionID'];
			else echo json_encode($data);
		}
		
		public function get_entrance_test_schedule($EntranceTestID = null,$postback = false)
		{
			$Param['KdMsJadwalET'] = $EntranceTestID;
			$data['EntranceTestSchedule'] = $this->entrance_test_schedule_model->get_entrance_test_schedule($Param);
			$data['PreTestSubject'] = $this->entrance_test_schedule_model->get_subject_entrance_test($Param);
			
			if($postback) return $data;
			else echo json_encode($data);
		}
		
		public function set_entrance_test_schedule()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			/*if($input['hfStatus'] != 'delete')
			{
				if($input['hfStatus'] == 'update')
				{
					$this->form_validation->set_rules("hfEntranceTestID","Entrance Test ID","required");
					$this->form_validation->set_rules("hfSchoolLevel","School Level","required");
					$this->form_validation->set_rules("hfAdmissionTerm","Admission Term","required");
				}
				else
				{
					$this->form_validation->set_rules("ddlSchoolLevel","School Level","required");
					$this->form_validation->set_rules("ddlAdmissionTerm","Admission Term","required");
				}
				$this->form_validation->set_rules("txtAdmissionID","Admission ID","required");
				$this->form_validation->set_rules("ddlVenue","Venue","required");
				$this->form_validation->set_rules("txtStartDate","Start Date","required");
				$this->form_validation->set_rules("txtEndDate","End Date","required");
				$this->form_validation->set_rules("txtStartTime","Start Time","required");
				$this->form_validation->set_rules("txtEndTime","End Time","required");
				if(isset($input['cbInterview']))
				{
					$this->form_validation->set_rules("ddlVenueInterview","Interview Venue","required");
					$this->form_validation->set_rules("txtDate","Interview Date","required");
					$this->form_validation->set_rules("txtStartTimeInterview","Interview Start Time","required");
					$this->form_validation->set_rules("txtEndTimeInterview","Interview End Time","required");
				}
				else
					$this->form_validation->set_rules("cbPreTestSubject","Pre Test Subject","required");
				
				
			}
			else
			{
				$this->form_validation->set_rules("hfEntranceTestID","Entrance Test ID","required");
			}
			
			if($this->form_validation->run() != false)
			{*/	
				$d = array();
				if($input['hfStatus'] != 'delete')
				{
					if($input['hfStatus'] == 'update')
					{
						$d['AdmissionTerm'] = $input['hfAdmissionTerm'];
					}
					else
					{
						$d['AdmissionTerm'] = $input['ddlAdmissionTerm'];
					}

					$d['Venue'] = $input['ddlVenue'];
					$d['StartDate'] = sql_srv_date($input['txtStartDate']);
					$d['EndDate'] = sql_srv_date($input['txtEndDate']);
					$d['StartTime'] = $input['txtStartTime'];
					$d['EndTime'] = $input['txtEndTime'];
					//edit by Wiedy 8 Agustus 2016, logic yang lama aneh
					// $d['VenueInterview'] = null_setter($input['ddlVenueInterview']);
					// $d['Date'] = null_setter(sql_srv_date($input['txtDate']));
					// $d['StartTimeInterview'] =null_setter($input['txtStartTimeInterview']);
					// $d['EndTimeInterview'] = null_setter($input['txtEndTimeInterview']);
					// $d['PretestSubjectId'] = '';
					// if(!isset($input['cbInterview']))
					// {
					// 	$flag = $this->input->post('cbPreTestSubject'); 
					// 	foreach($flag as $key=>$value)
					// 	{
					// 		$d['PretestSubjectId'] .= $value . ',';
					// 	}
					// }
					$d['VenueInterview'] = null_setter($input['ddlVenueInterview']);
					$d['Date'] = null_setter(sql_srv_date($input['txtDate']));
					$d['StartTimeInterview'] =null_setter($input['txtStartTimeInterview']);
					$d['EndTimeInterview'] = null_setter($input['txtEndTimeInterview']);
					
					if(!isset($input['cbInterview'])){
						$d['VenueInterview'] = null;
						$d['Date'] = null;
						$d['StartTimeInterview'] =null;
						$d['EndTimeInterview'] = null;
					}

					$d['PretestSubjectId'] = '';

					$flag = $this->input->post('cbPreTestSubject'); 
					foreach($flag as $key=>$value)
					{
							$d['PretestSubjectId'] .= $value . ',';
					}
					$d['KdMsJadwalET'] = null_setter($input['hfEntranceTestID']);
					$d['AuditUserName'] = $this->session->userdata('UserId');
					$d['Delete'] = '0';
				}
				else
				{
					$param['KdMsJadwalET'] = $input['hfEntranceTestID'];
					$checked = $this->entrance_test_schedule_model->check_exist_registrant($param);
					if($checked[0]->Message == 'Yes')
					{
						$data['message'] = 'You cannot delete this schedule. One or more registrants already allocated to this schedule';
						echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
						goto a;
					}
					//BEGIN -- NOT USE IN DELETE PART
					$d['AdmissionTerm'] = '';
					$d['Venue'] = '';
					$d['StartDate'] = '';
					$d['EndDate'] = '';
					$d['StartTime'] = '';
					$d['EndTime'] = '';
					$d['VenueInterview'] = '';
					$d['Date'] = '';
					$d['StartTimeInterview'] = '';
					$d['EndTimeInterview'] = '';
					$d['PretestSubjectId'] = '';
					//END -- NOT USE IN DELETE PART
					
					$d['KdMsJadwalET'] = $input['hfEntranceTestID'];
					$d['AuditUserName'] = $this->session->userdata('UserId');
					$d['Delete'] = '1';
				}				
				
				$query = $this->entrance_test_schedule_model->set_entrance_test_schedule($d);
				if($query)
				{

					$data['status'] = 'failed';
					$data['message'] = $query[0]->Message;
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				}
				else
				{
					$data['status'] = 'success';
					$data['message'] = "Data updated successfully" ;
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				}
			/*}
			else
			{
				$data['status'] = 'failed';
				//$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				//$data['message'] = str_replace('.','<br/><br/>',$data['message']);
				//echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}*/
			a :
			$this->session->set_flashdata('message', $data['message']);
			redirect(site_url().'/entrance_test/entrance_test_schedule');
			// $d = $this->get_entrance_test_schedule_paging();
			// $data['data']['result'] = $d['data']['result'];
			// $data['data']['pagination'] = $d['data']['pagination'];
			// $temp = explode("-", $this->period);
			// $data['year'] = $temp[0];
			// $data['semester'] = $temp[1];
			// $data['PretestSubject'] = $this->entrance_test_schedule_model->get_pre_test_subject();
			// $data['Venue'] = $this->entrance_test_schedule_model->get_venue();
			// $data['SchoolLevel'] = $this->entrance_test_schedule_model->get_school_level();
			// $schoollevel = (isset($input['ddlSchoolLevel']) ? $input['ddlSchoolLevel'] : '');
			// $data['PretestTerm'] = $this->get_pretest_term($schoollevel,true);

			// $this->template->display('entrance_test/entrance_test_schedule',$data);
		}
	
		public function get_entrance_test_schedule_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			$table['f'] = array(
				'Period'		=> $this->period,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'AdmissionTerm'	=> isset($input['ddlAdmissionTerm']) ? null_setter($input['ddlAdmissionTerm']) : null,
			);
			$query = $this->entrance_test_schedule_model->get_entrance_test_schedule_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->entrance_test_schedule_model->get_entrance_test_schedule_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_entrance_test_schedule_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestSubject'] = $this->entrance_test_schedule_model->get_pre_test_subject();
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['Venue'] = $this->entrance_test_schedule_model->get_venue();
			$data['SchoolLevel'] = $this->entrance_test_schedule_model->get_school_level();
         	$this->template->display('entrance_test/entrance_test_schedule',$data);
		}
	}
/*	End	of	file	entrance_test_schedule.php	*/
/*	Location:		./controllers/entrance_test/entrance_test_schedule.php */

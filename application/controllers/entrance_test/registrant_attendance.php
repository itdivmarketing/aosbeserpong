<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Registrant_attendance extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("entrance_test/registrant_attendance_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
			//$this->output->nocache();
		}
		public function index()
        {
			$d = $this->get_entrance_test_schedule_paging();
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->registrant_attendance_model->get_school_level();
         	$this->template->display('entrance_test/registrant_attendance',$data);
        }
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->registrant_attendance_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
		public function get_all_registrant($KdMsJadwalET, $termID, $venue, $capacity, $postback = false)
		{
			$Param['KdMsJadwalET'] = $KdMsJadwalET;
			$Param['termID'] = $termID;
			$data['scheduleID'] = $KdMsJadwalET;
			$data['termID'] = $termID;
			$data['venue'] = $venue;
			$data['capacity'] = $capacity;
			$data['registrants'] = $this->registrant_attendance_model->get_all_registrant_attendance($Param);
				
			if($postback) return $data['registrants'];
			else echo json_encode($data);
		}
		public function set_registrant_attendance()
		{
			$input = $this->input->post();
			$this->load->library('form_validation');
			$this->form_validation->set_rules("hfScheduleID","Entrance Test ID","required");
			$this->form_validation->set_rules("hfAdmissionTerm","School Level","required");
			$this->form_validation->set_rules("cbAttendance","attendance","required");
				
			if($this->form_validation->run() != false)
			{	
				$d = array();
				$this->db->trans_begin();
				$d['KdMsJadwalET'] = $input['hfScheduleID'];
				$d['AuditUserName'] = $this->session->userdata('UserId');
				$this->registrant_attendance_model->delete_registrant_attendance($d);
				
				
				$d['RegistrantID'] = '';
				$flag = $this->input->post('cbAttendance'); 
				foreach($flag as $key=>$value)
				{
					$d['RegistrantID'] = $value;
					$d['StatusHadir'] = '1';
					$this->registrant_attendance_model->save_registrant_attendance($d);
				}
				
				if ( $this->db->trans_status() === FALSE  ) {
				   $this->db->trans_rollback();
				} else {    
				   $this->db->trans_commit();    
				}
				
				$data['status'] = 'success';
				$data['message'] = "<span>Data updated successfully</span>" ;
			}
			else
			{
				$data['status'] = 'failed';
				$data['message'] = preg_replace('/\n/', '', validation_errors("<span>",'</span>'));
				$data['message'] = str_replace('.','<br/><br/>',$data['message']);
			}
			
			$this->index();
		}
		public function get_entrance_test_schedule_paging($input = null)
        {
			// TABEL PARAMETER SEARCH, SORT
			$table = array();
			$table['per_page'] =  10;
			$table['page'] = (isset($input['hfPage']) and !empty($input['hfPage'])) ? $input['hfPage'] : 1;
			$table['f'] = array(
				'Period'		=> $this->period,
				'SchoolLevel'	=> isset($input['ddlSchoolLevel']) ? null_setter($input['ddlSchoolLevel']) : null,
				'AdmissionTerm'	=> isset($input['ddlAdmissionTerm']) ? null_setter($input['ddlAdmissionTerm']) : null,
			);
			$query = $this->registrant_attendance_model->get_entrance_test_schedule_paging(array_merge(array('count'=>1),$table['f']));
			$table['total_data']    = $query[0]->TOTAL_DATA;
			$result = $this->registrant_attendance_model->get_entrance_test_schedule_paging(array_merge(array('page'=>$table['page'],'limit'=>$table['per_page'],'count'=>0),$table['f']));
			
			if($result)
			{
				$data['status'] = true;
				$data['data'] = array();
				$data['data']['pagination'] = calculate_pages($table['total_data'], $table['per_page'], $table['page']);
				$data['data']['result']     = $result;
			}
			else
			{
				$data['status'] = false;
				$data['data'] = array();
				$data['data']['pagination'] = null;
				$data['data']['result']     = null;
			}
			return $data;
		}
		public function search()
        {	
			$input = (array)$this->input->get();
			$P['PeriodSemesterSetting'] = $this->period;
			$d = $this->get_entrance_test_schedule_paging($input);
			$data['data']['result'] = $d['data']['result'];
			$data['data']['pagination'] = $d['data']['pagination'];
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['PretestTerm'] = $this->get_pretest_term($input['ddlSchoolLevel'],true);
			$data['SchoolLevel'] = $this->registrant_attendance_model->get_school_level();
         	$this->template->display('entrance_test/registrant_attendance',$data);
		}
	}
/*	End	of	file	entrance_test_schedule.php	*/
/*	Location:		./controllers/entrance_test/entrance_test_schedule.php */

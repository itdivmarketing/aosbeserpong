<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Form_sales extends CI_Controller {
	
		var $period;
		public $data    =       array();
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper('site_helper');
			is_logged_in();
	
		
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');



			$this->ckeditor->basePath = base_url().'resources/ckeditor/';
			$this->ckeditor->config['toolbar'] =  array(
              array( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),'/',
              array( 'Image', 'Link', 'Unlink', 'Anchor' )
          );
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '730px';
			$this->ckeditor->config['height'] = '300px'; 
		  
		}
		public function index()
        {
		
         	$this->load->view('ckeditor',$this->data);
        }
		
		public function get_string(){
			$input=$this->input->post('ckeditor');
			//print_r($input);
			echo '<script> alert("'.$input.'");</script>';
		}
		
	}
/*	End	of	file	form_sales.php	*/
/*	Location:		./controllers/entry/form_sales.php */

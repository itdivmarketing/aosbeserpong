<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class create_binusian_id extends CI_Controller {
	
		var $period;
		function __construct()
		{
			parent::__construct();
			$this->load->model("user_management/general_setting_model");
			$this->load->model("reregistration/create_binusian_id_model");
			$this->load->model("entry/reregistration_model");
			$this->load->library('form_validation');
			$this->load->helper('admission_helper');
			$this->load->helper('site_helper');
			is_logged_in();
			$Param['userId'] = $this->session->userData('UserId');
			$GeneralSetting = $this->general_setting_model->get_general_setting($Param);
			$this->period = trim($GeneralSetting[0]->Period);
		}
		
		public function index()
        {
			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$data['SchoolLevel'] = $this->create_binusian_id_model->get_school_level();
			$data['AcademicYear'] = $this->create_binusian_id_model->get_academic_year();
			$data['Registrant'] = null;			
         	$this->template->display('reregistration/create_binusian_id',$data);
        }
		
		public function search_binusian_id()
        {

			$temp = explode("-", $this->period);
			$data['year'] = $temp[0];
			$data['semester'] = $temp[1];
			$Param['TermId'] = (isset($_GET['ddlAdmissionTerm']) ? $_GET['ddlAdmissionTerm'] : '');
			$Param['RegistrantId'] = (isset($_GET['txtRegistrantID']) ? $_GET['txtRegistrantID'] : '') ;
			$Param['tipe'] = (isset($_GET['ddlStatus']) ? $_GET['ddlStatus'] : '');
			$data['SchoolLevel'] = $this->create_binusian_id_model->get_school_level();
			$schoolLevel = (isset($_GET['ddlSchoolLevel']) ? $_GET['ddlSchoolLevel'] : '');
			$data['PretestTerm'] = $this->get_pretest_term($schoolLevel,true);
			$data['Registrant'] = $this->create_binusian_id_model->search($Param);
			$data['AcademicYear'] = $this->create_binusian_id_model->get_academic_year();
			// $data['SchoolRegulation'] = $this->reregistration_model->get_school_regulation();

         	$this->template->display('reregistration/create_binusian_id',$data);
        }
		
		public function get_pretest_term($SchoolLevel = null,$postback = false)
		{
			if($SchoolLevel)
			{
				$Param['PeriodSemesterSetting'] = $this->period;
				$Param['SchoolLevelID'] = $SchoolLevel;
				$data['PretestTerm'] = $this->create_binusian_id_model->get_pretest_term($Param);
			}
			else
			{
				$data['PretestTerm'] = '';
			}
			if($postback) return $data['PretestTerm'];
			else echo json_encode($data);
		}
        
		public function set_binusian_id()
		{
			$input = $this->input->post();	

			if(!isset($input['cbRegistrant']))
			{
				$data['status'] = 'failed';
				$data['message'] = "You need to choose at least 1 (one) student to be generated" ;
				echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
			}
			else
			{

				if(sizeof($input['cbRegistrant']) <= 0 )
				{
					$data['status'] = 'failed';
					$data['message'] = "You need to choose at least 1 (one) student to be generated" ;
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				}
				else
				{

					$all_success = true;
					for($i = 0 ; $i < sizeof($input['hfRegistrantID']) ; $i++)
					{
						if(in_array($input['hfRegistrantID'][$i],$input['cbRegistrant']))
						{
							$d['RegistrantID'] = $input['hfRegistrantID'][$i];
							$d['AuditUserName'] = $this->session->userdata('UserId');
							
					
							$check = $this->create_binusian_id_model->check_registrant_data($d);			
							if($check[0]->Message == '')
							{
								$dat['SchoolLevelId']= $input['hfSchoolLevel'][$i];
								$dat['InsertIntoBiNusian']= '1' ;
								$dat['FullName']=  $input['hfName'][$i];
								$dat['GENDerID']=  $input['hfGender'][$i];
								$dat['RegistrantID']= $input['hfRegistrantID'][$i];
								$dat['AuditUserName']= $this->session->userdata('UserId');
								$temp = explode(" ", $input['hfDOB'][$i]);						
								$dat['DOB']= $temp[0];
								$dat['JoinToSchool']= sql_srv_date($input['txtJoinSchool']);
								
								//print_r($dat);
								//die;
								//var_dump($studentid);
								//die;
								try
								{
									$studentid = $this->create_binusian_id_model->generate_binusian_id($dat);
									if (isset($studentid->RESULT) && $studentid->RESULT != 0)
									{
										throw new Exception('Database Error: error on SP \"generate_binusian_id\"');
									}
								}
								catch(Exception $ex)
								{
									$data['status'] = 'failed';
									$data['message'] = $ex->Message() ;
									echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
									$this->search_binusian_id();
									return;
								}
								//print_r($studentid);
								//var_dump($studentid);
							
								$temp = explode("-", $this->period);
								$da['ThnAkad']= $input['ddlAcademicYear'];
								$da['Gelombang']= $input['ddlAcademicSemester'];
								//$da['ThnAkad']= $temp[0];
								//$da['Gelombang']= $input['hfTerm'][$i];
								//$da['StudentID']= "";
								$da['RegistrantID']= $input['hfRegistrantID'][$i];
								$da['AuditUserName']= $this->session->userdata('UserId');
								$temp = explode(" ", $input['hfDOB'][$i]);						
								$da['DOB']= $temp[0];
								try
								{
									$query = $this->create_binusian_id_model->transfer_data_registrant($da);
									if (isset($query->RESULT) &&  $query->RESULT != 0)
									{
										throw new Exception('Database Error: error on SP \"transfer_data_registrant\"');
									}
								}
								catch(Exception $ex)
								{
									$data['status'] = 'failed';
									$data['message'] = $ex->Message() ;
									echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
									$this->search_binusian_id();
									return;
								}
								
								$data['SchoolLevelId']= $input['hfSchoolLevel'][$i];
								$data['StudentID']= "";
								$data['InsertIntoBiNusian']=  '1';
								$data['FullName']=  $input['hfName'][$i];
								$data['GENDerID']=  $input['hfGender'][$i];
								$data['RegistrantID']= $input['hfRegistrantID'][$i];
								$data['Audit_User_Name']= $this->session->userdata('UserId');
								$temp = explode(" ", $input['hfDOB'][$i]);						
								$data['DOB']= $temp[0];
								$data['JoinToSchoolT']= sql_srv_date($input['txtJoinSchool']); 
								//print_r($data);
								//die;

								try
								{
									$query = $this->create_binusian_id_model->transfer_data_library($data);
									if (isset($query->RESULT) &&  $query->RESULT != 0)
									{
										throw new Exception('Database Error: error on SP \"transfer_data_library\"');
									}
								}
								catch(Exception $ex)
								{
									$data['status'] = 'failed';
									$data['message'] = $ex->Message() ;
									echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
									$this->search_binusian_id();
									return;
								}


							}else{
									$all_success= false;
							}

								
								/*
								$Param['RegistrantID'] = $d['RegistrantID'];
								$Param['AuditUserName']=$this->session->userdata('UserId');
								$Param['YearLevelID'] = $input['hfYearLevelID'][$i];
								$Param['Pass'] = '1';
								$Param['isAgree'] = '1';
								$this->reregistration_model->insert_check_agree($Param);	
								alert("sukses");*/

						}

						
					
					}
					
					if($all_success==true){
						$data['status'] = 'success';
						$data['message'] = "Binusian ID updated successfully" ;
					 }else{
					 	$data['status'] = 'success';
						$data['message'] = "Some student can\'t be generated, Please check student data" ;				 	
					 }
					 
					echo "<script type='text/javascript'>alert('".$data['message']."');</script>";
				}
			}
			$this->search_binusian_id();
		}


	}
/*	End	of	file	role_privilege.php	*/
/*	Location:		./controllers/user_management/role_privilege.php */

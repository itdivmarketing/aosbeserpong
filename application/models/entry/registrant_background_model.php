<?php
class registrant_background_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_type_of_school_list()
	{
		/**
		 *	SP Example : Adm_GetTypeOfPreviousSchool
		 **/
		$Result= $this->sp('Adm_GetTypeOfPreviousSchool');
		return $Result->result();
	}

	function get_year_level($RegistrantID)
	{
		/**
		 *	SP Example : Adm_GetRegistrantPrevSchoolYearLevelList
		 **/

		$Result= $this->sp('Adm_GetRegistrantPrevSchoolYearLevelList',array('RegistrantID'=>$RegistrantID))->result();
		return $Result;
		
		
	}
	
	public function get_school_list_paging($Param)
	{
		$Result= $this->sp('Adm_GetSchoolDataPaging',$Param)->result();
		return $Result;
	}
	
	function get_school_list($Param=NULL)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel
		 **/
		 if($Param==NULL)
			$Result= $this->sp('Adm_GetSchoolData')->result();
		else
			$Result= $this->sp('Adm_GetSchoolData',$Param)->result();
		return $Result;
	}
	
	function get_registrant_previous_school_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel
		 **/
		$Result=$this->sp('Adm_GetRegistrantPreviousSchoolData',$Param)->result();
		
		return $Result;
		
	}
	
	public function insert_registrant_previous_school_data ($Data)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel
		 **/
		$this->sp("Adm_InsertRegistrantPreviousSchoolData", $Data);
	}
	
	public function delete_registrant_previous_school_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel
		 **/
		$this->sp("Adm_DeleteRegistrantPreviousSchoolData",$Param);

	}
	
	//language
	
	public function get_language_list()
	{
		$Result= $this->sp('Adm_GetLanguage')->result();
		return $Result;
	}
	
	public function get_registrant_background_language_data($Param)
	{
		$Result=$this->sp('Adm_GetRegistrantLanguageData',$Param)->result();
		
		return $Result;
	}
	
	
	
	public function insert_registrant_background_language_data ($Param)
	{
		$this->sp("Adm_InsertRegistrantLanguageData", $Param);
	}
	
	
	public function delete_registrant_background_language_data ($Param)
	{
		 $this->sp("Adm_DeleteRegistrantLanguageData", $Param);
	}

	
}
/*	End	of	file	registrant_background_model.php	*/
/*	Location:		./models/entry/registrant_background_model.php */

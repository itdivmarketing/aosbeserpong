<?php
class sibling_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	
	public function check_sibling_studentID($Param)
	{
		/**
		 *	SP Example : Adm_CheckRegistrantSiblingStudentID
		 *	@StudentID,@SelfID
		 **/
	
		$Result= $this->sp('Adm_CheckRegistrantSiblingStudentID',$Param)->result();
		return $Result;
	}
	
	public function get_current_enrolled_sibling_id()
	{
		$Result= $this->sp('Adm_GetRegistrantCurrentEnrolledSiblingID')->row();
		return $Result;
	}
	
	public function get_current_enrolled_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantCurrentEnrolledSiblingData
		 *	@StudentID
		 **/
		 
		$Result=$this->sp('Adm_GetRegistrantCurrentEnrolledSiblingData',$Param)->result();
		
		return $Result;
	}
	
	public function delete_current_enrolled_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantCurrentEnrolledSiblingData
		 *	@StudentID,@AuditUserName
		 **/
		 $this->sp('Adm_DeleteRegistrantCurrentEnrolledSiblingData',$Param);
		
	}
	
	public function delete_current_enrolled_deleted_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantCurrentEnrolledSiblingData
		 *	@StudentID,@AuditUserName
		 **/
		 $this->sp('Adm_DeleteRegistrantCurrentEnrolledSiblingData',$Param);
		
	}
	
	public function insert_current_enrolled_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_InsertRegistrantCurrentEnrolledSiblingData
		 *	@SiblingID,@StudentID,@AuditUserName,@HaveSiblingID
		 **/
			 $this->sp('Adm_InsertRegistrantCurrentEnrolledSiblingData',$Param);
		
	}
	
	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}

	
	public function get_school_level_list()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevelList
		 **/
		$Result=$this->sp('Adm_GetSchoolLevelList')->result();
		
		return $Result;
		
	}
	
	public function get_others_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantPotentialSiblingData
		 *	@RegistrantID
		 **/
		$Result=$this->sp('Adm_GetRegistrantPotentialSiblingData',$Param)->result();
		
		return $Result;
		
	}
	
	public function search_sibling_by_name($Param)
	{
		/**
		 *	SP Example : Adm_SearchSiblingByName_Paging
		 *	@StudentName
		 **/
	
		$Result=$this->sp('Adm_SearchSiblingByName_Paging',$Param)->result();
		
		return $Result;
		
	}	
	public function delete_others_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantPotentialSiblingData
		 *	@RegistrantID,@AuditUserName
		 **/
		 
		$this->sp('Adm_DeleteRegistrantPotentialSiblingData',$Param);
		
	}
	
	public function insert_others_sibling_data($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantPotentialSiblingData
		 *	@RegistrantID,@PotentialSiblingName,@PotentialSiblingID,@SchoolLevelID,
		 *	@YearLevelID,@Birthday,@AuditUserName
		 **/
		 
		$this->sp('Adm_InsertRegistrantPotentialSiblingData',$Param);
	}


	
}
/*	End	of	file	sibling_model.php	*/
/*	Location:		./models/entry/form_return/sibling_model.php */

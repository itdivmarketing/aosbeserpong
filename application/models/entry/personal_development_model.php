<?php
class personal_development_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_type_of_school_list()
	{
		/**
		 *	SP Example : Adm_GetTypeOfPreviousSchool
		 **/
		$Result= $this->sp('Adm_GetTypeOfPreviousSchool');
		return $Result->result();
	}
	
	public function get_sickness_list ()
	{
		$Result=$this->sp('Adm_GetSicknessList')->result();
		
		return $Result;
	}
	
	public function get_registrant_sickness_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantSicknessData
		 *	@RegistrantID
		 **/
		$Result=$this->sp('Adm_GetRegistrantSicknessData',$Param)->result();
		
		return $Result;
	}

	
	public function delete_registrant_sickness_data ($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantSicknessData
		 *	@RegistrantID,@AuditUserName
		 **/
		$this->sp("Adm_DeleteRegistrantSicknessData",$Param);

	}
	
	public function insert_registrant_sickness_data ($Param)
	{
		/**
		 *	SP Example : Adm_InsertRegistrantSicknessData
		 *	@RegistrantID,@SicknessID INT,@Memo,@AuditUserName
		 **/
		$query = $this->sp("Adm_InsertRegistrantSicknessData", $Param);
	}
	
	
	
	
	public function get_registrant_problem_difficulties_data ($Param)
	{
		/**
		 *	SP Example : adm_GetRegistrantProblemsAndDifficultiesData
		 *	@RegistrantID,@ProblemID INT
		 **/
		$Result=$this->sp('adm_GetRegistrantProblemsAndDifficultiesData',$Param)->row();
		
		return $Result;
	}

	
	public function delete_registrant_problem_difficulties_data ($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantProblemsAndDifficulties
		 *	@RegistrantID,@ProblemTypeID,AuditUserName
		 **/
		$this->sp("Adm_DeleteRegistrantProblemsAndDifficulties",$Param);

	}
	
	public function get_registrant_problem_difficulties_list()
	{
		$Result= $this->sp('adm_GetProblemsAndDifficultiesList')->result();
		return $Result;
	}
	
	
	public function insert_registrant_problem_difficulties_data($Param)
	{
		$this->sp('Adm_InsertRegistrantProblemAndDifficulties', $Param);
	}
	
	
	
}
/*	End	of	file	personal_development_model.php	*/
/*	Location:		./models/entry/personal_development_model.php */

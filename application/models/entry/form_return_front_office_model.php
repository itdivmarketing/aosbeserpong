<?php
class form_return_front_office_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	
	function get_form_data($Param)
	{
		/**
		 *	SP Example : Adm_SearchFormSold_Paging
		 **/

		$Result = $this->sp('Adm_SearchFormSold_Paging',$Param);

		return $Result->result();
	}
	
	function get_form_data_paging($Param)
	{
		/**
		 *	SP Example : Adm_SearchFormReturnFO_Paging
		 **/

		$Result = $this->sp('Adm_SearchFormReturnFO_Paging',$Param);

		return $Result->result();
	}
	
	function get_form_view($Param)
	{
		/**
		 *	SP Example : Adm_GetFormReturnFrontOfficeView
		 **/

		$Result = $this->sp('Adm_GetFormReturnFrontOfficeView',$Param);

		return $Result->result();
	}
	
	
	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}
	
	function get_gender($Param=null)
	{
		/**
		 *	SP Example : Adm_GetLtGender
		 **/

		$Result = $this->sp('Adm_GetLtGender');
		
		return $Result->result();
	}

	function get_child_status($Param=null)
	{
		/**
		 *	SP Example : Adm_GetLTChildStatus 
		 **/

		$Result = $this->sp('Adm_GetLTChildStatus');
		
		return $Result->result();
	}
	
	
	function check_return_form_FO($Param=null)
	{
		/**
		 *	SP Example : Adm_CheckReturnFormFO 
		 **/

		$Result = $this->sp('Adm_CheckReturnFormFO',$Param);
		
		return $Result->row();
	}
	

	
	function get_type_of_registration($Param=null)
	{
		/**
		 *	SP Example : Adm_GetTypeOfRegistration
		 **/
		
		$Result = $this->sp('Adm_GetTypeOfRegistration');
		
		return $Result->result();
	}

	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_semester($Param=null)
	{
		/**
		 *	SP Example : Adm_GetLTSemester
		 **/

		$Result = $this->sp('Adm_GetLTSemester');

		return $Result->result();
	}
	
	function get_academic_year()
	{
		/**
		 *	SP Example : [Adm_GetAcademicYearList]
		 **/

		$Result = $this->sp('Adm_GetAcademicYearList');

		return $Result->result();
	}
	
	function save_return_form($Param)
	{
		/**
		 *	SP Example : Adm_SavePengembalianFO 
		 **/

		$Result = $this->sp('Adm_SavePengembalianFO',$Param);

		return $Result->result();
	}
	
	
	
	
	function get_admissionID($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDByPeriod
		 **/

		$Result = $this->sp('Adm_GetAdmissionID',$Param);

		return $Result->result();
	}
	

	
	function check_form_validation($Param)
	{
		/**
		 *	SP Example : Adm_CheckReturnFormFO
		 **/

		$Result = $this->sp('Adm_CheckReturnFormFO',$Param);

		return $Result->row();
	}


	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}

	

	

	function save_form_return_front_office($Param)
	{
		/**
		 *	SP Example : Adm_SaveFormSold
		 **/
		
		$Result = $this->sp('Adm_SaveFormSold',$Param);

		return $Result->result();
	}
}
/*	End	of	file	form_return_front_office_model.php	*/
/*	Location:		./models/entry/form_return/form_return_front_office_model.php */

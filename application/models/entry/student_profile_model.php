<?php
class student_profile_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_type_of_school_list()
	{
		/**
		 *	SP Example : Adm_GetTypeOfPreviousSchool
		 **/
		$Result= $this->sp('Adm_GetTypeOfPreviousSchool');
		return $Result->result();
	}

	function get_religion($Param=null)
	{
		/**
		 *	SP Example : Adm_GetLtReligion
		 **/

		$Result = $this->sp('Adm_GetLtReligion');
		
		return $Result->result();
	}
	
	function get_domicile_type($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdm_LTDomincile
		 **/

		$Result = $this->sp('Adm_GetAdm_LTDomincile');
		
		return $Result->result();
	}
	
	function get_country()
	{
		/**
		 *	SP Example : Adm_GetLTCountry
		 **/

		$Result = $this->sp('Adm_GetLTCountry');
		
		return $Result->result();
	}
	
	function get_city($Param=null)
	{
		/**
		 *	SP Example : Adm_GetLTCity
		 **/

		$Result = $this->sp('Adm_GetLTCity',$Param);
		
		return $Result->result();
	}
	
	function get_region($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdm_LTregistrantregion
		 **/

		$Result = $this->sp('Adm_GetAdm_LTregistrantregion');
		
		return $Result->result();
	}

	function get_parent($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdm_LTParent
		 **/

		$Result = $this->sp('Adm_GetAdm_LTParent');
		
		return $Result->result();
	}
	
	function insert_student_data ($Param)
	{
		$Result = $this->sp('Adm_InsertStudentProfileBackOffice',$Param);

		
	}
	
	function check_fo_registered ($Param)
	{
		$Result=$this->sp('Adm_CheckRegistrantFORegistered',$Param)->row();
		
		return $Result;
	}

	
	function get_student_data ($Param)
	{
		$Result=$this->sp('Adm_GetMsStudent1',$Param)->row();
		
		return $Result;
	}

	
}
/*	End	of	file	form_return_front_office_model.php	*/
/*	Location:		./models/entry/form_return/form_return_front_office_model.php */

<?php
class learning_note_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}


	public function get_registrant_learning_note_data ($Param)
	{
		/**
		 *	SP Example : adm_GetRegistrantLearningNoteData
		 *	@RegistrantID,@ProblemID
		 **/
		$Result=$this->sp('adm_GetRegistrantLearningNoteData',$Param)->result();
		return $Result;
	}

	
	public function delete_registrant_sickness_data ($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantSicknessData
		 *	@RegistrantID,@AuditUserName
		 **/
		$this->sp("Adm_DeleteRegistrantSicknessData", $Param);

	}
	
	public function insert_registrant_problem_difficulties_data($Param)
	{
		$this->sp('Adm_InsertRegistrantProblemAndDifficulties', $Param);
	}
	
	public function get_registrant_problem_difficulties_list()
	{
		$Result= $this->sp('adm_GetProblemsAndDifficultiesList')->result();
		return $Result;
	}
	
	public function delete_registrant_problem_difficulties_data ($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantProblemsAndDifficulties
		 *	@RegistrantID,@ProblemTypeID,@AuditUserName
		 **/
		$this->sp("Adm_DeleteRegistrantProblemsAndDifficulties", $Param);

	}
	
}
/*	End	of	file	learning_note_model.php	*/
/*	Location:		./models/entry/learning_note_model.php */

<?php
class form_return_back_office_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function check_student_status($Param)
	{
		/**
		 *	SP Example : [adm_CheckRegistrantPassword]
		 **/
		
		$Result = $this->sp('adm_CheckRegistrantPassword',$Param);

		return $Result->row();
	}
	
	function check_student_validity($Param)
	{
		/**
		 *	SP Example : [adm_Checkvw_adm_studentdata]
		 **/
		
		$Result = $this->sp('adm_Checkvw_adm_studentdata',$Param);

		return $Result->row();
	}
	

	
	public function insert_registrant_photo ($Array)
	{
		$Result=$this->sp('Adm_InsertRegistrantPhoto',$Array)->row();
		return $Result;

	}
	
	public function get_registrant_photo ($Array)
	{
		$Result=$this->sp('Adm_GetRegistrantPhoto',$Array)->row();
		return $Result;

	}
	
	public function search_student($Param)
	{
		/**
		 *	SP Example : Adm_SearchStudentBackOffice_Paging
		 *	@StudentName
		 **/
		$Result=$this->sp('Adm_SearchStudentBackOffice_Paging',$Param)->result();
		
		return $Result;
		
	}
}
/*	End	of	file	form_return_back_office_model.php	*/
/*	Location:		./models/entry/form_return/form_return_back_office_model.php */

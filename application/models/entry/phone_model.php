<?php
class phone_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	 function get_phone_data ($Param)
	{
		$Result=$this->sp('Adm_GetMsPhone',$Param)->result();
		return $Result;
	}
	
	
	 function delete_phone_data ($Param)
	{
		$Result=$this->sp("Adm_DeleteMsPhone",$Param);
		
	}
	
	
	 function insert_phone_data ($Param)
	{
		$Result=$this->sp("Adm_InsertMsPhone", $Param);	
	}
	

	
}
/*	End	of	file	phone_model.php	*/
/*	Location:		./models/entry/form_return/phone_model.php */

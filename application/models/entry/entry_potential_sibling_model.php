<?php
class Entry_potential_sibling_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function search_potential_sibling_studentID($Param)
	{
		$Result = $this->sp('Adm_SearchPotentialSiblingStudentID', $Param);

		return $Result->result();
	}

	function save_potential_sibling_student($Param)
	{
		$retVal = array();
	
		$flag = "";
		
		$count=0;
			
		foreach($Param['PotentialSiblingStudentID'] as $row){
			$count++;
		}
		
		
		
		//$this->db->trans_begin();
		
		for($i=1;$i<=$count;$i++) {
			if(isset($Param['PotentialSiblingStudentID'][$i]) and !empty($Param['PotentialSiblingStudentID'][$i])){			
				$data['RegistrantID']= $Param['RegistrantID'][$i];
				$data['PotentialSiblingID']= $Param['PotentialSiblingID'][$i];
				$data['SiblingStudentID'] = $Param['PotentialSiblingStudentID'][$i];
				$data['AuditUserName'] = $this->session->userdata("UserId");
							
				$check_id['SiblingStudentID'] = $Param['PotentialSiblingStudentID'][$i];
				
				$retVal["flag"] = $data['SiblingStudentID'];
			}
						
			$check = $this->sp('Adm_CheckPotentialSiblingStudentID', $check_id);
			$tempCheck = $check->result();
			if($tempCheck[0]->Msg == "Failed") {
				$retVal["Msg"] = $tempCheck[0]->Msg;
				return $retVal;
			}

		}
		
		$this->db->trans_begin();
		
		for($i=1;$i<=$count;$i++) {
			//if(isset($Param['PotentialSiblingStudentID'][$i]) and !empty($Param['PotentialSiblingStudentID'][$i])){				
				$data['RegistrantID']= $Param['RegistrantID'][$i];
				$data['PotentialSiblingID']= $Param['PotentialSiblingID'][$i];
				$data['SiblingStudentID'] = $Param['PotentialSiblingStudentID'][$i] == "" ? "" : $Param['PotentialSiblingStudentID'][$i];
				$data['AuditUserName'] = $this->session->userdata("UserId");				
				
				$Result = $this->sp('Adm_SavePotentialSiblingStudentID', $data);
			//}
		}
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		$retVal["Msg"] = "Success";
		return $retVal;
	}

	function get_potential_sibling_paging($Param)
	{
		/**
		 *	SP Example : Adm_SearchPotentialSiblingStudentID
		 **/

		$Result = $this->sp('Adm_SearchPotentialSiblingStudentID',$Param);

		return $Result->result();
	}
}
/*	End	of	file	entry_potential_sibling_model.php	*/
/*	Location:		./models/entry/entry_potential_sibling_model.php */

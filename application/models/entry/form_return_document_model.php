<?php
class form_return_document_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}



	
	public function insert_registrant_document_submission_data ($Param)
	{
			$this->sp("Adm_InsertRegistrantDocumentSubmissionData", $Param);

	}
	
	public function get_document_list ($Param)
	{
		/**
		 *	SP Example : Adm_GetDocumentList
		 *	@RegistrantID
		 **/
		$Result= 	$this->sp('Adm_GetDocumentList',$Param)->result();
		
		return $Result;

	}

	
	public function get_registrant_document_submission_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantDocumentSubmissionData
		 *	@RegistrantID
		 **/
		$Result=$this->sp('Adm_GetRegistrantDocumentSubmissionData',$Param)->result();
		
		return $Result;

	}
		
	public function check_student_information_completeness($Param)
	{
		/**
		 *	SP Example : Adm_CheckStudentInformationCompleteness
		 *	@StudentID
		 **/
		$Result=$this->sp('Adm_CheckStudentInformationCompleteness',$Param)->row();
		
		return $Result;

	}
	
	public function get_admission_process_list()
	{
		/**
		 *	SP Example : Adm_GetAdmissionProcess
		 **/
		$Result=$this->sp('Adm_GetAdmissionProcess')->result();
		
		return $Result;

	}
	
}
/*	End	of	file	form_return_document_model.php	*/
/*	Location:		./models/entry/form_return_document_model.php */

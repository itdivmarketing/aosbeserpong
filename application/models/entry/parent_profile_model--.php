<?php
class parent_profile_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_type_of_school_list()
	{
		/**
		 *	SP Example : Adm_GetTypeOfPreviousSchool
		 **/
		$Result= $this->sp('Adm_GetTypeOfPreviousSchool');
		return $Result->result();
	}
	
	function get_country()
	{
		/**
		 *	SP Example : Adm_GetLTCountry
		 **/

		$Result = $this->sp('Adm_GetLTCountry');
		
		return $Result->result();
	}
	
	function get_city($Param=null)
	{
		/**
		 *	SP Example : Adm_GetLTCity
		 **/

		$Result = $this->sp('Adm_GetLTCity',$Param);
		
		return $Result->result();
	}
	
	function get_parent_phone_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetMsPhone
		 	@StudentID,@ParentStatus
		 **/
	
		$Result=$this->sp('Adm_GetMsPhone',$Param);
		return $Result;
	}
	
	
	function get_parent_profile_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantParentData
		 	@studenttid,@parentstatus
		 **/
	
		$Result=$this->sp('Adm_GetRegistrantParentData',$Param)->row();
		return $Result;
	}

	function insert_parent_data ($Param)
	{
			$query =  $this->sp("Adm_InsertRegistrantParentProfileData", $Param);
		
	}
	
	public function get_life_status()
	{
		$Result=$this->sp('Adm_GetLifeStatusList')->result();
		return $Result;
	}


	public function insert_parent_employment_data ($Param)
	{
		$this->sp("Adm_InsertRegistrantParentEmploymentData", $Param);
		
	}
	
	public function insert_full_parent_data ($Param)
	{
			$query = $this->sp("Adm_InsertRegistrantParentProfileData", $Param);
		
	}
	
	
}
/*	End	of	file	parent_profile_model.php	*/
/*	Location:		./models/entry/form_return/parent_profile_model.php */

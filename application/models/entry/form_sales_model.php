<?php
class form_sales_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}
	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	public function get_email_detail ($RegistrantID,$NotificationTypeID,$Password)
	{
		
		$Result= 	$this->sp('adm_GetEmailContent',array( 
			'RegistrantID' => $RegistrantID,
			'NotificationTypeID' => $NotificationTypeID,
			'Password' => $Password
		))->row();
		
		return $Result;

	}

	
	function update_registrant_password($Param)
	{
		/**
		 *	SP Example : Adm_UpdateRegistrantPassword
		 **/

		$Result = $this->sp('Adm_UpdateRegistrantPassword',$Param);

		return $Result->result();
	}
	
	function get_form_sales($Param)
	{
		/**
		 *	SP Example : Adm_SearchFormSold_Paging
		 **/

		$Result = $this->sp('Adm_SearchFormSold_Paging',$Param);

		return $Result->result();
	}
	
	function get_form_amount($Param)
	{
		/**
		 *	SP Example : Adm_SearchFormSold_Paging
		 **/

		$Result = $this->sp('Adm_SearchFormSold_Paging',$Param);

		return $Result->result();
	}
	
	
	function get_admissionID($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDByPeriod
		 **/

		$Result = $this->sp('Adm_GetAdmissionID',$Param);

		return $Result->result();
	}
	

	
	function check_form_validation($Param)
	{
		/**
		 *	SP Example : Adm_CheckValidForm
		 **/

		$Result = $this->sp('Adm_CheckValidForm',$Param);

		return $Result->row();
	}
	
	function get_form_sold_view($Param)
	{
		/**
		 *	SP Example : Adm_GetFormSold
		 **/

		$Result = $this->sp('Adm_GetFormSold',$Param);

		return $Result->result();
	}
	
	function get_payment_method()
	{
		/**
		 *	SP Example : Adm_GetFormSold
		 **/

		$Result = $this->sp('Adm_GetPaymentMethod');

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_generated_password()
	{
		/**
		 *	SP Example : Adm_GetGeneratedPassword
		 **/

		$Result = $this->sp('Adm_GetGeneratedPassword');

		return $Result->row();
	}
	

	

	function save_form_sold($Param)
	{
		/**
		 *	SP Example : Adm_SaveFormSold
		 **/
		
		$Result = $this->sp('Adm_SaveFormSold',$Param);

		return $Result->result();
	}
}
/*	End	of	file	form_sales_model.php	*/
/*	Location:		./models/entry/form_sales_model.php */

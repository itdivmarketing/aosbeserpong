<?php
class ID_card_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}



	public function insert_registrant_nationality_data ($Param)
	{
			 $this->sp("Adm_InsertRegistrantNationalityData", $Param);

	}
	
	public function insert_registrant_parent_nationality_data ($Param)
	{
			$query = $this->sp("Adm_InsertRegistrantParentNationalityData", $Param);

	}
	
	public function get_country_list ()
	{
		$Result= $this->sp('Adm_GetCountryList')->result();
		
		return $Result;

	}

	public function get_visa_type_list ()
	{
		$Result=$this->sp('Adm_GetVisaTypeList')->result();
		return $Result;

	}
	
	public function get_registrant_nationality_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantNationalityData
		 *	@StudentID
		 **/
		$Result=$this->sp('Adm_GetRegistrantNationalityData',$Param)->row();
		
		return $Result;

	}
	
		
	public function get_registrant_parent_nationality_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantNationalityData
		 *	@StudentID,@ParentStatus
		 **/
		$Result=$this->sp('Adm_GetRegistrantParentNationalityData',$Param)->row();
		return $Result;

	}
	
	public function get_registrant_id_card_data ($Param)
	{
		/**
		 *	SP Example : Adm_GetRegistrantIDCardData
		 *	@RegistrantID,@ParentID
		 **/
			$Result=$this->sp('Adm_GetRegistrantIDCardData',$Param)->result();
		
			return $Result;

	}
	
	public function insert_registrant_id_card_data ($Data)
	{
		/**
		 *	SP Example : Adm_InsertRegistrantIDCardData
		 *	@RegistrantID,@ParentID
		 **/
		 $this->sp("Adm_InsertRegistrantIDCardData",$Data);

	}

	
	public function delete_registrant_id_card_data ($Param)
	{
		/**
		 *	SP Example : Adm_DeleteRegistrantIDCardData
		 *	@RegistrantID,@ParentID,@AuditUserName
		 **/
		$query = $this->sp("Adm_DeleteRegistrantIDCardData",$Param);

	}
	
	
	
}
/*	End	of	file	ID_card_model.php	*/
/*	Location:		./models/entry/form_return/ID_card_model.php */

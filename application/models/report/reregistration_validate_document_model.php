<?php
class reregistration_validate_document_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
			
		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	function get_document_list($Param)
	{
		/**
		 *	SP Example : adm_GetListDocPengembalianDokumen
		 **/

		$Result = $this->sp('adm_GetListDocPengembalianDokumen',$Param);

		return $Result->result();
		
	}

	function generate_report($Param)
	{
		/**
		 *	SP Example : Adm_GetValidasiPengembalianDokumenReg
		 **/

		$Result = $this->sp('Adm_GetValidasiPengembalianDokumenReg',$Param);

		return $Result->result();
		
	}
}
/*	End	of	file	reregistration_validate_document_model.php	*/
/*	Location:		./models/report/form_sales/reregistration_validate_document_model.php */

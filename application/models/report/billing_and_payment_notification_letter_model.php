<?php
class billing_and_payment_notification_letter_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function get_notification_letter($param)
	{
		/**
		 *	SP Example : Adm_GetNoSuratNotifikasi
		 **/

		$Result = $this->sp('Adm_GetNoSuratNotifikasi',$param);

		return $Result->result();
	}
	
	function get_paket_fee($param)
	{
		/**
		 *	SP Example : Adm_GetPaketFeeNotificationLetter
		 **/

		$Result = $this->sp('Adm_GetPaketFeeNotificationLetter',$param);

		return $Result->result();
	}
	
	function search($param)
	{
		/**
		 *	SP Example : Adm_GetNotifLetter
		 **/

		$Result = $this->sp('Adm_GetNotifLetter',$param);

		return $Result->result();
	}
	
	function save_nomor_surat_notifikasi($Param)
	{
		/**
		 *	SP Example : Adm_SaveNoSuratNotifikasi
		 **/

		$Result = $this->sp('Adm_SaveNoSuratNotifikasi',$Param);

		return $Result->result();
	}
	
	function generate_new_letter_number($Param)
	{
		/**
		 *	SP Example : Adm_GenerateNoSuratNotifikasi
		 **/

		$Result = $this->sp('Adm_GenerateNoSuratNotifikasi',$Param);

		return $Result->result();
	}
	
	function print_notification_letter($Param)
	{
		/**
		 *	SP Example : Adm_GetCetakNotificationLetter
		 **/

		$Result = $this->sp('Adm_GetCetakNotificationLetter',$Param);

		return $Result->result();
	}
	
	function get_biaya_paket_fee_tbc($Param)
	{
		/**
		 *	SP Example : Adm_GetBiayaPaketFeeTBC
		 **/

		$Result = $this->sp('Adm_GetBiayaPaketFeeTBC',$Param);

		return $Result->result();
	}
	
	function get_biaya_paket_fee_diskon($Param)
	{
		/**
		 *	SP Example : Adm_GetBiayaPaketFeeDiskon
		 **/

		$Result = $this->sp('Adm_GetBiayaPaketFeeDiskon',$Param);

		return $Result->result();
	}
	
	function get_biaya_paket_fee($Param)
	{
		/**
		 *	SP Example : Adm_GetBiayaPaketFee
		 **/

		$Result = $this->sp('Adm_GetBiayaPaketFee',$Param);

		return $Result->result();
	}
	
	
	function get_student_data ($Param)
	{
		$Result=$this->sp('Adm_GetMsStudent1',$Param)->row();
		
		return $Result;
	}
	



}
/*	End	of	file	billing_and_payment_notification_letter_model.php	*/
/*	Location:		./models/report/billing_and_payment_notification_letter_model.php */

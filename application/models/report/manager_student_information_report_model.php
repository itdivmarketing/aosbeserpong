<?php
class manager_student_information_report_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_year_level($Param)
	{
		/**
		 *	SP Example : Adm_GetYearLevel @userId
		 **/

		$Result = $this->sp('Adm_GetYearLevel',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function generate_report($Param)
	{
		/**
		 *	SP Example : Adm_GetStudentProfileReport
		 **/

		$Result = $this->sp('Adm_GetStudentProfileReport',$Param);

		return $Result->result();
	}
}
/*	End	of	file	manager_student_information_report.php	*/
/*	Location:		./models/report/manager_student_information_report.php */
	
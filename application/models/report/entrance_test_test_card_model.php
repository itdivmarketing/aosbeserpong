<?php
class entrance_test_test_card_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_schedule($Param)
	{
		/**
		 *	SP Example : Adm_GetEntranceTestScheduleID
		 **/

		$Result = $this->sp('Adm_GetEntranceTestScheduleID',$Param);

		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function generate_report($Param)
	{
		/**
		 *	SP Example : Adm_GetDataEntranceTest
		 **/

		$Result = $this->sp('Adm_GetDataEntranceTest',$Param);

		return $Result->result();
	}
}
/*	End	of	file	entrance_test_test_card_model.php	*/
/*	Location:		./models/report/entrance_test_test_card_model.php */

<?php
class manager_metrics_report_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_year_level($Param)
	{
		/**
		 *	SP Example : Adm_GetYearLevel @userId
		 **/

		$Result = $this->sp('Adm_GetYearLevel',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function generate_report_achievement($Param)
	{
		/**
		 *	SP Example : Adm_GetCetakPerbandinganFormSold
		 **/

		$Result = $this->sp('Adm_GetCetakPerbandinganFormSold',$Param);

		return $Result->result();
	}
	
	function generate_report_form_sold($Param)
	{
		/**
		 *	SP Example : Adm_GetCetakPerkembanganFormSold
		 **/

		$Result = $this->sp('Adm_GetCetakPerkembanganFormSold',$Param);

		return $Result->result();
	}
	
	function generate_report_metrics($Param)
	{
		/**
		 *	SP Example : Adm_GetCetakTabelPerbandinganMatrix
		 **/

		$Result = $this->sp('Adm_GetCetakTabelPerbandinganMatrix',$Param);

		return $Result->result();
	}
	
	function get_admission_term($Param)
	{
		/**
		 *	SP Example : [Adm_GetAdmissionTermMetrics]
		 **/

		$Result = $this->sp('Adm_GetAdmissionTermMetrics',$Param);

		return $Result->result();
	}
}
/*	End	of	file	manager_metrics_report_model.php	*/
/*	Location:		./models/report/manager_metrics_report_model.php */

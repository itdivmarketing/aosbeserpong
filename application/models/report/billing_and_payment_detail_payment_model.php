<?php
class billing_and_payment_detail_payment_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_payment_method()
	{
		/**
		 *	SP Example : Adm_GetPaymentMethod
		 **/
		$Result = $this->sp('Adm_GetPaymentMethod');

		return $Result->result();
	}

	function get_year_level($Param)
	{
		/**
		 *	SP Example : Adm_GetYearLevel @userId
		 **/

		$Result = $this->sp('Adm_GetYearLevel',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function generate_report($Param)
	{
		/**
		 *	SP Example : Adm_GetLaporanDetailPembayaran
		 **/

		$Result = $this->sp('Adm_GetLaporanDetailPembayaran',$Param);

		return $Result->result();
	}
}
/*	End	of	file	billing_and_payment_detail_payment_model.php	*/
/*	Location:		./models/report/billing_and_payment_detail_payment_model.php */

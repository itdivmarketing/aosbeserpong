<?php
class form_sales_cash_report_to_finance_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function generate_report($Param)
	{
		/**
		 *	SP Example : Adm_GetCashReportToFinance
		 **/

		$Result = $this->sp('Adm_GetCashReportToFinance',$Param);

		return $Result->result();
		
	}
}
/*	End	of	file	cash_report_to_finance_model.php	*/
/*	Location:		./models/report/form_sales/cash_report_to_finance_model.php */

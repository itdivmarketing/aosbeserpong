<?php
class billing_and_payment_receipt_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function generate_report($Param)
	{
		/**
		 *	SP Example : Adm_GetKwitansi
		 **/

		$Result = $this->sp('Adm_GetKwitansi',$Param);

		return $Result->result();
	}
}
/*	End	of	file	billing_and_payment_receipt_model.php	*/
/*	Location:		./models/report/billing_and_payment_receipt_model.php */

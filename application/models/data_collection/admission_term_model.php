<?php
class Admission_term_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_admission_term_schedule_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionTerm_Paging
		 **/

		$Result = $this->sp('Adm_GetAdmissionTerm_Paging',$Param);

		return $Result->result();
	}
	
	function get_admission_term_schedule($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionTerm
		 **/

		$Result = $this->sp('Adm_GetAdmissionTerm',$Param);

		return $Result->result();
	}
	
	function set_admission_term_schedule($Param)
	{
		/**
		 *	SP Example : Adm_SetAdmissionTerm
		 **/

		$Result = $this->sp('Adm_SetAdmissionTerm',$Param);

		return $Result->result();
	}
	
	function get_admission_id($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionYear
		 **/

		$Result = $this->sp('Adm_GetAdmissionYear',$Param);

		return $Result->result();
	}
	
	function check_exist_term($Param)
	{
		/**
		 *	SP Example : Adm_CheckExistTerm
		 **/

		$Result = $this->sp('Adm_CheckExistTerm',$Param);

		return $Result->result();
	}
}
/*	End	of	file	entrance_test_schedule_model.php	*/
/*	Location:		./models/user_management/entrance_test_schedule_model.php */

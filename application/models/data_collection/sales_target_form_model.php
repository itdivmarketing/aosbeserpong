<?php
class sales_target_form_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	
	
	
	function get_sales_target_form($Param=null)
	{
		/**
		 *	SP Example : [Adm_GetSalesTargetForm ]
		 **/
		
		$Result = $this->sp('Adm_GetSalesTargetForm ',$Param);

		
		return $Result->result();
	}
	
	

	
	function save_sales_target_form($Param=null)
	{
		/**
		 *	SP Example : [Adm_SaveSalesTargetForm] 
		 **/
		
		$Result = $this->sp('Adm_SaveSalesTargetForm',$Param);
	
		return $Result->result();
	}
		


}
/*	End	of	file	sales_target_form_model.php	*/
/*	Location:		./models/data_collection/sales_target_form_model.php */

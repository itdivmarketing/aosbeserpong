<?php
class form_format_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	
	
	
	function get_form_format_paging($Param=null)
	{
		/**
		 *	SP Example : [Adm_SearchFormFormat_Paging ]
		 **/
		
		$Result = $this->sp('Adm_SearchFormFormat_Paging ',$Param);

		
		return $Result->result();
	}
	
	function get_form_format_view($Param=null)
	{
		/**
		 *	SP Example : [Adm_GetFormFormat] 
		 **/
		
		$Result = $this->sp('Adm_GetFormFormat',$Param);

		
		return $Result->result();
	}
	
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_admissionID_by_school_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDBySchoolLevel 
		 **/

		$Result = $this->sp('Adm_GetAdmisionIDBySchoolLevel',$Param);
	
		
		return $Result->result();
	}
	
	function save_form_format($Param=null)
	{
		/**
		 *	SP Example : [Adm_SaveFormFormat] 
		 **/
		
		$Result = $this->sp('Adm_SaveFormFormat',$Param);
	
		return $Result->result();
	}
		
	function delete_form_format($Param=null)
	{
		/**
		 *	SP Example : [Adm_DeleteFormFormat] 
		 **/
		
		$Result = $this->sp('Adm_DeleteFormFormat',$Param);
	
		return $Result->result();
	}
	

	
	

}
/*	End	of	file	form_format_model.php	*/
/*	Location:		./models/data_collection/form_format_model.php */

<?php
class Master_document_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function get_master_document_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetMasterDocument_Paging
		 **/

		$Result = $this->sp('Adm_GetMasterDocument_Paging',$Param);

		return $Result->result();
	}
	
	function get_master_document($Param)
	{
		/**
		 *	SP Example : Adm_GetMasterDocument
		 **/

		$Result = $this->sp('Adm_GetMasterDocument',$Param);

		return $Result->result();
	}
	
	function set_master_document($Param)
	{
		/**
		 *	SP Example : Adm_SetMasterDocument
		 **/

		$Result = $this->sp('Adm_SetMasterDocument',$Param);

		return $Result->result();
	}
	
	function check_exist_document($Param)
	{
		/**
		 *	SP Example : Adm_CheckExistDocument
		 **/

		$Result = $this->sp('Adm_CheckExistDocument',$Param);

		return $Result->result();
	}
}
/*	End	of	file	entrance_test_schedule_model.php	*/
/*	Location:		./models/user_management/entrance_test_schedule_model.php */

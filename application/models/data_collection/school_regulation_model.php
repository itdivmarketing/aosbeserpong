<?php
class school_regulation_model extends BN_Model {
	function __construct()
	{
		parent::__construct();
	}

	public function insert_school_regulation($Param){
		$Result = $this->sp('Adm_InsertMsSchoolRegulation', $Param);
	}

	public function get_school_regulation($Param=null){
		$Result = $this->sp("Adm_GetMsSchoolRegulation");
		return $Result->result();
	}
	
}

?>
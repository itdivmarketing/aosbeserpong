<?php
class room_test_capacity_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_venue_list()
	{
		/**
		 *	SP Example : [Adm_GetVenueRoomTest] 
		 **/

		$Result = $this->sp('Adm_GetVenueRoomTest');

		return $Result->result();
	}
	function get_add_venue_list()
	{
		/**
		 *	SP Example : [Adm_GetVenue] 
		 **/

		$Result = $this->sp('Adm_GetVenue');

		return $Result->result();
	}

	function get_venue_by_ID_paging($Param)
	{
		/**
		 *	SP Example : [Adm_GetVenueDetailByID_Paging] 
		 **/

		$Result = $this->sp('Adm_GetVenueCapacity_Paging',$Param);

		return $Result->result();
	}
	
	function get_building()
	{
		/**
		 *	SP Example : [Adm_GetBulding] 
		 **/

		$Result = $this->sp('Adm_GetBulding');

		return $Result->result();
	}
	
	function get_view_venue_capacity($Param)
	{
		/**
		 *	SP Example : [Adm_GetVenueCapacityDetail] 
		 **/

		$Result = $this->sp('Adm_GetVenueCapacityDetail',$Param);

		return $Result->result();
	}
	
	function save_venue_capacity($Param)
	{
		/**
		 *	SP Example : [Adm_SaveVenueCapacityData] 
		 **/

		$Result = $this->sp('Adm_SaveVenueCapacityData',$Param);

		return $Result->result();
	}
	
	function delete_venue_capacity($Param)
	{
		/**
		 *	SP Example : [Adm_DeleteVenueCapacity] 
		 **/

		$Result = $this->sp('Adm_DeleteVenueCapacity',$Param);

	}
	

}
/*	End	of	file	room_test_capacity_model.php	*/
/*	Location:		./models/entry/room_test_capacity_model.php */

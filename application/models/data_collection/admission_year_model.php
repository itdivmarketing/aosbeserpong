<?php
class Admission_year_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_admission_year_schedule_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionYear_Paging
		 **/

		$Result = $this->sp('Adm_GetAdmissionYear_Paging',$Param);

		return $Result->result();
	}
	
	function get_admission_year_schedule($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionYear
		 **/

		$Result = $this->sp('Adm_GetAdmissionYear',$Param);

		return $Result->result();
	}
	
	function set_admission_year_schedule($Param)
	{
		/**
		 *	SP Example : Adm_SetAdmissionYear
		 **/

		$Result = $this->sp('Adm_SetAdmissionYear',$Param);

		return $Result->result();
	}
	
	function check_exist_year($Param)
	{
		/**
		 *	SP Example : Adm_CheckExistYear
		 **/

		$Result = $this->sp('Adm_CheckExistYear',$Param);

		return $Result->result();
	}
	
	function check_exist_schedule($Param)
	{
		/**
		 *	SP Example : Adm_CheckExistSchedule
		 **/

		$Result = $this->sp('Adm_CheckExistSchedule',$Param);

		return $Result->result();
	}
}
/*	End	of	file	entrance_test_schedule_model.php	*/
/*	Location:		./models/user_management/entrance_test_schedule_model.php */

<?php
class notification_setting_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_notification_setting_paging($Param)
	{
		/**
		 *	SP Example : [Adm_GetNotificationSetting_Paging] 
		 **/

		$Result = $this->sp('Adm_GetNotificationSetting_Paging',$Param);

		return $Result->result();
	}
	
	function get_notification_type_list()
	{
		/**
		 *	SP Example : [Adm_GetNotificationTypeList] 
		 **/

		$Result = $this->sp('Adm_GetNotificationTypeList');

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}

	
	
	function get_notification_setting_view($Param)
	{
		/**
		 *	SP Example : [Adm_GetNotificationSettingView] 
		 **/

		$Result = $this->sp('Adm_GetNotificationSettingView',$Param);

		return $Result->result();
	}
	
	function save_notification_setting($Param)
	{
		/**
		 *	SP Example : [Adm_SaveNotificationSetting] 
		 **/

		$Result = $this->sp('Adm_SaveNotificationSetting',$Param);

		return $Result->result();
	}
	
	function delete_notification_setting($Param)
	{
		/**
		 *	SP Example : [Adm_DeleteNotificationSetting] 
		 **/

		$Result = $this->sp('Adm_DeleteNotificationSetting',$Param);

	}
	

}
/*	End	of	file	notification_setting_model.php	*/
/*	Location:		./models/entry/notification_setting_model.php */

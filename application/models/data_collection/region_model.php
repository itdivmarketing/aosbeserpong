<?php
class region_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_region_paging($Param)
	{
		/**
		 *	SP Example : [Adm_GetRegion_Paging] 
		 **/

		$Result = $this->sp('Adm_GetRegion_Paging',$Param);

		return $Result->result();
	}
	
	function get_region_all()
	{
		/**
		 *	SP Example : [Adm_GetRegionList] 
		 **/

		$Result = $this->sp('Adm_GetRegionList');

		return $Result->result();
	}
	
	function get_region_view($Param)
	{
		/**
		 *	SP Example : [Adm_GetRegion] 
		 **/

		$Result = $this->sp('Adm_GetRegion',$Param);

		return $Result->result();
	}
	
	function save_region($Param)
	{
		/**
		 *	SP Example : [Adm_SaveRegionData] 
		 **/
		
		$Result = $this->sp('Adm_SaveRegionData',$Param);

		return $Result->result();
	}
	
	function delete_region($Param)
	{
		/**
		 *	SP Example : [Adm_DeleteRegion] 
		 **/

		$Result = $this->sp('Adm_DeleteRegion',$Param);

	}
	

}
/*	End	of	file	region_model.php	*/
/*	Location:		./models/entry/region_model.php */

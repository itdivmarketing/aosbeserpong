<?php
class fee_packet_amount_confirm_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}
	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	

	function get_fee_packet_amount_confirm_paging($Param)
	{
		/**
		 *	SP Example : Adm_SearchFeePacketAmountConfirm_Paging
		 **/

		$Result = $this->sp('Adm_SearchFeePacketAmountConfirm_Paging',$Param);

		return $Result->result();
	}
	
	function get_fee_type()
	{
		/**
		 *	SP Example : Adm_GetFeeType
		 **/

		$Result = $this->sp('Adm_GetFeeType');

		return $Result->result();
	}
	
	function get_packetId_list($Param)
	{
		/**
		 *	SP Example : Adm_GetFeePacketIDList
		 **/

		$Result = $this->sp('Adm_GetFeePacketIDList',$Param);

		return $Result->result();
	}
	
	
	function get_admissionID($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDByPeriod
		 **/

		$Result = $this->sp('Adm_GetAdmissionID',$Param);

		return $Result->result();
	}
	

	

	function get_fee_packet_view($Param)
	{
		/**
		 *	SP Example : Adm_GetFeePacket
		 **/

		$Result = $this->sp('Adm_GetFeePacket',$Param);

		return $Result->result();
	}
	
	function get_fee_packet_amount_confirm_view($Param)
	{
		/**
		 *	SP Example : Adm_GetFeePacketAmountConfirm
		 **/

		$Result = $this->sp('Adm_GetFeePacketAmountConfirm',$Param);

		return $Result->result();
	}
	
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	


	function save_fee_packet_amount_confirm($Param)
	{
		/**
		 *	SP Example : Adm_SaveFeePacketAmountConfirm
		 **/
		
		$Result = $this->sp('Adm_SaveFeePacketAmountConfirm',$Param);

		return $Result->result();
	}
	
	function delete_fee_packet_amount_confirm($Param)
	{
		/**
		 *	SP Example : Adm_DeleteFeePacketAmountConfirm
		 **/
		
		$this->sp('Adm_DeleteFeePacketAmountConfirm',$Param);

		
	}
	
	
}
/*	End	of	file	fee_packet_amount_confirm_model.php	*/
/*	Location:		./models/data_collection/fee_packet_amount_confirm_model.php */

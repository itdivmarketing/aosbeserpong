<?php
class composition_test_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_exam_paging($Param)
	{
		/**
		 *	SP Example : [Adm_GetExam_Paging] 
		 **/

		$Result = $this->sp('Adm_GetExam_Paging',$Param);

		return $Result->result();
	}
	

	
	function get_view_exam($Param)
	{
		/**
		 *	SP Example : [Adm_GetExamDetail] 
		 **/

		$Result = $this->sp('Adm_GetExamDetail',$Param);

		return $Result->result();
	}
	
	function save_exam($Param)
	{
		/**
		 *	SP Example : [Adm_SaveExam] 
		 **/

		$Result = $this->sp('Adm_SaveExamData',$Param);

		return $Result->result();
	}
	
	function delete_exam($Param)
	{
		/**
		 *	SP Example : [Adm_DeleteExam] 
		 **/

		$Result = $this->sp('Adm_DeleteExam',$Param);

	}
}
/*	End	of	file	composition_test_model.php	*/
/*	Location:		./models/entry/composition_test_model.php */

<?php
class form_generate_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	
	
	
	function get_form_generate_paging($Param=null)
	{
		/**
		 *	SP Example : [Adm_SearchFormGenerate_Paging ]
		 **/
		
		$Result = $this->sp('Adm_SearchFormGenerate_Paging ',$Param);

		
		return $Result->result();
	}
	
	function get_form_generate_view($Param=null)
	{
		/**
		 *	SP Example : [Adm_GetFormGenerate] 
		 **/
		
		$Result = $this->sp('Adm_GetFormGenerate',$Param);

		
		return $Result->result();
	}
	
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_admissionID_by_school_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDBySchoolLevel 
		 **/

		$Result = $this->sp('Adm_GetAdmisionIDBySchoolLevel',$Param);
	
		
		return $Result->result();
	}
	
	function insert_form_production($Param=null)
	{
		/**
		 *	SP Example : [Adm_InsertFormProduction] 
		 **/
		
		$Result = $this->sp('Adm_InsertFormProduction',$Param);
	
		return $Result->result();
	}
	
	function save_form_generate($Param=null)
	{
		/**
		 *	SP Example : [Adm_SaveFormGenerate] 
		 **/
		
		$Result = $this->sp('Adm_SaveFormGenerate',$Param);
	
		return $Result->result();
	}
		
	function delete_form_generate($Param=null)
	{
		/**
		 *	SP Example : [Adm_DeleteFormGenerate] 
		 **/
		
		$Result = $this->sp('Adm_DeleteFormGenerate',$Param);
	
		return $Result->result();
	}
	

	
	

}
/*	End	of	file	form_generate_model.php	*/
/*	Location:		./models/data_collection/form_generate_model.php */

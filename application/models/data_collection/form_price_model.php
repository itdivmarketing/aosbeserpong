<?php
class form_price_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	
	
	
	function get_form_price_paging($Param=null)
	{
		/**
		 *	SP Example : Adm_SearchFormPrice_Paging 
		 **/
		
		$Result = $this->sp('Adm_SearchFormPrice_Paging',$Param);

		
		return $Result->result();
	}
	
	function get_form_price_view($Param=null)
	{
		/**
		 *	SP Example : Adm_GetFormPrice 
		 **/
		
		$Result = $this->sp('Adm_GetFormPrice',$Param);

		
		return $Result->result();
	}
	
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_admissionID_by_school_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDBySchoolLevel 
		 **/

		$Result = $this->sp('Adm_GetAdmisionIDBySchoolLevel',$Param);
	
		
		return $Result->result();
	}
	
	function save_form_price($Param=null)
	{
		/**
		 *	SP Example : Adm_SaveFormPrice 
		 **/
		
		$Result = $this->sp('Adm_SaveFormPrice',$Param);
	
		return $Result->result();
	}
		
	function delete_form_price($Param=null)
	{
		/**
		 *	SP Example : Adm_DeleteFormPrice 
		 **/
		
		$Result = $this->sp('Adm_DeleteFormPrice',$Param);
	
		return $Result->result();
	}
	

	
	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}
	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	

}
/*	End	of	file	form_price_model.php	*/
/*	Location:		./models/data_collection/form_price_model.php */

<?php
class room_test_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_venue_paging($Param)
	{
		/**
		 *	SP Example : [Adm_GetVenue_Paging] 
		 **/

		$Result = $this->sp('Adm_GetVenue_Paging',$Param);

		return $Result->result();
	}
	
	function get_building()
	{
		/**
		 *	SP Example : [Adm_GetBulding] 
		 **/

		$Result = $this->sp('Adm_GetBulding');

		return $Result->result();
	}
	
	function get_view_venue($Param)
	{
		/**
		 *	SP Example : [Adm_GetVenueDetail] 
		 **/

		$Result = $this->sp('Adm_GetVenueDetail',$Param);

		return $Result->result();
	}
	
	function save_room_test($Param)
	{
		/**
		 *	SP Example : [Adm_SaveVenueData] 
		 **/

		$Result = $this->sp('Adm_SaveVenueData',$Param);

		return $Result->result();
	}
	
	function delete_venue($Param)
	{
		/**
		 *	SP Example : [Adm_DeleteVenue] 
		 **/

		$Result = $this->sp('Adm_DeleteVenue',$Param);

	}
	

}
/*	End	of	file	room_test_model.php	*/
/*	Location:		./models/entry/room_test_model.php */

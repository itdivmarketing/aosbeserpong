<?php
class List_document_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_process()
	{
		/**
		 *	SP Example : Adm_GetAdmissionProcess
		 **/

		$Result = $this->sp('Adm_GetAdmissionProcess');

		return $Result->result();
	}
	
	function get_document()
	{
		/**
		 *	SP Example : Adm_GetMasterDocument
		 **/

		$Result = $this->sp('Adm_GetMasterDocument');

		return $Result->result();
	}
	
	function get_list_document_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetListDocument_Paging
		 **/

		$Result = $this->sp('Adm_GetListDocument_Paging',$Param);

		return $Result->result();
	}
	
	function get_list_document($Param)
	{
		/**
		 *	SP Example : Adm_GetListDocument
		 **/

		$Result = $this->sp('Adm_GetListDocument',$Param);

		return $Result->result();
	}
	
	function get_admission_id($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionID
		 **/

		$Result = $this->sp('Adm_GetAdmissionID',$Param);

		return $Result->result();
	}
	
	function set_list_document($Param)
	{
		/**
		 *	SP Example : Adm_SetListDocument
		 **/

		$Result = $this->sp('Adm_SetListDocument',$Param);

		return $Result->result();
	}
	
	function save_from_last_year($Param)
	{
		/**
		 *	SP Example : Adm_SetListDocumentLastYear
		 **/

		$Result = $this->sp('Adm_SetListDocumentLastYear',$Param);

		return $Result->result();
	}
}
/*	End	of	file	entrance_test_schedule_model.php	*/
/*	Location:		./models/user_management/entrance_test_schedule_model.php */

<?php
class form_allocation_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_location()
	{
		/**
		 *	SP Example : [Adm_GetLocation] 
		 **/
		
		$Result = $this->sp('Adm_GetLocation ');

		
		return $Result->result();
	}
	
	function get_form_no($Param)
	{
		/**
		 *	SP Example : [Adm_GetFormNo] 
		 **/
		
		$Result = $this->sp('Adm_GetFormNo',$Param);

		
		return $Result->result();
	}
	
	function get_form_allocation_paging($Param=null)
	{
		/**
		 *	SP Example : [Adm_SearchFormAllocation_Paging ]
		 **/
		
		$Result = $this->sp('Adm_SearchFormAllocation_Paging ',$Param);

		
		return $Result->result();
	}
	
	function get_form_allocation_view($Param=null)
	{
		/**
		 *	SP Example : [Adm_GetFormGenerate] 
		 **/
		
		$Result = $this->sp('Adm_GetFormGenerate',$Param);

		
		return $Result->result();
	}
	
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_admissionID_by_school_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDBySchoolLevel 
		 **/

		$Result = $this->sp('Adm_GetAdmisionIDBySchoolLevel',$Param);
	
		
		return $Result->result();
	}
	

	
	function save_form_allocation($Param=null)
	{
		/**
		 *	SP Example : [Adm_SaveFormAllocation] 
		 **/
		
		$Result = $this->sp('Adm_SaveFormAllocation',$Param);
	
		return $Result->result();
	}
		
	function delete_form_allocation($Param=null)
	{
		/**
		 *	SP Example : [Adm_DeleteFormGenerate] 
		 **/
		
		$Result = $this->sp('Adm_DeleteFormGenerate',$Param);
	
		return $Result->result();
	}
	

	
	

}
/*	End	of	file	form_allocation_model.php	*/
/*	Location:		./models/data_collection/form_allocation_model.php */

<?php
class create_binusian_id_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function search($Param)
	{
		/**
		 *	SP Example : Adm_GetRegForCREATEStudentId 
		 **/

		$Result = $this->sp('Adm_GetRegForCREATEStudentId',$Param);

		return $Result->result();
	}
	
	function check_registrant_data($Param)
	{
		/**
		 *	SP Example : Adm_CheckDataRegistrant
		 **/

		$Result = $this->sp('Adm_CheckDataRegistrant', $Param);

		return $Result->result();
	}
	
	function transfer_data_registrant($Param)
	{
		/**
		 *	SP Example : Adm_TransferDataRegistrantToStudent
		 **/

		$Result = $this->sp('Adm_TransferDataRegistrantToStudent', $Param);

		return $Result->result();
	}
	
	function generate_binusian_id($Param)
	{
		/**
		 *	SP Example : Adm_GenerateBiNusianID
		 **/

		$Result = $this->sp('Adm_GenerateBiNusianID', $Param);
		
		return $Result->result();
	}
	
	function get_academic_year()
	{
		/**
		 *	SP Example : Adm_GetAcademicYear
		 **/

		$Result = $this->sp('Adm_GetAcademicYear');
		
		return $Result->result();
	}
	
	function transfer_data_library($Param)
	{
		/**
		 *	SP Example : Adm_TransferDataDbBinusianLibrary
		 **/

		$Result = $this->sp('Adm_TransferDataDbBinusianLibrary', $Param);

		return $Result->result();
	}
}
/*	End	of	file	role_privilege_model.php	*/
/*	Location:		./models/user_management/role_privilege_model.php */

<?php
class Entry_score_per_ms_hs_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_all_registrant_attendance($Param)
	{
		/**
		 *	SP Example : Adm_GetAllRegistrantAttendance @KdMsJadwalET, @termID
		 **/

		$Result = $this->sp('Adm_GetAllRegistrantAttendance',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_schedule($Param)
	{
		/**
		 *	SP Example : Adm_GetEntranceTestScheduleID
		 **/

		$Result = $this->sp('Adm_GetEntranceTestScheduleID',$Param);

		return $Result->result();
	}
	
	function get_current_date($Param)
	{
		/**
		 *	SP Example : Adm_GetCurrentDate
		 **/

		$Result = $this->sp('Adm_GetCurrentDate',$Param);

		return $Result->result();
	}
	
	function set_score($Param)
	{
		/**
		 *	SP Example : Adm_SaveNilaiEntranceTest
		 **/

		$Result = $this->sp('Adm_SaveNilaiEntranceTest',$Param);

		return $Result->result();
	}
	
	function set_score_interview($Param)
	{
		/**
		 *	SP Example : Adm_SaveNilaiP3AEntranceTest
		 **/

		$Result = $this->sp('Adm_SaveNilaiP3AEntranceTest',$Param);

		return $Result->result();
	}
	
	function set_status($Param)
	{
		/**
		 *	SP Example : Adm_SaveFinalScore
		 **/

		$Result = $this->sp('Adm_SaveFinalScore',$Param);

		return $Result->result();
	}
	
	
	
	

}
/*	End	of	file	entry_score_per_ms_hs_model.php	*/
/*	Location:		./models/entrance_test/entry_score_per_ms_hs_model.php */

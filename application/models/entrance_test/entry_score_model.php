<?php
class entry_score_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_type_of_school_list()
	{
		/**
		 *	SP Example : Adm_GetTypeOfPreviousSchool
		 **/
		$Result= $this->sp('Adm_GetTypeOfPreviousSchool');
		return $Result->result();
	}
	
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}
	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}

	
	function get_final_score_status_list()
	{
		/**
		 *	SP Example : Adm_GetLtFinalScoreStatus
		 **/

		$Result= $this->sp('Adm_GetLtFinalScoreStatus')->result();
		return $Result;
		
	}
	
	function get_result_ET_list()
	{
		/**
		 *	SP Example : Adm_GetLtResultEntranceTest
		 **/

		$Result= $this->sp('Adm_GetLtResultEntranceTest')->result();
		return $Result;
		
	}
	
	function get_entry_score_data($Param)
	{
		/**
		 *	SP Example : adm_getDataforentrynilaiet
		 **/

		$Result= $this->sp('adm_getDataforentrynilaiet',$Param)->result();
		return $Result;
		
	}

	function get_form_no($Param){
		$Result= $this->sp('Adm_GetDummyForUD',$Param)->result();
		if(count($Result)>0)
			return $Result[0];
		else 
			return "";
	}

	public function save_entrance_test_score ($Data)
	{
		/**
		 *	SP Example : Adm_SaveNilaiEntranceTest
		 **/
		$this->sp("Adm_SaveNilaiEntranceTest", $Data);
	}

	public function save_final_score ($Data)
	{
		/**
		 *	SP Example : Adm_SaveFinalScore
		 **/
		$this->sp("Adm_SaveFinalScore", $Data);
	}
	
	public function save_interview_score ($Data)
	{
		/**
		 *	SP Example : Adm_SaveNilaiP3AEntranceTest
		 **/
		$this->sp("Adm_SaveNilaiP3AEntranceTest", $Data);
	}
	
	public function save_UD ($Data)
	{
		/**
		 *	SP Example : adm_SaveUpgradeDowngrade
		 **/
		$this->sp("adm_SaveUpgradeDowngrade", $Data);
	}
	
	public function delete_final_score ($Data)
	{
		/**
		 *	SP Example : adm_DeleteFinalScore
		 **/
		$this->sp("adm_DeleteFinalScore", $Data);
	}
	
}
/*	End	of	file	entry_score_model.php	*/
/*	Location:		./models/entrance_test/entry_score_model.php */

<?php
class entrance_test_schedule_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_admission_id($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmissionID @userId
		 **/

		$Result = $this->sp('Adm_GetAdmissionID',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_entrance_test_schedule_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetEntranceTestSchedule_Paging
		 **/

		$Result = $this->sp('Adm_GetEntranceTestSchedule_Paging',$Param);

		return $Result->result();
	}
	
	function get_entrance_test_schedule($Param)
	{
		/**
		 *	SP Example : Adm_GetEntranceTestSchedule
		 **/

		$Result = $this->sp('Adm_GetEntranceTestSchedule',$Param);

		return $Result->result();
	}
	
	function get_subject_entrance_test($Param)
	{
		/**
		 *	SP Example : Adm_GetPreTestSubjectEntranceTest
		 **/

		$Result = $this->sp('Adm_GetPreTestSubjectEntranceTest',$Param);

		return $Result->result();
	}
	
	function get_pre_test_subject()
	{
		/**
		 *	SP Example : Adm_GetPreTestSubject
		 **/

		$Result = $this->sp('Adm_GetPreTestSubject');

		return $Result->result();
	}
	
	function set_entrance_test_schedule($Param)
	{
		/**
		 *	SP Example : Adm_SetEntranceTestSchedule
		 **/

		$Result = $this->sp('Adm_SetEntranceTestSchedule',$Param);

		return $Result->result();
	}
	
	function check_exist_registrant($Param)
	{
		/**
		 *	SP Example : Adm_CheckExistRegistrant
		 **/

		$Result = $this->sp('Adm_CheckExistRegistrant',$Param);

		return $Result->result();
	}
	
	function get_venue()
	{
		/**
		 *	SP Example : Adm_GetVenue
		 **/

		$Result = $this->sp('Adm_GetVenue');

		return $Result->result();
	}
}
/*	End	of	file	entrance_test_schedule_model.php	*/
/*	Location:		./models/user_management/entrance_test_schedule_model.php */

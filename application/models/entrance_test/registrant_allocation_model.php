<?php
class registrant_allocation_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_all_registrant($Param)
	{
		/**
		 *	SP Example : Adm_GetAllRegistrant @KdMsJadwalET, @termID
		 **/

		$Result = $this->sp('Adm_GetAllRegistrant',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_entrance_test_schedule_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetEntranceTestSchedule_Paging
		 **/

		$Result = $this->sp('Adm_GetEntranceTestSchedule_Paging',$Param);

		return $Result->result();
	}
	
	function delete_registrant_allocation($Param)
	{
		/**
		 *	SP Example : Adm_DeletePesertaEntranceTest
		 **/
		
		$Result = $this->sp('Adm_DeletePesertaEntranceTest',$Param);

		return $Result->result();
	}
	
	function save_registrant_allocation($Param)
	{
		/**
		 *	SP Example : Adm_SetEntranceTestSchedule
		 **/
		
		$Result = $this->sp('Adm_SavePesertaEntranceTest',$Param);

		return $Result->result();
	}
}
/*	End	of	file	registrant_allocation_model.php	*/
/*	Location:		./models/entrance_test/registrant_allocation_model.php */

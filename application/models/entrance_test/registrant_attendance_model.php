<?php
class registrant_attendance_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	
	function get_all_registrant_attendance($Param)
	{
		/**
		 *	SP Example : Adm_GetAllRegistrantAttendance @KdMsJadwalET, @termID
		 **/

		$Result = $this->sp('Adm_GetAllRegistrantAttendance',$Param);

		return $Result->result();
	}
	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
	function get_entrance_test_schedule_paging($Param)
	{
		/**
		 *	SP Example : Adm_GetEntranceTestSchedule_Paging
		 **/

		$Result = $this->sp('Adm_GetEntranceTestSchedule_Paging',$Param);

		return $Result->result();
	}
	
	function delete_registrant_attendance($Param)
	{
		/**
		 *	SP Example : Adm_DeleteKehadiranPesertaEntranceTest
		 **/
		
		$Result = $this->sp('Adm_DeleteKehadiranPesertaEntranceTest',$Param);

		return $Result->result();
	}
	
	function save_registrant_attendance($Param)
	{
		/**
		 *	SP Example : Adm_SaveKehadiranEntranceTest
		 **/
		
		$Result = $this->sp('Adm_SaveKehadiranEntranceTest',$Param);

		return $Result->result();
	}
}
/*	End	of	file	registrant_attendance_model.php	*/
/*	Location:		./models/entrance_test/registrant_attendance_model.php */

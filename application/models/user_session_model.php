<?php
class User_session_model extends BN_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function get_user_login_data($Param = NULL)
    {
    	$Result = $this->sp('Adm_GetLoginData', $Param);
    	 
    	return $Result->result();
    }
    
    function get_menu($Param = NULL)
    {
    	$Result = $this->sp('Adm_GetMenu', $Param);
    	
    	return $Result->result();
    }
}

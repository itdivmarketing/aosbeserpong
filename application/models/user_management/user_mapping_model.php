<?php
class User_mapping_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_user_mapping()
	{
		/**
		 *	SP Example : Adm_GetUserMapping   
		 **/

		$Result = $this->sp('Adm_GetUserMapping');

		return $Result->result();
	}
	
	function check_binusian_name($Param)
	{
		/**
		 *	SP Example : Adm_CheckBinusianName  @email
		 **/

		$Result = $this->sp('Adm_CheckBinusianName',$Param);

		return $Result->result();
	}

	function set_user_mapping($Param)
	{
		/**
		 *	SP Example : Adm_SetUserMapping @userchange, @StaffGroupMappingID, @roleID, @delete, @email
		 **/

		$Result = $this->sp('Adm_SetUserMapping', $Param);

		return $Result->result();
	}
}
/*	End	of	file	user_mapping_model.php	*/
/*	Location:		./models/user_management/user_mapping_model.php */

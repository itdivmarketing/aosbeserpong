<?php
class Role_privilege_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_role_privilege($Param)
	{
		/**
		 *	SP Example : Adm_GetRolePrivilege @roleid          
		 **/

		$Result = $this->sp('Adm_GetRolePrivilege',$Param);

		return $Result->result();
	}

	function set_role_privilege($Param)
	{
		/**
		 *	SP Example : Adm_SetRolePrivilege @roleid,  @staffmoduleIDS, @userchange
		 **/

		$Result = $this->sp('Adm_SetRolePrivilege', $Param);

		return $Result->result();
	}
}
/*	End	of	file	role_privilege_model.php	*/
/*	Location:		./models/user_management/role_privilege_model.php */

<?php
class Role_management_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_role()
	{
		/**
		 *	SP Example : Adm_GetRoles
		 **/

		$Result = $this->sp('Adm_GetRoles');

		return $Result->result();
	}
	
	function check_roles($Param)
	{
		/**
		 *	SP Example : Adm_CheckRoles
		 **/

		$Result = $this->sp('Adm_CheckRoles', $Param);

		return $Result->result();
	}

	function set_role($Param)
	{
		/**
		 *	SP Example : Adm_SetRoles @AuditUserName, @roleID = null, @roleName, @delete = '0'
		 **/

		$Result = $this->sp('Adm_SetRoles', $Param);

		return $Result->result();
	}
}
/*	End	of	file	role_management_model.php	*/
/*	Location:		./models/user_management/role_management_model.php */

<?php
class General_setting_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_general_setting($Param)
	{
		/**
		 *	SP Example : Adm_GetGeneralSetting @userId
		 **/

		$Result = $this->sp('Adm_GetGeneralSetting',$Param);

		return $Result->result();
	}

	function set_general_setting($Param)
	{
		/**
		 *	SP Example : Adm_SetGeneralSetting @userId, @period
		 **/

		$Result = $this->sp('Adm_SetGeneralSetting', $Param);

		return $Result->result();
	}
}
/*	End	of	file	general_setting_model.php	*/
/*	Location:		./models/user_management/general_setting_model.php */

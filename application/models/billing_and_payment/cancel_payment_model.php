<?php
class Cancel_payment_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_payment_list($Param)
	{
		/**
		 *	SP Example : Adm_GetPaymentList @Year, @Semester, @FormNo
		 **/
		$Result = $this->sp('Adm_GetPaymentList',$Param);
		//	var_dump( $Result);
		return $Result->result();
	}

	function cancel_payment($Param)
	{
		/**
		 *	SP Example : Adm_CancelPayment @paymentID,@userchange
		 **/

		$Result = $this->sp('Adm_CancelPayment', $Param);
	}
}
/*	End	of	file	role_management_model.php	*/
/*	Location:		./models/user_management/role_management_model.php */

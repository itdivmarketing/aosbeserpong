<?php
class Create_invoice_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_payment_registrant($Param)
	{
		/**
		 *	SP Example : Adm_GetPaymentRegistrant @Period, @RegistrantID, @PackageID
		 **/
		$Result = $this->sp('Adm_GetPaymentRegistrant',$Param);
		return $Result->result();
	}
	
	function get_packages($Param)
	{
		/**
		 *	SP Example : Adm_GetPackages @RegistrantID
		 **/
		$Result = $this->sp('Adm_GetPackages',$Param);
		return $Result->result();
	}
	
	function get_sibling($Param)
	{
		/**
		 *	SP Example : Adm_GetSiblingPaymentRegistrant @RegistrantID
		 **/
		$Result = $this->sp('Adm_GetSiblingPaymentRegistrant',$Param);
		return $Result->result();
	}
	
	function set_payment($Param)
	{
		/**
		 *	SP Example : Adm_SaveTagihanRegistrant @paymentID,@userchange
		 **/

		$Result = $this->sp('Adm_SaveTagihanRegistrant', $Param);
		return $Result->result();
	}
}
/*	End	of	file	create_invoice_model.php	*/
/*	Location:		./models/billing_and_payment/create_invoice_model.php */

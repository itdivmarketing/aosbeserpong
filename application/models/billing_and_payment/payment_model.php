<?php
class Payment_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_payment($Param)
	{
		/**
		 *	SP Example : Adm_GetPayment @Period, @RegistrantID
		 **/
		$Result = $this->sp('Adm_GetPayment',$Param);
		return $Result->result();
	}

	function get_package($Param)
	{
		/**
		 *	SP Example : Adm_GetPackage @Period,@RegistrantID, @FeeTypeID  
		 **/
		$Result = $this->sp('Adm_GetPackage',$Param);
		return $Result->result();
	}
	
	function get_fee_type()
	{
		/**
		 *	SP Example : Adm_GetFeeType
		 **/
		$Result = $this->sp('Adm_GetFeeType');
		return $Result->result();
	}

	function set_payment($Param)
	{
		/**
		 *	SP Example : Adm_SavePembayaranRegistrant @TrPembayaranID, @RegistrantId VARCHAR(13), @FeeTypeId, @AmountBayar,
		 @TanggalBayar, @Notes, @StatusLunas, @PaketId, @AuditUserName, @PaymentMethod
		 **/

		$Result = $this->sp('Adm_SavePembayaranRegistrant', $Param);
	}

	function get_payment_method()
	{
		/**
		 *	SP Example : Adm_GetPaymentMethod
		 **/
		$Result = $this->sp('Adm_GetPaymentMethod');
		return $Result->result();
	}
}
/*	End	of	file	payment_model.php	*/
/*	Location:		./models/billing_and_payment/payment_model.php */

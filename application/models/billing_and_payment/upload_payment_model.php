<?php
class Upload_payment_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_payment($Param)
	{
		$Result = $this->sp('Adm_GetPaymentViaATM',$Param)->result();
		return $Result;
	}

	function set_payment($Param)
	{
		$Result = $this->sp('Adm_SavePembayaranRegistrantViaATM', $Param);
	}
	
}
/*	End	of	file	update_payment_model.php	*/
/*	Location:		./models/billing_and_payment/update_payment_model.php */

<?php
class verify_document_model extends BN_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_year_level($Param=null)
	{
		/**
		 *	SP Example : Adm_GetMsYearLevel 
		 **/
		if($Param!=null)
			$Result = $this->sp('Adm_GetMsYearLevel',$Param);
		else
			$Result = $this->sp('Adm_GetMsYearLevel');
		
		return $Result->result();
	}
	
	function get_pretest_term($Param)
	{
		/**
		 *	SP Example : Adm_GetPretestTerm @userId
		 **/

		$Result = $this->sp('Adm_GetPretestTerm',$Param);

		return $Result->result();
	}
	

	
	
	function get_verify_document($Param)
	{
		/**
		 *	SP Example : [Adm_SearchVerifyDcoument_Paging]
		 **/

		$Result = $this->sp('Adm_SearchVerifyDocument_Paging',$Param);

		return $Result->result();
	}
	
	public function get_view_verify_document ($Param)
	{
		/**
		 *	SP Example : Adm_SearchViewVerifyDocument
		 *	
		 **/
		$Result=$this->sp('Adm_SearchViewVerifyDocument',$Param)->result();
		
		return $Result;

	}
	
	
	function get_admissionID($Param)
	{
		/**
		 *	SP Example : Adm_GetAdmisionIDByPeriod
		 **/

		$Result = $this->sp('Adm_GetAdmissionID',$Param);

		return $Result->result();
	}
	

	
	function get_school_level()
	{
		/**
		 *	SP Example : Adm_GetSchoolLevel
		 **/

		$Result = $this->sp('Adm_GetSchoolLevel');

		return $Result->result();
	}
	
		
	function get_document_upload_email_content($Param)
	{
		/**
		 *	SP Example : adm_GetEmailDocumentStatusContent
		 **/

		$Result = $this->sp('adm_GetEmailDocumentStatusContent',$Param);

		return $Result->row();
	}
	

}
/*	End	of	file	verify_document_model.php	*/
/*	Location:		./models/entry/verify_document_model.php */

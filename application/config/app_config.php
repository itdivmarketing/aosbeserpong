<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['score_ms_hs_path'] = 'D:\Web\PHP\FunctionAdmisi\AdmissionSchoolBackEnd/resources/useruploads/entrance_test/';

$config['form_return_BO_upload_path'] = 'D:\Web\PHP\FunctionAdmisi\AdmissionOnlineSchool/useruploads/admission/';
$config['form_return_BO_view_path'] = 'http://10.200.207.65:3802/useruploads/admission/';
$config['mail_host'] = 'smtp.binus.ac.id';
$config['mail_username'] = '';
$config['mail_password'] = '';
$config['upload_payment_path'] = 'D:\Web\PHP\FunctionAdmisi\AdmissionSchoolBackEnd/resources/useruploads/upload_payment/';
/* End of file app_config.php */
/* Location: ./application/config/app_config.php */

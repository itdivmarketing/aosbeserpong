<?php

	if (!function_exists('alert')) 
    {
		function alert($msg) 
        {
			echo "<script>
                alert('$msg');
            </script>";
		}
	}
    
    if (!function_exists('console_log')) 
	{
		function console_log($msg) 
        {
			echo "<script>
                console.log('$msg');
            </script>";
		}
	}

    if (!function_exists('redirect2')) 
    {
        function redirect2($location) 
        {
			echo "<script>
                document.location= '$location';
            </script>";
		}
	}


    /**
        Puskom.NamaBulan
    */
    if (!function_exists('nama_bulan')) 
    {
		function nama_bulan($bulan) 
        {
			switch($bulan)
            {
                case  "01":
                    return "Januari";
                case  "02":
                    return "Februari";
                case  "03":
                    return "Maret";
                case  "04":
                    return "April";
                case  "05":
                    return "Mei";
                case  "06":
                    return "Juni";
                case  "07":
                    return "Juli";
                case  "08":
                    return "Agustus";
                case  "09":
                    return "September";
                case "10":
                    return "Oktober";
                case  "11":
                    return "November";
                case  "12":
                    return "Desember";
            }
		}
	}
	
	if (!function_exists('nama_hari')) 
    {
		function nama_hari($hari) 
        {
			
		}
	}

	 if (!function_exists('is_logged_in')) {
		function is_logged_in(){
			$CI = &get_instance();
			$user = $CI->session->userdata('UserId');
				if($user =="" || $user == null){
					alert("You Must Login !!!");
					redirect2(base_url().'index.php');
				}
		}
	 }

     //Alter By Wiedy
    if (!function_exists('indonesian_date')) {
        function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
        if (trim ($timestamp) == '')
        {
            $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
            $timestamp = strtotime ($timestamp);
        }
         # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
        );
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
        'Oktober','November','Desember',
        );
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date} {$suffix}";
        return $date;
        } 
     }
 /*	End	of	file	site_helper.php	*/
 /*	Location:		./helpers/site_helper.php */

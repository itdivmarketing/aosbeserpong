<?php
function RecurseNav($parent, $child, $x, $y)
{
    for($i = 0; $i < count($parent); $i++) {
        $x = 0;
        for ($j = 0; $j < count($child); $j++) {
            if($child[$j]->ParentStaffModuleID == $parent[$i]->StaffModuleID){
                $parent[$i]->sub[$x] = $child[$j];
                $x = $x+1;
            }
        }
        @RecurseNav($parent[$i]->sub, $child);
    }
    return $parent;
}
function build_menu($data)
{
    if(!$data)return;
    $x = 0;
    $y = 0;
    for ($i = 0; $i < count($data); $i++) {
        if($data[$i]->ParentStaffModuleID == null)
        {
            $parent[$x] = $data[$i];
            $x = $x + 1;
        }
        else 
        {
            $child[$y] = $data[$i];
            $y = $y + 1;
        }
    }
    if(!isset($child) or  !isset($parent))
    {
        return array();
    }
    else
    {
        $menu = RecurseNav($parent, $child, $x, $y);
        return $menu;
    }
    
    
}
function nested_to_numeric($arr,$res = array())
{
    ;
    foreach($arr as $item)
    {
        if(isset($item->sub))
        {
            $sub = $item->sub;
            $item->sub = true;
            array_push($res, $item);
            $res = nested_to_numeric($sub,$res);
        }
        else
        {
            array_push($res, $item);
        }
        
    }
    return $res;
}
 /*	End	of	file	modules_helper.php	*/
 /*	Location:		./helpers/modules_helper.php */

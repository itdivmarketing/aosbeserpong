<?php
function calculate_pages($total_rows, $rows_per_page, $page_num)
{
    $arr = array();
    // calculate last page
    $last_page = ceil($total_rows / $rows_per_page);
    // make sure we are within limits
    $page_num = (int) $page_num;
    if ($page_num < 1)
    {
       $page_num = 1;
    } 
    elseif ($page_num > $last_page)
    {
       $page_num = $last_page;
    }
    $upto = ($page_num - 1) * $rows_per_page;
    $arr['current'] = $page_num;
    if ($page_num == 1)
        $arr['previous'] = $page_num;
    else
        $arr['previous'] = $page_num - 1;
    if ($page_num == $last_page)
        $arr['next'] = $last_page;
    else
        $arr['next'] = $page_num + 1;
    $arr['last'] = $last_page;
    $arr['info'] = 'Page ('.$page_num.' of '.$last_page.')';
    $arr['pages'] = get_surrounding_pages($page_num, $last_page, $arr['next']);
    return $arr;
}
function get_surrounding_pages($page_num, $last_page, $next)
{
    $arr = array();
    $show = 5; // how many boxes
    // at first
    if ($page_num == 1)
    {
        // case of 1 page only
        if ($next == $page_num) return array(1);
        for ($i = 0; $i < $show; $i++)
        {
            if ($i == $last_page) break;
            array_push($arr, $i + 1);
        }
        return $arr;
    }
    // at last
    if ($page_num == $last_page)
    {
        $start = $last_page - $show;
        if ($start < 1) $start = 0;
        for ($i = $start; $i < $last_page; $i++)
        {
            array_push($arr, $i + 1);
        }
        return $arr;
    }
    // at middle
    $start = $page_num - $show;
    if ($start < 1) $start = 0;
    for ($i = $start; $i < $page_num; $i++)
    {
        array_push($arr, $i + 1);
    }
    for ($i = ($page_num + 1); $i < ($page_num + $show); $i++)
    {
        if ($i == ($last_page + 1)) break;
        array_push($arr, $i);
    }
    return $arr;
}
function pre_debug($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}
function null_setter($val)
{
    $val = trim($val);
    return (isset($val) and !empty($val)) ? trim($val)  :null;
}
function sql_srv_date($d){
    if(is_null($d) or $d == '')
        return null;
    $t = explode('-',$d);
    return $t[2].'-'.$t[1].'-'.$t[0];
}
function validate_not_empty($array){
    $ok = true;
    $array = (array)$array;
    foreach($array as $key => $item){
        if(is_object($item) or is_array($item))
        {
            $item = (array)$item;
            $ok = validate_not_empty($item);
        }
        else if($item === '' or $item === null)
        {
            $ok = false;
        }
    }
    return $ok;
}
function decodeSpecialCharacter($str) {
	$str = str_replace('%20',' ',$str);
	$str = str_replace('~','/',$str);
	$str = str_replace(':','.',$str);
	$str = str_replace('_',',',$str);
	return $str;
}

function moneyFormat($money)
{
	return strrev(implode('.',str_split(strrev(strval($money)),3)));
}

function translateToWords($number) 
{
/*****
     * A recursive function to turn digits into words
     * Numbers must be integers from -999,999,999,999 to 999,999,999,999 inclussive.    
     *
     *  (C) 2010 Peter Ajtai
     *    This program is free software: you can redistribute it and/or modify
     *    it under the terms of the GNU General Public License as published by
     *    the Free Software Foundation, either version 3 of the License, or
     *    (at your option) any later version.
     *
     *    This program is distributed in the hope that it will be useful,
     *    but WITHOUT ANY WARRANTY; without even the implied warranty of
     *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *    GNU General Public License for more details.
     *
     *    See the GNU General Public License: <http://www.gnu.org/licenses/>.
     *
     */
    // zero is a special case, it cause problems even with typecasting if we don't deal with it here
    $max_size = pow(10,18);
    if (!$number) return "zero";
    if (is_int($number) && $number < abs($max_size)) 
    {            
        switch ($number) 
        {
            // set up some rules for converting digits to words
            case $number < 0:
                $prefix = "negative";
                $suffix = translateToWords(-1*$number);
                $string = $prefix . " " . $suffix;
                break;
            case 1:
                $string = "one";
                break;
            case 2:
                $string = "two";
                break;
            case 3:
                $string = "three";
                break;
            case 4: 
                $string = "four";
                break;
            case 5:
                $string = "five";
                break;
            case 6:
                $string = "six";
                break;
            case 7:
                $string = "seven";
                break;
            case 8:
                $string = "eight";
                break;
            case 9:
                $string = "nine";
                break;                
            case 10:
                $string = "ten";
                break;            
            case 11:
                $string = "eleven";
                break;            
            case 12:
                $string = "twelve";
                break;            
            case 13:
                $string = "thirteen";
                break;            
            // fourteen handled later
            case 15:
                $string = "fifteen";
                break;            
            case $number < 20:
                $string = translateToWords($number%10);
                // eighteen only has one "t"
                if ($number == 18)
                {
                $suffix = "een";
                } else 
                {
                $suffix = "teen";
                }
                $string .= $suffix;
                break;            
            case 20:
                $string = "twenty";
                break;            
            case 30:
                $string = "thirty";
                break;            
            case 40:
                $string = "forty";
                break;            
            case 50:
                $string = "fifty";
                break;            
            case 60:
                $string = "sixty";
                break;            
            case 70:
                $string = "seventy";
                break;            
            case 80:
                $string = "eighty";
                break;            
            case 90:
                $string = "ninety";
                break;                
            case $number < 100:
                $prefix = translateToWords($number-$number%10);
                $suffix = translateToWords($number%10);
                $string = $prefix . "-" . $suffix;
                break;
            // handles all number 100 to 999
            case $number < pow(10,3):                    
                // floor return a float not an integer
                $prefix = translateToWords(intval(floor($number/pow(10,2)))) . " hundred";
				$suffix = "";
                if ($number%pow(10,2)) $suffix = " and " . translateToWords($number%pow(10,2));
                $string = $prefix . $suffix;
                break;
            case $number < pow(10,6):
                // floor return a float not an integer
                $prefix = translateToWords(intval(floor($number/pow(10,3)))) . " thousand";
				$suffix = "";
                if ($number%pow(10,3)) $suffix = translateToWords($number%pow(10,3));
                $string = $prefix . " " . $suffix;
                break;
            case $number < pow(10,9):
                // floor return a float not an integer
                $prefix = translateToWords(intval(floor($number/pow(10,6)))) . " million";
				$suffix = "";
                if ($number%pow(10,6)) $suffix = translateToWords($number%pow(10,6));
                $string = $prefix . " " . $suffix;
                break;                    
            case $number < pow(10,12):
                // floor return a float not an integer
                $prefix = translateToWords(intval(floor($number/pow(10,9)))) . " billion";
                if ($number%pow(10,9)) $suffix = translateToWords($number%pow(10,9));
                $string = $prefix . " " . $suffix;    
                break;
            case $number < pow(10,15):
                // floor return a float not an integer
                $prefix = translateToWords(intval(floor($number/pow(10,12)))) . " trillion";
                if ($number%pow(10,12)) $suffix = translateToWords($number%pow(10,12));
                $string = $prefix . " " . $suffix;    
                break;        
            // Be careful not to pass default formatted numbers in the quadrillions+ into this function
            // Default formatting is float and causes errors
            case $number < pow(10,18):
                // floor return a float not an integer
                $prefix = translateToWords(intval(floor($number/pow(10,15)))) . " quadrillion";
                if ($number%pow(10,15)) $suffix = translateToWords($number%pow(10,15));
                $string = $prefix . " " . $suffix;    
                break;                    
        }
    } else
    {
        echo "ERROR with - $number<br/> Number must be an integer between -" . number_format($max_size, 0, ".", ",") . " and " . number_format($max_size, 0, ".", ",") . " exclussive.";
    }
    return $string;    
}

 /*	End	of	file	admission_helper.php	*/
 /*	Location:		./helpers/admission_helper.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class EmployeeData {
    function EmployeeData($Username, $BinusianId, $Email, $Name) 
    {
        $this->Username = $Username;
        $this->BinusianId = $BinusianId;
		$this->Email = $Email;
        $this->Name = $Name;
    }
}
class IsAuthenticated {
    function IsAuthenticated($username, $pwd) 
    {
        $this->username = $username;
        $this->pwd = $pwd;
    }
}

class Soap_library extends CI_Controller 
{
	function __construct()
	{
	
	}
	
	public function isAuth($username, $password) 
	{
		try
		{
			$ini = ini_set("soap.wsdl_cache_enabled","0");
			$soapClient = new SoapClient('http://employee.binus.edu/Service/Common.svc?wsdl',array('style' => 'document','trace' => true ));    
			//echo "<pre>";print_r($soapClient->__getFunctions()); echo "</pre>";
			//echo "<pre>";var_dump($soapClient->__getTypes()); echo "<pre>";
			
			$IsAuthenticated = new IsAuthenticated($username, $password);
			$params = array(
				  "IsAuthenticated" => $IsAuthenticated,
				);
			$response = $soapClient->__soapCall('IsAuthenticated', $params)->IsAuthenticatedResult;
			//echo "<pre>";print_r($response); echo "</pre>";
			//var_dump($result['IsAuthenticatedResult']);
			//echo "Request :<br>", htmlentities($soapClient->__getLastRequest()), "<br>";
			//echo "Response :<br>", htmlentities($soapClient->__getLastResponse()), "<br>";
			
			return $response;
		}
		catch (SoapFault $soapFault) 
		{
			var_dump($soapFault);
			
			return null;
		}
	}
}


/*	End	of	file	SOAP_library.php	*/
/*	Location:		./library/SOAP_library.php */

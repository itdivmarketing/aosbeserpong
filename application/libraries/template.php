<?php
    class Template 
	{
        protected $_CI;
        function __construct()
		{
            $this->_CI =&get_instance();
        }
        
        function display($template,$data=NULL)
        {
        	$data['_JSFile']="{$template}.js";
        	$data['_header']=$this->_CI->load->view('template/header',$data, TRUE);
        	$data['_content']=$this->_CI->load->view("{$template}",$data, TRUE);
        	$data['_footer']=$this->_CI->load->view('template/footer',$data, TRUE);
        	$this->_CI->load->view('template/template',$data);
        }
    }
/*	End	of	file	acquisition.php	*/
/*	Location:		./libraries/template.php	*/

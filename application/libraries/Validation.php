<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI = &get_instance();
$CI->load->library('form_validation');

class Validation extends CI_Form_validation {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function set_data($data)
	{
		// since the core validation library using POST data, we are going to assign object data to POST data
		
		if(is_object($data))
		{
			$data = (array)$data;
		}
		$_POST = $data;
	}
	
	public function run($group = '')
	{
		$result = parent::run($group);
		
		// clean up POST data after running the validation process, we don't want any other thing misusing this data
		$_POST = array();
		
		return $result;
	}
	
	public function set_error($field, $message)
	{
		if (!isset($this->_error_array[$field]))
		{
			$this->_error_array[$field] = $message;
		}
	}
	
	public function error_array()
	{
		return $this->_error_array;
	}
}

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Payment</h2>
				<form name="frmSearch" id="frmSearch" action="<?php echo site_url().'/';?>billing_and_payment/payment/search" method="get">
							<p>
								<label>admission Year : </label>
								<span class="input-wrap">
									<input type="text" name="txtYear" maxLength="4" readonly="readonly" class="numeric" value="<?php echo $year ?>"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span class="input-wrap">
									<input type="text" name="txtSemester" maxLength="1" readonly="readonly" class="numeric" value="<?php echo $semester ?>"/>
								</span>
							</p>
							<p>
								<label>Form No : </label>
								<span class="input-wrap">
									<input type="text" maxlength="13" name="txtFormNumber" class="numeric" value="<?php if(ISSET($_GET['txtFormNumber'])) echo $_GET['txtFormNumber'];?>"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
							
						</form>
						<p>
							<label>Name : </label>
							<span class="input-wrap">
								<input type="text" maxLength="1" readonly="readonly" value="<?php 
							if(isset($PaymentList)){
								if(sizeof($PaymentList)>0)
								{
									echo $PaymentList[0]->RegistrantName;
								}
							} ?>"/>
							</span>
						</p>
						<div class="floatThead-wrapper">
							<table class="bordered" style="margin-top:0;margin-bottom:0;" id="tblView">
								<thead>
									<tr>
										<th>Action</th>
										<th>Payment ID</th>
										<th>Fee Type</th>
										<th>Package Name</th>
										<th>Amount</th>
										<th>Due Date</th>
										<th>Amount Paid</th>
										<th>Payment Date</th>
										<th>Note</th>
									</tr>
								</thead>
								<tbody id="RateContent">
								<?php 
									if(isset($PaymentList)){
										if(sizeof($PaymentList)>0)
										{
											foreach($PaymentList as $Row){ ?>
												<tr data-id="<?php echo $Row->RegistrantID; ?>" 
													data-package="<?php echo $Row->PackageID; ?>" 
													data-paymentid="<?php echo $Row->PaymentID; ?>" 
													data-feetype="<?php echo $Row->FeeTypeID; ?>">
													<td>
														<a class="icon icon-edit btnEdit"></a>
													</td>
													<td><?php echo $Row->PaymentID; ?></td>
													<td><?php echo $Row->FeeTypeName; ?></td>
													<td><?php echo $Row->PackageName; ?></td>
													<td><?php echo $Row->Amount; ?></td>
													<td><?php echo $Row->DueDate; ?></td>
													<td><?php echo $Row->AmountPaid; ?></td>
													<td><?php echo $Row->PaymentDate; ?></td>
													<td><?php echo $Row->Notes; ?></td>
												</tr>
											<?php }
										}
										else
										{?>
											<td colspan="9">There's no Data</td>
										<?php
										}
									}?>
								</tbody>
							</table>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add Payment</h2>
			<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>billing_and_payment/payment/set_payment" method="post">
								<input type="hidden" name="hfRegistrantID" value=""/>
								<input type="hidden" name="hfFeeType" value=""/>
								<input type="hidden" name="hfPackage" value=""/>
								
								<input type="hidden" name="hfPaid" value=""/>
								<input type="hidden" name="hfRemainingPayment" value=""/>
								<p>
									<label>Payment ID : </label>
									<span>
										<input type="text" name="txtPaymentID" value=""  readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Fee Type : </label>
									<span>
										<input type="text" name="txtFeeType" value="" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Package : </label>
									<span>
										<input type="text" name="txtPackage" value="" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Amount : </label>
									<span>
										<input type="text" name="txtAmount" value="" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Due Date : </label>
									<span>
										<input type="text" name="txtDueDate" value="" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Payment Amount : </label>
									<span>
										<input type="text" name="txtAmountPaid" class="numeric" maxlength="9" />
									</span>
								</p>
								<p>
									<label>Payment Date : </label>
									<span class="custom-datepicker">
										<input type="text" name="txtPaymentDate" class="datepicker" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Note : </label>
									<span>
										<textarea type="text" name="txtNote" maxlength="500"></textarea>
									</span>
								</p>
								<p>
									<label>Payment Method : </label>
									<span class="custom-combobox" id="span_ddlPaymentMethod">
										<select name="ddlPaymentMethod">
											<option value="">---Please choose---</option>
											<?php foreach($Payment as $Row): ?>
												<option 
													<?php if(isset($_GET['ddlPaymentMethod']))
													{
														if ($_GET['ddlPaymentMethod'] == $Row->PaymentMethodID )
														echo 'selected="selected"'; 
													}
													else 
													{
														echo '';
													} ?> value="<?php echo $Row->PaymentMethodID;?>"> 
													<?php echo ltrim(rtrim($Row->PaymentMethodName));?> 
												</option>
											<?php endforeach; ?>
										</select>
									</span>
								</p>
								<!-- <p>
									<span>
										<input id="cbNotPaid" type="checkbox" name="cbNotPaid" value="1" />
									</span>
									<label for="cbNotPaid" class="inline">Not Paid</label>
								</p> -->
								<div id="status-box"></div>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();"/>
								</p>
				</form>
		</div>
	</div>
</div>

<style>
.floatThead-wrapper {
    max-height: 500px;
    width: 100%;
    overflow: auto;
}
</style>

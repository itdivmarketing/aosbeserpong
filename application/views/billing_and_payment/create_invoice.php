<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Create Invoice</h2>
				<form name="frmSearch" id="frmSearch" action="<?php echo site_url().'/';?>billing_and_payment/create_invoice/search" method="get">
							<p>
								<label>admission Year : </label>
								<span class="input-wrap">
									<input type="text" name="txtYear" maxLength="4" readonly="readonly" class="numeric" value="<?php echo $year; ?> "/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span class="input-wrap">
									<input type="text" name="txtSemester" maxLength="1" readonly="readonly" class="numeric" value="<?php echo $semester ;?> "/>
								</span>
							</p>
							<p>
								<label>Form No : </label>
								<span class="input-wrap">
									<input type="text" name="txtFormNumber" class="numeric" maxlength="13" value="<?php echo (ISSET($_GET['txtFormNumber']) ? $_GET['txtFormNumber'] : (ISSET($txtFormNumber) ? $txtFormNumber : ''))  ;?>"/>
								</span>
							</p>
							<p>
								<label>Package : </label>
								<span class="custom-combobox">
									<select name="ddlPackage">
										<option value="">--Please Choose--</option>
										<?php if(ISSET($Packages))
										{
											foreach($Packages as $Row): ?>
											<option value="<?php echo $Row->PackageID;?>" 
												<?php echo (isset($_GET['ddlPackage']) ? ($_GET['ddlPackage'] == $Row->PackageID ? 'selected="selected"' : '') : (isset($ddlPackage) ? ($ddlPackage == $Row->PackageID ? 'selected="selected"' : '') : ''));?>> 
												<?php echo $Row->PackageName;?> 
											</option>
										<?php endforeach; 
										}?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
							
						</form>
						<p>
							<label>Name : </label>
							<span class="input-wrap">
								<input type="text" readonly="readonly" value="<?php echo (isset($PaymentList) ? (sizeof($PaymentList)>0 ? $PaymentList[0]->RegistrantName : '') : '');?>"/>
							</span>
						</p>
						<div class="floatThead-wrapper">
							<table class="bordered" style="margin-top:0;margin-bottom:0;" id="tblView2">
								<thead>
									<tr>
										<th>Registrant ID</th>
										<th>Student ID</th>
										<th>Name</th>
										<th>Grade</th>
									</tr>
								</thead>
								<tbody id="RateContent">
								<?php 
									if(isset($Sibling)){
										if(sizeof($Sibling)>0)
										{
											foreach($Sibling as $Row){ ?>
												<tr>
													<td><?php echo $Row->RegistrantID; ?></td>
													<td><?php echo $Row->StudentID; ?></td>
													<td><?php echo $Row->StudentName; ?></td>
													<td><?php echo $Row->Grade; ?></td>
												</tr>
											<?php }
										}
										else
										{?>
											<td colspan="4">There's no Data</td>
										<?php
										}
									}?>
							</table>
						</div>
						</br></br>
						<form name="frmCreateInvoice" action="<?php echo site_url().'/';?>billing_and_payment/create_invoice/set_payment" method="post">
							<div class="floatThead-wrapper">
								<table class="bordered" style="margin-top:0;margin-bottom:0;" id="tblView">
									<thead>
										<tr>
											<th>Fee Type</th>
											<th>Amount</th>
											<th>Due Date</th>
											<th>Disc</th>
											<th>%Disc</th>
											<th>Amount Disc</th>
											<th>Grand Total</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody id=\"RateContent\">
									<?php 
										if(isset($PaymentList)){
											if(sizeof($PaymentList)>0)
											{
												$index = 0;
												foreach($PaymentList as $Row){ ?>
													<tr data-id="<?php echo $Row->RegistrantID; ?>">
														<td>
															<?php echo $Row->FeeTypeName; ?>
															<input type="hidden" name="hfAmount[]" value="<?php echo $Row->Amount;?>" />
															<input type="hidden" name="hfFeeType[]" value="<?php echo $Row->FeeTypeID;?>" />
															<input type="hidden" name="hfRegistrantID[]" value="<?php echo $Row->RegistrantID;?>" />
															<input type="hidden" name="hfPackageID[]" value="<?php echo $Row->PackageID;?>" />
															<input type="hidden" name="hfDueDate[]" value="<?php echo $Row->DueDate;?>" />
															<input type="hidden" name="hfPaymentStatus[]" value="<?php echo $Row->PaymentStatus;?>" />
														</td>
														<td><?php echo $Row->Amount; ?></td>
														<td><?php echo $Row->DueDate; ?></td>
														<td>
															<input type="checkbox" name="cbDiscount[]" class="check" <?php echo ($Row->DiscountFlag == '1' ? 'checked="checked"' : ''); ?> value="<?php echo $index;?>"/>
															<input type="hidden" name="hfDiscount[]" value="<?php echo $index;?>" />
														</td>
														<td><input type="text" name="txtPercentageDiscount[]" maxlength="3" class="txt txtPercentageDiscount numeric" <?php echo ($Row->DiscountFlag == '1' ? '' : 'readonly="readonly"'); ?>  value="<?php echo ($Row->DiscountFlag == '1' ? (isset($Row->PercentageDiscount) ? $Row->PercentageDiscount : '') : '') ?>"/> </td>
														<td><input type="text" name="txtAmountBeforeDiscount[]" class="txt txtAmountDiscount numeric" <?php echo ($Row->DiscountFlag == '1' ? '' : 'readonly="readonly"'); ?>  value="<?php echo ($Row->DiscountFlag == '1' ? (isset($Row->AmountBeforeDiscount) ? $Row->AmountBeforeDiscount : '') : '') ?>"/> </td> 
														<!--<td><input type="text" name="txtAmountBeforeDiscount[]" class="txtAmountDiscount numeric" <?php //echo ($Row->DiscountFlag == '1' ? '' : 'readonly="readonly"'); ?>  value="<?php //echo ($Row->DiscountFlag == '1' ? (isset($Row->AmountBeforeDiscount) ? $Row->AmountBeforeDiscount : '') : '') ?>"/> </td>-->
														<td><input type="text" name="txtNetAmount[]" class="txtNetAmount numeric" readonly="readonly" value="<?php echo ($Row->DiscountFlag == '1' ? (isset($Row->NetAmount) ? $Row->NetAmount : '') : $Row->Amount) ?>"/> </td>
														<td><?php echo ($Row->PaymentStatus == '1' ? 'PAID' : ''); ?></td>
													</tr>
												<?php $index++;}
											}
											else
											{?>
												<td colspan="8">There's no Data</td>
											<?php
											}
										}?>
								</table>
							</div>
							<?php 
									$flag = true;
									if(isset($PaymentList)){
										if(sizeof($PaymentList)>0)
										{
											foreach($PaymentList as $Row)
											{
												if($Row->PaymentStatus == '1')
												{
													$flag = false;
												}
												else
												{
													$flag = true;
													break;
												}
											}
											
										if($flag)
										{
										?>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Save" id="btnSave">
							</p>
							<?php }}} ?>
						</form>

				</div>
			</div>
		</div>
	</div>
</div>

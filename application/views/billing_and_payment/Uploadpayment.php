<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Upload Pembayaran Via Electronic Banking</h2>
				<form name="formBrowse" id="formBrowse" action="<?php echo site_url().'/';?>billing_and_payment/Uploadpayment/upload" method="post">
							<p>
								<label>Select File : </label>
								<span class="input-wrap">
									<input id="UploadFilePayment" type="file" name="datafile" size="40"/>
								</span>
								<label style="text-transform:none; margin-top:10px">Required file (.txt)</label>
							</p>
				</form>
				<div class="floatThead-wrapper">
						<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>billing_and_payment/Uploadpayment/set_payment" method="post">
							<table class="bordered" style="margin-top:0;margin-bottom:0;" id="tblView">
								<thead>
									<tr>
										<th><input type="checkbox" id="chkParent"></th>
										<th>RegistrantID</th>
										<th>Fee Type</th>
										<th>Amount</th>
										<th>Total Amount Paid</th>
										<th>Amount Paid</th>
										<th>Payment Date</th>
										<th>Notes</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>	
							</table>
							<center><input type="submit" id="btnSave" class="button button-primary" value="SAVE" style="margin-top:10px; display:none;"/></center>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Cancel Payment</h2>
				<form name="frmSearch" id="frmSearch" action="<?php echo site_url().'/';?>billing_and_payment/cancel_payment/search_payment_list" method="post">
							<p>
								<label>admission Year : </label>
								<span class="input-wrap">
									<input type="text" name="txtYear" maxLength="4" readonly="readonly" class="numeric" value="<?php echo $year ?> "/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span class="input-wrap">
									<input type="text" name="txtSemester" maxLength="1" readonly="readonly" class="numeric" value="<?php echo $semester ?> "/>
								</span>
							</p>
							<p>
								<label>Form No : </label>
								<span class="input-wrap">
									<input type="text" name="txtFormNo" maxlength="13" class="numeric" value="<?php if(ISSET($formNo)) echo $formNo?>"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
							
						</form>
					<?php 
								if(isset($PaymentList)){
									if(sizeof($PaymentList)>0)
									{
										echo "<p>
											<label>Name : </label>
											<span class=\"input-wrap\">
												<!--label>".$PaymentList[0]->Name."</label-->
												<input type=\"text\" maxLength=\"1\" readonly=\"readonly\" value=\"".$PaymentList[0]->Name."\"/>
											</span>
										</p>";
									}
								}
							?>
							<div class="floatThead-wrapper">
							<form name="frmCancelPayment" action="<?php echo site_url().'/';?>billing_and_payment/cancel_payment/set_payment" method="post">
								<input type="hidden" name="hfTrPaymentID" value="<?php if(isset($formNo)) echo $formNo; ?>"/>
								<input type="hidden" name="hfYear" value="<?php if(isset($formNo)) echo $year; ?>"/>
								<input type="hidden" name="hfSemester" value="<?php if(isset($formNo)) echo $semester; ?>"/>
								<input type="hidden" name="hfFormNo" value="<?php if(isset($formNo)) echo $formNo; ?>"/>
								<table class="bordered"style="margin-top:0;margin-bottom:0;" id="tblPaymentList">
									<thead>
										<tr>
											<th>Payment ID</th>
											<th>Fee Type</th>
											<th>Amount</th>
											<th>Due Date</th>
											<th>Amount Paid</th>
											<th>Payment Date</th>
											<th>Note</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<?php 
										if(isset($PaymentList)){
											if(sizeof($PaymentList)>0)
											{
												echo "<tbody id=\"RateContent\">";
											foreach($PaymentList as $Row){
												echo "<tr data-id=\"echo $Row->PaymentID\">
													<td> $Row->PaymentID</td>
													<td>$Row->FeeType</td>
													<td>$Row->Amount</td>
													<td>$Row->DueDate</td>
													<td>$Row->AmountPaid</td>
													<td>$Row->PaymentDate</td>
													<td>$Row->Notes</td>
													<td><input type=\"checkbox\" name=\"cbCancel[]\" value=\"$Row->PaymentID\"/></td>
												</tr>";
											}
											echo "</tbody>";
											}
											else
											{
												echo "<td colspan=\"8\">There's no Data</td>";
											}
										}
									?>
								</table>
								<?php 
									if(isset($PaymentList)){
										if(sizeof($PaymentList)>0){
											echo "<p align=\"center\" style=\"clear:both;padding-top:20px;\">
												<input type=\"submit\" class=\"button button-primary\" value=\"Save\" id=\"btnSave\">
											</p>";
										}								
									}
								?>
							</form>
						</div>
			</div>
		</div>
	</div>
</div>
<style>
.floatThead-wrapper {
    max-height: 500px;
    width: 100%;
    overflow: auto;
}
</style>



<footer class="footer new-layout" id="footer">

        <section class="footer-widget">
          <div class="container">
            <div class="group-item">
              <div class="item has-padding-right">
                <div class="footer-title">
                  <h2 class="title" title="Social Media tooltip">Social Media</h2>
                </div>
                <div class="social-media">
                  <a href="http://twitter.com/BINUSSerpong" class="icon icon-twitter"></a>
                  <a href="https://www.facebook.com/binus.serpong" class="icon icon-facebook"></a>
                  <a href="http://serpong.binus.sch.id/feed" class="icon icon-rss"></a>
                </div>
              </div>
<!--               <div class="item has-padding">
                <div class="footer-title">
                  <h2 class="title">Feedback &amp; Bug</h2>
                </div>
                <div class="feedback-area">
                  <a href="#" class="button button-primary wide" id="show-form-feedback">Report Feedback &amp; Bug</a>
                  <div class="feedback-popup">
                    <a href="#" class="feedback-close" id="close-form-feedback"></a>
                    <form action="#" method="post">
                      <p>
                        <input type="text" name="input[]" placeholder="Subject">
                      </p>
                      <p>
                        <textarea name="input[]" rowspan="2" placeholder="Message"></textarea>
                      </p>
                      <p>
                        <input type="submit" class="button button-primary wide" value="Send">
                      </p>
                    </form>
                  </div>
                </div>
              </div> -->
              <div class="item has-padding">
                <div class="corporate-area">
                  <h1 class="main-title">BINUS International</h1>
                  <h3 class="pre-title">School Serpong</h3>
                </div>
              </div>
              <div class="item has-padding-left bottom-direction">
                <div class="corporate-area" style="color:#fff;">
                    Jl. Lengkong Karya - Jelupang No. 58 Lengkong Karya <br/>
                    Serpong, Tangerang, Indonesia  <br/>
                    (021) 536 96 96 1 Ext. 5101, 5102, 5103, 5104 <br/>
                    Fax : (+62 - 21) 533 – 0715 
                </div>
              </div>
            </div>
          </div>
        </section><!-- .footer-widget -->

      </footer><!-- #footer -->

    </div><!-- #page -->

    <!-- Vendor -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.browser.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.fancyfields.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.mcustomscrollbar.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.datatables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.datatables.fixedcolumns.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.chosen.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.qtip.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/raphael.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/formalhaut/build/formalhaut.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/multiselect.js"></script>

    <!-- Calendar -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/date.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/jquery.formbubble.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/fullcalendar.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/fullcalendar.viewmore.js"></script>

    <!-- Global -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/script.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/script.navigation.js"></script>

    <!-- Plugins -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/accordion-toggle.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/advanced-combobox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/body-navigation.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/combobox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/editor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/freeze-pane.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/fullcalendar.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/scrollbar.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/tabulation.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/tooltip.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/uploader.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/widgets.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/plugins/multiselect.js"></script>

    <!-- Main JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/main.js"></script>

    <!--Old Plugin for Timepicker or DateTimepicker-->
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/script.js"></script>
    <script src="<?php echo base_url()?>resources/js/core/jqueryUI/ui/jquery-ui.timepicker-addon.js"></script>


    

    <!-- Modules -->

    <!-- BINUS Career
    <link rel="stylesheet" type="text/css" href="modules/binus-career/css/style.css">
    <script type="text/javascript" src="modules/binus-career/js/script.js"></script>-->

    <!-- BINUS LMS
    <link rel="stylesheet" type="text/css" href="modules/binus-lms/css/style.css">
    <script type="text/javascript" src="modules/binus-lms/js/script.js"></script>-->

    <!-- BINUS AMI
    <link rel="stylesheet" type="text/css" href="modules/binus-ami/css/style.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="modules/binus-ami/js/fullcalendar.custom.js"></script>
    <script type="text/javascript" src="modules/binus-ami/js/script.js"></script>-->

    <!-- BINUS Portal
    <link rel="stylesheet" type="text/css" href="modules/binus-portal/css/style.css">-->

  </body>
</html>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>BINUS</title>

	<!-- Favicon -->
	<link rel="Shortcut Icon" type="image/png" href="<?php echo base_url(); ?>resources/images/favicon.ico">

	<!-- Stylesheet -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/additional-screen.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/jquery-ui-timepicker-addon.css">

<!-- Themes -->
	<!--<link rel="stylesheet" type="text/css" href="themes/edu/css/style.css">-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/themes/school/css/style.css">


		<!--[if lt IE 9]>
			<script src="<?php echo base_url(); ?>resources/js/html5shiv.js"></script>
			<![endif]-->
    <!-- JS -->
	<script>
		var BaseURL = '<?php echo base_url() ?>';
		var SiteURL = '<?php echo site_url().'/';?>';
	</script>
	


	<!-- Moment.JS -->
	<script src="<?php echo base_url()?>resources/js/core/moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/additional.js"></script>
	
	<!-- FloatTHead.js for Scrollable Table -->
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery.floatThead.min.js"></script>
	<!-- Loading Dynamic JS for Each View -->
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/admission/<?php echo $_JSFile; ?>"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/ckeditor/ckeditor.js"></script>

	</head>

		<body>
			<div id="page" class="main-container">
				<section class="tagline-banner new-layout">
					<div class="container">
						<div class="wrap">
							<span class="banner">
								<span class="banner-image"></span>
							</span>
							<span class="tagline">People Innovation Excellence</span>
						</div>
					</div>
				</section><!-- .tagline-banner -->

				<header class="header" id="header">
					<div class="container">
						<section class="logo">
							<a href="<?php echo base_url(); ?>index.php/user_session/home">
								<img src="<?php echo base_url(); ?>resources/images/logo.png" alt="Logo">
							</a>
						</section><!-- .logo -->

						<section class="top-navigation">
							<nav class="navigation">
								<div class="container">
									<span class="logo-nav">
										<span class="logo-text">
											<span class="business-unit">BINUS INTERNATIONAL
											<br/> School Serpong</span>
										</span>
									</span>

									<ul class="menu left">
										<li class="expand-menu expand-item-menu">
											<a href="#" class="expand-action-menu">
												<i class="icon icon-burger-menu"></i>
												<span class="label">Menu</span>
											</a>
											<div class="expand-menu-container">
												<ul class="sub-menu main-menu primary-menu" id="menu-list">
													<!--Load by JavaScript-->
												</ul>
											</div>
										</li>
										<li>
											<a>
											Binus School <?php echo $this->session->userData('Period'); ?>
											</a>
										</li>
									</ul>

									<ul class="menu right">
										<li class="expand-notification expand-item has-child">
											<ul class="sub-menu">
												
											
											</ul>
										</li>

										<li class="expand-settings expand-item has-child">
											<a href="#" class="expand-action" style="border-style:none;">
												<span class="label"><?php echo $this->session->userData('Name');?></span>
											</a>
											<ul class="sub-menu">
												<li class="sub-title">Profile</li>
												<li class="setting-content" style="min-height:0px;">
													<div class="details" style="padding-left:10px;">
														<span class="student-name" style="color:#991845;font-size: 14px; "><?php echo $this->session->userData('Name');?></span>
														<span class="email-address"><?php echo $this->session->userData('UserEmail');?></span>
													</div>
												</li>
												<li class="logout">
													<a href="<?php echo site_url(); ?>/user_session/logout">Log Out</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</nav>
						</section><!-- .top-navigation -->
					</div>
			</header><!-- #header -->
						<section class="user-role">
			<div class="container">
				<div class="wrap" style="padding: 20px 0px;">
					<div class="field">
						<span class="label">Login As</span>
						<span class="component">
							<span class="login-role" id="login-role">
								<select name="login-role">
									<option value="Staff">Staff</option>
								</select>
							
						</span>
					</div>
				</div>
			</div>
		</section>
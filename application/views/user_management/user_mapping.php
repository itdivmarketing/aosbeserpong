<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">User Mapping</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmCheckUserMapping" action="<?php echo site_url().'/';?>user_management/user_mapping/check_name" method="post">
							<p>
								<label>Email : </label>
								<span class="input-wrap">
									<input type="text" name="txtStaffEmail" value="<?php echo $Email?>" maxlength="100"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Check" id="btnCheck">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
					</form>
					<form name="frmSetUserMapping" action="<?php echo site_url().'/';?>user_management/user_mapping/set_user_mapping" method="post">
							<input type="hidden" name="hfStaffEmail" value="<?php echo $Email?>" />
							<input type="hidden" name="hfDeleteStatus" />
							<input type="hidden" name="hfStaffGroupMapping" />
							<p>
								<label>Staff Name : </label>
								<span class="input-wrap">
									<input type="text" name="txtStaffName" value="<?php echo $StaffName?>"  readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Role Name : </label>
								<span class="custom-combobox">
									<select name="ddlRole">
										<?php foreach($Roles as $Row): ?>
											<option 
												value="<?php echo $Row->StaffGroupID;?>"> 
												<?php echo $Row->GroupName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<?php 
								if($StaffName != "" && $StaffName != "N/A")
								{
									echo '<p align="center" style="clear:both;padding-top:20px;">
											<input type="submit" class="button button-primary" value="Save" id="btnSave">
										</p>';
									if($IsNew == '0')
										echo '<p align="center" style="clear:both;padding-top:20px;">
											This user has been mapped in database. Click save will overwrite the user role.
										</p>';
								}
								else
								{
									echo '';
								}
							?>
						</form>
					<div class="floatThead-wrapper">
						<table style="margin-top:0;margin-bottom:0;" id="tblUserMapping">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Email</th>
										<th>Staff Name</th>
										<th>Role Name</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php foreach($UserMapping as $Row): ?>
									<tr data-id="<?php echo $Row->StaffGroupMappingID;?> ">
										<td>
											<a class="icon icon-edit btnEdit"></a> &nbsp;
											<a class="icon icon-trash btnDelete"></a>
										</td>
										<td><?php echo $Row->Email; ?></td>
										<td><?php echo $Row->StaffName; ?></td>
										<td><?php echo $Row->GroupName; ?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none">
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
			<h2 class="heading">Role Management</h2>
			<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
				
				<form name="frmLogin" action="<?php echo site_url().'/';?>user_management/role_management/set_role" method="post">
				<p>
					<label>Role Name : </label>
					<span class="input-wrap">
						<input type="hidden" name="hfRoleID"/>
						<input type="hidden" name="hfDeleteStatus"/>
						<input type="text" name="txtRoleName" maxlength="50"/>
					</span>
				</p>
				<p align="center" style="clear:both;padding-top:20px;">
					<input type="submit" class="button button-primary" value="Save" id="btnSave">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
				</p>
				</form>
				<div class="floatThead-wrapper">
				<table class="zebra bordered" style="margin-top:0;margin-bottom:0;" id="tblRole">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Role Name</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php foreach($Roles as $Row): ?>
									<tr data-id="<?php echo $Row->StaffGroupID;?> ">
										<td>
											<a class="icon icon-edit btnEdit"></a>&nbsp;
											<?php if($Row->IsMapped == '0'){ ?>
												<a class="icon icon-trash btnDelete"></a>
											<?php } ?>
										</td>
										<td><?php echo $Row->GroupName; ?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
				</table>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none">
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>

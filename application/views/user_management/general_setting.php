<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">General Setting</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmGeneralSetting" action="<?php echo site_url().'/';?>user_management/general_setting/set_general_setting" method="post">
							<p>
								<label>Year : </label>
								<span class="input-wrap">
									<input type="text" name="txtYear" required pattern=".*\S+.*" maxLength="4" class="numeric" value="<?php echo $year ?>"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span class="input-wrap">
									<input type="text" name="txtSemester" required pattern=".*\S+.*" maxLength="1" class="numeric" value="<?php echo $semester ?>"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Save" id="btnSave">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none">
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
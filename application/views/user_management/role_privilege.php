<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
			<h2 class="heading">Role Privilege</h2>
			<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
				
				<form name="frmRolePrivilege" action="<?php echo site_url().'/';?>user_management/role_privilege/get_privilege" method="get">
							<p>
								<label>Role Name : </label>
								<span class="custom-combobox">
									<select name="ddlRole">
										<?php foreach($Roles as $Row): ?>
											<option 
												<?php if($current == $Row->StaffGroupID ) echo 'selected="selected"'; 
												else echo ''; ?> value="<?php echo $Row->StaffGroupID;?>"> 
												<?php echo $Row->GroupName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
						</form>
				<form name="frmSetRolePrivilege" action="<?php echo site_url().'/';?>user_management/role_privilege/set_privilege" method="post">
					<input type="hidden" name="hfRoleID" value="<?php echo $current; ?>"/>
				<div class="floatThead-wrapper">
				<table class="zebra bordered" style="margin-top:0;margin-bottom:0;" id="tblRole">
									<thead>
										<tr>
											<th>Module Name</th>
											<th>Module Link</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="RateContent">
										<?php foreach($Menu as $Row): ?>
										<tr data-id="<?php echo $Row->StaffModuleID;?> ">
											<td><?php echo $Row->StaffModuleName; ?></td>
											<td><?php echo $Row->StaffModuleUrl; ?></td>
											<td>
												<input type="checkbox" name="cbPrivilege[]" <?php if($Row->Flag == 1) echo 'checked="checked"'; else echo ''; ?> value="<?php echo $Row->StaffModuleID;?>" />
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>		
						</table>
						<p align="center" style="clear:both;padding-top:20px;">
									<input type="submit" class="button button-primary" value="Save" id="btnSave">
									<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
						</p>
				</form>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none">
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>
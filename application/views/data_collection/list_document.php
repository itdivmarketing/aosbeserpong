<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<div class="single-content popup-content" id="target-id">
					<h2 class="heading">List Document</h2>
					<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/list_document/search" method="get">
						<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
						<input type="hidden" name="hfLastYear" value="<?php if(isset($_GET['hfLastYear'])) echo $_GET['hfLastYear']; else echo '1'?>"/>
						<p>
							<label>Admission Year : </label>
							<span>
								<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
							</span>
						</p>
						<p>
							<label>Semester : </label>
							<span>
								<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
							</span>
						</p>
						<p>
							<label>School Level : </label>
							<span class="custom-combobox">
								<select name="ddlSchoolLevel" onchange="restartPage()">
									<option value="">--Please Choose--</option>
									<?php foreach($SchoolLevel as $Row): ?>
										<option 
										<?php if(isset($_GET['ddlSchoolLevel']))
										{
											if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
												echo 'selected="selected"';
										}
										else 
										{
											echo '';
										} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
										<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
									</option>
								<?php endforeach; ?>
							</select>
							</span>
						</p>
						<p>
							<label>Admission Process : </label>
							<span class="custom-combobox">
							<select name="ddlProcess" onchange="restartPage()">
								<option value="">--Please Choose--</option>
								<?php foreach($Process as $Row): ?>
									<option 
									<?php if(isset($_GET['ddlProcess']))
									{
										if ($_GET['ddlProcess'] == $Row->AdmissionProcessID )
											echo 'selected="selected"';
									}
									else 
									{
										echo '';
									} ?> value="<?php echo $Row->AdmissionProcessID;?>"> 
									<?php echo $Row->AdmissionProcessName;?> 
								</option>
							<?php endforeach; ?>
							</select>
							</span>
						</p>
						<p align="center" style="clear:both;padding-top:20px;">
							<input type="button" class="button button-primary" value="Search" id="btnSearch"/>
								<!--<input type="submit" class="button" value="Search" id="btnSearch">
								<input type="reset" class="button" value="Reset" id="btnReset">-->
						</p>
					</form>
					<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
						<thead>
							<tr>
								<th width="65px">Action</th>
								<th>School Level</th>
								<th>Admission Process</th>
								<th>Document</th>
							</tr>
						</thead>
						<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->AdmissionId;?>" 
										data-process="<?php echo $Row->AdmissionProcessID;?>" 
										data-document="<?php echo $Row->DocumentID;?>">
											<td>
												<!--<a class="icon icon-edit btnEdit">Edit</a> -->
												<a class="icon icon-trash btnDelete"></a>
											</td>							
											<td><?php echo ($Row->SchoolLevelName == '' ? '' : $Row->SchoolLevelName); ?></td>
											<td><?php echo ($Row->AdmissionProcessName == '' ? '' : $Row->AdmissionProcessName); ?></td>
											<td><?php echo ($Row->DocumentName == '' ? '' : $Row->DocumentName); ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="8">There\'s no Data</td></tr>';}?>
						</tbody>
					</table>
					<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
						<div class="pagination">
							<span class="page-display"></span>
							<span class="page-number pages">
							</span>
							<span class="page-button">
								<a href="#" class="prev"></a>
								<a href="#" class="next"></a>
							</span>
						</div>

						<?php
						if($data['pagination'])
							echo "<script>
						Show_Pagination({
							'Selector' : '#paging',
							'TotalPage' : ".$data['pagination']['last'].",
							'CurrentPage' : ".$data['pagination']['current'].",
							'PreviousPage' : ".$data['pagination']['previous'].",
							'NextPage' : ".$data['pagination']['next'].",
							'InfoPage' : '".$data['pagination']['info']."'
						});
						</script>";
						?>
					</div>
					<p align="center" style="clear:both;padding-top:20px;">
						<input type="submit" class="button button-primary" value="Add" id="show-popup-internal">
						<?php if(sizeof($data['result']) <= 0) { ?>
							<a href="#" id="btnSearchLastYear" class="button button-primary">Save from Last Year</a>
						<?php } ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div style="display:none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add List Document</h2>
			<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>data_collection/list_document/set_list_document" method="post">
				<input type="hidden" name="hfStatus" value="<?php echo (isset($_POST["hfStatus"]) ? $_POST["hfStatus"] : ''); ?>" />
				<input type="hidden" name="hfAdmissionID" value="<?php echo (isset($_POST["hfAdmissionID"]) ? $_POST["hfAdmissionID"] : ''); ?>" />
				<input type="hidden" name="hfProcessID" value="<?php echo (isset($_POST["hfProcessID"]) ? $_POST["hfProcessID"] : ''); ?>" />
				<input type="hidden" name="hfDocumentID" value="<?php echo (isset($_POST["hfDocumentID"]) ? $_POST["hfDocumentID"] : ''); ?>" />
				<p>
					<label>Admission ID : </label>
					<span>
						<input type="text" name="txtAdmissionID" value="<?php echo (isset($_POST["txtAdmissionID"]) ? $_POST["txtAdmissionID"] : ''); ?>"  readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Admission Year : </label>
					<span> 
						<input type="text" name="txtAdmissionYear" value="<?php echo $year;?>" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Semester : </label>
					<span>
						<input type="text" name="txtAdmissionSemester" value="<?php echo $semester;?>" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>School Level <span class="required">*</span> : </label>
					<span class="custom-combobox">
						<select name="ddlSchoolLevel">
							<option value="" disabled selected>--Please Choose--</option>
							<?php foreach($SchoolLevel as $Row): ?>
							<option value="<?php echo $Row->SchoolLevelID;?>"> 
							<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
							</option>
							<?php endforeach; ?>
						</select>
					</span>
				</p>
				<p>
					<label>Admission Process <span class="required">*</span> :</label>
					<span class="custom-combobox">
						<select name="ddlProcess">
						<option value="" disabled selected>--Please Choose--</option>
						<?php foreach($Process as $Row): ?>
							<option value="<?php echo $Row->AdmissionProcessID;?>"> 
								<?php echo $Row->AdmissionProcessName;?> 
							</option>
						<?php endforeach; ?>
						</select>
					</span>
				</p>
				<p>
					<label>Document <span class="required">*</span> : </label>
					<span class="custom-combobox">
						<select name="ddlDocument">
						<option value="" disabled selected>--Please Choose--</option>
						<?php foreach($Document as $Row): ?>
						<option value="<?php echo $Row->DocumentID;?>"> 
							<?php echo $Row->DocumentName;?>
						</option>
						<?php endforeach; ?>
						</select>
					</span>
				</p>
				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
					<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
				</p>
			</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Fee Packet</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/fee_packet/search" method="get">
					<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
					<p>
						<label>Admission Year : </label>
						<span>
							<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
						</span>
					</p>
					<p>
						<label>Semester : </label>
						<span>
							<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
						</span>
					</p>
					<p>
						<label>School Level : </label>
						<span class="custom-combobox">
							<select name="ddlSchoolLevel">
								<option value="">--Please Choose--</option>
								<?php foreach($SchoolLevel as $Row): ?>
									<option 
									<?php if(isset($_GET['ddlSchoolLevel']))
									{
										if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
											echo 'selected="selected"'; 
									}
									else 
									{
										echo '';
									} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
									<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
								</option>
								<?php endforeach; ?>
								</select>
						</span>
					</p>
					<p>
						<label>Admission Term : </label>
						<span class="custom-combobox">
						<select name="ddlAdmissionTerm">
							<option value="">--Please Choose--</option>
							<?php foreach($PretestTerm as $Row): ?>
								<option 
								<?php if(isset($_GET['ddlAdmissionTerm']))
								{
									if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
										echo 'selected="selected"'; 
								}
								else 
								{
									echo '';
								} ?> value="<?php echo $Row->TermID;?>"> 
								<?php echo $Row->TermID .' - '. $Row->TermName;?> 
							</option>
						<?php endforeach; ?>
					</select>
				    </span>
					</p>
					<p>
						<label>Year Level : </label>
						<span class="custom-combobox">
							<select name="ddlYearLevel">
							<option value="">--Please Choose--</option>
								<?php foreach($YearLevel as $Row): ?>
							<option 
							<?php if(isset($_GET['ddlYearLevel']))
							{
								if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
									echo 'selected="selected"'; 
							}
							else 
							{
								echo '';
							} ?> value="<?php echo $Row->YearLevelID;?>"> 
							<?php echo $Row->YearLevelName;?> 
							</option>
							<?php endforeach; ?>
							</select>
						</span>
				    </p>
					<p align="center" style="clear:both;padding-top:20px;">
						<input type="submit" class="button button-primary" value="Search" id="btnSearch">
						<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
					</p>
			 	</form>
			 	<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Packet ID</th>
										<th>Packet Name</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->PaketID;?>">
											<td style="text-align:center">
												<a class="icon icon-edit btnView"></a> &nbsp;
												<a class="icon icon-trash btnDelete"></a>
											</td>
											<td><?php echo $Row->PaketID; ?></td>
											<td><?php echo $Row->NmPaket; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
				</table>
				<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>


				</div>
				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Add" id="btnAdd">
				</p>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add/Edit Fee Packet</h2>
			<form name="frmAdd" class="styled-content"  action="<?php echo site_url().'/';?>data_collection/fee_packet/save_fee_packet" method="post">
			<input type="hidden" name="hfStatus"/>
			<input type="hidden" name="hfViewPacketID"/>
			<p>
				<label>Admission ID <span class="required">*</span> : </label>
				<input type="text" name="txtViewAdmissionID" value="" readonly="readonly"/>
			</p>
			<p>
				<label>Admission Year <span class="required">*</span> : </label>
				<span>
				<input type="text" name="txtViewAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
				</span>
			</p>
			<p>
				<label>Semester <span class="required">*</span> : </label>
				<span>
				<input type="text" name="txtViewAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
				</span>
			</p>
			<p>
				<label>School Level <span class="required">*</span> : </label>
				<span class="custom-combobox" id="span_ddlViewSchoolLevel">
				<select name="ddlViewSchoolLevel">
					<option value="">--Please Choose--</option>
					<?php foreach($SchoolLevel as $Row): ?>
					<option  value="<?php echo $Row->SchoolLevelID;?>"> 
						<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
					</option>
					<?php endforeach; ?>
				</select>
				</span>
			</p>
			<p>
				<label>Admission Term <span class="required">*</span> : </label>
				<span class="custom-combobox" id="span_ddlViewAdmissionTerm">
					<select name="ddlViewAdmissionTerm">
						<option value="">--Please Choose--</option>
						<?php foreach($PretestTerm as $Row): ?>
							<option 
							value="<?php echo $Row->TermID;?>"> 
							<?php echo $Row->TermID .' - '. $Row->TermName;?> 
						</option>
					<?php endforeach; ?>
					</select>
				</span>
			</p>
			<p>
				<label>Year Level <span class="required">*</span> : </label>
				<span class="custom-combobox" id="span_ddlViewYearLevel">
				<select name="ddlViewYearLevel">
					<option value="">--Please Choose--</option>
					<?php foreach($YearLevel as $Row): ?>
						<option 
						value="<?php echo $Row->YearLevelID;?>"> 
						<?php echo $Row->YearLevelName;?> 
					</option>
				<?php endforeach; ?>
				</select>
				</span>
			</p>
			<p>
				<label>Packet ID <span class="required">*</span> : </label>
				<input type="text" name="txtViewPacketID" value="" maxlength=10 readonly="readonly"/>
			</p>
			<p>
				<label>Packet Name <span class="required">*</span> : </label>
				<input type="text" name="txtViewPacketName" maxlength=200 value="" />
			</p>

			<p align="center" style="clear:both;padding-top:20px;">
				<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
			</p>
			</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
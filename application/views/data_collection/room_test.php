<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<div class="single-content popup-content" id="target-id">
					<h2 class="heading">Exam Room</h2>
						<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/room_test/search" method="get">
						<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
						<p>
        					<label>Building : </label>
        					<span class="custom-combobox">
            					<select name="ddlBuilding">
										<option value="">--Please Choose--</option>
										<?php foreach($Building as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlBuilding']))
												{
													if ($_GET['ddlBuilding'] == $Row->BuildingID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->BuildingID;?>"> 
												<?php echo $Row->BuildingName;?> 
											</option>
										<?php endforeach; ?>
								</select>
							</span>
   						</p>
   						<p>
        						<label>Venue Name : </label>
        						<input type="text" name="txtVenueName" value="<?php if(isset($_GET['txtVenueName'])) echo $_GET['txtVenueName']; else echo "";?>" />
    					</p>
						<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
						</p>
					</form>
					<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Venue ID</th>
										<th>Building</th>
										<th>Venue Name</th>

									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->VenueID;?>">
											<td style="text-align:center">
												<a class="icon icon-edit btnView"></a>
												&nbsp;
												<?php echo $Row->CanBeDeleted==1?'<a style="height:20px; width:20px;" class="icon icon-trash btnDelete"></a>':''; ?>
											</td>
											<td><?php echo $Row->VenueID; ?></td>
											<td><?php echo $Row->BuildingName; ?></td>
											<td><?php echo $Row->VenueName; ?></td>

										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
							<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>


							</div>
							<p align="center" style="clear:both;padding-top:20px;">
								<!--<a href="<?php echo base_url(); ?>index.php/data_collection/room_test #internal-popup" class="button button-primary fancybox-trigger fancybox.ajax" role="button" onclick="Add_popup();" id="btnForm">Add</a>-->
								<input type="submit" class="button button-primary" value="Add" onclick="Popup_add();">
							</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add/Edit Venue</h2>
				<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>data_collection/room_test/save_room_test" method="post">
					<input type="hidden" name="hfVenueID"/>
					<input type="hidden" name="hfStatus"/>

					<p>
						<label>Building<span class="required">*</span> : </label>
						<span class="custom-combobox" id="span_ddlViewBuilding">
							<select name="ddlViewBuilding">
								<option value="" disabled selected>--Please Choose--</option>
								<?php foreach($Building as $Row): ?>
									<option value="<?php echo $Row->BuildingID;?>"> 
										<?php echo $Row->BuildingName;?> 
									</option>
								<?php endforeach; ?>
							</select>
						</span>
					</p>
					<p>
						<label>Venue ID<span class="required">*</span> : </label>
						<span>
							<input type="text" name="txtViewVenueID" value="" maxlength=5 readonly="readonly" />
						</span>
					</p>
					<p>
						<label>Venue Name<span class="required">*</span> : </label>
						<span>
							<input type="text" name="txtViewVenueName" value="" maxlength=100  />
						</span>
					</p>
				</p>
				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate()">
				</p>
				</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
					<?php $this->load->view("data_collection/form_header"); ?>
					<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
						<thead>
							<tr>
								<th>Form No</th>
								<th>Location</th>
								<th>Ready Sold</th>
							</tr>
						</thead>
						<tbody id="RateContent">
							<?php if($data['result']) {
								foreach($data['result'] as $Row): ?>
								<tr data-id="<?php echo $Row->FormNo;?>" >

									<td><?php echo $Row->FormNo; ?></td>
									<td><?php echo $Row->LocationName; ?></td>
									<td><input type="checkbox" <?php echo $Row->FormStatus==0?'checked':''; ?> / onclick="return false"></td>
								</tr>
							<?php endforeach; 
							}
							else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
						</tbody>
					</table>
					<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
					</div>
					<p align="center" style="clear:both;padding-top:20px;">
						<input type="button" class="button button-primary" value="DISTRIBUTION" id="btnAdd">
					</p>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add/Edit Form Generate</h2>
			<form name="frmAdd" class="styled-content"  action="<?php echo site_url().'/';?>data_collection/form_allocation/save_form_allocation" method="post">	
				<input type="hidden" name="hfStatus" />
				<input type="hidden" name="hfViewAdmissionID" />
				<input type="hidden" name="hfViewProductionID" />
				<p>
					<label>Admission ID <span class="required">*</span> : </label>
					<input type="text" name="txtViewAdmissionID" value="" readonly="readonly"/>
				</p>
				<p>
					<label>Admission Year <span class="required">*</span> : </label>
					<span>
						<input type="text" name="txtViewAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Semester <span class="required">*</span> : </label>
					<span>
						<input type="text" name="txtViewAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>

					</span>
				</p>
				<p>
					<label>School Level <span class="required">*</span> : </label>
					<span class="custom-combobox">
						<select name="ddlViewSchoolLevel">
							<option value="">--Please Choose--</option>
							<?php foreach($SchoolLevel as $Row): ?>
								<option 
								value="<?php echo $Row->SchoolLevelID;?>"> 
								<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
							</option>
						<?php endforeach; ?>
						</select>
						<input type="hidden" name="hfViewSchoolLevel" value="" />
					</span>
				</p>
				<p>
					<label>Form No From <span class="required">*</span>: </label>
					<span class="custom-combobox">
						<select class="ddlFormNo" name="ddlViewFrom">
							<option value="">--Please Choose--</option>
						</select>
					</span>
				</p>
				<p>
					<label>To <span class="required">*</span> : </label>
					<span class="custom-combobox">
						<select class="ddlFormNo" name="ddlViewTo">
							<option value="">--Please Choose--</option>
						</select>
					</span>
				</p>
				<p>
					<label>Location : </label>
					<span class="custom-combobox">
						<select name="ddlViewLocation">
							<option value="">--Please Choose--</option>
							<?php foreach($Location as $Row): ?>
								<option 
								value="<?php echo $Row->LocationID;?>"> 
								<?php echo $Row->LocationName;?> 
							</option>
						<?php endforeach; ?>
						</select>
					</span>
				</p>
				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
				</p>
			</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
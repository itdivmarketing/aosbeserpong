<script type="text/javascript" src="/resources/ckeditor/ckeditor.js"></script>
<div id="content">
	<!-- #body-nav -->
	<div id="body" class="styled-content">
		<div class="inside">
			<h1 class="body-title">ADD/EDIT Notification Setting</h1>
			<div class="row">
				<div class="column has-border three-third center">
					<div class="inside">
						<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>data_collection/notification_setting/save_notification_setting" method="post">
								<input type="hidden" name="hfNotificationSettingID"/>
								<input type="hidden" name="hfStatus"/>
								
								
								<p>
									<label>ID : </label>
									<span>
										<input type="text" name="txtViewNotificationSettingID" value="<?php echo isset($NotifData[0]->NotificationSettingID)?$NotifData[0]->NotificationSettingID:""; ?>" readonly="readonly" />
									</span>
								</p>
								<p>
									<label>Notification Type : </label>
									<span>
										<select name="ddlViewNotificationType">
										<option value="">--Please Choose--</option>
										<?php foreach($NotificationType as $Row): ?>
											<option 
												<?php if(isset($NotifData[0]->NotificationTypeID))
												{
													if ($NotifData[0]->NotificationTypeID == $Row->NotificationTypeID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->NotificationTypeID;?>"> 
												<?php echo $Row->Description; ?> 
											</option>
										<?php endforeach; ?>
									</select>
									</span>
								</p>
								<p>
									<label>School Level : </label>
									<span>
									<select name="ddlViewSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($NotifData[0]->SchoolLevelID))
												{
													if ($NotifData[0]->SchoolLevelID == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelName; ?> 
											</option>
										<?php endforeach; ?>
									</select>
									</span>
								</p>
								<p>
									<label>Email To : </label>
									<span>
										<input type="text" name="txtViewEmailTo" value="<?php echo isset($NotifData[0]->EmailTo)?$NotifData[0]->EmailTo:""?>" maxlength=50  />
									</span>
								</p>
								<p>
									<label>Email CC : </label>
									<span>
										<input type="text" name="txtViewEmailCC" value="<?php echo isset($NotifData[0]->EmailCC)?$NotifData[0]->EmailCC:""?>" maxlength=50  />
									</span>
								</p>
								<p>
									<label>Email BCC : </label>
									<span>
										<input type="text" name="txtViewEmailBCC" value="<?php echo isset($NotifData[0]->EmailBCC)?$NotifData[0]->EmailBCC:""?>" maxlength=50  />
									</span>
								</p>
								<p>
									<label>Email Subject : </label>
									<span>
										<input type="text" name="txtViewEmailSubject" value="<?php echo isset($NotifData[0]->EmailSubject)?$NotifData[0]->EmailSubject:""?>" maxlength=50  />
									</span>
								</p>
								<p>
								<label>Email Body : </label>
								<?php echo $this->ckeditor->editor("txtViewEmailBody",isset($NotifData[0]->EmailBody)?$NotifData[0]->EmailBody:"Enter Your Email Body"); ?>
								</p>
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button" value="Save" id="btnSave">
									<a href="<?php echo site_url().'/data_collection/notification_setting'; ?>"><input type="button" class="button" value="Back" id="btnBack"></a>
								</p>
							</form>
					</div>
					
					
				
				</div>
			</div>
		</div>
	</div>
</div>

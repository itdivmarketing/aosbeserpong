<script>


$(document).ready(function()
{
	var param=$('#ActiveTab').val();

		switch(param){
			case "fee_packet_amount":
				$(".nav-head li:nth-child(1)").addClass( "current");
				//$( "#icon-profile" ).addClass( "active");
				break;
			case "fee_packet_amount_confirm":	
				$(".nav-head li:nth-child(2)").addClass( "current");	
				//$( "#icon-background" ).addClass( "active");
				break;
		}
		
		// $(".TabLink").click(function(e) {
		// 	//Do stuff when clicked
		// 	e.preventDefault();
		// 	var SchoolLevelID=$("[name='ddlSchoolLevel']").val();
		// 	var YearLevelID=$("[name='ddlYearLevel']").val();
		// 	var TermID=$("[name='ddlAdmissionTerm']").val();
		// 	var PacketID=$("[name='txtPacketID']").val();
		// 	var link=$(this).prop("href");
			
		// 	window.location.href = link+SchoolLevelID+'&ddlYearLevel='+YearLevelID+'&ddlAdmissionTerm='+TermID+'&txtPacketID='+PacketID;
	
		// });
});

</script>
<h2 class="heading">Fee Packet Amount</h2>
<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/<?php echo $TabName;?>/search" method="get">
	<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
	<p>
		<label>Admission Year : </label>
		<span>
			<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
		</span>
	</p>
	<p>
		<label>Semester : </label>
		<span>
			<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
		</span>
	</p>
	<p>
		<label>School Level : </label>
		<span class="custom-combobox">
			<select name="ddlSchoolLevel">
				<option value="">--Please Choose--</option>
				<?php foreach($SchoolLevel as $Row): ?>
				<option 
					<?php if(isset($_GET['ddlSchoolLevel']))
					{
					if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
						echo 'selected="selected"'; 
					}
					else 
					{
						echo '';
					} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
					<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
				</option>
				<?php endforeach; ?>
			</select>
		</span>
	</p>
	<p>
		<label>Admission Term : </label>
		<span class="custom-combobox">
			<select name="ddlAdmissionTerm">
					<option value="">--Please Choose--</option>
					<?php foreach($PretestTerm as $Row): ?>
					<option 
						<?php if(isset($_GET['ddlAdmissionTerm'])){
							if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
								echo 'selected="selected"'; 
							}
							else {
								echo '';
							} ?> value="<?php echo $Row->TermID;?>"> 
						<?php echo $Row->TermID .' - '. $Row->TermName;?> 
					</option>
					<?php endforeach; ?>
			</select>
		</span>
	</p>
	<p>
		<label>Year Level : </label>
		<span class="custom-combobox">
			<select name="ddlYearLevel">
				<option value="">--Please Choose--</option>
				<?php foreach($YearLevel as $Row): ?>
				<option 
				<?php if(isset($_GET['ddlYearLevel']))
				{
					if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
						echo 'selected="selected"'; 
					}else {
						echo '';
					} ?> value="<?php echo $Row->YearLevelID;?>"> 
						<?php echo $Row->YearLevelName;?> 
				</option>
				<?php endforeach; ?>
			</select>
		</span>
	</p>
	<p>
		<label>Packet ID : </label>
			<span>
				<input type="text" name="txtPacketID" value="<?php if(isset($_GET['txtPacketID'])) echo $_GET['txtPacketID']; else echo "";?>" />
			</span>
			</p>
			<p align="center" style="clear:both;padding-top:20px;">
				<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
			</p>
</form>
<section class="body-navigation" style="margin-bottom: 50px;">
				<div class="nav-head">
					<input name="ActiveTab" id="ActiveTab" type="hidden" value="<?php if(isset($TabName)) echo  $TabName; else echo ""; ?>">
					<div class="container" style="width:100%">
						<ul>
							<li>
								<a href="<?php echo site_url().'/';?>data_collection/fee_packet_amount/search?ddlSchoolLevel=<?php echo ""; ?>">
									<span class="label">Fee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo site_url().'/';?>data_collection/fee_packet_amount_confirm/search?ddlSchoolLevel=<?php echo ""; ?>">
									<span class="label">To Be Confirm</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
</section>

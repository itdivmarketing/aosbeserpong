<script>


$(document).ready(function()
{
$('.datepicker').datepicker({ dateFormat: 'dd M yy' });
	var param=$('#ActiveTab').val();

		switch(param){
			case "form_price":
				$(".nav-head li:nth-child(1)").addClass( "current");
				//$( "#icon-profile" ).addClass( "active");
				break;
			case "form_format":	
				$(".nav-head li:nth-child(2)").addClass( "current");	
				//$( "#icon-background" ).addClass( "active");
				break;
			case "form_generate":	
					$(".nav-head li:nth-child(3)").addClass( "current");
					//$( "#icon-parent" ).addClass( "active");
				break;
			case "form_allocation":
					$(".nav-head li:nth-child(4)").addClass( "current");
					//$( "#icon-guardian" ).addClass( "active");
				break;
		}
		
		
		// $(".TabLink").click(function(e) {
		// 	//Do stuff when clicked
		// 	e.preventDefault();
		// 	var SchoolLevelID=$("[name='ddlSchoolLevel']").val();
		// 	var link=$(this).prop("href");
			
		// 	window.location.href = link+SchoolLevelID;
		// 	//$("#link2").prop('href',"www.google.com");
		// });
	
});

</script>
<h2 class="heading">Register Form</h2>
<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/<?php echo $TabName; ?>/search" method="get">
	<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
	<p>
		<label>Admission Year : </label>
		<span>
			<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
		</span>
	</p>
	<p>
		<label>Semester : </label>
		<span>
			<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
		</span>
	</p>
	<p>
		<label>School Level : </label>
		<span class="custom-combobox">
			<select name="ddlSchoolLevel">
			<option value="">--Please Choose--</option>
			<?php foreach($SchoolLevel as $Row): ?>
			<option <?php if(isset($_GET['ddlSchoolLevel'])){
				if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
					echo 'selected="selected"'; 
				}
				else {
					echo '';
				} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
			<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
			</option>
			<?php endforeach; ?>
			</select>
			</span>
	</p>					
	<p align="center" style="clear:both;padding-top:20px;">
		<input type="submit" class="button button-primary" value="Search" id="btnSearch">
		<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
	</p>
</form>

<section class="body-navigation" style="margin-bottom: 50px;">
				<div class="nav-head">
					<input name="ActiveTab" id="ActiveTab" type="hidden" value="<?php if(isset($TabName)) echo  $TabName; else echo ""; ?>">
					<div class="container">
						<ul>
							<li>
								<a href="<?php echo site_url().'/';?>data_collection/form_price/search?ddlSchoolLevel=<?php echo ""; ?>">
									<span class="label">Price</span>
								</a>
							</li>
							<li>
								<a href="<?php echo site_url().'/';?>data_collection/form_format/search?ddlSchoolLevel=<?php echo ""; ?>">
									<span class="label">Format Form</span>
								</a>
							</li>
							<li>
								<a href="<?php echo site_url().'/';?>data_collection/form_generate/search?ddlSchoolLevel=<?php echo ""; ?>">
									<span class="label">Generate Form</span>
								</a>
							</li>
							<li>
								<a href="<?php echo site_url().'/';?>data_collection/form_allocation/search?ddlSchoolLevel=<?php echo ""; ?>">
									<span class="label">Distribution</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
</section>


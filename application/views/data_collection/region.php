<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Region</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/region/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Region Name : </label>
								<span>
									<input type="text" name="txtRegionName" value="<?php if(isset($_GET['txtRegionName'])) echo $_GET['txtRegionName']; else echo "";?>" />
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
				</form>
				<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Region ID</th>
										<th>Region Name</th>

									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->RegionID;?>">
											<td style="text-align:center">
												<a class="icon icon-edit btnView"></a> &nbsp;
												<a class="icon icon-trash btnDelete"></a>
											</td>
											<td><?php echo $Row->RegionID; ?></td>
											<td><?php echo $Row->RegionName; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
				</table>
				<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Add" id="btnAdd">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add/Edit Region</h2>
			<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>data_collection/region/save_region" method="post">
								<input type="hidden" name="hfRegionID"/>
								<input type="hidden" name="hfStatus"/>
								
								
								<p>
									<label>Region ID* : </label>
									<span>
										<input type="text" name="txtViewRegionID" value="" maxlength=3 readonly="readonly" />
									</span>
								</p>
									<p>
									<label>Region Name* : </label>
									<span>
										<input type="text" name="txtViewRegionName" value="" maxlength=50  />
									</span>
								</p>
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
								</p>
							</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>



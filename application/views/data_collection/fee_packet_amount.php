<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<?php $this->load->view("data_collection/fee_packet_amount_header"); ?>
				<h2 class="heading">Fee Packet</h2>
				<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Packet ID</th>
										<th>Fee Type</th>
										<th>Amount</th>
										<th>Due Date</th>
										<th>Type Of Payment</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->PaketID;?>" second-data-id="<?php echo $Row->FeeTypeID;?>">
											<td style="text-align:center">
												<a class="icon icon-edit btnView"></a> &nbsp;
												<a class="icon icon-trash btnDelete"></a>
											</td>
											<td><?php echo $Row->PaketID; ?></td>
											<td><?php echo $Row->FeeTypeName; ?></td>
											<td><?php echo $Row->Amount; ?></td>
											<td><?php echo $Row->TanggalJatuhTempo; ?></td>
											<td><?php echo $Row->TypeOfPayment; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
				</table>
				<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>


				</div>

				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Add" id="btnAdd">			
				</p>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add/Edit Fee Packet</h2>
			<form name="frmAdd" id="frmAdd" class="styled-content"  action="<?php echo site_url().'/';?>data_collection/fee_packet_amount/save_fee_packet_amount" method="post">
								<input type="hidden" name="hfStatus"/>
								<input type="hidden" name="hfViewPacketID"/>
								<input type="hidden" name="hfViewFeeTypeID"/>
								
								<p>
									<label>Admission ID <span class="required">*</span> : </label>
									<input type="text" name="txtViewAdmissionID" value="" readonly="readonly"/>
								</p>
								<p>
									<label>Admission Year <span class="required">*</span> : </label>
									<span>
										<input type="text" name="txtViewAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Semester <span class="required">*</span> : </label>
									<span>
										<input type="text" name="txtViewAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
									</span>
								</p>
									<p>
								<label>School Level <span class="required">*</span> : </label>
								<span class="custom-combobox" id="span_ddlSchoolLevel">
									<select name="ddlViewSchoolLevel" disabled=true>
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												 value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term <span class="required">*</span> : </label>
								<span class="custom-combobox" id="span_ddlViewAdmissionTerm">
									<select name="ddlViewAdmissionTerm" disabled=true>
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												 value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level <span class="required">*</span> : </label>
								<span class="custom-combobox" id="span_ddlViewYearLevel">
									<select name="ddlViewYearLevel" disabled=true>
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
											 value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Packet ID <span class="required">*</span> : </label>
								<span class="custom-combobox" id="span_ddlViewPacketID">
									<select name="ddlViewPacketID">
										<option value="">--Please Choose--</option>
										<?php foreach($PacketID as $Row): ?>
											<option 
											 value="<?php echo $Row->PaketID;?>"> 
												<?php echo $Row->NmPaket;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Fee Type <span class="required">*</span> : </label>
								<span class="custom-combobox" id="span_ddlViewFeeType">
									<select name="ddlViewFeeType">
										<option value="">--Please Choose--</option>
										<?php foreach($FeeType as $Row): ?>
											<option 
											 value="<?php echo $Row->FeeTypeID;?>"> 
												<?php echo $Row->FeeTypeName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							
							<p>
								<label>Amount <span class="required">*</span> : </label>
								<input type="text" name="txtViewAmount" maxlength=18 value="" />
							</p>
							
							<p>
								<label>Due Date <span class="required">*</span> : </label>
								<span class="custom-datepicker">
           							<input type="text" class="datepicker" name="txtViewDueDate" maxlength=11 value="" readonly=true />
            						<span class="icon-area"></span>
   								</span>
							</p>
							<p>
								<label>Type Of Payment : </label>
								<input type="text" name="txtViewTypeOfPayment" maxlength=200 value="" />
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
								</p>
							</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>

<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
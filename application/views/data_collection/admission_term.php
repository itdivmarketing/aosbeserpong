<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<div class="single-content popup-content" id="target-id">
					<h2 class="heading">Admission Term</h2>
					<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/admission_term/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $term;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox" id="ddlSchoolLevel">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<p>
							<label>Admission ID : </label>
							<span>
								<input type="text" name="txtAdmissionID" value="<?php echo (isset($admissionID) ? $admissionID : '');?>" readonly="readonly"/>
							</span>
						</p>
						<p>
							<label>School Date : </label>
							<span>
								<input type="text" name="txtSchoolDate" value="<?php echo (isset($schoolDate) ? $schoolDate : '');?>" readonly="readonly"/>
							</span>
						</p>
						<p>
							<label>Effective Date : </label>
							<span>
								<input type="text" name="txtEffectiveDate" value="<?php echo (isset($effectiveDate) ? $effectiveDate : '');?>" readonly="readonly"/>
							</span>
						</p>
						<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Term ID</th>
										<th>Term Name</th>
										<th>Term Number</th>
										<th>Opening Date</th>
										<th>Closing Date</th>
										<th>Entrance Test</th>
										<th>Announcement Date</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if(isset($data['result'])) {
										
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->TermID;?> ">
											<td>
												<a class="icon icon-edit btnEdit"></a>
												&nbsp;
												<a class="icon icon-trash btnDelete"></a>
											</td>							
											<td><?php echo ($Row->TermID == '' ? '' : $Row->TermID); ?></td>
											<td><?php echo ($Row->TermName == '' ? '' : $Row->TermName); ?></td>
											<td><?php echo ($Row->TermOrder == '' ? '' : $Row->TermOrder); ?></td>
											<td><?php echo ($Row->OpeningDate == '' ? '' : $Row->OpeningDate); ?></td>
											<td><?php echo ($Row->ClosingDate == '' ? '' : $Row->ClosingDate); ?></td>
											<td><?php echo ($Row->PreTestDate == '' ? '' : $Row->PreTestDate); ?></td>
											<td><?php echo ($Row->AnnouncementDate == '' ? '' : $Row->AnnouncementDate); ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="8">There\'s No Data. Use the filter above for search the data.</td></tr>';}?>
								</tbody>
							</table>
							<div id="paging" <?php if(isset($data['pagination'])) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
									<span class="page-display"></span>
									<span class="page-number pages"></span>
									<span class="page-button">
											<a href="#" class="prev"></a>
											<a href="#" class="next"></a>
									</span>
								</div>
							<?php
								if(isset($data['pagination']))
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
							</div>
							<?php if(isset($admissionID)){ ?>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Add" onclick="Popup_add();">
							</p>
							<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display:none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add/Edit Admission Term</h2>
			<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>data_collection/admission_term/set_admission_term_schedule" method="post">
				<input type="hidden" name="hfStatus"  />
				<input type="hidden" name="hfTermID" />
				<input type="hidden" name="hfAdmissionID" />
				<p>
					<label>Term ID : </label>
					<span>
						<input type="text" name="txtTermID" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Term Name : </label>
					<span> 
						<input type="text" name="txtTermName" maxlength="10" />
					</span>
				</p>
				<p>
					<label>Term Number : </label>
					<span> 
						<input type="text" name="txtTermOrder" maxlength="3" class="numeric" value="<?php echo (isset($_POST["txtTermOrder"]) ? $_POST["txtTermOrder"] : '');?>"/>
					</span>
				</p>
				<p>
					<label>Opening Date : </label>
					<span class="custom-datepicker">
						<input type="text" name="txtOpeningDate" class="datepicker" readonly="readonly"/>
					</span>

				</p>
				<p>
					<label>Closing Date : </label>
					<span class="custom-datepicker">
						<input type="text" name="txtClosingDate" class="datepicker" readonly="readonly"/>
					</span>

				</p>
				<p>
					<label>Entrance Test : </label>
					<span class="custom-datepicker">
						<input type="text" name="txtEntranceTest" class="datepicker" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Announcement Date : </label>
					<span class="custom-datepicker">
						<input type="text" name="txtAnnouncementDate"  class="datepicker" readonly="readonly"/>
					</span>
				</p>

				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
					<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
				</p>
			</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->
</div>
<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){

				?>
				alert("<?php echo $message?>");
				// $.fancybox({
				// 	href: "#dialog-content-status",
				// 	minWidth    : 400,
				// 	fitToView   : true,
				// 	closeClick  : false,
				// 	afterShow	: function(){
				// 		$('#message').html("<?php echo $message?>");
				// 	}
				// });
				<?php
			}
		}
		?>
	});
</script>
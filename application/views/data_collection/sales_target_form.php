<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Sales Target</h2>
				<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
				<p>
					<label>Admission Year : </label>
					<span>
						<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Semester : </label>
					<span>
						<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
					</span>
				</p>
				<form name="frmSave" id="frmSave" action="<?php echo site_url().'/';?>data_collection/sales_target_form/save_sales_target_form" method="post">
						<div class="floatThead-wrapper" style="">
							<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="30%">School Level Name</th>
										<th width="30%">Year Level Name</th>
										<th width="20%">Form Sold</th>
										<th width="20%">Intake</th>	
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										$FormSoldTotal=0;
										$IntakeTotal=0;
										foreach($data['result'] as $Row):?>
										<tr data-id="<?php echo "";?>">
											
											<td><?php echo $Row->SchoolLevelName ?><input name="hfSchoolLevelID[]" type="hidden" value="<?php echo $Row->SchoolLevelID ?>" /></td>
											<td><?php echo $Row->YearLevelName; ?><input name="hfYearLevelID[]" type="hidden" value="<?php echo $Row->YearLevelID ?>" /></td>
											<td><input name="txtFormSold[]" class="txtNumber txtFormSold" maxlength=10 type="text" value="<?php echo $Row->FormSold!==NULL?$Row->FormSold:0; ?>"/></td>
											<td><input name="txtIntake[]" class="txtNumber txtIntake" maxlength=10 type="text" value="<?php echo $Row->Intake!==NULL?$Row->Intake:0; ?>"/></td>
										</tr>
										<?php $FormSoldTotal+=(int)$Row->FormSold;
											  $IntakeTotal+=(int)$Row->Intake;
										?>
									<?php endforeach; 
										
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
									
								</tbody>
							</table>
						</div>
						<div class="floatThead-wrapper">	
							<table id="tblViewSum" style="margin-top:0;margin-bottom:0;">
								
									<tr>
										<th width="60%" ><span style="float:right"><b>Total</b></span></th>
										<th width="20%"><input readonly="readonly" name="txtTotalFormSold" id="txtTotalFormSold" type="text" value="<?php echo $FormSoldTotal; ?>"/></th>
										<th width="20%"><input  readonly="readonly" name="txtTotalIntake"  id="txtTotalIntake"  type="text" value="<?php echo $IntakeTotal; ?>"/></th>	
									</tr>
							</table>
						</div>
						<script>
							$('#tblViewSum').css('width',($('#tblView').width())+'px');
						</script>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button button-primary" value="Save" id="btnSave">
								<input type="button" class="button button button-primary" value="RESET" id="btnReset">
							</p>
							</form>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<Style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</Style>



<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
					<?php $this->load->view("data_collection/form_header"); ?>

					<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
						<thead>
							<tr>
								<th width="65px">Action</th>
								<th>School Level</th>
								<th>Production Date</th>
								<th>Form Produced</th>
								<th>First Form No</th>
								<th>Last Form No</th>
								<th>Dummy</th>
							</tr>
						</thead>
						<tbody id="RateContent">
							<?php if($data['result']) {
								foreach($data['result'] as $Row): ?>
								<tr data-id="<?php echo $Row->ProductionID;?>"  data-second-id="<?php echo $Row->AdmissionID;?>">
									<td style="text-align:center">
										<a class="icon icon-view btnView"></a> &nbsp;
										<?php echo $Row->CanBeDeleted==1?'<a style="height:20px; width:20px;" class="icon icon-trash btnDelete"></a>':''; ?>
									</td>
									<td><?php echo $Row->SchoolLevelName; ?></td>
									<td><?php echo $Row->ProductionDate; ?></td>
									<td><?php echo $Row->TotalForms; ?></td>
									<td><?php echo $Row->StartForm; ?></td>
									<td><?php echo $Row->EndForm; ?></td>
									<td><input type="checkbox" <?php echo $Row->IsDummy==1?'checked':''; ?> /></td>
								</tr>
							<?php endforeach; 
							}
							else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
						</tbody>
					</table>

					<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
					</div>
					<p align="center" style="clear:both;padding-top:20px;">
						<input type="button" class="button button-primary" value="Add" id="btnAdd">	
					</p>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add / View Form Generate</h2>
			<form name="frmAdd" class="styled-content"  action="<?php echo site_url().'/';?>data_collection/form_generate/save_form_generate" method="post">	
				<input type="hidden" name="hfStatus" />
				<input type="hidden" name="hfViewAdmissionID" />
				<input type="hidden" name="hfViewProductionID" />
				<p>
					<label>Admission ID <span class="required">*</span> : </label>
					<input type="text" name="txtViewAdmissionID" value="" readonly="readonly"/>
				</p>
				<p>
					<label>Admission Year <span class="required">*</span> : </label>
					<span>
						<input type="text" name="txtViewAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>Semester <span class="required">*</span> : </label>
					<span>
						<input type="text" name="txtViewAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>

					</span>
				</p>
				<p>
					<label>School Level <span class="required">*</span> : </label>
					<span class="custom-combobox">
						<select name="ddlViewSchoolLevel">
							<option value="">--Please Choose--</option>
							<?php foreach($SchoolLevel as $Row): ?>
								<option 
								value="<?php echo $Row->SchoolLevelID;?>"> 
								<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
							</option>
						<?php endforeach; ?>
					</select>
					<input type="hidden" name="hfViewSchoolLevel" value="" />
					</span>
				</p>
				<p>
					<label>Production Date <span class="required">*</span> : </label>
					<span>
						<input type="text" class="datepicker" name="txtViewProductionDate" value="" maxlength=11/>

					</span>
				</p>
				<p>
					<label>Total Form Produced <span class="required">*</span>: </label>
					<span>
						<input type="text" name="txtViewTotalForms" value="" maxlength=3/>

					</span>
				</p>
				<p>
					<label>Is Dummy : </label>
					<span>
						<input type="checkbox" name="cbxViewIsDummy" value="" />

					</span>
				</p>
				<p>
					<label>Is Online : </label>
					<span>
						<input type="checkbox" name="cbxViewIsOnline" value="" />

					</span>
				</p>
				<p align="center" style="clear:both;padding-top:20px;">
					<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate()">
				</p>
			</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>


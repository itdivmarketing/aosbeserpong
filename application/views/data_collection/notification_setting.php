<script type="text/javascript" src="/resources/ckeditor/ckeditor.js"></script>
<div id="content">
	<!-- #body-nav -->
	<div id="body" class="styled-content">
		<div class="inside">
			<h1 class="body-title">Notification Setting</h1>
			<div class="row">
				<div class="column has-border three-third center">
					<div class="inside">
						<form name="frmSearch" action="<?php echo site_url().'/';?>data_collection/notification_setting/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Notification Type : </label>
								<span>
									<select name="ddlNotificationType">
										<option value="">--Please Choose--</option>
										<?php foreach($NotificationType as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlNotificationType']))
												{
													if ($_GET['ddlNotificationType'] == $Row->NotificationTypeID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->NotificationTypeID;?>"> 
												<?php echo $Row->Description; ?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Notification Type : </label>
								<span>
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelName; ?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Email To : </label>
								<span>
									<input type="text" name="txtEmailTo" value="<?php if(isset($_GET['txtEmailTo'])) echo $_GET['txtEmailTo']; else echo "" ;?>" />
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
					</div>
					<div class="inside">
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>ID</th>
										<th>Notification Type</th>
										<th>School Level</th>
										<th>Email To</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->NotificationSettingID;?>">
											<td style="text-align:center">
												<a class="icon icon-view btnView" href="<?php echo site_url().'/data_collection/notification_setting/view/'.$Row->NotificationSettingID; ?>" >Edit</a> &nbsp;
												<a class="icon icon-delete btnDelete">Delete</a>
											</td>
											<td><?php echo $Row->NotificationSettingID; ?></td>
											<td><?php echo $Row->Description; ?></td>
											<td><?php echo $Row->SchoolLevelName; ?></td>
											<td><?php echo $Row->EmailTo; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
						</div>
						
								<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
								<div class="prev-next-nav">
								  <a href="#" class="prev-nav icon icon-pagination-left disable">
									Prev
								  </a>
								  <a href="#" class="next-nav icon icon-pagination-right">
									Next
								  </a>
								</div><!-- .prev-next-nav -->
								<div class="pagenavi">
								  <div class="the-navi">
								  </div>
								  <div class="pages"></div>            
								</div>
							  </div>
								</div>
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
							
							<p align="center" style="clear:both;padding-top:20px;">
										<a href="<?php echo site_url().'/data_collection/notification_setting/view/'; ?>" ><input type="button" class="button" value="Add" ></a>
								
							</p>
		
					</div>
			
					<div id="internal-popup" style="display:none;">
						<h2 class="popup-title">Add/Edit Notification Setting</h2>
						<div class="popup-content">
							<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>data_collection/notification_setting/save_notification_setting" method="post">
								<input type="hidden" name="hfNotificationSettingID"/>
								<input type="hidden" name="hfStatus"/>
								
								
								<p>
									<label>ID : </label>
									<span>
										<input type="text" name="txtViewNotificationSettingID" value="" readonly="readonly" />
									</span>
								</p>
								<p>
									<label>Notification Type : </label>
									<span>
										<select name="ddlViewNotificationType">
										<option value="">--Please Choose--</option>
										<?php foreach($NotificationType as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlNotificationType']))
												{
													if ($_GET['ddlNotificationType'] == $Row->NotificationTypeID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->NotificationTypeID;?>"> 
												<?php echo $Row->Description; ?> 
											</option>
										<?php endforeach; ?>
									</select>
									</span>
								</p>
								<p>
									<label>School Level : </label>
									<span>
									<select name="ddlViewSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelName; ?> 
											</option>
										<?php endforeach; ?>
									</select>
									</span>
								</p>
								<p>
									<label>Email To : </label>
									<span>
										<input type="text" name="txtViewEmailTo" value="" maxlength=50  />
									</span>
								</p>
								<p>
									<label>Email CC : </label>
									<span>
										<input type="text" name="txtViewEmailCC" value="" maxlength=50  />
									</span>
								</p>
								<p>
									<label>Email BCC : </label>
									<span>
										<input type="text" name="txtViewEmailBCC" value="" maxlength=50  />
									</span>
								</p>
								<p>
									<label>Email Subject : </label>
									<span>
										<input type="text" name="txtViewEmailSubject" value="" maxlength=50  />
									</span>
								</p>
								<p >
								<label>Email Body : </label>
								<?php echo $this->ckeditor->editor("txtViewEmailBody","Enter your Email Body Here"); ?>
								</p>
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button" value="Save" id="btnSave">
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!doctype html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
	<link href="<?php echo base_url(); ?>resources/css/receipt.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>resources/css/receipt-print.css" media="print" rel="stylesheet" type="text/css" />
	<?php $this->load->helper('admission_helper');?>
   </head>
	<body>
		<div class="print-wrap">
            <div class="print-frame">
                <img src="<?php echo base_url('resources/images/kwitansi.jpg') ?>" alt="">
            </div>
        </div>   	
            <div class="print-content">
                <div class="print-details">
                    <p>
                        <label></label><?php echo (isset($report[0]) ? $report[0]->ReceiptId : ''); ?>
                    </p>
                    <p>
                        <label></label><?php echo (isset($report[0]) ? $report[0]->DueDate : ''); ?>
                    </p>
                    <p>
                        <label></label><?php echo (isset($report[0]) ? $report[0]->PaidBy : ''); ?>
                    </p>
                    <p>
                        <label></label><?php echo (isset($report[0]) ? $report[0]->RegistrantId : ''); ?>
                    </p>
					<p>
                        <label></label><?php echo (isset($report[0]) ? $report[0]->SchoolLevel : ''); ?>
                    </p>
                    <p class="h13">
                        <label></label><?php $total = 0;  ?>
                    </p>
					<p class="h13">
                        <label></label><span id="payment-child"><?php echo (isset($report[0]) ? ($report[0]->SchoolLevelID != '003' && $report[0]->SchoolLevelID != '004' ? 'X' : '') : ''); ?></span>
						<span><?php echo (isset($report[0]) ? ($report[0]->SchoolLevelID != '003' && $report[0]->SchoolLevelID != '004' ? ' '.moneyFormat($report[0]->AmountPaid) : '') : '') ; (isset($report[0]) ? $total+=$report[0]->AmountPaid : $total+=0); ?></span>
                    </p>
					<p class="h13">
                        <label></label><span id="payment-child"><?php echo (isset($report[0]) ? ($report[0]->SchoolLevelID == '003' || $report[0]->SchoolLevelID == '004' ? 'X' : '') : ''); ?></span>
						<span><?php echo (isset($report[0]) ? ($report[0]->SchoolLevelID == '003' || $report[0]->SchoolLevelID == '004' ? ' '.moneyFormat($report[0]->AmountPaid) : '') : '') ;?></span>
                    </p>
					<p>
                        <label></label><?php  ?>
                    </p>
					<p>
                        <label></label><?php  ?>
                    </p>
					<p>
                        <label></label><?php  ?>
                    </p>
					<p>
                        <label></label><?php  ?>
                    </p>
					<p>
                        <label></label><?php  ?>
                    </p>
                    <p>
                        <label></label><span ></span>
                    </p>
					<p>
                        <label></label><span id="payment-child">&nbsp;&nbsp;</span><span><?php echo ' '.moneyFormat($total); ?></span>
                    </p>
					<p>
                        <label></label><span ></span>
                    </p>
					<div class="print" style="margin-top:13px;">
						<label style="padding-left: 300px;"><?php echo translateToWords($total).' rupiahs'; ?> </label>
						<p>
							<label></label><span ></span>
						</p>
						<label style="padding-left: 300px;"><?php echo (isset($report[0]) ? $report[0]->Descriptions : ''); ?></label><br/>
						<p>
							<label></label><span ></span>
						</p>
						<label style="padding-left: 300px;"><?php echo 'Paid by'; ?></label>
						<p>
							<label> &nbsp;</label><span ></span>
						</p>
							<label style="padding-left: 300px;"><?php echo (isset($report[0]) ? '('.$report[0]->PaidBy.')' : ''); ?></label>
							<label style="padding-left: 550px;"><?php echo (isset($report[0]) ? $report[0]->RecepientName : ''); ?></label>
					</div>
					
					<div class="screen">
						<label style="padding-left: 200px;"><?php echo translateToWords($total).' rupiahs'; ?> </label>
						<p>
							<label></label><span ></span>
						</p>
						<label style="padding-left: 200px;"><?php echo (isset($report[0]) ? $report[0]->Descriptions : ''); ?></label><br/>
						<label style="padding-left: 200px;"><?php echo 'Paid by'; ?></label>
						<p>
							<label></label><span ></span>
						</p>
						<label style="padding-left: 200px;"><?php echo (isset($report[0]) ? '('.$report[0]->PaidBy.')' : ''); ?></label>
						<label style="padding-left: 330px;"><?php echo (isset($report[0]) ? $report[0]->RecepientName : ''); ?></label>
					</div>
                </div>
            </div>
	</body>
</html>

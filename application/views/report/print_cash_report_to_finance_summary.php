<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div div width="100%" style="text-align:center;">
				<b>FORM SALES BINUS INTERNATIONAL SCHOOL SERPONG <br/>
				ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+=1); ?><br/>
				PERIOD <?php echo $_GET['txtStartPeriod']; ?> - <?php echo $_GET['txtEndPeriod']; ?></b>
			</div>
			<br/>
			<br/>
			<br/>
			<?php if(isset($report))
			{
				$total = 0;
				$amountallocated = 0;
				$i = 0;
				$len = count($report);
			?>
			<table>
				<thead>
					<tr>
						<th>School Level</th>
						<th>Total Form</th>
						<th>Total Amount</th>
					</tr>
				</thead>
				<tbody id="RateContent">
			<?php
			foreach($report as $Row): 
				?>
					<tr>
						<td><?php echo $Row->SchoolLevelName; ?></td>
						<td><?php echo $Row->total; $total+=$Row->total; ?></td>
						<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->AmountAllocated)),3))); $amountallocated += $Row->AmountAllocated; ?></td>
					</tr>
			<?php
				if($i == $len-1)
				{
				?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total</th>
						<th><?php echo $total ; ?></th>
						<th><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($amountallocated)),3))); ?></th>
					</tr>
				</tfoot>
			</table>
			<br/>
			<br/>
			<?php
				}
			$i++;
			endforeach;
			}
			else //no data
			{
				
			}
			?>
						<div>
				<b>Note : Funds will be transfered directly to the Finance Departement</b>
				</br>
				</br>
				</br>
				</br>
				</br>
				</br>
			</div>
			<div width="100%" style="float:left;">
				Jakarta, <?php echo $currentDate[0]->CurrDate;?> </br>
			</div>
			</br>
			<div width="100%" align="center">
				<div width="50%" style="float:left; display:block;">
					Created By,
					</br>
					</br>
					</br>
					</br>
					______________________</br>
					Marketing Manager
				</div>
				<div width="50%" style="float:right; display:block;">
					Approved By,
					</br>
					</br>
					</br>
					</br>
					<u><?php echo $_GET['txtFinancialController'];?></u></br>
					Financial Controller
				</div>
			</div>
		</div>
	</body>
	<br/>
	</br>
		</br>
		</br>
		</br>
		<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Cash_Report_Summary.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<?php $this->load->helper('admission_helper');?>
	<body>
		<div id="data">
			<div width="30%">
			
				<p>
					Jakarta, <?php echo $printedDate[0]->CurrDate; ?><br/>
					No : <?php echo $_GET['txtCode'].'/'.$_GET['txtLetterNumber']; ?><br/>
					Subject : Notification of Full Payment Confirmation
				</p>
			</div>
			<?php if(isset($report))
			{
			?>
			<div width="100%" style="text-align:left;">
				<p>
					Dear Mr. & Mrs. <?php echo $report[0]->FatherName.' '.$report[0]->MotherName?>, <br/>
					<?php echo $report[0]->Address;?><br/>
					<?php echo $report[0]->City.' '.$report[0]->PostalCode;?><br/>
				</p>
				<table>
					<thead>
						<tr>
							<th>Date</th>
							<th>Amount</th>
							<th>Remarks</th>
						</tr>
					</thead>
					<?php
						$countLength = sizeof($report)+1;
						$count=1;
						$total = 0;
						foreach ($report as $Row) :
							if($count == 1)
							{
					?>
								<tr>
									<td rowspan="<?php echo $countLength; ?>">&nbsp;</td>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount);$total+=$Row->NetAmount; ?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							else
							{
					?>
								<tr>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount); $total+=$Row->NetAmount;?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							if ($count == $countLength) 
							{
								$count = 1;
							}
							else $count++;
						endforeach;
					?>
							<tr>
								<td colspan="2"><b><?php echo 'Rp '.moneyFormat($total);?></b>
									<?php
										$flag = false;
										for($i = 0 ; $i < sizeof($report) ; $i++)
										{
											if($report[$i]->FlagDiskon == '1')
											{
												$flag=true;
											}
										}
										
										if($flag) echo '(includes 10% of Sibling Discount)';
									?>*
								</td>
							</tr>
						</table>
						<p>
							<i>
								*) All the payment that have been made are non-refundable, cannot be transferred to other candidates and cannot be claimed for any other reasons.
							</i>
						</p>
					<?php
						if(isset($_GET['cbSpecial']))
						{
					?>
						<p>The School will send a reminder letter for the second installment as follow :</p>
						<table>
							<thead>
								<tr>
									<th>Amount **</th>
									<th>Remarks</th>
								</tr>
							</thead>
					<?php
							$total = 0;
							foreach ($package as $Row) :
					?>
								<tr>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount);$total+=$Row->FirstAmount; ?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							endforeach;
					?>
							<tr>
								<td colspan="2"><b><?php echo 'Rp '.moneyFormat($total);?></b>
								<?php echo (isset($package[0]->NoteDiskon) ? $package[0]->NoteDiskon : '');?>
								</td>
							</tr>
						
				</table>
				<p>
					<i>
						**) The amount does not include the 10% sibling discount. Sibling discount is subject to terms and conditions
					</i>
				</p>
				<?php }?>
				<p>
					You're most welcome to contact us at any time if you have any inquiries.<br/>
					Thank you for your kind attention and cooperation.
				</p>
				<br/>
				</br>
				<p>
					Yours Sincerely,<br/>
					<img src="<?php echo base_url();?>resources/images/digital_signature.jpg" width="100px;" /><br/>
					Marketing Manager<br/>
					<i>KSY/adm</i>
				</p>
			</div>
			<?php 
			}?>
		</div>
	</body>
	<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/billing_and_payment_full_payment_confirmation/print_report/">
		<input type="hidden" name="hfContent" />
	<a href="#" id="btnExport" class="button">Export To PDF</a> 
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

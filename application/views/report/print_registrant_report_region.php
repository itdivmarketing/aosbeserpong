<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					REGISTRANT REGION REPORT <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo $AcademicSemester; ?> <br/>
				</b>
			</div>
			<?php $total = 0;
			if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = $report[0]->SchoolLevelId;
				$total = 0;
			?>
			<div width="100%">
				School Level : <?php echo $report[0]->SchoolLevelName;?><br/>
				Year Level : <?php echo ($_GET['ddlYearLevel'] == 'all' ? $_GET['ddlYearLevel'] : $report[0]->YearLevelName)?><br/>
			</div>
			<table width="100%">
				<thead>
						<tr>
							<th>Region</th>
							<th>Count of Registrant</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $Row):
						if($Row->SchoolLevelId == $SchoolLevel)
						{
				?>
								<tr>

									<td><?php echo $Row->Region?></td>
									<td><?php echo $Row->TotalRegistrant; $total += $Row->TotalRegistrant;?></td>
								</tr>
				<?php
						}
						else
						{
							$SchoolLevel = $Row->SchoolLevelId;
				?>
				</tbody>
				<tfoot>
						<tr>
							<th> TOTAL</th>
							<th><?php echo $total;?></th>
						</tr>
				</tfoot>
			</table>
			<div style="page-break-after:always;">&nbsp;</div>
			<div id="repeat">
				<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div width="100%" style="text-align:center;">
					<b>
						REGISTRANT REGION REPORT <br/>
						ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo $AcademicSemester; ?> <br/>
					</b>
				</div>
			</div>
			<div width="100%">
				School Level : <?php echo $Row->SchoolLevelName;?><br/>
				Year Level : <?php echo ($_GET['ddlYearLevel'] == 'all' ? $_GET['ddlYearLevel'] : $report[0]->YearLevelName); $total = 0;?><br/>
			</div>
			<table width="100%">
				<thead>
					<tr>
						<th>Region</th>
						<th>Count of Registrant</th>
					</tr>
				</thead>
				<tbody id="RateContent">
					<tr>
						<tr>
							<td><?php echo $Row->Region?></td>
							<td><?php echo $Row->TotalRegistrant; $total += $Row->TotalRegistrant;?></td>
						</tr>
					</tr>
				<?php
						}
					endforeach;
			}
			else
			{
				?>
					<tr>
						<td colspan="3">There's No Data</td>
					</tr>
				<?php
			}
				?>
				<tfoot>
					<tr>
						<th> TOTAL</th>
						<th><?php echo $total;?></th>
					</tr>
				</tfoot>
				</tbody>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Validate_Form.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

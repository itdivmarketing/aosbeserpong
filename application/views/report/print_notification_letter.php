<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
   </head>
	<?php $this->load->helper('admission_helper');?>
	<body>
		<div id="data">
			<div width="30%">
				<p>
					Jakarta, <?php echo $printedDate[0]->CurrDate; ?><br/>
					No : <?php echo $_GET['txtCode'].'/'.$_GET['txtLetterNumber']; ?>
				</p>
			</div>
			<div width="100%" style="text-align:center;">
					<b><u>Subject : Notification Letter of Entrance Test Result </u></b>
			</div>
			<?php if(isset($report))
			{
			?>
			<div width="100%" style="text-align:left;">
				<p>
					Dear Mr. & Mrs. <?php echo $report[0]->FatherName.' '.$report[0]->MotherName?>, <br/>
				</p>
				<p>
					Please read the following notification carefully <br/>
					Based on the Final Entrance Test Result, we would like to inform you, that your child: <br/>
				</p>
				<p style="padding-left:30px;">
						Name			: <?php echo $report[0]->RegistrantName.' ('.$report[0]->RegistrantID.')'; ?> <br/>
						Academic Level	: <?php echo $report[0]->AdmissionYear; ?> <br/>
						Result			: Recommended <br/>
				</p>
				<p>
					Based on the School Fee Payment Agreement Letter that had been signed, we would be most grateful if you would proceed to the next step. Please follow the instructions below concerning payment procedure:<br/>
				</p>
				<table>
					<thead>
						<tr>
							<th>Due Date</th>
							<th>Amount</th>
							<th>Type of Payment</th>
						</tr>
					</thead>
					<?php
						$countLength = sizeof($report);
						$count=1;
						$total = 0;
						if($report[0]->FeeTypeIdTBC != '') $countLength+=1;
						foreach ($report as $Row) :
							if($count == 1)
							{
					?>
								<tr>
									<td rowspan="<?php echo $countLength; ?>"><?php echo $_GET['txtDueDate']; ?></td>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount);$total+=$Row->NetAmount; ?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							else
							{
					?>
								<tr>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount); $total+=$Row->NetAmount;?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							if ($count == $countLength) 
							{
								$count = 1;
							}
							else $count++;
						endforeach;
					?>
							<tr>
								<td colspan="2"><b><?php echo 'Rp '.moneyFormat($total);?></b>
									<?php
										$flag = false;
										for($i = 0 ; $i < sizeof($report) ; $i++)
										{
											if($report[$i]->FlagDiskon == '1')
											{
												$flag=true;
											}
										}
										
										if($flag) echo '(includes 10% of Sibling Discount)';
									?>*
								</td>
							</tr>
							<tr>
								<td>Date to be confirmed later</td>
								<td><?php echo 'Rp '.moneyFormat($package_tbc[0]->AmountTBC); ?></td>
								<td><?php echo  $package_tbc[0]->TypeOfPaymentTBC;?></td>
							</tr>
					<?php
						if(isset($_GET['cbSpecial']))
						{
					?>
							<tr>
								<td colspan="3" style="text-align:center;"><b>OR</b></td>
							</tr>
					<?php
							$countLength = sizeof($package)+1;
							$count=1;
							$total = 0;
							foreach ($package as $Row) :
								if($count == 1)
								{
					?>
								<tr>
									<td rowspan="<?php echo $countLength; ?>"><?php echo $Row->TanggalJatuhTempo; ?></td>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount);$total+=$Row->FirstAmount; ?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							else
							{
					?>
								<tr>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount); $total+=$Row->FirstAmount;?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							if ($count == $countLength) 
							{
								$count = 1;
							}
							else $count++;
						endforeach;
					?>
							<tr>
								<td colspan="2"><b><?php echo 'Rp '.moneyFormat($total);?></b>
								<?php echo (isset($package[0]->NoteDiskon) ? $package[0]->NoteDiskon : '');?>
								</td>
							</tr>
						<?php }?>
				</table>
				<p style="font-size:10px;"><i>
					*) Sibling Discount only applies to Enrollment Fee and if student has a sibling studying or also enrolling at BINUS INTERNATIONAL SCHOOL Simprug in the same academic year.</br>
					<?php echo ( $report[0]->SiblingName != "" ? 'Sibling\'s Name :'.$report[0]->SiblingName : '');?>
				</i></p>
				<p>
					All the payment above must be fulfilled by transfer to BCA ATM, please follow the payment steps as below :
				</p>
				<table>
					<tr>
						<td width="10%">1.</td>
						<td width="40%">Insert your ATM Card<br/><i>Masukkan kartu ATM Anda</i></td>
						<td width="10%">7.</td>
						<td width="40%">Enter Company's Code : 710022<br/><i>Masukkan Kode Perusahaan : 710022</i></td>
					</tr>
					<tr>
						<td width="10%">2.</td>
						<td width="40%">Enter your PIN<br/><i>Masukkan PIN Anda</i></td>
						<td width="10%">8.</td>
						<td width="40%">Enter Registration Number < <?php echo $report[0]->RegistrantID?> ><br/><i>Masukkan Nomor Registrasi < <?php echo $report[0]->RegistrantID?> ></i></td>
					</tr>
					<tr>
						<td width="10%">3.</td>
						<td width="40%">Press "Other Transaction"<br/><i>Pilih menu "Transaksi Lainnya"</i></td>
						<td width="10%">9.</td>
						<td width="40%">Enter Payment Amount<br/><i>Masukkan Jumlah Pembayaran</i></td>
					</tr>
					<tr>
						<td width="10%">4.</td>
						<td width="40%">Press "Payment"<br/><i>Pilih menu "Pembayaran"</i></td>
						<td width="10%">10.</td>
						<td width="40%">Payment Confirmation, press "YES" <br/><i>Konfirmasi Pembayaran, Pilih "YA"</i></td>
					</tr>
					<tr>
						<td width="10%">5.</td>
						<td width="40%">Press "Next Screen"<br/><i>Pilih menu "Layar Berikut"</i></td>
						<td width="10%">11.</td>
						<td width="40%">Another Transaction, press "NO"<br/><i>Pilihan untuk transaksi lain, pilih "TIDAK"</i></td>
					</tr>
					<tr>
						<td width="10%">6.</td>
						<td width="40%">Press "Others"<br/><i>Pilih menu "Lain-lain"</i></td>
						<td width="10%">12.</td>
						<td width="40%">Get ATM Payment Receipt<br/><i>Terima Bukti Pembayaran ATM</i></td>
					</tr>
				</table>
				<p>
					If you have not fulfilled the payment procedure or payment confirmation have not received yet by Marketing Dept. and have not-done Re-Registration by <?php echo $_GET['txtDueDate']; ?>, then we will consider that you have resigned your child's enrollment in BINUS INTERNATIONAL SCHOOL Simprug.
				</p>
				<p>
					You're most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.
				</p>
				<br/>
				</br>
				<p>
					Yours Sincerely,<br/>
					<?php 
						if(isset($_GET['cbSignature']))
						{
					?>
							<img src="<?php echo base_url();?>resources/images/digital_signature.jpg" width="100px;" /><br/>
					<?php 
						}
						else
						{
					?>
							<br/><br/><br/><br/><br/>
					<?php 	
						}
					?>
					Marketing Manager<br/>
					<i>KSY/adm</i>
				</p>
			</div>
			<?php 
			}?>
		</div>
	</body>
		<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/billing_and_payment_notification_letter/print_report/">
		<input type="hidden" name="hfContent" />
	<a href="#" id="btnExport" class="button">Export To PDF</a> 
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

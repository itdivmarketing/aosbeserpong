<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					RE-REGISTRATION REPORT - TRANSFER DATA TO ACOP <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo $AcademicSemester; ?> <br/>
				</b>
			</div>
			<?php $total = 0;
			if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = $report[0]->SchoolLevelId;
				$total = 0;
			?>
			<div width="100%">
				School Level : <?php echo $report[0]->SchoolLevelName;?><br/>
			</div>
			<table width="100%">
				<thead>
						<tr>
							<th>Registrant ID</th>
							<th>Registrant Name</th>
							<th>Term</th>
							<th>Year Level</th>
							<th>Student ID</th>
							<th>Join to School Date</th>
							<th>Academic Year</th>
							<th>Academic Semester</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $Row):
						if($Row->SchoolLevelId == $SchoolLevel)
						{
				?>
								<tr>
									<td><?php echo $Row->RegistrantId?></td>
									<td><?php echo $Row->RegistrantName?></td>
									<td><?php echo $Row->TermName?></td>
									<td><?php echo $Row->YearLevelName?></td>
									<td><?php echo $Row->StudentId?></td>
									<td><?php echo $Row->JoinToSchoolDate?></td>
									<td><?php echo $Row->AcademicYearReReg?></td>
									<td><?php echo $Row->AcademicSemesterReReg; $total += 1?></td>
								</tr>
				<?php
						}
						else
						{
							$SchoolLevel = $Row->SchoolLevelId;
				?>
				</tbody>
			</table>
			<div width="100%">
				Total Registrant : <?php echo $total; $total=0;?><br/>
			</div>
			<div style="page-break-after:always;">&nbsp;</div>
			<div id="repeat">
				<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div width="100%" style="text-align:center;">
					<b>
						RE-REGISTRATION REPORT - TRANSFER DATA TO ACOP <br/>
						ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo $AcademicSemester; ?> <br/>
					</b>
				</div>
			</div>
			<div width="100%">
				School Level : <?php echo $Row->SchoolLevelName;?><br/>
			</div>
			<table width="100%">
				<thead>
					<tr>
						<th>Registrant ID</th>
						<th>Registrant Name</th>
						<th>Term</th>
						<th>Year Level</th>
						<th>Student ID</th>
						<th>Join to School Date</th>
						<th>Academic Year</th>
						<th>Academic Semester</th>
					</tr>
				</thead>
				<tbody id="RateContent">
					<tr>
						<tr>
							<td><?php echo $Row->RegistrantId?></td>
							<td><?php echo $Row->RegistrantName?></td>
							<td><?php echo $Row->TermName?></td>
							<td><?php echo $Row->YearLevelName?></td>
							<td><?php echo $Row->StudentId?></td>
							<td><?php echo $Row->JoinToSchoolDate?></td>
							<td><?php echo $Row->AcademicYearReReg?></td>
							<td><?php echo $Row->AcademicSemesterReReg; $total += 1?></td>
						</tr>
					</tr>
				<?php
						}
					endforeach;
			}
			else
			{
				?>
					<tr>
						<td colspan="8">There's No Data</td>
					</tr>
				<?php
			}
				?>
				</tbody>
			</table>
			<div width="100%">
				Total Registrant : <?php echo $total;?><br/>
			</div>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'ReRegistration_Report.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

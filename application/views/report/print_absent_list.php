<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="80%" style="float:left;">
				BINUS INTERNATIONAL SCHOOL SERPONG<br/>
				Jl. Lengkong Karya - Jelupang No. 58, Lengkong Karya, Serpong</br>
				Tangerang Selatan 15323</br>
			</div>
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					ENTRANCE TEST ATTENDANCE LIST <br/>
					ADMISSION YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
					TERM <?php echo (isset($report[0]) ? $report[0]->TermId.' - '.$report[0]->TermName : '-');?>
				</b>
			</div>
			
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$ScheduleID = $report[0]->KdMsJadwalET;
				$SchoolLevel = $report[0]->SchoolLevelID;
				$VenueId = $report[0]->VenueId;
				$PreTestSubjectId = $report[0]->PreTestSubjectID;
			?>
			</br>
			<div>
				School Level : <?php echo $report[0]->SchoolLevelID.' - '.$report[0]->SchoolLevelName; ?> <br/>
				Date/Time : <?php echo $report[0]->TglAwal.' ( '.$report[0]->JamMulai.' - '.$report[0]->JamSelesai.' )'; ?> <br/>
				Venue : <?php echo $report[0]->VenueId.' - '.$report[0]->VenueName; ?> <br/>
				Entrance Test Subject : <?php echo $report[0]->PreTestSubjectName ; ?>
			</div>
			<table width="100%">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th width="50%">Registrant ID / Name</th>
						<th width="15%">Year Level</th>
						<th width="30%" colspan="2">Attendance</th>
					</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					$i = 1;
					foreach($report as $Row):
						if( $Row->KdMsJadwalET == $ScheduleID && $Row->SchoolLevelID == $SchoolLevel &&
							$Row->VenueId == $VenueId && $Row->PreTestSubjectID == $PreTestSubjectId)
						{
							if ($i %2 != 0)
							{
				?>
								<tr>
									<td style="text-align:right;"><?php echo $i; ?></td>
									<td><?php echo $Row->RegistrantID.' / '.$Row->RegistrantName; ?></td>
									<td><?php echo $Row->YearLevelName;?></td>
									<td rowspan="2" style="text-align:left; vertical-align:top"><?php echo $i; ?></td>
									<td rowspan="2" style="text-align:left; vertical-align:bottom; background: #ebebeb;"><?php echo ++$i; ?></td>
								</tr>
				<?php 	
							}
							else
							{
				?>
								<tr>
									<td style="text-align:right;"><?php echo $i; ?></td>
									<td><?php echo $Row->RegistrantID.' / '.$Row->RegistrantName; ?></td>
									<td><?php echo $Row->YearLevelName;?></td>
								</tr>
				<?php		
								++$i;
							}
						} 
						else
						{
							if (($i-1) %2 != 0) //odd data
							{
				?>
								<tr>
									<td style="text-align:right;"><?php echo $i; ?></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
				<?php
							}			
				?>
				</tbody>
			</table>
			<div style="page-break-after: always"></div>
			<div id="repeat">
				<div width="80%" style="float:left;">
					BINUS INTERNATIONAL SCHOOL SERPONG<br/>
					Jl. Lengkong Karya - Jelupang No. 58, Lengkong Karya, Serpong</br>
					Tangerang Selatan 15323</br>
				</div>
				<div width="20%" style="float:right;">
					Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
					Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
					User :  <?php echo $UserID; ?><br/>
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div width="100%" style="text-align:center;">
					<b>
						ENTRANCE TEST ATTENDANCE LIST <br/>
						ADMISSION YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
						TERM <?php echo (isset($report[0]) ? $report[0]->TermId.' - '.$report[0]->TermName : '-');?>
					</b>
				</div>
			</div>
				<?php
							$ScheduleID = $Row->KdMsJadwalET;
							$SchoolLevel = $Row->SchoolLevelID;
							$VenueId = $Row->VenueId;
							$PreTestSubjectId = $Row->PreTestSubjectID;
				 ?>
				 
			
			<div>
				School Level : <?php echo $Row->SchoolLevelID.' - '.$Row->SchoolLevelName; ?> <br/>
				Date/Time : <?php echo $Row->TglAwal.' ( '.$Row->JamMulai.' - '.$Row->JamSelesai.' )'; ?> <br/>
				Venue : <?php echo $Row->VenueId.' - '.$Row->VenueName; ?> <br/>
				Entrance Test Subject : <?php echo $Row->PreTestSubjectName ; ?>
			</div>
			<table width="100%">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th width="50%">Registrant ID / Name</th>
						<th width="15%">Year Level</th>
						<th width="30%" colspan="2">Attendance</th>
					</tr>
				</thead>
				<tbody id="RateContent">
				<?php 
							$i = 1; 
							if ($i %2 != 0)
							{
				?>
								<tr>
									<td style="text-align:right;"><?php echo $i; ?></td>
									<td><?php echo $Row->RegistrantID.' / '.$Row->RegistrantName; ?></td>
									<td><?php echo $Row->YearLevelName;?></td>
									<td rowspan="2" style="text-align:left; vertical-align:top"><?php echo $i; ?></td>
									<td rowspan="2" style="text-align:left; vertical-align:bottom; background: #ebebeb;"><?php echo ++$i; ?></td>
								</tr>
				<?php 	
							}
							else
							{
				?>
								<tr>
									<td style="text-align:right;"><?php echo $i; ?></td>
									<td><?php echo $Row->RegistrantID.' / '.$Row->RegistrantName; ?></td>
									<td><?php echo $Row->YearLevelName;?></td>
								</tr>
				<?php	
								++$i;
							}
						}
					endforeach;
					if (($i-1) %2 != 0) //odd data
					{
				?>
						<tr>
							<td style="text-align:right;"><?php echo $i; ?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
				<?php
					}	
				}
				else //no data
				{
							
				}
				?>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Absent List.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

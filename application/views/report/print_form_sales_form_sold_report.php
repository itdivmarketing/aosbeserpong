<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
	
		</style>
   </head>
	<body>
		<div id="data">
			<style>
			table tbody tr td:first-child {
				border-left:1px solid black;
			}
			table tbody tr td {
				border-left:1px solid black;
			}
			table thead th {
				font-weight:bold;
				border-left:1px solid black;
			}
			table{
		
				border-top:1px solid black;
				border:1px solid black;
				border-bottom:1px solid black;
			}
			table td{
				border-top:1px solid black;
				border-left: 1px solid black;
				border-bottom:1px solid black;
			}
			table th{
				font-weight:bold;
				border-top:1px solid black;
				text-align:center;
				border-left: 1px solid black;
				border-bottom:1px solid black;
			}
			
		</style>
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>REKAP PENJUALAN FORMULIR</b><br/>
				<b>BINUS INTERNATIONAL SCHOOL SERPONG<br/>
				TAHUN AJARAN <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
				PERIODE <?php echo $_GET['txtStartPeriod']; ?> - <?php echo $_GET['txtEndPeriod']; ?></b>
			</div>
			<br/>
			<br/>
			<br/>
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				$SchoolLevel = $report[0]->SchoolLevelID;
				$SchoolLevelName = $report[0]->SchoolLevelName;
				$DateOfFormPurchased= "";
				
				$total = 0;
			
			?>
			<b>School Level : <?php echo $SchoolLevel.' - '.$SchoolLevelName ; ?></b> <br/>
			<br/>
			<table>
				<thead>
					<tr>
						<th width="20%">Date</th>
						<th width="10%">Year Level</th>
						<th width="10%">No Form</th>
						<th width="50%">Candidate Name</th>
						<th width="10%">Term</th>
					</tr>
				</thead>
				<tbody id="RateContent">
				<?php
				foreach($report as $Row):
					
					if($Row->SchoolLevelID == $SchoolLevel ) 
					{
					?>
						<tr>
							<?php if( $Row->DateOfFormPurchased != $DateOfFormPurchased){ ?>
								<td width="20%" style="background-color:white;"  valign="middle"  rowspan="<?php  echo $Row->Jumlah; ?>" >
								<?php echo $Row->DateOfFormPurchased; ?>
								</td>
							<?php }else{ ?>
							
							<?php } ?>
							
							<td width="10%"><?php echo $Row->YearLevelName; $total+=1; ?></td>
							<td width="10%"><?php echo $Row->FormNo; ?></td>
							<td width="50%"><?php echo $Row->RegistrantName; ?></td>
							<td width="10%"><?php echo $Row->TermID; ?></td>
						</tr>
				<?php 
					$DateOfFormPurchased=$Row->DateOfFormPurchased;
					} 
					else
					{
					$DateOfFormPurchased="";					
				?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total <?php echo $SchoolLevelName ; ?></th>
							<th><?php echo $total ; ?></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				
				<div style="page-break-after:always;">&nbsp;</div>
				<div id="repeat">
						<div width="20%" style="float:right;">
					Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
					Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
					User :  <?php echo $UserID; ?><br/>
				</div>
				<div width="100%" style="text-align:center;">
					<b>REKAP PENJUALAN FORMULIR</b><br/>
					<b>BINUS INTERNATIONAL SCHOOL SERPONG<br/>
					TAHUN AJARAN <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
					PERIODE <?php echo $_GET['txtStartPeriod']; ?> - <?php echo $_GET['txtEndPeriod']; ?></b>
				</div>
				<br/>
				<br/>
				<br/>
				</div>
				<?php
					$SchoolLevel = $Row->SchoolLevelID;
					$SchoolLevelName = $Row->SchoolLevelName;
					
					
					$total = 0;
					
				?>
				<b>School Level : <?php echo $SchoolLevel.' - '.$SchoolLevelName ; ?></b> <br/>
				<br/>
				<table>
					<thead>
						<tr>
							<th width="20%">Date</th>
							<th width="10%">Year Level</th>
							<th width="10%">No Form</th>
							<th width="50%">Candidate Name</th>
							<th width="10%">Term</th>
						</tr>
					</thead>
					<tbody id="RateContent">
						<tr>
							<?php if( $Row->DateOfFormPurchased != $DateOfFormPurchased){ ?>
								<td width="20%" style="background-color:white;" valign="middle" rowspan="<?php  echo $Row->Jumlah; ?>">
								<?php echo $Row->DateOfFormPurchased; ?>
								</td>
							<?php }else{ ?>
						
							<?php } ?>
							<td width="10%"><?php echo $Row->YearLevelName; $total+=1; ?></td>
							<td width="10%"><?php echo $Row->FormNo; ?></td>
							<td width="50%"><?php echo $Row->RegistrantName; ?></td>
							<td width="10%"><?php echo $Row->TermID; ?></td>
						</tr>	
				<?php
					$DateOfFormPurchased=$Row->DateOfFormPurchased;
					}
					if($i == $len-1)
					{
				?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total <?php echo $SchoolLevelName ; ?></th>
							<th><?php echo $total ; ?></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				
				<?php
					}
				$i++;
				
				endforeach;
				}
				else //no data
				{
					
				}
				?>
			
		</div>
	</body>
	
		<a href="#" id="btnExportExcel" class="button">Export To Excel</a> 
		<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/form_sales_form_sold_report/print_report_pdf/">
			<input type="hidden" name="hfContent" />
		<a href="#" id="btnExportPDF" class="button">Export To PDF</a> 
		</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExportExcel').click(function(e) {
			$(this)
				.attr({
					'download': 'Form_Sold_Detail.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
			
		});
		$('#btnExportPDF').click(function(e) {
				$('[name="hfContent"]').val($('#data').html());
				$('[name="export"]').submit();
			});
	})
}(jQuery))
</script>

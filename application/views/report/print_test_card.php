<!doctype html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
	<link href="<?php echo base_url(); ?>resources/css/tpks.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>resources/css/tpks-print.css" media="print" rel="stylesheet" type="text/css" />
<!-- 	<link href="<?php echo base_url(); ?>resources/css/wiedy.css" media="screen" rel="stylesheet" type="text/css" />  -->
   </head>
	<body>
	<?php
		if (isset($report))
		{
			for($i = 0 ; $i < sizeof($report) ; $i++)
			{
	?>
		<div style="page-break-after: always;">
			<div id="tpks-container"></div>
			<div id="cetak_kartu">
				<div class="paper">
					<!-- <div class="tagline-banner new-layout">
					<div class="container">
						<div class="wrap">
							<span class="banner">
								<span class="banner-image"></span>
							</span>
						</div>
					</div>
					</div> -->

					<h1 style="font-weight:bold;padding-left:150px;" align="left">KARTU UJIAN SARINGAN MASUK / ENTRANCE TEST CARD</h1>
					
					<div class="first-content"> 
					<div class="student-data student-data1">
							<img src="<?php echo base_url(); ?>resources/images/logoET.png" class="logo">
							<table class="table-data table-data1">
								<tr class="spaceUnder">
									<td class="left">
										<span>Tahun Akademik</span><br/>
										<span class="english">Academic year<span>
									</td>
									<td class="middle">:</td>
									<td><?php echo $report[$i]->AdmissiONYear?></td>
								</tr>
								<tr class="spaceUnder">
									<td class="left">
										<span>Gelombang</span><br/>
										<span class="english">Enrollment Batch<span>
									</td>
									<td class="middle">:</td>
									<td><?php echo $report[$i]->TermId .' - '.$report[$i]->TermName ;   ?></td>
								</tr>
								</table>
							<table class="table-data table-data2">
								<tr class="spaceUnder">
									<td class="left">
										<span>Nomor Pendaftaran</span><br/>
										<span class="english">Registration Number</span>
									</td>
									<td class="middle">:</td>
									<td><?php echo $report[$i]->RegistrantID ;   ?></td>
								</tr>
								<tr class="spaceUnder">
									<td class="left">
										<span>Nama Lengkap</span><br/>
										<span class="english">Full Name</span>
									</td>
									<td class="middle">:</td>
									<td><?php echo $report[$i]->RegistrantName ; ?></td>
								</tr>

								<tr class="spaceUnder">
									<td class="left">
										<span>Jenis Pendaftaran</span><br/>
										<span class="english">Enrollment Track</span>
									</td>
									<td class="middle">:</td>
									<td></td>
								</tr>
							</table>
					</div>
					<div clas="student-data student-data2" style="">
						<div class="screen"><p style="text-align:right;">FM-BINUS-AA-FRM-13 R2</p></div>
						<div class="student-pic-wrap">
							<p class="photograph-information">
<!-- 								PASFOTO<br>
								PHOTOGRAPH<br>
								<br>
								3x4 -->
							</p>
						</div>
							<table class="table-data table-data3">
								<tbody>
								<tr class="spaceUnder">
									<td class="left">
										<p>Nomor Pendaftaran</p>
										<p class="english">Registratrion Number<p>
									</span></span></td>
									<td class="middle">
									:
									</td>
									<td class="right"><?php echo $report[$i]->RegistrantID ;   ?></td>
								</tr>
								<tr class="spaceUnder">
									<td class="left">
										<p>Tingkat</p>
										<p class="english">Level</p>
									</td>
									<td class="middle">
									:
									</td>
									<td class="right"><?php echo $report[$i]->YearLevelName ; ?></td>
								</tr>
								
								<tr class="spaceUnder">
									<td class="left">
										<p>Prog. Studi/Kelas</p>
										<p class="english">Study Prog/Class</p>
									</td>
									<td class="middle">
									:
									</td>
									<td class="right"></td>
								</tr>
								</tbody>
							</table>
					</div>
					</div>
					<div class="second-content">
						<table class="table-detail table-detail4" border="10px" style="margin-right:20px;width:58%">
								<tr>
									<td style="width:31%">
										<p>Jenis Ujian</p>
									</td>
									<td style="width:26%">
										<p>Hari, Tanggal</p>
									</td>
									<td style="width:15%">
										<p>Waktu</p>
									</td>
									<td style="width:8%">
										<p>Lokasi</p>
									</td>
									<td style="width:10%">
										<p>Ruang</p>
									</td>
									<td style="width:10%">
										<p>No. Kursi</p>
									</td>
								</tr>
								<?php //if(isset($report) ) {foreach( $report as $row){ ?>
								<tr>
									<td><?php echo $report[$i]->PreTestSubjectName; ?></td>
									<td><?php 
										$timestamp = strtotime($report[$i]->TglAwal);
										$day = date('l', $timestamp);
										echo $day.", ".$report[$i]->TglAwal; ?></td>
									<td><?php echo $report[$i]->JamMulai.'-'.$report[$i]->JamSelesai; ?></td>
									<td></td>
									<td><?php echo $report[$i]->VenueName; ?></td>
									<td></td>
									
								</tr>
								<?php //}} ?>
								<?php if( $report[$i]->TglP3A!="" && $report[$i]->TglP3A!= NULL && !empty($report[$i]->TglP3A)){ //SERPONG ?>
								<tr>
									<td>Interview</td>
									<td> <?php echo '-';//(isset($report[0]->TglP3A) ? ( $report[0]->TglP3A != '' ? $report[0]->TglP3A : '-') : '-');?></td>
									<td> <?php echo '-';//(isset($report[0]->JamMulaiP3A) ? ( $report[0]->JamMulaiP3A != '' ? $report[0]->JamMulai.'-'.$report[0]->JamSelesai : '-') : '-');?></td>
									<td> <?php echo '-';//(isset($report[0]->VenueNameP3A) ? ( $report[0]->VenueNameP3A != '' ? $report[0]->VenueNameP3A : '-') : '-');?></td>
									<td>-</td>
									<td>-</td>
								</tr>
								<?php
								}
								?>
								
							</table>
							<div class="test-rules">
						<table>
							<tr class="head-list">
								<td colspan="2">
									Perhatian : <br/>
									<span class="english">Attention</span>
								</td>
							</tr>
							<tr class="list">
								<td>
								1.
								</td>
								<td class="left">
								<p>Wajib dibawa saat ujian dan pengumuman.</p>
								<p class="english">Please bring this card to Entrance and Announcement Day.</p>
								</td>
							</tr>
							<tr class="list">
								<td>
								2.
								</td>
								<td class="left">
								Wajib membawa pensil 2B.</br>
								Please bring 2B pencil.
								</td>
							</tr>
							<tr class="list">
								<td>
								3.
								</td>
								<td class="left">
								Wajib hadir 30 menit sebelum ujian dimulai.</br>
								Please atttend 30 minutes befire Entrance Test starts.
								</td>
							</tr>
							<tr class="list">
								<td>
								4.
								</td>
								<td class="left">
								Wajib mengenakan sepatu.</br>
								Please wear shoes.
								</td>
							</tr>
						</table>
					</div>
					</div>
					<div class="third-content">
						<table class="table-information">
						<tr>
							<td colspan="3">
								<span class="coloring">BINUS SCHOOL Serpong</span>
							<td>
						</tr>
						<tr>
							<td class="col1">
								Jl. Lengkong Karya - Jelupang No.58
							</td>
							<td class="col2">
								<span class="coloring">t.</span> +6221 536 96961
							</td>
							<td class="col3">
								<span class="coloring">f.</span> +6221 533-0715
							</td>
						</tr>
						<tr>
							<td class="col1">
								Serpong, Tangerang, 15322, Indonesia
							</td>
							<td class="col2">
								<span class="coloring">t.</span> +6221 528 0400 <span class="coloring">ext</span> 5101-5104
							</td>
							<td class="col3">
								<span class="coloring">serpong.binus-school.net</span>
							</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php
			}
		}
		?>
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					POTENTIAL SIBLING REGISTRANT REPORT <br/>
					FOR ADMISSION PERIOD <?php echo $AcademicYear; ?> <br/>
				</b>
			</div>
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = trim($report[0]->SchLvlIdPotensialSibling);
				$Year = trim($report[0]->AdmissionYear);
			?>
			<div width="100%">
				School Level : <?php echo $report[0]->SchoolLevelName;?><br/>
			</div>
			<table width="100%">
				<thead>
						<tr>
							<th>Admission Year</th>
							<th>Registrant ID</th>
							<th>Registrant Name</th>
							<th>Potential Sibling Name</th>
							<th>HP Father</th>
							<th>HP Mother</th>
							<th>Potential For School Level</th>
							<th>Potential For Year Level</th>
							<th>Birthday</th>
							<th>Age</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $Row):
						if(trim($Row->SchLvlIdPotensialSibling) == $SchoolLevel)
						{
							if($Row->AdmissionYear == $Year)
							{
				?>
								<tr>
									<td><?php echo $Row->AdmissionYear?></td>
									<td><?php echo $Row->Registrantid?></td>
									<td><?php echo $Row->RegistrantName?></td>
									<td><?php echo $Row->PotentialSiblingName?></td>
									<td><?php echo $Row->HPAyah?></td>
									<td><?php echo $Row->HPIbu?></td>
									<td><?php echo $Row->SchoolLevelName?></td>
									<td><?php echo $Row->YearLevelName?></td>
									<td><?php echo $Row->Birthday?></td>
									<td><?php echo $Row->Age?></td>
								</tr>
				<?php
							}
							else
							{
								$Year = $Row->AdmissionYear;
				?>
								</tbody>
							</table>
							<br/>
							<table width="100%">
								<thead>
									<tr>
										<th>Admission Year</th>
										<th>Registrant ID</th>
										<th>Registrant Name</th>
										<th>Potential Sibling Name</th>
										<th>HP Father</th>
										<th>HP Mother</th>
										<th>Potential For School Level</th>
										<th>Potential For Year Level</th>
										<th>Birthday</th>
										<th>Age</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<tr>
										<td><?php echo $Row->AdmissionYear?></td>
										<td><?php echo $Row->Registrantid?></td>
										<td><?php echo $Row->RegistrantName?></td>
										<td><?php echo $Row->PotentialSiblingName?></td>
										<td><?php echo $Row->HPAyah?></td>
										<td><?php echo $Row->HPIbu?></td>
										<td><?php echo $Row->SchoolLevelName?></td>
										<td><?php echo $Row->YearLevelName?></td>
										<td><?php echo $Row->Birthday?></td>
										<td><?php echo $Row->Age?></td>
									</tr>
				<?php
							}
						}	
						else
						{
							$SchoolLevel = trim($Row->SchLvlIdPotensialSibling);
							$Year = $Row->AdmissionYear;
				?>
				</tbody>
			</table>
			<div style="page-break-after:always;">&nbsp;</div>
			<div id="repeat">
				<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div width="100%" style="text-align:center;">
					<b>
						POTENTIAL SIBLING REGISTRANT REPORT <br/>
						FOR ADMISSION PERIOD <?php echo $AcademicYear; ?> <br/>
					</b>
				</div>
			</div>
			<div width="100%">
				School Level : <?php echo $Row->SchoolLevelName;?><br/>
			</div>
			<table width="100%">
				<thead>
					<tr>
						<th>Admission Year</th>
						<th>Registrant ID</th>
						<th>Registrant Name</th>
						<th>Potential Sibling Name</th>
						<th>HP Father</th>
						<th>HP Mother</th>
						<th>Potential For School Level</th>
						<th>Potential For Year Level</th>
						<th>Birthday</th>
						<th>Age</th>
					</tr>
				</thead>
				<tbody id="RateContent">
					<tr>
						<td><?php echo $Row->AdmissionYear?></td>
						<td><?php echo $Row->Registrantid?></td>
						<td><?php echo $Row->RegistrantName?></td>
						<td><?php echo $Row->PotentialSiblingName?></td>
						<td><?php echo $Row->HPAyah?></td>
						<td><?php echo $Row->HPIbu?></td>
						<td><?php echo $Row->SchoolLevelName?></td>
						<td><?php echo $Row->YearLevelName?></td>
						<td><?php echo $Row->Birthday?></td>
						<td><?php echo $Row->Age?></td>
					</tr>
				<?php
						}
					endforeach;
			}
			else
			{
				?>
					<tr>
						<td colspan="10">There's No Data</td>
					</tr>
				<?php
			}
				?>
				</tbody>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> <br/>
	<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/billing_and_payment_notification_letter/print_report/">
		<input type="hidden" name="hfContent" />
		<a href="#" id="btnExportPdf" class="button">Export To Pdf</a>
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Validate_Form.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
		
		$('#btnExportPdf').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
   </head>
	<?php $this->load->helper('admission_helper');?>
	<body>
		<div id="data">
			<div width="30%">
				<p>
					Jakarta, <?php echo $printedDate[0]->CurrDate; ?><br/>
					No : <?php echo $_POST['txtCode'].'/'.$_POST['txtLetterNumber']; ?>
				</p>
			</div>
			<div width="100%" style="text-align:center;">
					<b><u>Subjek : Surat Notifikasi Hasil Ujian Masuk </u></b>
			</div>
			<?php if(isset($report))
			{
			?>
			<div width="100%" style="text-align:left;">
				<p>
					Yth. Bpk/Ibu <?php echo $report[0]->FatherName.' '.$report[0]->MotherName?>, <br/>
				</p>
				<p>
					Mohon membaca notifikasi berikut dengan seksama <br/>
					Berdasarkan Hasil Tes Masuk Akhir, kami ingin memberitahukan bahwa anak Bpk/Ibu: <br/>
				</p>
				<p style="padding-left:30px;">
						Nama			: <?php echo $report[0]->RegistrantName.' ('.$report[0]->RegistrantID.')'; ?> <br/>
						Level Akademis	: <?php echo $report[0]->AdmissionYear; ?> <br/>
						Hasil			: Direkomendasikan <br/>
				</p>
				<p>
					Berdasarkan surat perjanjian yang biaya uang sekolah yang sudah ditandatangani, kami menyambut dengan baik apabila Bpk/Ibu berkenan untuk melanjutkan ke proses selanjutnya. Mohon mengikuti instruksi pembayaran dibawah ini:<br/>
				</p>
				<table>
					<thead>
						<tr>
							<th>Batas Pembayaran</th>
							<th>Jumlah</th>
							<th>Tipe Pembayaran</th>
						</tr>
					</thead>
					<?php
						$countLength = sizeof($report);
						$count=1;
						$total = 0;
						if($report[0]->FeeTypeIdTBC != '') $countLength+=1;
						foreach ($report as $Row) :
							if($count == 1)
							{
					?>
								<tr>
									<td rowspan="<?php echo $countLength; ?>"><?php echo $_POST['txtDueDate']; ?></td>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount);$total+=$Row->NetAmount; ?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							else
							{
					?>
								<tr>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount); $total+=$Row->NetAmount;?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							if ($count == $countLength) 
							{
								$count = 1;
							}
							else $count++;
						endforeach;
					?>
							<tr>
								<td colspan="2"><b><?php echo 'Rp '.moneyFormat($total);?></b>
									<?php
										$flag = false;
										for($i = 0 ; $i < sizeof($report) ; $i++)
										{
											if($report[$i]->FlagDiskon == '1')
											{
												$flag=true;
											}
										}
										
										if($flag) echo '(Termasuk 10% Diskon Saudara)';
									?>*
								</td>
							</tr>
							<tr>
								<td>Tanggal Konfirmasi Pembayaran</td>
								<td><?php echo 'Rp '.moneyFormat($package_tbc[0]->AmountTBC); ?></td>
								<td><?php echo  $package_tbc[0]->TypeOfPaymentTBC;?></td>
							</tr>
					<?php
						if(isset($_GET['cbSpecial']))
						{
					?>
							<tr>
								<td colspan="3" style="text-align:center;"><b>OR</b></td>
							</tr>
					<?php
							$countLength = sizeof($package)+1;
							$count=1;
							$total = 0;
							foreach ($package as $Row) :
								if($count == 1)
								{
					?>
								<tr>
									<td rowspan="<?php echo $countLength; ?>"><?php echo $Row->TanggalJatuhTempo; ?></td>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount);$total+=$Row->FirstAmount; ?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							else
							{
					?>
								<tr>
									<td><?php echo 'Rp '.moneyFormat($Row->Amount); $total+=$Row->FirstAmount;?></td>
									<td><?php echo $Row->TypeOfPayment;?></td>
								</tr>
					<?php
							}
							if ($count == $countLength) 
							{
								$count = 1;
							}
							else $count++;
						endforeach;
					?>
							<tr>
								<td colspan="2"><b><?php echo 'Rp '.moneyFormat($total);?></b>
								<?php echo (isset($package[0]->NoteDiskon) ? $package[0]->NoteDiskon : '');?>
								</td>
							</tr>
						<?php }?>
				</table>
				<p style="font-size:10px;"><i>
					*) Diskon Saudara hanya berlaku untuk pembayaran pendaftaran dan jika murid mempunyai saudara yang sedang belajar atau sedang mendaftar di BINUS INTERNATIONAL SCHOOL Serpong pada tahun yang sama.</br>
					<?php echo ( $report[0]->SiblingName != "" ? 'Sibling\'s Name :'.$report[0]->SiblingName : '');?>
				</i></p>
				<p>
					Semua pembayaran harus dilakukan dengan transfer melalui ATM BCA, Mohon mengikuti langkah pembayaran dibawah ini :
				</p>
				<table>
					<tr>
						<td width="10%">1.</td>
						<td width="40%">Insert your ATM Card<br/><i>Masukkan kartu ATM Anda</i></td>
						<td width="10%">7.</td>
						<td width="40%">Enter Company's Code : 710022<br/><i>Masukkan Kode Perusahaan : 710022</i></td>
					</tr>
					<tr>
						<td width="10%">2.</td>
						<td width="40%">Enter your PIN<br/><i>Masukkan PIN Anda</i></td>
						<td width="10%">8.</td>
						<td width="40%">Enter Registration Number < <?php echo $report[0]->RegistrantID?> ><br/><i>Masukkan Nomor Registrasi < <?php echo $report[0]->RegistrantID?> ></i></td>
					</tr>
					<tr>
						<td width="10%">3.</td>
						<td width="40%">Press "Other Transaction"<br/><i>Pilih menu "Transaksi Lainnya"</i></td>
						<td width="10%">9.</td>
						<td width="40%">Enter Payment Amount<br/><i>Masukkan Jumlah Pembayaran</i></td>
					</tr>
					<tr>
						<td width="10%">4.</td>
						<td width="40%">Press "Payment"<br/><i>Pilih menu "Pembayaran"</i></td>
						<td width="10%">10.</td>
						<td width="40%">Payment Confirmation, press "YES" <br/><i>Konfirmasi Pembayaran, Pilih "YA"</i></td>
					</tr>
					<tr>
						<td width="10%">5.</td>
						<td width="40%">Press "Next Screen"<br/><i>Pilih menu "Layar Berikut"</i></td>
						<td width="10%">11.</td>
						<td width="40%">Another Transaction, press "NO"<br/><i>Pilihan untuk transaksi lain, pilih "TIDAK"</i></td>
					</tr>
					<tr>
						<td width="10%">6.</td>
						<td width="40%">Press "Others"<br/><i>Pilih menu "Lain-lain"</i></td>
						<td width="10%">12.</td>
						<td width="40%">Get ATM Payment Receipt<br/><i>Terima Bukti Pembayaran ATM</i></td>
					</tr>
				</table>
				<p>
					Jika Bpk/Ibu belum memenuhi pembayaran sesuai prosedur atau belim mendapat konfirmasi pembayaran dari Departemen Marketing. Dan belum melakukan registrasi ulang hingga <?php echo $_POST['txtDueDate']; ?>, maka kami akan menganggap bahwa anak Bpk/Ibu telah mengundurkan diri dari BINUS INTERNATIONAL SCHOOL Serpong.
				</p>
				<p>
					Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.
				</p>
				<br/>
				</br>
				<p>
					Hormat kami,<br/>
					<?php 
						if(isset($_GET['cbSignature']))
						{
					?>
							<img src="<?php echo base_url();?>resources/images/digital_signature.jpg" width="100px;" /><br/>
					<?php 
						}
						else
						{
					?>
							<br/><br/><br/><br/><br/>
					<?php 	
						}
					?>
					Marketing Manager<br/>
					<i>KSY/adm</i>
				</p>
			</div>
			<?php 
			}?>
		</div>
	</body>
		<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/billing_and_payment_notification_letter/print_report/">
		<input type="hidden" name="hfContent" />
	<a href="#" id="btnExport" class="button">Export To PDF</a> 
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

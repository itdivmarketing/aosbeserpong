<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					DEVELOPMENT OF FORM SOLD REPORT <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo ($Semester); ?> <br/>
					SCHOOL LEVEL : <?php echo ($_GET['ddlSchoolLevel'] == 'all' ? 'ALL' : STRTOUPPER(explode(' - ',$_GET['hfSchoolLevel'])[1])); ?><br/>
				</b>
			</div>
			<?php if(isset($report))
			{
			?>
			<table width="100%">
				<thead>
						<tr>
							<th>Academic Year</th>
							<th>Form Sold</th>
							<th>Payment</th>
							<th>Generate ID</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $Row):
				?>
						<tr>
							<td><?php echo $Row->AcademicYear;?></td>
							<td><?php echo $Row->FormSold;?></td>
							<td><?php echo $Row->Payment;?></td>
							<td><?php echo $Row->GenerateId;?></td>
						</tr>
				<?php
					endforeach;
				?>
				</tbody>
				<?php
			}
			else
			{
				?>
				<tbody>
					<tr>
						<td colspan="7">There's No Data</td>
					</tr>
				</tbody>
				<?php
			}
				?>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Metrics_Report_Form_Sold.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Receipt</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmSearch" target="_blank" action="<?php echo site_url().'/';?>report/billing_and_payment_receipt/generate_report" method="get">
							<p>
								<label>Registrant ID : </label>
								<span>
									<input type="text" class="numeric" name="txtRegistrantID"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					STUDENT INFORMATION REPORT <br/>
				</b>
			</div>
			<?php if(isset($report) && isset($field))
			{
			?>
			<table width="100%">
				<thead>
						<tr>
							<?php
								foreach($field as $row) :
									$column = str_replace(' ', '', $row->Column_Name);
									if(array_search($column,$_GET['cbField']) !== false)
									{
							?>
										<th><?php echo $row->Column_Name;?></th>
							<?php
									}
								endforeach;
							?>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $key):
				?>		
						<tr>
				<?php
						foreach ($key as $key2 => $value2):
							foreach ($field as $row):
								$column = str_replace(' ', '', $row->Column_Name);
						
								if(array_search($column,$_GET['cbField']) !== false)
								{
									if (str_replace(' ', '', $key2)  == $column)
									{
				?>
										<td><pre><?php echo $value2;?></pre></td>
				<?php
									}
								}
							endforeach;	
						endforeach;
				?>
						</tr>
				<?php
					endforeach;
				?>
				</tbody>
			</table>
				<?php
			}
				?>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Student_Information_Report.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

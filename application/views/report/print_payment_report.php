<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					VALIDATE PAYMENT REPORT <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo ($Semester); ?> <br/>
				</b>
			</div>
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = $report[0]->SchoolLevelId;
			?>
			<div width="100%">
				School Level : <?php echo $report[0]->SchoolLevelName;?><br/>
			</div>
			<table border="1" width="100%">
				<thead>
						<tr>
							<th width="10%">Registrant ID</th>
							<th width="10%">Name</th>
							<th width="10%">Father's Mobile</th>
							<th width="10%">Mother's Mobile</th>
							<th width="10%">Term</th>
							<th width="10%">Year</th>
							<th width="10%">Fee Type</th>
							<th width="10%">Amount</th>
							<th width="10%">Paid</th>
							<th width="10%">Deposit</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					$reportTemp = $report;
					$count = 1;
					foreach($report as $Row):
						if($Row->SchoolLevelId == $SchoolLevel)
						{
							
							if ($count == 1) 
							{
								$countRowSpan = 0;
								for($i = 0 ; $i < sizeof($reportTemp) ; $i++)
								{
									if($reportTemp[$i]->RegistrantId == $Row->RegistrantId)
									{
										$countRowSpan+=1;
									}
								}
				?>
								<tr>
									<td rowspan="<?php echo $countRowSpan;?>"><?php echo $Row->RegistrantId?></td>
									<td rowspan="<?php echo $countRowSpan;?>"><?php echo $Row->RegistrantName?></td>
									<td rowspan="<?php echo $countRowSpan;?>"><?php echo '&nbsp;'.$Row->HPAyah?></td>
									<td rowspan="<?php echo $countRowSpan;?>"><?php echo '&nbsp;'.$Row->HPIbu?></td>
									<td rowspan="<?php echo $countRowSpan;?>"><?php echo $Row->TermName?></td>
									<td rowspan="<?php echo $countRowSpan;?>"><?php echo $Row->YearLevelName;?></td>
									<td><?php echo $Row->FeeTypeName;?></td>
									<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->NetAmount)),3))) ;?></td>
									<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->AmountBayar)),3)));?></td>
									<td><?php $deposit = $Row->TotAmountBayar - $Row->NetAmount; echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($deposit)),3)));?></td>								</tr>
				<?php
							}
							else
							{
				?>
								<tr>
									<td><?php echo $Row->FeeTypeName;?></td>
									<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->NetAmount)),3))) ;?></td>
									<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->AmountBayar)),3)));?></td>
									<td><?php $deposit = ($Row->TotAmountBayar - $Row->NetAmount); echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($deposit)),3)));?></td>	
								</tr>
				<?php
							}
							if ($count == $countRowSpan) 
							{
								$count = 1;
							}
							else $count++;
						} 
					endforeach;
			}
			else
			{
				?>
					<tr>
						<td colspan="7">There's No Data</td>
					</tr>
				<?php
			}
				?>
				</tbody>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Payment_Report.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					REPORT ACHIEVEMENT <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo ($Semester); ?> <br/>
				</b>
			</div>
			<?php if(isset($report))
			{
			?>
			<table width="100%">
				<thead>
						<tr>
							<th>Academic Year</th>
							<th>School Level</th>
							<th>Year Level</th>
							<th>Form Sold</th>
							<th>Target Form Sold</th>
							<th>Intake</th>
							<th>Target Intake</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					$formSold = 0;
					$targetFormSold = 0;
					$intake = 0;
					$targetIntake = 0;
					foreach($report as $Row):
				?>
						<tr>
							<td><?php echo $Row->academicyear;?></td>
							<td><?php echo $Row->SchoolLevelID.' - '.$Row->SchoolLevelName;?></td>
							<td><?php echo $Row->YearLevelID.' - '.$Row->YearLevelName;?></td>
							<td><?php echo $Row->FormSold; $formSold += $Row->FormSold; ?></td>
							<td><?php echo $Row->TargetSold; $targetFormSold += $Row->TargetSold;?></td>
							<td><?php echo $Row->Intake; $intake += $Row->Intake;?></td>
							<td><?php echo $Row->TargetIntake; $targetIntake += $Row->TargetIntake;?></td>
						</tr>
				<?php
					endforeach;
				?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2">&nbsp;</td>
						<td>TOTAL</td>
						<td><?php echo $formSold;?></td>
						<td><?php echo $targetFormSold;?></td>
						<td><?php echo $intake;?></td>
						<td><?php echo $targetIntake;?></td>
					</tr>
					<tr>
						<td colspan="3" style="text-align:center; font-size:16px; font-weight:bold;"> Form Sold = <?php echo ($targetFormSold != 0 ? round((($formSold / $targetFormSold) * 100),0) : '0'); ?> % </td>
						<td colspan="4" style="text-align:center; font-size:16px; font-weight:bold;"> Intake = <?php echo ($targetIntake != 0 ? round((($intake / $targetIntake) * 100),0) : '0');?> % </td>
					</tr>
				</tfoot>
				<?php
			}
			else
			{
				?>
				<tbody>
					<tr>
						<td colspan="7">There's No Data</td>
					</tr>
				</tbody>
				<?php
			}
				?>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>

<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Metrics_Report_Achievement.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Test Card</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmSearch" action="<?php echo site_url().'/';?>report/entrance_test_test_card/search" method="get">
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Schedule : </label>
								<span class="custom-combobox">
									<select name="ddlSchedule">
										<option value="">--Please Choose--</option>
										<?php foreach($Schedule as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchedule']))
												{
													if ($_GET['ddlSchedule'] == $Row->ScheduleID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->ScheduleID;?>"> 
												<?php echo $Row->ScheduleID;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<!--<p>
								<label>Form Number : </label>
								<span>
									<input type="text" maxlength="13" class="numeric" name="txtFormNumber"/>
								</span>
							</p>-->
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<form name="frmPrint" target="_blank" action="<?php echo site_url().'/';?>report/entrance_test_test_card/generate_report" method="get">
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblPrint" class="bordered zebra">
								<thead>
									<tr>
										<th>Registrant ID</th>
										<th>Registrant Name</th>
										<th><input type="checkbox" name="cbCheckAll" value="1" /></th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if (isset($data['result'])) { 
									foreach($data['result'] as $Row): ?>
									<tr data-id="<?php echo $Row->RegistrantID;?> ">
										<td><?php echo $Row->RegistrantID; ?></td>
										<td><?php echo $Row->RegistrantName; ?></td>
										<td>
											<input type="checkbox" name="cbCheck[]" value="<?php echo $Row->RegistrantID;?>" />
										</td>
									</tr>
									<?php endforeach; 
									}
									else { ?>
									<tr>
										<td colspan="3">
											There's No Data
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>
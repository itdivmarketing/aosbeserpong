<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>FORM SALES BINUS INTERNATIONAL SCHOOL SERPONG <br/>
				ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
				PERIOD <?php echo $_GET['txtStartPeriod']; ?> - <?php echo $_GET['txtEndPeriod']; ?></b>
			</div>
			<br/>
			<br/>
			<br/>
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				$SchoolLevel = $report[0]->SchoolLevelID;
				$SchoolLevelName = $report[0]->SchoolLevelName;
				$Amount = $report[0]->Amount;
				
				$total = 0;
				$amount = 0;
				$amountallocated = 0;
			?>
			School Level : <?php echo $SchoolLevel.' - '.$SchoolLevelName; ?> <br/>
			Biaya Formulir : Rp.  <?php echo $Amount ; ?>
			<br/>
			<table>
				<thead>
					<tr>
						<th>Sales Location</th>
						<th>Form Number</th>
						<th>Candidate Name</th>
						<th>Grade</th>
						<th>Amount</th>
						<th>Amount Allocated</th>
						<th>Payment Method</th>
					</tr>
				</thead>
				<tbody id="RateContent">
				<?php
				foreach($report as $Row):
					if($Row->SchoolLevelID == $SchoolLevel && $Row->Amount == $Amount) 
					{
					?>
						<tr>
							<td><?php echo $Row->LocatiONName; ?></td>
							<td><?php echo $Row->FormNo; $total+=1; ?></td>
							<td><?php echo $Row->RegistrantName; ?></td>
							<td><?php echo $Row->YearLevelName; ?></td>
							<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->Amount)),3))); $amount+=$Row->Amount; ?></td>
							<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->AmountAllocated)),3))); $amountallocated+=$Row->AmountAllocated; ?></td>
							<td><?php echo $Row->paymentMethodname; ?></td>
						</tr>
				<?php 
					} 
					else
					{
									
				?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total <?php echo $SchoolLevelName ; ?></th>
							<th><?php echo $total ; ?></th>
							<th></th>
							<th></th>
							<th><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($amount)),3))); ?></th>
							<th><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($amountallocated)),3))); ?></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				<br/>
				<br/>
				<div style="page-break-after:always;">&nbsp;</div>
				<div id="repeat">
						<div width="20%" style="float:right;">
					Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
					Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
					User :  <?php echo $UserID; ?><br/>
				</div>
				<div width="100%">
					<div width="100%" style="text-align:center;">
						<b>FORM SALES BINUS INTERNATIONAL SCHOOL SERPONG <br/>
						ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
						PERIOD <?php echo $_GET['txtStartPeriod']; ?> - <?php echo $_GET['txtEndPeriod']; ?></b>
					</div>
				</div>
				<br/>
				<br/>
				<br/>
				</div>
				<?php
					$SchoolLevel = $Row->SchoolLevelID;
					$SchoolLevelName = $Row->SchoolLevelName;
					$Amount = $Row->Amount;
					
					$total = 0;
					$amount = 0;
					$amountallocated = 0;
				?>
				School Level : <?php echo $SchoolLevel.' - '.$SchoolLevelName ; ?> <br/>
				Biaya Formulir : Rp.  <?php echo $Amount ; ?>
				<table>
					<thead>
						<tr>
							<th>Sales Location</th>
							<th>Form Number</th>
							<th>Candidate Name</th>
							<th>Grade</th>
							<th>Amount</th>
							<th>Amount Allocated</th>
							<th>Payment Method</th>
						</tr>
					</thead>
					<tbody id="RateContent">
						<tr>
							<td><?php echo $Row->LocatiONName; ?></td>
							<td><?php echo $Row->FormNo; $total+=1; ?></td>
							<td><?php echo $Row->RegistrantName; ?></td>
							<td><?php echo $Row->YearLevelName; ?></td>
							<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->Amount)),3))); $amount+=$Row->Amount; ?></td>
							<td><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($Row->AmountAllocated)),3))); $amountallocated+=$Row->AmountAllocated; ?></td>
							<td><?php echo $Row->paymentMethodname; ?></td>
						</tr>			
				<?php
					}
					if($i == $len-1)
					{
				?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total <?php echo $SchoolLevelName ; ?></th>
							<th><?php echo $total ; ?></th>
							<th></th>
							<th></th>
							<th><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($amount)),3))); ?></th>
							<th><?php echo 'Rp. '.strrev(implode('.',str_split(strrev(strval($amountallocated)),3))); ?></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
				<br/>
				<br/>
				<?php
					}
				$i++;
				endforeach;
				}
				else //no data
				{
					
				}
				?>
			<div>
				<b>Note : Funds will be transfered directly to the Finance Departement</b>
				</br>
				</br>
				</br>
				</br>
				</br>
				</br>
			</div>
			<div width="100%" style="float:left;">
				Jakarta, <?php echo $currentDate[0]->CurrDate;?> </br>
			</div>
			</br>
			<div width="100%" align="center">
				<div width="50%" style="float:left; display:block;">
					Created By,
					</br>
					</br>
					</br>
					</br>
					______________________</br>
					Marketing Manager
				</div>
				<div width="50%" style="float:right; display:block;">
					Approved By,
					</br>
					</br>
					</br>
					</br>
					<u><?php echo $_GET['txtFinancialController'];?></u></br>
					Financial Controller
				</div>
			</div>
		</div>
	</body>
	<br/>
	</br>
		</br>
		</br>
		</br>
		<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Cash_Report_Detail.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

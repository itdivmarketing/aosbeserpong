<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div style="float:left;">
			<img src="<?php echo base_url()?>resources/images/logo Serpong.jpg" width="200px"/>
		</div>
		<div id="data">
			<div style="width:30%; float:right">
				<table style="margin-bottom:0px;">
					<thead>
						<tr>
							<th> <?php echo (isset($report[0]->SchoolLevelName) ? $report[0]->SchoolLevelName : '-' );?> </th>
						</tr>
					</thead>
					<tr>
						<td> <?php echo (isset($report[0]) ? $report[0]->YearLevelName.' of '.$report[0]->SchoolLevelName : '-');?> <br/>
							 <?php echo $printedDate[0]->CurrDate; ?>
						</td>
					</tr>
				</table>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
					<b>ENTRANCE TEST RESULT</b>
				<!--<p style="text-align:left;">
					"A-world class community of proud and outstanding achievers"
				</p>-->
			</div>
			<table width="100%">
				<thead>
			<?php if(isset($report) && sizeof($report) > 0)
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = $report[0]->SchoolLevelID;
				if($report[0]->JenisAssesment == "") 
				{
			?>
					<tr>
						<th>#</th>
						<th>Registrant ID</th>
						<th>Name</th>
						<th>Grade</th>
						<th>DOB</th>
						<th>SY</th>
						<th>Previous School</th>
						<th>Math</th>
						<th>English</th>
						<th>Final Result</th>
						<th>Sibling</th>
					</tr>
			<?php 
				} 
				else 
				{
			?>
					<tr>
						<th rowspan="2">#</th>
						<th rowspan="2">Registrant ID</th>
						<th rowspan="2">Name</th>
						<th rowspan="2">Grade</th>
						<th rowspan="2">DOB</th>
						<th rowspan="2">SY</th>
						<th rowspan="2">Previous School</th>
						<th colspan="2"><?php echo $report[0]->JenisAssesment; ?></th>
						<th rowspan="2">Math</th>
						<th rowspan="2">English</th>
						<th rowspan="2">Final Result</th>
						<th rowspan="2">Sibling</th>
					</tr>
					<tr>
						<th>Recommendation</th>
						<th>Notes</th>
					</tr>
			<?php 
				}
			?>
				</thead>
				<tbody id="RateContent">
				<?php
					$i = 1;
					foreach($report as $Row):
						if($Row->JenisAssesment == "")
						{
				?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $Row->RegistrantID?></td>
								<td><?php echo $Row->RegistrantName?></td>
								<td><?php echo $Row->YearLevelName?></td>
								<td><?php echo $Row->DOB?></td>
								<td><?php echo $Row->AdmissionYear;?></td>
								<td><?php echo $Row->SchoolName;?></td>
								<td><?php echo $Row->NilMath;?></td>
								<td><?php echo $Row->NilEng;?></td>
								<td><?php echo $Row->SumLulus;?></td>
								<td><?php echo $Row->SiblingName;?></td>
							</tr>
				<?php
						}
						else
						{
				?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $Row->RegistrantID?></td>
								<td><?php echo $Row->RegistrantName?></td>
								<td><?php echo $Row->YearLevelName?></td>
								<td><?php echo $Row->DOB?></td>
								<td><?php echo $Row->AdmissionYear;?></td>
								<td><?php echo $Row->SchoolName;?></td>
								<td><?php echo $Row->Recommend;?></td>
								<td><?php echo $Row->NotesAssess;?></td>
								<td><?php echo $Row->NilMath;?></td>
								<td><?php echo $Row->NilEng;?></td>
								<td><?php echo $Row->SumLulus;?></td>
								<td><?php echo $Row->SiblingName;?></td>
							</tr>
				<?php
						}
						$i++;
					endforeach;
			}
			else
			{
				?>
					<thead>
						<tr>
							<th>#</th>
							<th>Registrant ID</th>
							<th>Name</th>
							<th>Grade</th>
							<th>DOB</th>
							<th>SY</th>
							<th>Previous School</th>
							<th>Math</th>
							<th>English</th>
							<th>Final Result</th>
							<th>Sibling</th>
						</tr>
					</thead>
					<tr>
						<td colspan="13">There's No Data</td>
					</tr>
				<?php
			}
				?>
				</tbody>
			</table>
			<br/>
			<div style="page-break-after:always;">&nbsp;</div>
			<div style="width:30%;float:left;">
				<table style="margin-bottom:0px;">
					<thead>
						<tr>
							<th colspan="2">RESULT</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Passed</td>
							<td></td>
						</tr>
						<tr>
							<td>Consideration</td>
							<td></td>
						</tr>
						<tr>
							<td>Re-Test</td>
							<td></td>
						</tr>
						<tr>
							<td>Failed</td>
							<td></td>
						</tr>
						<tr>
							<td>Waiting List</td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>Total</td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div style="width:70%;float:left;">
				<p style="text-align:center;">Jakarta, <?php echo $printedDate[0]->CurrDate; ?> </p>
				<br/>
				<br/>
				<br/>
				
				<div style="width:50%;float:left;">
					<p style="text-align:center;">VP - Student Development</p>
				</div>
				
				<div style="width:50%;float:left;">
					<p style="text-align:center;">Principal</p>
				</div>
				<br/>
				<br/>
				<br/>
					<p style="text-align:center;">Marketing Manager</p>
				<br/>
				<br/>
				<br/>
				
				<div style="width:50%;float:left;">
					<p style="text-align:center;">Marketing General Manager</p>
				</div>
				
				<div style="width:50%;float:left;">
					<p style="text-align:center;">Reporting Officer</p>
				</div>
			</div>
		</div>
	</body>
	<br/>
	</br>
	</br>
	</br>
	</br>
	<br/>
	</br>
	</br>
	</br>
	</br>
	<br/>
	</br>
	</br>
	</br>
	</br>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Entrance_Test_Result.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

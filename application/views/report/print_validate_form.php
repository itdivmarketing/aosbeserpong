<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					VALIDATE FORM RETURN REPORT <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
				</b>
			</div>
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = $report[0]->SchoolLevelID;
			?>
			<div width="100%">
				School Level : <?php echo $report[0]->SchoolLevelName;?><br/>
			</div>
			<table width="100%">
				<thead>
						<tr>
							<th>Registrant ID</th>
							<th>Registrant Name</th>
							<th>Term</th>
							<th>Year Level</th>
							<th>Mobile Phone</th>
							<th>Recipient Name</th>
							<th>Status</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $Row):
						if($Row->SchoolLevelID == $SchoolLevel)
						{
				?>
							<tr <?php echo ($Row->Status != "SUDAH" ? 'style="color:red"; font-weight:bold;' : '') ;?>>
								<td><?php echo $Row->FormNo?></td>
								<td><?php echo $Row->RegistrantName?></td>
								<td><?php echo $Row->TermName?></td>
								<td><?php echo $Row->YearLevelName?></td>
								<td><?php echo '&nbsp;'.$Row->PhoneNumberSale;?></td>
								<td><?php echo $Row->RecepientName;?></td>
								<td><?php echo $Row->Status;?></td>
							</tr>
				<?php
						}
						else
						{
				?>
				</tbody>
			</table>
			<div style="page-break-after:always;">&nbsp;</div>
			<div id="repeat">
				<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div width="100%" style="text-align:center;">
					<b>
						VALIDATE FORM RETURN REPORT <br/>
						ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?><br/>
					</b>
				</div>
			</div>
			<div width="100%">
				School Level : <?php echo $SchoolLevel;?><br/>
			</div>
			<table width="100%">
				<thead>
						<tr>
							<th>Registrant ID</th>
							<th>Registrant Name</th>
							<th>Term</th>
							<th>Year Level</th>
							<th>Mobile Phone</th>
							<th>Recipient Name</th>
							<th>Status</th>
						</tr>
				</thead>
				<tbody id="RateContent">
					<tr <?php echo ($Row->Status != "SUDAH" ? 'fore-color="red"' : '') ;?>>
						<td><?php echo $Row->FormNo?></td>
						<td><?php echo $Row->RegistrantName?></td>
						<td><?php echo $Row->TermName?></td>
						<td><?php echo $Row->YearLevelName?></td>
						<td><?php echo $Row->PhoneNumberSale;?></td>
						<td><?php echo $Row->RecepientName;?></td>
						<td><?php echo $Row->Status;?></td>
					</tr>
				<?php
						}
					endforeach;
			}
			else
			{
				?>
					<tr>
						<td colspan="7">There's No Data</td>
					</tr>
				<?php
			}
				?>
				</tbody>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Validate_Form.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Full Payment Confirmation</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmSearch2" action="<?php echo site_url().'/';?>report/billing_and_payment_full_payment_confirmation/search" method="get">
							<p>
								<label>Registrant ID : </label>
								<span>
									<input type="text" name="txtRegistrantID" value="<?php echo (isset($_GET['txtRegistrantID']) ? $_GET['txtRegistrantID']: '') ;?>" class="numeric"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
							</p>
						</form>
						<form name="frmSearch" target="_blank" action="<?php echo site_url().'/';?>report/billing_and_payment_full_payment_confirmation/generate_report" method="get">
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php echo (isset($report[0]) ? $report[0]->AdmissionYear : '');?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
								<input type="text" name="txtAdmissionSemester" value="<?php echo (isset($report[0]) ? $report[0]->SmtId : '') ;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span>
									<input type="text" name="txtSchoolLevel" value="<?php echo (isset($report[0]) ? $report[0]->SchoolLevelName : '') ;?>" readonly="readonly"/>
									<input type="hidden" name="hfSchoolLevel" value="<?php echo (isset($report[0]) ? $report[0]->SchoolLevelID : '') ;?>"/>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span>
									<input type="text" name="txtAdmissionTerm" value="<?php echo (isset($report[0]) ? $report[0]->TermName : '') ;?>" readonly="readonly"/>
									<input type="hidden" name="hfAdmissionTerm" value="<?php echo (isset($report[0]) ? $report[0]->TermId : '') ;?>"/>
								</span>
							</p>
							<p>
								<label>Year Level : </label>
								<span>
									<input type="text" name="txtYearLevel" value="<?php echo (isset($report[0]) ? $report[0]->YearLevelName : '') ;?>" readonly="readonly"/>
									<input type="hidden" name="hfYearLevel" value="<?php echo (isset($report[0]) ? $report[0]->YearLevelID : '') ;?>"/>
								</span>
							</p>
							<p>
								<label>Name : </label>
								<span>
									<input type="text" name="txtName" value="<?php echo (isset($report[0]) ? $report[0]->RegistrantName : '') ;?>" readonly="readonly"/>
									<input type="hidden" name="hfRegistrantID" value="<?php echo (isset($report[0]) ? $report[0]->RegistrantID : '') ;?>"/>
								</span>
							</p>
							<p>
								<label>Letter Number : </label>
								<span>
									<input type="text" name="txtCode" value="<?php echo (isset($new_letter[0]) ? $new_letter[0]->RunningNumber : '');?>" style="width:50px" /> 
									<!--<label style="margin-top: -33px; margin-left:60px">&nbsp; </label> 
									<input type="text" name="txtLetterNumber" value="<?php// echo (isset($new_letter[0]) ? $new_letter[0]->LetterNumber : '') ;?>" 
									style="width: 467px; margin-top: -34px; margin-left: 80px;"/>-->
									<input type="text" name="txtLetterNumber" value="<?php echo (isset($new_letter[0]) ? $new_letter[0]->LetterNumber : '') ;?>" 
									style="width: 590px;margin-top: -40px;margin-left: 50px;"/>
								</span>
							</p>
							<!--<p>
								<label>Reminder : </label>
								<span>
									<input type="checkbox" name="cbSpecial" <?php //echo (isset($report[0]) ? ($report[0]->FlagDiskon == 1 ? 'checked="checked"' : '') : '');?>  value="1"/> Yes
								</span>
							</p>
							<p>
								<label>Packege Fee : </label>
								<span>
									<select name="ddlPackage" <?php //echo (isset($report[0]) ? ($report[0]->FlagDiskon == 1 ? '' : 'disabled="disabled"') : '');?>>
										<option value="">--Please Choose--</option>
										<?php
											//if($package)
											//{
												//foreach ($package as $Row) :
										?>
											<option <?php //echo (isset($report[0]) ? ($report[0]->PaketId == $Row->PaketId ? 'selected="selected"' : '') : ''); ?> value="<?php echo $Row->PaketId;?>"><?php echo $Row->NmPaket;?></option>
										<?php
											//	endforeach;
											//}
										?>
									</select>
								</span>
							</p>
							<p>
								<label>Discount : </label>
								<span>
									<input type="checkbox" name="cbDiscount" 
									 <?php //echo (isset($report[0]) ? ($report[0]->FlagDiskon == 1 ? 'checked="checked"' : 'disabled="disabled"') : 'checked="checked"');?>
									value="1"/> Discount Sibling 10% Enrollment Fee
								</span>
							</p> -->

							<p>
								<label>Languange :</label>
								<span>
									<input type="checkbox" name="lg[]" value="id" checked="checked"> Indonesian 
									<br>
									<input type="checkbox" name="lg[]" value="en" checked="checked"> English
								</span>
							</p>
							
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
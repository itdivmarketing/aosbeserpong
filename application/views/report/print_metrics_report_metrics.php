<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					MARKETING METRICS STATISTIC <br/>
					ACADEMIC YEAR <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?> SEMESTER <?php echo ($Semester); ?> <br/>
				</b>
			</div>
			<?php if(isset($report))
			{
			?>
			<table width="100%">
				<thead>
						<tr>
							<th>School Level</th>
							<th>Year Level</th>
							<th>Form Sold</th>
							<th>Target Form Sold</th>
							<th>% Sold</th>
							<th>Register</th>
							<th>Test</th>
							<th>% Test</th>	
							<th>Pass</th>
							<th>% Pass</th>
							<th>Payment</th>
							<th>% Payment</th>
							<th>Target Intake</th>
							<th>% Payment Target</th>
							<th>Generate ID</th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					$formSold = 0;
					$targetFormSold = 0;
					$register = 0;
					$generateID = 0;
					$pass = 0;
					$payment = 0;
					$test = 0;
					$targetIntake = 0;
					foreach($report as $Row):
				?>
						<tr>
							<td><?php echo $Row->SchoolLevelID.' - '.$Row->SchoolLevelName;?></td>
							<td><?php echo $Row->YearLevelID.' - '.$Row->YearLevelName;?></td>
							<td><?php echo $Row->FormSold; $formSold += $Row->FormSold; ?></td>
							<td><?php echo $Row->TargetSold; $targetFormSold += $Row->TargetSold;?></td>
							<td><?php echo ($Row->TargetSold != 0 ? round((($Row->FormSold / $Row->TargetSold) * 100),0).'%' : '0%'); ?></td>
							<td><?php echo $Row->Register; $register += $Row->Register;?></td>
							<td><?php echo $Row->Test; $test += $Row->Test;?></td>
							<td><?php echo ($Row->Register != 0 ? round((($Row->Test / $Row->Register) * 100),0).'%' : '0%'); ?></td>
							<td><?php echo $Row->PassTest; $pass += $Row->PassTest;?></td>
							<td><?php echo ($Row->Test != 0 ? round((($Row->PassTest / $Row->Test) * 100),0).'%' : '0%'); ?></td>
							<td><?php echo $Row->Payment; $payment += $Row->Payment;?></td>
							<td><?php echo ($Row->PassTest != 0 ? round((($Row->Payment / $Row->PassTest) * 100),0).'%' : '0%'); ?></td>
							<td><?php echo $Row->TargetIntake; $targetIntake += $Row->TargetIntake;?></td>
							<td><?php echo ($Row->TargetIntake != 0 ? round((($Row->Payment / $Row->TargetIntake) * 100),0).'%' : '0%'); ?></td>
							<td><?php echo $Row->GenerateId; $generateID += $Row->GenerateId; ?></td>
						</tr>
				<?php
					endforeach;
				?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2">GRAND TOTAL</td>
						<td><?php echo $formSold;?></td>
						<td><?php echo $targetFormSold;?></td>
						<td><?php echo ($targetFormSold != 0 ? round((($formSold / $targetFormSold) * 100),0).'%' : '0%'); ?></td>
						<td><?php echo $register;?></td>
						<td><?php echo $test;?></td>
						<td><?php echo ($register != 0 ? round((($test / $register) * 100),0).'%' : '0%'); ?></td>
						<td><?php echo $pass;?></td>
						<td><?php echo ($test != 0 ? round((($pass / $test) * 100),0).'%' : '0%'); ?></td>
						<td><?php echo $payment;?></td>
						<td><?php echo ($pass != 0 ? round((($payment / $pass) * 100),0).'%' : '0%'); ?></td>
						<td><?php echo $targetIntake;?></td>
						<td><?php echo ($targetIntake != 0 ? round((($payment / $targetIntake) * 100),0).'%' : '0%'); ?></td>
						<td><?php echo $generateID;?></td>
					</tr>
				</tfoot>
				<?php
			}
			else
			{
				?>
				<tbody>
					<tr>
						<td colspan="15">There's No Data</td>
					</tr>
				</tbody>
				<?php
			}
				?>
			</table>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'Metrics_Report.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

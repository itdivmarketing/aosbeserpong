<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<style>
			table tbody tr td:first-child {
				border-left:1px solid black;
			}
			table tbody tr td {
				border-left:1px solid black;
			}
			table thead th {
				font-weight:bold;
				border-left:1px solid black;
			}
			table{
		
				border-top:1px solid black;
				border:1px solid black;
				border-bottom:1px solid black;
			}
			table td{
				border-top:1px solid black;
				border-left: 1px solid black;
				border-bottom:1px solid black;
			}
			table th{
				font-weight:bold;
				border-top:1px solid black;
				text-align:center;
				border-left: 1px solid black;
				border-bottom:1px solid black;
			}
			
			
			</style>
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>LAPORAN VALIDASI KELENGKAPAN DOKUMEN REGISTRAN <br/>
				TAHUN AJARAN <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?></b><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<?php if(isset($report))
			{
				$i = 0;
				$len = count($report);
				$SchoolLevel = $report[0]->SchoolLevelID;
				$SchoolLevelName = $report[0]->SchoolLevelName;
			
				
				$total = 0;

			?>
			<b>School Level : <?php echo $SchoolLevel.' - '.$SchoolLevelName; ?></b> <br/>
	
			<br/>
			<table>
				<thead>
					<tr>
						<th>RegistrantID</th>
						<th>Registrant Name</th>
						<th>Term</th>
						<th>Year Level</th>
						
						<?php 
						if (isset($DocumentList)){
							foreach($DocumentList as $DocRow): ?>
								<th>
									<?php echo $DocRow->DocumentName; ?>
								</th>
							<?php endforeach; 
						}
						?>
					</tr>
				</thead>
				<tbody id="RateContent">
				<?php
			
				foreach($report as $Row):
					if($Row->SchoolLevelID == $SchoolLevel) 
					{
					?>
						<tr>
							<td><?php echo $Row->RegistrantId; ?></td>
							<td><?php echo $Row->RegistrantName; $total+=1; ?></td>
							<td><?php echo $Row->TermID; ?></td>
							<td><?php echo $Row->YearLevelName; ?></td>
							<?php 
							if (isset($DocumentList)){
								foreach($DocumentList as $DocRow): ?>
									<td>
										<?php $ColName=$DocRow->DocumentId.'a'; ?>
										<?php echo $Row->{"$ColName"}; ?> 
									</td>
								<?php endforeach; 
							}
							?>
						</tr>
				<?php 
					} 
					else
					{
									
				?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total <?php echo $SchoolLevelName ; ?></th>
							<th><?php echo $total ; ?></th>
							<th></th>
							<th></th>
							<?php 
							if (isset($DocumentList)){
								foreach($DocumentList as $DocRow): ?>
									<th>
									
									</th>
								<?php endforeach; 
							}
							?>
						</tr>
					</tfoot>
				</table>
				<br/>
				<br/>
				<div style="page-break-after:always;">&nbsp;</div>
				<div id="repeat">
						<div width="20%" style="float:right;">
					Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
					Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
					User :  <?php echo $UserID; ?><br/>
				</div>
				<div width="100%" style="text-align:center;">
					<b>LAPORAN VALIDASI KELENGKAPAN DOKUMEN REGISTRAN <br/>
					TAHUN AJARAN <?php echo $AcademicYear; ?> / <?php echo ($AcademicYear+1); ?></b><br/>
				</div>
				<br/>
				<br/>
				<br/>
				</div>
				<?php
					$SchoolLevel = $Row->SchoolLevelID;
					$SchoolLevelName = $Row->SchoolLevelName;
				
					
					$total = 0;
		
				?>
				<b>School Level : <?php echo $SchoolLevel.' - '.$SchoolLevelName ; ?></b> <br/>
			
				<table>
					<thead>
						<tr>
							<th>RegistrantID</th>
							<th>Registrant Name</th>
							<th>Term</th>
							<th>Year Level</th>
							<?php 
							if (isset($DocumentList)){
								foreach($DocumentList as $DocRow): ?>
								<th>
									<?php echo $DocRow->DocumentName; ?>
								</th>
								<?php endforeach; 
							}
							?>
						</tr>
					</thead>
					<tbody id="RateContent">
					<tr>
							<td><?php echo $Row->RegistrantId; ?></td>
							<td><?php echo $Row->RegistrantName; $total+=1; ?></td>
							<td><?php echo $Row->TermID; ?></td>
							<td><?php echo $Row->YearLevelName; ?></td>
								<?php 
							if (isset($DocumentList)){
								foreach($DocumentList as $DocRow): ?>
									<td>
										<?php echo $DocRow->DocumentId; ?>
									</td>
								<?php endforeach; 
							}
							?>
						</tr>		
				<?php
					}
					if($i == $len-1)
					{
				?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total <?php echo $SchoolLevelName ; ?></th>
							<th><?php echo $total ; ?></th>
							<th></th>
							<th></th>
							<?php 
							if (isset($DocumentList)){
								foreach($DocumentList as $DocRow): ?>
									<th>
									
									</th>
								<?php endforeach; 
							}
							?>
						</tr>
					</tfoot>
				</table>
				<?php
					}
				$i++;
				endforeach;
				}
				else //no data
				{
					
				}
				?>
	

		</div>
	</body>
	
		<a href="#" id="btnExportExcel" class="button">Export To Excel</a> 
		<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/form_return_validate_document/print_report_pdf/">
			<input type="hidden" name="hfContent" />
		<a href="#" id="btnExportPDF" class="button">Export To PDF</a> 
		</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExportExcel').click(function(e) {
			$(this)
				.attr({
					'download': 'Return_Validate_Document.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});
		$('#btnExportPDF').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

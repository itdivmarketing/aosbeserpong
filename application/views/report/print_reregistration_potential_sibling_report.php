<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div id="data">
			<div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<div width="100%" style="text-align:center;">
				<b>
					POTENTIAL SIBLING REPORT<br/>
				</b>
			</div>
			<?php //$total = 0;
			if(isset($report))
			{
				$i = 0;
				$len = count($report);
				
				$SchoolLevel = $report[0]->SchoolLevelID;
				//$total = 0;
			?>
			<div width="100%">
				<!--hool Level : <?php echo $report[0]->SchoolLevelName;?><br/>-->
			</div>
			<table width="100%">
				<thead>
						<tr>
							<th><strong>Potential Sibling Name</strong></th>
							<th><strong>Birthday</strong></th>
							<th><strong>Age</strong></th>
							<th><strong>HP Father</strong></th>
							<th><strong>Email Father</strong></th>
							<th><strong>HP Mother</strong></th>
							<th><strong>Email Mother</strong></th>
							<th><strong>Potential For School Level</strong></th>
							<th><strong>Potential For Year Level</strong></th>
							<th><strong>Registration ID / Student ID</strong></th>
							<th><strong>Sibling Name</strong></th>
							<th><strong>Year Level Sibling</strong></th>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $Row):
						if($Row->SchoolLevelID == $SchoolLevel)
						{
				?>
								<tr>
									<td><?php echo $Row->PotentialSiblingName?></td>
									<td><?php echo $Row->Birthday?></td>
									<td><?php echo floatval($Row->Age)?></td>
									<td><?php echo '&nbsp;'.$Row->FatherHP?></td>
									<td><?php echo $Row->FatherEmail?></td>
									<td><?php echo '&nbsp;'.$Row->MotherHP?></td>
									<td><?php echo $Row->MotherEmail?></td>
									<td><?php echo $Row->SchoollevelName?></td>
									<td><?php echo $Row->YearLevelName?></td>
									<td><?php echo $Row->RegistrantID?></td>
									<td><?php echo $Row->SiblingName?></td>
									<td><?php echo $Row->SiblingYearLevel?></td>
								</tr>
				<?php
						}
						else
						{
							//$SchoolLevel = $Row->SchoolLevelID
				?>
				</tbody>
			</table>			
			<!--<div width="100%">
				Total Registrant : <?php echo $total; $total=0;?><br/>
			</div>-->
			<div style="page-break-after:always;">&nbsp;</div>
			<!--<div id="repeat">
				 <div width="20%" style="float:right;">
				Date : <?php echo $printedDate[0]->CurrDate; ?><br/>
				Time  : <?php echo $printedTime[0]->CurrDate; ?> <br/>
				User :  <?php echo $UserID; ?><br/>
				</div> 
				<br/>
				<br/>
				<br/>
				<br/>
				<div width="100%" style="text-align:center;">
					<b>
						POTENTIAL SIBLING REPORT<br/>
					</b>
				</div>
			</div>-->
			<!-- <div width="100%">
				School Level : <?php echo $Row->SchoolLevelName;?><br/>
			</div> -->
			<table width="100%">
				<!-- <thead>
					<tr>
							<th>Potential Sibling Name</th>
							<th>Birthday</th>
							<th>Age</th>
							<th>HP Father</th>
							<th>Email Father</th>
							<th>HP Mother</th>
							<th>Email Mother</th>
							<th>Potential For School Level</th>
							<th>Potential For Year Level</th>
							<th>Registration ID / Student ID</th>
							<th>Sibling Name</th>
							<th>Year Level Sibling</th>
					</tr>
				</thead> -->
				<!-- <tbody id="RateContent">
					<tr>
						<tr>
							<td><?php echo $Row->PotentialSiblingName?></td>
							<td><?php echo $Row->Birthday?></td>
							<td><?php echo floatval($Row->Age)?></td>
							<td><?php echo $Row->FatherHP?></td>
							<td><?php echo $Row->FatherEmail?></td>
							<td><?php echo $Row->MotherHP?></td>
							<td><?php echo $Row->MotherEmail?></td>
							<td><?php echo $Row->SchoolLevelID?></td>
							<td><?php echo $Row->SiblingYearLevel?></td>
							<td><?php echo $Row->RegistrantID?></td>
							<td><?php echo $Row->SiblingName?></td>
							<td><?php echo $Row->YearLevelName?></td>
						</tr>
					</tr> -->
				<?php
						}
					endforeach;
			}
			else
			{
				?>
					<tr>
						<td colspan="8">There's No Data</td>
					</tr>
				<?php
			}
				?>
				</tbody>
			</table>
			 <!-- <div width="100%">
				Total Registrant : <?php echo $total;?><br/>
			</div> -->
		</div>
	</body>
	<a href="#" id="btnExportExcel" class="button">Export To Excel</a>
	<br/>
	
	<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/reregistration_potential_sibling/print_report_pdf/">
			<input type="hidden" name="hfContent" />
		<a href="#" id="btnExportPDF" class="button">Export To PDF</a> 
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExportExcel').click(function(e) {
			$(this)
				.attr({
					'download': 'reregistration_potential_sibling_report.xls',
					'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
					'target': '_blank'
			});
		});

		$('#btnExportPDF').click(function(e) {
			$('#data').find("table").attr('border', '1');
			//$('#data').find("table").find("tr").find("thead").attr("font-weight","bold");
			$('[name="hfContent"]').val($('#data').html());
			$('#data').find("table").attr('border', '0');
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

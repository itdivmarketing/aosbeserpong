<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Detail Payment Report</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmSearch" target="_blank" action="<?php echo site_url().'/';?>report/billing_and_payment_detail_payment/generate_report" method="get">
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							
							
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="all">All</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level : </label>
								<span class="custom-combobox">
									<select name="ddlYearLevel">
										<option value="all">All</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Payment Method : </label>
								<span class="custom-combobox">
									<select name="ddlPaymentMethod">
										<option value="0">ALL</option>
										<?php foreach($Payment as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlPaymentMethod']))
												{
													if ($_GET['ddlPaymentMethod'] == $Row->PaymentMethodID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->PaymentMethodID;?>"> 
												<?php echo ltrim(rtrim($Row->PaymentMethodName));?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Payment Type : </label>
								<span>
									<input type="checkbox" name="cbxAll" checked="checked" value="0"/> All <br/>
									<input type="checkbox" name="cbxFull" value="1" class="cbxType"/> Fully Paid <br/>
									<input type="checkbox" name="cbxNotFull" value="2" class="cbxType"/> Partially Paid <br/>
									<input type="checkbox" name="cbxNotPaid" value="3" class="cbxType"/> Un-Paid
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
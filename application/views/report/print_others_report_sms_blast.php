<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	 <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
	 <style type="text/css" media="print">
			.button {
			display:none;
			}
		</style>
   </head>
	<body>
		<div >
			<?php if(isset($report) && isset($field))
			{
			?>
			<table width="100%" id="data">
				<thead>
						<tr>
							<?php

							foreach($field as $row){
								if(array_key_exists($row->Column_Name, $field_filter)){								
							
							?>
										<th><?php echo $row->Column_Name;?></th>
							<?php
									}
								}
							?>
						</tr>
				</thead>
				<tbody id="RateContent">
				<?php
					foreach($report as $key):
				?>		
						<tr>
				<?php
						foreach ($key as $key2 => $value2):
							foreach ($field as $row):
								$column = str_replace(' ', '', $row->Column_Name);
						
								if(array_search($column,$_GET['cbField']) !== false)
								{
									if (str_replace(' ', '', $key2)  == $column)
									{
				?>
										<td><pre><?php echo $value2;?></pre></td>
				<?php
									}
								}
							endforeach;	
						endforeach;
				?>
						</tr>
				<?php
					endforeach;
				?>
				</tbody>
			</table>
				<?php
			}
				?>
		</div>
	</body>
	<a href="#" id="btnExport" class="button">Export To Excel</a> 
</html>
<script>
(function($){
	$(document).ready(function(){
		// $('#btnExport').click(function(e) {
		// 	$(this)
		// 		.attr({
		// 			'download': 'Student_Information_Report.xls',
		// 			'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
		// 			'target': '_blank'
		// 	});
		// });
		// $('#btnExport').click(function(e) {
		// 	$(this).attr({
		// 		'download': 'Student_Information_Report.xls',
		// 		'href': 'data:application/vnd.ms-excel,' + encodeURIComponent($('#data').html()),
		// 		'target': '_blank'
		// });});
		jQuery.fn.table2CSV = function(options) {
			    var options = jQuery.extend({
		        separator: ',',
		        header: [],
		        delivery: 'popup' // popup, value
		    },
		    options);

		    var csvData = [];
		    var headerArr = [];
		    var el = this;

		    //header
		    var numCols = options.header.length;
		    var tmpRow = []; // construct header avalible array

		    if (numCols > 0) {
		        for (var i = 0; i < numCols; i++) {
		            tmpRow[tmpRow.length] = formatData(options.header[i]);
		        }
		    } else {
		        $(el).filter(':visible').find('th').each(function() {
		            if ($(this).css('display') != 'none') tmpRow[tmpRow.length] = formatData($(this).html());
		        });
		    }

		    row2CSV(tmpRow);

		    // actual data
		    $(el).find('tr').each(function() {
		        var tmpRow = [];
		        $(this).filter(':visible').find('td').each(function() {
		            if ($(this).css('display') != 'none') tmpRow[tmpRow.length] = formatData($(this).html());
		        });
		        row2CSV(tmpRow);
		    });
		    if (options.delivery == 'popup') {
		        var mydata = csvData.join('\n');
		        return popup(mydata);
		    } else {
		        var mydata = csvData.join('\n');
		        return mydata;
		    }

		    function row2CSV(tmpRow) {
		        var tmp = tmpRow.join('') // to remove any blank rows
		        // alert(tmp);
		        if (tmpRow.length > 0 && tmp != '') {
		            var mystr = tmpRow.join(options.separator);
		            csvData[csvData.length] = mystr;
		        }
		    }
		    function formatData(input) {
		        // replace " with â€œ
		        var regexp = new RegExp(/["]/g);
		        var output = input.replace(regexp, "â€œ");
		        //HTML
		        var regexp = new RegExp(/\<[^\<]+\>/g);
		        var output = output.replace(regexp, "");
		        if (output == "") return '';
		        return '"' + output + '"';
		    }
		    function popup(data) {
		        var generator = window.open('', 'csv', 'height=400,width=600');
		        generator.document.write('<html><head><title>CSV</title>');
		        generator.document.write('</head><body >');
		        generator.document.write('<textArea cols=70 rows=15 wrap="off" >');
		        generator.document.write(data);
		        generator.document.write('</textArea>');
		        generator.document.write('</body></html>');
		        generator.document.close();
		        return true;
		    }
		};

		var data = $('#data').table2CSV({delivery:'value'});

		// $('<a></a>')
		//     .attr('id','downloadFile')
		//     .attr('href','data:text/csv;charset=utf8,' + encodeURIComponent(data))
		//     .attr('download','filename.csv')
		//     .appendTo('body');

		// $('#downloadFile').ready(function() {
		//     $('#downloadFile').get(0).click();
		// });

		$('#btnExport').click(function(e) {
			$(this)
				.attr({
					'download': 'report_for_sms_blast.csv',
					'href': 'data:text/csv;charset=utf8,' + encodeURIComponent(data),
					'target': '_blank'
			});
		});
	})
}(jQuery))
</script>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
   </head>
	<?php $this->load->helper('admission_helper');?>
	<body>
		<div id="data">
			<div width="30%">
				<p>
					Jakarta, <?php echo $printedDate[0]->CurrDate; ?><br/>
					No : <?php echo $_POST['txtCode'].'/'.$_POST['txtLetterNumber']; ?>
				</p>
			</div>
			<div width="100%" style="text-align:center;">
					<b><u>Subjek : Surat Notifikasi Hasil Ujian Masuk </u></b>
			</div>
			<?php if(isset($report))
			{
			?>
			<div width="100%" style="text-align:left;">
				<p>
					Yth. Bpk/Ibu <?php echo $report[0]->FatherName.' '.$report[0]->MotherName?>, <br/>
				</p>
				<p>
					Mohon membaca notifikasi berikut dengan seksama <br/>
					Berdasarkan Hasil Tes Masuk Akhir, kami ingin memberitahukan bahwa anak Bpk/Ibu: <br/>
				</p>
				<p style="padding-left:30px;">
						Nama			: <?php echo $report[0]->RegistrantName.' ('.$report[0]->RegistrantID.')'; ?> <br/>
						Level Akademis	: <?php echo $report[0]->AdmissionYear; ?> <br/>
						Hasil			: Direkomendasikan <br/>
				</p>			
				<p>
					Kami menyambut dengan baik apabila ada pertanyaan yang ingin ditanyakan. Terima kasih atas kerja sama dan perhatiannya.
				</p>
				<br/>
				</br>
				<p>
					Hormat kami,<br/>
					<?php 
						if(isset($_GET['cbSignature']))
						{
					?>
							<img src="<?php echo base_url();?>resources/images/digital_signature.jpg" width="100px;" /><br/>
					<?php 
						}
						else
						{
					?>
							<br/><br/><br/><br/><br/>
					<?php 	
						}
					?>
					Marketing Manager<br/>
					<i>KSY/adm</i>
				</p>
			</div>
			<?php 
			}?>
		</div>
	</body>
		<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/billing_and_payment_notification_letter/print_report/">
		<input type="hidden" name="hfContent" />
	<a href="#" id="btnExport" class="button">Export To PDF</a> 
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

<!DOCTYPE html>
<html>
	<head>
		<title>BINA NUSANTARA | Admission School BackEnd</title>
    <!-- CSS -->
    <link href="<?php echo base_url()?>resources/css/report.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url()?>resources/css/report-print.css" rel="stylesheet" type="text/css" media="print">
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/core/jquery-1.7.2.min.js"></script>
   </head>
	<?php $this->load->helper('admission_helper');?>
	<body>
		<div id="data">
			<div width="30%">
				<p>
					Jakarta, <?php echo $printedDate[0]->CurrDate; ?><br/>
					No : <?php echo $_POST['txtCode'].'/'.$_POST['txtLetterNumber']; ?>
				</p>
			</div>
			<div width="100%" style="text-align:center;">
					<b><u>Subject : Notification Letter of Entrance Test Result </u></b>
			</div>
			<?php if(isset($report))
			{
			?>
			<div width="100%" style="text-align:left;">
				<p>
					Dear Mr. & Mrs. <?php echo $report[0]->FatherName.' '.$report[0]->MotherName?>, <br/>
				</p>
				<p>
					Please read the following notification carefully <br/>
					Based on the Final Entrance Test Result, we would like to inform you, that your child: <br/>
				</p>
				<p style="padding-left:30px;">
						Name			: <?php echo $report[0]->RegistrantName.' ('.$report[0]->RegistrantID.')'; ?> <br/>
						Academic Level	: <?php echo $report[0]->AdmissionYear; ?> <br/>
						Result			: Not Recommended <br/>
				</p>
				
				<p>
					You're most welcome to contact us at any time if you have any points to clarify, or questions to inquire. Thank you for your kind attention and cooperation.
				</p>
				<br/>
				</br>
				<p>
					Yours Sincerely,<br/>
					<?php 
						if(isset($_GET['cbSignature']))
						{
					?>
							<img src="<?php echo base_url();?>resources/images/digital_signature.jpg" width="100px;" /><br/>
					<?php 
						}
						else
						{
					?>
							<br/><br/><br/><br/><br/>
					<?php 	
						}
					?>
					Marketing Manager<br/>
					<i>KSY/adm</i>
				</p>
			</div>
			<?php 
			}?>
		</div>
	</body>
		<form target="_blank" method="post" name="export" action="<?php echo site_url().'/' ;?>report/billing_and_payment_notification_letter/print_report/">
		<input type="hidden" name="hfContent" />
	<a href="#" id="btnExport" class="button">Export To PDF</a> 
	</form>
</html>
<script>
(function($){
	$(document).ready(function(){
		$('#btnExport').click(function(e) {
			$('[name="hfContent"]').val($('#data').html());
			$('[name="export"]').submit();
		});
	})
}(jQuery))
</script>

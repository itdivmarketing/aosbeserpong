<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Metrics Report</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
				<form name="frmSearch" target="_blank" action="<?php echo site_url().'/';?>report/manager_metrics_report/generate_report" method="get">
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							
							<p>
								<label>Report Type : </label>
								<span>
									<input type="radio" name="rbReportType" checked="checked" value="report_achievement"/> Report Achievement <br/>
									<input type="radio" name="rbReportType" value="form_sold"/> Development Form Sold Chart <br/>
									<input type="radio" name="rbReportType" value="metrics_statistic"/> Marketing Metrics Statistic
								</span>
							</p>
							<div id="div_form_sold">
								<p>
									<label>School Level : </label>
									<span class="custom-combobox">
										<select name="ddlSchoolLevel">
											<option value="all">All</option>
											<?php foreach($SchoolLevel as $Row): ?>
												<option 
													<?php if(isset($_GET['ddlSchoolLevel']))
													{
														if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
														echo 'selected="selected"'; 
													}
													else 
													{
														echo '';
													} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
													<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
												</option>
											<?php endforeach; ?>
										</select>
										<input type="hidden" name="hfSchoolLevel"/>
									</span>
								</p>
							</div>
							<div id="div_marketing_metrics">
								<p>
									<label>Admission Term : </label>
									<span class="custom-combobox">
										<select name="ddlAdmissionTerm">
											<option value="all">All</option>
											<?php foreach($AdmissionTerm as $Row): ?>
												<option 
													<?php if(isset($_GET['ddlAdmissionTerm']))
													{
														if ($_GET['ddlAdmissionTerm'] == $Row->TermNumber )
														echo 'selected="selected"'; 
													}
													else 
													{
														echo '';
													} ?> value="<?php echo $Row->TermNumber;?>"> 
													<?php echo 'Term' .' - '. $Row->TermNumber;?> 
												</option>
											<?php endforeach; ?>
										</select>
										<input type="hidden" name="hfAdmissionTerm"/>
									</span>
								</p>
							</div>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
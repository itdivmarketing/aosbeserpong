<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Potential Sibling Report</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
				<form name="frmSearch" target="_blank" action="<?php echo site_url().'/';?>report/reregistration_potential_sibling/generate_report" method="get">
							<p>
								<label>Potential for School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							
							<p>
								<label>Potential for Year Level : </label>
								<span class="custom-combobox">
									<select name="ddlYearLevel">
										<option value="all">All</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php 
													if(isset($_GET['ddlYearLevel']))
													{
														if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
														echo 'selected="selected"'; 
													}
													else 
													{
														echo '';
													} 
												?> 
												value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Report For SMS Blast</h2>
				<div class="container w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmSearch" target="_blank" action="<?php echo site_url().'/';?>report/others_report_sms_blast/generate_report" method="get">
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<!-- <p>
								<label>Admission Term : </label>
								<span>
									<select name="ddlAdmissionTerm">
										<option value="all">ALL</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p> -->
							<p>
								<label>Year Level : </label>
								<span >
									<div name="YearLevel">
										<input type="checkbox" name="cbYearLevelAll" value="all"/> All </br>
									</div>
								</span>
							</p>
							<p>
								<label>Contact : </label>
								<span>
									<div name="Contact">
										<input type="checkbox"  name="cbContactAll" value="all"/> All </br>
										<?php
											// if($Field)
											// {
											// 	foreach ($Field as $Row) :
										?>
												<!-- <input type="checkbox"  class="field" name="cbField[]" value="<?php echo str_replace(' ', '', $Row->Column_Name); ?>"/> <?php echo $Row->Column_Name; ?> </br>	 -->
												<input type="checkbox"  class="Contact" name="cbContact[]" value="F"/>HP Father</br>	
												<input type="checkbox"  class="Contact" name="cbContact[]" value="M"/>HP Mother</br>	
												<input type="checkbox"  class="Contact" name="cbContact[]" value="G"/>HP Guardian</br>	
										<?php
											// 	endforeach;
											// }
										
										?>
									</div>
								</span>
							</p>

							<p>
								<label>Field : </label>
								<span>
									<div name="Field">
										<input type="checkbox"  name="cbFieldAll" value="all"/> All </br>
										<?php
											if($Field)
											{
												foreach ($Field as $Row) :
													if($Row->Column_Name == 'MSISDN')
													{
											?>
														<input type="checkbox"  class="field" name="cbField[]" value="<?php echo str_replace(' ', '', $Row->Column_Name); ?>"/>
											<?php
													}
													else
													{
											?>
														<input type="checkbox"  class="field" name="cbField[]" value="<?php echo str_replace(' ', '', $Row->Column_Name); ?>"/> <?php echo $Row->Column_Name; ?> </br>	
										<?php
													}
												endforeach;
											}
										
										?>
									</div>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Print" id="btnPrint">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>


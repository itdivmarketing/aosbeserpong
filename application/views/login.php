<!DOCTYPE HTML>
<html lang="en">
	<head profile="http://gmpg.org/xfn/11">
		<title>BINA NUSANTARA | Admission School BackEnd - Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>resources/images/favicon.ico">

		<!-- Stylesheet -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/login.css">
		<!--<link rel="stylesheet" type="text/css" href="themes/edu/css/login.css">-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/themes/school/css/login.css">
		<!--<link rel="stylesheet" type="text/css" href="themes/school/css/login.css">-->

		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
		<![endif]-->
	</head>

	<body>

		<div id="page" class="main-container">

			<section class="background">
				<div class="overlay"></div>
			</section>
			<section class="wrapper">
				<div class="login">
					<div class="login-head">
						<div class="ribbon"></div>
						<div class="logo">
							<a href="index.php" class="site-name">
								<img src="<?php echo base_url(); ?>resources/images/logo.png" alt="Logo">
							</a>
							<span class="site-description">
								People Innovation Excellence
							</span>
						</div>
					</div>
					<div class="login-body">
						<form id="frmLogin" class="custom-form" action="<?php echo site_url();?>/user_session/login" method="post">
							<p>
								<span class="custom-textbox">
									<span class="icon-wrap">
										<i class="icon icon-user"></i>
									</span>
									<input type="text" placeholder="Username" name="txtUserName">
								</span>
							</p>
							<p>
								<span class="custom-textbox">
									<span class="icon-wrap">
										<i class="icon icon-pass"></i>
									</span>
									<input type="password" placeholder="Password" name="txtPassword" >
								</span>
							</p>
							<p>
								<input type="submit" value="Login" class="button button-primary wide">
								<?php
								if($errorMessage)
								{	
									echo '<span class="error" style="text-align:center;">'.$errorMessage.'</span>';
								}
								else echo '';
								?>
							</p>
						</form>
					</div>
					<div class="login-footer">
						 <div id="disclaimer-wrap" style="text-align:center;">
						 	<p style="margin:5px; font-size:12px;">
						 		This application is compatible with modern browsers:</br> 
						 		Firefox 4+, Google Chrome, Opera 12+
						 	</p>
						 </div><!-- #disclaimer-wrap -->
					</div>
				</div>
			</section>

		</div>

		<!-- JavaScript -->
		<script src="<?php echo base_url(); ?>resources/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>resources/js/script.login.js"></script>
		<style>
			.error{
				color: red;
				display: block;
			}
		</style>
	</body>
</html>
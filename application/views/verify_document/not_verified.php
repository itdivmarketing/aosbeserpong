<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading" style="margin-bottom: 5px;">Verify Document</h2>
				<section class="body-navigation" style="margin-bottom: 50px;">
				<div class="nav-head">
					<input name="ActiveTab" id="ActiveTab" type="hidden" value="<?php if(isset($TabName)) echo  $TabName; else echo ""; ?>">
					<div class="container">
						<ul>
							<li class="current">
								<a href="<?php echo site_url().'/verify_document/not_verified'; ?>">
									<span class="label">Not Verified</span>
								</a>
							</li>
							<li>
								<a href="<?php echo site_url().'/verify_document/verified'; ?>">
									<span class="label">Already Verified</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				</section>
							<form name="frmSearch" action="<?php echo site_url().'/';?>verify_document/not_verified/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level : </label>
								<span class="custom-combobox">
									<select name="ddlYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>No Form: </label>
								<span>
									<input type="text" name="txtFormNumber" value="<?php if(isset($_GET['txtFormNumber'])) echo $_GET['txtFormNumber']; else echo "";?>" />
								</span>
							</p>
							<p>
								<label>Name </label>
								<span>
									<input type="text" name="txtStudentName" value="<?php if(isset($_GET['txtStudentName'])) echo $_GET['txtStudentName']; else echo "";?>" />
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>No Form</th>
										<th>Name</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->FormNumber;?>">
											<td style="text-align:center">
												<a class="icon icon-view btnView"></a>
											</td>
											<td><?php echo $Row->FormNumber; ?></td>
											<td><?php echo $Row->StudentName; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
				<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
				</div>
				<input type="hidden" id="file_url" value="<?php echo $this->config->item('form_return_BO_view_path').'/DocumentUpload'.'/' ?>" />
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Verify Document</h2>
			<form name="frmVerify" class="styled-content" action="<?php echo site_url().'/';?>verify_document/not_verified/save_verify_document" method="post">
				<input type="hidden" name="hfViewSchoolLevel" value=""/>
				<input type="hidden" name="hfViewFormNumber" value=""/>			
				<p>
					<label>Form No: </label>
					<input type="text" name="txtViewFormNumber" value="" readonly="readonly"/>
				</p>
				<p>
					<label>Name : </label>
					<span>
						<input type="text" name="txtViewStudentName" value="" readonly="readonly"/>
					</span>
				</p>
				<p>
					<label>SchoolLevel* : </label>
					<span>
						<input type="text" name="txtViewSchoolLevelName" value="" readonly="readonly"/>
					</span>
				</p>		
					<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView">
								<thead>
									<tr>
										<th>Document</th>
										<th>View</th>
										<th>Receive</th>
										<th>Reject</th>
										<th>Pending</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody id="TableVerify">									

								</tbody>
							</table>
						</div>
								<p>
									<label>Send Email To Parent : </label>
									<span>
										<input type="checkbox" name="cbxViewIsEmail" />
									</span>
								</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="save();">
								</p>
			</form>
		</div>
	</div>
	<div id="view-popup" style="display:none;">
			<h2 class="popup-title" id="modal-file_title">File Name</h2>
			<div class="popup-content">
				<img id="popup-image" src=""></img>
			</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>





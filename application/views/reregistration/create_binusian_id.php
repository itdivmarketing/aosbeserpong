<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Create Student ID</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>reregistration/create_binusian_id/search_binusian_id" method="get">
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Status : </label>
								<span class="custom-combobox">
									<select name="ddlStatus">
										<option value="1" <?php echo (isset($_GET['ddlStatus']) ? ($_GET['ddlStatus'] == '1' ? 'selected="selected"' : '') : 'selected="selected"' )?> >All</option>
										<option value="2" <?php echo (isset($_GET['ddlStatus']) ? ($_GET['ddlStatus'] == '2' ? 'selected="selected"' : '') : '' )?>>Paid</option>
										<option value="3" <?php echo (isset($_GET['ddlStatus']) ? ($_GET['ddlStatus'] == '3' ? 'selected="selected"' : '') : '' )?>>Unpaid</option>
									</select>
								</span>
							</p>
							<p>
								<label>Registrant ID : </label>
								<span>
									<input type="text" name="txtRegistrantID" value="<?php if(isset($_GET['txtRegistrantID'])) echo $_GET['txtRegistrantID']; else echo '';?>"/>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<form name="frmSet" action="<?php echo site_url().'/';?>reregistration/create_binusian_id/set_binusian_id" method="post">
								
								<p>
									<label>Academic Year : </label>
									<span class="custom-combobox">
										<select name="ddlAcademicYear">
											<option value="">--Please Select--</option>
											<?php foreach($AcademicYear as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAcademicYear']))
												{
													if ($_GET['ddlAcademicYear'] == $Row->AcademicYear )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->AcademicYear;?>"> 
												<?php echo $Row->AcademicYear;?>
											</option>
										<?php endforeach; ?>
										</select>
									</span>
								</p>
								<p>
									<label>Academic Semester : </label>
									<span class="custom-combobox">
										<select name="ddlAcademicSemester">
											<option value="">--Please Select--</option>
											<option value="1" <?php echo (isset($_GET['ddlAcademicSemester']) ? ($_GET['ddlAcademicSemester'] == '1' ? 'selected="selected"' : '') : 'selected="selected"' )?> >1</option>
											<option value="2" <?php echo (isset($_GET['ddlAcademicSemester']) ? ($_GET['ddlAcademicSemester'] == '2' ? 'selected="selected"' : '') : '' )?>>2</option>
										</select>
									</span>
								</p>
								<p>
									<label>Join To School : </label>
									<span>
										<input type="text" name="txtJoinSchool" class="date" readonly="readonly"/>
									</span>
								</p>
								<div class="floatThead-wrapper">
								<table id="tblRegistrant" style="margin-top:0;margin-bottom:0;">
									<thead>
										<tr>
											<th>Student ID</th>
											<th>Registrant ID</th>
											<th>Name</th>
											<th></th>
											<th>Note</th>
										</tr>
									</thead>
									<tbody id="RateContent">
										
										<?php if (isset($Registrant)) {foreach($Registrant as $Row): ?>
										<tr data-id="<?php echo $Row->RegistrantID;?> ">
											<td>
												<?php echo $Row->StudentID; ?>
												<input type="hidden" name="hfRegistrantID[]" value="<?php echo $Row->RegistrantID; ?>" />
												<input type="hidden" name="hfSchoolLevel[]" value="<?php echo $Row->SchoolLevelID; ?>" />
												<input type="hidden" name="hfDOB[]" value="<?php echo $Row->DOB; ?>" />
												<input type="hidden" name="hfTerm[]" value="<?php echo $Row->TermId; ?>" />
												<input type="hidden" name="hfName[]" value="<?php echo $Row->RegistrantName; ?>" />
												<input type="hidden" name="hfGender[]" value="<?php echo $Row->GenderID; ?>" />
											</td>
											<td><?php echo $Row->RegistrantID; ?></td>
											<td><?php echo $Row->RegistrantName; ?></td>
											<td>
												<input type="checkbox" name="cbRegistrant[]" value="<?php echo $Row->RegistrantID;?>" />
											</td>
											<td><?php echo $Row->Note; ?></td>
										</tr>
										<?php endforeach; } else {
										echo '<tr><td colspan="5">There\'s No Data</td></tr>'; }?>
									</tbody>
								</table>
							</div>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Create" id="btnSave">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>

							<!--Pop Up-->
							<!-- <div style="display:none">
								<div id="internal-popup">
										<div class="single-content popup-content" style="padding:60px">
											
											<input type="hidden" name="hfPage" value="1"/>
										
											<p>
												<label>
													<span>If you create binusian ID, you must agree with the school regulation</span></br>
												</label>
											</p>

											<p><h2 class="heading">School Regulation</h2></p>
											<div id="text">
												<?php
													foreach ($SchoolRegulation as $regulation) {
														echo $regulation->SchoolRegulationText;
													}
												?>
											</div>

											<p>
												<input type="checkbox" value="1" name="agree" id="agree"> I Agree with the School Regulation
											</p>

											<p align="center" style="clear:both;padding-top:20px;">
												<input type="submit" class="button button-primary" value="Save" id="btnCreate">
											</p>
													
										</div>

								</div>
							</div> -->

							<!--End Pop Up-->
						</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});



</script>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>
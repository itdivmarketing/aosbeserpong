<style>
#drop{
	border:2px dashed #bbb;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	padding:100px;
	text-align:center;
	font:20pt bold,"Vollkorn";color:#bbb
}
#b64data{
	width:100%;
}
</style>
<script lang="javascript" src="<?php echo base_url();?>resources/js/core/jszip.js"></script>
<script lang="javascript" src="<?php echo base_url();?>resources/js/core/xlsx.js"></script>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Entry Score</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>entrance_test/entry_score_per_ms_hs/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Schedule : </label>
								<span class="custom-combobox">
									<select name="ddlSchedule">
										<option value="">--Please Choose--</option>
										<?php foreach($Schedule as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchedule']))
												{
													if ($_GET['ddlSchedule'] == $Row->ScheduleID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->ScheduleID;?>"> 
												<?php echo $Row->ScheduleID;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
												<input type="hidden" name="hfSchedule" value="<?php echo (isset($hfSchedule) ? $hfSchedule : ''); ?>"/>
						<p>
							<label>Venue : </label>
							<span>
								<input type="text" name="txtVenue" value="<?php echo (isset($txtVenue) ? $txtVenue : ''); ?>" readonly="readonly"/>
							</span>
						</p>
						<p>
							<label>Date : </label>
							<span>
								<input type="text" name="txtDate" value="<?php echo (isset($txtDate) ? $txtDate : ''); ?>" readonly="readonly"/>
							</span>
						</p>
						<p>
							<label>Time : </label>
							<span>
								<input type="text" name="txtTime" value="<?php echo (isset($txtTime) ? $txtTime : ''); ?>" readonly="readonly"/>
							</span>
						</p>
						<br/>
						<?php if (isset($txtVenue))
						{?>
							<a id="templateGrade" target="_blank">Click here to download template excel</a>
							<br/><br/>
							<label><input type="file" name="file" id="file" style="display:none">
							<br/><br/>
							<div id="drop">Click or drop .XLSX file here to upload the score</div>
							<!-- <textarea id="b64data">... or paste a base64-encoding here</textarea> -->
							<!-- <input type="button" id="dotext" value="Click here to process the base64 text" onclick="b64it"/> -->
							<pre id="out"></pre>
						<?php 
						}?>
			</div>
		</div>
	</div>
</div>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<?php $this->load->view("entrance_test/header"); ?>
			</div>
			<div class="box-container">
				<div class="container w-640px profile-setting">
						<div class="form-section" id="setting-PreTestSubject">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="title left">Pre Test Subject</span>
							</div>
							<div  id="subject-edit-section" class="right clearfix save-section 
							<?php if(isset($StudentValidity)&& $StudentValidity==1){
										echo '';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!" id="edit-subject">Edit</a></span>
							</div>
							<div id="subject-save-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!" id="cancel-edit-subject">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="left"><a  class="button button-primary" id="btnSaveSubject"  style="margin-top:5px;" >Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-subject" action="<?php echo site_url().'/';?>entrance_test/entry_score/save_subject_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											<p>
												<label for="">Venue	:</label>
												<label name="lblSubjectVenue" id="lblSubjectVenue"  ><?php echo isset($EntryScoreData[0]->VenueName)?$EntryScoreData[0]->VenueName:''; ?></label>
											</p>
											<p>
												<label for="">Date	:</label>
												<label name="lblSubjectDate" id="lblSubjectDate"  ><?php echo isset($EntryScoreData[0]->TglAwal)?$EntryScoreData[0]->TglAwal.' - '.$EntryScoreData[0]->TglAkhir:''; ?></label>
											</p>
												<p>
												<label for="">Time	:</label>
												<label name="lblSubjectTime" id="lblSubjectTime"  ><?php echo isset($EntryScoreData[0]->JamMulai)?$EntryScoreData[0]->JamMulai.' - '.$EntryScoreData[0]->JamSelesai:''; ?></label>
											</p>
											<p>
												<label for="">Final Score</label>

												<span class="">
												<select name="ddlSubjectFinalScore" id="ddlSubjectFinalScore" >
													<option value="">--Please Choose--</option>
													<?php foreach($FinalScoreStatusList as $Row): ?>
														<option
														<?php if(isset($EntryScoreData[0]->Status))
														{
															if ($EntryScoreData[0]->Status== $Row->ID )
															echo 'selected="selected"'; 
														}
														else 
														{
															echo '';
														} ?> 
											value="<?php echo $Row->ID;?>"> 
															<?php echo $Row->Description;?> 
														</option>
													<?php endforeach; ?>
												</select>
												</span>
											</p>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
											
										</div>
									</div>
								
							</div>
							<table id="TableSubject" class="zebra bordered">
								<thead>
									<tr>
										<th>Pre Test Subject Name</th>
										<th>Score</th>
										<th>Note</th>
										<th>Result:Entrance Test</th>
										<th>Status Pass</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($EntryScoreData)){ 
									$i=0;?>
									
									<?php foreach($EntryScoreData as $row): ?>
									<tr>
									<td>
										<label name="lblSubjectName[]"><?php echo $row->PreTestSubjectName; ?></label>	
										<input type="hidden" name="hfSubjectID[]" value="<?php echo $row->PreTestSubjectID; ?>"/>		
									</td>
									<td>
										<input type="text" name="txtSubjectScore[]" class="txtSubjectScore" value="<?php echo $row->Nilai; ?>"maxlength=10></input>
										<input type="hidden" name="txtBeforeReTestSubjectScore[]" value="<?php echo $row->ResultET==4?$row->Nilai:0; ?>"/>					
									</td>
									<td>
										<input type="text" name="txtSubjectNote[]" class="txtSubjectNote" value="<?php echo trim($row->Notes); ?>"></input>		
									</td>
									<td>

										<select name="ddlSubjectResultET[]" class="ddlSubjectResultET" >
											<option value="">--Please Choose--</option>
											<?php foreach($ResultETList as $Row): ?>
												<option 
												<?php if(isset($row->ResultET))
												{
													if ($row->ResultET == $Row->ResultET )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> 
												value="<?php echo $Row->ResultET;?>"> 
													<?php echo $Row->NamaResultET;?> 
												</option>
											<?php endforeach; ?>
										</select>
										
									</td>
									<td>
										<input type="checkbox" name="cbxSubjectStatusPass[<?php echo $i;?>]" <?php echo $row->StatusLulus==1?"checked":""; ?> />		
									</td>
									</tr>
									<?php $i++; ?>
									<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
								</tfoot>
							</table>
								
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
					

					<div class="form-section" id="setting-Interview">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="title left">Interview</span>
							</div>
							<div  id="interview-edit-section" class="right  clearfix save-section 
							<?php if(isset($StudentValidity)&& $StudentValidity==1){
									if(isset($EntryScoreData[0]->VenueNameP3A)&&isset($EntryScoreData[0]->TglP3A)){
										if($EntryScoreData[0]->VenueNameP3A!=NULL&&$EntryScoreData[0]->VenueNameP3A!=""&&$EntryScoreData[0]->TglP3A!=NULL&&$EntryScoreData[0]->TglP3A!="")
											echo '';
										else
											echo 'hide';
									}
									else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!" id="edit-interview">Edit</a></span>
							</div>
							<div id="interview-save-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!" id="cancel-edit-interview">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="btnSaveInterview" style="margin-top:5px;">Save</a></span>
							</div>
						</div>
						<div class="content">
							<div class="row clearfix">
								<form  id="form-interview" action="<?php echo site_url().'/';?>entrance_test/entry_score/save_interview_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											<p>
												<label for="">Venue	:</label>
												<label name="lblSubjectVenue" id="lblSubjectVenue"  ><?php echo isset($EntryScoreData[0]->VenueNameP3A)?$EntryScoreData[0]->VenueNameP3A:''; ?></label>
											</p>
											<p>
												<label for="">Date	:</label>
												<label name="lblSubjectDate" id="lblSubjectDate"  ><?php echo isset($EntryScoreData[0]->TglP3A)?$EntryScoreData[0]->TglP3A:''; ?></label>
											</p>
												<p>
												<label for="">Time	:</label>
												<label name="lblSubjectTime" id="lblSubjectTime"  ><?php echo isset($EntryScoreData[0]->JamMulaiP3A)?$EntryScoreData[0]->JamMulaiP3A.' - '.$EntryScoreData[0]->JamSelesaiP3A:''; ?></label>
											</p>
									
											
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
											<p>
												<label for="">Score*	:</label>
												<input type="text" name="txtP3AScore" class="highlight" value="<?php echo isset($EntryScoreData[0]->NilaiP3A)?$EntryScoreData[0]->NilaiP3A:''; ?>" maxlength=10 ></input>
											</p>
											<p>
												<label for="">Notes	:</label>
												<textarea  name="txtP3ANote"  class="highlight" placeholder="" maxlength=300><?php echo isset($EntryScoreData[0]->NotesP3A)?$EntryScoreData[0]->NotesP3A:''; ?></textarea>
											</p>
											<p>
												<label for="">Result:Entrance Test*	:</label>
												<span class="">
													<select name="ddlP3AResultET">
											<option value="">--Please Choose--</option>
											<?php foreach($ResultETList as $Row): ?>
												<option 
												<?php if(isset($EntryScoreData[0]->ResultETP3A))
												{
													if ($EntryScoreData[0]->ResultETP3A == $Row->ResultET )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> 
												value="<?php echo $Row->ResultET;?>"> 
													<?php echo $Row->NamaResultET;?> 
												</option>
											<?php endforeach; ?>
										</select>
										</span>
											</p>
												<p>
												<label for="">Status Pass	:</label>
												<input type="checkbox"  name="cbxP3AStatusPass" <?php echo isset($EntryScoreData[0]->StatusLulusP3A)?($EntryScoreData[0]->StatusLulusP3A==1?"checked":""):""; ?> />	
											</p>
										</div>
									</div>
								
							</div>
							<input type="hidden" value="<?php echo isset($EntryScoreData[0]->SchoolLevelID)?$EntryScoreData[0]->SchoolLevelID:''; ?>" name="hfSchoolLevelID"/>
							<input type="hidden" value="<?php echo isset($EntryScoreData[0]->TermID)?$EntryScoreData[0]->TermID:''; ?>" name="hfTermID"/>	
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
				<div class="form-section" id="setting-UD">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="title left">UpGrade-DownGrade</span>
							</div>
							<div  id="UD-edit-section" class="right  clearfix save-section 
							<?php if(isset($StudentValidity)&& $StudentValidity==1){
										echo '';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!" id="edit-UD">Edit</a></span>
							</div>
							<div id="UD-save-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!" id="cancel-edit-UD">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a class="button button-primary" id="btnSaveUD" style="margin-top:5px;">Save</a></span>
							</div>

						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-UD" action="<?php echo site_url().'/';?>entrance_test/entry_score/save_UD_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											<p>
												<label for="">School Level*	:</label>
												<span class="">
													<select name="ddlUDSchoolLevel">
														<option value="">--Please Choose--</option>
														<?php foreach($SchoolLevel as $Row): ?>
															<option 
																<?php if(isset($EntryScoreData[0]->SchoolLevelIdDU))
																{
																	if ($EntryScoreData[0]->SchoolLevelIdDU== $Row->SchoolLevelID )
																	echo 'selected="selected"'; 
																}
																else 
																{
																	echo '';
																} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
																<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
															</option>
														<?php endforeach; ?>
													</select>
												</span>
											</p>
											<p>
												<label for="">Admission Term*	:</label>
												<span class="">
													<select name="ddlUDAdmissionTerm">
														<option value="">--Please Choose--</option>
														<?php foreach($PretestTerm as $Row): ?>
															<option 
																<?php if(isset($EntryScoreData[0]->TermIdDU))
																{
																	if ($EntryScoreData[0]->TermIdDU == $Row->TermID )
																	echo 'selected="selected"'; 
																}
																else 
																{
																	echo '';
																} ?> value="<?php echo $Row->TermID;?>"> 
																<?php echo $Row->TermID .' - '. $Row->TermName;?> 
															</option>
														<?php endforeach; ?>
													</select>
												</span>
											</p>
												<p>
												<label for="">Year Level*	:</label>
													<span class="">
													<select name="ddlUDYearLevel">
														<option value="">--Please Choose--</option>
														<?php foreach($YearLevel as $Row): ?>
															<option 
																<?php if(isset($EntryScoreData[0]->YearLevelIdDU))
																{
																	if ($EntryScoreData[0]->YearLevelIdDU == $Row->YearLevelID )
																	echo 'selected="selected"'; 
																}
																else 
																{
																	echo '';
																} ?> value="<?php echo $Row->YearLevelID;?>"> 
																<?php echo $Row->YearLevelName;?> 
															</option>
														<?php endforeach; ?>
													</select>
													</span>
											</p>
											<p>
												<label for="">Note</label>
												<textarea class="highlight" name="txtUDNote" id="txtUDNote"  maxlength="300" ><?php echo isset($EntryScoreData[0]->NotesDU)?trim($EntryScoreData[0]->NotesDU):""; ?></textarea>
											</p>
										</div>
									</div>
									<div class="column half">
										<div class="inside-right">
											
										</div>
									</div>
								
							</div>
							
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>

							<input type="hidden" value="<?php  
							if(isset($formNo)){
								if($formNo != '') echo $formNo->formno; else echo "";
							}?>" id="formNoTxt" name="formNoTxt"/>
								</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.hide {
    display: none !important;
}
.profile-setting .content {
    padding-left: 50px;
}
.profile-setting .form-section {
    margin-bottom: 40px;
}

.form-section .header{
	height: 50px;
    line-height: 50px;
    padding-bottom: 5px;
    margin-bottom: 30px;
    border-bottom: 1px solid #c9c9c9;
    text-transform: uppercase;
}
.title{
	display: inline-block;
    font-size: 18px;
    color: #666;
}

.divider-left{
	display: inline-block;
    width: 1px;
    height: 36px;
    background-color: #c9c9c9;
    margin: 7px 15px;"
}
</style>


    

  <!-- .container -->
  

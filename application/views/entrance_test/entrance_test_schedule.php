<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Entrance Test Schedule</h2>
										<form name="frmSearch" action="<?php echo site_url().'/';?>entrance_test/entrance_test_schedule/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel" onchange="resetAdmissionTerm();">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox" id="span_ddlAdmissionTermSrch">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
							<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Schedule ID</th>
										<th>Venue</th>
										<th>Start Date</th>
										<th>Start Time</th>
										<th>End Date</th>
										<th>End Time</th>
										<th>Interview Venue</th>
										<th>Interview Date</th>
										<th>Interview Start Time</th>
										<th>Interview End Time</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->ScheduleID;?> ">
											<td>
												<a class="icon icon-edit btnEdit"></a> &nbsp;
												<a class="icon icon-trash btnDelete"></a>
											</td>
											<td><?php echo $Row->ScheduleID; ?></td>
											<td><?php echo $Row->Venue; ?></td>
											<td><?php echo $Row->StartDate; ?></td>
											<td><?php echo $Row->StartTime; ?></td>
											<td><?php echo $Row->EndDate; ?></td>
											<td><?php echo $Row->EndTime; ?></td>
											<td><?php echo $Row->InterviewVenue; ?></td>
											<td><?php echo $Row->InterviewDate; ?></td>
											<td><?php echo $Row->InterviewStartTime; ?></td>
											<td><?php echo $Row->InterviewEndTime; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
						</div>
				<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>


				</div>
				<p align="center" style="clear:both;padding-top:20px;">
					<a href="#" id="show-popup-internal" class="button button-primary">Add</a>
				</p>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add Entrance Test Schedule</h2>
							<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>entrance_test/entrance_test_schedule/set_entrance_test_schedule" method="post">
								<input type="hidden" name="hfStatus" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($hfStatus) ? $hfStatus : 'insert') : 'insert') : 'insert'); ?>"/>
								<input type="hidden" name="hfEntranceTestID" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["hfEntranceTestID"]) ? $_POST["hfEntranceTestID"] : '') : '') : ''); ?>"/>
								<input type="hidden" name="hfSchoolLevel" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["hfSchoolLevel"]) ? $_POST["hfSchoolLevel"] : '') : '') : ''); ?>"/>
								<input type="hidden" name="hfAdmissionTerm" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["hfAdmissionTerm"]) ? $_POST["hfAdmissionTerm"] : '') : '') : ''); ?>"/>
								<p>
									<label>Admission ID : </label>
									<span>
										<input type="text" name="txtAdmissionID" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtAdmissionID"]) ? $_POST["txtAdmissionID"] : 'a') : 'b') : ''); ?>"  readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Admission Year : </label>
									<span> 
										<input type="text" name="txtAdmissionYear" value="<?php echo $year;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Semester : </label>
									<span>
										<input type="text" name="txtAdmissionSemester" value="<?php echo $semester;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>School Level <span class="required">*</span> : </label>
									<span class="custom-combobox" id="span_ddlSchoolLevel">
										<select name="ddlSchoolLevel">
											<option value="">--Please Choose--</option>
											<?php foreach($SchoolLevel as $Row): ?>
												<option 
													<?php echo (isset($status) ? ($status == "failed" ? (isset($_POST["ddlSchoolLevel"]) ? 
													($_POST["ddlSchoolLevel"] == $Row->SchoolLevelID ? 'selected="selected"' : '') : '') : '' ) : '');
													?> value="<?php echo $Row->SchoolLevelID;?>"> 
													<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
												</option>
											<?php endforeach; ?>
										</select>
									</span>
								</p>
								<p>
									<label>Admission Term <span class="required">*</span> : </label>
									<span class="custom-combobox" id="span_ddlAdmissionTerm">
										<select name="ddlAdmissionTerm" onchange="loadAdmissiID();">
											<option value="">--Please Choose--</option>
											<?php foreach($PretestTerm as $Row): ?>
												<option
													<?php echo (isset($status) ? ($status == "failed" ? (isset($_POST["ddlAdmissionTerm"]) ? 
													($_POST["ddlAdmissionTerm"] == $Row->TermID ? 'selected="selected"' : '') : '') : '' ) : '');
													?> value="<?php echo $Row->TermID;?>"> 
													<?php echo $Row->TermID .' - '. $Row->TermName;?> 
												</option>
											<?php endforeach; ?>
										</select>
									</span>
								</p>
								<p>
									<label>Venue <span class="required">*</span> : </label>
									<span class="custom-combobox" id="span_ddlVenue">
										<select name="ddlVenue">
											<option value="">--Please Choose--</option>
											<?php foreach($Venue as $Row): 
												if($Row->Capacity!=0){?>
												<option <?php echo (isset($status) ? ($status == "failed" ? (isset($_POST["ddlVenue"]) ? 
													($_POST["ddlVenue"] == $Row->VenueId ? 'selected="selected"' : '') : '') : '' ) : '');
													?> value="<?php echo $Row->VenueId;?>"> 
													<?php echo $Row->VenueName . ' (Capacity :'. $Row->Capacity .')';?> 
												</option>
											<?php } endforeach; ?>
										</select>
									</span>
								</p>
								<p>
									<label>Start Date  <span class="required">*</span>: </label>
									<span class="custom-datepicker">
										<input type="text" name="txtStartDate" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtStartDate"]) ? $_POST["txtStartDate"] : '') : '') : ''); ?>" class="datepicker" readonly="readonly"/>
									</span>
				
								</p>
								<p>
									<label>End Date <span class="required">*</span>: </label>
									<span class="custom-datepicker">
										<input type="text" name="txtEndDate" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtEndDate"]) ? $_POST["txtEndDate"] : '') : '') : ''); ?>"class="datepicker" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Start Time <span class="required">*</span>: </label>
									<span>
										<input type="text" name="txtStartTime" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtStartTime"]) ? $_POST["txtStartTime"] : '') : '') : ''); ?>"class="timepicker" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>End Time <span class="required">*</span> : </label>
									<span>
										<input type="text" name="txtEndTime" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtEndTime"]) ? $_POST["txtEndTime"] : '') : '') : ''); ?>"class="timepicker" readonly="readonly"/>
									</span>
								</p>
								 <p>
											<span>
												<input type="checkbox" name="cbInterview" id="expand-control" class="expand-control-toggle"
												<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["cbInterview"]) && $_POST["cbInterview"] == '1' ? 'checked' : '') : '') : ''); ?> onchange="drop_toggle();">
											</span>
											<label for="expand-control" class="inline">Interview</label>
											</p>
								<div class="toggle">
									<div class="item-wrap">
										<div class="head">
											
										</div>
										<div class="body">
											<!-- your content here -->
											<p>
										<label>Venue <span class="required">*</span>: </label>
										<span class="custom-combobox" id="span_ddlVenueInterview">
											<select name="ddlVenueInterview">
												<option value="">--Please Choose--</option>
												<?php foreach($Venue as $Row): 
													if($Row->Capacity!=0){?>
													<option <?php echo (isset($status) ? ($status == "failed" ? (isset($_POST["C"]) ? 
													($_POST["ddlVenueInterview"] == $Row->VenueId ? 'selected="selected"' : '') : '') : '' ) : '');
													?> value="<?php echo $Row->VenueId;?>"> 
														<?php echo $Row->VenueName . ' (Capacity :'. $Row->Capacity .')';?> 
													</option>
												<?php } 
													endforeach; ?>
											</select>
										</span>
									</p>
									<p>
										<label>Date <span class="required">*</span> : </label>
										<span class="custom-datepicker">
											<input type="text" name="txtDate" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtDate"]) ? $_POST["txtDate"] : '') : '') : ''); ?>" class="datepicker" readonly="readonly"/>
										</span>
									</p>
									<p>
										<label>Start Time <span class="required">*</span> : </label>
										<span>
											<input type="text" name="txtStartTimeInterview" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtStartTimeInterview"]) ? $_POST["txtStartTimeInterview"] : '') : '') : ''); ?>" class="timepicker" readonly="readonly"/>
										</span>
									</p>
									<p>
										<label>End Time <span class="required">*</span> : </label>
										<span>
											<input type="text" name="txtEndTimeInterview" value="<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtEndTimeInterview"]) ? $_POST["txtEndTimeInterview"] : '') : '') : ''); ?>" class="timepicker" readonly="readonly"/>
										</span>
									</p>
										</div>
									</div>

								</div>
								<div class="expand-control-wrap">
								
								<div name="divInterview" class="expand-control">
									
								</div>
								<div class="floatThead-wrapper">
								<table class="bordered" id="tblPreTestSubject">
									<tr>
										<th>Pre Test Subject Name</th>
										<th>Action* </th>
									</tr>
									<?php if($PretestSubject) {
										foreach($PretestSubject as $Row): ?>
										<tr>
											<td><?php echo $Row->PreTestSubjectName; ?></td>
											<td><input name="cbPreTestSubject[]" value="<?php echo $Row->PreTestSubjectId;?>" 
											<?php if(isset($_POST["cbPreTestSubject"])) {
											foreach($_POST["cbPreTestSubject"] as $Row2):
											echo (isset($status) ? (($status == "failed") ? ($Row2 == $Row->PreTestSubjectId ? 'checked="checked"' : '') : '') : '');
											endforeach;}
											?> type="checkbox" /></td>
										</tr>
									<?php endforeach;
									}
									else { echo '<tr ><td colspan="2">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
							</div>
							<div id="status-box">
								
							</div>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
									<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
								</p>
							</form>
		</div>
	</div>
	</div>
		<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
   	

	var default_tblPreTestSubject = $('#tblPreTestSubject').html();
	function resetAdmissionTerm(){
		$('#span_ddlAdmissionTermSrch .combobox-label').text("--Please Choose--");
	} 


var default_admissionID = "<?php echo (isset($status) ? (($status == "failed") ? (isset($_POST["txtAdmissionID"]) ? $_POST["txtAdmissionID"] : 'a') : 'b') : ''); ?>";

</script>
<style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</style>

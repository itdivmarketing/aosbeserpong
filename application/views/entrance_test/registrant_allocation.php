<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Registrant Allocation</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>entrance_test/registrant_allocation/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<div class="floatThead-wrapper">
				<table style="margin-top:0;margin-bottom:0;" id="tblView">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Schedule ID</th>
										<th>Venue</th>
										<th>Start Date</th>
										<th>Start Time</th>
										<th>End Date</th>
										<th>End Time</th>
										<th>Interview Venue</th>
										<th>Interview Date</th>
										<th>Interview Start Time</th>
										<th>Interview End Time</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->ScheduleID;?>" data-term="<?php echo $Row->TermID;?>" data-venue="<?php echo $Row->Venue;?>" data-capacity="<?php echo $Row->Capacity;?>">
											<td>
												<a class="icon icon-edit btnEdit"></a>
											</td>
											<td><?php echo $Row->ScheduleID; ?></td>
											<td><?php echo $Row->Venue; ?></td>
											<td><?php echo $Row->StartDate; ?></td>
											<td><?php echo $Row->StartTime; ?></td>
											<td><?php echo $Row->EndDate; ?></td>
											<td><?php echo $Row->EndTime; ?></td>
											<td><?php echo $Row->InterviewVenue; ?></td>
											<td><?php echo $Row->InterviewDate; ?></td>
											<td><?php echo $Row->InterviewStartTime; ?></td>
											<td><?php echo $Row->InterviewEndTime; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
								</div>
				<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
							
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>


				</div>
			</div>
		</div>
	</div>
</div>

<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Registrant Allocation</h2>
										<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>entrance_test/registrant_allocation/set_registrant_allocation" method="post">
								<input type="hidden" name="hfAdmissionTerm"/>
								<input type="hidden" name="hfScheduleID"/>
								<input type="hidden" name="hfVenue"/>
								<input type="hidden" name="hfCapacity"/>
								<p>
									<label>Schedule ID : </label>
									<span>
										<label name="lblScheduleID"></label>
									</span>
								</p>
								<p>
									<label>Venue : </label>
									<span>
										<label name="lblVenue"></label>
									</span>
								</p>
								<p>
									<label>Capacity : </label>
									<span>
										<label name="lblCapacity"></label>
									</span>
								</p>
								<div class="floatThead-wrapper">
									<p>
										<table id="tblRegistrant">
											<thead>
												<tr>
													<th>Registrant ID</th>
													<th>Registrant Name</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</p>
								</div>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
								</p>
				</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
	<!-- End Content Status-->	
</div>


<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<Style>
	/* Scrollable Table FloatThead */
		.floatThead-wrapper {
			max-height : 500px;
			width : 100%;
			overflow : auto;
		}
	/* End of Scrollable Table FloatThead */
</Style>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
					<div class="form-section" id="setting-personal">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Personal Information</span>
							</div>
							<div class="right clearfix edit-section 
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-personal">Edit</a></span>
							</div>
							<div class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-personal">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a id="form-submit" class="button button-primary">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="frmSave" action="<?php echo site_url().'/';?>entry/student_profile/save_student_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											<p>
												<label for="">Religion <span class="required">*</span></label>
												<select name="ddlReligion">
													<option value="">--Please Choose--</option>
													<?php foreach($Religion as $Row): ?>
														<option 
															<?php if(isset($StudentData->ReligionID))
															{
																if ($StudentData->ReligionID == $Row->ReligionID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->ReligionID;?>"> 
															<?php echo $Row->ReligionName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Subject Religion</label>
												<select name="ddlSubjectReligion">
													<option value="">--Please Choose--</option>
													<?php foreach($Religion as $Row): ?>
														<option 
															<?php if(isset($StudentData->SubjectReligionID))
															{
																if ($StudentData->SubjectReligionID == $Row->ReligionID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->ReligionID;?>"> 
															<?php echo $Row->ReligionName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>

											<p>
												<label for="">Email Address <span class="required">*</span></label>
												<input name="txtEmail" id="txtEmail" placeholder="" value="<?php if(isset($StudentData->Email)) echo $StudentData->Email; else echo "";?>"  maxlength=60 type="text">
											</p>
											<p>
												<label for="">Cell Phone No <span class="required">*</span></label>
												<input id="" name="txtCellPhone"  maxlength=50 type="text" value="<?php if(isset($StudentCellPhoneData)) echo $StudentCellPhoneData; else echo "";?>">
											</p>
											<p>
												<label for="">Domicile Type</label>
												<select name="ddlDomicile">
													<option value="">--Please Choose--</option>
													<?php foreach($DomicileType as $Row): ?>
														<option 
															<?php if(isset($StudentData->DomincileID))
															{
																if ($StudentData->DomincileID == $Row->DomincileID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->DomincileID;?>"> 
															<?php echo $Row->Description;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Current Address <span class="required">*</span></label>
												<textarea class="textarea" name="txtAddress" maxlength=256 ><?php if(isset($StudentData->Address)) echo $StudentData->Address; else echo "";?></textarea>
											</p>
											
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
											<p>
												<label for="">
													Country <span class="required">*</span>
												</label>
												<select name="ddlCountry">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($StudentData->CountryID))
															{
																if ($StudentData->CountryID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">
													City <span class="required">*</span>
												</label>
												<select name="ddlCity">
													<option value="">--Please Choose--</option>
													<?php foreach($City as $Row): ?>
														<option 
															<?php if(isset($StudentData->CityID))
															{
																if ($StudentData->CityID == $Row->CityID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CityID;?>"> 
															<?php echo $Row->CityName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">
													Region
												
												</label>
												<select name="ddlRegion">
													<option value="">--Please Choose--</option>
													<?php foreach($Region as $Row): ?>
														<option 
															<?php if(isset($StudentData->RegionID))
															{
																if ($StudentData->RegionID == $Row->IDRegion )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->IDRegion;?>"> 
															<?php echo $Row->NmRegion;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Postal Code</label>
												<input name="txtPostalCode" placeholder="" maxlength=5 type="text" value="<?php if(isset($StudentData->PostalCode)) echo $StudentData->PostalCode; else echo "";?>">
											</p>
											<p>
												<label for="">Phone</label>
												<input name="txtPhone" maxlength=50 type="text" value="<?php if(isset($StudentPhoneData)) echo $StudentPhoneData; else echo "";?>">
											</p>
											<p>
												<label for="">Staying With <span class="required">*</span></label>
												<select name="ddlStayingWith">
													<option value="">--Please Choose--</option>
													
													<?php 
													
													
														if(isset($StudentData->StayingWithID)){
														switch($StudentData->StayingWithID){
															case 1:$StudentData->StayingWithID='F';
																break;
															case 2:$StudentData->StayingWithID='M';
																break;
															case 3:$StudentData->StayingWithID='BP';
																break;
															case 4:$StudentData->StayingWithID='G';
																break;
															case 5:$StudentData->StayingWithID='O';
																break;
																
															
														
														}
													
													}
													?>
												
													<?php foreach ($StayingWith as $key => $value): ?>
														<option 
															<?php if(isset($StudentData->StayingWithID))
															{
																if ($StudentData->StayingWithID == $key )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $key;?>"> 
															<?php echo $value;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Emergency Contact <span class="required">*</span></label>
												<select name="ddlEmergencyContact">
													<option value="">--Please Choose--</option>
													<option value="F" <?php if(isset($StudentData->EmergencyStatus))
															{
																if ($StudentData->EmergencyStatus == 'F' )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> >Father</option>
													<option value="M" <?php if(isset($StudentData->EmergencyStatus))
															{
																if ($StudentData->EmergencyStatus == 'M' )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> >Mother</option>
													<option value="G" <?php if(isset($StudentData->EmergencyStatus))
															{
																if ($StudentData->EmergencyStatus == 'G' )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> >Guardian</option>
												</select>
											</p>
											
										</div>
									</div>

								</form>
								</div>
								</div>

				
				</div>
			</div>

			
		</div>
		
	</div>
</div>


  

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
			<div class="container w-640px profile-setting">

					<div class="form-section" id="setting-personal">
						<div class="header clearfix">
							<div class="left clearfix">

								<span class="title left">Re-registration</span>
							</div>
						</div>
						<div class="content">
							
								<div class="row clearfix">

								<div style="font-size:20px;text-align:center;margin-bottom:20px;"><b>School Regulation</b></div>
								
								<div id="regulation" style="overflow:scroll; margin-bottom:10px;">
									
									
									<div id="text">
										<?php
											
											foreach ($SchoolRegulation as $regulation){
												echo $regulation->SchoolRegulationText;
										
											}

											
										?>
									</div>
								</div>
							

								<?php 
									$Value="";
									if(isset($_GET['txtFormNumber']))
										$Value=$_GET['txtFormNumber']; 
									else if(isset($FormNumber))
										$Value=$FormNumber; 
								?>

								<form action="<?php echo site_url().'/';?>entry/reregistration/check_agree" method="post" class="front-editing">
									<div class="row">
										<input type="hidden" value="<?php echo $Value;?>" name="hfFormNumber"/>
										<input type="checkbox" name="agree" id="agree" value="1" <?php if($Value=="") echo "disabled"; ?>> I Agree with the School Regulation
										<input type="hidden" name="check" id="check" value="<?php echo $isAgree; ?>">
									</div>

									<input type="submit" value="Save" name="btnSave" id="btnSave" class="button button-primary" style="margin-top:20px;" <?php if($Value=="") echo "disabled"; ?>>
								</form>


								</div>
							
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
	$('#btnSave').click(function(){
		if(document.getElementById('agree').checked) {
	    	document.getElementById('agree').value=1;
		} else {
		  	document.getElementById('agree').value=0;
		}

	})

	if(document.getElementById('check').value=='1'){
			document.getElementById('agree').checked=true;
	}
	
</script>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
					<div class="form-section" id="setting-sickness">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Sickness</span>
							</div>
							<div  id="sickness-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-sickness">Edit</a></span>
							</div>
							<div id="sickness-cancel-edit-section"  class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-sickness">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a id="btnSaveSickness" class="button button-primary">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-sickness" action="<?php echo site_url().'/';?>entry/personal_development/save_sickness_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											
											<p>
												
												<label for="">Sickness</label>
												<select name="ddlSickness" id="ddlSickness">
													<option value="">--Please Choose--</option>
													<?php foreach($Sickness as $Row): ?>
														<option value="<?php echo $Row->SicknessID;?>"> 
															<?php echo $Row->SicknessName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Note</label>
												<textarea name="txtSicknessNote" id="txtSicknessNote" placeholder="" maxlength=1000 ></textarea>
											</p>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
								
										</div>
									</div>
								
							</div>
							<table class="bordered zebra" id="TableSickness" class="ListData">
								<thead>
									<tr>
										<th></th>
										<th>Sickness</th>
										<th>Note</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($SicknessData)){ ?>
									<?php foreach($SicknessData as $row): ?>
										<tr>
										<td class='IconColumn'>
										<i class='icon icon-trash hide'></i>&nbsp;<i class='icon icon-edit hide'></i>	
										</td>
										<td>
										<?php echo $row->SicknessName; ?>		
										</td>
										<td>
										<?php echo $row->Memo; ?>		
										</td>
										</tr>
									<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a  class="button button-primary" id="btnAddSickness">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
					

					<div class="form-section" id="setting-health">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">health</span>
							</div>
							<div  id="health-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-health">Edit</a></span>
							</div>
							<div id="health-cancel-edit-section"  class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-health">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a id="btnSaveHealth" class="button button-primary">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-health" action="<?php echo site_url().'/';?>entry/personal_development/save_health_and_difficulties" method="post" class="front-editing" enctype="multipart/form-data" >
									<div class="column one-half">
										<div class="inside-left">

											<p>
												<label for="">Note</label>
												<textarea name="txtProblemsNote" id="txtHealthNote" placeholder="" maxlength=1000 ><?php if(isset($HealthData->Description)) echo $HealthData->Description; else echo "" ?></textarea>
											</p>
											<p>
											<input type="file" name="file" id="HealthFile" />
											</br>
											<div>.jpg .jpeg .png .pdf .zip .rar</div></br>
											<div>6MB of Max Size</div>
											<input type="hidden" name="problem_id" id="problem_id" value="3" ></input>
											<input type="hidden" name="HasFile" id="HasFile" value="0" ></input>
											<input type="hidden" name="SavedFile" id="SavedFile" value="<?php echo !empty($HealthData->AttachReports)&&$HealthData->AttachReports==1?$HealthData->AttachReportName:""; ?>"/>
											</p>
											<p <?php  echo isset($HealthFile)&&$HealthFile!=""?"":"hidden"; ?>>
												<a id="downloadlink" style="text-decoration:none"   download="<?php echo isset($HealthFile)?$HealthFile:"" ?>" href="<?php echo isset($HealthFile)?($HealthFile!=""?$this->config->item('form_return_BO_view_path')."StudentInformation/Health/".$HealthFile:'#'):"#"; ?>"   target="_blank"><i class="icon icon-download"></i></a>
												&nbsp;&nbsp;Download File 
											</p>
											<p>
												<?php  echo isset($HealthFile)&&$HealthFile!=""?$HealthFile:""; ?>
											</p>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
								
										</div>
									</div>
								
							</div>
							
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
					
				<div class="form-section" id="setting-SchoolWithdrawal">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">SchoolWithdrawal</span>
							</div>
							<div  id="SchoolWithdrawal-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-SchoolWithdrawal">Edit</a></span>
							</div>
							<div id="SchoolWithdrawal-cancel-edit-section"  class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-SchoolWithdrawal">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a id="btnSaveSchoolWithdrawal" class="button button-primary">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-SchoolWithdrawal" action="<?php echo site_url().'/';?>entry/personal_development/save_health_and_difficulties" method="post" class="front-editing" enctype="multipart/form-data" >
									<div class="column one-half">
										<div class="inside-left">

											<p>
												<label for="">Note</label>
												<textarea name="txtProblemsNote" id="txtSchoolWithdrawalNote" placeholder="" maxlength=1000 ><?php if(isset($SchoolWithdrawalData->Description)) echo $SchoolWithdrawalData->Description; else echo "" ?></textarea>
											</p>
											<p>
											<input type="file" name="file" id="SchoolWithdrawalFile" />
											</br>
											<div>.jpg .jpeg .png .pdf .zip .rar</div></br>
											<div>6MB of Max Size</div>
											<input type="hidden" name="problem_id" id="problem_id" value="4" ></input>
											<input type="hidden" name="HasFile" id="HasFile" value="0" ></input>
											<input type="hidden" name="SavedFile" id="SavedFile" value="<?php echo !empty($SchoolWithdrawalData->AttachReports)&&$SchoolWithdrawalData->AttachReports==1?$SchoolWithdrawalData->AttachReportName:""; ?>"/>
											</p>
											<p <?php  echo isset($SchoolWithdrawalFile)&&$SchoolWithdrawalFile!=""?"":"hidden"; ?> >
												<a id="downloadlink" style="text-decoration:none"  target="_blank" download="<?php echo isset($SchoolWithdrawalFile)?$SchoolWithdrawalFile:"" ?>" href="<?php echo isset($SchoolWithdrawalFile)?($SchoolWithdrawalFile!=""? $this->config->item('form_return_BO_view_path')."StudentInformation/SchoolWithdrawal/".$SchoolWithdrawalFile:'#'):"#"; ?>"   ><i class="icon icon-download"></i></a>
												&nbsp;&nbsp; Download File 
											</p>
											<p>
												<?php  echo isset($SchoolWithdrawalFile)&&$SchoolWithdrawalFile!=""?$SchoolWithdrawalFile:""; ?>
											</p>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
								
										</div>
									</div>
								
							</div>
							
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
  

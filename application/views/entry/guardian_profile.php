<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
									<div class="form-section">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Guardian Information</span>
							</div>
							<div id="guardian-profile-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-guardian-information/" id="edit-guardian-profile">Edit</a></span>
							</div>
							<div id="guardian-profile-save-section" class="right clearfix hide save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-guardian-profile">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="btnSave">Save</a></span>
							</div>
						</div>
						<div class="content">
						<form  id="form-GuardianProfile" action="<?php echo site_url().'/';?>entry/guardian_profile/save_guardian_profile_data" method="post" class="front-editing">
							<div class="form-section" id="setting-guardian-info">
								<div class="row clearfix box-equivalent">
				
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											<input type="hidden" value="<?php  if(isset($EmergencyContact)) echo $EmergencyContact; else echo ""; ?>" name="txtEmergencyContact"/>
											<input type="hidden" value="<?php  if(isset($StayingWithID)) echo $StayingWithID; else echo ""; ?>" name="txtStayingWithID"/>
											<p>
												<label for="">Name*</label>
												<input name="txtGuardianName" id="txtGuardianName" placeholder="" value="<?php if(isset($GuardianData->ParentName)) echo $GuardianData->ParentName; else echo "";?>"  maxlength=100 type="text">
											</p>
											<p>
												<label for="">Address*</label>
												<textarea class="textarea" name="txtGuardianAddress" id="txtGuardianAddress" maxlength=256 ><?php if(isset($GuardianData->Address)) echo $GuardianData->Address; else echo "";?></textarea>
											</p>
											<p>
												<label for="">
													Country
												</label>
												<select name="ddlGuardianCountry">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($GuardianData->CountryID))
															{
																if ($GuardianData->CountryID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">
													City
												</label>
												<select name="ddlGuardianCity" id="ddlGuardianCity">
													<option value="">--Please Choose--</option>
													<?php foreach($City as $Row): ?>
														<option 
															<?php if(isset($GuardianData->CityID))
															{
																if ($GuardianData->CityID == $Row->CityID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CityID;?>"> 
															<?php echo $Row->CityName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
											<label for="">Postal Code</label>
												<input name="txtGuardianPostalCode" id="txtGuardianPostalCode" placeholder="" maxlength=5 type="text" value="<?php if(isset($GuardianData->PostalCode)) echo $GuardianData->PostalCode; else echo "";?>">
											</p>
											<p>
												<label for="">Cell Phone No*</label>
												<input id="" name="txtGuardianCellPhone" id="txtGuardianCellPhone" maxlength=50 type="text" value="<?php if(isset($GuardianCellPhoneData)) echo $GuardianCellPhoneData; else echo "";?>">
											</p>											
											<p>
												<label for="">Phone No</label>
												<input id="" name="txtGuardianPhone" id="txtGuardianPhone" maxlength=50 type="text" value="<?php if(isset($GuardianPhoneData)) echo $GuardianPhoneData; else echo "";?>">
											</p>		
											<p>
												<label for="">Fax No</label>
												<input id="" name="txtGuardianFax" id="txtGuardianFax" maxlength=50 type="text" value="<?php if(isset($GuardianFaxData)) echo $GuardianFaxData; else echo "";?>">
											</p>	
											
										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
										
											<p>
												<label for="">Email Address<span id="email-guardian-required"></span></label>
												<input name="txtGuardianEmail" id="txtGuardianEmail" placeholder="" value="<?php if(isset($GuardianData->Email)) echo $GuardianData->Email; else echo "";?>"  maxlength=60 type="text">
											</p>	
											<p>
												<label for="">
													Life Status
												</label>
												<select name="ddlGuardianLifeStatus" id="ddlGuardianLifeStatus" onchange="checkRequirementbyLifeStatus()">
													<option value="">--Please Choose--</option>
													<?php foreach($LifeStatus as $Row): ?>
														<option 
															<?php if(isset($GuardianData->LifeStatusID))
															{
																if ($GuardianData->LifeStatusID == $Row->LifeStatusID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->LifeStatusID;?>"> 
															<?php echo $Row->LifeStatusName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
												<label for="">RIP Year<span id="ripyear-guardian-required"></span></label>
												<input name="txtGuardianRIPYear" id="txtGuardianRIPYear" placeholder="" value="<?php if(isset($GuardianData->RIPYear)) echo $GuardianData->RIPYear; else echo "";?>"  maxlength=4 type="text">
											</p>	
											<p>
												<label for="">Place Of Birth</label>
												<input name="txtGuardianPOB" id="txtGuardianPOB" placeholder="" value="<?php if(isset($GuardianData->POB)) echo $GuardianData->POB; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Date Of Birth</label>
												<input name="txtGuardianDOB" id="txtGuardianDOB" readonly="true" class="guardiandatepicker" placeholder="" value="<?php if(isset($GuardianData->DOB)) echo date("d M Y", strtotime($GuardianData->DOB)); else echo "";?>"  maxlength=11 type="text">
											</p>												
											<p>
												<label for="">Religion</label>
												<select name="ddlGuardianReligion">
													<option value="">--Please Choose--</option>
													<?php foreach($Religion as $Row): ?>
														<option 
															<?php if(isset($GuardianData->ReligionID))
															{
																if ($GuardianData->ReligionID == $Row->ReligionID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->ReligionID;?>"> 
															<?php echo $Row->ReligionName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Last Education</label>
												<input name="txtGuardianEducation" id="txtGuardianEducation"  placeholder="" value="<?php if(isset($GuardianData->Education)) echo $GuardianData->Education; else echo "";?>"  maxlength=50 type="text">
											</p>	
											
										</form>
									</div>
								</div>
								
								</div>
								</div>

				</div>
			</div>
		</div>
	</div>
</div>

				


<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
										<div class="form-section" id="setting-personal">
						<div class="header clearfix">
							<div class="left clearfix">

								<span class="title left">Background School</span>
							</div>
							<div id="edit-section-bg-school" class="right clearfix edit-section  							
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-personal">Edit</a></span>
							</div>
							<div id="save-section-bg-school" class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-personal">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a id="form-submit" class="button button-primary" id="btnSaveSchool" >Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-BGSchool" action="<?php echo site_url().'/';?>entry/registrant_background/save_BG_school_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											
											<p>
												
												<label for="">Type Of Previous School</label>
												<select name="ddlTypeOfSchool" id="ddlTypeOfSchool">
													<option value="">--Please Choose--</option>
													<?php foreach($TypeOfSchool as $Row): ?>
														<option value="<?php echo $Row->TypeOfSchoolID;?>"> 
															<?php echo $Row->TypeOfSchool;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">School Name*</label>
								<input name="txtSchoolName" id="txtSchoolName" readonly="true" maxlength=10 type="text" style="float:left;width:70% !important" />
								<input type="hidden" name="hfSearchSchool" id="hfSearchSchool" />
										<a id="btnSearchSchool" class="div-search" onclick="popup_SearchSchool();">
											<i class="icon icon-search-modal"></i>
										</a>
												
					<!--
												<select name="ddlSchoolName" id="ddlSchoolName">
													<option value="">--Please Choose--</option>
													<?php /* foreach($SchoolName as $Row): ?>
														<option  value="<?php echo $Row->SchoolID;?>"> 
															<?php echo $Row->SchoolName;?> 
														</option>
													<?php endforeach; */ ?>
												</select>-->
											</p>


										
											
											
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
											<p>
												<label for="">Last Year Level*</label>
												<select name="ddlYearLevel" id="ddlYearLevel" >
													<option value="">--Please Choose--</option>
													<?php foreach($YearLevel as $Row): ?>
														<option value="<?php echo $Row->YearLevelID;?>"> 
															<?php echo $Row->YearLevelName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Starting/Closing Attended Year*</label>
												<input name="txtStartAttendedYear" id="txtStartAttendedYear" style="width:140px;float:left" id="txtStartAttendedYear" placeholder=""   maxlength=4 type="text">
												<div style="width:10px;align:center;margin-top: -7px;margin-left: 147.5px;">-</div>
												<input name="txtEndAttendedYear" id="txtEndAttendedYear" style="width:140px;margin-top: -27px;margin-left: 160px;"  id="txtEndAttendedYear" placeholder="" maxlength=4 type="text">
											</p>
												<p>
												<label for="">Begin/End of School Year*</label>
												<select name="ddlBeginSchoolYear" id="ddlBeginSchoolYear" style="width:150px;float:left" >
													<option value="">--Please Choose--</option>
													<?php for($i=1;$i<=13;$i++){ ?>
														<option value="<?php echo $i;?>" ><?php echo $i;?></option>
													<?php } ?>
												</select>
												<select name="ddlEndSchoolYear" id="ddlEndSchoolYear" style="width:150px"   >
													<option value="">--Please Choose--</option>
													<?php for($i=1;$i<=13;$i++){ ?>
														<option value="<?php echo $i;?>" ><?php echo $i;?></option>
													<?php } ?>
												</select>
											</p>
										</div>
									</div>
								
							</div>
							<table id="TableBGSchool" class="bordered zebra">
								<thead>
									<tr>
										<th></th>
										<th>School Name</th>
										<th>Last Year Level</th>
										<th>Starting Attended Year</th>
										<th>Closing Attended Year</th>
										<th>Begin</th>
										<th>End</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($BGSchoolData)){ ?>
									<?php foreach($BGSchoolData as $row): ?>
									<tr>
									<td class='IconColumn'>
									<i class='icon icon-trash'></i>&nbsp;<i class='icon icon-edit'>
									</i>
									</td>
									<td>
										<?php echo $row->SchoolName; ?>		
									</td>
									<td>
										<?php echo $row->LastYearLevel==0?"-":$row->YearLevelName; ?>		
									</td>
									<td>
										<?php echo trim($row->YearAttended)==""?"-":$row->YearAttended; ?>		
									</td>
									<td>
										<?php echo trim($row->YearWithDrawn)==""?"-":$row->YearWithDrawn; ?>		
									</td>
									<td>
										<?php echo $row->BeginningOfSchoolYear==0?"-":$row->BeginningOfSchoolYear; ?>		
									</td>
									<td>
										<?php echo $row->EndOfSchoolYear==0?"-":$row->EndOfSchoolYear; ?>		
									</td>
									</tr>

									<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a class="button button-primary" id="btnAddBGSchool">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
								
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
					

					<div class="form-section" id="setting-language">
						<div class="header clearfix">
							<div class="left clearfix">
								
								<span class="title left">Background Language</span>
							</div>
							<div id="language-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-language/" id="edit-language">Edit</a></span>
							</div>
							<div id="language-save-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-language">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a href="#" class="button button-primary" id="btnSaveBGLanguage">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-BGLanguage" action="<?php echo site_url().'/';?>entry/registrant_background/save_BG_language_data" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											
											<p>
												
												<label for="">Language*</label>
												<select name="ddlLanguage" id="ddlLanguage">
													<option value="">--Please Choose--</option>
													<?php foreach($Language as $Row): ?>
														<option value="<?php echo $Row->LanguageID;?>"> 
															<?php echo $Row->LanguageName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
										</div>
									</div>
									
								
							</div>
							<table id="TableBGLanguage" class="bordered zebra">
								<thead>
									<tr>
										<th></th>
										<th>Language</th>
										<th>First Language</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($BGLanguageData)){ ?>
									<?php foreach($BGLanguageData as $row): ?>
									<tr>
									<td class='IconColumn'>
									<i class='icon icon-trash hide'></i>&nbsp;<i class='icon icon-edit hide'>
									</i>
									</td>
									<td>
										<?php echo $row->LanguageName; ?>		
									</td>
									<td class='CheckBoxColumn'>
									<input type="checkbox" id="cbxSame" <?php echo $row->FirstLanguage==1?'checked':''; ?>  class='cbxSame' />
									</td>
									</tr>

									<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a class="button button-primary" id="btnAddBGLanguage">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
							
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


    

  <!-- .container -->
<div style="display: none;">
	<div id="internal-popup-school" >
		<div class="single-content popup-content" style="padding:60px">
						<div id="body" class="styled-content">
						<div class="inside">
							<div class="row">
						<input type="hidden" name="hfPage" value="1" />
					
						<p>
							<label>
								<span>School Name</span></br>
								<span><i>Nama Sekolah</i></span>
							</label>
						</p>
						<p>
							<input type="text" class="text" id="txtSearchSchoolName" ></input>
						</p>
						<p>
							<input type="button" value="SEARCH" id="btnSearch" class="button button-primary" style="align:center"/ onclick="Search()">
						</p>
						</div>
						</div>
						</div>
					
				<table id="TableSearchSchool" class="ListData bordered zebra">
					<thead>
						<tr>
							<th></th>
							<th>SchoolID</th>
							<th>School Name</th>
						</tr>
						</thead>
						
						<tbody>
						
						
						
						</tbody>
							</table>
							
							
					
						
						<div id="paging" style="display:none">
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev disable"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
						</div>
							<?php
									if(isset($data['pagination']))
									echo "<script>
											Show_Pagination({
												'Selector' : '#paging',
												'TotalPage' : ".$data['pagination']['last'].",
												'CurrentPage' : ".$data['pagination']['current'].",
												'PreviousPage' : ".$data['pagination']['previous'].",
												'NextPage' : ".$data['pagination']['next'].",
												'InfoPage' : '".$data['pagination']['info']."'
												});
										</script>";
							?>
					</div><!-- #internal-popup -->
					</div>
</div>
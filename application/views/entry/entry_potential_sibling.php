<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Update Potential Sibling Student ID</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>entry/entry_potential_sibling/search" method="get">
							<p>
								<label>Name : </label>
								<span>
									<input type="text" id="txtSiblingNameID" maxlength="8000" name="txtSiblingName" value="<?php if(isset($_GET['txtSiblingName']))echo $_GET['txtSiblingName']; ?>"/>
								</span>
							</p>
							<p>
								<label>Date of Birth : </label>
								<span>
									<input type="text" id="txtDOBID" name="txtDOB" class="datepicker" value="<?php if(isset($_GET['txtDOB']))echo $_GET['txtDOB']; ?>" readonly="true" />
								</span>
							</p>
							<p>
								<label>Do you want filter with data that already have StudentID? </label>
								<span>
									<input type="radio" name="rbFilterSibling" value="0" <?php if(isset($_GET['rbFilterSibling']) && $_GET['rbFilterSibling']==0) echo 'checked';?>>All
									<input type="radio" name="rbFilterSibling" value="1" <?php if(isset($_GET['rbFilterSibling']) && $_GET['rbFilterSibling']==1) echo 'checked';?>>Yes
									<input type="radio" name="rbFilterSibling" value="2" <?php if(isset($_GET['rbFilterSibling']) && $_GET['rbFilterSibling']==2) echo 'checked';?>>No
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
							</p>
						</form>
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th>Potential Sibling StudentID</th>
										<th>Potential Sibling ID</th>
										<th>Potential Sibling Name</th>
										<th>Date Of Birth</th>
										<th>RegistrationID/StudentID</th>
										<th>Sibling Name</th>
									</tr>
								</thead>
								<tbody id="RateContent">
								<form id="form_entry">
									<input type="hidden" id="counterFlag" value="<?php if(isset($result)) echo count($result); ?>"
									<?php 
										$flag = 0;
										if(isset($result) && sizeof($result) > 0) {
										foreach($result as $Row): 
										$flag ++;
										?>

										<tr data-id="<?php echo $Row->RegistrantID;?> ">
											
											<input name="RegistrantID[<?php echo $flag?>]" type="hidden" value='<?php echo $Row->RegistrantID;?>'>
											<input name="PotentialSiblingID[<?php echo $flag;?>]" type="hidden" value='<?php echo $Row->PotentialSiblingID;?>'>
											
											
											
											<td><input onkeypress="return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));" name="PotentialSiblingStudentID[<?php echo $flag?>]" type="text" maxlength="10" id="txtInputPotentialSibling<?php echo $flag ?>" value='<?php echo ($Row->SiblingStudentID == '' ? '' : $Row->SiblingStudentID); ?>'></td>					
											
											<td><?php echo ($Row->PotentialSiblingID == '' ? '' : $Row->PotentialSiblingID); ?></td>
											<td><?php echo ($Row->PotentialSiblingName == '' ? '' : $Row->PotentialSiblingName); ?></td>
											<td><?php echo ($Row->Birthday == '' ? '' : $Row->Birthday); ?></td>
											<td><?php echo ($Row->RegistrantID == '' ? '' : $Row->RegistrantID); ?></td>
											<td><?php echo ($Row->SiblingName == '' ? '' : $Row->SiblingName); ?></td>
											
										</tr>
									<?php endforeach; 
									} else { 
										echo '<tr><td colspan="8">There\'s no Data</td></tr>';
									}
									?>
									
								</tbody>
							</table>
						</div>
						
						<p align="center" style="clear:both;padding-top:20px;">
							<input type="submit" class="button button-primary" id="btnSave" value="Save">
						</p>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>

<style>
.floatThead-wrapper {
    max-height: 500px;
    width: 100%;
    overflow: auto;
}
</style>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<div class="single-content popup-content" id="target-id">
					<h2 class="heading">Form Sales</h2>
					<div class="inside">
						<form name="frmSearch" action="<?php echo site_url().'/';?>entry/form_sales/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level : </label>
								<span class="custom-combobox">
									<select name="ddlYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>No Form: </label>
								<span>
									<input type="text" maxlength=13 name="txtFormNumber" value="<?php if(isset($_GET['txtFormNumber'])) echo $_GET['txtFormNumber']; else echo "";?>" />
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
					</div><!-- end of inside-->
					<form name="frmReceipt" action="<?php echo site_url().'/';?>entry/form_sales/get_printable_view" method="get"  target="_blank">
						<input type="hidden" name="hfFormNumber" value=""/>
					</form>
					<div class="inside">
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Receipt ID</th>
										<th>Form Number</th>
										<th>Receipt Name</th>

									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->FormNumber;?>">
											<td style="text-align:center">
												<a class="icon icon-view btnView"></a>
												&nbsp;
												<a class="icon icon-download btnPrint"></a>
											</td>
											<td><?php echo $Row->ReceiptID; ?></td>
											<td><?php echo $Row->FormNumber; ?></td>
											<td><?php echo $Row->RecepientName; ?></td>

										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
						</div>
						
						<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
							<div class="pagination">
								<span class="page-display"></span>
								<span class="page-number pages">
								</span>
								<span class="page-button">
									<a href="#" class="prev"></a>
									<a href="#" class="next"></a>
								</span>
							</div>

							<?php
							if($data['pagination'])
								echo "<script>
							Show_Pagination({
								'Selector' : '#paging',
								'TotalPage' : ".$data['pagination']['last'].",
								'CurrentPage' : ".$data['pagination']['current'].",
								'PreviousPage' : ".$data['pagination']['previous'].",
								'NextPage' : ".$data['pagination']['next'].",
								'InfoPage' : '".$data['pagination']['info']."'
							});
							</script>";
						?>
						</div>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="button" class="button button-primary" value="Add" id="btnAdd" onClick="Popup_add();">
							</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display:none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<!-- <h2 class="">Add Form Sales</h2> -->
			<h2 class="heading" id="popup-title">Add Form Sales</h2>
			<form name="frmAdd" class="styled-content" target="_blank" action="<?php echo site_url().'/';?>entry/form_sales/save_form_sold" method="post" onsubmit="setTimeout(function () { window.location.reload(); }, 10)">
								<input type="hidden" name="hfStatus" />
								<input type="hidden" name="hfAdmissionID"/>
								<input type="hidden" name="hfScheduleID"/>
								<input type="hidden" name="hfVenue"/>
								<input type="hidden" name="hfViewAdmissionSemester"/>
								
								<p>
									<label>Admission ID* : </label>
									<input type="text" name="txtViewAdmissionID" value="" readonly="readonly" />
								</p>
								<p>
									<label>Admission Year* : </label>
									<span>
										<input type="text" name="txtViewAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Semester* : </label>
									<span>
										<input type="text" name="txtViewAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
								<label>School Level* : </label>
								<span class="custom-combobox" id="ddlViewSchoolLevel">
									<select name="ddlViewSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 
												 value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>No Form* : </label>
								<span>
									<input type="text" name="txtViewFormNumber" value="" maxlength=13/>
								</span>
								<input type="button" id="btnCheck" class="button button-primary" value="check" onclick="check();"></input>
							</p>
							<p>
								<label>Admission Term* : </label>
								<span class="custom-combobox" id="ddlViewAdmissionTerm">
									<select name="ddlViewAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												 value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level* : </label>
								<span class="custom-combobox" id="ddlViewYearLevel">
									<select name="ddlViewYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
											 value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>

							<p>
								<label>Payment Date* : </label>
								<span class="custom-datepicker">
									<input type="text" class="datetimepicker" name="txtViewPaymentDate" readonly="readonly" value="" />
									<span class="icon-area"></span>
								</span>
							</p>
														<p>
								<label>Receipt Name* : </label>
								<span>
									<input type="text" name="txtViewReceiptName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Paid By* : </label>
								<span>
									<input type="text" name="txtViewPaidBy" maxlength=30 value="" />
								</span>
							</p>
							<p>
								<label>Officer Name*: </label>
								<span>
									<input type="text" name="txtViewOfficerName" maxlength=15 value="" />
								</span>
							</p>
							<p>
								<label>Student Name* : </label>
								<span>
									<input type="text" name="txtViewStudentName" value="" maxlength=35 />
								</span>
							</p>
							<p>
								<label>Phone Number to be Contacted* : </label>
								<span>
									<input type="text" name="txtViewPhoneNumber" class="numeric" value="" maxlength=50   />
								</span>
							</p>
							<p>
								<label>Amount* : </label>
								<span>
									<input type="text" name="txtViewAmount" value="" readonly="true" />
									<input type="hidden" name="hfViewAmount" value="" readonly="true" />
								</span>
							</p>
							<p>
								<label>Amount Paid* : </label>
								<span>
									<input type="text" name="txtViewAmountPaid" maxlength="12" class="numeric" value="" readonly="true"/>
								</span>
							</p>
							<p>
								<label>Payment Method* : </label>
								<span class="custom-combobox" id="ddlViewPaymentMethod">
									<select name="ddlViewPaymentMethod">
										<option value="">--Please Choose--</option>
										<?php foreach($PaymentMethod as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlPaymentMethod']))
												{
													if ($_GET['ddlPaymentMethod'] == $Row->PaymentMethodID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->PaymentMethodID;?>"> 
												<?php echo $Row->PaymentMethodName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Email* : </label>
								<span>
									<input type="text" name="txtViewEmail" class="email" value="" />
								</span>
							</p>
							<p>
								<label>Password* : </label>
								<span>
									<input type="text" name="txtViewPassword" value="" readonly="readonly" />
								</span>
								<input type="button" id="btnGenerate" class="button button-primary" value="GENERATE" onclick="generate();"></input>
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save & Print" id="btnSave" onclick="validate()">
								</p>
							</form>
		</div>
	</div>
	<div id="dialog-content-status" class="popup-content-dialog">
		<form>
			<p id="message"><!--Message Content--></p>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function (){
		<?php
		if(isset($message)){
			if(!($message==null  || $message=="")){
				?>
				$.fancybox({
					href: "#dialog-content-status",
					minWidth    : 400,
					fitToView   : true,
					closeClick  : false,
					afterShow	: function(){
						$('#message').html("<?php echo $message?>");
					}
				});
				<?php
			}
		}
		?>
	});
</script>
<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
					<div class="form-section" id="setting-learning_note">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Learning Note</span>
							</div>
							<div  id="learning_note-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-learning_note">Edit</a></span>
							</div>
							<div id="learning_note-cancel-edit-section"  class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-learning_note">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a id="btnSaveLearningNote" class="button button-primary">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-learning_note" action="<?php echo site_url().'/';?>entry/learning_note/save_learning_note_data" method="post" class="front-editing" enctype="multipart/form-data" >
									<div class="column one-half">
										<div class="inside-left">
											<p>
												<label for="">Area*</label>
												<select name="ddlArea" id="ddlArea">
													<option value="">--Please Choose--</option>
													<?php foreach($Area as $Row): ?>
														<option value="<?php echo $Row->ProblemID;?>"> 
															<?php echo $Row->ProblemName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Description</label>
												<textarea name="txtNote" id="txtNote" placeholder="" maxlength=1000 ></textarea>
											</p>
											<p>
											<span class="file-container">
													<input type="file" name="file" id="file" />
											</span>
											</br></br>
											<div>.jpg .jpeg .png .pdf .zip .rar</div></br>
											<div>6MB of Max Size</div>
											</p>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
								
										</div>
									</div>
								
							</div>
							<table class="bordered zebra"id="TableLearningNote" class="ListData">
								<thead>
									<tr>
										<th></th>
										<th>Area</th>
										<th>Description</th>
										<th>Attachment</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($LearningNoteData)){ ?>
										<?php foreach($LearningNoteData as $row): ?>
											<tr>
											<td class="IconColumn">
											<i class="icon icon-trash hide"></i>&nbsp;<i class="icon icon-edit hide"></i>	
											</td>
											<td>
											<?php echo $row->ProblemName; ?>		
											</td>
											<td>
											<?php echo $row->Description; ?>		
											</td>
										
											<td style="text-align:center">
											<?php if ($row->AttachReports!=0) { ?>
												<a style="text-decoration:none" href="<?php echo isset($row->AttachReportName)?($row->AttachReportName!=""? $this->config->item('form_return_BO_view_path')."StudentInformation/LearningNote/".$row->AttachReportName:'#'):"#"; ?>" target="_blank" download="" ><i class="icon icon-download"></i></a>
											<?php }else{ ?>
												No Attachment
											<?php } ?>
											</td>
											
											</tr>
										<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a  class="button button-primary" id="btnAddLearningNote">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

  

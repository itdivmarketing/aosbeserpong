<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
					<div class="form-section">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Family Information</span>
							</div>
							<div id="parent-profile-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-father-information/" id="edit-parent-profile">Edit</a></span>
							</div>
							<div id="parent-profile-save-section" class="right clearfix hide save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-parent-profile">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="btnProfileSave">Save</a></span>
							</div>
						</div>
						<div class="content">
						<form  id="form-ParentProfile" action="<?php echo site_url().'/';?>entry/parent_profile/save_parent_profile_data" method="post" class="front-editing">
							<div class="form-section" id="setting-father-info">
								<div class="header clearfix">
									<div class="left clearfix">
										<span class="title left">Father</span>
										<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
									</div>

								</div>
								<div class="row clearfix box-equivalent">
				
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											
											<p>
												<label for="">Name*</label>
												<input name="txtFatherName" id="txtFatherName" placeholder="" value="<?php if(isset($FatherData->ParentName)) echo $FatherData->ParentName; else echo "";?>"  maxlength=100 type="text">
											</p>
											<p>
												<label for="">Address*</label>
												<textarea class="textarea" name="txtFatherAddress" id="txtFatherAddress" maxlength=256 ><?php if(isset($FatherData->Address)) echo $FatherData->Address; else echo "";?></textarea>
											</p>
											<p>
												<label for="">
													Country*
												</label>
												<select name="ddlFatherCountry" id="ddlFatherCountry">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($FatherData->CountryID))
															{
																if ($FatherData->CountryID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">
													City*
												</label>
												<select name="ddlFatherCity" id="ddlFatherCity">
													<option value="">--Please Choose--</option>
													<?php foreach($FatherCity as $Row): ?>
														<option 
															<?php if(isset($FatherData->CityID))
															{
																if ($FatherData->CityID == $Row->CityID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CityID;?>"> 
															<?php echo $Row->CityName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
											<label for="">Postal Code</label>
												<input name="txtFatherPostalCode" id="txtFatherPostalCode" placeholder="" maxlength=5 type="text" value="<?php if(isset($FatherData->PostalCode)) echo $FatherData->PostalCode; else echo "";?>">
											</p>
											<!--<p>
												<label for="">Cell Phone No*</label>
												<input id="" name="txtFatherCellPhone" id="txtFatherCellPhone" maxlength=50 type="text" value="<?php if(isset($FatherCellPhoneData)) echo $FatherCellPhoneData; else echo "";?>">
											</p>											
											<p>
												<label for="">Phone No</label>
												<input id="" name="txtFatherPhone" id="txtFatherPhone" maxlength=50 type="text" value="<?php if(isset($FatherPhoneData)) echo $FatherPhoneData; else echo "";?>">
											</p>	-->		
											<p>
												<label for="">Email Address<span id="email-father-required"></span></label>
												<input name="txtFatherEmail" id="txtFatherEmail" placeholder="" value="<?php if(isset($FatherData->Email)) echo $FatherData->Email; else echo "";?>"  maxlength=60 type="text">
											</p>									
											<p>
												<label for="">Both Parent Live Together</label>
												<input type="checkbox" id="cbxLiveTogether" />
											</p>
										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<!--<p>
												<label for="">Fax No</label>
												<input id="" name="txtFatherFax" id="txtFatherFax" maxlength=50 type="text" value="<?php if(isset($FatherFaxData)) echo $FatherFaxData; else echo "";?>">
											</p>	-->
											<p>
												<label for="">
													Life Status
												</label>
												<select name="ddlFatherLifeStatus" id="ddlFatherLifeStatus" onchange="checkRequirementbyLifeStatus()">
													<option value="">--Please Choose--</option>
													<?php foreach($LifeStatus as $Row): ?>
														<option 
															<?php if(isset($FatherData->LifeStatusID))
															{
																if ($FatherData->LifeStatusID == $Row->LifeStatusID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->LifeStatusID;?>"> 
															<?php echo $Row->LifeStatusName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
												<label for="">RIP Year<span id="ripyear-father-required"></span></label>
												<input name="txtFatherRIPYear" id="txtFatherRIPYear" placeholder="" value="<?php if(isset($FatherData->RIPYear)) echo $FatherData->RIPYear; else echo "";?>"  maxlength=4 type="text">
											</p>	
											
											
											
											<p>
												<label for="">Place Of Birth</label>
												<input name="txtFatherPOB" id="txtFatherPOB" placeholder="" value="<?php if(isset($FatherData->POB)) echo $FatherData->POB; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Date Of Birth</label>
												<input name="txtFatherDOB" id="txtFatherDOB" readonly="true" class="parentdatepicker" placeholder="" value="<?php if(isset($FatherData->DOB)) echo date("d M Y", strtotime($FatherData->DOB)); else echo "";?>"  maxlength=11 type="text">
											</p>												
											<p>
												<label for="">Religion*</label>
												<select name="ddlFatherReligion">
													<option value="">--Please Choose--</option>
													<?php foreach($Religion as $Row): ?>
														<option 
															<?php if(isset($FatherData->ReligionID))
															{
																if ($FatherData->ReligionID == $Row->ReligionID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->ReligionID;?>"> 
															<?php echo $Row->ReligionName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Last Education</label>
												<input name="txtFatherEducation" id="txtFatherEducation"  placeholder="" value="<?php if(isset($FatherData->Education)) echo $FatherData->Education; else echo "";?>"  maxlength=50 type="text">
											</p>	
									</div>
								</div>
								
								</div>
							</div>
							<div class="form-section" id="setting-mother-info">
								<div class="header clearfix">
									<div class="left clearfix">
										<span class="title left">Mother</span>
										<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
									</div>
								</div>
								<div class="row clearfix box-equivalent">

									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											
											<p>
												<label for="">Name*</label>
												<input name="txtMotherName" id="txtMotherName" placeholder="" value="<?php if(isset($MotherData->ParentName)) echo $MotherData->ParentName; else echo "";?>"  maxlength=100 type="text">
											</p>
											<p>
												<label for="">Address*</label>
												<textarea class="textarea" name="txtMotherAddress" id="txtMotherAddress" maxlength=256 ><?php if(isset($MotherData->Address)) echo $MotherData->Address; else echo "";?></textarea>
											</p>
											<p>
												<label for="">
													Country*
												</label>
												<select name="ddlMotherCountry" id="ddlMotherCountry">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($MotherData->CountryID))
															{
																if ($MotherData->CountryID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">
													City*
												</label>
												<select name="ddlMotherCity" id="ddlMotherCity">
													<option value="">--Please Choose--</option>
													<?php foreach($MotherCity as $Row): ?>
														<option 
															<?php if(isset($MotherData->CityID))
															{
																if ($MotherData->CityID == $Row->CityID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CityID;?>"> 
															<?php echo $Row->CityName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
											<label for="">Postal Code</label>
												<input name="txtMotherPostalCode" id="txtMotherPostalCode" placeholder="" maxlength=5 type="text" value="<?php if(isset($MotherData->PostalCode)) echo $MotherData->PostalCode; else echo "";?>">
											</p>
											<!--<p>
												<label for="">Cell Phone No*</label>
												<input id="" name="txtMotherCellPhone" id="txtMotherCellPhone" maxlength=50 type="text" value="<?php if(isset($MotherCellPhoneData)) echo $MotherCellPhoneData; else echo "";?>">
											</p>										
											<p>
												<label for="">Phone No</label>
												<input id="" name="txtMotherPhone" id="txtMotherPhone" maxlength=50 type="text" value="<?php if(isset($MotherPhoneData)) echo $MotherPhoneData; else echo "";?>">
											</p>		-->											
											<p>
												<label for="">Email Address<span id="email-mother-required"></span></label>
												<input name="txtMotherEmail" id="txtMotherEmail" placeholder="" value="<?php if(isset($MotherData->Email)) echo $MotherData->Email; else echo "";?>"  maxlength=60 type="text">
											</p>
										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<!--<p>
												<label for="">Fax No</label>
												<input id="" name="txtMotherFax" id="txtMotherFax" maxlength=50 type="text" value="<?php if(isset($MotherFaxData)) echo $MotherFaxData; else echo "";?>">
											</p>	-->

												
											<p>
												<label for="">
													Life Status
												</label>
												<select name="ddlMotherLifeStatus" id="ddlMotherLifeStatus" onchange="checkRequirementbyLifeStatus()">
													<option value="">--Please Choose--</option>
													<?php foreach($LifeStatus as $Row): ?>
														<option 
															<?php if(isset($MotherData->LifeStatusID))
															{
																if ($MotherData->LifeStatusID == $Row->LifeStatusID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->LifeStatusID;?>"> 
															<?php echo $Row->LifeStatusName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
												<label for="">RIP Year<span id="ripyear-mother-required"></span></label>
												<input name="txtMotherRIPYear" id="txtMotherRIPYear" placeholder="" value="<?php if(isset($MotherData->RIPYear)) echo $MotherData->RIPYear; else echo "";?>"  maxlength=4 type="text">
											</p>	
											<p>
												<label for="">Place Of Birth</label>
												<input name="txtMotherPOB" id="txtMotherPOB" placeholder="" value="<?php if(isset($MotherData->POB)) echo $MotherData->POB; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Date Of Birth</label>
												<input name="txtMotherDOB" id="txtMotherDOB" readonly="true" class="parentdatepicker" placeholder="" value="<?php if(isset($MotherData->DOB)) echo date("d M Y", strtotime($MotherData->DOB)); else echo "";?>"  maxlength=11 type="text">
											</p>												
											<p>
												<label for="">Religion*</label>
												<select name="ddlMotherReligion">
													<option value="">--Please Choose--</option>
													<?php foreach($Religion as $Row): ?>
														<option 
															<?php if(isset($MotherData->ReligionID))
															{
																if ($MotherData->ReligionID == $Row->ReligionID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->ReligionID;?>"> 
															<?php echo $Row->ReligionName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Last Education</label>
												<input name="txtMotherEducation" id="txtMotherEducation"  placeholder="" value="<?php if(isset($MotherData->Education)) echo $MotherData->Education; else echo "";?>"  maxlength=50 type="text">
											</p>	
									</div>
								</div>		
							</form>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-setting">
		<div class="inside">
			<div class="form-section">
				<div class="header clearfix">
					<div class="left clearfix">
						<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
						<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
						<span class="title left">Parent Phone</span>
					</div>
					<div id="parent-phone-edit-section" class="right clearfix edit-section  
					<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
						if( isset($StudentStatus)&&$StudentStatus!=1){
								echo '';
						}
						else
								echo 'hide';
					}
					else{
						echo 'hide';
					} ?>
					">
						<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
						<span class="link left"><a href="#!/edit-father-information/" id="edit-parent-phone">Edit</a></span>
					</div>
					<div id="parent-phone-save-section" class="right clearfix hide save-section">
						<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-parent-phone">Cancel</a></span>
						<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
						<span class="action left"><a  class="button button-primary" id="btnPhoneSave">Save</a></span>
					</div>
				</div>
				<div class="content">
					<form  id="form-ParentPhone" action="<?php echo site_url().'/';?>entry/parent_profile/save_parent_phone_data" method="post" class="front-editing">
						<div class="form-section" id="setting-father-phone">
							<div class="header clearfix">
								<div class="left clearfix">
									<span class="title left">Father *</span>
									<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								</div>
							</div>
							<div class="row clearfix box-equivalent">
								<div class="column one-half">
									<div class="inside-left">
										<p>
											<label for="">Phone Type *</label>
											<select name="ddlFatherPhoneType" id="ddlFatherPhoneType">
												<option value="">--Please Choose--</option>
												<option value="F">Fax</option>
												<option value="M">Mobile Phone</option>
												<option value="P">Residence Phone</option>
											</select>
										</p>
										


									</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
										<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
										<p>
											<label for="">Phone *</label>
											<input type="text" id="txtFatherPhone" maxlength="50" class="numeric"/>
										</p>
										<p>
											<input type="button" name="btnAddFatherPhone" class="button button-primary" value="ADD" />
										</p>
									</div>
								</div>
								
								<table id="tblFatherPhone">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>Phone Type</th>
										<th>Phone Number</th>
									</tr>
									</thead>
									<?php if(isset($FatherPhone)) {
										foreach($FatherPhone as $Row): ?>
										<tr data-type="<?php echo $Row->PhoneType;?>" data-phone="<?php echo $Row->PhoneNumber;?>">
											<td>
												<!--<a class="icon icon-edit btnEdit">Edit</a>-->
												<a class="icon icon-trash btnDelete"></a>
											</td>
											<td><input type="hidden" name="hfFatherPhoneType[]" value="<?php echo $Row->PhoneTypeName;?>" /> <?php echo $Row->PhoneTypeName;?> </td>
											<td><input type="hidden" name="hfFatherPhone[]" value="<?php echo $Row->PhoneNumber;?>" /> <?php echo $Row->PhoneNumber;?></td>
										</tr>
									<?php endforeach; }?>
								</table>
							</div>
						</div>
						<div class="form-section" id="setting-mother-phone">
							<div class="header clearfix">
								<div class="left clearfix">
									<span class="title left">Mother *</span>
									<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								</div>
							</div>
							<div class="row clearfix box-equivalent">
								<div class="column one-half">
									<div class="inside-left">
										<p>
											<label for="">Phone Type *</label>
											<select name="ddlMotherPhoneType" id="ddlMotherPhoneType" style="width:100%">
												<option value="">--Please Choose--</option>
												<option value="F">Fax</option>
												<option value="M">Mobile Phone</option>
												<option value="P">Residence Phone</option>
											</select>
										</p>
									</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
										<p>
											<label for="">Phone *</label>
											<input type="text" id="txtMotherPhone" maxlength="50" class="numeric" style="width:100%"/>
										</p>
										<p>
											<input type="button" name="btnAddMotherPhone" class="button button-primary" value="ADD" />
										</p>
									</div>
								</div>
								<table id="tblMotherPhone">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>Phone Type</th>
										<th>Phone Number</th>
									</tr>
									</thead>
									<?php if(isset($MotherPhone)) {
										foreach($MotherPhone as $Row): ?>
										<tr data-type="<?php echo $Row->PhoneType;?>" data-phone="<?php echo $Row->PhoneNumber;?>">
											<td>
												<!--<a class="icon icon-edit btnEdit">Edit</a>-->
												<a class="icon icon-trash btnDelete"></a>
											</td>
											<td><input type="hidden" name="hfMotherPhoneType[]" value="<?php echo $Row->PhoneTypeName;?>" /> <?php echo $Row->PhoneTypeName;?> </td>
											<td><input type="hidden" name="hfMotherPhone[]" value="<?php echo $Row->PhoneNumber;?>" /> <?php echo $Row->PhoneNumber;?></td>
										</tr>
									<?php endforeach; }?>
								</table>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!--PARENT EMPLOYMENT AREA-->
	<div class="profile-setting">
			
				<div class="inside">
				<div class="form-section">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Parent Employment</span>
							</div>
							<div id="parent-employment-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-father-information/" id="edit-parent-employment">Edit</a></span>
							</div>
							<div id="parent-employment-save-section" class="right clearfix hide save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-parent-employment">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="btnOfficeSave">Save</a></span>
							</div>
						</div>
						<div class="content">
						<form  id="form-ParentEmployment" action="<?php echo site_url().'/';?>entry/parent_profile/save_parent_employment_data" method="post" class="front-editing">
							<div class="form-section" id="setting-father-info">
								<div class="header clearfix">
									<div class="left clearfix">
										<span class="title left">Father</span>
										<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
									</div>

								</div>
								<div class="row clearfix box-equivalent">
				
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											
											<p>
												<label for="">Company</label>
												<input name="txtFatherCompany" id="txtFatherCompany" placeholder="" value="<?php if(isset($FatherData->Company)) echo $FatherData->Company; else echo "";?>"  maxlength=150 type="text">
											</p>
											<p>
												<label for="">Job Title</label>
												<input name="txtFatherJobTitle" id="txtFatherJobTitle" placeholder="" value="<?php if(isset($FatherData->JobTitle)) echo $FatherData->JobTitle; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Office Phone Number</label>
												<input name="txtFatherOfficePhone" id="txtFatherOfficePhone" placeholder="" value="<?php if(isset($FatherData->ParentName)) echo $FatherData->OfficePhone; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Office Fax Number</label>
												<input name="txtFatherOfficeFax" id="txtFatherOfficeFax" placeholder="" value="<?php if(isset($FatherData->ParentName)) echo $FatherData->OfficeFax; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Office Address</label>
												<textarea class="textarea" name="txtFatherOfficeAddress" id="txtFatherOfficeAddress" maxlength=256 ><?php if(isset($FatherData->OfficeAddress)) echo $FatherData->OfficeAddress; else echo "";?></textarea>
											</p>
											<p>
												<label for="">Both Parent Work At The Same Company</label>
												<input type="checkbox" id="cbxWorkTogether" />
											</p>
										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<p>
												<label for="">
													Country
												</label>
												<select name="ddlFatherOfficeCountry" id="ddlFatherOfficeCountry">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($FatherData->OfficeCountryID))
															{
																if ($FatherData->OfficeCountryID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">
													City
												</label>
												<select name="ddlFatherOfficeCity" id="ddlFatherOfficeCity">
													<option value="">--Please Choose--</option>
													<?php foreach($FatherOfficeCity as $Row): ?>
														<option 
															<?php if(isset($FatherData->OfficeCityID))
															{
																if ($FatherData->OfficeCityID == $Row->CityID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CityID;?>"> 
															<?php echo $Row->CityName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
												<label for="">Postal Code</label>
												<input name="txtFatherOfficePostalCode" id="txtFatherOfficePostalCode" placeholder="" value="<?php if(isset($FatherData->OfficePostalCode)) echo $FatherData->OfficePostalCode; else echo "";?>"  maxlength=5 type="text">
											</p>
											<p>
												<label for="">Contact Person</label>
												<input name="txtFatherContactPerson" id="txtFatherContactPerson" placeholder="" value="<?php if(isset($FatherData->ContactPersonName)) echo $FatherData->ContactPersonName; else echo "";?>"  maxlength=60 type="text">
											</p>
											<p>
												<label for="">Title</label>
												<input name="txtFatherTitle" id="txtFatherTitle" placeholder="" value="<?php if(isset($FatherData->ContactPersonTitle)) echo $FatherData->ContactPersonTitle; else echo "";?>"  maxlength=50 type="text">
											</p>											
											<p>
												<label for="">Does The Company Pay The School Fee?</label>
												<input type="radio" name="rbtFatherPaySchoolFee" id="rbtFatherPaySchoolFeeNone"  value="3" placeholder="" <?php if(isset($FatherData->PayTheSchoolFee) && $FatherData->PayTheSchoolFee==3 ) echo "checked=true"; else echo "";?>  />None
												<input type="radio" name="rbtFatherPaySchoolFee" id="rbtFatherPaySchoolFeeAll"   value="1" placeholder="" <?php if(isset($FatherData->PayTheSchoolFee) && $FatherData->PayTheSchoolFee==1) echo "checked=true"; else echo "";?> />All
												<input type="radio" name="rbtFatherPaySchoolFee" id="rbtFatherPaySchoolFeePart"   value="2" placeholder="" <?php if(isset($FatherData->PayTheSchoolFee) && $FatherData->PayTheSchoolFee==2) echo "checked=true"; else echo "";?>  />Part
											</p>

									</div>
								</div>
								
								</div>
							</div>
							<div class="form-section" id="setting-mother-info">
								<div class="header clearfix">
									<div class="left clearfix">
										<span class="title left">Mother</span>
										<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
									</div>
								</div>
								<div class="row clearfix box-equivalent">

									<div class="column one-half">
										<div class="inside-left">
											
											
											<p>
												<label for="">Company</label>
												<input name="txtMotherCompany" id="txtMotherCompany" placeholder="" value="<?php if(isset($MotherData->Company)) echo $MotherData->Company; else echo "";?>"  maxlength=150 type="text">
											</p>
											<p>
												<label for="">Job Title</label>
												<input name="txtMotherJobTitle" id="txtMotherJobTitle" placeholder="" value="<?php if(isset($MotherData->JobTitle)) echo $MotherData->JobTitle; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Office Phone Number</label>
												<input name="txtMotherOfficePhone" id="txtMotherOfficePhone" placeholder="" value="<?php if(isset($MotherData->ParentName)) echo $MotherData->OfficePhone; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Office Fax Number</label>
												<input name="txtMotherOfficeFax" id="txtMotherOfficeFax" placeholder="" value="<?php if(isset($MotherData->ParentName)) echo $MotherData->OfficeFax; else echo "";?>"  maxlength=50 type="text">
											</p>
											<p>
												<label for="">Office Address</label>
												<textarea class="textarea" name="txtMotherOfficeAddress" id="txtMotherOfficeAddress" maxlength=256 ><?php if(isset($MotherData->OfficeAddress)) echo $MotherData->OfficeAddress; else echo "";?></textarea>
											</p>
											
										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<p>
												<label for="">
													Country
												</label>
												<select name="ddlMotherOfficeCountry" id="ddlMotherOfficeCountry">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($MotherData->OfficeCountryID))
															{
																if ($MotherData->OfficeCountryID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
												<p>
												<label for="">
													City
												</label>
												<select name="ddlMotherOfficeCity" id="ddlMotherOfficeCity">
													<option value="">--Please Choose--</option>
													<?php foreach($MotherOfficeCity as $Row): ?>
														<option 
															<?php if(isset($MotherData->OfficeCityID))
															{
																if ($MotherData->OfficeCityID == $Row->CityID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CityID;?>"> 
															<?php echo $Row->CityName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
												<label for="">Postal Code</label>
												<input name="txtMotherOfficePostalCode" id="txtMotherOfficePostalCode" placeholder="" value="<?php if(isset($MotherData->OfficePostalCode)) echo $MotherData->OfficePostalCode; else echo "";?>"  maxlength=10 type="text">
											</p>
											<p>
												<label for="">Contact Person</label>
												<input name="txtMotherContactPerson" id="txtMotherContactPerson" placeholder="" value="<?php if(isset($MotherData->ContactPersonName)) echo $MotherData->ContactPersonName; else echo "";?>"  maxlength=60 type="text">
											</p>
											<p>
												<label for="">Title</label>
												<input name="txtMotherTitle" id="txtMotherTitle" placeholder="" value="<?php if(isset($MotherData->ContactPersonTitle)) echo $MotherData->ContactPersonTitle; else echo "";?>"  maxlength=50 type="text">
											</p>	
											<p>
												<label for="">Does The Company Pay The School Fee?</label>
												<input type="radio" name="rbtMotherPaySchoolFee" id="rbtMotherPaySchoolFeeNone"  value="3"  placeholder="" <?php if(isset($MotherData->PayTheSchoolFee) && $MotherData->PayTheSchoolFee==3 ) echo "checked=true"; else echo "";?>  />None
												<input type="radio" name="rbtMotherPaySchoolFee" id="rbtMotherPaySchoolFeeAll"  value="1"  placeholder="" <?php if(isset($MotherData->PayTheSchoolFee) && $MotherData->PayTheSchoolFee==1) echo "checked=true"; else echo "";?> />All
												<input type="radio" name="rbtMotherPaySchoolFee" id="rbtMotherPaySchoolFeePart"  value="2"  placeholder="" <?php if(isset($MotherData->PayTheSchoolFee) && $MotherData->PayTheSchoolFee==2) echo "checked=true"; else echo "";?>  />Part
											</p>
									</div>
								</div>
											
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.highlight {
    display: inline;
    position: relative;
}
</style>
    

  <!-- .container -->
  


<div id="content">
	<!-- #body-nav -->
	<div id="body" class="styled-content">
		<div class="inside">
			<h1 class="body-title">Document</h1>
				<div class="row">
				<div class="column has-border three-third center">
				<div class="inside">
					
						<form name="frmSearch" action="<?php echo site_url().'/';?>entry/<?php echo $TabName; ?>/search" method="get">
							<p>
								<label>No Form: </label>
								<span>
									<?php 
									$Value="";
									if(isset($_GET['txtFormNumber']))
										$Value=$_GET['txtFormNumber']; 
									else if(isset($FormNumber))
										$Value=$FormNumber; 
									?>
									<input type="text" name="txtFormNumber" value="<?php echo $Value; ?>" />
							<p>
								<label>Admission Process : </label>
								<span>
									<select name="ddlAdmissionProcess" id="ddlAdmissionProcess">
										<option value="">--Please Choose--</option>
										<?php foreach($ddlAdmissionProcess as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionProcess']))
												{
													if ($_GET['ddlAdmissionProcess'] == $Row->AdmissionProcessID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->AdmissionProcessID;?>"> 
												<?php echo $Row->AdmissionProcessName; ?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						</div>
			</div>
		</div>
				</div>
		</div>
	</div>


	<div id="body-components" class="component">

		<?php $this->load->view("entry/header"); ?>

		<div class="dashboard-parent">

			<div class="profile-setting">
				<div class="inside">
					<div id="internal-popup" style="display: none;">
						<h2 class="popup-title" id="popup-title"></h2>
						<div id="body" class="styled-content">
						<div class="inside">
							<div class="row">
						<input type="hidden" name="hfPage" value="1" />
					
						<p>
							<label>
								<span>School Name</span></br>
								<span><i>Nama Sekolah</i></span>
							</label>
						</p>
						<p>
							<input type="text" class="text" id="txtSearchSchoolName" ></input>
						</p>
						<p>
							<input type="button" value="SEARCH" id="btnSearch" class="button" style="align:center"/>
						</p>
						</div>
						</div>
						</div>
					
				<table id="TableSearchSchool" class="ListData">
					<thead>
						<tr>
							<th></th>
							<th>SchoolID</th>
							<th>School Name</th>
						</tr>
						</thead>
						
						<tbody>
						
						
						
						</tbody>
							</table>
							
							
					
						
						<div id="paging" style="display:none">
									<div class="pagination">
									<div class="prev-next-nav">
									  <a href="#" class="prev-nav icon icon-pagination-left disable">
										Prev
									  </a>
									  <a href="#" class="next-nav icon icon-pagination-right">
										Next
									  </a>
									</div><!-- .prev-next-nav -->
									<div class="pagenavi">
									  <div class="the-navi">
									  </div>
									  <div class="pages"></div>            
									</div>
								  </div>
						</div>
							<?php
									if(isset($data['pagination']))
									echo "<script>
											Show_Pagination({
												'Selector' : '#paging',
												'TotalPage' : ".$data['pagination']['last'].",
												'CurrentPage' : ".$data['pagination']['current'].",
												'PreviousPage' : ".$data['pagination']['previous'].",
												'NextPage' : ".$data['pagination']['next'].",
												'InfoPage' : '".$data['pagination']['info']."'
												});
										</script>";
							?>
							
					</div><!-- #internal-popup -->
					<div class="form-section" id="setting-personal">
						<div class="header clearfix">
							<div class="left clearfix">

								<span class="title left">Background School</span>
							</div>
							<div class="right clearfix edit-section  							
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="divider left"></span>
								<span class="link left"><a href="#!/edit-personal-information/" id="edit-personal">Edit</a></span>
							</div>
							<div class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-personal-information/" id="cancel-edit-personal">Cancel</a></span>
								<span class="divider left"></span>
								<span class="action left"><a id="form-submit" class="button" id="btnSaveSchool" >Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-BGSchool" action="<?php echo site_url().'/';?>entry/registrant_background/save_BG_school_data" method="post" class="front-editing">
									<div class="column half">
										<div class="inside-left">
											
											<p>
												
												<label for="">Type Of Previous School*</label>
												<select name="ddlTypeOfSchool" id="ddlTypeOfSchool">
													<option value="">--Please Choose--</option>
													<?php foreach($TypeOfSchool as $Row): ?>
														<option value="<?php echo $Row->TypeOfSchoolID;?>"> 
															<?php echo $Row->TypeOfSchool;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">School Name</label>
								<input name="txtSchoolName" id="txtSchoolName" readonly="true" maxlength=10 type="text" style="float:left;width:70% !important" />
								<input type="hidden" name="hfSearchSchool" id="hfSearchSchool" />
												<a class="div-search"><i class="icon icon-search-modal"></i></a>
					<!--
												<select name="ddlSchoolName" id="ddlSchoolName">
													<option value="">--Please Choose--</option>
													<?php /* foreach($SchoolName as $Row): ?>
														<option  value="<?php echo $Row->SchoolID;?>"> 
															<?php echo $Row->SchoolName;?> 
														</option>
													<?php endforeach; */ ?>
												</select>-->
											</p>


										
											
											
										</div>
									</div>
									<div class="column half">
										<div class="inside-right">
											<p>
												<label for="">Last Year Level</label>
												<select name="ddlYearLevel" id="ddlYearLevel" >
													<option value="">--Please Choose--</option>
													<?php foreach($YearLevel as $Row): ?>
														<option value="<?php echo $Row->YearLevelID;?>"> 
															<?php echo $Row->YearLevelName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Starting/Closing Attended Year*</label>
												<input name="txtStartAttendedYear" id="txtStartAttendedYear" style="width:120px;float:left" id="txtStartAttendedYear" placeholder=""   maxlength=4 type="text">
												<div style="width:10px;align:center;margin-top: -20px;margin-left: 125px;">-</div>
												<input name="txtEndAttendedYear" id="txtEndAttendedYear" style="width:120px;margin-top: -24px;margin-left: 135px;"  id="txtEndAttendedYear" placeholder="" maxlength=4 type="text">
											</p>
												<p>
												<label for="">Begin/End of School Year</label>
												<select name="ddlBeginSchoolYear" id="ddlBeginSchoolYear" style="width:125px;float:left" >
													<option value="">--Please Choose--</option>
													<?php for($i=1;$i<=13;$i++){ ?>
														<option value="<?php echo $i;?>" ><?php echo $i;?></option>
													<?php } ?>
												</select>
												<select name="ddlEndSchoolYear" id="ddlEndSchoolYear" style="width:125px"   >
													<option value="">--Please Choose--</option>
													<?php for($i=1;$i<=13;$i++){ ?>
														<option value="<?php echo $i;?>" ><?php echo $i;?></option>
													<?php } ?>
												</select>
											</p>
										</div>
									</div>
								
							</div>
							<table id="TableBGSchool">
								<thead>
									<tr>
										<th></th>
										<th>School Name</th>
										<th>Last Year Level</th>
										<th>Starting Attended Year</th>
										<th>Closing Attended Year</th>
										<th>Begin</th>
										<th>End</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($BGSchoolData)){ ?>
									<?php foreach($BGSchoolData as $row): ?>
									<tr>
									<td class='IconColumn'>
									<i class='icon icon-delete'></i>&nbsp;<i class='icon icon-edit'>
									</i>
									</td>
									<td>
										<?php echo $row->SchoolName; ?>		
									</td>
									<td>
										<?php echo $row->LastYearLevel==0?"-":$row->YearLevelName; ?>		
									</td>
									<td>
										<?php echo trim($row->YearAttended)==""?"-":$row->YearAttended; ?>		
									</td>
									<td>
										<?php echo trim($row->YearWithDrawn)==""?"-":$row->YearWithDrawn; ?>		
									</td>
									<td>
										<?php echo $row->BeginningOfSchoolYear==0?"-":$row->BeginningOfSchoolYear; ?>		
									</td>
									<td>
										<?php echo $row->EndOfSchoolYear==0?"-":$row->EndOfSchoolYear; ?>		
									</td>
									</tr>

									<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a  class="button" id="btnAddBGSchool">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
								
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
					

					<div class="form-section" id="setting-language">
						<div class="header clearfix">
							<div class="left clearfix">
								
								<span class="title left">Background Language</span>
							</div>
							<div id="language-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="divider left"></span>
								<span class="link left"><a href="#!/edit-language/" id="edit-language">Edit</a></span>
							</div>
							<div id="language-save-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-language">Cancel</a></span>
								<span class="divider left"></span>
								<span class="action left"><a href="#" class="button" id="btnSaveBGLanguage">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-BGLanguage" action="<?php echo site_url().'/';?>entry/registrant_background/save_BG_language_data" method="post" class="front-editing">
									<div class="column half">
										<div class="inside-left">
											
											<p>
												
												<label for="">Language*</label>
												<select name="ddlLanguage" id="ddlLanguage">
													<option value="">--Please Choose--</option>
													<?php foreach($Language as $Row): ?>
														<option value="<?php echo $Row->LanguageID;?>"> 
															<?php echo $Row->LanguageName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
										</div>
									</div>
									
								
							</div>
							<table id="TableBGLanguage">
								<thead>
									<tr>
										<th></th>
										<th>Language</th>
										<th>First Language</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($BGLanguageData)){ ?>
									<?php foreach($BGLanguageData as $row): ?>
									<tr>
									<td class='IconColumn'>
									<i class='icon icon-delete hide'></i>&nbsp;<i class='icon icon-edit hide'>
									</i>
									</td>
									<td>
										<?php echo $row->LanguageName; ?>		
									</td>
									<td class='CheckBoxColumn'>
									<input type="checkbox" id="cbxSame" <?php echo $row->FirstLanguage==1?'checked':''; ?>  class='cbxSame' />
									</td>
									</tr>

									<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a  class="button" id="btnAddBGLanguage">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
							
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
				
				</div>
			</div>

		</div>

	</div>

    

  <!-- .container -->
  

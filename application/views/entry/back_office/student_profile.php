<div id="content">
	<!-- #body-nav -->
	<div id="body" class="styled-content">
		<div class="inside">
			<h1 class="body-title">Form Sales</h1>
			<div class="row">
				<div class="column has-border three-third center">
					<div class="inside">
						<form name="frmSearch" action="<?php echo site_url().'/';?>entry/form_return_front_office/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span>
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 

												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span>
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level : </label>
								<span>
									<select name="ddlYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>No Form: </label>
								<span>
									<input type="text" name="txtFormNumber" value="<?php if(isset($_GET['txtFormNumber'])) echo $_GET['txtFormNumber']; else echo "";?>" />
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
					</div>
					<div class="inside">
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Form Number</th>
										<th>Student Name</th>
										<th>School Level</th>
										<th>Year Level</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->FormNumber;?>">
											<td style="text-align:center">
												<a class="icon icon-view btnView">Edit</a>
											</td>
											<td><?php echo $Row->FormNumber; ?></td>
											<td><?php echo $Row->StudentName; ?></td>
											<td><?php echo $Row->SchoolLevelName; ?></td>
											<td><?php echo $Row->YearLevelName; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
						</div>
						
								<div id="paging" <?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
								<div class="prev-next-nav">
								  <a href="#" class="prev-nav icon icon-pagination-left disable">
									Prev
								  </a>
								  <a href="#" class="next-nav icon icon-pagination-right">
									Next
								  </a>
								</div><!-- .prev-next-nav -->
								<div class="pagenavi">
								  <div class="the-navi">
								  </div>
								  <div class="pages"></div>            
								</div>
							  </div>
								</div>
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>
							
							<p align="center" style="clear:both;padding-top:20px;">
										<input type="button" class="button" value="Add" id="btnAdd">
								
							</p>
		
					</div>
					<div id="internal-popup" style="display:none;">
						<h2 class="popup-title">Add Form Sales</h2>
						<div class="popup-content">
							<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>entry/form_return_front_office/save_form_return_FO" method="post">
								<input type="hidden" name="hfAdmissionID"/>
								<input type="hidden" name="hfScheduleID"/>
								<input type="hidden" name="hfVenue"/>
								<input type="hidden" name="hfViewAdmissionSemester"/>
								
								<p>
									<label>Admission ID* : </label>
									<input type="text" name="txtViewAdmissionID"  maxlength=6  value="" readonly="readonly"/>
								</p>
								<p>
									<label>Admission Year* : </label>
									<span>
										<input type="text" name="txtViewAdmissionYear" maxlength=4 value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Semester* : </label>
									<span>
										<input type="text" name="txtViewAdmissionSemester" maxlength=1 value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
									</span>
								</p>
									<p>
								<label>School Level* : </label>
								<span>
									<input type="text" name="txtViewSchoolLevel" value="<?php if(isset($_GET['txtViewSchoolLevel'])) echo $_GET['txtViewSchoolLevel']; else echo "";?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Admission Term* : </label>
								<span>
									<input type="text" name="txtViewAdmissionTerm" value="<?php if(isset($_GET['txtViewAdmissionTerm'])) echo $_GET['txtViewAdmissionTerm']; else echo "";?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>No Form* : </label>
								<span>
									<input type="text" name="txtViewFormNumber" maxlength=13  value="" />
								</span>
								<input type="button" id="btnCheck" class="button" value="check" ></input>
							</p>
							<p>
								<label>First Name* : </label>
								<span>
									<input type="text"  name="txtViewFirstName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Middle Name : </label>
								<span>
									<input type="text"  name="txtViewMiddleName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Last Name : </label>
								<span>
									<input type="text"  name="txtViewLastName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Date Of Birth*: </label>
								<span>
									<input type="text"  class="datepicker" name="txtViewDateOfBirth"  maxlength=11 value="" readonly="true"/>
								</span>
							</p>
							<p>
								<label>Place Of Birth* : </label>
								<span>
									<input type="text" name="txtViewPlaceOfBirth"  maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Gender* : </label>
								<span>
									<select name="ddlViewGender">
										<option value="">--Please Choose--</option>
										<?php foreach($Gender as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlGender']))
												{
													if ($_GET['ddlGender'] == $Row->GenderID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->GenderID;?>"> 
												<?php echo $Row->GenderDescription;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Child Status* : </label>
								<span>
									<select name="ddlViewChildStatus">
										<option value="">--Please Choose--</option>
										<?php foreach($ChildStatus as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlChildStatus']))
												{
													if ($_GET['ddlChildStatus'] == $Row->ChildStatusID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->ChildStatusID;?>"> 
												<?php echo $Row->ChildStatusName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Type OF Registration* : </label>
								<span>
									<select name="ddlViewTypeOfRegistration">
										<option value="">--Please Choose--</option>
										<?php foreach($TypeOfRegistration as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlTypeOfRegistration']))
												{
													if ($_GET['ddlTypeOfRegistration'] == $Row->TypeOfRegistrationId )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TypeOfRegistrationId;?>"> 
												<?php echo $Row->TypeOfRegistration;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>To Year Level* : </label>
							<span>
									<select name="ddlViewToYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>To Semester* : </label>
								<span>
									<select name="ddlViewToSemester">
										<option value="">--Please Choose--</option>
										<?php foreach($Semester as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSemester']))
												{
													if ($_GET['ddlSemester'] == $Row->SmtID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SmtID;?>"> 
												<?php echo $Row->SmtDescription;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Date Of Entry* : </label>
								<span>
									<input type="text" class="datetimepicker" maxlength=11 name="txtViewDateOfEntry" value="" readonly="true" />
								</span>
							</p>
							<p>
								<label>Date Of Form Receipt* : </label>
								<span>
									<input type="text" class="datetimepicker"  maxlength=11 name="txtViewDateOfFormReceipt" value="" readonly="true" />
								</span>
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button" value="Save" id="btnSave">
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

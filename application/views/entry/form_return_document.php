<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Document Upload</h2>
					<form name="frmSearch" action="<?php echo site_url().'/';?>entry/form_return_document/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							
							<p>
								<label>No Form: </label>
								<span>
									<?php 
									$Value="";
									if(isset($_GET['txtFormNumber']))
										$Value=$_GET['txtFormNumber']; 
									else if(isset($FormNumber))
										$Value=$FormNumber; 
									?>
									<input type="text" name="txtFormNumber" value="<?php echo $Value; ?>" />
							<p>
								<label>Admission Process : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionProcess" id="ddlAdmissionProcess">
										<option value="">--Please Choose--</option>
										<?php foreach($AdmissionProcess as $Row): ?>
											<option 
												<?php if(isset($AdmissionProcessID))
												{
													if ($AdmissionProcessID == $Row->AdmissionProcessID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->AdmissionProcessID;?>"> 
												<?php echo $Row->AdmissionProcessName; ?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView" class="ListData bordered zebra ">
								<thead>
									<tr>
										<th>Upload</th>
										<th>View</th>
										<th>Download</th>
										<th>Document Name</th>
										<th>Status Upload</th>
										<th>Verify Document</th>
										<th>File Name</th>
										<th>Pending Date</th>
										<th>Received Date</th>
										<th>Received By</th>
										<th>Note</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php //if($data['result']) { ?>
										<?php if(empty($RegistrantSubmissionData)){ ?>
												<?php if(!empty($DocumentList)){ ?>
													<?php $i=0; ?>
													<?php foreach($DocumentList as $row): ?>
													<tr>
													<td class="IconColumn">
													<?php //if(isset($row->Verification)&&$row->Verification!='1'){ ?>
														<i class="icon icon-upload"><input id="<?php echo "file-".$i; ?>" name="<?php echo "file-".$i; ?>" type="file" hidden="true"  /></i>
													<?php //} ?>
													</td>
													<td>
													
													</td>
													<td>
													
													</td>
													<td style="text-align:left">
														<?php echo $row->DocumentName; ?>
													</td>
													<td style="text-align:left" hidden="true">
														<?php echo $row->DocumentID; ?>
													</td>
													<td>
														No
													</td>
													<td>
													</td>
													<td>
													</td>
													<td>
													</td>
													<td>
													</td>
													<td>
													</td>
													<td>
													</td>
													</tr>
													<?php $i++; ?>
													<?php endforeach; ?>
												<?php } ?>
												
												<?php }else{ ?>
														<?php $i=0; ?>
														<?php foreach($RegistrantSubmissionData as $row): ?>
															<tr>
				
															<td style="text-align:center" class="IconColumn">
																<?php if(isset($row->Verification)&&$row->Verification!='1'){ ?>
																	<i class="icon icon-upload"><input id="<?php echo "file-".$i; ?>" name="<?php echo "file-".$i; ?>" type="file" hidden="true" /></i>
																<?php } ?>
															</td>
															<td style="text-align:center">
															<?php if($row->FileName==NULL || $row->FileSize==0 || $row->FileName=="documen_fisik.jpg"){ 

																}else{?>
															<a><i class='icon icon-view' filename="<?php  echo $row->FileName ?>" source="<?php echo $this->config->item('form_return_BO_view_path').'/DocumentUpload'.'/'.$row->FileName; ?>" ></i></a>
															<?php } ?>
															</td>
															<td style="text-align:center">
															<?php if($row->FileName==NULL || $row->FileSize==0 || $row->FileName=="documen_fisik.jpg"){ 

																}else{?>
															<a href="<?php echo $this->config->item('form_return_BO_view_path').'/DocumentUpload'.'/'.$row->FileName; ?>"  download='<?php echo $row->FileName; ?>' target='_blank' ><i class='icon icon-download' ></i></a>
															<?php } ?>
															</td>
															<td style="text-align:left">
																<?php echo $row->DocumentName; ?>
															</td>
															<td style="text-align:left" hidden="true">
																<?php echo $row->DocumentID; ?>
															</td>
															<td style="text-align:left" hidden="true">
																<?php echo $row->SubmissionID; ?>
															</td>
															<td style="text-align:center">
															<?php
															if($row->FileName!="" && !empty($row->FileName))
																echo "Yes";
															else
																echo "No";
															?>
															</td>

															<td style="text-align:center">
															<?php  
																$Icon="";
																if($row->Verification!="" && !empty($row->Verification)){
																	if($row->Verification=='1')
																		$Icon="icon icon-check";
																	else if($row->Verification=='2')
																		$Icon="icon icon-fail";
																	else
																		$Icon="icon icon-widget-attendance";
																}

															?>
															<?php if($row->Verification!="" && !empty($row->Verification))?>
															<i class="<?php echo $Icon; ?>"></i>
															
															</td>
															<td>
																<?php 
																	echo $row->FileName;
																?>
															</td>
															<td>
																<?php 
																	echo $row->DueDate;
																?>
															</td>
															<td>
																<?php 
																	echo $row->ReceivedDate;
																?>
															</td>
															<td>
																<?php 
																	echo $row->ReceivedBy;
																?>
															</td>
															<td>
																<?php 
																	echo $row->Note;
																?>
															</td>
															</tr>
															<?php $i++; ?>
															<?php endforeach; ?>
													
												
												<?php } ?>
									<?php // } 
									//else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';} ?>
								</tbody>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading" id="modal-document_title">Document Upload</h2>
			<form name="frmUpload" class="styled-content" action="<?php echo site_url().'/';?>entry/form_return_document/save_document"  enctype="multipart/form-data" method="post">
								<input type="hidden" name="hfFormNumber" value="<?php echo isset($FormNumber)?$FormNumber:"" ?>" />
								<input type="hidden" name="hfAdmissionProcess" value="<?php echo isset($AdmissionProcessID)?$AdmissionProcessID:""  ?>" />
								<input type="hidden" name="hfDocumentID" value="" />
								<input type="hidden" name="hfSubmissionID" value="" />
								<input type="hidden" name="hfFileName" value="" />
								<p>
									<label>Select Action : </label>
									<input type="radio" name="rbtAction" id='rbtUploadFile' value="UploadFile" checked="true"/>Upload File
									<input type="radio" name="rbtAction" id='rbtSubmitPhysicalFile' value="SubmitPhysicalFile" />Submit Physical File
									<input type="radio" name="rbtAction" id='rbtPending' value="Pending" />Pending
								</p>
								<p  id="p-file"  hidden="true">
									<label>Choose File : </label>
									
									<input type="file" name="file" id='file' />
									<div id="divKeterangan">
									<div>.jpg .jpeg .png .pdf .zip .rar</div></br>
									<div>6MB of Max Size</div>
									</div>
								</p>
								<p  id="p-DateLine"   >
									<label>Date Line : </label>
									<span class="custom-datepicker">
										<input type="text" name="txtDateLine" id="txtDateLine" value="" class="datepicker"  readonly="readonly"/>
									</span>
								</p>
								<p  id="p-txtDate" hidden="true">
									<label>Date : </label>
									<span class="custom-datepicker">
										<input type="text" name="txtDate" value="" class="text datepicker" readonly="readonly"/>
									</span>
								</p>
								
								<p>
									<label>Note : </label>
									<span>
										<textarea type="text" name="txtNote" id="txtNote" value="" maxlength=100 ></textarea>
								</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="save();">
								</p>
							</form>
		</div>
	</div>
	<!--Content Status-->
	<div id="view-popup" class="popup-content" style="padding:60px">
		<h2 class="popup-title" id="modal-file_title">File Name</h2>
		<div class="popup-content">
			<img id="popup-image" src=""></img>
		</div>
	</div>
	<!-- End Content Status-->	
</div>

<style>
	.floatThead-wrapper {
    max-height: 500px;
    width: 100%;
    overflow: auto;
}
</style>
<script>
$(document).ready(function()
{
	var internalpopup_default = $("#internal-popup").html();

$('.datepicker').datepicker({ dateFormat: 'dd M yy' });
	var param=$('#ActiveTab').val();
		switch(param){
			case "student_profile":
				$(".nav-head li:nth-child(1)").addClass( "current");
				//$( "#icon-profile" ).addClass( "active");
				break;
			case "registrant_background":	
				$(".nav-head li:nth-child(2)").addClass( "current");	
				//$( "#icon-background" ).addClass( "active");
				break;
			case "parent_profile":	
					$(".nav-head li:nth-child(3)").addClass( "current");
					//$( "#icon-parent" ).addClass( "active");
				break;
			case "guardian_profile":
					$(".nav-head li:nth-child(4)").addClass( "current");
					//$( "#icon-guardian" ).addClass( "active");
				break;
			case "sibling":	
					$(".nav-head li:nth-child(5)").addClass( "current");
				//$( "#icon-sibling" ).addClass( "active");
				break;
			case "personal_development":	
					$(".nav-head li:nth-child(6)").addClass( "current");
				break;
			case "ID_card":		
					$(".nav-head li:nth-child(7)").addClass( "current");
				break;
			case "learning_note":	
				$(".nav-head li:nth-child(8)").addClass( "current");
				break;
			case "reregistration":
				$(".nav-head li:nth-child(9)").addClass( "current");
				break;
		}

			$( "#profpic").click(function(){
			if($('[name="txtFormNumber"]').val()!="")
				$('#file').click();
		});
		function ValidateFile(file){
			var ErrText="";
			if(file && file.length > 0) {
				file = file[0];
				var ext = file.name.split(/\./).pop().toLowerCase();
			
			
				if(file.type != 'image/jpeg' && file.type != 'image/png') 
				{
					ErrText='Your file is not in jpg, jpeg, png format';
				}
				
				if(ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'pdf' && ext != 'rar'  && ext != 'zip') {
					ErrText='Yor file is not in jpg, jpeg, png format';
				}
				
				if(file.size > 2 * 1024 * 1024) {
					ErrText='Your file size can not be bigger than 2MB';
				}
			} else {
				ErrText="";
			}
			return ErrText;
		}
		function SaveFile(){
			var formData = new FormData();
			var file=$('#file').prop('files');
			formData.append('FileName', file[0]);
			
			$.ajax({
					url: '<?php echo site_url('/entry/form_return_back_office/save_profile_picture'); ?>',  //Server script to process data
					type: 'POST',
					// Form data
					data:formData,
					dataType:'json',
					//Options to tell jQuery not to process data or worry about content-type.
					cache: false,
					contentType: false,
					processData: false,
					beforeSend:function(){
						$('#dvLoading').show();
					},
					success:  function(data){
							//alert("Count"+data.Count);
							
							if(data.Photo!=""){
								
								SaveToDB(data.Photo);
							}
							else{
								alert("failed");
								$('#dvLoading').hide();
							}
							
						},
					complete:function(){
						//$('#dvLoading').hide();
					},
					error: function(xhr, textStatus, errorThrown){
						$('#dvLoading').hide();
						alert('failed');
					}
			});

		}
	
		function SaveToDB(file){
				
			$.ajax({
				type: "post",
				url:  '<?php echo site_url('/entry/form_return_back_office/save_profile_picture_db'); ?>',
				data: {
					   Data:file,
					   RegistrantID:$('[name="txtFormNumber"]').val()
					   },
				beforeSend:function(){
					//$('#dvLoading').show();
				},	   
				dataType: "json",
				success: function(){
						alert("success");
				},
				complete:function(){
					$('#dvLoading').hide();
				},
				error: function(xhr, textStatus, errorThrown){
					alert('failed');
				}
			})
	

		}
		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var err=ValidateFile(input.files);
				if(err!=""){
					alert(err);
					return;
				}
				var reader = new FileReader();
				
				reader.onload = function (e) {
					$('#profpic').attr('src', e.target.result);
				}
				
				reader.readAsDataURL(input.files[0]);
				SaveFile();
			}
		}
    
		$("#file").change(function(){
		
			readURL(this);
		});
	    

	function Search(){
		var txtSearchName=$.trim($('#txtSearchStudentName').val());
		var txtSearchForm=$.trim($('#txtSearchFormNumber').val());
		if(txtSearchName==""){
			txtSearchName=null;
		}
		if(txtSearchForm=="")
			txtSearchForm=null;
			
		var hfPage=$.trim($('#internal-popup [name=hfPage]').val());
		$.ajax({
			type: "get",
			url: SiteURL+'entry/form_return_back_office/get_student_paging/'+txtSearchName+'/'+txtSearchForm+'/'+hfPage,
			async:true,
			dataType: "json",
			contentType: 'application/json;charset=utf-8',
			beforeSend: function(){
				var Table=$("#TableSearchStudent");
				//var ImgLoading=$("<tr><td colspan=3><img style='width:50px;height:50px' src='<?php echo base_url();?>images/loading.gif' title='Loading...' class='loading' /></td></tr>");
				//Table.append(ImgLoading);
			},
			
			success: function(data){
					if(data.status == true) {
							TempData=new Array();
							$("#TableSearchStudent > tbody:last").children().remove();
							if(!data.SearchResult){
								alert("There is No data");
								$('#internal-popup #paging').hide();
							}
							else{
									//alert("DATA EXIST");
									$('#internal-popup #paging').show();
								}
								
								
								
								var Table=$("#TableSearchStudent");
									for(var i in data.SearchResult){
										
										var Row=$("<tr>");
										
										if(!data.SearchResult[i].StudentID)
											data.SearchResult[i].StudentID="-";
										if(!data.SearchResult[i].StudentName)
											data.SearchResult[i].StudentName="-";	
										
										var td="<td><a href='' class='selector'>select</a></td>";
										Row.append(td);
										for(var j=0;j<2;j++){
											var Text="";
											switch(j){
												case 0:Text=data.SearchResult[i].StudentID;	
													break;
												case 1:Text=data.SearchResult[i].StudentName;
													break;
											}
											
											Row.append( $("<td>").text(Text) );
										}
										
											
										var Data={"StudentID":data.SearchResult[i].StudentID,"StudentName":data.SearchResult[i].StudentName}										
										TempData.push(Data);
										Table.append(Row);
									}	

																
					} 
					else {
						alert("There is No data");
					}
					if(data.pagination){
						Show_Pagination({
							'Selector' : '#paging',
							'TotalPage' : data.pagination.last,
							'CurrentPage' : data.pagination.current,
							'PreviousPage' : data.pagination.previous,
							'NextPage' : data.pagination.next,
							'InfoPage' : data.pagination.info
							});
							
					}
				//$('[name=hfPage]').val($(this).attr('data-page'));		
			},
			complete: function(){
					$('.selector').on('click',function(e){
							e.preventDefault();
							var myRow = $(this).parent().parent().index();
							$('[name="txtFormNumber"]').val($.trim(TempData[myRow].StudentID));
							$('#txtName').val(TempData[myRow].StudentName);
							
							$.fancybox.close();
							$('[name="frmSearch"]').submit();
					});

					$('#internal-popup .page-click').on('click',function(){
						$('#internal-popup [name=hfPage]').val($(this).attr('data-page'));
						Search();
						
						//location.href = updateQueryStringParameter('hfPage',$(this).attr('data-page'));
					});	
			},
			error: function(xhr, textStatus, errorThrown){
				alert('failed');
			}
		})
	
			
	}
	
	
	$("#btnSearchRegistrant").on('click', function(e) {
		$("#internal-popup").html(internalpopup_default);
		e.preventDefault();
		$.fancybox({
        	href 		: '#internal-popup',
       	    minWidth    : 640,
            fitToView   : true,
            closeClick  : false,
            live:false,
            afterShow: function () {
            	
            }
    	});

		$("#btnSearchHeader").click(function(){
			$('#internal-popup [name=hfPage]').val("1");
			Search();
			
			$('#internal-popup .prev').on('click',function(e){
		
				if($(this).hasClass('disable')){
					e.preventDefault();
					return;
				}
			$('#internal-popup [name=hfPage]').val(parseInt($('[name=hfPage]').val())-1);
			Search();
			});
			$('#internal-popup .next').on('click',function(e){
				//alert($('[name=hfPage]').val());
				if($(this).hasClass('disable')){
					e.preventDefault();
					return;
				}
				$('#internal-popup [name=hfPage]').val(parseInt($('[name=hfPage]').val())+1);
				
				Search();
			});
		});
		
	});
});

</script>

<div class="box-container">
				<h2 class="heading" style="margin:0px 0px 25px">Return Form Back Office</h2>
				<div class="w-640px" style="padding:20px 20px; background: #f6f6f6; border: 1px solid #c9c9c9;">
					<form name="frmSearch" action="<?php echo site_url().'/';?>entry/<?php echo $TabName; ?>/search" method="get">
							
							<!--
								<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">
							</p> -->
														<p>
								<label>No Formulir: </label>
								<span>
									<?php 
									$Value="";
									if(isset($_GET['txtFormNumber']))
										$Value=$_GET['txtFormNumber']; 
									else if(isset($FormNumber))
										$Value=$FormNumber; 
									?>
									<input type="text" readonly="readonly" name="txtFormNumber"  style="float:left;width:92%;"  value="<?php echo $Value; ?>" />
									
									<a id="btnSearchRegistrant" class="div-search" style="float:left;width:7%; margin-left:1%;    text-align: center;     background-color: #991845;" ><span class="icon icon-search-modal"></span></a>
								</span>
								
							</p>
							
								<table style="background-color:transparent;">
								<tr style="background-color:transparent; border-bottom:0px;">
									<td style="width:120px;vertical-align:top">
											<img width="120px" height="120px" style="cursor: pointer; cursor: hand;" id='profpic' src="<?php echo isset($Photo->Photo)?$this->config->item('form_return_BO_view_path')."/ProfilePicture/".$Photo->Photo: base_url().'resources/images/images/UploadPhotoHere.png'; ?>">
											<input hidden type="file" name="file" id="file"/></img>
									</td>
									<td>
											<label>Name: </label>
										<span>
											<label  id="txtName" ><strong><?php echo $txtName; ?></strong></label>
										</span>
										<label>Grade: </label>
										<span>
											<label  id="txtName" ><strong><?php echo $txtGrade; ?></strong></label>
										</span>
										<label>Term: </label>
										<span>
											<label  id="txtName" ><strong><?php echo $txtTerm; ?></strong></label>
										</span>
										<label>Result Entrance Test: </label>
										<span>
											<label  id="txtResultET" ><strong><?php echo $txtResultET; ?></strong></label>
										</span>
									</td>
								</tr>
								</table>
						
						</form>
				</div>
</div>

<section class="body-navigation on-pre-content" style="border: 1px solid #c9c9c9;background-color: white;">
			<div class="nav-head" style="height: 100px;">
				<input name="ActiveTab" id="ActiveTab" type="hidden" value="<?php if(isset($TabName)) echo  $TabName; else echo ""; ?>">
				<div class="container">
					<ul>
						<li data-number="1">
							<a href="<?php echo site_url().'/';?>entry/student_profile/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Registrant Profile</span>
							</a>
						</li>
						<li data-number="2">
							<a href="<?php echo site_url().'/';?>entry/registrant_background/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Registrant Background</span>
							</a>
						</li>
						<li data-number="3" >
							<a href="<?php echo site_url().'/';?>entry/parent_profile/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Parent Profile</span>
							</a>
						</li>
						<li data-number="4" >
							<a href="<?php echo site_url().'/';?>entry/guardian_profile/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Guardian</span>
							</a>
						</li>
						<li data-number="5" >
							<a href="<?php echo site_url().'/';?>entry/sibling/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Sibling</span>
							</a>
						</li>
						<li data-number="6" >
							<a href="<?php echo site_url().'/';?>entry/personal_development/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px; width:120px;">
								<span class="label">Child's Personal Development</span>
							</a>
						</li>
						<li data-number="7" >
							<a href="<?php echo site_url().'/';?>entry/ID_card/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">ID Card</span>
							</a>
						</li>
						<li data-number="8" >
							<a href="<?php echo site_url().'/';?>entry/learning_note/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Learning Note</span>
							</a>
						</li>
						<li data-number="9" >
							<a href="<?php echo site_url().'/';?>entry/reregistration/search?txtFormNumber=<?php echo $Value; ?>" style="height: 34px; line-height: 34px;">
								<span class="label">Re-registration</span>
							</a>
						</li>
					</ul>

			</div>
</section>
<div style="display:none">
	<div id="internal-popup">
			<div class="single-content popup-content" style="padding:60px">
					<input type="hidden" name="hfPage" value="1" />
					<p>
						<label>
							<span>Student Name</span></br>
							<span><i>Nama Siswa</i></span>
						</label>
					</p>
					<p>
						<input type="text" class="text" id="txtSearchStudentName" ></input>
					</p>
					<p>
						<label>
							<span>Form Number</span></br>
							<span><i>Nomor Formulir</i></span>
						</label>
					</p>
					<p>
						<input type="text" class="text" id="txtSearchFormNumber" ></input>
					</p>
					<p>
						<input type="button" value="SEARCH" id="btnSearchHeader" class="button button-primary" style="align:center"/>
					</p>
				
			<table id="TableSearchStudent" class="ListData bordered zebra">
					<thead>
						<tr>
							<th></th>
							<th>Form Number</th>
							<th>Student Name</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
			</table>
					<div id="paging" style="display:none">
					<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev pagination-left disable"></a>
        							<a href="#" class="next pagination-right"></a>
    								</span>
					</div>
					</div>
						<?php
								if(isset($data['pagination']))
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
						?>
		</div>				
	</div>
</div><!-- #internal-popup -->

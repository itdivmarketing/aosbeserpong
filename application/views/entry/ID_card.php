<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
					<div class="form-section">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Student</span>
							</div>
							<div id="ID_card-student-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-student-information/" id="edit-student-ID_card">Edit</a></span>
							</div>
							<div id="ID_card-student-save-section" class="right clearfix hide save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-student-ID_card">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a class="button button-primary" id="save-student-ID_card">Save</a></span>
							</div>
						</div>
						<div class="content">
						<form  id="form-StudentIDCard" action="<?php echo site_url().'/';?>entry/ID_card/save_student_ID_card" method="post" class="front-editing">
							<div class="form-section" id="setting-student-ID_card">
								<div class="row clearfix box-equivalent">
				
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											
											<p>
												<label for="">
													Nationality
												</label>
												<select name="ddlStudentNationality" id="ddlStudentNationality">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($StudentNationality->NationalityID))
															{
																if ($StudentNationality->NationalityID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
											<label for="">Identity No</label>
												<input name="txtStudentIDNo" id="txtStudentIDNo" placeholder="" maxlength=50 type="text" value="<?php if(isset($StudentData['Identity']->IDNo)) echo $StudentData['Identity']->IDNo; else echo "";?>">
											</p>
											<p>
												<label for="">Passport No</label>
												<input  name="txtStudentPassportNo" id="txtStudentPassportNo" maxlength=50 type="text" value="<?php if(isset($StudentData['passport']->IDNo)) echo $StudentData['passport']->IDNo; else echo "";?>">
											</p>											
											<p>
												<label for="">Passport Exp Date</label>
												<input name="txtStudentPassportExpDate" id="txtStudentPassportExpDate"  maxlength=11 class="IDCarddatepicker" type="text" value="<?php if(isset($StudentData['passport']->ExpiredDate)) echo date("d M Y", strtotime($StudentData['passport']->ExpiredDate)); else echo "";?>">
											</p>												

										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<p>
												<label for="">KITAS NO</label>
												<input name="txtStudentKITASNo" id="txtStudentKITASNo" maxlength=50 type="text" value="<?php if(isset($StudentData['KITAS']->IDNo)) echo $StudentData['KITAS']->IDNo; else echo "";?>">
											</p>
											<p>
												<label for="">KITAS Exp Date</label>
												<input  name="txtStudentKITASExpDate" id="txtStudentKITASExpDate" maxlength=11 class="IDCarddatepicker" type="text" value="<?php if(isset($StudentData['KITAS']->ExpiredDate)) echo date("d M Y", strtotime($StudentData['KITAS']->ExpiredDate)); else echo "";?>">
											</p>	

											<p>
												<label for="">VISA No</label>
												<input name="txtStudentVISANo" id="txtStudentVISANo" placeholder="" value="<?php if(isset($StudentData['visa']->IDNo)) echo $StudentData['visa']->IDNo; else echo "";?>"  maxlength=50 type="text">
											</p>	
											<p>
												<label for="">VISA Exp Date</label>
												<input name="txtStudentVISAExpDate" id="txtStudentVISAExpDate"  placeholder="" class="IDCarddatepicker" value="<?php if(isset($StudentData['visa']->ExpiredDate)) echo date("d M Y", strtotime($StudentData['visa']->ExpiredDate)); else echo "";?>"  maxlength=11 type="text">
											</p>
										</form>
									</div>
								</div>
								
								</div>
							</div>
						</div>
					</div>
	<!--PARENT EMPLOYMENT AREA-->
	<div class="profile-setting">
			
				<div class="inside">
				<div class="form-section">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Father</span>
							</div>
							<div id="ID_card-father-edit-section" class="right clearfix edit-section 
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-father-information/" id="edit-father-ID_card">Edit</a></span>
							</div>
							<div id="ID_card-father-save-section" class="right clearfix hide save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-father-ID_card">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="save-father-ID_card">Save</a></span>
							</div>
						</div>
						<div class="content">
						<form  id="form-FatherIDCard" action="<?php echo site_url().'/';?>entry/ID_card/save_father_ID_card" method="post" class="front-editing">
							<div class="form-section" id="setting-father-ID_card">
								<div class="row clearfix box-equivalent">
				
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											
											<p>
												<label for="">
													Nationality
												</label>
												<select name="ddlFatherNationality" id="ddlFatherNationality">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($FatherNationality->NationalityID))
															{
																if ($FatherNationality->NationalityID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
											<label for="">Identity No</label>
												<input name="txtFatherIDNo" id="txtFatherIDNo" placeholder="" maxlength=50 type="text" value="<?php if(isset($FatherData['Identity']->IDNo)) echo $FatherData['Identity']->IDNo; else echo "";?>">
											</p>
											<p>
												<label for="">Passport No</label>
												<input  name="txtFatherPassportNo" id="txtFatherPassportNo" maxlength=50 type="text" value="<?php if(isset($FatherData['passport']->IDNo)) echo $FatherData['passport']->IDNo; else echo "";?>">
											</p>											
																						

										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<p>
												<label for="">Passport Exp Date</label>
												<input  name="txtFatherPassportExpDate" id="txtFatherPassportExpDate"  maxlength=11 class="IDCarddatepicker" type="text" value="<?php if(isset($FatherData['passport']->ExpiredDate)) echo date("d M Y", strtotime($FatherData['passport']->ExpiredDate)); else echo "";?>">
											</p>	
											<p>
												<label for="">KITAS NO</label>
												<input  name="txtFatherKITASNo" id="txtFatherKITASNo" maxlength=50 type="text" value="<?php if(isset($FatherData['KITAS']->IDNo)) echo $FatherData['KITAS']->IDNo; else echo "";?>">
											</p>
											<p>
												<label for="">KITAS Exp Date</label>
												<input  name="txtFatherKITASExpDate" id="txtFatherKITASExpDate"  maxlength=11 class="IDCarddatepicker" type="text" value="<?php if(isset($FatherData['KITAS']->ExpiredDate)) echo date("d M Y", strtotime($FatherData['KITAS']->ExpiredDate)); else echo "";?>">
											</p>	
										</form>
									</div>
								</div>
								
								</div>
							</div>
						</div>
					</div>

		</div>

	</div>

	<!--mother-->
	
	<div class="profile-setting">
			
				<div class="inside">
				<div class="form-section">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Mother</span>
							</div>
							<div id="ID_card-mother-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-mother-information/" id="edit-mother-ID_card">Edit</a></span>
							</div>
							<div id="ID_card-mother-save-section" class="right clearfix hide save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-mother-ID_card">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="save-mother-ID_card">Save</a></span>
							</div>
						</div>
						<div class="content">
						<form  id="form-MotherIDCard" action="<?php echo site_url().'/';?>entry/ID_card/save_mother_ID_card" method="post" class="front-editing">
							<div class="form-section" id="setting-mother-ID_card">
								<div class="row clearfix box-equivalent">
				
									<div class="column one-half">
										<div class="inside-left">
											<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="txtFormNumber"/>
											
											<p>
												<label for="">
													Nationality
												</label>
												<select name="ddlMotherNationality" id="ddlMotherNationality">
													<option value="">--Please Choose--</option>
													<?php foreach($Country as $Row): ?>
														<option 
															<?php if(isset($MotherNationality->NationalityID))
															{
																if ($MotherNationality->NationalityID == $Row->CountryID )
																echo 'selected="selected"'; 
															}
															else 
															{
																echo '';
															} ?> value="<?php echo $Row->CountryID;?>"> 
															<?php echo $Row->CountryName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											
											</p>
											<p>
											<label for="">Identity No</label>
												<input name="txtMotherIDNo" id="txtMotherIDNo" placeholder="" maxlength=50 type="text" value="<?php if(isset($MotherData['Identity']->IDNo)) echo $MotherData['Identity']->IDNo; else echo "";?>">
											</p>
											<p>
												<label for="">Passport No</label>
												<input name="txtMotherPassportNo" id="txtMotherPassportNo" maxlength=50 type="text" value="<?php if(isset($MotherData['passport']->IDNo)) echo $MotherData['passport']->IDNo; else echo "";?>">
											</p>											
																							

										</div>
								</div>
								<div class="column one-half">
									<div class="inside-right">
											<p>
												<label for="">Passport Exp Date</label>
												<input  name="txtMotherPassportExpDate" id="txtMotherPassportExpDate"  maxlength=11 class="IDCarddatepicker" type="text" value="<?php if(isset($MotherData['passport']->ExpiredDate)) echo date("d M Y", strtotime($MotherData['passport']->ExpiredDate)); else echo "";?>">
											</p>
											<p>
												<label for="">KITAS NO</label>
												<input  name="txtMotherKITASNo" id="txtMotherKITASNo" maxlength=50 type="text" value="<?php if(isset($MotherData['KITAS']->IDNo)) echo $MotherData['KITAS']->IDNo; else echo "";?>">
											</p>
											<p>
												<label for="">KITAS Exp Date</label>
												<input  name="txtMotherKITASExpDate" id="txtMotherKITASExpDate" maxlength=11 class="IDCarddatepicker" type="text" value="<?php if(isset($MotherData['KITAS']->ExpiredDate)) echo date("d M Y", strtotime($MotherData['KITAS']->ExpiredDate)); else echo "";?>">
											</p>	
										</form>
									</div>
								</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

  

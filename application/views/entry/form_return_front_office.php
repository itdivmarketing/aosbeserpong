<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<div class="box-container">
				<h2 class="heading">Return Form Front Office</h2>
				<form name="frmSearch" action="<?php echo site_url().'/';?>entry/form_return_front_office/search" method="get">
							<input type="hidden" name="hfPage" value="<?php if(isset($_GET['hfPage'])) echo $_GET['hfPage']; else echo '1'?>"/>
							<p>
								<label>Admission Year : </label>
								<span>
									<input type="text" name="txtAdmissionYear" value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Semester : </label>
								<span>
									<input type="text" name="txtAdmissionSemester" value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>School Level : </label>
								<span class="custom-combobox">
									<select name="ddlSchoolLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($SchoolLevel as $Row): ?>
											<option 

												<?php if(isset($_GET['ddlSchoolLevel']))
												{
													if ($_GET['ddlSchoolLevel'] == $Row->SchoolLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SchoolLevelID;?>"> 
												<?php echo $Row->SchoolLevelShortName .' - '. $Row->SchoolLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Admission Term : </label>
								<span class="custom-combobox">
									<select name="ddlAdmissionTerm">
										<option value="">--Please Choose--</option>
										<?php foreach($PretestTerm as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlAdmissionTerm']))
												{
													if ($_GET['ddlAdmissionTerm'] == $Row->TermID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TermID;?>"> 
												<?php echo $Row->TermID .' - '. $Row->TermName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Year Level : </label>
								<span class="custom-combobox">
									<select name="ddlYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($YearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>No Form: </label>
								<span>
									<input type="text" name="txtFormNumber" value="<?php if(isset($_GET['txtFormNumber'])) echo $_GET['txtFormNumber']; else echo "";?>" />
								</span>
							</p>
							<p align="center" style="clear:both;padding-top:20px;">
								<input type="submit" class="button button-primary" value="Search" id="btnSearch">
								<!--<input type="reset" class="button" value="Reset" id="btnReset">-->
							</p>
						</form>
						<div class="floatThead-wrapper">
							<table style="margin-top:0;margin-bottom:0;" id="tblView" class="bordered zebra">
								<thead>
									<tr>
										<th width="65px">Action</th>
										<th>Form Number</th>
										<th>Student Name</th>
										<th>School Level</th>
										<th>Year Level</th>
									</tr>
								</thead>
								<tbody id="RateContent">
									<?php if($data['result']) {
										foreach($data['result'] as $Row): ?>
										<tr data-id="<?php echo $Row->FormNumber;?>">
											<td style="text-align:center">
												<a class="icon icon-view btnView"></a>
											</td>
											<td><?php echo $Row->FormNumber; ?></td>
											<td><?php echo $Row->StudentName; ?></td>
											<td><?php echo $Row->SchoolLevelName; ?></td>
											<td><?php echo $Row->YearLevelName; ?></td>
										</tr>
									<?php endforeach; 
									}
									else { echo '<tr ><td colspan="10">There\'s no Data</td></tr>';}?>
								</tbody>
							</table>
						</div>
						<div id="paging"<?php if($data['pagination']) echo ''; else echo 'style="display:none"';?>>
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
						</div>
						
						<?php
								if($data['pagination'])
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
							?>


				</div>
			</div>
		</div>
	</div>
</div>


<div style="display: none">
	<div id="internal-popup">
		<div class="single-content popup-content" style="padding:60px">
			<h2 class="heading">Add Form Return</h2>
			<form name="frmAdd" class="styled-content" action="<?php echo site_url().'/';?>entry/form_return_front_office/save_form_return_FO" method="post">
								<input type="hidden" name="hfAdmissionID"/>
								<input type="hidden" name="hfScheduleID"/>
								<input type="hidden" name="hfVenue"/>
								<input type="hidden" name="hfViewAdmissionSemester"/>
								
								<p>
									<label>Admission ID* : </label>
									<input type="text" name="txtViewAdmissionID"  maxlength=6  value="" readonly="readonly"/>
								</p>
								<p>
									<label>Admission Year* : </label>
									<span>
										<input type="text" name="txtViewAdmissionYear" maxlength=4 value="<?php if(isset($_GET['txtAdmissionYear'])) echo $_GET['txtAdmissionYear']; else echo $year;?>" readonly="readonly"/>
									</span>
								</p>
								<p>
									<label>Semester* : </label>
									<span>
										<input type="text" name="txtViewAdmissionSemester" maxlength=1 value="<?php if(isset($_GET['txtAdmissionSemester'])) echo $_GET['txtAdmissionSemester']; else echo $semester;?>" readonly="readonly"/>
									</span>
								</p>
									<p>
								<label>School Level* : </label>
								<span>
									<input type="text" name="txtViewSchoolLevel" value="<?php if(isset($_GET['txtViewSchoolLevel'])) echo $_GET['txtViewSchoolLevel']; else echo "";?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>Admission Term* : </label>
								<span>
									<input type="text" maxlength=13 name="txtViewAdmissionTerm" value="<?php if(isset($_GET['txtViewAdmissionTerm'])) echo $_GET['txtViewAdmissionTerm']; else echo "";?>" readonly="readonly"/>
								</span>
							</p>
							<p>
								<label>No Form* : </label>
								<span>
									<input type="text" name="txtViewFormNumber" maxlength=13  value="" />
								</span>
								<input type="button" id="btnCheck" class="button button-primary" value="check" ></input>
							</p>
							<p>
								<label>First Name* : </label>
								<span>
									<input type="text"  name="txtViewFirstName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Middle Name : </label>
								<span>
									<input type="text"  name="txtViewMiddleName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Last Name : </label>
								<span>
									<input type="text"  name="txtViewLastName" maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Date Of Birth*: </label>
								<span class="custom-datepicker">
									<input type="text"  class="datepicker" name="txtViewDateOfBirth"  maxlength=11 value="" readonly="true"/>
								</span>
							</p>
							<p>
								<label>Place Of Birth* : </label>
								<span>
									<input type="text" name="txtViewPlaceOfBirth"  maxlength=50 value="" />
								</span>
							</p>
							<p>
								<label>Gender* : </label>
								<span class="custom-combobox">
									<select name="ddlViewGender">
										<option value="">--Please Choose--</option>
										<?php foreach($Gender as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlGender']))
												{
													if ($_GET['ddlGender'] == $Row->GenderID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->GenderID;?>"> 
												<?php echo $Row->GenderDescription;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Child Status* : </label>
								<span class="custom-combobox">
									<select name="ddlViewChildStatus">
										<option value="">--Please Choose--</option>
										<?php foreach($ChildStatus as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlChildStatus']))
												{
													if ($_GET['ddlChildStatus'] == $Row->ChildStatusID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->ChildStatusID;?>"> 
												<?php echo $Row->ChildStatusName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Type OF Registration* : </label>
								<span class="custom-combobox">
									<select name="ddlViewTypeOfRegistration">
										<option value="">--Please Choose--</option>
										<?php foreach($TypeOfRegistration as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlTypeOfRegistration']))
												{
													if ($_GET['ddlTypeOfRegistration'] == $Row->TypeOfRegistrationId )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->TypeOfRegistrationId;?>"> 
												<?php echo $Row->TypeOfRegistration;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>To Year Level* : </label>
							<span class="custom-combobox">
									<select name="ddlViewToYearLevel">
										<option value="">--Please Choose--</option>
										<?php foreach($ViewYearLevel as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlYearLevel']))
												{
													if ($_GET['ddlYearLevel'] == $Row->YearLevelID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->YearLevelID;?>"> 
												<?php echo $Row->YearLevelName;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>To Semester* : </label>
								<span class="custom-combobox">
									<select name="ddlViewToSemester">
										<option value="">--Please Choose--</option>
										<?php foreach($Semester as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlSemester']))
												{
													if ($_GET['ddlSemester'] == $Row->SmtID )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->SmtID;?>"> 
												<?php echo $Row->SmtDescription;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Date Of Entry* : </label>
								<span>
									<input type="text" class="datetimepicker" maxlength=11 name="txtViewDateOfEntry" value="" readonly="true" />
								</span>
							</p>
							<p>
								<label>Date Of Form Receipt* : </label>
								<span>
									<input type="text" class="datetimepicker"  maxlength=11 name="txtViewDateOfFormReceipt" value="" readonly="true" />
								</span>
							</p>
							<p>
								<label>Joined Year : </label>
								<span class="custom-combobox">
									<select name="ddlViewJoinedYear">
										<option value="">--Please Choose--</option>
										<?php foreach($JoinedYear as $Row): ?>
											<option 
												<?php if(isset($_GET['ddlJoinedYear']))
												{
													if ($_GET['ddlJoinedYear'] == $Row->AcademicYear )
													echo 'selected="selected"'; 
												}
												else 
												{
													echo '';
												} ?> value="<?php echo $Row->AcademicYear;?>"> 
												<?php echo $Row->AcademicYear;?> 
											</option>
										<?php endforeach; ?>
									</select>
								</span>
							</p>
							<p>
								<label>Joined Month : </label>
								<span class="custom-combobox">
									<select name="ddlViewJoinedMonth">
										<option value="">--Please Choose--</option>
											<option value="1"> 
												<?php echo "January"; ?> 
											</option>
											<option value="2"> 
												<?php echo "February"; ?> 
											</option>
											<option value="3"> 
												<?php echo "March"; ?> 
											</option>
											<option value="4"> 
												<?php echo "April"; ?> 
											</option>
											<option value="5"> 
												<?php echo "May"; ?> 
											</option>
											<option value="6"> 
												<?php echo "June"; ?> 
											</option>
											<option value="7"> 
												<?php echo "July"; ?> 
											</option>
											<option value="8"> 
												<?php echo "August"; ?> 
											</option>
											<option value="9"> 
												<?php echo "September"; ?> 
											</option>
											<option value="10"> 
												<?php echo "October"; ?> 
											</option>
											<option value="11"> 
												<?php echo "November"; ?> 
											</option>
											<option value="12"> 
												<?php echo "December"; ?> 
											</option>
									</select>
								</span>
							</p>
								<p align="center" style="clear:both;padding-top:20px;">
									<input type="button" class="button button-primary" value="Save" id="btnSave" onclick="validate();">
								</p>
							</form>
		</div>
	</div>
</div>
							

<div class="site-content" id="site-content">
	<div class="main-content has-widget" id="main-content">
		<div class="container">
			<?php $this->load->view("entry/header"); ?>
			<div class="box-container" style="margin-top:20px;">
				<div class="container w-640px profile-setting">
					<div class="form-section" id="setting-CESibling">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-personal left" style="margin-top: 10px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Current/Enrolled Together</span>
							</div>
							<div id="edit-CESibling-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-CESibling/" id="edit-CESibling">Edit</a></span>
							</div>
							<div id="cancel-edit-CESibling-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-edit-CESibling/" id="cancel-edit-CESibling">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="btnSaveCESibling">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-CESibling" action="<?php echo site_url().'/';?>entry/sibling/save_current_enrolled_sibling" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">			
											<p>
												<label for="">Student ID*</label>
												<input name="txtCheckStudentID" id="txtCheckStudentID"  maxlength=10 type="text" style="float:left;width:70% !important" />
												<a class="div-search" id="search-sibling"><i class="icon icon-search-modal"></i></a>
												
									
											</p>
											<p>
											
												
												</br><input type="button" value="CHECK" id="btnCheck" class="button button-primary"  />
											</p>
											<table id="TableInfoSibling" style="margin-bottom:30px">
								<thead>
									<tr>
										<th>Student/RegistrantID</th>
										<th>Name</th>
										<th>Year Level</th>
										<th>Pathway</th>
									</tr>
								</thead>
								<tbody>

								</tbody>

							</table>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">
								
										</div>
									</div>
								
							</div>
							<table id="TableCESibling" style="margin-bottom: ">
								<thead>
									<tr>
										<th></th>
										<th>Student/RegistrantID</th>
										<th>Name</th>
										<th>Year Level</th>
										<th>Pathway</th>
										<th>Student Status</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($CESiblingData)){ ?>
										<?php foreach($CESiblingData as $row): ?>
										<tr>
									
										<td class='IconColumn'>
											<?php if(!($row->SelfID==1)){ ?>
										<i class='icon icon-trash hide'></i>
										</i>
											<?php } ?>
										</td>
									
										<td>
											<?php echo $row->StudentID; ?>		
										</td>
										<td>
											<?php echo $row->Name; ?>		
										</td>
										<td>
											<?php echo $row->YearLevelName; ?>		
										</td>
										<td>
											<?php echo $row->PathWay==""?'-':$row->PathWay; ?>		
										</td>
										<td>
											<?php echo $row->StudentStatusID==1?"Yes":"No"; ?>		
										</td>
										</tr>
										<?php endforeach; ?>
									<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a  class="button button-primary" id="btnAddCESibling">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
								
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>
					

					<div class="form-section" id="setting-OthersSibling">
						<div class="header clearfix">
							<div class="left clearfix">
								<span class="icon icon-widget-family-member left" style="margin-top: 15px;"></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="title left">Others Siblings</span>
							</div>
							<div id="OtherSibling-edit-section" class="right clearfix edit-section  
							<?php if(isset($StudentValidity)&& count($StudentValidity)>0){
								if( isset($StudentStatus)&&$StudentStatus!=1){
										echo '';
								}
								else
										echo 'hide';
							}
							else{
								echo 'hide';
							} ?>
							">
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="link left"><a href="#!/edit-OtherSibling/" id="edit-OtherSibling">Edit</a></span>
							</div>
							<div id="OtherSibling-save-section" class="right hide clearfix save-section">
								<span class="link left"><a href="#!/cancel-action/" id="cancel-edit-OtherSibling">Cancel</a></span>
								<span class="left" class="divider-left" style="	display: inline-block; width: 1px; height: 36px;background-color: #c9c9c9; margin: 7px 15px;"></span>
								<span class="action left"><a  class="button button-primary" id="btnSaveOthersSibling">Save</a></span>
							</div>
						</div>
						<div class="content">

							<div class="row clearfix">
								<form  id="form-OthersSibling" action="<?php echo site_url().'/';?>entry/sibling/save_others_sibling" method="post" class="front-editing">
									<div class="column one-half">
										<div class="inside-left">
											<p>
												<label for="">Name</label>
												<input name="txtNameOthersSibling" id="txtNameOthersSibling"   placeholder=""   maxlength=100 type="text">
											</p>
											<p>
												<label for="">Date Of Birth</label>
												<input name="txtDOBOthersSibling" readonly="true" id="txtDOBOthersSibling"  class="siblingdatepicker" placeholder=""   maxlength=13 type="text">
											</p>
											<p>
												<label for="">Admission Period</label>
												<input name="txtAdmissionPeriodOthersSibling"  id="txtAdmissionPeriodOthersSibling"   maxlength=4 type="text">
											</p>
										</div>
									</div>
									<div class="column one-half">
										<div class="inside-right">

											<p>
												
												<label for="">School Level</label>
												<select name="ddlSchoolLevel" id="ddlSchoolLevel">
													<option value="">--Please Choose--</option>
													<?php foreach($SchoolLevel as $Row): ?>
														<option value="<?php echo $Row->SchoolLevelID;?>"> 
															<?php echo $Row->SchoolLevelName;?> 
														</option>
													<?php endforeach; ?>
												</select>
											</p>
											<p>
												<label for="">Year Level</label>
												<select name="ddlYearLevel" id="ddlYearLevel">
													<option value="">--Please Choose--</option>
												</select>
											</p>
										</div>
									</div>
									
								
							</div>
							<table id="TableOthersSibling">
								<thead>
									<tr>
										<th></th>
										<th>Name</th>
										<th>Date Of Birth</th>
										<th>School Level</th>
										<th>Year Level</th>
										<th>Admission Period</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($OthersSiblingData)){ ?>
									<?php foreach($OthersSiblingData as $row): ?>	
										<tr>
										<td class='IconColumn'>
										<i class='icon icon-trash hide'></i>&nbsp;<i class='icon icon-edit hide'>
										</i>
										</td>
										<td>
											<?php echo $row->PotentialSiblingName; ?>		
										</td>
										<td>
											<?php echo date("d M Y", strtotime($row->Birthday)); ?>		
										</td>
										<td>
											<?php echo $row->SchoolLevelName; ?>		
										</td>
										<td>
											<?php echo $row->YearLevelName; ?>		
										</td>
										<td>
											<?php echo $row->AdmissionPeriod; ?>		
										</td>
										</tr>
										<?php endforeach; ?>
										<?php } ?>
								</tbody>
								<tfoot >
									<tr>
										<td colspan="7" class="text-right">
											<a  class="button button-primary" id="btnAddOthersSibling">Add New</a>
										</td>
									</tr>
								</tfoot>
							</table>
								
							<input type="hidden" value="<?php  if(isset($FormNumber)) echo $FormNumber; else echo ""; ?>" name="hfFormNumber"/>
								</form>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</div>


    

<div style="display: none">
  <!--div start pop up -->
	<div id="internal-popup-sibling">
			<div class="single-content popup-content" style="padding:60px">
					
					<div id="body" class="styled-content">
					<div class="inside">
						<div class="row">
					<input type="hidden" name="hfPage" value="1" />
				
					<p>
						<label>
							<span>Student Name</span></br>
							<span><i>Nama Siswa</i></span>
						</label>
					</p>
					<p>
						<input type="text" class="text" id="txtSearchStudentName_Sibling" ></input>
					</p>
					<p>
						<input type="button" value="SEARCH" id="btnSearch" class="button button-primary" style="align:center" onclick="Search(1);"/>
					</p>
					</div>
					</div>
					</div>
				
			<table id="TableSearchSibling" class="ListData bordered zebra">
				<thead>
					<tr>
						<th></th>
						<th>StudentID</th>
						<th>Student Name</th>
					</tr>
					</thead>
					
					<tbody>
					
					
					
					</tbody>
						</table>
						
						
				
					
					<div id="paging" style="display:none">
								<div class="pagination">
    								<span class="page-display"></span>
    								<span class="page-number pages"></span>
    								<span class="page-button">
        							<a href="#" class="prev"></a>
        							<a href="#" class="next"></a>
    								</span>
								</div>
					</div>
						<?php
								if(isset($data['pagination']))
								echo "<script>
										Show_Pagination({
											'Selector' : '#paging',
											'TotalPage' : ".$data['pagination']['last'].",
											'CurrentPage' : ".$data['pagination']['current'].",
											'PreviousPage' : ".$data['pagination']['previous'].",
											'NextPage' : ".$data['pagination']['next'].",
											'InfoPage' : '".$data['pagination']['info']."'
											});
									</script>";
						?>
			</div>	
	</div><!-- #internal-popup -->
</div>